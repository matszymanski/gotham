﻿namespace StrazzeGT.UI.Ferdec.Forms
{
    partial class FerdecDataSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.etOrders = new System.Windows.Forms.RadioButton();
            this.etTrades = new System.Windows.Forms.RadioButton();
            this.gbExtraction = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbInstrument = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGo = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gbExtraction.SuspendLayout();
            this.SuspendLayout();
            // 
            // etOrders
            // 
            this.etOrders.AutoSize = true;
            this.etOrders.Location = new System.Drawing.Point(19, 74);
            this.etOrders.Name = "etOrders";
            this.etOrders.Size = new System.Drawing.Size(56, 17);
            this.etOrders.TabIndex = 0;
            this.etOrders.Text = "Orders";
            this.etOrders.UseVisualStyleBackColor = true;
            this.etOrders.CheckedChanged += new System.EventHandler(this.etOrders_CheckedChanged);
            // 
            // etTrades
            // 
            this.etTrades.AutoSize = true;
            this.etTrades.Checked = true;
            this.etTrades.Location = new System.Drawing.Point(19, 31);
            this.etTrades.Name = "etTrades";
            this.etTrades.Size = new System.Drawing.Size(58, 17);
            this.etTrades.TabIndex = 1;
            this.etTrades.TabStop = true;
            this.etTrades.Text = "Trades";
            this.etTrades.UseVisualStyleBackColor = true;
            this.etTrades.CheckedChanged += new System.EventHandler(this.etTrades_CheckedChanged);
            // 
            // gbExtraction
            // 
            this.gbExtraction.Controls.Add(this.label2);
            this.gbExtraction.Controls.Add(this.etOrders);
            this.gbExtraction.Controls.Add(this.cbInstrument);
            this.gbExtraction.Controls.Add(this.etTrades);
            this.gbExtraction.Location = new System.Drawing.Point(12, 68);
            this.gbExtraction.Name = "gbExtraction";
            this.gbExtraction.Size = new System.Drawing.Size(590, 163);
            this.gbExtraction.TabIndex = 2;
            this.gbExtraction.TabStop = false;
            this.gbExtraction.Text = "Extraction Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Instrument";
            // 
            // cbInstrument
            // 
            this.cbInstrument.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbInstrument.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbInstrument.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInstrument.FormattingEnabled = true;
            this.cbInstrument.Location = new System.Drawing.Point(69, 123);
            this.cbInstrument.Name = "cbInstrument";
            this.cbInstrument.Size = new System.Drawing.Size(515, 21);
            this.cbInstrument.TabIndex = 5;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(114, 27);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(103, 20);
            this.dateTimePicker1.TabIndex = 3;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Extraction Date";
            // 
            // btnGo
            // 
            this.btnGo.Location = new System.Drawing.Point(31, 264);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(75, 23);
            this.btnGo.TabIndex = 5;
            this.btnGo.Text = "Go";
            this.btnGo.UseVisualStyleBackColor = true;
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(521, 264);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // FerdecDataSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 324);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnGo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.gbExtraction);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FerdecDataSelector";
            this.Text = "FerdecDataSelector";
            this.gbExtraction.ResumeLayout(false);
            this.gbExtraction.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton etOrders;
        private System.Windows.Forms.RadioButton etTrades;
        private System.Windows.Forms.GroupBox gbExtraction;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbInstrument;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGo;
        private System.Windows.Forms.Button btnCancel;
    }
}