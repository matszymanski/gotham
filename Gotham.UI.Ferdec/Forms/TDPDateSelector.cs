﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StrazzeGT.UI.Ferdec.Forms
{
    public partial class TDPDateSelector : Form
    {
        public DateTime SelectedDate { get; set; }

        public TDPDateSelector()
        {
            InitializeComponent();
            this.dtpExtractionDate.Value = DateTime.Now.Date.AddDays(-1);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            this.SelectedDate = this.dtpExtractionDate.Value.Date;
            this.Close();
        }
    }
}
