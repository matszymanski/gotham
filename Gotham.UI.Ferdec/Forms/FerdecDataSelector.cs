﻿using StrazzeGT.UI.Ferdec.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StrazzeGT.UI.Ferdec.Forms
{
    public partial class FerdecDataSelector : Form
    {
        public string Instrument { get; set; }
        public DateTime ExtractionDate { get; set; }
        public DataTable Data { get; set; }
        public string ExtractionType { get; set; }

        public FerdecRetrievalPOC selector = null;
        public FerdecDataSelector()
        {
            InitializeComponent();
            this.dateTimePicker1.Value = DateTime.Now.AddDays(-1);
            this.selector = new Data.FerdecRetrievalPOC();
            this.FillInstruments();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            if(!this.QueryBuilder())
            {
                MessageBox.Show("Could not generate query.");
                return;
            }

            this.Data = new DataTable();

            string instrument = Convert.ToString(this.cbInstrument.SelectedValue);

            if(this.ExtractionType.Equals("ORDERS"))
            {
                this.Data = selector.FerdecOrderDataRetrieval(instrument, this.ExtractionDate);
            }
            else if (this.ExtractionType.Equals("TRADES"))
            {
                this.Data = selector.FerdecTradeDataRetrieval(instrument, this.ExtractionDate);
            }

            this.Close();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            //this.SelectDailyInsturments();
        }

        RadioButton GetCheckedRadio(Control container)
        {
            foreach (var control in container.Controls)
            {
                RadioButton radio = control as RadioButton;

                if (radio != null && radio.Checked)
                {
                    return radio;
                }
            }

            return null;
        }

        private void etTrades_CheckedChanged(object sender, EventArgs e)
        {
            //this.SelectDailyInsturments();
        }

        private void etOrders_CheckedChanged(object sender, EventArgs e)
        {
            //this.SelectDailyInsturments();
        }

        private void SelectDailyInsturments()
        {
            if (this.QueryBuilder())
            {
                DataTable inst = selector.Instruments(this.ExtractionType, this.ExtractionDate);

                this.cbInstrument.DataSource = inst.DefaultView;
                this.cbInstrument.DisplayMember = "INSTRUMENT";
                this.cbInstrument.ValueMember = "INSTRUMENT";
                this.cbInstrument.BindingContext = this.BindingContext;
                //this.cbInstrument.SelectedValue = -1;
            }
        }

        private void FillInstruments()
        {
            DataTable inst = selector.Instruments("", DateTime.Now.Date);

            this.cbInstrument.DataSource = inst.DefaultView;
            this.cbInstrument.DisplayMember = "INSTRUMENT";
            this.cbInstrument.ValueMember = "INSTRUMENT";
            this.cbInstrument.BindingContext = this.BindingContext;
        }

        private bool QueryBuilder()
        {
            RadioButton b = this.GetCheckedRadio(this.gbExtraction);
            if (b != null)
            {
                this.ExtractionType = b.Text.ToUpper();
                this.ExtractionDate = this.dateTimePicker1.Value.Date;

                return true;
            }

            return false;
        }
    }
}
