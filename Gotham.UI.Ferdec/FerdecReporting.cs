﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using System.Data;
using System.Windows.Forms;
using StrazzeGT.UI.Ferdec.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using StrazzeGT.UI.Ferdec.Data;

namespace StrazzeGT.UI.Ferdec
{
    public partial class FerdecReporting
    {
        private void FerdecReporting_Load(object sender, RibbonUIEventArgs e)
        {

        }

        private void btnRetrieveData_Click(object sender, RibbonControlEventArgs e)
        {
            FerdecDataSelector s = new FerdecDataSelector();
            s.ShowDialog();

            if (s.Data != null && s.Data.Rows.Count > 0)
            {
                this.Cleanup(e);
                ADODB.Recordset dataToPaste = ConvertToRecordset(s.Data);
                this.PasteRecordSet(dataToPaste, e);
                this.FormatColumns(s.Data, e);
            }
        }

        private void btnCMDPExtractor_Click(object sender, RibbonControlEventArgs e)
        {
            TDPDateSelector s = new TDPDateSelector();
            DialogResult result = s.ShowDialog();

            if (result == DialogResult.OK)
            {
                DateTime selectedDate = s.SelectedDate;

                //string query = "select * from tdp.CORE.trayport_trade where datetime > convert(date,(getdate ()-1)) and (InitSleeve = 'True' or AggSleeve = 'True')";
                //query = @"
                //    select
                //    pkid
                //    ,EngineID
                //    ,OrderID
                //    ,Action
                //    --,DateTime
                //    ,CONVERT(varchar, DateTime) as 'Date Time'
                //    ,Price
                //    ,SettlementPrice
                //    ,Volume
                //    ,InstID
                //    ,InstCode
                //    ,InstName
                //    ,FirstSequenceID
                //    ,SeqSpan
                //    ,FirstSequenceItemID
                //    ,FirstSequenceItemName
                //    ,SecondSequenceItemID
                //    ,SecondSequenceItemName
                //    ,TermFormatID
                //    ,TGTermFormatID
                //    ,ForceTrade
                //    ,PopupTradeConfirm
                //    ,IgnoreMinQuantity
                //    ,TradeID
                //    ,RelationshipID
                //    ,AggressorCompany
                //    ,AggressorCompanyID
                //    ,AggressorTrader
                //    ,AggressorTraderID
                //    ,AggressorUser
                //    ,AggressorUserID
                //    ,AggressorAction
                //    ,AggressorBroker
                //    ,AggressorBrokerID
                //    ,AggressorAccount
                //    ,AggressorAccountID
                //    ,InitiatorCompany
                //    ,InitiatorCompanyID
                //    ,InitiatorTrader
                //    ,InitiatorTraderID
                //    ,InitiatorUser
                //    ,InitiatorUserID
                //    ,InitiatorAction
                //    ,InitiatorBroker
                //    ,InitiatorBrokerID
                //    ,InitiatorAccount
                //    ,InitiatorAccountID
                //    --,LastUpdate
                //    ,CONVERT(varchar, LastUpdate) as 'Last Update'
                //    ,ManualDeal
                //    ,AuctionedDeal
                //    ,VoiceDeal
                //    ,InitSleeve
                //    ,AggSleeve
                //    ,PNC
                //    ,ClearingStatus
                //    ,ClearingID
                //    ,InitiatorOwnedSpread
                //    ,AggressorOwnedSpread
                //    ,PostTradeNegotiating
                //    ,UnderInvestigation
                //    ,PriceSetting
                //    ,AggressorVoiceDealConfirmState
                //    ,InitiatorVoiceDealConfirmState
                //    ,ClearingHouse
                //    ,FromBrokenSpread
                //    --,Time
                //    ,CONVERT(varchar, Time) as 'Time'
                //    ,ExecutionVenueID
                //    ,ForeignContractID
                //    ,ForeignRelationshipID
                //    ,ExecutionWorkflow
                //    --,XMLReceiveTime
                //    ,CONVERT(varchar, XMLReceiveTime) as 'XML Receive Time'
                //    --from tdp.CORE.trayport_trade where datetime > convert(date,(getdate()-1)) and (InitSleeve = 'True' or AggSleeve = 'True')
                //    from tdp.CORE.trayport_trade where datetime > convert(date, @ExtractionDate) and (InitSleeve = 'True' or AggSleeve = 'True')
                //    ";

                FerdecRetrievalPOC selector = new FerdecRetrievalPOC();
                //DataTable d = selector.ExecTDPQuery(query, selectedDate);
                DataTable d = selector.FerdecSleevesDataRetrieval(selectedDate);

                if (d != null && d.Rows.Count > 0)
                {
                    this.Cleanup(e);
                    ADODB.Recordset dataToPaste = ConvertToRecordset(d);
                    this.PasteRecordSet(dataToPaste, e);
                    this.FormatColumns(d, e);
                }
            }
        }

        public void Cleanup(RibbonControlEventArgs e)
        {
            Excel.Window window = (Excel.Window)e.Control.Context;
            Excel.Worksheet activeWorksheet = ((Excel.Worksheet)window.Application.ActiveSheet);
            activeWorksheet.Cells.Clear();
        }

        private void FormatColumns(DataTable dt, RibbonControlEventArgs e)
        {
            Excel.Window window = (Excel.Window)e.Control.Context;
            Excel.Worksheet activeWorksheet = ((Excel.Worksheet)window.Application.ActiveSheet);

            //if (!ETRMDataAccess.IsEmpty(dt))
            //{
            //    for (Int32 i = 1; i < dt.Columns.Count + 1; i++)
            //    {
            //        Excel.Range topCell = (Excel.Range)activeWorksheet.Cells[1, i];
            //        topCell.EntireColumn.NumberFormat = this.AssignFormatType(dt.Columns[i - 1].DataType.Name);
            //    }
            //}

            for (Int32 i = 1; i < dt.Columns.Count + 1; i++)
            {
                Excel.Range topCell = (Excel.Range)activeWorksheet.Cells[1, i];
                topCell.EntireColumn.NumberFormat = this.AssignFormatType(dt.Columns[i - 1].DataType.Name);
            }

            Excel.Range topCell1 = (Excel.Range)activeWorksheet.Cells[1, 1];
            topCell1.EntireColumn.NumberFormat = "@";

        }
        private string AssignFormatType(string columnTypeName)
        {
            string formatSpec = "@";

            switch (columnTypeName)
            {
                case CommonFormats.DateTime:
                    formatSpec = "dd MMM yyyy";
                    break;
                case CommonFormats.Decimal:
                    formatSpec = "#,###.0000";
                    //formatSpec = "#'##0.00_ ;[Red]-#'##0.00";
                    break;
                case CommonFormats.Int32:
                    formatSpec = "#,###";
                    break;
                case CommonFormats.Currency:
                    formatSpec = "#,##0.00_ ;[Red]-#,##0.00";
                    break;
                case CommonFormats.String:
                    break;
            }

            return formatSpec;
        }

        private void PasteRecordSet(ADODB.Recordset pdRS, RibbonControlEventArgs e)
        {
            if (pdRS != null)
            {
                try
                {
                    pdRS.MoveFirst();
                    Int32 rows = pdRS.RecordCount;
                    Int32 cols = pdRS.Fields.Count;
                    this.CreateHeader(e, pdRS);
                    Excel.Range topLeft = this.GetTopLeft(e, rows, cols);
                    topLeft.CopyFromRecordset(pdRS);
                    topLeft.EntireColumn.AutoFit();
                }
                catch { }
            }
        }

        void CreateHeader(RibbonControlEventArgs e, ADODB.Recordset pdRS)
        {
            Excel.Window window = (Excel.Window)e.Control.Context;
            Excel.Worksheet sheet = ((Excel.Worksheet)window.Application.ActiveSheet);
            Excel.Range tl = (Excel.Range)sheet.Cells[1, 1];

            for (Int32 i = 0; i < pdRS.Fields.Count; i++)
            {
                Excel.Range header = (Excel.Range)sheet.Cells[1, i + 1];
                header.Value2 = pdRS.Fields[i].Name;
            }
        }

        Excel.Range GetTopLeft(RibbonControlEventArgs e, Int32 rows, Int32 cols)
        {
            Excel.Window window = (Excel.Window)e.Control.Context;
            Excel.Worksheet sheet = ((Excel.Worksheet)window.Application.ActiveSheet);
            Excel.Range tl = (Excel.Range)sheet.Cells[2, 1];
            Excel.Range br = (Excel.Range)sheet.Cells[rows + 1, cols];
            Excel.Range topLeftExtended = sheet.get_Range(tl, br);
            return topLeftExtended;
        }


        public ADODB.Recordset ConvertToRecordset(DataTable inTable)
        {
            ADODB.Recordset result = new ADODB.Recordset();
            try
            {
                result.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                ADODB.Fields resultFields = result.Fields;
                System.Data.DataColumnCollection inColumns = inTable.Columns;

                foreach (DataColumn inColumn in inColumns)
                {
                    resultFields.Append(inColumn.ColumnName
                        , TranslateType(inColumn.DataType)
                        , inColumn.MaxLength
                        , inColumn.AllowDBNull ? ADODB.FieldAttributeEnum.adFldIsNullable :
                                                 ADODB.FieldAttributeEnum.adFldUnspecified
                        , null);
                }

                result.Open(System.Reflection.Missing.Value
                        , System.Reflection.Missing.Value
                        , ADODB.CursorTypeEnum.adOpenStatic
                        , ADODB.LockTypeEnum.adLockOptimistic, 0);

                foreach (DataRow dr in inTable.Rows)
                {
                    result.AddNew(System.Reflection.Missing.Value, System.Reflection.Missing.Value);

                    for (int columnIndex = 0; columnIndex < inColumns.Count; columnIndex++)
                    {
                        var type = dr[columnIndex].GetType();
                        resultFields[columnIndex].Value = dr[columnIndex];
                    }
                }
            }
            catch (Exception ex)
            {
                //ETRMDataAccess.WriteLogEntry(ex.Message);
            }

            return result;
        }

        ADODB.DataTypeEnum TranslateType(Type columnType)
        {
            switch (columnType.UnderlyingSystemType.ToString())
            {
                case "System.Boolean":
                    return ADODB.DataTypeEnum.adBoolean;

                case "System.Byte":
                    return ADODB.DataTypeEnum.adUnsignedTinyInt;

                case "System.Char":
                    return ADODB.DataTypeEnum.adChar;

                case "System.DateTime":
                    return ADODB.DataTypeEnum.adDate;

                case "System.Decimal":
                    return ADODB.DataTypeEnum.adCurrency;

                case "System.Double":
                    return ADODB.DataTypeEnum.adDouble;

                case "System.Int16":
                    return ADODB.DataTypeEnum.adSmallInt;

                case "System.Int32":
                    return ADODB.DataTypeEnum.adInteger;

                case "System.Int64":
                    return ADODB.DataTypeEnum.adBigInt;

                case "System.SByte":
                    return ADODB.DataTypeEnum.adTinyInt;

                case "System.Single":
                    return ADODB.DataTypeEnum.adSingle;

                case "System.UInt16":
                    return ADODB.DataTypeEnum.adUnsignedSmallInt;

                case "System.UInt32":
                    return ADODB.DataTypeEnum.adUnsignedInt;

                case "System.UInt64":
                    return ADODB.DataTypeEnum.adUnsignedBigInt;

                case "System.DateTimeOffset":
                    return ADODB.DataTypeEnum.adDBTimeStamp;

                case "System.String":
                default:
                    return ADODB.DataTypeEnum.adVarChar;
            }
        }

        private void btnFerdecRetrieval_Click(object sender, RibbonControlEventArgs e)
        {
            Int32 rows = 0;
            try
            {
                Ferdec.Data.FerdecRetrievalPOC d = new Data.FerdecRetrievalPOC();
                //DataTable tt = d.ExecQuery("SELECT 1 FROM client_trades_test");
                string query = @"select * from client_trades_test where to_char(tran_datetime,'YYYY.MM.DD') = '2017.08.22'";
                DataTable q = d.ExecQuery(query);
                rows = q.Rows.Count;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problems with the connectivity");
            }
            MessageBox.Show(string.Format("{0} rows were selected", rows.ToString()));
        }
    }
    static public class CommonFormats
    {
        public const string DateTime = "DateTime";
        public const string Int32 = "Int32";
        public const string String = "String";
        public const string Decimal = "Decimal";
        public const string Currency = "Currency";
    }

}
