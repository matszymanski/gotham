﻿namespace StrazzeGT.UI.Ferdec
{
    partial class FerdecReporting : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public FerdecReporting()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.grpMAR = this.Factory.CreateRibbonGroup();
            this.btnRetrieveData = this.Factory.CreateRibbonButton();
            this.tbMAR = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.btnFerdecRetrieval = this.Factory.CreateRibbonButton();
            this.btnTDPExtractor = this.Factory.CreateRibbonButton();
            this.tab1.SuspendLayout();
            this.grpMAR.SuspendLayout();
            this.tbMAR.SuspendLayout();
            this.group1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.Groups.Add(this.grpMAR);
            this.tab1.Label = "TabAddIns";
            this.tab1.Name = "tab1";
            // 
            // grpMAR
            // 
            this.grpMAR.Items.Add(this.btnRetrieveData);
            this.grpMAR.Label = "MAR";
            this.grpMAR.Name = "grpMAR";
            // 
            // btnRetrieveData
            // 
            this.btnRetrieveData.Label = "GO!";
            this.btnRetrieveData.Name = "btnRetrieveData";
            this.btnRetrieveData.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnRetrieveData_Click);
            // 
            // tbMAR
            // 
            this.tbMAR.Groups.Add(this.group1);
            this.tbMAR.Label = "MAR";
            this.tbMAR.Name = "tbMAR";
            // 
            // group1
            // 
            this.group1.Items.Add(this.btnFerdecRetrieval);
            this.group1.Items.Add(this.btnTDPExtractor);
            this.group1.Label = "Data Extractions";
            this.group1.Name = "group1";
            // 
            // btnFerdecRetrieval
            // 
            this.btnFerdecRetrieval.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnFerdecRetrieval.Image = global::StrazzeGT.UI.Ferdec.Properties.Resources.ferdec_logo_rgb;
            this.btnFerdecRetrieval.Label = "Ferdec Extractor";
            this.btnFerdecRetrieval.Name = "btnFerdecRetrieval";
            this.btnFerdecRetrieval.ShowImage = true;
            this.btnFerdecRetrieval.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnRetrieveData_Click);
            // 
            // btnTDPExtractor
            // 
            this.btnTDPExtractor.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnTDPExtractor.Image = global::StrazzeGT.UI.Ferdec.Properties.Resources.Trayport;
            this.btnTDPExtractor.Label = "TDP Extractor";
            this.btnTDPExtractor.Name = "btnTDPExtractor";
            this.btnTDPExtractor.ShowImage = true;
            this.btnTDPExtractor.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnCMDPExtractor_Click);
            // 
            // FerdecReporting
            // 
            this.Name = "FerdecReporting";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.tab1);
            this.Tabs.Add(this.tbMAR);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.FerdecReporting_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.grpMAR.ResumeLayout(false);
            this.grpMAR.PerformLayout();
            this.tbMAR.ResumeLayout(false);
            this.tbMAR.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpMAR;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnRetrieveData;
        internal Microsoft.Office.Tools.Ribbon.RibbonTab tbMAR;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnFerdecRetrieval;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnTDPExtractor;
    }

    partial class ThisRibbonCollection
    {
        internal FerdecReporting FerdecReporting
        {
            get { return this.GetRibbon<FerdecReporting>(); }
        }
    }
}
