﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrazzeGT.UI.Ferdec
{
    public class Configuration
    {
        public const string AxpoNameTdp = "Axpo Trading AG";
        public const string AxpoNameItaly = "Axpo Italia Spa";
        public static readonly bool DefaultToAggressor = true;
        public const string Timezone = "UTC";
        public static readonly string AxpoITSourceName = "TrayportITA";
        public const string TDPSourceName = "TrayportTDP";
        public const string AxpoPartyName = "Axpo";
        public const string AxpoITPartyName = "Axpo IT";

        public int RelativeDayNumberFromToday { get; set; }
        public bool TruncateTables { get; set; }
        public string AxpoITDBConnection { get; set; }
        public string TDPConnection { get; set; }

        public string FerdecDBConnection { get; set; }
        public string FerdecTradesTableName { get; set; }
        public string FerdecOrdersTableName { get; set; }

        public string TrayportCsvProcessingFolder { get; set; }
        public string TrayportFTPHost { get; set; }
        public string TrayportFTPUser { get; set; }
        public string TrayportFTPPassword { get; set; }
        public string TrayportFTPRemoteFolder { get; set; }
        public string TrayportArchiveFolder { get; set; }

        public static Configuration Build(NameValueCollection appSettings)
        {
            var result = new Configuration();
            foreach (var prop in typeof(Configuration).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance))
            {
                if (prop.PropertyType == typeof(int))
                    prop.SetValue(result, int.Parse(appSettings[prop.Name]));
                else if (prop.PropertyType == typeof(bool))
                    prop.SetValue(result, bool.Parse(appSettings[prop.Name]));
                else
                    prop.SetValue(result, appSettings[prop.Name]);
            }

            return result;
        }
    }
}
