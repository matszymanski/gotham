﻿using Oracle.ManagedDataAccess.Client;
using Strazze.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrazzeGT.UI.Ferdec.Data
{
    public class FerdecRetrievalPOC
    {
        Configuration c;

        public FerdecRetrievalPOC()
        {
            //c = new Ferdec.Configuration();
            c = Ferdec.Configuration.Build(System.Configuration.ConfigurationManager.AppSettings);
        }

        //public static void Reload(Configuration config, LocalDate date, List<Trade> trades, List<Order> orders, bool rollDate)
        //{
        //    if (config.TruncateTables)
        //        Truncate(config.FerdecDBConnection, config.FerdecTradesTableName);
        //    SendBulk<Trade>(config.FerdecDBConnection, trades, config.FerdecTradesTableName);

        //    if (config.TruncateTables)
        //        Truncate(config.FerdecDBConnection, config.FerdecOrdersTableName);
        //    SendBulk<Order>(config.FerdecDBConnection, orders, config.FerdecOrdersTableName);

        //    StartEOD(config.FerdecDBConnection, date, rollDate);
        //}


        //public static void StartEOD(string connectionString, LocalDate date, bool rollDate)
        //{
        //    if (rollDate)
        //    {
        //        ExecSql(connectionString, @"begin 
        //                ROLL_ANALYSIS_DATE(1,1);
        //                START_EOD_RUN('EOD run" + date.ToString("MM-dd-yyyy", DateTimeFormatInfo.InvariantInfo) + "', 0); end;", "Could not start EOD");
        //    }
        //    else
        //    {
        //        ExecSql(connectionString, @"begin 
        //                    START_EOD_RUN('EOD run" + date.ToString("MM-dd-yyyy", DateTimeFormatInfo.InvariantInfo) + "', 0); end;", "Could not start EOD");
        //    }
        //}

        //private static void ExecSql(string connectionString, string sql, string niceErrorMessage)
        //{
        //    using (var oracle = new OracleConnection(connectionString))
        //    {
        //        oracle.Open();
        //        var command = oracle.CreateCommand();

        //        try
        //        {
        //            OracleCommand com = oracle.CreateCommand();
        //            com.CommandText = sql;
        //            com.ExecuteNonQuery();
        //        }
        //        catch (Exception e)
        //        {
        //            //L.Error(niceErrorMessage, e);
        //        }

        //    }
        //}


        public DataTable ExecQuery(string query)
        {
            string connection = c.FerdecDBConnection;
            return ExecSqlQuery(connection, query, "");
        }

        public DataTable ExecTDPQuery(string query)
        {
            string connection = c.TDPConnection;
            return ExecSqlServerQuery(connection, query, "");
        }

        public DataTable ExecTDPQuery(string query, DateTime date)
        {
            string connection = c.TDPConnection;
            return ExecSqlServerQuery(connection, query, "", date);
        }

        //public DataTable Instruments(string tableType, DateTime date)
        //{
        //    string dt = date.ToString("yyyy.MM.dd");
        //    //string rootQuery = @"select DISTINCT(INS_TYPE) as INSTRUMENT from _TABLE_TYPE_ where to_char(tran_datetime, 'YYYY.MM.DD') = '2017.08.30' ORDER BY INSTRUMENT";
        //    string rootQuery = @"select DISTINCT(INS_TYPE) as INSTRUMENT from _TABLE_TYPE_ where to_char(tran_datetime, 'YYYY.MM.DD') = '_DATETIME_' ORDER BY INSTRUMENT";
        //    string specificTable = rootQuery.Replace("_TABLE_TYPE_", tableType).Replace("_DATETIME_", dt);

        //    return this.ExecQuery(specificTable);

        //}

        public DataTable Instruments(string tableType, DateTime date)
        {
            //string dt = date.ToString("yyyy.MM.dd");
            //string rootQuery = @"select DISTINCT(INS_TYPE) as INSTRUMENT from _TABLE_TYPE_ where to_char(tran_datetime, 'YYYY.MM.DD') = '2017.08.30' ORDER BY INSTRUMENT";
            //string rootQuery = @"select DISTINCT(INS_TYPE) as INSTRUMENT from _TABLE_TYPE_ where to_char(tran_datetime, 'YYYY.MM.DD') = '_DATETIME_' ORDER BY INSTRUMENT";
            //string specificTable = rootQuery.Replace("_TABLE_TYPE_", tableType).Replace("_DATETIME_", dt);

            //string query = "SELECT INSTRUMENT FROM INSTRUMENTS ORDER BY INSTRUMENT";
            string query = "SELECT * FROM INSTRUMENTS ORDER BY INSTRUMENT";

            return this.ExecQuery(query);

        }

        public DataTable FerdecSleevesDataRetrieval(DateTime date)
        {
            string dt = date.ToString("yyyy.MM.dd");

            //string orderQuery = @"select * from SLEEVE_TRADES where to_char(tran_datetime, 'YYYY.MM.DD') = '_DATETIME_'";
            string sleeveQuery = @"select * from SLEEVE_TRADES where to_char(TO_DATE(SUBSTR(datetime, 0,10), 'dd.mm.yyyy'), 'YYYY.MM.DD') = '_DATETIME_'";
            sleeveQuery = sleeveQuery.Replace("_DATETIME_", dt);

            return this.ExecQuery(sleeveQuery);
        }

        public DataTable FerdecOrderDataRetrieval(string instrument, DateTime date)
        {
            string dt = date.ToString("yyyy.MM.dd");
            //string rootQuery = @"select DISTINCT(INS_TYPE) as INSTRUMENT from _TABLE_TYPE_ where to_char(tran_datetime, 'YYYY.MM.DD') = '2017.08.30' ORDER BY INSTRUMENT";
            //string rootQuery = @"select DISTINCT(INS_TYPE) as INSTRUMENT from _TABLE_TYPE_ where to_char(tran_datetime, 'YYYY.MM.DD') = '_DATETIME_' ORDER BY INSTRUMENT";
            //string query = rootQuery.Replace("_TABLE_TYPE_", tableType).Replace("_DATETIME_", dt);



            string orderQuery = @"select TRAN_ID, TRAN_STATUS, COUNTRY, COMMODITY, TRAN_INS_TYPE, INS_TYPE, TRADER, MARKET_PLACE, ORIG_TRAN_ID, BID_ASK, TRAN_DATETIME, VOLUME, UNIT, PRICE, CURRENCY, PARTY, SUBPARTY, COUNTERPARTY, BROKER, TIMEZONE, SIDE, PREMIUM, SPREAD from orders where to_char(tran_datetime, 'YYYY.MM.DD') = '_DATETIME_' and ins_type = '_INSTRUMENT_'";
            orderQuery = orderQuery.Replace("_DATETIME_", dt).Replace("_INSTRUMENT_", instrument);

            return this.ExecQuery(orderQuery);
        }


        public DataTable FerdecTradeDataRetrieval(string instrument, DateTime date)
        {
            string dt = date.ToString("yyyy.MM.dd");

            //string tradeQuery = @"select TRAN_ID, TRAN_STATUS, COUNTRY, COMMODITY, TRAN_INS_TYPE, INS_TYPE, TRADER, MARKET_PLACE, ORIG_TRAN_ID, BUY_SELL, TRAN_DATETIME, VOLUME, UNIT, PRICE, CURRENCY, PARTY, SUBPARTY, COUNTERPARTY, BROKER, TIMEZONE, SIDE, PREMIUM, SPREAD from trades where to_char(tran_datetime, 'YYYY.MM.DD') = '_DATETIME_' and ins_type = '_INSTRUMENT_' and counterparty ='AXPO'";
            string tradeQuery = @"select TRAN_ID, TRAN_STATUS, COUNTRY, COMMODITY, TRAN_INS_TYPE, INS_TYPE, TRADER, MARKET_PLACE, ORIG_TRAN_ID, BUY_SELL, TRAN_DATETIME, VOLUME, UNIT, PRICE, CURRENCY, PARTY, SUBPARTY, COUNTERPARTY, BROKER, TIMEZONE, SIDE, PREMIUM, SPREAD from trades where to_char(tran_datetime, 'YYYY.MM.DD') = '_DATETIME_' and ins_type = '_INSTRUMENT_'";

            tradeQuery = tradeQuery.Replace("_DATETIME_", dt).Replace("_INSTRUMENT_", instrument);
            return this.ExecQuery(tradeQuery);
        }


        //////////public DataTable FerdecTradeDataRetrieval(string instrument, DateTime date)
        //////////{
        //////////    string dt = date.ToString("yyyy.MM.dd");
        //////////    ////////////string rootQuery = @"select DISTINCT(INS_TYPE) as INSTRUMENT from _TABLE_TYPE_ where to_char(tran_datetime, 'YYYY.MM.DD') = '2017.08.30' ORDER BY INSTRUMENT";
        //////////    //////////string rootQuery = @"select DISTINCT(INS_TYPE) as INSTRUMENT from _TABLE_TYPE_ where to_char(tran_datetime, 'YYYY.MM.DD') = '_DATETIME_' ORDER BY INSTRUMENT";
        //////////    //////////string query = rootQuery.Replace("_TABLE_TYPE_", tableType).Replace("_DATETIME_", dt);
        //////////    //////////return this.ExecQuery(query);

        //////////    string tradeQuery = @"select TRAN_ID, TRAN_STATUS, COUNTRY, COMMODITY, TRAN_INS_TYPE, INS_TYPE, TRADER, MARKET_PLACE, ORIG_TRAN_ID, BUY_SELL, TRAN_DATETIME, VOLUME, 
        //////////                          UNIT, PRICE, CURRENCY, PARTY, SUBPARTY, COUNTERPARTY, BROKER, TIMEZONE, SIDE, PREMIUM, SPREAD 
        //////////                          from trades 
        //////////                          where to_char(tran_datetime, 'YYYY.MM.DD') = '_DATETIME_'
        //////////                          and ins_type = '_INSTRUMENT_' 
        //////////                          and counterparty ='AXPO'";

        //////////    tradeQuery = tradeQuery.Replace("_DATETIME_", dt).Replace("_INSTRUMENT_", instrument);
        //////////    return this.ExecQuery(tradeQuery);
        //////////}





        private static DataTable ExecSqlQuery(string connectionString, string sql, string niceErrorMessage)
        {
            DataTable dt = new DataTable();

            using (var oracle = new OracleConnection(connectionString))
            {
                oracle.Open();
                var command = oracle.CreateCommand();

                try
                {
                    OracleCommand com = oracle.CreateCommand();
                    com.CommandText = sql;

                    OracleDataAdapter da = new OracleDataAdapter(com);

                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    dt = ds.Tables[0];

                    //com.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    //L.Error(niceErrorMessage, e);
                }

            }

            return dt;
        }

        private static DataTable ExecSqlServerQuery(string connectionString, string sql, string niceErrorMessage)
        {
            DataTable dt = new DataTable();

            DataManager.GetDM().ConnectionString = connectionString;
            SqlCommand cmd = DataManager.GetDM().GetCommandT(sql);
            cmd.CommandTimeout = 300;
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        private static DataTable ExecSqlServerQuery(string connectionString, string sql, string niceErrorMessage, DateTime date)
        {
            DataTable dt = new DataTable();

            DataManager.GetDM().ConnectionString = connectionString;
            SqlCommand cmd = DataManager.GetDM().GetCommandT(sql);
            cmd.CommandTimeout = 300;
            cmd.Parameters.AddWithValue("@ExtractionDate", date.Date);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

    }
}
