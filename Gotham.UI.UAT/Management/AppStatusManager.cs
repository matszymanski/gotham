﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.UI.UAT.Management
{
    public sealed class AppStatusManager
    {
        static string Status;
        private static readonly AppStatusManager _instance = new AppStatusManager();

        private AppStatusManager()
        {
            Status = string.Empty;
        }

        public static AppStatusManager GetInstance()
        {
            return _instance;
        }

        public static string GetStatus()
        {
            return Status;
        }

        public static void SetStatus(string newStatus)
        {
            Status = newStatus;
        }
    }
}
