﻿namespace Gotham.UI.UAT
{
    partial class ETRM_Light : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public ETRM_Light()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.grpCPTY = this.Factory.CreateRibbonGroup();
            this.btnLE = this.Factory.CreateRibbonButton();
            this.grpActions = this.Factory.CreateRibbonGroup();
            this.btnEdit = this.Factory.CreateRibbonButton();
            this.btnNew = this.Factory.CreateRibbonButton();
            this.tab1.SuspendLayout();
            this.grpCPTY.SuspendLayout();
            this.grpActions.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.Groups.Add(this.group1);
            this.tab1.Label = "TabAddIns";
            this.tab1.Name = "tab1";
            // 
            // group1
            // 
            this.group1.Label = "group1";
            this.group1.Name = "group1";
            // 
            // 
            // grpCPTY
            // 
            this.grpCPTY.Items.Add(this.btnLE);
            this.grpCPTY.Label = "Counterparty";
            this.grpCPTY.Name = "grpCPTY";
            // 
            // btnLE
            // 
            this.btnLE.Label = "LE";
            this.btnLE.Name = "btnLE";
            this.btnLE.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnLE_Click);
            // 
            // grpActions
            // 
            this.grpActions.Items.Add(this.btnEdit);
            this.grpActions.Items.Add(this.btnNew);
            this.grpActions.Label = "Actions";
            this.grpActions.Name = "grpActions";
            // 
            // btnEdit
            // 
            this.btnEdit.Label = "Edit";
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnEdit_Click);
            // 
            // btnNew
            // 
            this.btnNew.Label = "New";
            this.btnNew.Name = "btnNew";
            // 
            // ETRM_Light
            // 
            this.Name = "ETRM_Light";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.ETRM_Light_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.grpCPTY.ResumeLayout(false);
            this.grpCPTY.PerformLayout();
            this.grpActions.ResumeLayout(false);
            this.grpActions.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpCPTY;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnLE;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpActions;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnEdit;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnNew;
    }

    partial class ThisRibbonCollection
    {
        internal ETRM_Light ETRM_Light
        {
            get { return this.GetRibbon<ETRM_Light>(); }
        }
    }
}
