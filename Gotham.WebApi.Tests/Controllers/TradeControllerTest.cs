﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Gotham.WebApi;
using Gotham.WebApi.Controllers;
using System.Web.Http;
using Gotham.Domain.Confirmation;
using System.Web.Http.Results;
using Newtonsoft.Json.Linq;

namespace Gotham.WebApi.Tests.Controllers
{
    [TestClass]
    public class TradeControllerTest
    {
        [TestMethod]
        public void ById()
        {
            ConfirmationViewController controller = new ConfirmationViewController();
            var tradeKey = new TradeUniqueKey { TradeID = "2014", InternalPartyID = "1" };
            IHttpActionResult result = controller.GetById(tradeKey) as IHttpActionResult;
            var conNegResult = result as OkNegotiatedContentResult<JToken>;
            
            
            Assert.AreEqual(tradeKey.TradeID, conNegResult.Content.First["TradeID"]);
            
        }
    }
}
