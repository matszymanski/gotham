﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gotham.WebApi.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json.Linq;

namespace Gotham.WebApi.Tests.Controllers
{
    [TestClass]
    public class ConfigurationControllerTests
    {

        [TestMethod]
        public void PriceIndexUnaligned()
        {
            ConfigurationController controller = new ConfigurationController();
            var count = 2;

            IHttpActionResult result = controller.GetPriceIndexesUnaligned(count) as IHttpActionResult;
            var conNegResult = result as OkNegotiatedContentResult<IList<string>>;


            Assert.IsTrue(conNegResult.Content.Count() <= count);

        }
    }
}
