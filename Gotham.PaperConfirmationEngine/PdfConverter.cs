﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.PaperConfirmationEngine
{
    public class PdfConverter
    {

        public static byte[] AsPdf(byte[] docxDocument)
        {
            byte[] file = new byte[0];
            try
            {
                var converter = new Syncfusion.DocToPDFConverter.DocToPDFConverter();
                var docx = new MemoryStream(docxDocument);
                
                var mem = new MemoryStream();
                converter.ConvertToPDF(docx).Save(mem);
                file = mem.ToArray();
            }
            catch (Exception e)
            {
                throw;
            }
            return file;
        }
    }
}
