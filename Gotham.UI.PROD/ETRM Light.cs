﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using Gotham.Domain;
using Excel = Microsoft.Office.Interop.Excel;
using Gotham.Data;
using System.Data;
using System.Windows.Forms;
using Gotham.UI.PROD.Management;
using Gotham.AddIn.Party;

namespace Gotham.UI.PROD
{
    public partial class ETRM_Light
    {
        public string AppMode { get; set; }
        private bool CanEditStatic { get; set; }
        private void ETRM_Light_Load(object sender, RibbonUIEventArgs e)
        {
            this.CanEditStatic = true;
        }
        private void EnableEditMode(bool isEnabled)
        {
            //this.btnEdit.Enabled = isEnabled;
            //this.btnNew.Enabled = isEnabled;
        }

        private Excel.Worksheet ManageWorksheet(RibbonControlEventArgs e)
        {
            Excel.Window window = (Excel.Window)e.Control.Context;

            if (this.AppMode == string.Empty)
            {
                return ((Excel.Worksheet)window.Application.ActiveSheet);
            }

            Excel.Workbook currentWorkbook = Globals.ThisAddIn.Application.ActiveWorkbook;
            Excel.Sheets sheets = currentWorkbook.Worksheets;

            foreach (Excel.Worksheet sheet in sheets)
            {
                if (sheet.Name == this.AppMode)
                {
                    return sheet;
                }
            }

            Excel.Worksheet newSheet = (Excel.Worksheet)currentWorkbook.Worksheets.Add(System.Reflection.Missing.Value, currentWorkbook.Worksheets[currentWorkbook.Worksheets.Count]);
            newSheet.Name = this.AppMode;
            return newSheet;
        }

        private void PerformanceFeatures(RibbonControlEventArgs e, bool isOff)
        {
            Excel.Window window = (Excel.Window)e.Control.Context;
            Excel.Worksheet sheet = ((Excel.Worksheet)window.Application.ActiveSheet);
            window.Application.ScreenUpdating = isOff;
        }

        private void PasteRecordSet(ADODB.Recordset pdRS, RibbonControlEventArgs e)
        {
            if (pdRS != null)
            {
                try
                {
                    pdRS.MoveFirst();
                    Int32 rows = pdRS.RecordCount;
                    Int32 cols = pdRS.Fields.Count;
                    this.CreateHeader(e, pdRS);
                    Excel.Range topLeft = this.GetTopLeft(e, rows, cols);
                    topLeft.CopyFromRecordset(pdRS);
                    topLeft.EntireColumn.AutoFit();
                }
                catch { }
            }
        }

        void CreateHeader(RibbonControlEventArgs e, ADODB.Recordset pdRS)
        {
            Excel.Window window = (Excel.Window)e.Control.Context;
            Excel.Worksheet sheet = ((Excel.Worksheet)window.Application.ActiveSheet);
            Excel.Range tl = (Excel.Range)sheet.Cells[1, 1];

            for (Int32 i = 0; i < pdRS.Fields.Count; i++)
            {
                Excel.Range header = (Excel.Range)sheet.Cells[1, i + 1];
                header.Value2 = pdRS.Fields[i].Name;
            }
        }

        Excel.Range GetTopLeft(RibbonControlEventArgs e, Int32 rows, Int32 cols)
        {
            Excel.Window window = (Excel.Window)e.Control.Context;
            Excel.Worksheet sheet = ((Excel.Worksheet)window.Application.ActiveSheet);
            Excel.Range tl = (Excel.Range)sheet.Cells[2, 1];
            Excel.Range br = (Excel.Range)sheet.Cells[rows + 1, cols];
            Excel.Range topLeftExtended = sheet.get_Range(tl, br);
            return topLeftExtended;
        }

        Excel.Range GetTopLeft(Excel.Worksheet sheet, Int32 rows, Int32 cols)
        {
            Excel.Range tl = (Excel.Range)sheet.Cells[2, 1];
            Excel.Range br = (Excel.Range)sheet.Cells[rows + 1, cols];
            Excel.Range topLeftExtended = sheet.get_Range(tl, br);
            return topLeftExtended;
        }

        private void FormatColumns(Int32 size, RibbonControlEventArgs e)
        {
            Excel.Window window = (Excel.Window)e.Control.Context;
            Excel.Worksheet activeWorksheet = ((Excel.Worksheet)window.Application.ActiveSheet);

            for (Int32 i = 1; i < size + 1; i++)
            {
                Excel.Range topCell = (Excel.Range)activeWorksheet.Cells[1, i];
                topCell.EntireColumn.NumberFormat = "@";
            }
        }

        private void FormatColumns(DataTable dt, RibbonControlEventArgs e)
        {
            Excel.Window window = (Excel.Window)e.Control.Context;
            Excel.Worksheet activeWorksheet = ((Excel.Worksheet)window.Application.ActiveSheet);

            if (!ETRMDataAccess.IsEmpty(dt))
            {
                for (Int32 i = 1; i < dt.Columns.Count + 1; i++)
                {
                    Excel.Range topCell = (Excel.Range)activeWorksheet.Cells[1, i];
                    topCell.EntireColumn.NumberFormat = this.AssignFormatType(dt.Columns[i - 1].DataType.Name);
                }
            }
        }

        private string AssignFormatType(string columnTypeName)
        {
            string formatSpec = "@";

            switch (columnTypeName)
            {
                case CommonFormats.DateTime:
                    formatSpec = "dd MMM yyyy";
                    break;
                case CommonFormats.Decimal:
                    formatSpec = "#,###.0000";
                    //formatSpec = "#'##0.00_ ;[Red]-#'##0.00";
                    break;
                case CommonFormats.Int32:
                    formatSpec = "#,###";
                    break;
                case CommonFormats.Currency:
                    formatSpec = "#,##0.00_ ;[Red]-#,##0.00";
                    break;
                case CommonFormats.String:
                    break;
            }

            return formatSpec;
        }

        private void FormatListing(RibbonControlEventArgs e)
        {
            Excel.Window window = (Excel.Window)e.Control.Context;
            window.Zoom = 80;
            Excel.Worksheet activeWorksheet = ((Excel.Worksheet)window.Application.ActiveSheet);
            activeWorksheet.Columns.AutoFit();
        }

        public void RefreshMainScreen(RibbonControlEventArgs e)
        {
            Excel.Window window = (Excel.Window)e.Control.Context;

            Excel.Worksheet destinationSheet = this.ManageWorksheet(e);
            destinationSheet.Activate();

            Excel.Worksheet activeWorksheet = ((Excel.Worksheet)window.Application.ActiveSheet);
            activeWorksheet.Cells.Clear();

            try
            {
                this.PerformanceFeatures(e, false);

                if (this.AppMode == ApplicationMode.LegalEntities)
                {
                    ADODB.Recordset pdRS = ETRMDataAccess.GetLegalEntities().Records;
                    this.PasteRecordSet(pdRS, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.BusinessUnits)
                {
                    ADODB.Recordset pdRS = ETRMDataAccess.GetBusinessUnits();
                    this.PasteRecordSet(pdRS, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.PowerFutures)
                {
                    DoubleDecker dd = ETRMDataAccess.GetFuturesListingDDSP(TradingProduct.Power);
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.GasFutures)
                {
                    DoubleDecker dd = ETRMDataAccess.GetFuturesListingDDSP(TradingProduct.Gas);
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.Portfolios)
                {
                    DoubleDecker dd = ETRMDataAccess.GetPortfolioListing();
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.CurveDef)
                {
                    DoubleDecker dd = ETRMDataAccess.GetCurveListing();
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.PricingRef)
                {
                    DoubleDecker dd = ETRMDataAccess.GetPricingReferenceListing();
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.OTCPriceReference)
                {
                    DoubleDecker dd = ETRMDataAccess.GetOTCPricingReferenceListing();
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.PowerLocations)
                {
                    DoubleDecker dd = ETRMDataAccess.GetPowerLocations();
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.GasLocations)
                {
                    DoubleDecker dd = ETRMDataAccess.GetGasLocations();
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.SwapSpread)
                {
                    DoubleDecker dd = ETRMDataAccess.GetSwapSpread();
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.ExchangeFees)
                {
                    DoubleDecker dd = ETRMDataAccess.GetExchangeClearingRateListing();
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.ExchangeProd)
                {
                    DoubleDecker dd = ETRMDataAccess.GetExchangeProducts();
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.ExchangeContract)
                {
                    DoubleDecker dd = ETRMDataAccess.GetExchangeContractsTemplate();
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.CashTrades)
                {
                    DoubleDecker dd = ETRMDataAccess.GetCashTrades();
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.PowerPhysical)
                {
                    DoubleDecker dd = ETRMDataAccess.GetPowerPhysicalListing();
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.PowerSwaps)
                {
                    DoubleDecker dd = ETRMDataAccess.GetSwapsListing(Underlying.Power);
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.GasSwaps)
                {
                    DoubleDecker dd = ETRMDataAccess.GetSwapsListing(Underlying.Gas);
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.Options)
                {
                    DoubleDecker dd = ETRMDataAccess.GetOptionsListing();
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.ShellTrades)
                {
                    //DoubleDecker dd = ETRMDataAccess.GetOptionsListing();
                    //this.PasteRecordSet(dd.Records, e);
                    //this.FormatColumns(dd.Table, e);
                    //this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.BeaStraHospital)
                {
                    DoubleDecker dd = ETRMDataAccess.GetNonProcessedDeals();
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.ConfirmationLight)
                {
                    DoubleDecker dd = ETRMDataAccess.GetConfLightConfirmableTrades();
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.ConfirmationLightOverview)
                {
                    DoubleDecker dd = ETRMDataAccess.GetConfLightOverview();
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.SchedulingLight)
                {
                    DoubleDecker dd = ETRMDataAccess.GetSchedulableTrades();
                    this.PasteRecordSet(dd.Records, e);
                    this.FormatColumns(dd.Table, e);
                    this.FormatListing(e);
                }
                else if (this.AppMode == ApplicationMode.Confirmations)
                {
                    //RefreshConfirmations(window);
                }
                else if (this.AppMode == ApplicationMode.Scheduling)
                {
                    //RefreshScheduling(window);
                }
                //else if (this.AppMode == ApplicationMode.NordicStaticData)
                //{
                //    DoubleDecker dd = ETRMDataAccess.GetNordicStaticData();
                //    this.PasteRecordSet(dd.Records, e);
                //    this.FormatColumns(dd.Table, e);
                //    this.FormatListing(e);
                //}
                //else if (this.AppMode == ApplicationMode.NordicSignatures)
                //{
                //    DoubleDecker dd = ETRMDataAccess.GetNordicConfirmationOfficers();
                //    this.PasteRecordSet(dd.Records, e);
                //    this.FormatColumns(dd.Table, e);
                //    this.FormatListing(e);
                //}
                //else if (this.AppMode == ApplicationMode.NordicPriceReference)
                //{
                //    DoubleDecker dd = ETRMDataAccess.GetNordicPriceReference();
                //    this.PasteRecordSet(dd.Records, e);
                //    this.FormatColumns(dd.Table, e);
                //    this.FormatListing(e);
                //}
                //else if (this.AppMode == ApplicationMode.NordicConfirmations)
                //{
                //    DoubleDecker dd = ETRMDataAccess.GetNordicConfirmationListing();
                //    this.PasteRecordSet(dd.Records, e);
                //    this.FormatColumns(dd.Table, e);
                //    this.FormatListing(e);
                //}
                else
                {
                    MessageBox.Show("Not yet implemented...");
                }
            }
            finally
            {
                this.PerformanceFeatures(e, true);
            }
        }

        private void btnLE_Click(object sender, RibbonControlEventArgs e)
        {
            this.AppMode = Constants.LegalEntities;
            this.EnableEditMode(true);
            this.RefreshMainScreen(e);
        }

        private void SetApplicationMode(RibbonControlEventArgs e)
        {
            Excel.Window window = (Excel.Window)e.Control.Context;
            Excel.Range rng = window.Application.ActiveCell;
            Excel.Worksheet sheet = ((Excel.Worksheet)window.Application.ActiveSheet);
            Excel.Range target = (Excel.Range)sheet.Cells[rng.Row, 1];

            switch (sheet.Name)
            {
                case ApplicationMode.BusinessUnits:
                    this.AppMode = ApplicationMode.BusinessUnits;
                    AppStatusManager.SetStatus(ApplicationMode.BusinessUnits);
                    break;
                case ApplicationMode.CurveDef:
                    this.AppMode = ApplicationMode.CurveDef;
                    AppStatusManager.SetStatus(ApplicationMode.CurveDef);
                    break;
                case ApplicationMode.PricingRef:
                    this.AppMode = ApplicationMode.PricingRef;
                    AppStatusManager.SetStatus(ApplicationMode.PricingRef);
                    break;
                case ApplicationMode.OTCPriceReference:
                    this.AppMode = ApplicationMode.OTCPriceReference;
                    AppStatusManager.SetStatus(ApplicationMode.OTCPriceReference);
                    break;
                case ApplicationMode.PowerLocations:
                    this.AppMode = ApplicationMode.PowerLocations;
                    AppStatusManager.SetStatus(ApplicationMode.PowerLocations);
                    break;
                case ApplicationMode.GasLocations:
                    this.AppMode = ApplicationMode.GasLocations;
                    AppStatusManager.SetStatus(ApplicationMode.GasLocations);
                    break;
                case ApplicationMode.SwapSpread:
                    this.AppMode = ApplicationMode.SwapSpread;
                    AppStatusManager.SetStatus(ApplicationMode.SwapSpread);
                    break;
                case ApplicationMode.ExchangeFees:
                    this.AppMode = ApplicationMode.ExchangeFees;
                    AppStatusManager.SetStatus(ApplicationMode.ExchangeFees);
                    break;
                case ApplicationMode.LegalEntities:
                    this.AppMode = ApplicationMode.LegalEntities;
                    AppStatusManager.SetStatus(ApplicationMode.LegalEntities);
                    break;
                case ApplicationMode.PowerFutures:
                    this.AppMode = ApplicationMode.PowerFutures;
                    AppStatusManager.SetStatus(ApplicationMode.PowerFutures);
                    break;
                case ApplicationMode.PowerPhysical:
                    this.AppMode = ApplicationMode.PowerPhysical;
                    AppStatusManager.SetStatus(ApplicationMode.PowerPhysical);
                    break;
                case ApplicationMode.PowerSwaps:
                    this.AppMode = ApplicationMode.PowerSwaps;
                    AppStatusManager.SetStatus(ApplicationMode.PowerSwaps);
                    break;
                case ApplicationMode.GasSwaps:
                    this.AppMode = ApplicationMode.GasSwaps;
                    AppStatusManager.SetStatus(ApplicationMode.GasSwaps);
                    break;
                case ApplicationMode.GasFutures:
                    this.AppMode = ApplicationMode.GasFutures;
                    AppStatusManager.SetStatus(ApplicationMode.GasFutures);
                    break;
                case ApplicationMode.Options:
                    this.AppMode = ApplicationMode.Options;
                    AppStatusManager.SetStatus(ApplicationMode.Options);
                    break;
                case ApplicationMode.ShellTrades:
                    this.AppMode = ApplicationMode.ShellTrades;
                    AppStatusManager.SetStatus(ApplicationMode.ShellTrades);
                    break;
                case ApplicationMode.Portfolios:
                    this.AppMode = ApplicationMode.Portfolios;
                    AppStatusManager.SetStatus(ApplicationMode.Portfolios);
                    break;
                case ApplicationMode.ExchangeProd:
                    this.AppMode = ApplicationMode.ExchangeProd;
                    AppStatusManager.SetStatus(ApplicationMode.ExchangeProd);
                    break;
                case ApplicationMode.ExchangeContract:
                    this.AppMode = ApplicationMode.ExchangeContract;
                    AppStatusManager.SetStatus(ApplicationMode.ExchangeContract);
                    break;
                case ApplicationMode.ExRecReports:
                    this.AppMode = ApplicationMode.ExRecReports;
                    AppStatusManager.SetStatus(ApplicationMode.ExRecReports);
                    break;
                case ApplicationMode.CashTrades:
                    this.AppMode = ApplicationMode.CashTrades;
                    AppStatusManager.SetStatus(ApplicationMode.CashTrades);
                    break;
                case ApplicationMode.BeaStraHospital:
                    this.AppMode = ApplicationMode.BeaStraHospital;
                    AppStatusManager.SetStatus(ApplicationMode.BeaStraHospital);
                    break;
                case ApplicationMode.Invoice:
                    this.AppMode = ApplicationMode.Invoice;
                    AppStatusManager.SetStatus(ApplicationMode.Invoice);
                    break;
                case ApplicationMode.ConfirmationLight:
                    this.AppMode = ApplicationMode.ConfirmationLight;
                    AppStatusManager.SetStatus(ApplicationMode.ConfirmationLight);
                    break;
                case ApplicationMode.ConfirmationLightOverview:
                    this.AppMode = ApplicationMode.ConfirmationLightOverview;
                    AppStatusManager.SetStatus(ApplicationMode.ConfirmationLightOverview);
                    break;
                case ApplicationMode.SchedulingLight:
                    this.AppMode = ApplicationMode.SchedulingLight;
                    AppStatusManager.SetStatus(ApplicationMode.SchedulingLight);
                    break;
                //case ApplicationMode.NordicConfirmations:
                //    this.AppMode = ApplicationMode.NordicConfirmations;
                //    AppStatusManager.SetStatus(ApplicationMode.NordicConfirmations);
                //    break;
                //case ApplicationMode.NordicStaticData:
                //    this.AppMode = ApplicationMode.NordicStaticData;
                //    AppStatusManager.SetStatus(ApplicationMode.NordicStaticData);
                //    break;
                //case ApplicationMode.NordicSignatures:
                //    this.AppMode = ApplicationMode.NordicSignatures;
                //    AppStatusManager.SetStatus(ApplicationMode.NordicSignatures);
                //    break;
                //case ApplicationMode.NordicPriceReference:
                //    this.AppMode = ApplicationMode.NordicPriceReference;
                //    AppStatusManager.SetStatus(ApplicationMode.NordicPriceReference);
                //    break;
                default:
                    this.AppMode = string.Empty;
                    AppStatusManager.SetStatus(string.Empty);
                    break;
            }
        }
        private void btnEdit_Click(object sender, RibbonControlEventArgs e)
        {
            Excel.Window window = (Excel.Window)e.Control.Context;
            Excel.Range rng = window.Application.ActiveCell;
            Excel.Worksheet sheet = ((Excel.Worksheet)window.Application.ActiveSheet);
            Excel.Range target = (Excel.Range)sheet.Cells[rng.Row, 1];

            this.SetApplicationMode(e);

            if (this.AppMode == string.Empty)
            {
                MessageBox.Show("Please enter an application mode first...");
                return;
            }

            int id;
            bool isId = Int32.TryParse(Convert.ToString(target.Value2), out id);

            if (isId)
            {
                if (this.AppMode == string.Empty)
                {
                    MessageBox.Show("Please enter an application mode first...");
                    return;
                }
                else if (this.AppMode == ApplicationMode.LegalEntities)
                {
                    DataTable partyDetails = ETRMDataAccess.GetParty(id);
                    AddIn.Party.Party party = new AddIn.Party.Party(id, partyDetails, this.CanEditStatic);
                    party.ShowDialog();
                }
            }

            this.RefreshMainScreen(e);

        }
    }
}
