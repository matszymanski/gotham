﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Command
{
    class Program
    {
        static void Main(string[] args)
        {
            var api = new RestClient("http://web-confirm.com/api/v1");

            InsertExampleCorrectDeals(api);
            InsertExampleIncorrectDeals(api);
        }

        private static void InsertExampleIncorrectDeals(RestClient api)
        {
            var requests = BuildRequest("Resources/WTIexampleInvalid.json", "WTIFloatingPrice");
            requests.AddRange(BuildRequest("Resources/ZBTexampleInvalid.json", "ZBTFixedPrice"));
            foreach (var req in requests)
                api.Execute(req, Method.POST);
        }

        private static void InsertExampleCorrectDeals(RestClient api)
        {
            var requests = BuildRequest("Resources/WTIexample.json", "WTIFloatingPrice");
            requests.AddRange(BuildRequest("Resources/ZBTexample.json", "ZBTFixedPrice"));
            foreach (var req in requests)
                api.Execute(req, Method.POST);
        }

        private static List<RestRequest> BuildRequest(string filePath, string controller)
        {
            var requests = new List<RestRequest>();
            using (var file = new StreamReader(filePath))
            {
                var content = file.ReadToEnd();
                var jsonArray = JArray.Parse(content);
                foreach(var json in jsonArray.Children<JObject>())
                {
                    var request = new RestRequest(Method.POST);
                    request.Resource = "message/" + controller + "/{routeId}/{workflowId}/{templateId}";

                    request.AddUrlSegment("routeId", 1);
                    request.AddUrlSegment("workflowId", 1);
                    request.AddUrlSegment("templateId", 1);
                    foreach(var elem in json.Properties())
                    {
                        request.AddQueryParameter(elem.Name, elem.Value.ToString());
                    }
                    requests.Add(request);
                }
            }
            return requests;
        }

    }
}
