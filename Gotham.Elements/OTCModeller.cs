﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Gotham.Elements
{
    class OTCModeller
    {
    }
    public class StandardGasSwap
    {
        public List<party> party { get; set; }
        public tradeHeader tradeHeader { get; set; }
        public CommoditySwap commoditySwap { get; set; }
    }

    public class ETRM
    {
        public string systemName { get; set; }
        public string systemLocale { get; set; }
        public string internalCounterparty { get; set; }
    }

    public class PricingModelInfo
    {
        public string useModel { get; set; }
    }

    public class UserParameters
    {
        public up source { get; set; }
    }

    public class up
    {
        public string system { get; set; }
        public string sourceId { get; set; }
    }

    public class tradeHeader
    {
        public List<partyTradeIdentifier> partyTradeIdentifier { get; set; }
        public Date tradeDate { get; set; }
        public TradeExecutionDateTime tradeExecutionDateTime { get; set; }

        public tradeHeader()
        {
            this.partyTradeIdentifier = new List<partyTradeIdentifier>();
            this.tradeExecutionDateTime = new TradeExecutionDateTime();
        }
        public tradeHeader(Date date)
        {
            this.partyTradeIdentifier = new List<partyTradeIdentifier>();
            this.tradeExecutionDateTime = new TradeExecutionDateTime();
            this.tradeDate = date;
        }
    }

    public class TradeExecutionDateTime
    {
        public DateTime executionDateTime { get; set; }
        public string executionTimeZone { get; set; }
    }
    public class partyTradeIdentifier
    {
        public string tradeId { get; set; }
        public string tradeIdContext { get; set; }
        public string partyReference { get; set; }
    }

    public class TradeID
    {
        public string id { get; set; }
        public string idType { get; set; }
    }

    public class party
    {
        public string partyName { get; set; }
        public PartyID partyId { get; set; }
        public string partyRole { get; set; }
        public partyAccount partyAccount { get; set; }

        public party()
        {
            this.partyId = new PartyID();
            this.partyAccount = new partyAccount();
        }
    }

    public class PartyID
    {
        public string id { get; set; }
        public string idSource { get; set; }

    }

    public class partyAccount
    {
        public string partyAccountId { get; set; }
        public string partyAccountType { get; set; }
    }

    public class fixedLeg
    {
        public string payerPartyReference { get; set; }
        public string receiverPartyReference { get; set; }
        public string calculationPeriodsScheduleReference { get; set; }
        public Price fixedPrice { get; set; }
        public Quantity notionalQuantity { get; set; }
        public double totalNotionalQuantity { get; set; }
        public PaymentDates paymentDates { get; set; }
        public fixedLeg() {  }
        public fixedLeg(double fixedPrice, string fixedPriceCCY, string fixedPriceUnit)
        {
            this.fixedPrice = new Price(fixedPrice, fixedPriceCCY, fixedPriceUnit);
        }

    }

    public class fixedLegElCert
    {
        public string payerPartyReference { get; set; }
        public string receiverPartyReference { get; set; }
        public string calculationPeriodsScheduleReference { get; set; }
        public Price fixedPrice { get; set; }
        public Quantity notionalQuantity { get; set; }
        public double totalNotionalQuantity { get; set; }
        public PaymentDates paymentDates { get; set; }
        public TotalPrice totalPrice { get; set; }
    }


    public class TotalPrice
    {
        public string currency { get; set; }
        public double amount { get; set; }  //in the case of elcerts, nr of allowances * fixedPrice
    }
    public class PaymentDatesPTD
    {
        public RelativePaymentDatesPTR relativePaymentDates { get; set; }
    }
    public class PaymentDates
    {
        public RelativePaymentDates relativePaymentDates { get; set; }
    }

    public class floatingLeg
    {
        public string payerPartyReference { get; set; }
        public string receiverPartyReference { get; set; }
        public CalculationPeriodsSchedule calculationPeriodSchedule { get; set; }
        public Commodity commodity { get; set; }
        public Quantity notionalQuantity { get; set; }
        public double totalNotionalQuantity { get; set; }
        public Calculation calculation { get; set; }
        public PaymentDates paymentDates { get; set; }

    }

    public class environmentalPhysicalLeg
    {
        public string payerPartyReference { get; set; }
        public string receiverPartyReference { get; set; }
        public Allowances numberOfAllowances { get; set; }
        public Environmental environmental { get; set; }
        public DeliveryDates deliveryDates { get; set; }
        public BusinessCenters businessCenter { get; set; }
    }

    public class DeliveryDates
    {
        public DateTime deliveryDate { get; set; }
    }
    public class Environmental
    {
        public string productType { get; set; }
        public CompliancePeriod compliancePeriod { get; set; }
    }

    public class CompliancePeriod
    {
        public string startYear { get; set; }
        public string endYear { get; set; }
    }
    public class Allowances
    {
        public string name { get; set; }
        public string quantityUnit { get; set; }
        public double quantity { get; set; }
    }

    public class Calculation
    {
        public PricingDates pricingDates { get; set; }
    }

    public class PricingDates
    {
        public string calculationPeriodsScheduleReference { get; set; }
        public string dayType { get; set; }
        public string dayDistribution { get; set; }
        public string businessCalendar { get; set; }
    }

    public class CommodityRef
    {
        public string Instrument { get; set; }
        public string SpecifiedPrice { get; set; }
    }

    public class Commodity
    {
        public string instrumentId { get; set; }
        public string specifiedPrice { get; set; }
        public double indexPercentage { get; set; }
        public string deliveryDates { get; set; }
    }

    public class CalculationPeriodsSchedule
    {
        public string id { get; set; }
        public Int32 periodMultiplier { get; set; }
        public string period { get; set; }
        public bool balanceOfFirstPeriod { get; set; }
    }

    //////public class PricingDates
    //////{
    //////    public string CalculationPeriodsScheduleReference { get; set; }
    //////}

    //////public class PaymentDates
    //////{
    //////    public string payRelativeTo { get; set; }
    //////    public PaymentDaysOffset paymentDaysOffset { get; set; }
    //////    public string businessCenters { get; set; }
    //////    public bool masterAgreementPaymentDates { get; set; }
    //////}

    public class RelativePaymentDates
    {
        //public string id { get; set; }
        public string payRelativeTo { get; set; }
        public string calculationPeriodsScheduleReference { get; set; }
        public PaymentDaysOffset paymentDaysOffset { get; set; }
        public BusinessCenters businessCenter { get; set; }
        public bool? masterAgreementPaymentDates { get; set; }
    }

    public class RelativePaymentDatesPTR
    {
        public string payRelativeTo { get; set; }
        public string calculationPeriodsScheduleReference { get; set; }
    }

    public class BusinessCenters
    {
        public string businessCenter { get; set; }
    }

    public class PaymentDaysOffset
    {
        public Int32 periodMultipier { get; set; }
        public string period { get; set; }
        public string dayType { get; set; }
        public string businessDayConvention { get; set; }
    }

    public class CommoditySwap
    {
        public Product product { get; set; }
        public CommoditySwapDetails commoditySwapDetails { get; set; }
    }

    public class envCommoditySwap
    {
        public Product product { get; set; }
        public envCommoditySwapDetails envCommoditySwapDetails { get; set; }
    }

    public class envCommoditySwapDetails
    {
        public string effectiveDate { get; set; }
        public string terminationDate { get; set; }
        public string settlementCurrency { get; set; }
        public string powerProduct { get; set; }
        public fixedLeg fixedLeg { get; set; }
        public environmentalPhysicalLeg environmentalPhysicalLeg { get; set; }
        //public MarketDisruption marketDisruption { get; set; }
    }

    public class CommoditySwapDetails
    {
        public string effectiveDate { get; set; }
        public string terminationDate { get; set; }
        public string settlementCurrency { get; set; }
        public string powerProduct { get; set; }
        public fixedLeg fixedLeg { get; set; }
        public floatingLeg floatingLeg { get; set; }
        public MarketDisruption marketDisruption { get; set; }
    }



    public class adjustableDate
    {
        public Date unadjustedDate { get; set; }
        public adjustableDate(Date d)
        {
            this.unadjustedDate = d;
        }
        public adjustableDate()
        {
        }
    }

    public class gasPhysicalLeg
    {
        public string payerPartyReference { get; set; }
        public string receiverPartyReference { get; set; }
        public CalculationPeriodsSchedule periodsSchedule { get; set; } //Should this be a list of objects?
        public gas gas { get; set; }
        public GasDeliveryConditions deliveryConditions { get; set; }
        public DeliveryQuantity deliveryQuantity { get; set; }

        public gasPhysicalLeg()
        {
            this.periodsSchedule = new CalculationPeriodsSchedule();
            this.deliveryConditions = new GasDeliveryConditions();
            this.deliveryQuantity = new DeliveryQuantity();
            this.gas = new gas();
        }

        public gasPhysicalLeg(string gasType, string deliveryPt, string deliveryType, string physQtyUnit, string physQtyFreq, double physQty, string totalQtyUnit, double totalQty)
        {
            this.gas = new gas(gasType);
            this.periodsSchedule = new CalculationPeriodsSchedule();
            this.deliveryConditions = new GasDeliveryConditions(deliveryPt, deliveryType);
            this.deliveryQuantity = new DeliveryQuantity(physQtyUnit, physQtyFreq, physQty, totalQtyUnit, totalQty);
        }
    }

    public class gas
    {
        public string type { get; set; }
        public gas() { }
        public gas(string t) { this.type = t; }
    }
    public class GasDeliveryConditions
    {
        public string deliveryPoint { get; set; }
        public string deliveryType { get; set; }

        public GasDeliveryConditions() { }
        public GasDeliveryConditions(string deliveryPt, string deliveryTp)
        {
            this.deliveryPoint = deliveryPt;
            this.deliveryType = deliveryTp;
        }
    }

    public class DeliveryQuantity
    {
        public PhysicalQuantity physicalQuantity { get; set; }
        public TotalPhysicalQuantity totalPhysicalQuantity { get; set; }

        public DeliveryQuantity()
        {
            this.physicalQuantity = new PhysicalQuantity();
            this.totalPhysicalQuantity = new TotalPhysicalQuantity();
        }

        public DeliveryQuantity(string physQtyUnit, string physQtyFreq, double physQty, string totalQtyUnit, double totalQty)
        {
            this.physicalQuantity = new PhysicalQuantity(physQty, physQtyFreq, physQtyUnit);
            this.totalPhysicalQuantity = new TotalPhysicalQuantity(totalQty, totalQtyUnit);
        }
    }

    public class PhysicalQuantity
    {
        public double? quantity { get; set; }
        public string quantityFrequency { get; set; }
        public string quantityUnit { get; set; }

        public PhysicalQuantity() { }

        public PhysicalQuantity(double? q, string freq, string unit)
        {
            this.quantity = q;
            this.quantityFrequency = freq;
            this.quantityUnit = unit;
        }
    }

    public class TotalPhysicalQuantity
    {
        public double? quantity { get; set; }
        public string quantityUnit { get; set; }

        public TotalPhysicalQuantity() { }
        public TotalPhysicalQuantity(double? q, string unit)
        {
            this.quantity = q;
            this.quantityUnit = unit;
        }
    }


    /*
     * 

    public class floatingLeg
    {
        public string payerPartyReference { get; set; }
        public string receiverPartyReference { get; set; }
        public CalculationPeriodsSchedule calculationPeriodSchedule { get; set; }
        public Commodity commodity { get; set; }
        public Quantity notionalQuantity { get; set; }
        public double totalNotionalQuantity { get; set; }
        public Calculation calculation { get; set; }
        public PaymentDates paymentDates { get; set; }

    }

    */
    public class MarketDisruption
    {
        public string marketDisruptionEvents { get; set; }
        public string disruptionFallbacks { get; set; }
    }

    public class Product
    {
        public string primaryAssetClass { get; set; }
        public string secondaryAssetClass { get; set; }
        public string productType { get; set; }
        public string productID { get; set; }
    }
    public class Price
    {
        public double? price { get; set; }
        public string priceCurrency { get; set; }
        public string priceUnit { get; set; }

        public Price() { }
        public Price(double fixedPrice, string fixedPriceCCY, string fixedPriceUnit)
        {
            this.price = fixedPrice;
            this.priceCurrency = fixedPriceCCY;
            this.priceUnit = fixedPriceUnit;
        }
    }

    public class Quantity
    {
        public double? quantity { get; set; }
        public string quantityFrequency { get; set; }
        public string quantityUnit { get; set; }
    }
}
