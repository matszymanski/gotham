﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.Common
{
    public class CustomTable
    {
        public string TableName { get; set; }
        public DataTable Table { get; set; }
    }
}
