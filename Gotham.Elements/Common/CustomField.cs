﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.Common
{
    public class CustomField
    {
        public string Tag { get; set; }
        public string Value { get; set; }
    }
}
