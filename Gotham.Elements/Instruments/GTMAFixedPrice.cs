﻿using Gotham.Elements.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Gotham.Elements.Instruments
{
    public class GTMAFixedPrice : TradeRepresentation
    {
        public standard.tradeHeader tradeHeader { get; set; }
        public standard.GTMACommoditySwap commoditySwap { get; set; }
        public standard.documentation documentation { get; set; }
        private GTMAFixedPrice() { }
        //DateTime effectDate, DateTime termDate, double qty, int periodMultiplier, string period, bool balanceOfFirstPeriod, double price, string ccy, string priceUnit, bool masterAgPaymentDates
        //DateTime effectDate, DateTime termDate, double qty, int periodMultiplier, string period, bool balanceOfFirstPeriod, double price, string ccy, string priceUnit, bool masterAgPaymentDates
        public GTMAFixedPrice(
                string partyAID,
                string partyBID,
                Date tradeDate,
                DateTime executionDateTime,
                string executionTimeZone,
                string internalPartyID,
                string externalPartyID,
                DateTime effectDate, 
                DateTime termDate, 
                double qty, 
                int periodMultiplier, 
                string period, 
                bool balanceOfFirstPeriod, 
                double price, 
                string ccy, 
                string priceUnit, 
                bool masterAgPaymentDates)
        {
            this.tradeHeader = new standard.tradeHeader();
            base.CustomFields = new List<CustomField>();
            base.CustomTables = new List<CustomTable>();
            this.tradeHeader.tradeDate = tradeDate;
            this.tradeHeader.tradeExecutionDateTime = new standard.TradeExecutionDateTime();
            this.tradeHeader.tradeExecutionDateTime.executionDateTime = executionDateTime;
            this.tradeHeader.tradeExecutionDateTime.executionTimeZone = executionTimeZone;

            //GTMACommoditySwap(DateTime effectDate, DateTime termDate, double qty, int periodMultiplier, string period, bool balanceOfFirstPeriod, double price, string ccy, string priceUnit, bool masterAgPaymentDates)

            this.commoditySwap = new standard.GTMACommoditySwap(
                effectDate, 
                termDate, 
                qty, 
                periodMultiplier, 
                period, 
                balanceOfFirstPeriod, 
                price, 
                ccy, 
                priceUnit, 
                masterAgPaymentDates
                );

            this.partyA = new party() { partyId = new PartyID { id = partyAID } };
            this.partyB = new party() { partyId = new PartyID { id = partyBID } };

            this.InternalPartyID = internalPartyID;
            this.ExternalPartyID = externalPartyID;
        }

        public override XmlDocument AsCPML()
        {
            throw new NotImplementedException();
        }

        public override XmlDocument AseConfirm()
        {
            throw new NotImplementedException();
        }
    }
}
