﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Gotham.Elements.Instruments.Brent
{
    public class Brent : TradeRepresentation
    {
        public Brent() { }
        public Gotham.Elements.Instruments.Brent.commoditySwap commoditySwap { get; set; }
        public Brent(
            DateTime start,
            DateTime end,
            string settCcy,
            string floReceiverPartyReference,
            Int32 multiplier,
            string period,
            string specifiedPrice,
            string deliveryDates,
            string floQuantityUnit,
            string floQuantityFrequency,
            double floQuantity,
            double floTotalNotionalQuantity,
            string fixReceiverPartyReference,
            double fixPrice,
            string fixPriceUnit,
            string fixPriceCCY,
            string fixQuantityUnit,
            string fixQuantityFrequency,
            double fixQuantity,
            double fixTotalNotionalQuantity
            )
        {
            commoditySwap = new Instruments.Brent.commoditySwap(start, end, settCcy, floReceiverPartyReference, multiplier, period, specifiedPrice,deliveryDates, floQuantityUnit, floQuantityFrequency, floQuantity, floTotalNotionalQuantity, fixReceiverPartyReference, fixPrice, fixPriceUnit, fixPriceCCY, fixQuantityUnit, fixQuantityFrequency, fixQuantity, fixTotalNotionalQuantity);
        }

        public override XmlDocument AseConfirm()
        {
            throw new NotImplementedException();
        }

        public override XmlDocument AsCPML()
        {
            throw new NotImplementedException();
        }
    }
    public class commoditySwap
    {
        public string productType { get; set; }
        public string assetClass { get; set; }
        public Gotham.Elements.Instruments.Brent.adjustableDate effectiveDate { get; set; }
        public Gotham.Elements.Instruments.Brent.adjustableDate terminationDate { get; set; }
        public string settlementCurrency { get; set; }
        public Gotham.Elements.Instruments.Brent.fixedLeg fixedLeg { get; set; }
        public Gotham.Elements.Instruments.Brent.floatingLeg floatingLeg { get; set; }
        public commoditySwap() { }
        public commoditySwap(
            DateTime start,
            DateTime end,
            string settCcy,
            string floReceiverPartyReference,
            Int32 multiplier,
            string period,
            string specifiedPrice,
            string deliveryDates,
            string floQuantityUnit,
            string floQuantityFrequency,
            double floQuantity,
            double floTotalNotionalQuantity,
            string fixReceiverPartyReference,
            double fixPrice,
            string fixPriceUnit,
            string fixPriceCCY,
            string fixQuantityUnit,
            string fixQuantityFrequency,
            double fixQuantity,
            double fixTotalNotionalQuantity
            )
        {
            this.productType = "CrudeOilSwap";
            this.assetClass = "Commodity";
            string instrumentID = "OIL-BRENT-IPE";
            this.effectiveDate = new Gotham.Elements.Instruments.Brent.adjustableDate(start);
            this.terminationDate = new Gotham.Elements.Instruments.Brent.adjustableDate(end);
            this.settlementCurrency = settCcy;
            this.floatingLeg = new floatingLeg(floReceiverPartyReference, multiplier, period, instrumentID, specifiedPrice, deliveryDates, floQuantityUnit, floQuantityFrequency, floQuantity, floTotalNotionalQuantity);
            this.fixedLeg = new fixedLeg(fixReceiverPartyReference, fixPrice, fixPriceCCY, fixPriceUnit, fixQuantity, fixQuantityUnit, fixQuantityFrequency, fixTotalNotionalQuantity);
        }
    }
    public class floatingLeg
    {
        public string receiverPartyReference { get; set; }
        public Gotham.Elements.Instruments.Brent.calculationPeriodsSchedule calculationPeriodsSchedule { get; set; }
        public Gotham.Elements.Instruments.Brent.commodity commodity { get; set; }
        public Gotham.Elements.Instruments.Brent.notionalQuantity notionalQuantity { get; set; } //pls check
        public double totalNotionalQuantity { get; set; }

        private floatingLeg() { }
        public floatingLeg(
            string recPartyRef,
            Int32 periodMultiplier,
            string period,
            string instrumentID,
            string specifiedPrice,
            string deliveryDates,
            string quantityUnit,
            string quantityFreq,
            double quantity,
            double totalNotionalQty
            )
        {
            this.receiverPartyReference = recPartyRef;
            this.calculationPeriodsSchedule = new Instruments.Brent.calculationPeriodsSchedule(periodMultiplier, period);
            this.commodity = new Instruments.Brent.commodity(instrumentID, specifiedPrice, deliveryDates);
            this.notionalQuantity = new notionalQuantity(quantityUnit, quantityFreq, quantity);
            this.totalNotionalQuantity = totalNotionalQty;
        }

    }
    public class adjustableDate
    {
        public DateTime unadjustedDate { get; set; }
        public adjustableDate(DateTime d)
        {
            this.unadjustedDate = d;
        }
        private adjustableDate()
        {
        }
    }
    public class commodity
    {
        public string instrumentId { get; set; }
        public string specifiedPrice { get; set; }
        public string deliveryDates { get; set; }
        private commodity() { }
        public commodity(string inst, string specPrice, string delDates)
        {
            this.instrumentId = inst;
            this.specifiedPrice = specPrice;
            this.deliveryDates = delDates;
        }
    }
    class calculation
    {
        public Gotham.Elements.Instruments.Brent.pricingDates pricingDates { get; set; }
    }
    public class pricingDates
    {
        public string dayType { get; set; }
        public string dayDistribution { get; set; }
    }
    public class calculationPeriodsSchedule
    {
        public Int32 periodMultiplier { get; set; }
        public string period { get; set; }

        private calculationPeriodsSchedule() { }
        public calculationPeriodsSchedule(Int32 mult, string per)
        {
            this.periodMultiplier = mult;
            this.period = per;
        }

    }
    public class fixedLeg
    {
        public string receiverPartyReference { get; set; }
        public fixedPrice fixedPrice { get; set; } //pls check
        public Gotham.Elements.Instruments.Brent.notionalQuantity notionalQuantity { get; set; } //pls check
        public double totalNotionalQuantity { get; set; }
        private fixedLeg() { }
        public fixedLeg(string recPartyRef, double price, string priceCcy, string priceUnit, double qty, string qtyUnit, string qtyFreq, double totalQty)
        {
            this.receiverPartyReference = recPartyRef;
            this.fixedPrice = new Gotham.Elements.Instruments.Brent.fixedPrice(price, priceCcy, priceUnit);
            this.notionalQuantity = new notionalQuantity(qtyUnit, qtyFreq, qty);
            this.totalNotionalQuantity = totalQty;
        }
    }
    public class fixedPrice
    {
        public double price { get; set; }
        public string priceCurrency { get; set; }
        public string priceUnit { get; set; }
        private fixedPrice() { }
        public fixedPrice(double p, string ccy, string unit)
        {
            this.price = p;
            this.priceCurrency = ccy;
            this.priceUnit = unit;
        }
    }
    public class notionalQuantity
    {
        public string quantityUnit { get; set; }
        public string quantityFrequency { get; set; }
        public double quantity { get; set; }
        private notionalQuantity() { }
        public notionalQuantity(string qtyUnit, string qtyFreq, double q)
        {
            this.quantity = q;
            this.quantityFrequency = qtyFreq;
            this.quantityUnit = qtyUnit;
        }
    }
}
