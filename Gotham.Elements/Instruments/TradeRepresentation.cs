﻿using Gotham.Elements.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Gotham.Elements.Instruments
{
    public abstract class TradeRepresentation
    {
        public string TradeID { get; set; }
        public string InternalPartyID { get; set; }
        public string ExternalPartyID { get; set; }

        public List<CustomField> CustomFields { get; set; }
        public List<CustomTable> CustomTables { get; set; }
        public party partyA { get; set; }
        public party partyB { get; set; }

        public virtual string AsFPMLJsonified()
        {
            return JsonConvert.SerializeObject(this);
        }

        public abstract XmlDocument AseConfirm();
        public abstract XmlDocument AsCPML();
    }
}
