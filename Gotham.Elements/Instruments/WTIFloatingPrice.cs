﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Gotham.Elements.Common;
using Gotham.Elements.standard;
using Newtonsoft.Json;

namespace Gotham.Elements.Instruments
{
    public class WTIFloatingPrice : TradeRepresentation
    {
        public standard.tradeHeader tradeHeader { get; set; }
        public standard.WTICommoditySwap commoditySwap { get; set; }
        public standard.documentation documentation { get; set; }
        
        private WTIFloatingPrice()
        {
            base.CustomFields = new List<CustomField>();
            base.CustomTables = new List<CustomTable>();
        }

        public WTIFloatingPrice(
                string internalPartyID, 
                string externalPartyID,
                string partyAID,
                string partyBID,
                Date tradeDate, 
                DateTime executionDateTime, 
                string executionTimeZone,
                Date effectiveDate,
                Date terminationDate,
                string settlementCurrency,
                Int32 periodMultiplier,
                string period,
                bool balanceOfFirstPeriod,
                string oilType,
                string oilGrade,
                string pipelineName,
                string withdrawalPoint,
                bool deliverableByBarge,
                string risk,
                string quantityUnit,
                string quantityFrequency,
                double quantity,
                string instrumentID,
                string specifiedPrice,
                string dayType,
                string dayDistribution
            )
        {
            this.tradeHeader = new standard.tradeHeader();
            this.tradeHeader.tradeDate = tradeDate;
            this.tradeHeader.tradeExecutionDateTime = new standard.TradeExecutionDateTime();
            this.tradeHeader.tradeExecutionDateTime.executionDateTime = executionDateTime;
            this.tradeHeader.tradeExecutionDateTime.executionTimeZone = executionTimeZone;

            this.commoditySwap = new standard.WTICommoditySwap(
                                effectiveDate, 
                                terminationDate, 
                                settlementCurrency, 
                                periodMultiplier, 
                                period, 
                                balanceOfFirstPeriod, 
                                oilType, 
                                oilGrade, 
                                pipelineName,
                                withdrawalPoint, 
                                deliverableByBarge, 
                                risk, 
                                quantityUnit, 
                                quantityFrequency, 
                                quantity,
                                instrumentID,
                                specifiedPrice,
                                dayType,
                                dayDistribution
                                );

            this.partyA = new party() { partyId = new PartyID { id = partyAID } };
            this.partyB = new party() { partyId = new PartyID { id = partyBID } };
            
            this.InternalPartyID = internalPartyID;
            this.ExternalPartyID = externalPartyID;
        }
        

        public override XmlDocument AseConfirm()
        {
            throw new NotImplementedException();
        }

        public override XmlDocument AsCPML()
        {
            throw new NotImplementedException();
        }
    }



    /*

                DateTime effectiveDate,
                DateTime terminationDate,
                string settlementCurrency,
                Int32 periodMultiplier,
                string period,
                bool balanceOfFirstPeriod,
                string oilType,
                string oilGrade,
                string pipelineName,
                string deliveryPointScheme,
                bool deliverableByBarge,
                string risk,
                string quantityUnit,
                string quantityFrequency,
                double quantity










    */
    //public class commoditySwap
    //{
    //    public adjustableDate effectiveDate { get; set; }
    //    public adjustableDate terminationDate { get; set; }
    //    public string settlementCurrency { get; set; }
    //    public gasPhysicalLeg gasPhysicalLeg { get; set; }
    //    public fixedLeg fixedLeg { get; set; }
    //    public MarketDisruption marketDisruption { get; set; }

    //    public commoditySwap()
    //    {
    //        this.gasPhysicalLeg = new gasPhysicalLeg();
    //        this.fixedLeg = new fixedLeg();
    //        this.marketDisruption = new MarketDisruption();
    //        this.effectiveDate = new adjustableDate();
    //        this.terminationDate = new adjustableDate();
    //    }

    //    public commoditySwap(DateTime effectiveDate, DateTime terminationDate, string settlementCCY, string gasType, string deliveryPt, string deliveryType, string physQtyUnit, string physQtyFreq, double physQty, string totalQtyUnit, double totalQty, double fixedPrice, string fixedPriceCCY, string fixedPriceUnit)
    //    {
    //        this.gasPhysicalLeg = new gasPhysicalLeg(gasType, deliveryPt, deliveryType, physQtyUnit, physQtyFreq, physQty, totalQtyUnit, totalQty);
    //        this.fixedLeg = new fixedLeg(fixedPrice, fixedPriceCCY, fixedPriceUnit);
    //        this.marketDisruption = new MarketDisruption();
    //        this.effectiveDate = new adjustableDate(effectiveDate);
    //        this.terminationDate = new adjustableDate(terminationDate);
    //        this.settlementCurrency = settlementCCY;
    //    }
    //}


}
