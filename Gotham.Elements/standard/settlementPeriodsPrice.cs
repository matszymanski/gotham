﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class settlementPeriodsPrice
    {
        public double price { get; set; }
        public string priceCurrency { get; set; }
        public string priceUnit { get; set; }

        public settlementPeriodsPrice() { }
        public settlementPeriodsPrice(double p, string ccy, string punit)
        {
            this.price = p;
            this.priceCurrency = ccy;
            this.priceUnit = punit;
        }
    }
}
