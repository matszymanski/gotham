﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class periodsSchedule
    {
        public Int32 periodMultiplier { get; set; }
        public string period { get; set; }
        public bool balanceOfFirstPeriod { get; set; }

        private periodsSchedule() { }
        public periodsSchedule(Int32 mult, string period, bool balance)
        {
            this.periodMultiplier = mult;
            this.period = period;
            this.balanceOfFirstPeriod = balance;
        }

    }
}
