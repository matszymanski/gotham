﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class calculation
    {
        public pricingDates pricingDates { get; set; }

        private calculation() { }
        public calculation(string dayType, string dayDistribution)
        {
            this.pricingDates = new standard.pricingDates(dayType, dayDistribution);
        }
    }
}
