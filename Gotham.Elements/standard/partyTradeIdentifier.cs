﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class partyTradeIdentifier
    {
        public string tradeId { get; set; }
        public string tradeIdContext { get; set; }
        public string partyReference { get; set; }
    }
}
