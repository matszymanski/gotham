﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class WTICommoditySwap
    {
        public adjustableDate effectiveDate { get; set; }
        public adjustableDate terminationDate { get; set; }
        public string settlementCurrency { get; set; }

        public oilPhysicalLeg oilPhysicalLeg { get; set; }
        public floatingLeg floatingLeg { get; set; }

        public WTICommoditySwap() { }

        public WTICommoditySwap
            (
                Date effectiveDate,
                Date terminationDate,
                string settlementCurrency,
                Int32 periodMultiplier,
                string period,
                bool balanceOfFirstPeriod,
                string oilType,
                string oilGrade,
                string pipelineName,
                string withdrawalPoint,
                bool deliverableByBarge,
                string risk,
                string quantityUnit,
                string quantityFrequency,
                double quantity,
                string instrumentID,
                string specifiedPrice, 
                string dayType, 
                string dayDistribution
            )
        {
            this.effectiveDate = new adjustableDate(effectiveDate);
            this.terminationDate = new adjustableDate(terminationDate);
            this.settlementCurrency = settlementCurrency;

            pipeline p = new pipeline(pipelineName, withdrawalPoint, deliverableByBarge, risk);
            deliveryConditions c = new deliveryConditions(p);

            physicalQuantity pq = new physicalQuantity(quantityUnit, quantityFrequency, quantity);
            deliveryQuantity q = new deliveryQuantity(pq);
            oil o = new oil(oilType, oilGrade);

            deliveryPeriods dp = new deliveryPeriods();
            dp.periodSchedule = new List<periodsSchedule>();
            periodsSchedule ps = new periodsSchedule(periodMultiplier, period, balanceOfFirstPeriod);
            dp.periodSchedule.Add(ps);

            this.oilPhysicalLeg = new oilPhysicalLeg(dp, o, c, q);

            this.floatingLeg = new standard.floatingLeg(instrumentID, specifiedPrice, dayType, dayDistribution);
        }
    }
}
