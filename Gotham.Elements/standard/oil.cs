﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class oil
    {
        public string type { get;set;}
        public string grade { get; set; }

        private oil() { }
        public oil(string t,string g )
        {
            this.type = t;
            this.grade = g;
        }
    }
}
