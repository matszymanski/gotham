﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class commodity
    {
        public string instrumentId { get; set; }
        public string specifiedPrice { get; set; }

        private commodity() { }

        public commodity(string instID, string specPrice)
        {
            this.instrumentId = instID;
            this.specifiedPrice = specPrice;
        }
    }
}
