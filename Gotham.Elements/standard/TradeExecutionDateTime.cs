﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class TradeExecutionDateTime
    {
        public DateTime executionDateTime { get; set; }
        public string executionTimeZone { get; set; }
    }
}
