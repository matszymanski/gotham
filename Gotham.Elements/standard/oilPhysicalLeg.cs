﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class oilPhysicalLeg
    {
        public deliveryPeriods deliveryPeriods { get; set; }
        public oil oil { get; set; }
        public deliveryConditions deliveryConditions { get; set; }
        public deliveryQuantity deliveryQuantity { get; set; }

        private oilPhysicalLeg() { }
        public oilPhysicalLeg(deliveryPeriods periods, oil o, deliveryConditions conditions, deliveryQuantity q)
        {
            this.deliveryPeriods = new standard.deliveryPeriods();
            this.deliveryPeriods.periodSchedule = new List<standard.periodsSchedule>();
            this.deliveryPeriods = periods;


            this.oil = o;
            this.deliveryConditions = conditions;
            this.deliveryQuantity = q;

        }
    }
}
