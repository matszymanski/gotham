﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class floatingLeg
    {
        public commodity commodity { get; set; }
        public calculation calculation { get; set; }
        public bool masterAgreementPaymentDates { get; set; }

        private floatingLeg() { }
        public floatingLeg(string instrumentID, string specifiedPrice, string dayType, string dayDistribution)
        {
            this.commodity = new standard.commodity(instrumentID, specifiedPrice);
            this.calculation = new standard.calculation(dayType, dayDistribution);
        }
    }
}
