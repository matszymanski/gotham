﻿using System;
using Newtonsoft.Json;

namespace Gotham.Elements.standard
{
    public class party
    {
        public string partyName { get; set; }
        [JsonProperty]
        public PartyID partyId { get; set; }
        public string partyRole { get; set; }
        public partyAccount partyAccount { get; set; }

        public party()
        {
            this.partyId = new PartyID();
            this.partyAccount = new partyAccount();
        }
    }
}
