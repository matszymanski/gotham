﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class physicalQuantity
    {
        public string quantityUnit { get; set; }
        public string quantityFrequency { get; set; }
        public double quantity { get; set; }

        private physicalQuantity() { }
        public physicalQuantity(string unit, string freq, double qty)
        {
            this.quantityUnit = unit;
            this.quantityFrequency = freq;
            this.quantity = qty;
        }
    }
}
