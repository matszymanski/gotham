﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class pricingDates
    {
        public string dayType { get; set; }
        public string dayDistribution { get; set; }

        private pricingDates() { }

        public pricingDates(string dt, string dd)
        {
            this.dayType = dt;
            this.dayDistribution = dd;
        }
    }
}
