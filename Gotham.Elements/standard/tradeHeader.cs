﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class tradeHeader
    {
        public List<partyTradeIdentifier> partyTradeIdentifier { get; set; }
        public Date tradeDate { get; set; }
        public TradeExecutionDateTime tradeExecutionDateTime { get; set; }

        public tradeHeader()
        {
            this.partyTradeIdentifier = new List<partyTradeIdentifier>();
            this.tradeExecutionDateTime = new TradeExecutionDateTime();
        }
        public tradeHeader(Date date)
        {
            this.partyTradeIdentifier = new List<partyTradeIdentifier>();
            this.tradeExecutionDateTime = new TradeExecutionDateTime();
            this.tradeDate = date;
        }
    }
}
