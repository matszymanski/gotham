﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class deliveryQuantity
    {
        public physicalQuantity physicalQuantity { get; set; }
        private deliveryQuantity() { }
        public deliveryQuantity(physicalQuantity pq)
        {
            this.physicalQuantity = pq;
        }
    }
}
