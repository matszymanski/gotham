﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class calculationPeriodsSchedule
    {
        public int periodMultiplier { get; set; }
        public string period { get; set; }
        public bool balanceOfFirstPeriod { get; set; }

        public calculationPeriodsSchedule() { }
        public calculationPeriodsSchedule(int pm, string p, bool balance)
        {
            this.periodMultiplier = pm;
            this.period = p;
            this.balanceOfFirstPeriod = balance;
        }
    }
}
