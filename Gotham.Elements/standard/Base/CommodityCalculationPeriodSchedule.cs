﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard.Base
{
    public class CommodityCalculationPeriodSchedule
    {
        public int periodMultiplier { get; set; }
        public string period { get; set; }
        public bool balanceOfFirstPeriod { get; set; }
    }
}
