﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard.Base
{
    public class CommoditySwap
    {
        public AdjustableOrRelativeDate effectiveDate { get; set; }
        public AdjustableOrRelativeDate terminationDate { get; set; }
        public SettlementCurrency settlementCurrency { get; set; }
        public FixedLeg fixedLeg { get; set; }
        public FloatingLeg floatingLeg { get; set; }
        public CoalPhysicalLeg coalPhysicalLeg { get; set; }
        public ElectricityPhysicalLeg electricityPhysicalLeg { get; set; }
        public GasPhysicalLeg gasPhysicalLeg { get; set; }
        public OilPhysicalLeg oilPhysicalLeg { get; set; }
        public AdditionalCommoditySwapLeg additionalCommoditySwapLeg { get; set; }
    }
}
