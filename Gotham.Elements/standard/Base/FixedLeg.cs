﻿using Gotham.Elements.standard.Base.Dates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard.Base
{
    public class FixedLeg
    {
        public party payerPartyReference { get; set; }
        public party receiverPartyReference { get; set; }
        public CalculationDates calculationDates { get; set; }
        public CalculationDates calculationPeriods { get; set; }
    }
}
