﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard.Base
{
    public class CommodityCalculationPeriod
    {
        public AdjustableDate calculationDates { get; set; }
        public CommodityCalculationPeriod calculationPeriods { get; set; }
        public CommodityCalculationPeriodSchedule calculationPeriodsSchedule { get; set; }
    }
}
