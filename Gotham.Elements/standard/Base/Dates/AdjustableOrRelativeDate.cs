﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard.Base
{
    public class AdjustableOrRelativeDate
    {
        public AdjustableDate adjustableDate { get; set; }
        public RelativeDate relativeDate { get; set; }
    }
}
