﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard.Base.Dates
{
    public class CalculationDates
    {
        public List<AdjustableDate> unadjustedDate { get; set; }
        public DateAdjustments dateAdjustments { get; set; }
        public List<AdjustableDate> adjustedDates { get; set; }
    }
}
