﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class GTMAFixedLeg
    {
        public calculationPeriodsSchedule calculationPeriodsSchedule { get; set; }
        public settlementPeriodsPrice settlementPeriodsPrice { get; set; }
        public bool masterAgreementPaymentDates { get; set; }
        public GTMAFixedLeg() { }
        public GTMAFixedLeg(int periodMultiplier, string period, bool balanceOfFirstPeriod, double price, string ccy, string priceUnit, bool masterAgPaymentDates)
        {
            this.calculationPeriodsSchedule = new calculationPeriodsSchedule(periodMultiplier, period, balanceOfFirstPeriod);
            this.settlementPeriodsPrice = new settlementPeriodsPrice(price, ccy, priceUnit);
            this.masterAgreementPaymentDates = masterAgPaymentDates;
        }
    }
}
