﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class GTMADeliveryPeriods
    {
        public string calculationPeriodsScheduleReference { get; set; }

        public GTMADeliveryPeriods()
        {
            this.calculationPeriodsScheduleReference = "calculationPeriods";
        }
    }
}
