﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class GTMAelectricityPhysicalLeg
    {
        public GTMADeliveryPeriods deliveryPeriods { get; set; }
        public GTMASettlementPeriods settlementPeriods { get; set; }
        public GTMAdeliveryConditions deliveryConditions { get; set; }
        public GTMAPhysicalQuantity deliveryQuantity { get; set; }
        public string electricity { get; set; }
        private GTMAelectricityPhysicalLeg()
        {
            this.deliveryPeriods = new GTMADeliveryPeriods();
            this.settlementPeriods = new GTMASettlementPeriods();
            this.deliveryConditions = new GTMAdeliveryConditions();
            this.electricity = "Electricity";
        }

        public GTMAelectricityPhysicalLeg(double quantity)
        {
            this.deliveryPeriods = new GTMADeliveryPeriods();
            this.settlementPeriods = new GTMASettlementPeriods();
            this.deliveryConditions = new GTMAdeliveryConditions();
            this.deliveryQuantity = new GTMAPhysicalQuantity(quantity);
            this.electricity = "Electricity";
        }

    }
}
