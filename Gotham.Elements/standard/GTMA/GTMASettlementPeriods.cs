﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class GTMASettlementPeriods
    {
        public List<string> applicableDay { get; set; }
        public string duration { get; set; }
        public string excludeHolidays { get; set; }
        public time startTime { get; set; }
        public time endTime { get; set; }
        public  GTMASettlementPeriods()
        {
            this.applicableDay = new List<string>();
            this.applicableDay.Add("MON");
            this.applicableDay.Add("TUE");
            this.applicableDay.Add("WED");
            this.applicableDay.Add("THU");
            this.applicableDay.Add("FRI");

            this.duration = "30Minutes";

            this.startTime = new time();
            this.startTime.hourMinuteTime = "07:00:00";
            this.startTime.location = "Europe/London";

            this.endTime = new time();
            this.startTime.hourMinuteTime = "19:00:00";
            this.startTime.location = "Europe/London";

            this.excludeHolidays = "N2EX";
        }
    }
}
