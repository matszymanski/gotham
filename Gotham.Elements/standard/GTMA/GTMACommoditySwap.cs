﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class GTMACommoditySwap
    {
        public adjustableDate effectiveDate { get; set; }
        public adjustableDate terminationDate { get; set; }
        public string settlementCurrency { get; set; }
        public GTMAelectricityPhysicalLeg electricityPhysicalLeg { get; set; }
        public GTMAFixedLeg fixedLeg { get; set; }

        public GTMACommoditySwap() { }
        public GTMACommoditySwap(DateTime effectDate, DateTime termDate, double qty, int periodMultiplier, string period, bool balanceOfFirstPeriod, double price, string ccy, string priceUnit, bool masterAgPaymentDates)
        {
            this.effectiveDate = new adjustableDate(new Date(effectDate));
            this.terminationDate = new adjustableDate(new Date(termDate));
            this.electricityPhysicalLeg = new GTMAelectricityPhysicalLeg(qty);
            this.fixedLeg = new GTMAFixedLeg(periodMultiplier, period, balanceOfFirstPeriod, price, ccy, priceUnit, masterAgPaymentDates);
        }
    }
}
