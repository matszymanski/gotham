﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class GTMAPhysicalQuantity
    {
        public string quantityUnit { get; set; }
        public string quantityFrequency { get; set; }
        public double quantity { get; set; }
        
        private GTMAPhysicalQuantity() { }
        public GTMAPhysicalQuantity(double qty)
        {
            this.quantityUnit = "MWh";
            this.quantityFrequency = "PerSettlementPeriod";
            this.quantity = qty;
        }
    }
}
