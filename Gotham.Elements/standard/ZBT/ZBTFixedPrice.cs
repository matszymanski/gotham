﻿using Gotham.Elements.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Gotham.Elements
{
    public class ZBTFixedPrice : Instruments.TradeRepresentation
    {
        public tradeHeader tradeHeader { get; set; }
        public ZBTCommoditySwap commoditySwap { get; set; }

        public ZBTFixedPrice()
        {
            base.CustomFields = new List<CustomField>();
            base.CustomTables = new List<CustomTable>();
            this.tradeHeader = new tradeHeader();
            this.commoditySwap = new ZBTCommoditySwap();
        }

        public ZBTFixedPrice(string internalPartyId, string externalPartyId, string partyAID, string partyBID, Date tradeDate, Date effectiveDate, Date terminationDate, string settlementCCY, string gasType, string deliveryPt, string deliveryType, string physQtyUnit, string physQtyFreq, double physQty, string totalQtyUnit, double totalQty, double fixedPrice, string fixedPriceCCY, string fixedPriceUnit, string agreement)
        {
            this.tradeHeader = new tradeHeader(tradeDate);
            this.InternalPartyID = internalPartyId;
            this.ExternalPartyID = externalPartyId;
            this.partyA = new party { partyId = new PartyID { id = partyAID } };
            this.partyB = new party { partyId = new PartyID { id = partyBID } };
            this.commoditySwap = new ZBTCommoditySwap(effectiveDate, terminationDate, settlementCCY, gasType, deliveryPt, deliveryType, physQtyUnit, physQtyFreq, physQty, totalQtyUnit, totalQty, fixedPrice, fixedPriceCCY, fixedPriceUnit);
            
        }

        public override XmlDocument AseConfirm()
        {
            throw new NotImplementedException();
        }

        public override XmlDocument AsCPML()
        {
            throw new NotImplementedException();
        }
    }
}
