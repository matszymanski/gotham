﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements
{
    public class ZBTCommoditySwap
    {
        public adjustableDate effectiveDate { get; set; }
        public adjustableDate terminationDate { get; set; }
        public string settlementCurrency { get; set; }
        public gasPhysicalLeg gasPhysicalLeg { get; set; }
        public fixedLeg fixedLeg { get; set; }
        public MarketDisruption marketDisruption { get; set; }

        public ZBTCommoditySwap()
        {
            this.gasPhysicalLeg = new gasPhysicalLeg();
            this.fixedLeg = new fixedLeg();
            this.marketDisruption = new MarketDisruption();
            this.effectiveDate = new adjustableDate();
            this.terminationDate = new adjustableDate();
        }

        public ZBTCommoditySwap(Date effectiveDate, Date terminationDate, string settlementCCY, string gasType, string deliveryPt, string deliveryType, string physQtyUnit, string physQtyFreq, double physQty, string totalQtyUnit, double totalQty, double fixedPrice, string fixedPriceCCY, string fixedPriceUnit)
        {
            this.gasPhysicalLeg = new gasPhysicalLeg(gasType, deliveryPt, deliveryType, physQtyUnit, physQtyFreq, physQty, totalQtyUnit, totalQty);
            this.fixedLeg = new fixedLeg(fixedPrice, fixedPriceCCY, fixedPriceUnit);
            this.marketDisruption = new MarketDisruption();
            this.effectiveDate = new adjustableDate(effectiveDate);
            this.terminationDate = new adjustableDate(terminationDate);
            this.settlementCurrency = settlementCCY;
        }
    }
}
