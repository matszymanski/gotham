﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements.standard
{
    public class pipeline
    {
        public string pipelineName { get; set; }
        public string withdrawalPoint { get; set; }
        public bool deliverableByBarge { get; set; }
        public string risk { get; set; }

        private pipeline() { }
        public pipeline(string p, string withdrawalPnt, bool byBarge, string rk)
        {
            this.pipelineName = p;
            this.withdrawalPoint = withdrawalPnt;
            this.deliverableByBarge = byBarge;
            this.risk = rk;
        }
    }
}
