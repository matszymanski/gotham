﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Elements
{
    [JsonConverter(typeof(DateConverter))]
    public class Date : IEquatable<Date>, IEqualityComparer<Date>
    {
        public int Year => _dateTime.Year;
        public int Month => _dateTime.Month;
        public int Day => _dateTime.Day;

        private DateTime _dateTime;

        //Added empty constructor or xml serialisation will fail.
        private Date()
        {            
        }

        [JsonConstructor]
        public Date(string date)
        {
            _dateTime = DateTime.Parse(date);
        }

        public Date(int year, int month, int day)
        {
            _dateTime = new DateTime(year, month, day); 
        }

        public Date(DateTime date)
        {
            _dateTime = date;
        }

        public override string ToString()
        {
            return _dateTime.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is Date)
            {
                var d = obj as Date;
                return this.Year == d.Year && this.Month == d.Month && this.Day == d.Day;
            }
            else
                return false;
        }

        public bool Equals(Date other)
        {
            return Year == other.Year && Month == other.Month && Day == other.Day;
        }

        public bool Equals(Date x, Date y)
        {
            return x._dateTime.Date == y._dateTime.Date;
        }

        public int GetHashCode(Date obj)
        {
            return obj.ToString().GetHashCode();
        }

        public static bool operator ==(Date obj1, Date obj2) => obj1.Equals(obj2);
        public static bool operator !=(Date obj1, Date obj2) => !obj1.Equals(obj2);
    }
}
