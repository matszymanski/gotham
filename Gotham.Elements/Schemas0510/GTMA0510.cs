﻿//using fpml510ConfirmProcess;
using fpml510Main;
//fpml510Main
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommoditySwap = fpml510ConfirmProcess.CommoditySwap;

namespace Gotham.Elements.Schemas0510
{
    public class GTMA0510
    {
        //public fpml510ConfirmProcess.TradeHeader tradeHeader { get; set; }
        //public fpml510ConfirmProcess.CommoditySwap commoditySwap { get; set; }
        //public fpml510ConfirmProcess.FixedPriceLeg fixedLeg { get; set; }
        //public fpml510ConfirmProcess.ElectricityPhysicalLeg electrictypPhysicalLeg { get; set; }
        //public fpml510ConfirmProcess.FixedPrice fixedPrice { get; set; }
        //public fpml510ConfirmProcess.SettlementPeriods settlementPeriod { get; set; }
        //public fpml510ConfirmProcess.ElectricityProduct electricityProduct { get; set; }

        public TradeHeader tradeHeader { get; set; }
        //public fpml510Main.CommoditySwap commoditySwap { get; set; }
        public CommoditySwap commoditySwap { get; set; }
        FixedPriceLeg fixedLeg { get; set; }
        ElectricityPhysicalLeg electrictypPhysicalLeg { get; set; }
        FixedPrice fixedPrice { get; set; }
        SettlementPeriods settlementPeriod { get; set; }
        ElectricityProduct electricityProduct { get; set; }
        public GTMA0510()
        {

            //error CS0030: Cannot convert type 'fpml510ConfirmProcess.SettlementPeriodsReference[]' to 'fpml510ConfirmProcess.SettlementPeriodsReference'
            //error CS0029: Cannot implicitly convert type 'fpml510ConfirmProcess.SettlementPeriodsReference' to 'fpml510ConfirmProcess.SettlementPeriodsReference[]''
            //this.tradeHeader = new fpml510ConfirmProcess.TradeHeader();

            //this.commoditySwap = new fpml510ConfirmProcess.CommoditySwap();
            //this.settlementPeriod = new fpml510ConfirmProcess.SettlementPeriods();

            //this.tradeHeader = new TradeHeader();
            //this.commoditySwap = new fpml510Main.CommoditySwap();
            //this.settlementPeriod = new SettlementPeriods();
            //SettlementPeriodsReference[] settlementPeriodsReferences = new SettlementPeriodsReference[0];
        }
        public GTMA0510(string currency, decimal price, string priceUnit, decimal quantity, string quantityUnit)
        {
            tradeHeader = new TradeHeader();

            //this.commoditySwap = new fpml510ConfirmProcess.CommoditySwap();
            //commoditySwap = new fpml510Main.CommoditySwap();
            commoditySwap = new CommoditySwap();
            commoditySwap.settlementCurrency = new IdentifiedCurrency();
            commoditySwap.settlementCurrency.Value = currency;

            FixedPriceLeg fixedLeg = new FixedPriceLeg();
            fixedLeg.calculationPeriodsSchedule = new CommodityCalculationPeriodsSchedule();
            fixedLeg.calculationPeriodsSchedule.periodMultiplier = "1";
            fixedLeg.calculationPeriodsSchedule.period = "M";
            fixedLeg.calculationPeriodsSchedule.balanceOfFirstPeriod = false;

            FixedPrice fixedPrice = new FixedPrice();
            fixedPrice.price = price;
            fixedPrice.priceCurrency = new Currency();
            fixedPrice.priceCurrency.Value = currency;
            fixedPrice.priceUnit = new QuantityUnit();
            fixedPrice.priceUnit.Value = priceUnit;

            //fixedLeg.Items = new object[1];
            //fixedLeg.Items[0] = fixedPrice;

            ElectricityPhysicalLeg electrictypPhysicalLeg = new ElectricityPhysicalLeg();

            //string[] days = new string[] { "MON", "TUE", "WED", "THU", "FRI" };

            //fpml510ConfirmProcess.SettlementPeriods[] settlementPeriods = new fpml510ConfirmProcess.SettlementPeriods[1];

            SettlementPeriods settlementPeriod = new SettlementPeriods();
            settlementPeriod.duration = SettlementPeriodDurationEnum.Item30Minutes;
            settlementPeriod.applicableDay = new string[5] { "MON", "TUE", "WED", "THU", "FRI" };
            OffsetPrevailingTime start = new OffsetPrevailingTime();
            start.time = new PrevailingTime();
            start.time.hourMinuteTime = Convert.ToDateTime(new DateTime(2010, 1, 1, 7, 0, 0));
            start.time.location = new TimezoneLocation();
            start.time.location.Value = "Europe/London";
            settlementPeriod.startTime = start;

            OffsetPrevailingTime end = new OffsetPrevailingTime();
            end.time = new PrevailingTime();
            end.time.hourMinuteTime = Convert.ToDateTime(new DateTime(2010, 1, 1, 19, 0, 0));
            end.time.location = new TimezoneLocation();
            end.time.location.Value = "Europe/London";
            settlementPeriod.endTime = end;
            //settlementPeriod.Item = null;

            electrictypPhysicalLeg.settlementPeriods = new SettlementPeriods[1] { settlementPeriod };

            //var ep = new ElectricityProduct();

            ElectricityProduct electricityProduct = new ElectricityProduct();
            electricityProduct.type = ElectricityProductTypeEnum.Electricity;
            electrictypPhysicalLeg.electricity = electricityProduct;

            ElectricityDelivery electrDelivery = new ElectricityDelivery();

            //electrDelivery.Items = new object[1];
            //////this.electrictypPhysicalLeg.deliveryConditions = new fpml510ConfirmProcess.ElectricityDelivery();
            //////this.electrictypPhysicalLeg.deliveryConditions.Items = new object[1];
            CommodityDeliveryPoint deliveryPoint = new CommodityDeliveryPoint();
            deliveryPoint.deliveryPointScheme = "http://www.etso-net.org/Activities/edi/eic/ars/area.htm";
            deliveryPoint.Value = "10YGB----------A";
            electrictypPhysicalLeg.deliveryConditions = new ElectricityDelivery();
            electrictypPhysicalLeg.deliveryConditions = electrDelivery;

            //electrictypPhysicalLeg.deliveryConditions.Items = new object[1] { deliveryPoint };

            electrictypPhysicalLeg.deliveryConditions.electricityDeliveryPoint = deliveryPoint;

            electrictypPhysicalLeg.deliveryQuantity = new ElectricityPhysicalQuantity();
            //electrictypPhysicalLeg.deliveryQuantity.Items = new object[1];

            ElectricityPhysicalDeliveryQuantity physicalQuantity = new ElectricityPhysicalDeliveryQuantity();
            physicalQuantity.quantityUnit = new QuantityUnit();
            physicalQuantity.quantityUnit.Value = quantityUnit;
            physicalQuantity.quantityFrequency = new CommodityQuantityFrequency();
            physicalQuantity.quantityFrequency.Value = "PerSettlementPeriod";
            physicalQuantity.quantity = quantity;

            electrictypPhysicalLeg.deliveryQuantity.physicalQuantity = physicalQuantity;

            commoditySwap.fixedLeg = fixedLeg;
            commoditySwap.electricityPhysicalLeg = electrictypPhysicalLeg;
        }

        public string AsFPMLJsonified()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class FixedPriceLeg
    {
        private AdjustableDates calculationDatesField;

        private AdjustableDates calculationPeriodsField;

        private CommodityCalculationPeriodsSchedule calculationPeriodsScheduleField;

        private CalculationPeriodsReference calculationPeriodsReferenceField;

        private CalculationPeriodsScheduleReference calculationPeriodsScheduleReferenceField;

        private CalculationPeriodsDatesReference calculationPeriodsDatesReferenceField;

        private FixedPrice fixedPriceField;

        //private object[] itemsField;

        private NonNegativeMoney totalPriceField;

        //private object[] items1Field;

        private decimal totalNotionalQuantityField;

        private bool totalNotionalQuantityFieldSpecified;

        private QuantityReference quantityReferenceField;

        private CommodityRelativePaymentDates relativePaymentDatesField;

        private AdjustableDatesOrRelativeDateOffset paymentDatesField;

        private bool masterAgreementPaymentDatesField;

        private FlatRateEnum flatRateField;

        private NonNegativeMoney flatRateAmountField;

        /// <remarks/>
        public AdjustableDates calculationDates
        {
            get
            {
                return this.calculationDatesField;
            }
            set
            {
                this.calculationDatesField = value;
            }
        }

        /// <remarks/>
        public AdjustableDates calculationPeriods
        {
            get
            {
                return this.calculationPeriodsField;
            }
            set
            {
                this.calculationPeriodsField = value;
            }
        }

        /// <remarks/>
        public CommodityCalculationPeriodsSchedule calculationPeriodsSchedule
        {
            get
            {
                return this.calculationPeriodsScheduleField;
            }
            set
            {
                this.calculationPeriodsScheduleField = value;
            }
        }

        /// <remarks/>
        public CalculationPeriodsReference calculationPeriodsReference
        {
            get
            {
                return this.calculationPeriodsReferenceField;
            }
            set
            {
                this.calculationPeriodsReferenceField = value;
            }
        }

        /// <remarks/>
        public CalculationPeriodsScheduleReference calculationPeriodsScheduleReference
        {
            get
            {
                return this.calculationPeriodsScheduleReferenceField;
            }
            set
            {
                this.calculationPeriodsScheduleReferenceField = value;
            }
        }

        /// <remarks/>
        public CalculationPeriodsDatesReference calculationPeriodsDatesReference
        {
            get
            {
                return this.calculationPeriodsDatesReferenceField;
            }
            set
            {
                this.calculationPeriodsDatesReferenceField = value;
            }
        }

        ///// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute("contractRate", typeof(NonNegativeMoney))]
        //[System.Xml.Serialization.XmlElementAttribute("fixedPrice", typeof(FixedPrice))]
        //[System.Xml.Serialization.XmlElementAttribute("fixedPriceSchedule", typeof(CommodityFixedPriceSchedule))]
        //[System.Xml.Serialization.XmlElementAttribute("settlementPeriodsPrice", typeof(SettlementPeriodsFixedPrice))]
        //[System.Xml.Serialization.XmlElementAttribute("worldscaleRate", typeof(decimal))]
        //public object[] Items
        //{
        //    get
        //    {
        //        return this.itemsField;
        //    }
        //    set
        //    {
        //        this.itemsField = value;
        //    }
        //}


        public FixedPrice fixedPrice
        {
            get
            {
                return this.fixedPriceField;
            }
            set
            {
                this.fixedPriceField = value;
            }
        }

        /// <remarks/>
        public NonNegativeMoney totalPrice
        {
            get
            {
                return this.totalPriceField;
            }
            set
            {
                this.totalPriceField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute("notionalQuantity", typeof(CommodityNotionalQuantity))]
        //[System.Xml.Serialization.XmlElementAttribute("notionalQuantitySchedule", typeof(CommodityNotionalQuantitySchedule))]
        //[System.Xml.Serialization.XmlElementAttribute("settlementPeriodsNotionalQuantity", typeof(CommoditySettlementPeriodsNotionalQuantity))]
        //public object[] Items1
        //{
        //    get
        //    {
        //        return this.items1Field;
        //    }
        //    set
        //    {
        //        this.items1Field = value;
        //    }
        //}

        /// <remarks/>
        public decimal totalNotionalQuantity
        {
            get
            {
                return this.totalNotionalQuantityField;
            }
            set
            {
                this.totalNotionalQuantityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool totalNotionalQuantitySpecified
        {
            get
            {
                return this.totalNotionalQuantityFieldSpecified;
            }
            set
            {
                this.totalNotionalQuantityFieldSpecified = value;
            }
        }

        /// <remarks/>
        public QuantityReference quantityReference
        {
            get
            {
                return this.quantityReferenceField;
            }
            set
            {
                this.quantityReferenceField = value;
            }
        }

        /// <remarks/>
        public CommodityRelativePaymentDates relativePaymentDates
        {
            get
            {
                return this.relativePaymentDatesField;
            }
            set
            {
                this.relativePaymentDatesField = value;
            }
        }

        /// <remarks/>
        public AdjustableDatesOrRelativeDateOffset paymentDates
        {
            get
            {
                return this.paymentDatesField;
            }
            set
            {
                this.paymentDatesField = value;
            }
        }

        /// <remarks/>
        public bool masterAgreementPaymentDates
        {
            get
            {
                return this.masterAgreementPaymentDatesField;
            }
            set
            {
                this.masterAgreementPaymentDatesField = value;
            }
        }

        /// <remarks/>
        public FlatRateEnum flatRate
        {
            get
            {
                return this.flatRateField;
            }
            set
            {
                this.flatRateField = value;
            }
        }

        /// <remarks/>
        public NonNegativeMoney flatRateAmount
        {
            get
            {
                return this.flatRateAmountField;
            }
            set
            {
                this.flatRateAmountField = value;
            }
        }
    }


    public class ElectricityPhysicalQuantity : CommodityPhysicalQuantityBase
    {

        //private object[] itemsField;
        private ElectricityPhysicalDeliveryQuantity physicalQuantityField;
        private ElectricityPhysicalDeliveryQuantitySchedule ElectricityPhysicalDeliveryQuantitySchedule;
        private UnitQuantity totalPhysicalQuantityField;

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute("physicalQuantity", typeof(ElectricityPhysicalDeliveryQuantity))]
        //[System.Xml.Serialization.XmlElementAttribute("physicalQuantitySchedule", typeof(ElectricityPhysicalDeliveryQuantitySchedule))]
        //public object[] Items
        //{
        //    get
        //    {
        //        return this.itemsField;
        //    }
        //    set
        //    {
        //        this.itemsField = value;
        //    }
        //}

        public ElectricityPhysicalDeliveryQuantitySchedule PhysicalDeliveryQuantitySchedule
        {
            get
            {
                return this.ElectricityPhysicalDeliveryQuantitySchedule;
            }
            set
            {
                this.ElectricityPhysicalDeliveryQuantitySchedule = value;
            }
        }

        public ElectricityPhysicalDeliveryQuantity physicalQuantity
        {
            get
            {
                return this.physicalQuantityField;
            }
            set
            {
                this.physicalQuantityField = value;
            }
        }

        /// <remarks/>
        public UnitQuantity totalPhysicalQuantity
        {
            get
            {
                return this.totalPhysicalQuantityField;
            }
            set
            {
                this.totalPhysicalQuantityField = value;
            }
        }
    }


    public class CommoditySwap
    {
        public FixedPriceLeg fixedLeg { get; set; }
        public ElectricityPhysicalLeg electricityPhysicalLeg { get; set; }


        private AdjustableOrRelativeDate effectiveDateField;

        private AdjustableOrRelativeDate terminationDateField;

        private IdentifiedCurrency settlementCurrencyField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public AdjustableOrRelativeDate effectiveDate
        {
            get
            {
                return this.effectiveDateField;
            }
            set
            {
                this.effectiveDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public AdjustableOrRelativeDate terminationDate
        {
            get
            {
                return this.terminationDateField;
            }
            set
            {
                this.terminationDateField = value;
            }
        }


        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public IdentifiedCurrency settlementCurrency
        {
            get
            {
                return this.settlementCurrencyField;
            }
            set
            {
                this.settlementCurrencyField = value;
            }
        }
    }

    public class ElectricityPhysicalLeg : PhysicalSwapLeg
    {

        private CommodityDeliveryPeriods deliveryPeriodsField;

        private SettlementPeriods[] settlementPeriodsField;

        private SettlementPeriodsSchedule settlementPeriodsScheduleField;

        private LoadTypeEnum loadTypeField;

        private bool loadTypeFieldSpecified;

        private ElectricityProduct electricityField;

        private ElectricityDelivery deliveryConditionsField;

        private ElectricityPhysicalQuantity deliveryQuantityField;

        /// <remarks/>
        public CommodityDeliveryPeriods deliveryPeriods
        {
            get
            {
                return this.deliveryPeriodsField;
            }
            set
            {
                this.deliveryPeriodsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("settlementPeriods")]
        public SettlementPeriods[] settlementPeriods
        {
            get
            {
                return this.settlementPeriodsField;
            }
            set
            {
                this.settlementPeriodsField = value;
            }
        }

        /// <remarks/>
        public SettlementPeriodsSchedule settlementPeriodsSchedule
        {
            get
            {
                return this.settlementPeriodsScheduleField;
            }
            set
            {
                this.settlementPeriodsScheduleField = value;
            }
        }

        /// <remarks/>
        public LoadTypeEnum loadType
        {
            get
            {
                return this.loadTypeField;
            }
            set
            {
                this.loadTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool loadTypeSpecified
        {
            get
            {
                return this.loadTypeFieldSpecified;
            }
            set
            {
                this.loadTypeFieldSpecified = value;
            }
        }

        /// <remarks/>
        public ElectricityProduct electricity
        {
            get
            {
                return this.electricityField;
            }
            set
            {
                this.electricityField = value;
            }
        }

        /// <remarks/>
        public ElectricityDelivery deliveryConditions
        {
            get
            {
                return this.deliveryConditionsField;
            }
            set
            {
                this.deliveryConditionsField = value;
            }
        }

        /// <remarks/>
        public ElectricityPhysicalQuantity deliveryQuantity
        {
            get
            {
                return this.deliveryQuantityField;
            }
            set
            {
                this.deliveryQuantityField = value;
            }
        }
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.fpml.org/FpML-5/confirmation")]
    public partial class SettlementPeriods
    {

        private SettlementPeriodDurationEnum durationField;

        private string[] applicableDayField;

        private OffsetPrevailingTime startTimeField;

        private OffsetPrevailingTime endTimeField;

        private System.DateTime timeDurationField;

        private bool timeDurationFieldSpecified;

        private CommodityBusinessCalendar itemField;

        private ItemChoiceType4 itemElementNameField;

        private string idField;

        /// <remarks/>
        [JsonConverter(typeof(StringEnumConverter))]
        public SettlementPeriodDurationEnum duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("applicableDay")]
        public string[] applicableDay
        {
            get
            {
                return this.applicableDayField;
            }
            set
            {
                this.applicableDayField = value;
            }
        }

        /// <remarks/>
        public OffsetPrevailingTime startTime
        {
            get
            {
                return this.startTimeField;
            }
            set
            {
                this.startTimeField = value;
            }
        }

        /// <remarks/>
        public OffsetPrevailingTime endTime
        {
            get
            {
                return this.endTimeField;
            }
            set
            {
                this.endTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime timeDuration
        {
            get
            {
                return this.timeDurationField;
            }
            set
            {
                this.timeDurationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool timeDurationSpecified
        {
            get
            {
                return this.timeDurationFieldSpecified;
            }
            set
            {
                this.timeDurationFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("excludeHolidays", typeof(CommodityBusinessCalendar))]
        [System.Xml.Serialization.XmlElementAttribute("includeHolidays", typeof(CommodityBusinessCalendar))]
        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemElementName")]
        public CommodityBusinessCalendar Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public ItemChoiceType4 ItemElementName
        {
            get
            {
                return this.itemElementNameField;
            }
            set
            {
                this.itemElementNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "ID")]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.fpml.org/FpML-5/confirmation")]
    public enum SettlementPeriodDurationEnum
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("2Hours")]
        Item2Hours,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1Hour")]
        Item1Hour,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("30Minutes")]
        Item30Minutes,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("15Minutes")]
        Item15Minutes,
    }




    //[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    //[System.SerializableAttribute()]
    //[System.Diagnostics.DebuggerStepThroughAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.fpml.org/FpML-5/confirmation")]
    public class ElectricityDelivery
    {

        //private object[] itemsField;
        private CommodityDeliveryPoint electricityDeliveryPointField;
        ///// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute("deliveryPoint", typeof(ElectricityDeliveryPoint))]
        //[System.Xml.Serialization.XmlElementAttribute("deliveryType", typeof(ElectricityDeliveryType))]
        //[System.Xml.Serialization.XmlElementAttribute("deliveryZone", typeof(CommodityDeliveryPoint))]
        //[System.Xml.Serialization.XmlElementAttribute("electingPartyReference", typeof(PartyReference))]
        //[System.Xml.Serialization.XmlElementAttribute("interconnectionPoint", typeof(InterconnectionPoint))]
        //[System.Xml.Serialization.XmlElementAttribute("transmissionContingency", typeof(ElectricityTransmissionContingency))]

        //public object[] Items
        //{
        //    get
        //    {
        //        return this.itemsField;
        //    }
        //    set
        //    {
        //        this.itemsField = value;
        //    }
        //}

        public CommodityDeliveryPoint electricityDeliveryPoint
        {
            get
            {
                return this.electricityDeliveryPointField;
            }
            set
            {
                this.electricityDeliveryPointField = value;
            }
       }
    }


    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.fpml.org/FpML-5/confirmation")]
    public partial class ElectricityProduct
    {

        private ElectricityProductTypeEnum typeField;

        private decimal voltageField;

        private bool voltageFieldSpecified;

        /// <remarks/>
        [JsonConverter(typeof(StringEnumConverter))]
        public ElectricityProductTypeEnum type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public decimal voltage
        {
            get
            {
                return this.voltageField;
            }
            set
            {
                this.voltageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool voltageSpecified
        {
            get
            {
                return this.voltageFieldSpecified;
            }
            set
            {
                this.voltageFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    //[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    //[System.SerializableAttribute()]
    //[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.fpml.org/FpML-5/confirmation")]
    public enum ElectricityProductTypeEnum
    {

        /// <remarks/>
        Electricity,
    }


    //public class FixedLeg
    //{
    //    string payerPartyReference { get; set; }
    //    string receiverPartyReference { get; set; }
    //    SettlementPeriodsFixedPrice settlementPeriodsSchedule { get; set; }
    //    fpml510ConfirmProcess.CommodityCalculationPeriodsSchedule calculationPeriodsSchedule { get; set; }
    //    bool masterAgreementPaymentDates { get; set; }
    //    public FixedLeg() { }
    //    public FixedLeg(string payer, string receiver, SettlementPeriodsFixedPrice settPeriodFixedPrice, fpml510ConfirmProcess.CommodityCalculationPeriodsSchedule calcPeriodSchedule, bool paymentDatesAsPerMasterAgreement)
    //    {
    //        this.payerPartyReference = payer;
    //        this.receiverPartyReference = receiver;
    //        this.calculationPeriodsSchedule = calcPeriodSchedule;
    //        this.settlementPeriodsSchedule = settPeriodFixedPrice;
    //        this.masterAgreementPaymentDates = paymentDatesAsPerMasterAgreement;
    //    }
    //}
}

/*
 
    public class GTMA0510
    {
        //public fpml510ConfirmProcess.TradeHeader tradeHeader { get; set; }
        //public fpml510ConfirmProcess.CommoditySwap commoditySwap { get; set; }
        //public fpml510ConfirmProcess.FixedPriceLeg fixedLeg { get; set; }
        //public fpml510ConfirmProcess.ElectricityPhysicalLeg electrictypPhysicalLeg { get; set; }
        //public fpml510ConfirmProcess.FixedPrice fixedPrice { get; set; }
        //public fpml510ConfirmProcess.SettlementPeriods settlementPeriod { get; set; }
        //public fpml510ConfirmProcess.ElectricityProduct electricityProduct { get; set; }

        public TradeHeader tradeHeader { get; set; }
        public fpml510Main.CommoditySwap commoditySwap { get; set; }
        public FixedPriceLeg fixedLeg { get; set; }
        public ElectricityPhysicalLeg electrictypPhysicalLeg { get; set; }
        public FixedPrice fixedPrice { get; set; }
        public SettlementPeriods settlementPeriod { get; set; }
        public ElectricityProduct electricityProduct { get; set; }
        public GTMA0510()
        {

            //error CS0030: Cannot convert type 'fpml510ConfirmProcess.SettlementPeriodsReference[]' to 'fpml510ConfirmProcess.SettlementPeriodsReference'
            //error CS0029: Cannot implicitly convert type 'fpml510ConfirmProcess.SettlementPeriodsReference' to 'fpml510ConfirmProcess.SettlementPeriodsReference[]''
            //this.tradeHeader = new fpml510ConfirmProcess.TradeHeader();
            this.tradeHeader = new TradeHeader();
            //this.commoditySwap = new fpml510ConfirmProcess.CommoditySwap();
            //this.settlementPeriod = new fpml510ConfirmProcess.SettlementPeriods();
            this.commoditySwap = new fpml510Main.CommoditySwap();
            this.settlementPeriod = new SettlementPeriods();
            SettlementPeriodsReference[] settlementPeriodsReferences = new SettlementPeriodsReference[0];
        }
        public GTMA0510(string currency, decimal price, string priceUnit, decimal quantity, string quantityUnit)
        {
            this.tradeHeader = new TradeHeader();

            //this.commoditySwap = new fpml510ConfirmProcess.CommoditySwap();
            this.commoditySwap = new fpml510Main.CommoditySwap();
            this.commoditySwap.settlementCurrency = new IdentifiedCurrency();
            this.commoditySwap.settlementCurrency.Value = currency;

            //this.commoditySwap.
            //this.commoditySwap.commoditySwapDetails.fixedLeg
                 
            this.commoditySwap.Items = new CommoditySwapLeg[2];

            this.fixedLeg = new FixedPriceLeg();
            fixedLeg.calculationPeriodsSchedule = new CommodityCalculationPeriodsSchedule();
            fixedLeg.calculationPeriodsSchedule.periodMultiplier = "1";
            fixedLeg.calculationPeriodsSchedule.period = "M";
            fixedLeg.calculationPeriodsSchedule.balanceOfFirstPeriod = false;

            this.fixedPrice = new FixedPrice();
            fixedPrice.price = price;
            fixedPrice.priceCurrency = new Currency();
            fixedPrice.priceCurrency.Value = currency;
            fixedPrice.priceUnit = new QuantityUnit();
            fixedPrice.priceUnit.Value = priceUnit;

            fixedLeg.Items = new object[1];
            fixedLeg.Items[0] = fixedPrice;

            this.electrictypPhysicalLeg = new ElectricityPhysicalLeg();

            //string[] days = new string[] { "MON", "TUE", "WED", "THU", "FRI" };

            //fpml510ConfirmProcess.SettlementPeriods[] settlementPeriods = new fpml510ConfirmProcess.SettlementPeriods[1];

            this.settlementPeriod = new SettlementPeriods();
            settlementPeriod.applicableDay = new string[5] { "MON", "TUE", "WED", "THU", "FRI" };
            //settlementPeriods[0].applicableDay = days;
            OffsetPrevailingTime start = new OffsetPrevailingTime();
            start.time = new PrevailingTime();
            start.time.hourMinuteTime = Convert.ToDateTime(new DateTime(2010, 1, 1, 7, 0, 0));
            start.time.location = new TimezoneLocation();
            start.time.location.Value = "Europe/London";
            settlementPeriod.startTime = start;

            OffsetPrevailingTime end = new OffsetPrevailingTime();
            end.time = new PrevailingTime();
            end.time.hourMinuteTime = Convert.ToDateTime(new DateTime(2010, 1, 1, 19, 0, 0));
            end.time.location = new TimezoneLocation();
            end.time.location.Value = "Europe/London";
            settlementPeriod.endTime = end;
            this.electrictypPhysicalLeg.settlementPeriods = new SettlementPeriods[1] { settlementPeriod };
            var ep = new ElectricityProduct();

            this.electricityProduct = new ElectricityProduct();
            electricityProduct.type = ElectricityProductTypeEnum.Electricity;
            this.electrictypPhysicalLeg.electricity = electricityProduct;

            ElectricityDelivery electrDelivery = new ElectricityDelivery();
            electrDelivery.Items = new object[1];
            //////this.electrictypPhysicalLeg.deliveryConditions = new fpml510ConfirmProcess.ElectricityDelivery();
            //////this.electrictypPhysicalLeg.deliveryConditions.Items = new object[1];
            CommodityDeliveryPoint deliveryPoint = new CommodityDeliveryPoint();
            deliveryPoint.deliveryPointScheme = "http://www.etso-net.org/Activities/edi/eic/ars/area.htm";
            deliveryPoint.Value = "10YGB----------A";
            this.electrictypPhysicalLeg.deliveryConditions = new ElectricityDelivery();
            this.electrictypPhysicalLeg.deliveryConditions = electrDelivery;
            this.electrictypPhysicalLeg.deliveryConditions.Items = new object[1] { deliveryPoint };

            this.electrictypPhysicalLeg.deliveryQuantity = new ElectricityPhysicalQuantity();
            this.electrictypPhysicalLeg.deliveryQuantity.Items = new object[1];
            ElectricityPhysicalDeliveryQuantity physicalQuantity = new ElectricityPhysicalDeliveryQuantity();
            physicalQuantity.quantityUnit = new QuantityUnit();
            physicalQuantity.quantityUnit.Value = quantityUnit;
            physicalQuantity.quantityFrequency = new CommodityQuantityFrequency();
            physicalQuantity.quantityFrequency.Value = "PerSettlementPeriod";
            physicalQuantity.quantity = quantity;

            this.electrictypPhysicalLeg.deliveryQuantity.Items[0] = physicalQuantity;

            this.commoditySwap.Items = new CommoditySwapLeg[2] { fixedLeg, electrictypPhysicalLeg };
        }

        public string AsFPMLJsonified()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    //public class FixedLeg
    //{
    //    string payerPartyReference { get; set; }
    //    string receiverPartyReference { get; set; }
    //    SettlementPeriodsFixedPrice settlementPeriodsSchedule { get; set; }
    //    fpml510ConfirmProcess.CommodityCalculationPeriodsSchedule calculationPeriodsSchedule { get; set; }
    //    bool masterAgreementPaymentDates { get; set; }
    //    public FixedLeg() { }
    //    public FixedLeg(string payer, string receiver, SettlementPeriodsFixedPrice settPeriodFixedPrice, fpml510ConfirmProcess.CommodityCalculationPeriodsSchedule calcPeriodSchedule, bool paymentDatesAsPerMasterAgreement)
    //    {
    //        this.payerPartyReference = payer;
    //        this.receiverPartyReference = receiver;
    //        this.calculationPeriodsSchedule = calcPeriodSchedule;
    //        this.settlementPeriodsSchedule = settPeriodFixedPrice;
    //        this.masterAgreementPaymentDates = paymentDatesAsPerMasterAgreement;
    //    }
    //}
     
     */
