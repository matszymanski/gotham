﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Gotham.Core;
using System.Collections.Generic;
using Gotham.Core.Web;

namespace Gotham.Tests.Core
{
    [TestClass]
    public class TradeColumnComparerTest
    {
        [TestMethod]
        public void DefinedColumnsPrecedeOthers()
        {
            var order = new Dictionary<string, int>();
            order.Add("ConfirmationViewId", 1);
            var columnComparer = new ConfirmationColumnComparer(order);

            var compare = columnComparer.Compare("ConfirmationViewId", "RandomColumn");

            Assert.AreEqual(-1, compare);

            compare = columnComparer.Compare("RandomColumn", "ConfirmationViewId");

            Assert.AreEqual(1, compare);
        }
        

        [TestMethod]
        public void SameOrderIsTreatedAlphabetically()
        {
            var order = new Dictionary<string, int>();
            order.Add("ConfirmationViewId", 1);
            order.Add("Status", 1);
            var columnComparer = new ConfirmationColumnComparer(order);

            var compare = columnComparer.Compare("Status", "ConfirmationViewId");

            Assert.AreEqual(1, compare);
        }

        [TestMethod]
        public void UndefinedIsTreatedAlphabetically()
        {
            var order = new Dictionary<string, int>();
            var columnComparer = new ConfirmationColumnComparer(order);

            var compare = columnComparer.Compare("Status", "ConfirmationViewId");

            Assert.AreEqual(1, compare);
        }
    }
}
