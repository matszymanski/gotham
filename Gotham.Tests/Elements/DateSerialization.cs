﻿using System;
using Gotham.Elements;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace Gotham.Tests.Elements
{
    [TestClass]
    public class DateSerialization
    {
        [TestMethod]
        public void CanSerialize()
        {
            var d = new Date("2019-01-01");
            var json = JsonConvert.SerializeObject(d);
            Assert.IsTrue(json == "\"2019-01-01\"");
        }

        [TestMethod]
        public void CanDeserialize()
        {
            var d = new Date("2019-01-01");
            var date = JsonConvert.DeserializeObject<Date>("\"2019-01-01\"");
            Assert.IsTrue(d == date);
        }
    }
}
