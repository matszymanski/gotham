﻿namespace Gotham.ElementsUI.Templates
{
    partial class SchemaBuilder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SchemaBuilder));
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.schemaBuilderControl = new System.Windows.Forms.Panel();
            this.btnDeleteElement = new System.Windows.Forms.Button();
            this.txtSchemaID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSchemaName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnUpdateElement = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnAddElement = new System.Windows.Forms.Button();
            this.txtRootSchema = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvSchemaElements = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.schemaBuilderControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSchemaElements)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.schemaBuilderControl);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.dgvSchemaElements);
            this.splitContainer.Size = new System.Drawing.Size(1514, 861);
            this.splitContainer.SplitterDistance = 205;
            this.splitContainer.SplitterWidth = 6;
            this.splitContainer.TabIndex = 0;
            // 
            // schemaBuilderControl
            // 
            this.schemaBuilderControl.AutoScroll = true;
            this.schemaBuilderControl.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.schemaBuilderControl.Controls.Add(this.btnDeleteElement);
            this.schemaBuilderControl.Controls.Add(this.txtSchemaID);
            this.schemaBuilderControl.Controls.Add(this.label3);
            this.schemaBuilderControl.Controls.Add(this.txtSchemaName);
            this.schemaBuilderControl.Controls.Add(this.label2);
            this.schemaBuilderControl.Controls.Add(this.btnUpdateElement);
            this.schemaBuilderControl.Controls.Add(this.btnClose);
            this.schemaBuilderControl.Controls.Add(this.btnAddElement);
            this.schemaBuilderControl.Controls.Add(this.txtRootSchema);
            this.schemaBuilderControl.Controls.Add(this.label1);
            this.schemaBuilderControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.schemaBuilderControl.Location = new System.Drawing.Point(0, 0);
            this.schemaBuilderControl.Margin = new System.Windows.Forms.Padding(4);
            this.schemaBuilderControl.Name = "schemaBuilderControl";
            this.schemaBuilderControl.Size = new System.Drawing.Size(1514, 205);
            this.schemaBuilderControl.TabIndex = 1;
            // 
            // btnDeleteElement
            // 
            this.btnDeleteElement.Location = new System.Drawing.Point(1164, 16);
            this.btnDeleteElement.Margin = new System.Windows.Forms.Padding(4);
            this.btnDeleteElement.Name = "btnDeleteElement";
            this.btnDeleteElement.Size = new System.Drawing.Size(296, 44);
            this.btnDeleteElement.TabIndex = 9;
            this.btnDeleteElement.Text = "Delete Element";
            this.btnDeleteElement.UseVisualStyleBackColor = true;
            this.btnDeleteElement.Click += new System.EventHandler(this.btnDeleteElement_Click);
            // 
            // txtSchemaID
            // 
            this.txtSchemaID.Enabled = false;
            this.txtSchemaID.Location = new System.Drawing.Point(266, 16);
            this.txtSchemaID.Margin = new System.Windows.Forms.Padding(4);
            this.txtSchemaID.Name = "txtSchemaID";
            this.txtSchemaID.ReadOnly = true;
            this.txtSchemaID.Size = new System.Drawing.Size(471, 30);
            this.txtSchemaID.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(55, 19);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 25);
            this.label3.TabIndex = 7;
            this.label3.Text = "Schema ID:";
            // 
            // txtSchemaName
            // 
            this.txtSchemaName.Enabled = false;
            this.txtSchemaName.Location = new System.Drawing.Point(266, 122);
            this.txtSchemaName.Margin = new System.Windows.Forms.Padding(4);
            this.txtSchemaName.Name = "txtSchemaName";
            this.txtSchemaName.ReadOnly = true;
            this.txtSchemaName.Size = new System.Drawing.Size(471, 30);
            this.txtSchemaName.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 127);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "Schema Name:";
            // 
            // btnUpdateElement
            // 
            this.btnUpdateElement.Location = new System.Drawing.Point(823, 83);
            this.btnUpdateElement.Margin = new System.Windows.Forms.Padding(4);
            this.btnUpdateElement.Name = "btnUpdateElement";
            this.btnUpdateElement.Size = new System.Drawing.Size(296, 44);
            this.btnUpdateElement.TabIndex = 4;
            this.btnUpdateElement.Text = "Update Element";
            this.btnUpdateElement.UseVisualStyleBackColor = true;
            this.btnUpdateElement.Click += new System.EventHandler(this.btnUpdateElement_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(1164, 83);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(296, 44);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnAddElement
            // 
            this.btnAddElement.Location = new System.Drawing.Point(823, 16);
            this.btnAddElement.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddElement.Name = "btnAddElement";
            this.btnAddElement.Size = new System.Drawing.Size(296, 44);
            this.btnAddElement.TabIndex = 2;
            this.btnAddElement.Text = "Add Element";
            this.btnAddElement.UseVisualStyleBackColor = true;
            this.btnAddElement.Click += new System.EventHandler(this.btnAddElement_Click);
            // 
            // txtRootSchema
            // 
            this.txtRootSchema.Enabled = false;
            this.txtRootSchema.Location = new System.Drawing.Point(266, 67);
            this.txtRootSchema.Margin = new System.Windows.Forms.Padding(4);
            this.txtRootSchema.Name = "txtRootSchema";
            this.txtRootSchema.ReadOnly = true;
            this.txtRootSchema.Size = new System.Drawing.Size(471, 30);
            this.txtRootSchema.TabIndex = 100;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 70);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Root Schema:";
            // 
            // dgvSchemaElements
            // 
            this.dgvSchemaElements.AllowUserToAddRows = false;
            this.dgvSchemaElements.AllowUserToDeleteRows = false;
            this.dgvSchemaElements.AllowUserToResizeRows = false;
            this.dgvSchemaElements.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSchemaElements.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvSchemaElements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSchemaElements.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSchemaElements.Location = new System.Drawing.Point(0, 0);
            this.dgvSchemaElements.Margin = new System.Windows.Forms.Padding(4);
            this.dgvSchemaElements.Name = "dgvSchemaElements";
            this.dgvSchemaElements.ReadOnly = true;
            this.dgvSchemaElements.RowHeadersVisible = false;
            this.dgvSchemaElements.RowTemplate.Height = 28;
            this.dgvSchemaElements.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSchemaElements.Size = new System.Drawing.Size(1514, 650);
            this.dgvSchemaElements.TabIndex = 0;
            // 
            // SchemaBuilder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1514, 861);
            this.Controls.Add(this.splitContainer);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "SchemaBuilder";
            this.Text = "Schema Editor";
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.schemaBuilderControl.ResumeLayout(false);
            this.schemaBuilderControl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSchemaElements)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.DataGridView dgvSchemaElements;
        private System.Windows.Forms.Panel schemaBuilderControl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtRootSchema;
        private System.Windows.Forms.Button btnUpdateElement;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnAddElement;
        private System.Windows.Forms.TextBox txtSchemaName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSchemaID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnDeleteElement;
    }
}