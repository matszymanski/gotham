﻿using Gotham.ElementsUI.UIUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gotham.ElementsUI.Templates
{
    public partial class TemplateUpload : Form
    {
        private ErrorProvider ep = new ErrorProvider();
        public Int32 SelectedTemplateID { get; set; }
        public string TemplateName { get; set; }
        public bool IsCancel { get; set; }

        public TemplateUpload(string name)
        {
            InitializeComponent();
            this.InitialiseView();
            this.txtTemplateName.Text = name;
            this.TemplateName = name;
            this.IsCancel = false;
            this.ep = new ErrorProvider();
        }

        private void InitialiseView()
        {
            LookAndFeel.SetFormStatus(this);
            this.txtTemplateID.ReadOnly = true;
            this.SelectedTemplateID = Int32.MinValue;
        }
        private void btnGo_Click(object sender, EventArgs e)
        {
            if (this.ValidateContent())
            {
                this.Close();
            }
        }

        private bool ValidateContent()
        {
            if(!string.IsNullOrEmpty(this.txtTemplateName.Text) &&  this.IsValidFilename(this.txtTemplateName.Text))
            {
                return true;
            }

            ep.SetError(this.txtTemplateName, "Please enter a valid filename!");
            return false;
        }

        private void chkReplacing_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chkReplacing.Checked)
            {
                TemplatePicker picker = new TemplatePicker();

                picker.ShowDialog();

                if (picker.TemplateID > 0)
                {
                    this.txtTemplateID.Text = picker.TemplateID.ToString();
                    this.txtTemplateID.ReadOnly = true;
                    this.txtTemplateID.Enabled = false;
                    this.txtOldName.Text = picker.TemplateName;
                    this.txtOldName.Enabled = false;
                    this.SelectedTemplateID = picker.TemplateID;
                }
            }
            else
            {
                this.txtTemplateID.Text = string.Empty;
                this.txtOldName.Text = string.Empty;
                this.txtOldName.ReadOnly = true;
                this.txtOldName.Enabled = false;
                this.txtTemplateID.ReadOnly = true;
                this.txtTemplateID.Enabled = false;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.TemplateName = string.Empty;
            this.SelectedTemplateID = Int32.MinValue;
            this.IsCancel = true;
            this.Close();
        }

        public bool IsValidFilename(string testName)
        {
            Regex containsABadCharacter = new Regex("[" + Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars())) + "]");
            if (containsABadCharacter.IsMatch(testName)) { return false; };
            return true;
        }

        private void txtTemplateName_TextChanged(object sender, EventArgs e)
        {
            this.TemplateName = this.txtTemplateName.Text;
        }
    }
}
