﻿using Gotham.Data;
using Gotham.ElementsUI.UIUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gotham.ElementsUI.Templates
{
    public partial class SchemaPicker : Form
    {
        public Int32 SelectedSchemaID { get; set; }
        public string SchemaName { get; set; }
        public bool? IsNew { get; set; }
        public SchemaPicker(bool isNew)
        {
            InitializeComponent();
            this.IsNew = isNew;
            this.InitialiseView();
        }

        public SchemaPicker()
        {
            InitializeComponent();
            this.IsNew = null;
            this.InitialiseView();
        }

        private void InitialiseView()
        {
            LookAndFeel.SetFormStatus(this);
            this.FillComboBoxes();
            this.SetVisibility();
        }

        public void SetVisibility()
        {
            if (this.IsNew.HasValue)
            {
                this.txtSchemaName.Visible = IsNew.Value;
                this.lblSchemaName.Visible = IsNew.Value;
                this.lblSchemaType.Text = (IsNew.Value == true) ? "Root Schema" : "Custom Schema";
            }
            else
            {
                this.lblSchemaName.Visible = false;
                this.txtSchemaName.Visible = false;
            }
        }

        private void FillComboBoxes()
        {
            DataTable rootSchemas = ETRMDataAccess.GetGothamRootSchemas(this.IsNew);
            this.cbSelectedRootSchema.DataSource = rootSchemas.DefaultView;
            this.cbSelectedRootSchema.DisplayMember = TemplateConstants.SchemaName;
            this.cbSelectedRootSchema.ValueMember = TemplateConstants.SchemaID;
            this.cbSelectedRootSchema.BindingContext = this.BindingContext;
            this.cbSelectedRootSchema.SelectedValue = -1;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.SelectedSchemaID = Convert.ToInt32(this.cbSelectedRootSchema.SelectedValue);

            if (this.IsNew.HasValue)
            {
                if (this.IsNew.Value)
                {
                    this.SchemaName = this.txtSchemaName.Text;
                }
                else
                {
                    this.SchemaName = this.cbSelectedRootSchema.Text;
                }
            }
            else
            {
                this.SchemaName = this.cbSelectedRootSchema.Text;
            }

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.SelectedSchemaID = -1;
            this.Close();
        }
    }
}
