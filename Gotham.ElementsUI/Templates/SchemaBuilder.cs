﻿using Gotham.Data;
using Gotham.ElementsUI.UIUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gotham.ElementsUI.Templates
{
    public partial class SchemaBuilder : Form
    {
        private DataTable SchemaDefinition { get; set; }
        private Int32 SchemaID { get; set; }
        private string SchemaName { get; set; }

        public SchemaBuilder()
        {
            InitializeComponent();
        }
        public SchemaBuilder(Int32 newSchemaID)
        {
            InitializeComponent();
            this.SchemaID = newSchemaID;
        }
        public SchemaBuilder(DataTable dt, Int32 schemaID, string schemaName)
        {
            InitializeComponent();
            this.SchemaDefinition = dt;
            this.SchemaID = schemaID;
            this.SchemaName = schemaName;
            this.FillSchemaDefinition();
            this.InitialiseView();
        }
        private void InitialiseView()
        {
            LookAndFeel.SetFormStatus(this);

            this.txtSchemaID.Text = this.SchemaID.ToString();
            this.txtSchemaName.Text = this.SchemaName;

            if (this.dgvSchemaElements.DataSource != null)
            {
                try
                {
                    //string rootSchema = Convert.ToString(this.dgvSchemaElements.Rows[0].Cells[Gotham.ElementsUI.Templates.SchemaDefinition.RootSchemaName].Value);

                    string rootSchema = ETRMDataAccess.GetRootSchema(this.SchemaID);

                    this.txtRootSchema.Text = rootSchema.Equals("ROOT") ? "" : rootSchema;
                }
                catch (Exception ex)
                {
                }
            }
        }
        private void FillSchemaDefinition()
        {
            this.dgvSchemaElements.DataSource = this.SchemaDefinition;
            this.dgvSchemaElements.Columns.Cast<DataGridViewColumn>().ToList().ForEach(f => f.SortMode = DataGridViewColumnSortMode.NotSortable);

            this.dgvSchemaElements.Columns[Gotham.ElementsUI.Templates.SchemaDefinition.SchemaID].Visible = false;
            this.dgvSchemaElements.Columns[Gotham.ElementsUI.Templates.SchemaDefinition.KeyID].Visible = false;
            this.dgvSchemaElements.Columns[Gotham.ElementsUI.Templates.SchemaDefinition.FieldTypeID].Visible = false;
            this.dgvSchemaElements.Columns[Gotham.ElementsUI.Templates.SchemaDefinition.IsFixed].Visible = false;
            this.dgvSchemaElements.Columns[Gotham.ElementsUI.Templates.SchemaDefinition.RootSchemaName].Visible = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddElement_Click(object sender, EventArgs e)
        {
            SchemaElement se = new SchemaElement(this.SchemaID);
            se.ShowDialog();
            this.RefreshSchema();
        }

        private void btnUpdateElement_Click(object sender, EventArgs e)
        {
            Int32 selected = this.dgvSchemaElements.CurrentCell.RowIndex;

            if (selected < 0)
            {
                MessageBox.Show("Please select one row");
                return;
            }
            if (this.dgvSchemaElements.SelectedRows.Count > 1)
            {
                MessageBox.Show("Please select a row only");
                return;
            }

            Int32 elementID = Convert.ToInt32(this.dgvSchemaElements.SelectedRows[0].Cells[Gotham.ElementsUI.Templates.SchemaDefinition.KeyID].Value);
            bool isFixed = Convert.ToBoolean(this.dgvSchemaElements.SelectedRows[0].Cells[Gotham.ElementsUI.Templates.SchemaDefinition.IsFixed].Value);

            if (isFixed)
            {
                MessageBox.Show("You cannot edit root schema elements!");
                return;
            }

            DataTable element = ETRMDataAccess.GetSchemaElement(this.SchemaID, elementID);
            SchemaElement se = new SchemaElement(element);
            se.ShowDialog();
            this.RefreshSchema();
        }

        private void btnDeleteElement_Click(object sender, EventArgs e)
        {
            Int32 selected = this.dgvSchemaElements.CurrentCell.RowIndex;

            if (selected < 0)
            {
                MessageBox.Show("Please select one row");
                return;
            }
            if (this.dgvSchemaElements.SelectedRows.Count > 1)
            {
                MessageBox.Show("Please select one row only");
                return;
            }

            Int32 elementID = Convert.ToInt32(this.dgvSchemaElements.SelectedRows[0].Cells[Gotham.ElementsUI.Templates.SchemaDefinition.KeyID].Value);
            bool isFixed = Convert.ToBoolean(this.dgvSchemaElements.SelectedRows[0].Cells[Gotham.ElementsUI.Templates.SchemaDefinition.IsFixed].Value);
            string tag = Convert.ToString(this.dgvSchemaElements.SelectedRows[0].Cells[Gotham.ElementsUI.Templates.SchemaDefinition.Tag].Value);

            if (isFixed)
            {
                MessageBox.Show("You cannot delete root schema elements!");
                return;
            }

            DialogResult dialogResult = MessageBox.Show(string.Format("Are you sure you want to remove element {0} from this schema?", tag), "Schema Element Deletion", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                if (ETRMDataAccess.DeleteSchemaElement(this.SchemaID, elementID))
                {
                    MessageBox.Show("Element deleted");
                }
                else
                {
                    MessageBox.Show("Cannot delete schema element. Please contact support");
                }
            }

            this.RefreshSchema();
        }

        private void RefreshSchema()
        {
            this.SchemaDefinition = ETRMDataAccess.GetGothamSchemaDefinition(this.SchemaID);
            this.FillSchemaDefinition();
        }
    }
}
