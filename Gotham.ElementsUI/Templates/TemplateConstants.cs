﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.ElementsUI.Templates
{
    public static class TemplateConstants
    {
        public static string SchemaID = "SchemaID";
        public static string SchemaName = "SchemaName";
        public static string TemplateID = "TemplateID";
        public static string TemplateName = "TemplateName";
        public static string Template = "Template";
    }
    public static class Field
    {
        public static string FieldTypeID = "FieldTypeID";
        public static string FieldType = "FieldType";
    }

    public static class UserItems
    {
        public static string ViewTypeID = "ViewTypeID";
        public static string ViewTypeName = "ViewTypeName";
    }
    public static class SchemaDefinition
    {
        public static string SchemaID = "SchemaID";
        public static string KeyID = "KeyID";
        public static string Key = "Key";
        public static string Tag = "Tag";
        public static string FieldTypeID = "FieldTypeID";
        public static string IsFixed = "IsFixed";
        public static string RootSchemaName = "RootSchemaName";
        public static string Columns = "Columns";
    }

    public static class FType
    {
        public static Int32 Fixed = 1;
        public static Int32 Table = 2;
    }

}
