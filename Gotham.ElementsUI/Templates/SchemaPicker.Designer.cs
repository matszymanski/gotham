﻿namespace Gotham.ElementsUI.Templates
{
    partial class SchemaPicker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SchemaPicker));
            this.lblSchemaType = new System.Windows.Forms.Label();
            this.cbSelectedRootSchema = new System.Windows.Forms.ComboBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblSchemaName = new System.Windows.Forms.Label();
            this.txtSchemaName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblSchemaType
            // 
            this.lblSchemaType.AutoSize = true;
            this.lblSchemaType.Location = new System.Drawing.Point(51, 64);
            this.lblSchemaType.Name = "lblSchemaType";
            this.lblSchemaType.Size = new System.Drawing.Size(107, 20);
            this.lblSchemaType.TabIndex = 0;
            this.lblSchemaType.Text = "Root Schema";
            // 
            // cbSelectedRootSchema
            // 
            this.cbSelectedRootSchema.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSelectedRootSchema.FormattingEnabled = true;
            this.cbSelectedRootSchema.Location = new System.Drawing.Point(346, 64);
            this.cbSelectedRootSchema.Name = "cbSelectedRootSchema";
            this.cbSelectedRootSchema.Size = new System.Drawing.Size(626, 28);
            this.cbSelectedRootSchema.TabIndex = 1;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(55, 245);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(126, 48);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(846, 245);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(126, 48);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblSchemaName
            // 
            this.lblSchemaName.AutoSize = true;
            this.lblSchemaName.Location = new System.Drawing.Point(51, 142);
            this.lblSchemaName.Name = "lblSchemaName";
            this.lblSchemaName.Size = new System.Drawing.Size(114, 20);
            this.lblSchemaName.TabIndex = 4;
            this.lblSchemaName.Text = "Schema Name";
            // 
            // txtSchemaName
            // 
            this.txtSchemaName.Location = new System.Drawing.Point(346, 135);
            this.txtSchemaName.Name = "txtSchemaName";
            this.txtSchemaName.Size = new System.Drawing.Size(626, 26);
            this.txtSchemaName.TabIndex = 5;
            // 
            // SchemaPicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1041, 389);
            this.Controls.Add(this.txtSchemaName);
            this.Controls.Add(this.lblSchemaName);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.cbSelectedRootSchema);
            this.Controls.Add(this.lblSchemaType);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SchemaPicker";
            this.Text = "Choose Root Schema";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSchemaType;
        private System.Windows.Forms.ComboBox cbSelectedRootSchema;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblSchemaName;
        private System.Windows.Forms.TextBox txtSchemaName;
    }
}