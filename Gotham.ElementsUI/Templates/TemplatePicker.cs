﻿using Gotham.Data;
using Gotham.ElementsUI.UIUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gotham.ElementsUI.Templates
{
    public partial class TemplatePicker : Form
    {
        public Int32 TemplateID { get; set; }
        public string TemplateName { get; set;}
        public TemplatePicker()
        {
            InitializeComponent();
            this.InitialiseView();
        }

        private void InitialiseView()
        {
            LookAndFeel.SetFormStatus(this);
            this.FillComboBox();
        }

        private void FillComboBox()
        {
            DataTable templates = ETRMDataAccess.GetGTMTemplates();

            if(!ETRMDataAccess.IsEmpty(templates))
            {
                this.cbTemplatePicker.DataSource = templates.DefaultView;
                this.cbTemplatePicker.DisplayMember = TemplateConstants.TemplateName;
                this.cbTemplatePicker.ValueMember = TemplateConstants.TemplateID;
                this.cbTemplatePicker.BindingContext = this.BindingContext;
                this.cbTemplatePicker.SelectedValue = -1;
            }

            this.cbTemplatePicker.SelectedValue = -1;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.TemplateID = Int32.MinValue;
            this.TemplateName = string.Empty;

            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.TemplateID = Convert.ToInt32(this.cbTemplatePicker.SelectedValue);
            this.TemplateName = this.cbTemplatePicker.Text;

            this.Close();
        }
    }
}
