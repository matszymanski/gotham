﻿namespace Gotham.ElementsUI.Templates
{
    partial class SchemaElement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SchemaElement));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtFieldTag = new System.Windows.Forms.TextBox();
            this.cbFieldType = new System.Windows.Forms.ComboBox();
            this.lblColumns = new System.Windows.Forms.Label();
            this.nudColumnsCounter = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nudColumnsCounter)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Element Tag:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 96);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Element Type:";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(44, 235);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(130, 35);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(610, 224);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(130, 35);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtFieldTag
            // 
            this.txtFieldTag.Location = new System.Drawing.Point(214, 23);
            this.txtFieldTag.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtFieldTag.Name = "txtFieldTag";
            this.txtFieldTag.Size = new System.Drawing.Size(525, 26);
            this.txtFieldTag.TabIndex = 4;
            // 
            // cbFieldType
            // 
            this.cbFieldType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFieldType.FormattingEnabled = true;
            this.cbFieldType.Location = new System.Drawing.Point(214, 81);
            this.cbFieldType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbFieldType.Name = "cbFieldType";
            this.cbFieldType.Size = new System.Drawing.Size(525, 28);
            this.cbFieldType.TabIndex = 5;
            this.cbFieldType.SelectedIndexChanged += new System.EventHandler(this.cbFieldType_SelectedIndexChanged);
            // 
            // lblColumns
            // 
            this.lblColumns.AutoSize = true;
            this.lblColumns.Location = new System.Drawing.Point(39, 155);
            this.lblColumns.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblColumns.Name = "lblColumns";
            this.lblColumns.Size = new System.Drawing.Size(75, 20);
            this.lblColumns.TabIndex = 6;
            this.lblColumns.Text = "Columns:";
            // 
            // nudColumnsCounter
            // 
            this.nudColumnsCounter.Location = new System.Drawing.Point(214, 153);
            this.nudColumnsCounter.Name = "nudColumnsCounter";
            this.nudColumnsCounter.Size = new System.Drawing.Size(120, 26);
            this.nudColumnsCounter.TabIndex = 8;
            // 
            // SchemaElement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 316);
            this.Controls.Add(this.nudColumnsCounter);
            this.Controls.Add(this.lblColumns);
            this.Controls.Add(this.cbFieldType);
            this.Controls.Add(this.txtFieldTag);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SchemaElement";
            this.Text = "Schema Element";
            ((System.ComponentModel.ISupportInitialize)(this.nudColumnsCounter)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtFieldTag;
        private System.Windows.Forms.ComboBox cbFieldType;
        private System.Windows.Forms.Label lblColumns;
        private System.Windows.Forms.NumericUpDown nudColumnsCounter;
    }
}