﻿using Gotham.Data;
using Gotham.ElementsUI.UIUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gotham.ElementsUI.Templates
{
    public partial class SchemaElement : Form
    {
        private Int32 SchemaID { get; set; }
        private Int32 KeyID { get; set; }
        private DataTable Schema { get; set; }
        private bool IsNew { get; set; }
        private bool IsColumn { get; set; }

        public SchemaElement(Int32 schemaID)
        {
            InitializeComponent();
            LookAndFeel.SetFormStatus(this);
            this.SchemaID = schemaID;
            this.FillFieldTypes();
            this.IsNew = true;
        }

        public SchemaElement(DataTable element)
        {
            InitializeComponent();
            LookAndFeel.SetFormStatus(this);
            this.Schema = element;
            this.FillFieldTypes();
            this.FillElement();
            this.IsNew = false;
        }

        private void FillElement()
        {
            if (ETRMDataAccess.IsEmpty(this.Schema)) { return; }
            this.SchemaID = ETRMDataAccess.GetInt32DTRElement(this.Schema, SchemaDefinition.SchemaID).Value;
            this.KeyID = ETRMDataAccess.GetInt32DTRElement(this.Schema, SchemaDefinition.KeyID).Value;
            this.cbFieldType.SelectedValue = ETRMDataAccess.GetInt32DTRElement(this.Schema, SchemaDefinition.FieldTypeID).Value;
            this.txtFieldTag.Text = ETRMDataAccess.GetStringDTRElement(this.Schema, SchemaDefinition.Tag);

            if (ETRMDataAccess.GetInt32DTRElement(this.Schema, SchemaDefinition.Columns).HasValue)
            {
                this.SetColumnControlsVisibility(true);
                this.nudColumnsCounter.Value = ETRMDataAccess.GetInt32DTRElement(this.Schema, SchemaDefinition.Columns).Value;
            }
        }

        private void FillFieldTypes()
        {
            DataTable types = ETRMDataAccess.GetFieldType();
            this.cbFieldType.DataSource = types.DefaultView;
            this.cbFieldType.DisplayMember = Field.FieldType;
            this.cbFieldType.ValueMember = Field.FieldTypeID;
            this.cbFieldType.BindingContext = this.BindingContext;
            this.cbFieldType.SelectedValue = -1;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.txtFieldTag.Text.Contains(" "))
            {
                MessageBox.Show("Cannot save tags containing white spaaces");
                return;
            }
            //if (this.IsNew && ETRMDataAccess.IsSchemaElementIn(this.SchemaID, this.txtFieldTag.Text.ToUpper()))
            if (ETRMDataAccess.IsSchemaElementIn(this.SchemaID, this.txtFieldTag.Text.ToUpper()))
            {
                MessageBox.Show("The same tag already exists in the schema. Please chose a different one.");
                return;
            }

            Int32 fieldType = Convert.ToInt32(this.cbFieldType.SelectedValue);

            if (fieldType <= 0)
            {
                MessageBox.Show("Please select a field type!");
                return;
            }

            Int32? numberOfColumns = null;

            if (this.IsColumn)
            {
                numberOfColumns = Convert.ToInt32(this.nudColumnsCounter.Value);

                if (numberOfColumns <= 0)
                {
                    MessageBox.Show("Table fields must have at least one column!");
                    return;
                }
            }

            if (this.IsNew)
            {
                if (!ETRMDataAccess.InsertSchemaElement(this.SchemaID, this.txtFieldTag.Text, fieldType, numberOfColumns))
                {
                    MessageBox.Show("Cannot save new field. Please contact support");
                }
            }
            else
            {
                if (!ETRMDataAccess.UpdateSchemaElement(this.SchemaID, this.KeyID, this.txtFieldTag.Text, fieldType, numberOfColumns))
                {
                    MessageBox.Show("Cannot update field. Please contact support");
                }
            }

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbFieldType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Int32 selectedIndex = Convert.ToInt32(this.cbFieldType.SelectedValue);

                if (selectedIndex == FType.Table)
                {
                    this.SetColumnControlsVisibility(true);
                }
                else
                {
                    this.SetColumnControlsVisibility(false);
                }
            }
            catch
            {
            }
        }

        private void SetColumnControlsVisibility(bool areVisible)
        {
            this.lblColumns.Visible = areVisible;
            this.nudColumnsCounter.Visible = areVisible;
            this.IsColumn = areVisible;
        }
    }
}
