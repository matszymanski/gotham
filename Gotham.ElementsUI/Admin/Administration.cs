﻿using Gotham.Data;
using Gotham.ElementsUI.Templates;
using Gotham.ElementsUI.UIUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gotham.ElementsUI.Admin
{
    public partial class Administration : Form
    {
        public Administration()
        {
            InitializeComponent();
            this.InitialiseView();
        }

        private void InitialiseView()
        {
            LookAndFeel.SetFormStatus(this);
            this.FillComboBox();
        }

        private void FillComboBox()
        {
            DataTable views = ETRMDataAccess.GetUserDefinedItems();

            this.cbItems.DataSource = views.DefaultView;
            this.cbItems.DisplayMember = UserItems.ViewTypeName;
            this.cbItems.ValueMember = UserItems.ViewTypeID;
            this.cbItems.BindingContext = this.BindingContext;
            this.cbItems.SelectedValue = -1;
        }

        private void cbItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.dgvContent.DataSource = null;
            this.dgvContent.ClearSelection();
            this.dgvContent.Refresh();

            Int32 selectedItem = Int32.MinValue;

            try
            {
                selectedItem = Convert.ToInt32(this.cbItems.SelectedValue);
            }
            catch
            {

            }

            DataTable source = new DataTable();

            if (selectedItem == 1)
            {
                source = ETRMDataAccess.GetGothamRootSchemas(null);
            }
            else if (selectedItem == 2)
            {
                source = ETRMDataAccess.GetGTMTemplateNames();
            }

            if (!ETRMDataAccess.IsEmpty(source))
            {
                this.dgvContent.DataSource = source;
            }
        }
    }
}
