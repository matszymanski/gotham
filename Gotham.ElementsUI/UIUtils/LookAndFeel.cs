﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gotham.ElementsUI.UIUtils
{
    public class LookAndFeel
    {
        static string Status;
        static int FontSize = 10;
        private static readonly LookAndFeel _instance = new LookAndFeel();

        private LookAndFeel()
        {
            Status = string.Empty;
        }

        public static LookAndFeel GetInstance()
        {
            return _instance;
        }

        public static string GetStatus()
        {
            return Status;
        }

        public static string GetEnvironment()
        {
            return Constants.DEMO;
            //return DBManager.GetDB.db.ApplicationMode();
        }

        public static void SetStatus(string newStatus)
        {
            Status = newStatus;
        }

        public static void SetFormStatus(Form f)
        {
            string status = GetEnvironment();

            Color c = new Color();

            switch (status)
            {
                case Constants.DEMO:
                    //c = Color.LightCyan;
                    c = Color.White;
                    break;
                default:
                    c = Color.White;
                    break;
            }

            if (c != Color.White)
            {
                f.BackColor = c;

                foreach (Control control in f.Controls)
                {
                    if (control.GetType() == typeof(TabControl))
                    {
                        TabControl tc = control as TabControl;
                        foreach (TabPage tp in tc.TabPages)
                        {
                            tp.BackColor = c;
                        }
                    }
                }
            }
            else if (c == Color.White)
            {
                foreach (Control control in f.Controls)
                {
                    f.BackColor = Color.White;
                    f.ForeColor = Color.Black;
                    SetControlColours(control, Color.White, Color.Black);
                }
            }
            else if (c == Color.LightCyan)
            {
                foreach (Control control in f.Controls)
                {
                    f.BackColor = Color.LightCyan;
                    f.ForeColor = Color.Black;
                    SetControlColours(control, Color.LightCyan, Color.Black);
                }
            }
        }

        public static void SetFormStatus(Form f, List<Label> labels)
        {
            SetFormStatus(f);
            ColourLabels(labels);
        }

        public static void ColourLabels(List<Label> labels)
        {
            foreach (Label l in labels)
            {
                l.BackColor = Color.White;
                l.ForeColor = Color.Blue;
                SetLabelColours(l, Color.White, Color.Blue);
            }
        }

        private static void SetLabelColours(Label control, Color backColor, Color foreColor)
        {
            Font newFontBold = new Font("Microsoft Sans Serif", FontSize, FontStyle.Bold);
            control.Font = newFontBold;
            control.BackColor = backColor;
            control.ForeColor = foreColor;
        }

        private static void SetControlColours(Control control, Color backColor, Color foreColor)
        {
            Font newFont = new Font("Microsoft Sans Serif", FontSize, FontStyle.Regular);
            Font newFontBold = new Font("Microsoft Sans Serif", FontSize, FontStyle.Bold);

            control.Font = newFont;
            control.BackColor = backColor;
            control.ForeColor = foreColor;

            if (control.GetType() == typeof(TabControl))
            {
                TabControl tc = control as TabControl;
                foreach (TabPage tp in tc.TabPages)
                {
                    tp.BackColor = backColor;
                    foreach (Control tpC in tp.Controls)
                    {
                        SetControlColours(tpC, backColor, foreColor);
                    }
                }
            }
            else if (control.GetType() == typeof(Button))
            {
                Button b = control as Button;
                b.Font = newFontBold;
                b.FlatStyle = FlatStyle.Flat;
                b.FlatAppearance.BorderColor = foreColor;
                b.FlatAppearance.BorderSize = 2;
            }
            //if (control.GetType() == typeof(ComboBox))
            //{
            //    ComboBox b = control as ComboBox;
            //    b.Font = newFontBold;
            //    //b.FlatStyle = FlatStyle.Standard;
            //    //b.FlatAppearance.BorderColor = foreColor;
            //    //b.FlatAppearance.BorderSize = 2;
            //}
            else if (control.GetType() == typeof(GroupBox))
            {
                foreach (Control gbC in control.Controls)
                {
                    SetControlColours(gbC, backColor, foreColor);
                }
            }
            else if (control.GetType() == typeof(Form))
            {
                Form b = control as Form;
                b.Font = newFontBold;
            }
            else if (control.GetType() == typeof(TextBox))
            {
                TextBox t = control as TextBox;
                t.Font = newFontBold;
            }
            else if (control.GetType() == typeof(Label))
            {
                Label t = control as Label;
                t.Font = newFontBold;
            }

            foreach (Control control0 in control.Controls)
            {
                SetControlColours(control0, backColor, foreColor);
            }
        }


        public static void PersuadeForm(Form f)
        {
            foreach (Control control in f.Controls)
            {
                SetControl(control);
            }
        }

        private static void SetControl(Control control)
        {
            //control.BackColor = control.BackColor;
            //control.ForeColor = control.ForeColor;
            //control.BackColor = SystemColors.ControlLightLight;
            //control.ForeColor = SystemColors.ControlDarkDark;

            control.BackColor = Color.White;
            control.ForeColor = Color.Black;

            if (control.Controls != null && control.Controls.Count > 0)
            {
                foreach (Control c0 in control.Controls)
                {
                    SetControl(c0);
                }
            }
        }
    }
}
