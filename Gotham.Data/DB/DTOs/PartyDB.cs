﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Data.DB.DTOs
{
    internal class PartyDB
    {
        public string PartyID { get; set; }
        public string PartyName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string StreetAddressLine1 { get; set; }
        public string StreetAddressLine2 { get; set; }
        public string StreetAddressLine3 { get; set; }
        public string StreetAddressLine4 { get; set; }
        public string StreetAddressLine5 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string LEI { get; set; }
        public string EIC { get; set; }
        public string Agreement;
        public string EAN { get; set; }
    }
}
