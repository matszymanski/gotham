﻿using Gotham.Domain;
using Gotham.Domain.Confirmation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Gotham.Data.DB
{
    public class Schema
    {
        private const string FIELDS = "SchemaID, KeyID, [Key], Tag, IsFixed, Columns, DisplayName";
        public static IList<SchemaField> GetSchemaFields()
        {
            using (var sqlConn = Factory.Connection())
            {
                sqlConn.Open();
                return sqlConn.Query<SchemaField>($"SELECT {FIELDS} FROM [dbo].[GothamSchemaDefinition]").ToList();
            }
        }

        public static IList<SchemaField> GetSchemaFields(int schemaId)
        {
            using (var sqlConn = Factory.Connection())
            {
                sqlConn.Open();
                return sqlConn.Query<SchemaField>($"SELECT {FIELDS} FROM [dbo].[GothamSchemaDefinition] WHERE SchemaID = {schemaId}").ToList();
            }
        }

        public static IList<Domain.Confirmation.Schema> GetSchemas()
        {
            using (var sqlConn = Factory.Connection())
            {
                sqlConn.Open();
                return sqlConn.Query<Domain.Confirmation.Schema>($"SELECT * FROM [dbo].[GothamSchemas]").ToList();
            }
        }
    }
}
