﻿using Gotham.Domain.Confirmation;
using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using System.Data.SqlClient;

namespace Gotham.Data.DB
{
    public class Translator
    {

        public static IList<TranslationPhrase> GetAllTranslations()
        {
            using (var sqlConn = Factory.Connection())
            {
                sqlConn.Open();
                return sqlConn.Query<TranslationPhrase>($"SELECT Phrase, Translation FROM [api].[Translator]").ToList();
            }
        }

        public static void Upsert(TranslationPhrase translationPhrase)
        {
            using (var sqlConn = Factory.Connection())
            {
                sqlConn.Open();
                try
                {
                    using (var command = new SqlCommand("api.sp_GTUpsertTranslation", sqlConn))
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Phrase", translationPhrase.Phrase);
                        command.Parameters.AddWithValue("@Translation", translationPhrase.Translation);
                        command.ExecuteNonQuery();
                    }
                }
                catch (Exception e)
                {
                    throw;
                }
            }
        }
    }
}
