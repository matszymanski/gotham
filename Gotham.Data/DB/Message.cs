﻿using Gotham.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Gotham.Domain.Confirmation;

namespace Gotham.Data.DB
{
    public class Message
    {
        private const string FIELDS = "SchemaID, Root, InsertionTimeStamp, ConfirmationStatus, ClassName, InternalPartyName, ExternalPartyName";



        public static IList<ConfirmationViewContainer> ConfirmationViews()
        {
            using (var sqlConn = Factory.Connection())
            {
                sqlConn.Open();
                return sqlConn.Query<ConfirmationViewContainer>(
                    $"SELECT {FIELDS} FROM [api].[ConfirmationView]").ToList();
            }
        }

        public static IList<ConfirmationViewContainer> ConfirmationViewsBySchema(int schemaId)
        {
            using (var sqlConn = Factory.Connection())
            {
                sqlConn.Open();
                return sqlConn.Query<ConfirmationViewContainer>(
                    $"SELECT {FIELDS} FROM [api].[ConfirmationView] where SchemaID = {schemaId}").ToList();
            }
        }

        public static IList<ConfirmationViewContainer> ConfirmationViewsNew(int limit)
        {
            if (limit < 1)
                return new List<ConfirmationViewContainer>();

            using (var sqlConn = Factory.Connection())
            {
                sqlConn.Open();
                return sqlConn.Query<ConfirmationViewContainer>(
                    $"SELECT TOP {limit} {FIELDS} FROM [api].[ConfirmationView] order by InsertionTimestamp desc").ToList();
            }
        }

        public static IList<ConfirmationViewContainer> ConfirmationViewById(TradeUniqueKey tradeKey)
        {
            using (var sqlConn = Factory.Connection())
            {
                sqlConn.Open();
                string query = $"SELECT {FIELDS} ";
                query += @"FROM [api].[ConfirmationView] 
                           where TradeID = @TradeID and InternalPartyID = @InternalPartyID";
                return sqlConn.Query<ConfirmationViewContainer>(query,
                                                             new { TradeID = tradeKey.TradeID, InternalPartyID = tradeKey.InternalPartyID }).ToList();
            }
        }

        public static IList<HospitalContainer> Hospital()
        {
            using (var sqlConn = Factory.Connection())
            {
                sqlConn.Open();
                return sqlConn.Query<HospitalContainer>(
                    $"SELECT * FROM [api].[MessageHospital]").ToList();
            }
        }
        public static int GetHospitalCount()
        {
            using (var sqlConn = Factory.Connection())
            {
                sqlConn.Open();
                return sqlConn.QueryFirst<int>("SELECT count(1) FROM [dbo].[GTMessageHospital]");
            }
        }


    }
}