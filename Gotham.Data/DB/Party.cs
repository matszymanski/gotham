﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using Gotham.Domain;
using Gotham.Data.DB.DTOs;

namespace Gotham.Data.DB
{
    public class Party
    {
        private const string FIELDS = @"PartyID
                                      ,PartyName
                                      ,Phone
                                      ,Email
                                      ,StreetAddressLine1
                                      ,StreetAddressLine2
                                      ,StreetAddressLine3
                                      ,StreetAddressLine4
                                      ,StreetAddressLine5
                                      ,City
                                      ,State
                                      ,Country
                                      ,PostalCode
                                      ,LEI
	                                  ,EIC
	                                  ,EAN
                                      ,Agreement";

        private const string ViewParty = "api.Party";
        private const string ViewPartyAddress = "api.PartyAddress";
        private const string SpUpsertParty = "api.sp_GTUpsertParty";
        private const string SpUpsertPartyAddress = "api.sp_GTUpsertPartyAddress";

        public static IList<string> PartyIDsUnaligned(int count)
        {
            using (var sqlConn = Factory.Connection())
            {
                sqlConn.Open();
                var parties = sqlConn.Query<string>($"select TOP {count} PartyID from api.Party_Unaligned");
                return parties.ToList();
            }
        }

        
        public static IEnumerable<Domain.Party> ConstructParties(IEnumerable<dynamic> partiesWithoutAddress, IEnumerable<Address> addresses)
        {
            var parties = partiesWithoutAddress.Select(party => new Domain.Party
            {
                PartyID = party.PartyID,
                PartyName = party.PartyName,
                ContactInfo = new ContactInfo { Phone = party.Phone, Email = party.Email }
            });
            //parties = parties.Join(addresses, party => party.PartyID, address => address.PartyID, (party, address) => { party.ContactInfo.Addresses.Add(address); return party; });
            parties = parties.Join(addresses, party => party.PartyID, address => address.PartyID, (party, address) => { party.ContactInfo.Address = address; return party; });
            return parties;
        }

        public static IEnumerable<Domain.Party> Parties()
        {
            using (var sqlConn = Factory.Connection())
            {
                sqlConn.Open();
                var dbParties = sqlConn.Query<PartyDB>($"select {FIELDS} from {ViewParty}");

                //var addresses = sqlConn.Query<Address>($"select * from {DB.Factory.ViewPartyAddress}");
                var parties = ConstructParties(dbParties);

                return parties.ToList();
            }
        }

        public static Domain.Party PartyById(string partyId)
        {
            using (var sqlConn = DB.Factory.Connection())
            {
                sqlConn.Open();
                var dbParties = sqlConn.Query<PartyDB>($"select {FIELDS} from {ViewParty} where PartyID = @PartyID", new { PartyID = partyId });
                //var addresses = sqlConn.Query<Address>($"select * from {DB.Factory.ViewPartyAddress} where PartyID = @PartyID", new { PartyID = partyId });
                var parties = ConstructParties(dbParties);

                return parties.FirstOrDefault();
            }
        }

        private static IEnumerable<Domain.Party> ConstructParties(IEnumerable<PartyDB> partiesFlat)
        {
            var parties = partiesFlat.Select(party => new Domain.Party
            {
                PartyID = party.PartyID,
                PartyName = party.PartyName,
                ContactInfo = new ContactInfo
                {
                    Phone = party.Phone,
                    Email = party.Email,
                    Address = new Address
                    {
                        City = party.City,
                        Country = party.Country,
                        PostalCode = party.PostalCode,
                        State = party.State,
                        StreetAddressLine1 = party.StreetAddressLine1,
                        StreetAddressLine2 = party.StreetAddressLine2,
                        StreetAddressLine3 = party.StreetAddressLine3,
                        StreetAddressLine4 = party.StreetAddressLine4,
                        StreetAddressLine5 = party.StreetAddressLine5
                    }
                },
                LEI = party.LEI,
                EAN = party.EAN,
                EIC = party.EIC,
                Agreement = party.Agreement
            });
            //parties = parties.Join(addresses, party => party.PartyID, address => address.PartyID, (party, address) => { party.ContactInfo.Addresses.Add(address); return party; });
            //parties = parties.Join(addresses, party => party.PartyID, address => address.PartyID, (party, address) => { party.ContactInfo.Address = address; return party; });
            return parties;
        }

        public static void PartyUpsert(Domain.Party party)
        {
            using (var sqlConn = Factory.Connection())
            {
                sqlConn.Open();
                var transaction = sqlConn.BeginTransaction(IsolationLevel.RepeatableRead);

                try
                {
                    using (var command = new SqlCommand(SpUpsertParty, sqlConn, transaction))
                    {
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@PartyID", party.PartyID);
                        command.Parameters.AddWithValue("@PartyName", party.PartyName);
                        command.Parameters.AddWithValue("@Email", party.ContactInfo.Email);
                        command.Parameters.AddWithValue("@Phone", party.ContactInfo.Phone);
                        command.Parameters.AddWithValue("@StreetAddressLine1", party.ContactInfo.Address.StreetAddressLine1);
                        command.Parameters.AddWithValue("@StreetAddressLine2", party.ContactInfo.Address.StreetAddressLine2);
                        command.Parameters.AddWithValue("@StreetAddressLine3", party.ContactInfo.Address.StreetAddressLine3);
                        command.Parameters.AddWithValue("@StreetAddressLine4", party.ContactInfo.Address.StreetAddressLine4);
                        command.Parameters.AddWithValue("@StreetAddressLine5", party.ContactInfo.Address.StreetAddressLine5);
                        command.Parameters.AddWithValue("@City", party.ContactInfo.Address.City);
                        command.Parameters.AddWithValue("@Country", party.ContactInfo.Address.Country);
                        command.Parameters.AddWithValue("@PostalCode", party.ContactInfo.Address.PostalCode);
                        command.Parameters.AddWithValue("@State", party.ContactInfo.Address.State);
                        command.Parameters.AddWithValue("@LEI", party.LEI);
                        command.Parameters.AddWithValue("@EIC", party.EIC);
                        command.Parameters.AddWithValue("@EAN", party.EAN);
                        command.Parameters.AddWithValue("@Agreement", party.Agreement);
                        command.ExecuteNonQuery();
                    }
                    var addressList = new List<Address> { party.ContactInfo.Address };
                    //if (party.ContactInfo.Addresses.Any())
                    //if(addressList.Any())
                    //{
                    //    //var addrTables = party.ContactInfo.Addresses.Select(adr => adr.AsTable()).ToList();
                    //    var addrTables = addressList.Select(adr => adr.AsTable()).ToList();
                    //    var addresses = new DataTable();
                    //    addrTables.ForEach(tab => addresses.Merge(tab));
                    //    using (var command = new SqlCommand(DB.Factory.SpUpsertPartyAddress, sqlConn, transaction))
                    //    {
                    //        command.CommandType = System.Data.CommandType.StoredProcedure;
                    //        command.Parameters.AddWithValue("@AddressList", addrTables);

                    //        command.ExecuteNonQuery();
                    //    }
                    //}
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }
    }
}
