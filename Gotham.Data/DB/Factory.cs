﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Data.DB
{
    public class Factory
    {
        public static SqlConnection Connection()
        {
            var connStr = "Server=37.34.50.24;Database=GothamDev;User Id=ch4ts;Password = Rapsodia1;MultipleActiveResultSets=True;Timeout=300";
            return new SqlConnection(connStr);
        }



        public static string ViewPriceIndexUnaligned => "api.[PriceIndex_Unaligned]";
        public static string ViewTabPriceIndex => "api.[PriceIndex]";
        public static string ViewPriceIndexMap => "api.[PriceIndex_Map]";
        public static string SpUpdatePriceIndexMap => "api.sp_GTUpdatePriceIndexMap";


        public static string ViewParty => "api.Party";
        public static string ViewPartyAddress => "api.PartyAddress";
        public static string SpUpsertParty => "api.sp_GTUpsertParty";
        public static string SpUpsertPartyAddress => "api.sp_GTUpsertPartyAddress";
        public static object ViewPartiesUnaligned => "api.Party_Unaligned";

        public static string ViewDocument => "api.Document";

        public static string ViewDocumentUploaded => "api.DocumentUploaded";

        public static string SpUpdateConfirmationStatus => "api.sp_GTUpdateConfirmationStatus";

        public static string TabMatchBoxEmailServer => "dbo.MatchBoxEmailServer";
        public static string TabMatchBoxRecipient => "dbo.MatchBoxRecipient";
        public static string TabMatchBoxTemplate => "dbo.MatchBoxTemplate";
    }
}
