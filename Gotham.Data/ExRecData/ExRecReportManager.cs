﻿using Gotham.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gotham.Data.ExRecData
{
    public static class ExRecReportManager
    {
        public enum RowStatus
        {
            First, Match, Missmatch
        }

        public enum ReportType
        {
            Monthly, Daily
        }


        ////////////////This is working.
        public static List<DataTable> CreateReport(DataTable matches, DataTable missMatches, DataTable reportTemplate, string applicationMode, Int32 recId)
        {
            DataTable report = reportTemplate;
            report.Columns.Add(ExRecInfo.MATCHSTATUS);
            report.Columns.Add(ExRecInfo.SOURCE);
            Int32 nCols = report.Columns.Count;
            DataTable sr = ShadowReport(nCols);
            bool isFirstEntry = true; //ugly as hell but can't do any better.

            try
            {
                //START of MATCHES
                foreach (DataRow match in matches.Rows)
                {
                    DataTable bscRecord = null;
                    DataTable bnpRecord = null;

                    switch (applicationMode)
                    {
                        case ApplicationMode.OpenPos:
                            bscRecord = ETRMDataAccess.ExRecGetOpenPosRecord(recId, match, ExRecInfo.BSC);
                            bnpRecord = ETRMDataAccess.ExRecGetOpenPosRecord(recId, match, ExRecInfo.BNP);
                            break;
                        case ApplicationMode.OpenPosDetails:
                            bscRecord = ETRMDataAccess.ExRecGetOpenPosDetailsRecord(recId, match, ExRecInfo.BSC);
                            bnpRecord = ETRMDataAccess.ExRecGetOpenPosDetailsRecord(recId, match, ExRecInfo.BNP);
                            break;
                        case ApplicationMode.CFITrades:
                            bscRecord = ETRMDataAccess.ExRecGetCFITradesRecord(recId, match, ExRecInfo.BSC);
                            bnpRecord = ETRMDataAccess.ExRecGetCFITradesRecord(recId, match, ExRecInfo.BNP);
                            break;
                        case ApplicationMode.ExpiringDeals:
                            bscRecord = ETRMDataAccess.ExRecGetExpiringTradesRecord(recId, match, ExRecInfo.BSC);
                            bnpRecord = ETRMDataAccess.ExRecGetExpiringTradesRecord(recId, match, ExRecInfo.BNP);
                            break;
                        case ApplicationMode.CancelledDeals:
                            bscRecord = ETRMDataAccess.ExRecGetCancelledTradesRecord(recId, match, ExRecInfo.BSC);
                            bnpRecord = ETRMDataAccess.ExRecGetCancelledTradesRecord(recId, match, ExRecInfo.BNP);
                            break;
                        default:
                            {
                                MessageBox.Show(string.Format("Report {0} not yet implemented.", applicationMode));
                                return null;
                            }
                    }

                    DataRow sr1 = sr.NewRow();
                    DataRow sr2 = sr.NewRow();
                    DataRow r1 = report.NewRow();
                    DataRow r2 = report.NewRow();

                    bool isMatch = true;
                    string bscValue = string.Empty;
                    string bnpValue = string.Empty;

                    for (Int32 i = 0; i < (nCols - 2); i++)
                    {
                        bscValue = Convert.ToString(bscRecord.Rows[0][i]);
                        bnpValue = Convert.ToString(bnpRecord.Rows[0][i]);
                        if (bscValue.Equals(bnpValue))
                        {
                            sr1[string.Format("v{0}", i.ToString())] = 0;
                            sr2[string.Format("v{0}", i.ToString())] = 0;
                        }
                        else
                        {
                            sr1[string.Format("v{0}", i.ToString())] = 1;
                            sr2[string.Format("v{0}", i.ToString())] = 1;
                            isMatch = false;
                        }

                        r1[i] = bscValue;
                        r2[i] = bnpValue;
                    }

                    if (isMatch)
                    {
                        sr1[ExRecInfo.RowStatus] = ExRecInfo.MATCHED;
                        sr2[ExRecInfo.RowStatus] = ExRecInfo.MATCHED;
                        r1[ExRecInfo.MATCHSTATUS] = ExRecInfo.MATCHED;
                        r2[ExRecInfo.MATCHSTATUS] = ExRecInfo.MATCHED;
                    }
                    else
                    {
                        sr1[ExRecInfo.RowStatus] = ExRecInfo.BREAK;
                        sr2[ExRecInfo.RowStatus] = ExRecInfo.BREAK;
                        r1[ExRecInfo.MATCHSTATUS] = ExRecInfo.BREAK;
                        r2[ExRecInfo.MATCHSTATUS] = ExRecInfo.BREAK;
                    }

                    if (!isFirstEntry)
                    {
                        DataRow rEmpty = report.NewRow();
                        DataRow sEmpty = sr.NewRow();
                        report.Rows.Add(rEmpty);
                        sr.Rows.Add(sEmpty);
                    }

                    r1[ExRecInfo.SOURCE] = ExRecInfo.BSC;
                    r2[ExRecInfo.SOURCE] = ExRecInfo.CLEARER;
                    sr.Rows.Add(sr1);
                    sr.Rows.Add(sr2);
                    report.Rows.Add(r1);
                    report.Rows.Add(r2);

                    isFirstEntry = false;
                }
                //END of MATCHES

                DataRow repEmpty = report.NewRow();
                DataRow shadowEmpty = sr.NewRow();
                report.Rows.Add(repEmpty);
                sr.Rows.Add(shadowEmpty);

                //START of MISSMATCHES
                foreach (DataRow match in missMatches.Rows)
                {
                    string source = Convert.ToString(match[ExRecInfo.SOURCE]).ToUpper();
                    DataRow r1 = report.NewRow();

                    DataTable record = null;

                    if (source.Equals(ExRecInfo.BSC))
                    {
                        switch (applicationMode)
                        {
                            case ApplicationMode.OpenPos:
                                record = ETRMDataAccess.ExRecGetOpenPosRecord(recId, match, ExRecInfo.BSC);
                                break;
                            case ApplicationMode.OpenPosDetails:
                                record = ETRMDataAccess.ExRecGetOpenPosDetailsRecord(recId, match, ExRecInfo.BSC);
                                break;
                            case ApplicationMode.CFITrades:
                                record = ETRMDataAccess.ExRecGetCFITradesRecord(recId, match, ExRecInfo.BSC);
                                break;
                            case ApplicationMode.ExpiringDeals:
                                record = ETRMDataAccess.ExRecGetExpiringTradesRecord(recId, match, ExRecInfo.BSC);
                                break;
                            case ApplicationMode.CancelledDeals:
                                record = ETRMDataAccess.ExRecGetCancelledTradesRecord(recId, match, ExRecInfo.BSC);
                                break;
                        }

                        r1[ExRecInfo.MATCHSTATUS] = ExRecInfo.BSCONLY;
                        r1[ExRecInfo.SOURCE] = ExRecInfo.BSC;
                    }
                    else
                    {
                        switch (applicationMode)
                        {
                            case ApplicationMode.OpenPos:
                                record = ETRMDataAccess.ExRecGetOpenPosRecord(recId, match, ExRecInfo.BNP);
                                break;
                            case ApplicationMode.OpenPosDetails:
                                record = ETRMDataAccess.ExRecGetOpenPosDetailsRecord(recId, match, ExRecInfo.BNP);
                                break;
                            case ApplicationMode.CFITrades:
                                record = ETRMDataAccess.ExRecGetCFITradesRecord(recId, match, ExRecInfo.BNP);
                                break;
                            case ApplicationMode.ExpiringDeals:
                                record = ETRMDataAccess.ExRecGetExpiringTradesRecord(recId, match, ExRecInfo.BNP);
                                break;
                            case ApplicationMode.CancelledDeals:
                                record = ETRMDataAccess.ExRecGetCancelledTradesRecord(recId, match, ExRecInfo.BNP);
                                break;
                        }

                        r1[ExRecInfo.MATCHSTATUS] = ExRecInfo.BNPONLY;
                        r1[ExRecInfo.SOURCE] = ExRecInfo.CLEARER;
                    }

                    for (Int32 i = 0; i < (nCols - 2); i++)
                    {
                        r1[i] = Convert.ToString(record.Rows[0][i]);
                    }

                    report.Rows.Add(r1);
                    report.Rows.Add(report.NewRow());
                }
                //END of MISSMATCHES
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
                MessageBox.Show("Error encountered in generating the report. Please contact support.");
                return null;
            }

            report.Columns[ExRecInfo.MATCHSTATUS].SetOrdinal(0);
            report.Columns[ExRecInfo.SOURCE].SetOrdinal(1);

            List<DataTable> rec = new List<DataTable>();
            rec.Add(report);
            rec.Add(sr);

            return rec;
        }


        public static List<DataTable> CreateReport(DataTable matches, DataTable dailyMatches, DataTable missMatches, DataTable dailyMissMatches, DataTable reportTemplate, string applicationMode, Int32 recId)
        {
            DataTable report = reportTemplate;
            report.Columns.Add(ExRecInfo.MATCHSTATUS);
            report.Columns.Add(ExRecInfo.SOURCE);
            Int32 nCols = report.Columns.Count;
            DataTable sr = ShadowReport(nCols);

            try
            {
                InsertMatches(report, matches, sr, ReportType.Monthly, applicationMode, recId, true, nCols);
                InsertMatches(report, dailyMatches, sr, ReportType.Daily, applicationMode, recId, ETRMDataAccess.IsEmpty(sr), nCols);

                DataRow repEmpty = report.NewRow();
                DataRow shadowEmpty = sr.NewRow();
                report.Rows.Add(repEmpty);
                sr.Rows.Add(shadowEmpty);

                InsertMissMatches(report, missMatches, sr, ReportType.Monthly, applicationMode, recId, ETRMDataAccess.IsEmpty(sr), nCols);
                InsertMissMatches(report, dailyMissMatches, sr, ReportType.Daily, applicationMode, recId, ETRMDataAccess.IsEmpty(sr), nCols);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
                MessageBox.Show("Error encountered in generating the report. Please contact support.");
                return null;
            }

            report.Columns[ExRecInfo.MATCHSTATUS].SetOrdinal(0);
            report.Columns[ExRecInfo.SOURCE].SetOrdinal(1);

            List<DataTable> rec = new List<DataTable>();
            rec.Add(report);
            rec.Add(sr);

            return rec;
        }

        private static void InsertMissMatches(DataTable report, DataTable missMatches, DataTable sr, ReportType type, string applicationMode, Int32 recId, bool isFirstEntry, Int32 nCols)
        {
            foreach (DataRow match in missMatches.Rows)
            {
                string source = Convert.ToString(match[ExRecInfo.SOURCE]).ToUpper();
                DataRow r1 = report.NewRow();

                DataTable record = null;

                if (type == ReportType.Monthly)
                {
                    if (source.Equals(ExRecInfo.BSC))
                    {
                        switch (applicationMode)
                        {
                            case ApplicationMode.OpenPos:
                                record = ETRMDataAccess.ExRecGetOpenPosRecord(recId, match, ReportType.Monthly, ExRecInfo.BSC);
                                break;
                            case ApplicationMode.OpenPosDetails:
                                record = ETRMDataAccess.ExRecGetOpenPosDetailsRecord(recId, match, ReportType.Monthly, ExRecInfo.BSC);
                                break;
                            case ApplicationMode.CFITrades:
                                record = ETRMDataAccess.ExRecGetCFITradesRecord(recId, match, ReportType.Monthly, ExRecInfo.BSC);
                                break;
                            case ApplicationMode.ExpiringDeals:
                                //record = ETRMDataAccess.ExRecGetExpiringTradesRecord(recId, match, ExRecInfo.BSC);
                                record = ETRMDataAccess.ExRecGetExpiringTradesRecord(recId, match, ReportType.Monthly, ExRecInfo.BSC);
                                break;
                            case ApplicationMode.CancelledDeals:
                                record = ETRMDataAccess.ExRecGetCancelledTradesRecord(recId, match, ExRecInfo.BSC);
                                break;
                        }

                        r1[ExRecInfo.MATCHSTATUS] = ExRecInfo.BSCONLY;
                        r1[ExRecInfo.SOURCE] = ExRecInfo.BSC;
                    }
                    else
                    {
                        switch (applicationMode)
                        {
                            case ApplicationMode.OpenPos:
                                record = ETRMDataAccess.ExRecGetOpenPosRecord(recId, match, ReportType.Monthly, ExRecInfo.BNP);
                                break;
                            case ApplicationMode.OpenPosDetails:
                                record = ETRMDataAccess.ExRecGetOpenPosDetailsRecord(recId, match, ReportType.Monthly, ExRecInfo.BNP);
                                break;
                            case ApplicationMode.CFITrades:
                                record = ETRMDataAccess.ExRecGetCFITradesRecord(recId, match, ReportType.Monthly, ExRecInfo.BNP);
                                break;
                            case ApplicationMode.ExpiringDeals:
                                //record = ETRMDataAccess.ExRecGetExpiringTradesRecord(recId, match, ExRecInfo.BNP);
                                record = ETRMDataAccess.ExRecGetExpiringTradesRecord(recId, match, ReportType.Monthly, ExRecInfo.BNP);
                                break;
                            case ApplicationMode.CancelledDeals:
                                record = ETRMDataAccess.ExRecGetCancelledTradesRecord(recId, match, ExRecInfo.BNP);
                                break;
                        }

                        r1[ExRecInfo.MATCHSTATUS] = ExRecInfo.BNPONLY;
                        r1[ExRecInfo.SOURCE] = ExRecInfo.CLEARER;
                    }
                }
                else if (type == ReportType.Daily)
                {
                    if (source.Equals(ExRecInfo.BSC))
                    {
                        switch (applicationMode)
                        {
                            case ApplicationMode.OpenPos:
                                record = ETRMDataAccess.ExRecGetOpenPosRecord(recId, match, ReportType.Daily, ExRecInfo.BSC);
                                break;
                            case ApplicationMode.OpenPosDetails:
                                record = ETRMDataAccess.ExRecGetOpenPosDetailsRecord(recId, match, ReportType.Daily, ExRecInfo.BSC);
                                break;
                            case ApplicationMode.CFITrades:
                                record = ETRMDataAccess.ExRecGetCFITradesRecord(recId, match, ReportType.Daily, ExRecInfo.BSC);
                                break;
                            case ApplicationMode.ExpiringDeals:
                                //record = ETRMDataAccess.ExRecGetExpiringTradesRecord(recId, match, ExRecInfo.BSC);
                                record = ETRMDataAccess.ExRecGetExpiringTradesRecord(recId, match, ReportType.Daily, ExRecInfo.BSC);
                                break;
                            case ApplicationMode.CancelledDeals:
                                record = ETRMDataAccess.ExRecGetCancelledTradesRecord(recId, match, ExRecInfo.BSC);
                                break;
                        }

                        r1[ExRecInfo.MATCHSTATUS] = ExRecInfo.BSCONLY;
                        r1[ExRecInfo.SOURCE] = ExRecInfo.BSC;
                    }
                    else
                    {
                        switch (applicationMode)
                        {
                            case ApplicationMode.OpenPos:
                                record = ETRMDataAccess.ExRecGetOpenPosRecord(recId, match, ReportType.Daily, ExRecInfo.BNP);
                                break;
                            case ApplicationMode.OpenPosDetails:
                                record = ETRMDataAccess.ExRecGetOpenPosDetailsRecord(recId, match, ReportType.Daily, ExRecInfo.BNP);
                                break;
                            case ApplicationMode.CFITrades:
                                record = ETRMDataAccess.ExRecGetCFITradesRecord(recId, match, ReportType.Daily, ExRecInfo.BNP);
                                break;
                            case ApplicationMode.ExpiringDeals:
                                record = ETRMDataAccess.ExRecGetExpiringTradesRecord(recId, match, ReportType.Daily, ExRecInfo.BNP);
                                break;
                            case ApplicationMode.CancelledDeals:
                                record = ETRMDataAccess.ExRecGetCancelledTradesRecord(recId, match, ExRecInfo.BNP);
                                break;
                        }

                        r1[ExRecInfo.MATCHSTATUS] = ExRecInfo.BNPONLY;
                        r1[ExRecInfo.SOURCE] = ExRecInfo.CLEARER;
                    }
                }

                for (Int32 i = 0; i < (nCols - 2); i++)
                {
                    if (!ETRMDataAccess.IsEmpty(record))
                    {
                        r1[i] = Convert.ToString(record.Rows[0][i]);
                    }
                }

                report.Rows.Add(r1);
                report.Rows.Add(report.NewRow());
            }
        }

        private static void InsertMatches(DataTable report, DataTable matches, DataTable sr, ReportType type, string applicationMode, Int32 recId, bool isFirstEntry, Int32 nCols)
        {
            foreach (DataRow match in matches.Rows)
            {
                DataTable bscRecord = null;
                DataTable bnpRecord = null;

                if (type == ReportType.Monthly)
                {
                    switch (applicationMode)
                    {
                        case ApplicationMode.OpenPos:
                            bscRecord = ETRMDataAccess.ExRecGetOpenPosRecord(recId, match, ReportType.Monthly, ExRecInfo.BSC);
                            bnpRecord = ETRMDataAccess.ExRecGetOpenPosRecord(recId, match, ReportType.Monthly, ExRecInfo.BNP);
                            break;
                        case ApplicationMode.OpenPosDetails:
                            bscRecord = ETRMDataAccess.ExRecGetOpenPosDetailsRecord(recId, match, ReportType.Monthly, ExRecInfo.BSC);
                            bnpRecord = ETRMDataAccess.ExRecGetOpenPosDetailsRecord(recId, match, ReportType.Monthly, ExRecInfo.BNP);
                            break;
                        case ApplicationMode.CFITrades:
                            bscRecord = ETRMDataAccess.ExRecGetCFITradesRecord(recId, match, ReportType.Monthly, ExRecInfo.BSC);
                            bnpRecord = ETRMDataAccess.ExRecGetCFITradesRecord(recId, match, ReportType.Monthly, ExRecInfo.BNP);
                            break;
                        case ApplicationMode.ExpiringDeals:
                            bscRecord = ETRMDataAccess.ExRecGetExpiringTradesRecord(recId, match, ReportType.Monthly, ExRecInfo.BSC);
                            bnpRecord = ETRMDataAccess.ExRecGetExpiringTradesRecord(recId, match, ReportType.Monthly, ExRecInfo.BNP);
                            break;
                        case ApplicationMode.CancelledDeals:
                            bscRecord = ETRMDataAccess.ExRecGetCancelledTradesRecord(recId, match, ExRecInfo.BSC);
                            bnpRecord = ETRMDataAccess.ExRecGetCancelledTradesRecord(recId, match, ExRecInfo.BNP);
                            break;
                        default:
                            {
                                MessageBox.Show(string.Format("Report {0} not yet implemented.", applicationMode));
                                return;
                            }
                    }
                }
                else if( type == ReportType.Daily)
                {
                    switch (applicationMode)
                    {
                        case ApplicationMode.OpenPos:
                            bscRecord = ETRMDataAccess.ExRecGetOpenPosRecord(recId, match, ReportType.Daily, ExRecInfo.BSC);
                            bnpRecord = ETRMDataAccess.ExRecGetOpenPosRecord(recId, match, ReportType.Daily, ExRecInfo.BNP);
                            break;
                        case ApplicationMode.OpenPosDetails:
                            bscRecord = ETRMDataAccess.ExRecGetOpenPosDetailsRecord(recId, match, ReportType.Daily, ExRecInfo.BSC);
                            bnpRecord = ETRMDataAccess.ExRecGetOpenPosDetailsRecord(recId, match, ReportType.Daily, ExRecInfo.BNP);
                            break;
                        case ApplicationMode.CFITrades:
                            bscRecord = ETRMDataAccess.ExRecGetCFITradesRecord(recId, match, ReportType.Daily, ExRecInfo.BSC);
                            bnpRecord = ETRMDataAccess.ExRecGetCFITradesRecord(recId, match, ReportType.Daily, ExRecInfo.BNP);
                            break;
                        case ApplicationMode.ExpiringDeals:
                            bscRecord = ETRMDataAccess.ExRecGetExpiringTradesRecord(recId, match, ReportType.Daily, ExRecInfo.BSC);
                            bnpRecord = ETRMDataAccess.ExRecGetExpiringTradesRecord(recId, match, ReportType.Daily, ExRecInfo.BNP);
                            break;
                        case ApplicationMode.CancelledDeals:
                            bscRecord = ETRMDataAccess.ExRecGetCancelledTradesRecord(recId, match, ExRecInfo.BSC);
                            bnpRecord = ETRMDataAccess.ExRecGetCancelledTradesRecord(recId, match, ExRecInfo.BNP);
                            break;
                        default:
                            {
                                MessageBox.Show(string.Format("Report {0} not yet implemented.", applicationMode));
                                return;
                            }
                    }
                }

                DataRow sr1 = sr.NewRow();
                DataRow sr2 = sr.NewRow();
                DataRow r1 = report.NewRow();
                DataRow r2 = report.NewRow();

                bool isMatch = true;
                string bscValue = string.Empty;
                string bnpValue = string.Empty;

                for (Int32 i = 0; i < (nCols - 2); i++)
                {
                    bscValue = Convert.ToString(bscRecord.Rows[0][i]);
                    bnpValue = Convert.ToString(bnpRecord.Rows[0][i]);
                    if (bscValue.Equals(bnpValue))
                    {
                        sr1[string.Format("v{0}", i.ToString())] = 0;
                        sr2[string.Format("v{0}", i.ToString())] = 0;
                    }
                    else
                    {
                        sr1[string.Format("v{0}", i.ToString())] = 1;
                        sr2[string.Format("v{0}", i.ToString())] = 1;
                        isMatch = false;
                    }

                    r1[i] = bscValue;
                    r2[i] = bnpValue;
                }

                if (isMatch)
                {
                    sr1[ExRecInfo.RowStatus] = ExRecInfo.MATCHED;
                    sr2[ExRecInfo.RowStatus] = ExRecInfo.MATCHED;
                    r1[ExRecInfo.MATCHSTATUS] = ExRecInfo.MATCHED;
                    r2[ExRecInfo.MATCHSTATUS] = ExRecInfo.MATCHED;
                }
                else
                {
                    sr1[ExRecInfo.RowStatus] = ExRecInfo.BREAK;
                    sr2[ExRecInfo.RowStatus] = ExRecInfo.BREAK;
                    r1[ExRecInfo.MATCHSTATUS] = ExRecInfo.BREAK;
                    r2[ExRecInfo.MATCHSTATUS] = ExRecInfo.BREAK;
                }

                if (!isFirstEntry)
                {
                    DataRow rEmpty = report.NewRow();
                    DataRow sEmpty = sr.NewRow();
                    report.Rows.Add(rEmpty);
                    sr.Rows.Add(sEmpty);
                }

                r1[ExRecInfo.SOURCE] = ExRecInfo.BSC;
                r2[ExRecInfo.SOURCE] = ExRecInfo.CLEARER;
                sr.Rows.Add(sr1);
                sr.Rows.Add(sr2);
                report.Rows.Add(r1);
                report.Rows.Add(r2);

                isFirstEntry = false;
            }
        }

        public static DataTable ShadowReport(Int32 nCols)
        {
            DataTable report = new DataTable("d");

            report.Columns.Add("RowStatus", typeof(string));

            for (int i = 0; i < nCols; i++)
            {
                report.Columns.Add(string.Format("v{0}", i.ToString()), typeof(int));
            }

            return report;
        }

    }
}
