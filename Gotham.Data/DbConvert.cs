﻿using System;

namespace Gotham.Data
{
    public static class DbConvert
    {
        public static T GetValue<T>(object dbResult, T defaultValue = default(T))
        {
            if (Convert.IsDBNull(dbResult) || dbResult == null)
                return defaultValue;
            return (T)Convert.ChangeType(dbResult, typeof(T));
        }

        public static object GetObject(object dbResult, object defaultValue = null) { return Convert.IsDBNull(dbResult) ? defaultValue : dbResult; }
        public static byte GetByte(object dbResult, byte defaultValue = 0) { return GetValue(dbResult, defaultValue); }
        public static char GetChar(object dbResult, char defaultValue = '\0') { return GetValue(dbResult, defaultValue); }
        public static bool GetBool(object dbResult, bool defaultValue = false) { return GetValue(dbResult, defaultValue); }
        public static short GetShort(object dbResult, short defaultValue = 0) { return GetValue(dbResult, defaultValue); }
        public static float GetFloat(object dbResult, float defaultValue = 0F) { return GetValue(dbResult, defaultValue); }
        public static double GetDouble(object dbResult, double defaultValue = 0) { return GetValue(dbResult, defaultValue); }
        public static decimal GetDecimal(object dbResult, decimal defaultValue = 0) { return GetValue(dbResult, defaultValue); }
        public static int GetInt(object dbResult, int defaultValue = 0) { return GetValue(dbResult, defaultValue); }
        public static long GetLong(object dbResult, long defaultValue = 0L) { return GetValue(dbResult, defaultValue); }
        public static ushort GetUShort(object dbResult, ushort defaultValue = 0) { return GetValue(dbResult, defaultValue); }
        public static uint GetUInt(object dbResult, uint defaultValue = 0U) { return GetValue(dbResult, defaultValue); }
        public static ulong GetULong(object dbResult, ulong defaultValue = 0UL) { return GetValue(dbResult, defaultValue); }
        public static string GetString(object dbResult, string defaultValue = null) { return GetValue(dbResult, defaultValue); }
        public static DateTime GetDateTime(object dbResult, DateTime defaultValue = default(DateTime)) { return GetValue(dbResult, defaultValue); }

        public static class Nullable
        {
            public static byte? GetByte(object dbResult, byte? defaultValue = null) { return GetValue(dbResult, defaultValue); }
            public static char? GetChar(object dbResult, char? defaultValue = null) { return GetValue(dbResult, defaultValue); }
            public static bool? GetBool(object dbResult, bool? defaultValue = null) { return GetValue(dbResult, defaultValue); }
            public static short? GetShort(object dbResult, short? defaultValue = null) { return GetValue(dbResult, defaultValue); }
            public static int? GetInt(object dbResult, int? defaultValue = null) { return GetValue(dbResult, defaultValue); }
            public static long? GetLong(object dbResult, long? defaultValue = null) { return GetValue(dbResult, defaultValue); }
            public static ushort? GetUShort(object dbResult, ushort? defaultValue = null) { return GetValue(dbResult, defaultValue); }
            public static uint? GetUInt(object dbResult, uint? defaultValue = null) { return GetValue(dbResult, defaultValue); }
            public static ulong? GetULong(object dbResult, ulong? defaultValue = null) { return GetValue(dbResult, defaultValue); }
            public static DateTime? GetDateTime(object dbResult, DateTime? defaultValue = null) { return GetValue(dbResult, defaultValue); }
            public static float? GetFloat(object dbResult, float? defaultValue = null) { return GetValue(dbResult, defaultValue); }
            public static double? GetDouble(object dbResult, double? defaultValue = null) { return GetValue(dbResult, defaultValue); }
            public static decimal? GetDecimal(object dbResult, decimal? defaultValue = null) { return GetValue(dbResult, defaultValue); }
        }
    }
}
