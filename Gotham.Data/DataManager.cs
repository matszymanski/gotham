﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Data
{
    public class DataManager : IDisposable
    {
        private static DataManager _dm = new DataManager();
        //private SqlCommand cmd;
        private SqlConnection _cn;
        public string ConnectionString { get; set; }
        public System.Data.SqlClient.SqlConnection cn
        {
            get
            {
                if (_cn == null || _cn.State == ConnectionState.Closed)
                {
                    _cn = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["."].ConnectionString);
                    //_cn = new SqlConnection(this.ConnectionString);
                    _cn.Open();
                    try
                    {
                        SqlCommand cmd = _cn.CreateCommand();
                        cmd.CommandText = "dbo.sp_SetContext";
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlCommandBuilder.DeriveParameters(cmd);
                        cmd.Parameters["@Context"].Value = "Name:" + System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.Print(ex.Message);
                    }
                }

                return _cn;
            }
        }

        private DataManager()
        {

        }

        public static DataManager GetDM()
        {
            return _dm;
        }

        public bool IsServerConnected()
        {
            if(this.cn.State == ConnectionState.Open)
            {
                return true;
            }
            else
            {
                try
                {
                    this.cn.Open();
                    return true;
                }
                catch
                {
                    return false;
                }
                finally
                {

                }
            }
        }

        public SqlCommand GetCommand(string storedProcedure)
        {
            if (this.cn.State != ConnectionState.Open)
            {
                this.cn.Open();
            }

            SqlCommand cmd = new SqlCommand(storedProcedure, this.cn);
            cmd.CommandType = CommandType.StoredProcedure;
            return cmd;
        }

        public SqlCommand GetCommandT(string sql)
        {
            if (this.cn.State != ConnectionState.Open)
            {
                this.cn.Open();
            }

            SqlCommand cmd = new SqlCommand(sql, this.cn);
            return cmd;
        }

        public DataTable ExecuteDataTable(SqlCommand sqlCommand)
        {
            DataTable data = new DataTable("Table");

            try
            {
                if (sqlCommand.Connection.State != ConnectionState.Open)
                {
                    sqlCommand.Connection.Open();
                }

                if (this.cn.State != ConnectionState.Open)
                {
                    this.cn.Open();
                }

                SqlDataReader dataReader = sqlCommand.ExecuteReader();
                data.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                sqlCommand.Dispose();
            }

            return data;
        }

        public bool ExecuteNonQuery(SqlCommand sqlCommand)
        {
            try
            {
                if (sqlCommand.Connection.State != ConnectionState.Open)
                {
                    sqlCommand.Connection.Open();
                }

                if (this.cn.State != ConnectionState.Open)
                {
                    this.cn.Open();
                }

                sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                System.IO.File.WriteAllText(System.IO.Path.GetTempFileName() + ".txt", sqlCommand.CommandText + ": " + ex.Message);
                System.Diagnostics.Debug.Print(System.IO.Path.GetTempFileName() + ".txt");
                ETRMDataAccess.WriteLogEntry(ex.Message);
                sqlCommand.Dispose();
                return false;
            }
            finally
            {
                sqlCommand.Dispose();
            }

            return true;
        }

        public int ExecuteNonQueryWithReurnValue(SqlCommand sqlCommand)
        {
            int noOfRowsAffected = Int32.MinValue;
            try
            {
                if (sqlCommand.Connection.State != ConnectionState.Open)
                {
                    sqlCommand.Connection.Open();
                }

                if (this.cn.State != ConnectionState.Open)
                {
                    this.cn.Open();
                }

                 noOfRowsAffected = sqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                System.IO.File.WriteAllText(System.IO.Path.GetTempFileName() + ".txt", sqlCommand.CommandText + ": " + ex.Message);
                System.Diagnostics.Debug.Print(System.IO.Path.GetTempFileName() + ".txt");
                ETRMDataAccess.WriteLogEntry(ex.Message);
                sqlCommand.Dispose();
                return noOfRowsAffected;
            }
            finally
            {
                sqlCommand.Dispose();
            }

            return noOfRowsAffected;
        }

        public bool BulkInsertion(DataTable source, string destination)
        {
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(this.cn))
            {
                try
                {
                    bulkCopy.DestinationTableName = destination;
                    bulkCopy.WriteToServer(source);
                }
                catch(Exception ex)
                {
                    ETRMDataAccess.WriteLogEntry(ex.InnerException.Message);
                    ETRMDataAccess.WriteLogEntry(ex.Message);
                    return false;
                }

                return true;
            }
        }

        public void Dispose()
        {
            cn.Close();
        }
    }
}
