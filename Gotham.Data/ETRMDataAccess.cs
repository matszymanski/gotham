﻿using Gotham.Domain;
using Gotham.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Xml;
using System.Configuration;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.IO;
using Gotham.Data.ExRecData;
using Gotham.Domain.Documents;

namespace Gotham.Data
{
    public enum FeeType
    {
        Exchange, OTC
    }
    public enum CashSettlementType
    {
        Fixed, FirstIndex, SecondIndex
    }

    public enum SwapType
    {
        FixedFloat, FloatFloat
    }

    public enum TradingProduct
    {
        Power, Gas, Option, Cash, IR
    }

    public enum PersonnelFunction
    {
        Trading, Sales
    }

    public enum LEBU
    {
        LegalEntity, BusinessUnit
    }

    public enum IntExt
    {
        Internal, External
    }

    public enum PartyFunctionType
    {
        OTCBroker, ClearingBroker, ExecutingBroker, Broker, Exchange
    }

    public static class ETRMDataAccess
    {
        public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable
        {
            return listToClone.Select(item => (T)item.Clone()).ToList();
        }

        public static List<SelectionItems> CreateSelectionItemsList(DataTable source, string idColumn, string valueColumn)
        {
            List<SelectionItems> list = new List<SelectionItems>();

            if (!IsEmpty(source))
            {
                foreach (DataRow row in source.Rows)
                {
                    Int32 identifier = Convert.ToInt32(row[idColumn]);
                    string name = Convert.ToString(row[valueColumn]);
                    SelectionItems item = new SelectionItems(name, identifier);
                    list.Add(item);
                }
            }
            return list;
        }

        public static string GetUserName()
        {
            return System.Security.Principal.WindowsIdentity.GetCurrent().Name;
        }

        public static bool IsEmpty(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                return false;
            }
            return true;
        }

        public static bool IsBroker(Int32 partyId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_IsBroker");
                SqlParameter pparty = new SqlParameter("@PartyId", partyId);
                cmd.Parameters.Add(pparty);
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return (!IsEmpty(dt));
        }

        public static bool IsOTCPriceReference(string ISDAShortName, Int32 referenceID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_IsOTCPriceReference");
                cmd.Parameters.AddWithValue("@ISDAShortName", ISDAShortName);
                cmd.Parameters.Add(CreateInt32Parameter("@OTCReferenceID", referenceID));
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return (!IsEmpty(dt));
        }

        public static bool IsSwapSpreadReference(string ISDAShortName, Int32 referenceID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_IsSwapSpread");
                cmd.Parameters.AddWithValue("@ISDAShortName", ISDAShortName);
                cmd.Parameters.Add(CreateInt32Parameter("@SwapSpreadReferenceID", referenceID));
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return (!IsEmpty(dt));
        }

        public static Int32 DeletePartyBrokerRate(Int32 rateID)
        {
            Int32 result = rateID;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_DeletePartyBrokerRate");
                SqlParameter partyAddressId = new SqlParameter("@PartyBrokerRateID", rateID);
                cmd.Parameters.Add(partyAddressId);
                //DataManager.GetDM().ExecuteNonQuery(cmd);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 DeleteContact(Int32 contactId)
        {
            Int32 result = contactId;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_DeleteContact");
                SqlParameter partyAddressId = new SqlParameter("@PartyContactID", contactId);
                cmd.Parameters.Add(partyAddressId);
                //DataManager.GetDM().ExecuteNonQuery(cmd);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 DeleteAddress(Int32 addressId)
        {
            Int32 result = addressId;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_DeleteAddress");
                SqlParameter partyAddressId = new SqlParameter("@PartyAddressID", addressId);
                cmd.Parameters.Add(partyAddressId);

                //DataManager.GetDM().ExecuteNonQuery(cmd);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 DeleteAgreement(Int32 agreementID)
        {
            Int32 result = agreementID;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_DeleteAgreements");
                SqlParameter partyAddressId = new SqlParameter("@AgreementID", agreementID);
                cmd.Parameters.Add(partyAddressId);

                //DataManager.GetDM().ExecuteNonQuery(cmd);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 UpdateAddress(Domain.Entities.Address add)
        {
            Int32 result = add.PartyAddressID;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_UpdatePartyAddress");

                SqlParameter partyAddressId = new SqlParameter("@PartyAddressID", add.PartyAddressID);
                SqlParameter addressId = new SqlParameter("@PartyID", add.PartyID);
                SqlParameter addressTypeId = new SqlParameter("@PartyAddressTypeID", add.AddressType);
                SqlParameter defaultId = new SqlParameter("@DefaultID", add.AddressDefault);

                SqlParameter addressLine1 = new SqlParameter("@AddressLine1", add.AddressLine1);
                SqlParameter addressLine2 = new SqlParameter("@AddressLine2", add.AddressLine2);
                SqlParameter addressLine3 = new SqlParameter("@AddressLine3", add.AddressLine3);
                SqlParameter addressLine4 = new SqlParameter("@AddressLine4", add.AddressLine4);
                SqlParameter addressLine5 = new SqlParameter("@AddressLine5", add.AddressLine5);

                SqlParameter cityID = new SqlParameter("@City", add.CityId);
                SqlParameter stateID = new SqlParameter("@StateID", add.StateId);
                SqlParameter sountryID = new SqlParameter("@CountryID", add.CountryId);

                SqlParameter mailCode = new SqlParameter("@MailCode", add.MailCode);
                SqlParameter contactId = new SqlParameter("@ContactID", add.ContactId);
                SqlParameter groupEmail = new SqlParameter("@GroupEmail", add.GroupEmail);
                SqlParameter groupPhone = new SqlParameter("@GroupPhone", add.GroupPhone);
                SqlParameter groupFax = new SqlParameter("@GroupFax", add.GroupFax);
                SqlParameter description = new SqlParameter("@Description", add.Description);
                SqlParameter effectiveDate = new SqlParameter("@EffectiveDate", DateTime.Now);
                SqlParameter updateUser = new SqlParameter("@UpdateUser", add.UpdateUser);

                cmd.Parameters.Add(partyAddressId);
                cmd.Parameters.Add(addressId);
                cmd.Parameters.Add(addressTypeId);
                cmd.Parameters.Add(defaultId);
                cmd.Parameters.Add(addressLine1);
                cmd.Parameters.Add(addressLine2);
                cmd.Parameters.Add(addressLine3);
                cmd.Parameters.Add(addressLine4);
                cmd.Parameters.Add(addressLine5);
                cmd.Parameters.Add(cityID);
                cmd.Parameters.Add(stateID);
                cmd.Parameters.Add(sountryID);
                cmd.Parameters.Add(mailCode);
                cmd.Parameters.Add(contactId);
                cmd.Parameters.Add(groupEmail);
                cmd.Parameters.Add(groupPhone);
                cmd.Parameters.Add(groupFax);
                cmd.Parameters.Add(description);
                cmd.Parameters.Add(effectiveDate);
                cmd.Parameters.Add(updateUser);

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
            }
            return result;
        }

        public static Int32 UpdateContact(Domain.Entities.Contact contact)
        {
            Int32 result = contact.PartyContactID;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_UpdateContact");

                SqlParameter partyContactId = new SqlParameter("@PartyContactID", contact.PartyContactID);
                SqlParameter partyId = new SqlParameter("@PartyID", contact.PartyID);
                SqlParameter addressTypeId = new SqlParameter("@PartyAddressTypeID", contact.PartyAddressTypeID);
                SqlParameter titleId = new SqlParameter("@PartyTitleID", contact.PartyTitleID);
                SqlParameter defaultId = new SqlParameter("@DefaultId", contact.DefaultId);
                SqlParameter firstName = new SqlParameter("@FirstName", contact.FirstName);
                SqlParameter lastName = new SqlParameter("@LastName", contact.LastName);
                SqlParameter phoneNr = new SqlParameter("@PhoneNumber", contact.PhoneNumber);
                SqlParameter email = new SqlParameter("@Email", contact.Email);
                SqlParameter faxNumber = new SqlParameter("@FaxNumber", contact.FaxNumber);
                SqlParameter updateUser = new SqlParameter("@UpdateUser", contact.UpdateUser);
                SqlParameter isActive = new SqlParameter("@IsActive", contact.IsActive);
                SqlParameter deleted = new SqlParameter("@Deleted", false);

                cmd.Parameters.Add(partyContactId);
                cmd.Parameters.Add(titleId);
                cmd.Parameters.Add(partyId);
                cmd.Parameters.Add(addressTypeId);
                cmd.Parameters.Add(defaultId);
                cmd.Parameters.Add(firstName);
                cmd.Parameters.Add(lastName);
                cmd.Parameters.Add(phoneNr);
                cmd.Parameters.Add(email);
                cmd.Parameters.Add(faxNumber);
                cmd.Parameters.Add(updateUser);
                cmd.Parameters.Add(deleted);
                cmd.Parameters.Add(isActive);

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
            }
            return result;
        }

        public static Int32 SaveContact(Domain.Entities.Contact contact)
        {
            Int32 result = contact.PartyContactID;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_InsertContact");
                SqlParameter partyContactID = new SqlParameter("@PartyContactID", contact.PartyContactID);
                SqlParameter partyAddressTypeID = new SqlParameter("@PartyAddressTypeID", contact.PartyAddressTypeID);
                SqlParameter partyTitleID = new SqlParameter("@PartyTitleID", contact.PartyTitleID);
                SqlParameter firstName = new SqlParameter("@FirstName", contact.FirstName);
                SqlParameter lastName = new SqlParameter("@LastName", contact.LastName);
                SqlParameter defaultId = new SqlParameter("@DefaultId", contact.DefaultId);
                SqlParameter phoneNr = new SqlParameter("@PhoneNumber", contact.PhoneNumber);
                SqlParameter email = new SqlParameter("@Email", contact.Email);
                SqlParameter faxNumber = new SqlParameter("@FaxNumber", contact.FaxNumber);
                SqlParameter partyID = new SqlParameter("@PartyID", contact.PartyID);
                SqlParameter deleted = new SqlParameter("@Deleted", false);
                SqlParameter isActive = new SqlParameter("@IsActive", contact.IsActive);
                SqlParameter updateUser = new SqlParameter("@UpdateUser", contact.UpdateUser);

                cmd.Parameters.Add(partyContactID);
                cmd.Parameters.Add(partyID);
                cmd.Parameters.Add(partyAddressTypeID);
                cmd.Parameters.Add(defaultId);
                cmd.Parameters.Add(firstName);
                cmd.Parameters.Add(lastName);
                cmd.Parameters.Add(phoneNr);
                cmd.Parameters.Add(email);
                cmd.Parameters.Add(faxNumber);
                cmd.Parameters.Add(partyTitleID);
                cmd.Parameters.Add(deleted);
                cmd.Parameters.Add(isActive);
                cmd.Parameters.Add(updateUser);

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
            }
            return result;
        }

        public static Int32 UpdatePartyBrokerRate(BrokerRate rate)
        {
            Int32 result = rate.BrokerRateTypeID;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_UpdatePartyBrokerRate");

                SqlParameter brokerId = new SqlParameter("@BrokerID", rate.BrokerID);
                SqlParameter partyBrokerRateId = new SqlParameter("@PartyBrokerRateID", rate.PartyBrokerRateID);

                SqlParameter exchangeId = new SqlParameter("@ExchangeID", rate.ExchangeID);
                if (rate.ExchangeID == Int32.MinValue)
                {
                    exchangeId = new SqlParameter("@ExchangeID", DBNull.Value);
                }

                SqlParameter executionTypeId = new SqlParameter("@ExecutionTypeID", rate.ExecutionTypeID);
                if (rate.ExecutionTypeID == Int32.MinValue)
                {
                    executionTypeId = new SqlParameter("@ExecutionTypeID", DBNull.Value);
                }

                SqlParameter settlementTypeId = new SqlParameter("@SettlementTypeID", rate.SettlementTypeID);
                SqlParameter underlyingId = new SqlParameter("@UnderlyingID", rate.UnderlyingID);
                SqlParameter tradeTypeID = new SqlParameter("@TradeTypeID", rate.TradeTypeID);
                SqlParameter feeRate = new SqlParameter("@FeeRate", rate.FeeRate);
                SqlParameter feeTypeId = new SqlParameter("@FeeTypeID", rate.FeeTypeID);
                SqlParameter brokerRateTypeId = new SqlParameter("@BrokerRateTypeID", rate.BrokerRateTypeID);
                SqlParameter rateCurrencyId = new SqlParameter("@RateCurrencyID", rate.RateCurrencyID);
                SqlParameter paymentFrequencyId = new SqlParameter("@PaymentFrequencyID", rate.PaymentFrequencyID);
                SqlParameter paymentDateOffset = new SqlParameter("@PaymentDateOffset", rate.PaymentDateOffset);
                SqlParameter defaultRate = new SqlParameter("@DefaultRate", rate.DefaultRate);

                SqlParameter miniFeeRate = new SqlParameter("@MiniFeeRate", rate.MiniFeeRate);
                if (rate.MiniFeeRate == null)
                {
                    miniFeeRate = new SqlParameter("@MiniFeeRate", DBNull.Value);
                }

                SqlParameter miniBrokerRateTypeId = new SqlParameter("@MiniBrokerRateTypeID", rate.MiniBrokerRateTypeID);
                if (rate.MiniBrokerRateTypeID < 0 || rate.MiniBrokerRateTypeID == null)
                {
                    miniBrokerRateTypeId = new SqlParameter("@MiniBrokerRateTypeID", DBNull.Value);
                }

                cmd.Parameters.Add(brokerId);
                cmd.Parameters.Add(partyBrokerRateId);
                cmd.Parameters.Add(exchangeId);
                cmd.Parameters.Add(executionTypeId);
                cmd.Parameters.Add(settlementTypeId);
                cmd.Parameters.Add(underlyingId);
                cmd.Parameters.Add(tradeTypeID);
                cmd.Parameters.Add(feeRate);
                cmd.Parameters.Add(brokerRateTypeId);
                cmd.Parameters.Add(rateCurrencyId);
                cmd.Parameters.Add(paymentFrequencyId);
                cmd.Parameters.Add(paymentDateOffset);
                cmd.Parameters.Add(feeTypeId);
                cmd.Parameters.Add(defaultRate);
                cmd.Parameters.Add(miniFeeRate);
                cmd.Parameters.Add(miniBrokerRateTypeId);

                //DataManager.GetDM().ExecuteNonQuery(cmd);
                cmd.ExecuteNonQuery();

                if (rate.DefaultRate == true)
                {
                    SetDefaultRate(rate);
                }
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 SavePartyBrokerRate(BrokerRate rate)
        {
            Int32 result = rate.PartyBrokerRateID;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_InsertPartyBrokerRate");

                SqlParameter brokerId = new SqlParameter("@BrokerID", rate.BrokerID);
                SqlParameter partyBrokerRateId = new SqlParameter("@PartyBrokerRateID", rate.PartyBrokerRateID);

                SqlParameter exchangeId = new SqlParameter("@ExchangeID", rate.ExchangeID);
                if (rate.ExchangeID == Int32.MinValue)
                {
                    exchangeId = new SqlParameter("@ExchangeID", DBNull.Value);
                }

                SqlParameter executionTypeId = new SqlParameter("@ExecutionTypeID", rate.ExecutionTypeID);
                if (rate.ExecutionTypeID == Int32.MinValue)
                {
                    executionTypeId = new SqlParameter("@ExecutionTypeID", DBNull.Value);
                }

                SqlParameter settlementTypeId = new SqlParameter("@SettlementTypeID", rate.SettlementTypeID);
                SqlParameter underlyingId = new SqlParameter("@UnderlyingID", rate.UnderlyingID);
                SqlParameter tradeTypeID = new SqlParameter("@TradeTypeID", rate.TradeTypeID);

                SqlParameter feeRate = new SqlParameter("@FeeRate", rate.FeeRate);

                SqlParameter miniFeeRate = new SqlParameter("@MiniFeeRate", rate.MiniFeeRate);
                if (rate.MiniFeeRate == null)
                {
                    miniFeeRate = new SqlParameter("@MiniFeeRate", DBNull.Value);
                }

                SqlParameter miniBrokerRateTypeId = new SqlParameter("@MiniBrokerRateTypeID", rate.MiniBrokerRateTypeID);

                if (rate.MiniBrokerRateTypeID == null || rate.MiniBrokerRateTypeID < 0)
                {
                    miniBrokerRateTypeId = new SqlParameter("@MiniBrokerRateTypeID", DBNull.Value);
                }

                SqlParameter feeTypeId = new SqlParameter("@FeeTypeID", rate.FeeTypeID);
                SqlParameter brokerRateTypeId = new SqlParameter("@BrokerRateTypeID", rate.BrokerRateTypeID);
                SqlParameter rateCurrencyId = new SqlParameter("@RateCurrencyID", rate.RateCurrencyID);
                SqlParameter paymentFrequencyId = new SqlParameter("@PaymentFrequencyID", rate.PaymentFrequencyID);
                SqlParameter paymentDateOffset = new SqlParameter("@PaymentDateOffset", rate.PaymentDateOffset);
                SqlParameter description = new SqlParameter("@Description", string.Empty);
                SqlParameter defaultRate = new SqlParameter("@DefaultRate", rate.DefaultRate);

                cmd.Parameters.Add(brokerId);
                cmd.Parameters.Add(partyBrokerRateId);
                cmd.Parameters.Add(exchangeId);
                cmd.Parameters.Add(settlementTypeId);
                cmd.Parameters.Add(executionTypeId);
                cmd.Parameters.Add(underlyingId);
                cmd.Parameters.Add(tradeTypeID);
                cmd.Parameters.Add(feeRate);
                cmd.Parameters.Add(brokerRateTypeId);
                cmd.Parameters.Add(rateCurrencyId);
                cmd.Parameters.Add(paymentFrequencyId);
                cmd.Parameters.Add(paymentDateOffset);
                cmd.Parameters.Add(description);
                cmd.Parameters.Add(feeTypeId);
                cmd.Parameters.Add(defaultRate);
                cmd.Parameters.Add(miniBrokerRateTypeId);
                cmd.Parameters.Add(miniFeeRate);

                //DataManager.GetDM().ExecuteNonQuery(cmd);
                cmd.ExecuteNonQuery();

                if (rate.DefaultRate == true)
                {
                    SetDefaultRate(rate);
                }
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        private static void SetDefaultRate(BrokerRate rate)
        {
            try
            {
                string sql2 = @"UPDATE PartyBrokerRate SET
                                DefaultRate = 0
                                WHERE 
                                    FeeTypeID=@FeeTypeID and
                                    ExchangeID=@ExchangeID and
                                    ExecutionTypeID=@ExecutionTypeID and
                                    UnderlyingID=@UnderlyingID and
                                    TradeTypeID=@TradeTypeID and
                                    SettlementTypeID=@SettlementTypeID and 
                                    RateCurrencyID=@RateCurrencyID and
                                    PartyBrokerRateID != @PartyBrokerRateID
                                ";

                SqlParameter partyBrokerRateId = new SqlParameter("@PartyBrokerRateID", rate.PartyBrokerRateID);
                SqlParameter feeTypeId = new SqlParameter("@FeeTypeID", rate.FeeTypeID);
                SqlParameter brokerId = new SqlParameter("@BrokerID", rate.BrokerID);

                SqlParameter exchangeId;

                if (rate.ExchangeID != Int32.MinValue)
                {
                    exchangeId = new SqlParameter("@ExchangeID", rate.ExchangeID);
                }
                else
                {
                    exchangeId = new SqlParameter("@ExchangeID", DBNull.Value);
                }

                SqlParameter executionTypeId = new SqlParameter("@ExecutionTypeID", rate.ExecutionTypeID);
                if (rate.ExecutionTypeID == Int32.MinValue)
                {
                    executionTypeId = new SqlParameter("@ExecutionTypeID", DBNull.Value);
                }

                SqlParameter underlyingId = new SqlParameter("@UnderlyingID", rate.UnderlyingID);
                SqlParameter tradeTypeId = new SqlParameter("@TradeTypeID", rate.TradeTypeID);
                SqlParameter settlementTypeId = new SqlParameter("@SettlementTypeID", rate.SettlementTypeID);
                SqlParameter rateCurrencyId = new SqlParameter("@RateCurrencyID", rate.RateCurrencyID);

                SqlCommand cmd = DataManager.GetDM().GetCommandT(sql2);

                cmd.Parameters.Add(brokerId);
                cmd.Parameters.Add(partyBrokerRateId);
                cmd.Parameters.Add(exchangeId);
                cmd.Parameters.Add(executionTypeId);
                cmd.Parameters.Add(settlementTypeId);
                cmd.Parameters.Add(underlyingId);
                cmd.Parameters.Add(tradeTypeId);
                cmd.Parameters.Add(rateCurrencyId);
                cmd.Parameters.Add(feeTypeId);

                //DataManager.GetDM().ExecuteNonQuery(cmd);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
            }
        }

        public static Int32 SaveAddress(Domain.Entities.Address add)
        {
            Int32 result = add.PartyAddressID;

            try
            {
                string sql2 = @"INSERT INTO PartyAddress (
                                PartyAddressID,
                                PartyID,
                                PartyAddressTypeID,
                                DefaultID,
                                AddressLine1,
                                AddressLine2,
                                AddressLine3,
                                AddressLine4,
                                AddressLine5,
                                City,
                                StateID,
                                CountryID,
                                MailCode,
                                ContactID,
                                GroupEmail,
                                GroupPhone,
                                GroupFax,
                                Description,
                                EffectiveDate,
                                UpdateUser) 
                                VALUES (
                                @PartyAddressID,
                                @PartyID,
                                @PartyAddressTypeID,
                                @DefaultID,
                                @AddressLine1,
                                @AddressLine2,
                                @AddressLine3,
                                @AddressLine4,
                                @AddressLine5,
                                @City,
                                @StateID,
                                @CountryID,
                                @MailCode,
                                @ContactID,
                                @GroupEmail,
                                @GroupPhone,
                                @GroupFax,
                                @Description,
                                @EffectiveDate,
                                @UpdateUser)";


                SqlParameter partyAddressId = new SqlParameter("@PartyAddressID", add.PartyAddressID);
                SqlParameter addressId = new SqlParameter("@PartyID", add.PartyID);
                SqlParameter addressTypeId = new SqlParameter("@PartyAddressTypeID", add.AddressType);
                SqlParameter defaultId = new SqlParameter("@DefaultID", add.AddressDefault);
                SqlParameter updateUser = new SqlParameter("@UpdateUser", add.UpdateUser);
                SqlParameter addressLine1 = new SqlParameter("@AddressLine1", add.AddressLine1);
                SqlParameter addressLine2 = new SqlParameter("@AddressLine2", add.AddressLine2);
                SqlParameter addressLine3 = new SqlParameter("@AddressLine3", add.AddressLine3);
                SqlParameter addressLine4 = new SqlParameter("@AddressLine4", add.AddressLine4);
                SqlParameter addressLine5 = new SqlParameter("@AddressLine5", add.AddressLine5);

                SqlParameter city = new SqlParameter("@City", add.CityId);
                SqlParameter stateID = new SqlParameter("@StateID", add.StateId);
                SqlParameter sountryID = new SqlParameter("@CountryID", add.CountryId);

                SqlParameter mailCode = new SqlParameter("@MailCode", add.MailCode);
                SqlParameter contactId = new SqlParameter("@ContactID", add.ContactId);
                SqlParameter groupEmail = new SqlParameter("@GroupEmail", add.GroupEmail);
                SqlParameter groupPhone = new SqlParameter("@GroupPhone", add.GroupPhone);
                SqlParameter groupFax = new SqlParameter("@GroupFax", add.GroupFax);
                SqlParameter description = new SqlParameter("@Description", add.Description);
                SqlParameter effectiveDate = new SqlParameter("@EffectiveDate", DateTime.Now);

                SqlCommand cmd = DataManager.GetDM().GetCommandT(sql2);

                cmd.Parameters.Add(partyAddressId);
                cmd.Parameters.Add(addressId);
                cmd.Parameters.Add(addressTypeId);
                cmd.Parameters.Add(defaultId);
                cmd.Parameters.Add(addressLine1);
                cmd.Parameters.Add(addressLine2);
                cmd.Parameters.Add(addressLine3);
                cmd.Parameters.Add(addressLine4);
                cmd.Parameters.Add(addressLine5);
                cmd.Parameters.Add(city);
                cmd.Parameters.Add(stateID);
                cmd.Parameters.Add(sountryID);
                cmd.Parameters.Add(mailCode);
                cmd.Parameters.Add(contactId);
                cmd.Parameters.Add(groupEmail);
                cmd.Parameters.Add(groupPhone);
                cmd.Parameters.Add(groupFax);
                cmd.Parameters.Add(description);
                cmd.Parameters.Add(effectiveDate);
                cmd.Parameters.Add(updateUser);

                //DataManager.GetDM().ExecuteNonQuery(cmd);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 UpdateBusinessUnit(BusinessUnit bu)
        {
            Int32 result = bu.PartyId;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_UpdateParty");
                SqlParameter shortName = new SqlParameter("@ShortName", bu.ShortName);
                SqlParameter longName = new SqlParameter("@LongName", bu.LongName);
                SqlParameter updateUser = new SqlParameter("@UpdateUser", bu.UpdateUser);
                SqlParameter partyClassId = new SqlParameter("@PartyClassId", bu.PartyClass);

                SqlParameter lei;
                if (string.IsNullOrEmpty(bu.LEI))
                {
                    lei = new SqlParameter("@LEI", DBNull.Value);
                }
                else
                {
                    lei = new SqlParameter("@LEI", bu.LEI);
                }

                SqlParameter authoriser;
                if (bu.AuthoriserId.HasValue)
                {
                    authoriser = new SqlParameter("@AuthoriserId", bu.AuthoriserId.Value);
                }
                else
                {
                    authoriser = new SqlParameter("@AuthoriserId", DBNull.Value);
                }

                SqlParameter status = new SqlParameter("@PartyStatusId", bu.PartyStatusId);
                SqlParameter spiderId = new SqlParameter("@SpiderId", bu.SpiderId);
                SqlParameter intExt = new SqlParameter("@InternalExternalId", bu.InternalExternal);
                SqlParameter partyId = new SqlParameter("@PartyID", bu.PartyId);

                cmd.Parameters.Add(shortName);
                cmd.Parameters.Add(longName);
                cmd.Parameters.Add(updateUser);
                cmd.Parameters.Add(partyId);
                cmd.Parameters.Add(partyClassId);
                cmd.Parameters.Add(lei);
                cmd.Parameters.Add(status);
                cmd.Parameters.Add(authoriser);
                cmd.Parameters.Add(spiderId);
                cmd.Parameters.Add(intExt);

                cmd.Parameters.Add(CreateStringParameter("@SchedulingPartyName", bu.SchedulingPartyName));
                cmd.Parameters.Add(CreateStringParameter("@MICCode", bu.MICCode));
                cmd.Parameters.Add(CreateStringParameter("@TaxID", bu.TaxID));
                cmd.Parameters.Add(CreateInt32Parameter("@ConfirmingParty", bu.ConfirmingPartyID));
                cmd.Parameters.Add(CreateInt32Parameter("@DoddFrankType", bu.DoddFrankTypeID));
                cmd.Parameters.Add(CreateInt32Parameter("@DoddFrankReportingPartyId", bu.DoddFrankReportingParty));
                cmd.Parameters.Add(CreateInt32Parameter("@TradingCountry", bu.PrincipalTradingCountry));
                cmd.Parameters.Add(CreateInt32Parameter("@RegisteredCountry", bu.RegisteredCountry));
                cmd.Parameters.Add(CreateInt32Parameter("@TaxDomicile", bu.TaxDomicileID));
                cmd.Parameters.Add(CreateInt32Parameter("@TaxStatus", bu.TaxStatusID));
                cmd.Parameters.Add(CreateBoolParameter("@IConRequired", bu.IConReq));
                cmd.Parameters.Add(CreateBoolParameter("@OConRequired", bu.OConReq));
                cmd.Parameters.Add(CreateBoolParameter("@InvoiceRequired", bu.InvoiceReq));
                cmd.Parameters.Add(CreateBoolParameter("@CommercialParticipant", bu.CommercialParticipant));
                cmd.Parameters.Add(CreateInt32Parameter("@ConfirmMethodID", bu.ConfirmMethodID));
                cmd.Parameters.Add(CreateStringParameter("@BeaconID", bu.BeaconID));

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }

            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
            }
            return result;
        }

        public static Int32 GetTradeLegID(Int32 tradeId)
        {
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetTradeLegID");
                SqlParameter TradeId = CreateInt32Parameter("@TradeId", tradeId);
                cmd.Parameters.Add(TradeId);

                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }

            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static Int32 GetTradeEventID(Int32 tradeId, Int32 tradeLegId, Int32 eventSubTypeId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetTradeEventID");

                SqlParameter TradeId = CreateInt32Parameter("@TradeId", tradeId);
                cmd.Parameters.Add(TradeId);
                SqlParameter LegId = CreateInt32Parameter("@LegId", tradeLegId);
                cmd.Parameters.Add(LegId);
                SqlParameter EventSubTypeId = CreateInt32Parameter("@EventSubTypeId", eventSubTypeId);
                cmd.Parameters.Add(EventSubTypeId);

                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }

            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static Int32 GetNextTradeLegID()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetNextTradeLegID");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }

            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static Int32 GetNextTradeEventID()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetNextTradeEventID");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }

            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static Int32 GetNextTradeID()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetNextTradeID");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }

            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static Int32 GetNextValuationFuturesDataSequenceID()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetNextValuationFuturesDataSequenceID");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }

            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static Int32 GetNextValuationControlSequence()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetNextValuationControlSequence");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }

            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static Int32 GetNextTradeStructureDefinitionSequenceID()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetNextTradeStructureDefinitionSequenceID");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }

            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static Int32 GetNextAgreementID()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetNextAgreementID");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }

            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static Int32 SaveFutureTradeLegSP(TradeLeg t)
        {
            try
            {
                SqlParameter tLegId = new SqlParameter("@TradeLegId", t.TradeLegId);
                SqlParameter tId = new SqlParameter("@TradeId", t.TradeId);
                SqlParameter SettlementTypeId = CreateInt32Parameter("@SettlementTypeId", t.SettlementTypeId);
                SqlParameter OptionTypeId = CreateInt32Parameter("@OptionTypeId", t.OptionTypeId);
                SqlParameter PricingIndexId = CreateInt32Parameter("@PricingIndexId", t.PricingIndexId);
                SqlParameter PricePercentage = CreateDecimalParameter("@PricePercentage", t.PricePercentage);
                SqlParameter PricingCurrencyId = CreateInt32Parameter("@PricingCurrencyId", t.PricingCurrencyId);
                SqlParameter Position = CreateInt32Parameter("@Position", t.Position);
                SqlParameter Price = CreateDecimalParameter("@Price", t.Price);
                SqlParameter Notional = CreateDecimalParameter("@Notional", t.Notional);
                SqlParameter UnitId = CreateInt32Parameter("@UnitId", t.UnitId);
                SqlParameter StartDate = CreateDateTimeParameter("@StartDate", t.StartDate);
                SqlParameter EndDate = CreateDateTimeParameter("@EndDate", t.EndDate);
                SqlParameter DeliveryTypeId = CreateInt32Parameter("@DeliveryTypeId", t.DeliveryTypeId);
                SqlParameter UpdateUser = CreateInt32Parameter("@UpdateUser", t.UpdateUser);

                SqlCommand cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_InsertTradeLeg");

                cmd.Parameters.Add(tLegId);
                cmd.Parameters.Add(tId);
                cmd.Parameters.Add(SettlementTypeId);
                cmd.Parameters.Add(OptionTypeId);
                cmd.Parameters.Add(PricingIndexId);
                cmd.Parameters.Add(PricePercentage);
                cmd.Parameters.Add(Position);
                cmd.Parameters.Add(Price);
                cmd.Parameters.Add(PricingCurrencyId);
                cmd.Parameters.Add(Notional);
                cmd.Parameters.Add(UnitId);
                cmd.Parameters.Add(StartDate);
                cmd.Parameters.Add(EndDate);
                cmd.Parameters.Add(DeliveryTypeId);
                cmd.Parameters.Add(UpdateUser);

                cmd.ExecuteNonQuery();
                //DataManager.GetDM().ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }
            return t.TradeLegId;
        }

        public static Int32 SaveValuationControlRecord(Int32 valuationControlID, Int32 valuationCOntrolType, DateTime current, DateTime previous, string createdBy)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_InsertValuationControl");
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@ValuationControlID", valuationControlID);
                cmd.Parameters.AddWithValue("@ValuationTypeID", valuationCOntrolType);
                cmd.Parameters.AddWithValue("@CurrentValuationDate", current);
                cmd.Parameters.AddWithValue("@PreviousValuationDate", previous);
                cmd.Parameters.AddWithValue("@CreatedBy", createdBy);

                DataManager.GetDM().ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }
            return valuationControlID;
        }

        public static Int32 SaveExRecControlRecord(Int32 exRecID, DateTime exRecDate, string createdBy)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("dbo.sp_ExRec_InsertValuationControl");
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@ExRecControlID", exRecID);
                cmd.Parameters.AddWithValue("@ExRecDate", exRecDate);
                cmd.Parameters.AddWithValue("@CreatedBy", createdBy);

                DataManager.GetDM().ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }
            return exRecID;
        }

        public static Int32 SaveFuturesStructuredDeal(FuturesStructuredDeal deal)
        {
            try
            {
                SqlParameter TradeStructureDefinitionID = new SqlParameter("@TradeStructureDefinitionID", deal.TradeStructureDefinitionID);
                SqlParameter SeasonId = new SqlParameter("@SeasonId", deal.SeasonID);
                SqlParameter StartDate = new SqlParameter("@StartDate", deal.StartDate);
                SqlParameter StopDate = new SqlParameter("@StopDate", deal.StopDate);

                SqlCommand cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_InsertTradeStructureDefinition");

                cmd.Parameters.Add(TradeStructureDefinitionID);
                cmd.Parameters.Add(SeasonId);
                cmd.Parameters.Add(StartDate);
                cmd.Parameters.Add(StopDate);

                DataManager.GetDM().ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }
            return deal.TradeStructureDefinitionID;
        }

        public static Int32 UpdateFuturesStructuredDeal(FuturesStructuredDeal deal)
        {
            try
            {
                SqlParameter TradeStructureDefinitionID = new SqlParameter("@TradeStructureDefinitionID", deal.TradeStructureDefinitionID);
                SqlParameter SeasonId = new SqlParameter("@SeasonId", deal.SeasonID);
                SqlParameter StartDate = new SqlParameter("@StartDate", deal.StartDate);
                SqlParameter StopDate = new SqlParameter("@StopDate", deal.StopDate);

                SqlCommand cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_UpdateTradeStructureDefinition");

                cmd.Parameters.Add(TradeStructureDefinitionID);
                cmd.Parameters.Add(SeasonId);
                cmd.Parameters.Add(StartDate);
                cmd.Parameters.Add(StopDate);

                DataManager.GetDM().ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }
            return deal.TradeStructureDefinitionID;
        }

        public static bool CompareFuturesStructuredDeal(FuturesStructuredDeal deal)
        {
            bool result = false;

            try
            {
                DataTable existingStructure = ETRMDataAccess.GetStructureDetails(deal.TradeStructureDefinitionID);

                if (!ETRMDataAccess.IsEmpty(existingStructure))
                {
                    if (GetInt32DTRElement(existingStructure, StructureDetails.SeasonId).HasValue && deal.SeasonID == GetInt32DTRElement(existingStructure, StructureDetails.SeasonId).Value)
                    {
                        if (GetDateTimeDTRElement(existingStructure, StructureDetails.StartDate).HasValue && deal.StartDate == GetDateTimeDTRElement(existingStructure, StructureDetails.StartDate).Value)
                        {
                            if (GetDateTimeDTRElement(existingStructure, StructureDetails.StopDate).HasValue && deal.StopDate == GetDateTimeDTRElement(existingStructure, StructureDetails.StopDate).Value)
                            {
                                result = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
                return false;
            }
            return result;
        }

        public static Int32 SaveFutureTradeEventSP(TradeEvent t)
        {
            try
            {
                SqlParameter TradeEventId = new SqlParameter("@TradeEventId", t.TradeEventID);
                SqlParameter TradeId = new SqlParameter("@TradeId", t.TradeId);
                SqlParameter LegId = new SqlParameter("@LegId", t.LegId);
                SqlParameter EventTypeId = new SqlParameter("@EventTypeId", t.EventTypeId);
                SqlParameter EventSubTypeId = CreateInt32Parameter("@EventSubTypeId", t.EventSubTypeId);
                SqlParameter ExternalBusinessUnitId = CreateInt32Parameter("@ExternalBusinessUnitId", t.ExternalBusinessUnitId);
                SqlParameter Price = CreateDecimalParameter("@Price", t.Price);
                SqlParameter Notional = CreateDecimalParameter("@Notional", t.Notional);
                SqlParameter NotionalUnitId = CreateInt32Parameter("@NotionalUnitId", t.NotionalUnitId);
                SqlParameter Amount = CreateDecimalParameter("@Amount", t.Amount);
                SqlParameter CurrencyId = CreateInt32Parameter("@CurrencyId", t.CurrencyId);
                SqlParameter EventDate = new SqlParameter("@EventDate", t.EventDate);
                SqlParameter EventStatusId = new SqlParameter("@EventStatusId", t.EventStatusId);
                SqlParameter UpdateUser = CreateInt32Parameter("@UpdateUser", t.UpdateUser);

                SqlCommand cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_InsertTradeEvent");

                cmd.Parameters.Add(TradeEventId);
                cmd.Parameters.Add(TradeId);
                cmd.Parameters.Add(LegId);
                cmd.Parameters.Add(EventTypeId);
                cmd.Parameters.Add(EventSubTypeId);
                cmd.Parameters.Add(ExternalBusinessUnitId);
                cmd.Parameters.Add(Price);
                cmd.Parameters.Add(Notional);
                cmd.Parameters.Add(NotionalUnitId);
                cmd.Parameters.Add(Amount);
                cmd.Parameters.Add(CurrencyId);
                cmd.Parameters.Add(EventDate);
                cmd.Parameters.Add(EventStatusId);
                cmd.Parameters.Add(UpdateUser);

                DataManager.GetDM().ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }
            return t.TradeEventID;
        }

        public static Int32 UpdateFutureTradeEventSP(TradeEvent t)
        {
            try
            {
                SqlParameter TradeEventId = new SqlParameter("@TradeEventId", t.TradeEventID);
                SqlParameter TradeId = new SqlParameter("@TradeId", t.TradeId);
                SqlParameter LegId = new SqlParameter("@LegId", t.LegId);
                SqlParameter EventTypeId = new SqlParameter("@EventTypeId", t.EventTypeId);
                SqlParameter EventSubTypeId = CreateInt32Parameter("@EventSubTypeId", t.EventSubTypeId);
                SqlParameter ExternalBusinessUnitId = CreateInt32Parameter("@ExternalBusinessUnitId", t.ExternalBusinessUnitId);
                SqlParameter Price = CreateDecimalParameter("@Price", t.Price);
                SqlParameter Notional = CreateDecimalParameter("@Notional", t.Notional);
                SqlParameter NotionalUnitId = CreateInt32Parameter("@NotionalUnitId", t.NotionalUnitId);
                SqlParameter Amount = CreateDecimalParameter("@Amount", t.Amount);
                SqlParameter CurrencyId = CreateInt32Parameter("@CurrencyId", t.CurrencyId);
                SqlParameter EventDate = new SqlParameter("@EventDate", t.EventDate);
                SqlParameter EventStatusId = new SqlParameter("@EventStatusId", t.EventStatusId);
                SqlParameter UpdateUser = CreateInt32Parameter("@UpdateUser", t.UpdateUser);

                SqlCommand cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_UpdateTradeEvent");

                cmd.Parameters.Add(TradeEventId);
                cmd.Parameters.Add(TradeId);
                cmd.Parameters.Add(LegId);
                cmd.Parameters.Add(EventTypeId);
                cmd.Parameters.Add(EventSubTypeId);
                cmd.Parameters.Add(ExternalBusinessUnitId);
                cmd.Parameters.Add(Price);
                cmd.Parameters.Add(Notional);
                cmd.Parameters.Add(NotionalUnitId);
                cmd.Parameters.Add(Amount);
                cmd.Parameters.Add(CurrencyId);
                cmd.Parameters.Add(EventDate);
                cmd.Parameters.Add(EventStatusId);
                cmd.Parameters.Add(UpdateUser);

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    return Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }
            return t.TradeEventID;
        }

        public static Int32 UpdateTradeLegSP(TradeLeg t)
        {
            try
            {
                SqlParameter tLegId = CreateInt32Parameter("@TradeLegId", t.TradeLegId);
                SqlParameter tId = CreateInt32Parameter("@TradeId", t.TradeId);
                SqlParameter SettlementTypeId = CreateInt32Parameter("@SettlementTypeId", t.SettlementTypeId);
                SqlParameter OptionTypeId = CreateInt32Parameter("@OptionTypeId", t.OptionTypeId);
                SqlParameter PricingIndexId = CreateInt32Parameter("@PricingIndexId", t.PricingIndexId);
                SqlParameter PricingCurrencyId = CreateInt32Parameter("@PricingCurrencyId", t.PricingCurrencyId);
                SqlParameter Position = CreateInt32Parameter("@Position", t.Position);
                SqlParameter PricePercentage = CreateDecimalParameter("@PricePercentage", t.PricePercentage);
                SqlParameter Price = CreateDecimalParameter("@Price", t.Price);
                SqlParameter Notional = CreateDecimalParameter("@Notional", t.Notional);
                SqlParameter UnitId = CreateInt32Parameter("@UnitId", t.UnitId);
                SqlParameter StartDate = CreateDateTimeParameter("@StartDate", t.StartDate);
                SqlParameter EndDate = CreateDateTimeParameter("@EndDate", t.EndDate);
                SqlParameter DeliveryTypeId = CreateInt32Parameter("@DeliveryTypeId", t.DeliveryTypeId);
                SqlParameter UserId = new SqlParameter("@UpdateUser", t.UpdateUser);

                SqlCommand cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_UpdateTradeLeg");

                cmd.Parameters.Add(tLegId);
                cmd.Parameters.Add(tId);
                cmd.Parameters.Add(SettlementTypeId);
                cmd.Parameters.Add(OptionTypeId);
                cmd.Parameters.Add(PricingIndexId);
                cmd.Parameters.Add(PricePercentage);
                cmd.Parameters.Add(Position);
                cmd.Parameters.Add(PricingCurrencyId);
                cmd.Parameters.Add(Price);
                cmd.Parameters.Add(Notional);
                cmd.Parameters.Add(UnitId);
                cmd.Parameters.Add(StartDate);
                cmd.Parameters.Add(EndDate);
                cmd.Parameters.Add(UserId);

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    return Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }
            return t.TradeLegId;
        }

        public static Int32 UpdateTradeSP(Trade t)
        {
            Int32 result = Int32.MinValue;

            try
            {
                SqlParameter tId = new SqlParameter("@TradeId", t.TradeID);
                SqlParameter TradeStatusId = new SqlParameter("@TradeStatusId", t.TradeStatusID);
                SqlParameter TemplateName = CreateStringParameter("@TemplateName", t.TemplateName);
                SqlParameter TemplateID = CreateInt32Parameter("@TemplateID", t.TemplateID);
                SqlParameter BtBTradeID = CreateInt32Parameter("@BtBTradeID", t.BtBTradeID);
                SqlParameter StructureID = CreateInt32Parameter("@StructureID", t.StructureID);
                SqlParameter BuySellID = new SqlParameter("@BuySellID", t.BuySellID);
                SqlParameter PricingModelId = new SqlParameter("@PricingModelId", t.PriceModellingID);
                SqlParameter TradeTypeID = new SqlParameter("@TradeTypeID", t.TradeTypeID);
                SqlParameter TradeSubTypeID = new SqlParameter("@TradeSubTypeID", t.TradeSubTypeID);
                SqlParameter PricingTypeID = new SqlParameter("@PricingTypeID", t.PriceTypeID);
                SqlParameter ContractCodeID = new SqlParameter("@ContractCodeID", t.ContractCodeID);
                SqlParameter TraderId = new SqlParameter("@TraderId", t.TraderID);
                SqlParameter UserId = new SqlParameter("@UpdateUser", t.UpdateUser);
                SqlParameter SalesPersonId = CreateInt32Parameter("@SalesPersonId", t.SalesPersonID);
                SqlParameter Reference = CreateStringParameter("@Reference", t.Reference);
                SqlParameter TraderComments = CreateStringParameter("@TraderComments", t.TraderComment);
                SqlParameter InternalBusinessUnitId = new SqlParameter("@InternalBusinessUnitId", t.InternalBusinessUnitID);
                SqlParameter InternalPortfolioId = CreateInt32Parameter("@InternalPortfolioId", t.InternalPortfolioID);
                SqlParameter InternalSubPortfolioId = CreateInt32Parameter("@InternalSubPortfolioId", t.InternalSubPortfolioID);
                SqlParameter ExternalBusinessUnitId = CreateInt32Parameter("@ExternalBusinessUnitId", t.ExternalBusinessUnitID);
                SqlParameter ExternalPortfolioId = CreateInt32Parameter("@ExternalPortfolioId", t.ExternalPortfolioID);
                SqlParameter ExternalSubPortfolioId = CreateInt32Parameter("@ExternalSubPortfolioId", t.ExternalSubPortfolioID);
                SqlParameter AgreementID = CreateInt32Parameter("@AgreementID", t.AgreementID);
                SqlParameter TradeTime = new SqlParameter("@TradeTime", t.TradeTime);
                SqlParameter TradeDate = new SqlParameter("@TradeDate", t.TradeDate);
                SqlParameter StartDate = CreateDateTimeParameter("@StartDate", t.StartDate);
                SqlParameter MaturityDate = CreateDateTimeParameter("@MaturityDate", t.MaturityDate);
                SqlParameter ClearingBrokerId = CreateInt32Parameter("@ClearingBrokerId", t.ClearingBrokerID);
                SqlParameter ClearingBrokerRateTypeId = CreateInt32Parameter("@ClearingBrokerRateTypeId", t.ClearingBrokerRateTypeID);
                SqlParameter ClearingBrokerAmount = CreateDecimalParameter("@ClearingBrokerAmount", t.ClearingBrokerAmount);
                SqlParameter ClearingBrokerCCYId = CreateInt32Parameter("@ClearingBrokerCCYId", t.ClearingBrokerCCYID);
                SqlParameter ExecutingBrokerId = CreateInt32Parameter("@ExecutingBrokerId", t.ExecutionBrokerID);
                SqlParameter ExecutingBrokerRateTypeId = CreateInt32Parameter("@ExecutingBrokerRateTypeId", t.ExecutionBrokerRateTypeID);
                SqlParameter ExecutingBrokerAmount = CreateDecimalParameter("@ExecutionBrokerAmount", t.ExecutionBrokerAmount);
                SqlParameter ExecutingBrokerCCYId = CreateInt32Parameter("@ExecutingBrokerCCYId", t.ExecutionBrokerCCYID);
                SqlParameter OTCBrokerId = CreateInt32Parameter("@OTCBrokerId", t.OTCBrokerID);
                SqlParameter OTCBrokerRateTypeId = CreateInt32Parameter("@OTCBrokerRateTypeId", t.OTCBrokerRateTypeID);
                SqlParameter OTCBrokerAmount = CreateDecimalParameter("@OTCBrokerAmount", t.OTCBrokerAmount);
                SqlParameter OTCBrokerAmountCCYId = CreateInt32Parameter("@OTCBrokerAmountCCYId", t.OTCBrokerCCYID);
                SqlParameter IntentToClear = new SqlParameter("@IntentToClear", t.IntentToClear);
                SqlParameter NonStandardTerms = new SqlParameter("@NonStandardTerms", t.NonStandardTerms);
                SqlParameter OffMarketPrice = new SqlParameter("@OffMarketPrice", t.OffMarketPrice);
                SqlParameter LargeSizeTrade = new SqlParameter("@LargeSizeTrade", t.LargeSizeTrade);
                SqlParameter Cleared = new SqlParameter("@Cleared", t.Cleared);
                SqlParameter CommonPricing = new SqlParameter("@CommonPricing", t.CommonPricing);
                SqlParameter ExecutionTypeId = new SqlParameter("@ExecutionTypeId", t.ExecutionTypeID);
                SqlParameter ExecutionVenueId = new SqlParameter("@ExecutionVenueId", DBNull.Value);
                SqlParameter ExecutionVenueTypeId = CreateInt32Parameter("@ExecutionVenueTypeId", t.ExecutionVenueTypeID);
                SqlParameter UPI = new SqlParameter("@UPI", t.UPI);
                SqlParameter USI = new SqlParameter("@USI", t.USI);
                SqlParameter ClearingAccount = new SqlParameter("@ClearingAccount", t.ClearingAccount);
                SqlParameter ClearingSubAccount = new SqlParameter("@ClearingSubAccount", t.ClearingSubAccount);
                SqlParameter AccountingObservability = new SqlParameter("@AccountingObservability", t.AccountingObservability);

                SqlCommand cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_UpdateTrade");

                cmd.Parameters.Add(tId);
                cmd.Parameters.Add(TradeStatusId);
                cmd.Parameters.Add(TemplateName);
                cmd.Parameters.Add(TemplateID);
                cmd.Parameters.Add(BtBTradeID);
                cmd.Parameters.Add(StructureID);
                cmd.Parameters.Add(BuySellID);
                cmd.Parameters.Add(PricingModelId);
                cmd.Parameters.Add(TradeTypeID);
                cmd.Parameters.Add(TradeSubTypeID);
                cmd.Parameters.Add(PricingTypeID);
                cmd.Parameters.Add(ContractCodeID);
                cmd.Parameters.Add(TraderId);
                cmd.Parameters.Add(UserId);
                cmd.Parameters.Add(SalesPersonId);
                cmd.Parameters.Add(Reference);
                cmd.Parameters.Add(TraderComments);
                cmd.Parameters.Add(InternalBusinessUnitId);
                cmd.Parameters.Add(InternalPortfolioId);
                cmd.Parameters.Add(InternalSubPortfolioId);
                cmd.Parameters.Add(ExternalBusinessUnitId);
                cmd.Parameters.Add(ExternalPortfolioId);
                cmd.Parameters.Add(ExternalSubPortfolioId);
                cmd.Parameters.Add(AgreementID);
                cmd.Parameters.Add(TradeTime);
                cmd.Parameters.Add(TradeDate);
                cmd.Parameters.Add(StartDate);
                cmd.Parameters.Add(MaturityDate);
                cmd.Parameters.Add(ClearingBrokerId);
                cmd.Parameters.Add(ClearingBrokerRateTypeId);
                cmd.Parameters.Add(ClearingBrokerAmount);
                cmd.Parameters.Add(ClearingBrokerCCYId);
                cmd.Parameters.Add(ExecutingBrokerId);
                cmd.Parameters.Add(ExecutingBrokerRateTypeId);
                cmd.Parameters.Add(ExecutingBrokerAmount);
                cmd.Parameters.Add(ExecutingBrokerCCYId);
                cmd.Parameters.Add(OTCBrokerId);
                cmd.Parameters.Add(OTCBrokerRateTypeId);
                cmd.Parameters.Add(OTCBrokerAmount);
                cmd.Parameters.Add(OTCBrokerAmountCCYId);
                cmd.Parameters.Add(IntentToClear);
                cmd.Parameters.Add(NonStandardTerms);
                cmd.Parameters.Add(OffMarketPrice);
                cmd.Parameters.Add(LargeSizeTrade);
                cmd.Parameters.Add(Cleared);
                cmd.Parameters.Add(CommonPricing);
                cmd.Parameters.Add(ExecutionTypeId);
                cmd.Parameters.Add(ExecutionVenueId);
                cmd.Parameters.Add(ExecutionVenueTypeId);
                cmd.Parameters.Add(UPI);
                cmd.Parameters.Add(USI);
                cmd.Parameters.Add(ClearingAccount);
                cmd.Parameters.Add(ClearingSubAccount);
                cmd.Parameters.Add(AccountingObservability);

                if (DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = t.TradeID;
                }
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static DataTable GetStripComponents(Int32 structureId)
        {
            DataTable dt = new DataTable("Data");

            try
            {
                SqlParameter pStructureId = new SqlParameter("@StructureID", structureId);
                SqlCommand cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetStripComponents");

                cmd.Parameters.Add(pStructureId);

                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }

            return dt;
        }

        public static DataTable GetStructureDetails(Int32 structureId)
        {
            DataTable dt = new DataTable("Data");

            try
            {
                SqlParameter pStructureId = new SqlParameter("@TradeStructureDefinitionID", structureId);
                SqlCommand cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetStructureDefinition");

                cmd.Parameters.Add(pStructureId);

                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static Int32 VoidUpdateTradeSP(Int32 tradeId, Int32 newStatus, Int32 userId)
        {
            Int32 result = Int32.MinValue;

            try
            {
                SqlParameter tId = new SqlParameter("@TradeId", tradeId);
                SqlParameter TradeStatusId = new SqlParameter("@TradeStatusId", newStatus);
                SqlParameter UpdateUser = new SqlParameter("@UpdateUser", userId);

                SqlCommand cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_VoidUpdateTrade");

                cmd.Parameters.Add(tId);
                cmd.Parameters.Add(TradeStatusId);
                cmd.Parameters.Add(UpdateUser);

                if (DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = tradeId;
                }
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 SaveFuturesEvaluationData(DateTime valuationDate, Int32 curveMarkType, string granularity, Int32 snapshotID, Int32 underlyingID)
        {
            Int32 valuationId = GetNextValuationFuturesDataSequenceID();

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_InsertFuturesEvaluationData");

                cmd.Parameters.AddWithValue("@ValuationDate", valuationDate);
                cmd.Parameters.AddWithValue("@ValuationID", valuationId);
                cmd.Parameters.AddWithValue("@CurveMarkType", curveMarkType);
                cmd.Parameters.AddWithValue("@Granularity", granularity);
                cmd.Parameters.AddWithValue("@SnapshotID", snapshotID);
                cmd.Parameters.AddWithValue("@UnderlyingID", underlyingID);

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    return Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
                valuationId = Int32.MinValue;
            }
            return valuationId;
        }

        public static Int32 SaveFutureTradeSP(Trade t)
        {
            Int32 tradeId = GetNextTradeID();

            try
            {
                SqlParameter tId = new SqlParameter("@TradeId", tradeId);
                SqlParameter TradeStatusId = new SqlParameter("@TradeStatusId", t.TradeStatusID);
                SqlParameter TemplateName = CreateStringParameter("@TemplateName", t.TemplateName);
                SqlParameter TemplateID = CreateInt32Parameter("@TemplateID", t.TemplateID);
                SqlParameter BtBTradeID = CreateInt32Parameter("@BtBTradeID", t.BtBTradeID);
                SqlParameter StructureID = CreateInt32Parameter("@StructureID", t.StructureID);
                SqlParameter BuySellID = new SqlParameter("@BuySellID", t.BuySellID);
                SqlParameter PriceModellingId = new SqlParameter("@PriceModellingId", t.PriceModellingID);
                SqlParameter TradeTypeID = new SqlParameter("@TradeTypeID", t.TradeTypeID);
                SqlParameter TradeSubTypeID = new SqlParameter("@TradeSubTypeID", t.TradeSubTypeID);
                SqlParameter PricingTypeID = new SqlParameter("@PricingTypeID", t.PriceTypeID);
                SqlParameter ContractCodeID = new SqlParameter("@ContractCodeID", t.ContractCodeID);
                SqlParameter TraderId = new SqlParameter("@TraderId", t.TraderID);
                SqlParameter UserId = new SqlParameter("@UpdateUser", t.UpdateUser);
                SqlParameter SalesPersonId = CreateInt32Parameter("@SalesPersonId", t.SalesPersonID);
                SqlParameter Reference = new SqlParameter("@Reference", t.Reference);
                SqlParameter TraderComments = new SqlParameter("@TraderComments", t.TraderComment);
                SqlParameter InternalBusinessUnitId = new SqlParameter("@InternalBusinessUnitId", t.InternalBusinessUnitID);
                SqlParameter InternalPortfolioId = CreateInt32Parameter("@InternalPortfolioId", t.InternalPortfolioID);
                SqlParameter InternalSubPortfolioId = CreateInt32Parameter("@InternalSubPortfolioId", t.InternalSubPortfolioID);
                SqlParameter ExternalBusinessUnitId = CreateInt32Parameter("@ExternalBusinessUnitId", t.ExternalBusinessUnitID);
                SqlParameter ExternalPortfolioId = CreateInt32Parameter("@ExternalPortfolioId", t.ExternalPortfolioID);
                SqlParameter ExternalSubPortfolioId = CreateInt32Parameter("@ExternalSubPortfolioId", t.ExternalSubPortfolioID);
                SqlParameter AgreementID = CreateInt32Parameter("@AgreementID", t.AgreementID);
                SqlParameter TradeTime = new SqlParameter("@TradeTime", t.TradeTime);
                SqlParameter TradeDate = new SqlParameter("@TradeDate", t.TradeDate);
                SqlParameter StartDate = CreateDateTimeParameter("@StartDate", t.StartDate);
                SqlParameter MaturityDate = CreateDateTimeParameter("@MaturityDate", t.MaturityDate);
                SqlParameter ClearingBrokerId = CreateInt32Parameter("@ClearingBrokerId", t.ClearingBrokerID);
                SqlParameter ClearingBrokerRateTypeId = CreateInt32Parameter("@ClearingBrokerRateTypeId", t.ClearingBrokerRateTypeID);
                SqlParameter ClearingBrokerAmount = CreateDecimalParameter("@ClearingBrokerAmount", t.ClearingBrokerAmount);
                SqlParameter ClearingBrokerCCYId = CreateInt32Parameter("@ClearingBrokerCCYId", t.ClearingBrokerCCYID);
                SqlParameter ExecutingBrokerId = CreateInt32Parameter("@ExecutingBrokerId", t.ExecutionBrokerID);
                SqlParameter ExecutingBrokerRateTypeId = CreateInt32Parameter("@ExecutionBrokerRateTypeId", t.ExecutionBrokerRateTypeID);
                SqlParameter ExecutingBrokerAmount = CreateDecimalParameter("@ExecutionBrokerAmount", t.ExecutionBrokerAmount);
                SqlParameter ExecutingBrokerCCYId = CreateInt32Parameter("@ExecutionBrokerCCYId", t.ExecutionBrokerCCYID);
                SqlParameter OTCBrokerId = CreateInt32Parameter("@OTCBrokerId", t.OTCBrokerID);
                SqlParameter OTCBrokerRateTypeId = CreateInt32Parameter("@OTCBrokerRateTypeId", t.OTCBrokerRateTypeID);
                SqlParameter OTCBrokerAmount = CreateDecimalParameter("@OTCBrokerAmount", t.OTCBrokerAmount);
                SqlParameter OTCBrokerAmountCCYId = CreateInt32Parameter("@OTCBrokerAmountCCYId", t.OTCBrokerCCYID);
                SqlParameter InsertToClear = new SqlParameter("@InsertToClear", t.IntentToClear);
                SqlParameter NonStandardTerms = new SqlParameter("@NonStandardTerms", t.NonStandardTerms);
                SqlParameter OffMarketPrice = new SqlParameter("@OffMarketPrice", t.OffMarketPrice);
                SqlParameter LargeSizeTrade = new SqlParameter("@LargeSizeTrade", t.LargeSizeTrade);
                SqlParameter Cleared = new SqlParameter("@Cleared", t.Cleared);
                SqlParameter CommonPricing = new SqlParameter("@CommonPricing", t.CommonPricing);
                SqlParameter ExecutionTypeId = new SqlParameter("@ExecutionTypeId", t.ExecutionTypeID);
                SqlParameter ExecutionVenueId = new SqlParameter("@ExecutionVenueId", DBNull.Value);
                SqlParameter ExecutionVenueTypeId = CreateInt32Parameter("@ExecutionVenueTypeId", t.ExecutionVenueTypeID);
                SqlParameter UPI = new SqlParameter("@UPI", t.UPI);
                SqlParameter USI = new SqlParameter("@USI", t.USI);
                SqlParameter ClearingAccount = new SqlParameter("@ClearingAccount", t.ClearingAccount);
                SqlParameter ClearingSubAccount = new SqlParameter("@ClearingSubAccount", t.ClearingSubAccount);
                SqlParameter AccountingObservability = new SqlParameter("@AccountingObservability", t.AccountingObservability);

                SqlCommand cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_InsertTrade");

                cmd.Parameters.Add(tId);
                cmd.Parameters.Add(TradeStatusId);
                cmd.Parameters.Add(TemplateName);
                cmd.Parameters.Add(TemplateID);
                cmd.Parameters.Add(BtBTradeID);
                cmd.Parameters.Add(StructureID);
                cmd.Parameters.Add(BuySellID);
                cmd.Parameters.Add(PriceModellingId);
                cmd.Parameters.Add(TradeTypeID);
                cmd.Parameters.Add(TradeSubTypeID);
                cmd.Parameters.Add(PricingTypeID);
                cmd.Parameters.Add(ContractCodeID);
                cmd.Parameters.Add(TraderId);
                cmd.Parameters.Add(UserId);
                cmd.Parameters.Add(SalesPersonId);
                cmd.Parameters.Add(Reference);
                cmd.Parameters.Add(TraderComments);
                cmd.Parameters.Add(InternalBusinessUnitId);
                cmd.Parameters.Add(InternalPortfolioId);
                cmd.Parameters.Add(InternalSubPortfolioId);
                cmd.Parameters.Add(ExternalBusinessUnitId);
                cmd.Parameters.Add(ExternalPortfolioId);
                cmd.Parameters.Add(ExternalSubPortfolioId);
                cmd.Parameters.Add(AgreementID);
                cmd.Parameters.Add(TradeTime);
                cmd.Parameters.Add(TradeDate);
                cmd.Parameters.Add(StartDate);
                cmd.Parameters.Add(MaturityDate);
                cmd.Parameters.Add(ClearingBrokerId);
                cmd.Parameters.Add(ClearingBrokerRateTypeId);
                cmd.Parameters.Add(ClearingBrokerAmount);
                cmd.Parameters.Add(ClearingBrokerCCYId);
                cmd.Parameters.Add(ExecutingBrokerId);
                cmd.Parameters.Add(ExecutingBrokerRateTypeId);
                cmd.Parameters.Add(ExecutingBrokerAmount);
                cmd.Parameters.Add(ExecutingBrokerCCYId);
                cmd.Parameters.Add(OTCBrokerId);
                cmd.Parameters.Add(OTCBrokerRateTypeId);
                cmd.Parameters.Add(OTCBrokerAmount);
                cmd.Parameters.Add(OTCBrokerAmountCCYId);
                cmd.Parameters.Add(InsertToClear);
                cmd.Parameters.Add(NonStandardTerms);
                cmd.Parameters.Add(OffMarketPrice);
                cmd.Parameters.Add(LargeSizeTrade);
                cmd.Parameters.Add(Cleared);
                cmd.Parameters.Add(CommonPricing);
                cmd.Parameters.Add(ExecutionTypeId);
                cmd.Parameters.Add(ExecutionVenueId);
                cmd.Parameters.Add(ExecutionVenueTypeId);
                cmd.Parameters.Add(UPI);
                cmd.Parameters.Add(USI);
                cmd.Parameters.Add(ClearingAccount);
                cmd.Parameters.Add(ClearingSubAccount);
                cmd.Parameters.Add(AccountingObservability);

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    return Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
                tradeId = Int32.MinValue;
            }
            return tradeId;
        }

        private static SqlParameter CreateStringParameter(string parameterName, string parameterValue)
        {
            SqlParameter sqlParameter;
            if (parameterValue == string.Empty)
            {
                sqlParameter = new SqlParameter(parameterName, DBNull.Value);
            }
            else
            {
                sqlParameter = new SqlParameter(parameterName, parameterValue);
            }
            return sqlParameter;
        }

        private static SqlParameter CreateStringParameterCBText(string parameterName, string parameterValue)
        {
            SqlParameter sqlParameter;
            if (parameterValue == string.Empty || parameterValue == @"System.Data.DataRowView")
            {
                sqlParameter = new SqlParameter(parameterName, string.Empty);
            }
            else
            {
                sqlParameter = new SqlParameter(parameterName, parameterValue);
            }
            return sqlParameter;
        }

        private static SqlParameter CreateInt32Parameter(string parameterName, Int32? parameterValue)
        {
            SqlParameter sqlParameter;
            if (parameterValue == Int32.MinValue || parameterValue == null)
            {
                sqlParameter = new SqlParameter(parameterName, DBNull.Value);
            }
            else
            {
                sqlParameter = new SqlParameter(parameterName, parameterValue);
            }
            return sqlParameter;
        }

        private static SqlParameter CreateBoolParameter(string parameterName, bool? parameterValue)
        {
            SqlParameter sqlParameter;
            if (parameterValue == null)
            {
                sqlParameter = new SqlParameter(parameterName, DBNull.Value);
            }
            else
            {
                sqlParameter = new SqlParameter(parameterName, parameterValue);
            }
            return sqlParameter;
        }

        private static SqlParameter CreateDateTimeParameter(string parameterName, DateTime? parameterValue)
        {
            SqlParameter sqlParameter;
            if (parameterValue == null)
            {
                sqlParameter = new SqlParameter(parameterName, DBNull.Value);
            }
            else
            {
                sqlParameter = new SqlParameter(parameterName, parameterValue);
            }
            return sqlParameter;
        }

        private static SqlParameter CreateDecimalParameter(string parameterName, decimal? parameterValue)
        {
            SqlParameter sqlParameter;
            if (parameterValue == decimal.MinValue || parameterValue == null)
            {
                sqlParameter = new SqlParameter(parameterName, DBNull.Value);
            }
            else
            {
                sqlParameter = new SqlParameter(parameterName, parameterValue);
            }
            return sqlParameter;
        }

        private static SqlParameter CreateDoubleParameter(string parameterName, double? parameterValue)
        {
            SqlParameter sqlParameter;
            if (parameterValue == double.MinValue || parameterValue == null)
            {
                sqlParameter = new SqlParameter(parameterName, DBNull.Value);
            }
            else
            {
                sqlParameter = new SqlParameter(parameterName, parameterValue);
            }
            return sqlParameter;
        }

        public static Int32 SaveBusinessUnit(BusinessUnit bu)
        {
            Int32 result = bu.PartyId;
            SqlCommand cmd = new SqlCommand();
            try
            {

                cmd = DataManager.GetDM().GetCommand("sp_StaticData_InsertParty");

                SqlParameter shortName = new SqlParameter("@ShortName", bu.ShortName);
                SqlParameter longName = new SqlParameter("@LongName", bu.LongName);
                SqlParameter partyClassId = new SqlParameter("@PartyClassId", bu.PartyClass);

                SqlParameter lei;
                if (string.IsNullOrEmpty(bu.LEI))
                {
                    lei = new SqlParameter("@LEI", DBNull.Value);
                }
                else
                {
                    lei = new SqlParameter("@LEI", bu.LEI);
                }

                SqlParameter authoriser;
                if (bu.AuthoriserId.HasValue)
                {
                    authoriser = new SqlParameter("@AuthoriserId", bu.AuthoriserId.Value);
                }
                else
                {
                    authoriser = new SqlParameter("@AuthoriserId", DBNull.Value);
                }

                SqlParameter status = new SqlParameter("@PartyStatusId", bu.PartyStatusId);
                SqlParameter spiderId = new SqlParameter("@SpiderId", bu.SpiderId);
                SqlParameter intExt = new SqlParameter("@InternalExternalId", bu.InternalExternal);
                SqlParameter partyId = new SqlParameter("@PartyID", bu.PartyId);
                SqlParameter updateUser = new SqlParameter("@UpdateUser", bu.UpdateUser);

                cmd.Parameters.Add(CreateStringParameter("@SchedulingPartyName", bu.SchedulingPartyName));
                cmd.Parameters.Add(CreateStringParameter("@MICCode", bu.MICCode));
                cmd.Parameters.Add(CreateStringParameter("@TaxID", bu.TaxID));
                cmd.Parameters.Add(CreateInt32Parameter("@ConfirmingParty", bu.ConfirmingPartyID));
                cmd.Parameters.Add(CreateInt32Parameter("@DoddFrankType", bu.DoddFrankTypeID));
                cmd.Parameters.Add(CreateInt32Parameter("@DoddFrankReportingPartyId", bu.DoddFrankReportingParty));
                cmd.Parameters.Add(CreateInt32Parameter("@TradingCountry", bu.PrincipalTradingCountry));
                cmd.Parameters.Add(CreateInt32Parameter("@RegisteredCountry", bu.RegisteredCountry));
                cmd.Parameters.Add(CreateInt32Parameter("@TaxDomicile", bu.TaxDomicileID));
                cmd.Parameters.Add(CreateInt32Parameter("@TaxStatus", bu.TaxStatusID));
                cmd.Parameters.Add(CreateBoolParameter("@IConRequired", bu.IConReq));
                cmd.Parameters.Add(CreateBoolParameter("@OConRequired", bu.OConReq));
                cmd.Parameters.Add(CreateBoolParameter("@CommercialParticipant", bu.CommercialParticipant));
                cmd.Parameters.Add(CreateBoolParameter("@InvoiceRequired", bu.InvoiceReq));
                cmd.Parameters.Add(CreateInt32Parameter("@ConfirmMethodID", bu.ConfirmMethodID));
                cmd.Parameters.Add(CreateStringParameter("@BeaconID", bu.BeaconID));

                cmd.Parameters.Add(shortName);
                cmd.Parameters.Add(longName);
                cmd.Parameters.Add(updateUser);
                cmd.Parameters.Add(partyId);
                cmd.Parameters.Add(partyClassId);
                cmd.Parameters.Add(lei);
                cmd.Parameters.Add(status);
                cmd.Parameters.Add(authoriser);
                cmd.Parameters.Add(spiderId);
                cmd.Parameters.Add(intExt);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
                result = Int32.MinValue;
            }
            finally
            {
                cmd.Dispose();
            }
            return result;
        }

        public static Int32 MaxContactId()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"select MAX(PartyContactID) from PartyContact";

                cmd = DataManager.GetDM().GetCommandT(sql2);

                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[0][0])))
            {
                return 0;
            }

            return Convert.ToInt32(dt.Rows[0][0]);
        }

        public static Int32 MaxPartyBrokerRateId()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"select MAX(PartyBrokerRateID) from PartyBrokerRate";
                cmd = DataManager.GetDM().GetCommandT(sql2);
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[0][0])))
            {
                return 0;
            }

            return Convert.ToInt32(dt.Rows[0][0]);
        }

        public static Int32 MaxAddressId()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"select MAX(PartyAddressID) from PartyAddress";
                cmd = DataManager.GetDM().GetCommandT(sql2);
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[0][0])))
            {
                return 0;
            }

            return Convert.ToInt32(dt.Rows[0][0]);
        }

        public static Int32 MaxPartyId()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"select MAX(PartyID) from Party";
                cmd = DataManager.GetDM().GetCommandT(sql2);
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[0][0])))
            {
                return 0;
            }

            return Convert.ToInt32(dt.Rows[0][0]);
        }

        public static DataTable GetParties(LEBU pclass, IntExt intExt)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                Int32 pclassId = 1;
                Int32 pIntExt = 1;

                if (pclass == LEBU.BusinessUnit)
                {
                    pclassId = 2;
                }

                if (intExt == IntExt.External)
                {
                    pIntExt = 2;
                }

                SqlParameter parId = new SqlParameter("@PartyClassId", pclassId);
                SqlParameter addId = new SqlParameter("@InternalExternalId", pIntExt);

                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetParties");

                cmd.Parameters.Add(parId);
                cmd.Parameters.Add(addId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetPartyContactRecord(Int32 partyId, Int32 contactId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                SqlParameter parId = new SqlParameter("@PartyID", partyId);
                SqlParameter addId = new SqlParameter("@PartyContactID", contactId);

                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPartyContactRecord");
                cmd.Parameters.Add(parId);
                cmd.Parameters.Add(addId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetPartyAddressRecord(Int32 partyId, Int32 addressId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                SqlParameter parId = new SqlParameter("@partyId", partyId);
                SqlParameter addId = new SqlParameter("@PartyAddressID", addressId);

                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPartyAddressRecord");

                cmd.Parameters.Add(parId);
                cmd.Parameters.Add(addId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetAgreements(Int32 partyId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                SqlParameter par = new SqlParameter("@PartyID", partyId);
                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetAgreements");
                cmd.Parameters.Add(par);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        //Not used - no need to make SP
        public static DataTable GetAgreementConditions(Int32 conditionId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql = @"
                                SELECT *
                                  FROM PartyAgreementCondition PAC
                                  where PartyAgreementConditionID = @PartyAgreementConditionID
                            ";

                cmd = DataManager.GetDM().GetCommandT(sql);
                cmd.Parameters.AddWithValue("@PartyAgreementConditionID", conditionId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        //Not used - no need to make SP
        public static DataTable GetAgreementConditions()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql = @"
                                SELECT 
                                   PartyAgreementConditionID
                                  ,PartyAgreementCondition
                                FROM PartyAgreementCondition PAC
                                where PAC.Deleted = 0
                            ";

                cmd = DataManager.GetDM().GetCommandT(sql);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetPartyContacts(Int32 partyId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                SqlParameter par = new SqlParameter("@partyId", partyId);
                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPartyContacts");
                cmd.Parameters.Add(par);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetPartyAddresses(Int32 partyId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPartyAddresses");
                SqlParameter par = new SqlParameter("@partyId", partyId);
                cmd.Parameters.Add(par);
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static Int32 UpdateParentId(Int32 parentId, Int32 childId)
        {
            SqlCommand cmd = new SqlCommand();
            Int32 result = parentId;

            try
            {
                Int32 maxRelationID = MaxRelationId() + 1;

                string sql3 = @"UPDATE PartyRelation 
                                SET ParentID = @ParentID
                                WHERE ChildID = @ChildID";

                SqlParameter parent = new SqlParameter("@ParentID", parentId);
                SqlParameter child = new SqlParameter("@ChildID", childId);

                cmd = DataManager.GetDM().GetCommandT(sql3);
                cmd.Parameters.Add(parent);
                cmd.Parameters.Add(child);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
                result = Int32.MinValue;
            }
            finally
            {
                cmd.Dispose();
            }
            return result;
        }

        public static Int32 SaveParentId(Int32 parentId, Int32 childId)
        {
            SqlCommand cmd = new SqlCommand();
            Int32 result = parentId;

            try
            {
                Int32 maxRelationID = MaxRelationId() + 1;

                string sql3 = @"INSERT INTO PartyRelation (
                                ParentID,
                                ChildID,
                                PartyRelationID
                                ) VALUES  (
                                @ParentID,
                                @ChildID,
                                @PartyRelationID
                                )";

                SqlParameter parent = new SqlParameter("@ParentID", parentId);
                SqlParameter child = new SqlParameter("@ChildID", childId);
                SqlParameter relation = new SqlParameter("@PartyRelationID", maxRelationID);

                cmd = DataManager.GetDM().GetCommandT(sql3);
                cmd.Parameters.Add(parent);
                cmd.Parameters.Add(child);
                cmd.Parameters.Add(relation);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return result;
        }

        public static Int32 MaxRelationId()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                string sql2 = @"select MAX(PartyRelationID) from PartyRelation";

                cmd = DataManager.GetDM().GetCommandT(sql2);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[0][0])))
            {
                return 0;
            }

            return Convert.ToInt32(dt.Rows[0][0]);
        }

        public static string GetCompanyName(Int32 businessUnitPartyId)
        {
            string result = string.Empty;
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                SqlParameter par = new SqlParameter("@PartyId", businessUnitPartyId);
                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetCompanyName");

                cmd.Parameters.Add(par);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);

                if (!ETRMDataAccess.IsEmpty(dt))
                {
                    result = Convert.ToString(dt.Rows[0][0]);
                }

            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return result;
        }

        public static string GetOptionCategoryForTrade(Int32 tradeID)
        {
            string result = string.Empty;
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_BeaStra_GetOptionCategoryForTrade");
                cmd.Parameters.AddWithValue("@TradeID", tradeID);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);

                if (!ETRMDataAccess.IsEmpty(dt))
                {
                    result = Convert.ToString(dt.Rows[0][0]);
                }

            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return result;
        }

        //sp_StaticData_GetConfirmationAddresses
        public static List<string> GetConfirmationAddresses(Int32 tradeID)
        {
            List<string> contacts = new List<string>();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                SqlParameter par = new SqlParameter("@TradeId", tradeID);
                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetConfirmationAddresses");
                cmd.Parameters.Add(par);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);

                if (!ETRMDataAccess.IsEmpty(dt))
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        contacts.Add(ETRMDataAccess.GetStringDTRElement(row, ConfLightInfo.Email));
                    }
                }
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return contacts;
        }

        public static Int32 GetParentCompany(Int32 businessUnitPartyId)
        {
            Int32 result = Int32.MinValue;
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                SqlParameter par = new SqlParameter("@PartyId", businessUnitPartyId);
                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetParentCompany");
                cmd.Parameters.Add(par);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);

                if (!ETRMDataAccess.IsEmpty(dt))
                {
                    result = Convert.ToInt32(dt.Rows[0][0]);
                }
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return result;
        }

        public static string GetUnit(Int32 unitId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                SELECT Unit
                                FROM Unit
                                where UnitId = @UnitId
                             ";

                SqlParameter par = new SqlParameter("@UnitId", unitId);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(par);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            if (IsEmpty(dt))
            {
                return string.Empty;
            }
            else
            {
                return Convert.ToString(dt.Rows[0][0]);
            }
        }

        public static string GetPaymentDateOffset(Int32 brokerId, Int32 exchangeId, Int32 underlyingId, Int32 tradeTypeId, Int32 brokerRateTypeId, Int32 rateCCYId, Int32 settlementTypeId)
        {
            string result = string.Empty;
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                SELECT 
                                       PaymentDateOffset
                                  FROM PartyBrokerRate

                                  where BrokerID = @BrokerID 
                                    and ExchangeID = @ExchangeID
                                    and UnderlyingID = @UnderlyingID 
                                    and TradeTypeId = @TradeTypeId 
                                    and BrokerRateTypeID = @BrokerRateTypeID 
                                    and RateCurrencyID = @RateCurrencyID 
                                    and SettlementTypeID = @SettlementTypeID
                                    and Deleted = 0
                                  order by DefaultRate desc

                             ";

                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.AddWithValue("@BrokerID", brokerId);
                cmd.Parameters.AddWithValue("@ExchangeID", exchangeId);
                cmd.Parameters.AddWithValue("@UnderlyingID", underlyingId);
                cmd.Parameters.AddWithValue("@TradeTypeId", tradeTypeId);
                cmd.Parameters.AddWithValue("@BrokerRateTypeID", brokerRateTypeId);
                cmd.Parameters.AddWithValue("@RateCurrencyID", rateCCYId);
                cmd.Parameters.AddWithValue("@SettlementTypeID", settlementTypeId);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);

                if (!ETRMDataAccess.IsEmpty(dt))
                {
                    result = Convert.ToString(dt.Rows[0][0]);
                }

            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return result;
        }

        public static Int32 GetPaymentFrequency(Int32 brokerId, Int32 exchangeId, Int32 underlyingId, Int32 tradeTypeId, Int32 brokerRateTypeId, Int32 rateCCYId, Int32 settlementTypeId)
        {
            Int32 result = Int32.MinValue;
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                SELECT 
                                       PaymentFrequencyID
                                  FROM PartyBrokerRate

                                  where BrokerID = @BrokerID 
                                    and ExchangeID = @ExchangeID
                                    and UnderlyingID = @UnderlyingID 
                                    and TradeTypeId = @TradeTypeId 
                                    and BrokerRateTypeID = @BrokerRateTypeID 
                                    and RateCurrencyID = @RateCurrencyID 
                                    and SettlementTypeID = @SettlementTypeID
                                    and Deleted = 0
                                  order by DefaultRate desc

                             ";

                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.AddWithValue("@BrokerID", brokerId);
                cmd.Parameters.AddWithValue("@ExchangeID", exchangeId);
                cmd.Parameters.AddWithValue("@UnderlyingID", underlyingId);
                cmd.Parameters.AddWithValue("@TradeTypeId", tradeTypeId);
                cmd.Parameters.AddWithValue("@BrokerRateTypeID", brokerRateTypeId);
                cmd.Parameters.AddWithValue("@RateCurrencyID", rateCCYId);
                cmd.Parameters.AddWithValue("@SettlementTypeID", settlementTypeId);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);

                if (!ETRMDataAccess.IsEmpty(dt))
                {
                    result = Convert.ToInt32(dt.Rows[0][0]);
                }

            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return result;
        }

        public static string GetPriceBand(Int32 powerPriceBandid)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                SELECT PowerPriceBand
                                FROM PowerPriceBand
                                where PowerPriceBandId = @PowerPriceBandid
                             ";

                SqlParameter par = new SqlParameter("@PowerPriceBandid", powerPriceBandid);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(par);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            if (IsEmpty(dt))
            {
                return string.Empty;
            }
            else
            {
                return Convert.ToString(dt.Rows[0][0]);
            }
        }

        public static string GetPricingReference(Int32 pricingReferenceId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                SELECT PricingReference
                                FROM PricingReference
                                where PricingReferenceId = @PricingReferenceId
                             ";

                SqlParameter par = new SqlParameter("@PricingReferenceId", pricingReferenceId);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(par);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            if (IsEmpty(dt))
            {
                return string.Empty;
            }
            else
            {
                return Convert.ToString(dt.Rows[0][0]);
            }
        }

        public static string GetCurveName(Int32 curveDefinitionId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                SELECT CurveName
                                FROM CurveDefinition
                                where CurveDefinitionId = @CurveDefinitionId
                             ";

                SqlParameter par = new SqlParameter("@CurveDefinitionId", curveDefinitionId);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(par);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            if (IsEmpty(dt))
            {
                return string.Empty;
            }
            else
            {
                return Convert.ToString(dt.Rows[0][0]);
            }
        }

        public static string GetSettlementType(Int32 settlementTypeId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                SELECT SettlementType
                                FROM SettlementType
                                where SettlementTypeId = @SettlementTypeId
                             ";

                SqlParameter par = new SqlParameter("@SettlementTypeId", settlementTypeId);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(par);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            if (IsEmpty(dt))
            {
                return string.Empty;
            }
            else
            {
                return Convert.ToString(dt.Rows[0][0]);
            }
        }

        public static string GetTradeType(Int32 tradeTypeId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                SELECT TradeType
                                FROM TradeType
                                where TradeTypeId = @TradeTypeId
                             ";

                SqlParameter par = new SqlParameter("@TradeTypeId", tradeTypeId);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(par);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            if (IsEmpty(dt))
            {
                return string.Empty;
            }
            else
            {
                return Convert.ToString(dt.Rows[0][0]);
            }
        }

        public static DataTable GetCurveDefinition(Int32 curveDefinitionId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                SELECT *
                                FROM CurveDefinition
                                where CurveDefinitionId = @CurveDefinitionId
                             ";

                SqlParameter par = new SqlParameter("@CurveDefinitionId", curveDefinitionId);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(par);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetAssetHierarchy(Int32 underlyingId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                SELECT *
                                FROM AssetHierarchy
                                where DeliveryTypeId = @DeliveryTypeId
                             ";

                SqlParameter par = new SqlParameter("@DeliveryTypeId", underlyingId);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(par);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetParentCompanies(Int32 intExt)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetParentCompanies");
                cmd.Parameters.AddWithValue("@IntExt", intExt);
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DataTable GetEditableFees(Int32 tradeId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetEditableFees");

                SqlParameter par = new SqlParameter("@TradeID", tradeId);
                cmd.Parameters.Add(par);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static Int32 UpdateTradeEventAmount(Int32 tradeEventId, double amount, Int32 userId)
        {
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_UpdateTradeEventAmount");
                cmd.Parameters.AddWithValue("@TradeEventId", tradeEventId);
                cmd.Parameters.AddWithValue("@UpdateUser", userId);

                if (amount == double.MinValue)
                {
                    cmd.Parameters.AddWithValue("@Amount", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Amount", amount);
                }

                Int32 rowsAffected = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }
            finally
            {
                cmd.Dispose();
            }
            return tradeEventId;
        }

        public static DataTable GetValuationControlRecords()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetValuationControlRecords");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetSnapshotReport(DateTime reportDate, Int32 validationRecordID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetSnapshotReport3");
                cmd.Parameters.AddWithValue("@ReportDate", reportDate);
                cmd.Parameters.AddWithValue("@ValuationControlSequence", validationRecordID);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }


        public static DataTable GetFuturesMtMReport(DateTime baselineDate, DateTime comparingDate, Int32 valuationControlSequence)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetMtMReport3");
                cmd.CommandTimeout = 300;
                cmd.Parameters.AddWithValue("@BaselineDate", baselineDate);
                cmd.Parameters.AddWithValue("@ComparingDate", comparingDate);
                cmd.Parameters.AddWithValue("@ValuationControlSequence", valuationControlSequence);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetFuturesPnLExplainedReport(Int32 valuationRecordID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetPnLExplained");
                cmd.CommandTimeout = 300;
                cmd.Parameters.AddWithValue("@ValuationRecordID", valuationRecordID);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetReports(Int32 reportID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetReports");
                SqlParameter par = new SqlParameter("@ReportTypeID", reportID);
                cmd.Parameters.Add(par);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetUserFunctions(string userName)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_Security_GetFunctions");
                cmd.Parameters.AddWithValue("@Username", userName);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetUserFunctions(string userName, ref string error)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            error = Constants.NOERROR;

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_Security_GetFunctions");
                cmd.Parameters.AddWithValue("@Username", userName);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetTradeSP(Int32 tradeId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetTradeB");
                SqlParameter par = new SqlParameter("@TradeID", tradeId);
                cmd.Parameters.Add(par);
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DataTable GetPersonnelRecord(string username)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetPersonnelRecord");
                cmd.Parameters.AddWithValue("@Username", username);
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static void SetPersonnelID(string personnelID)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("dbo.sp_Security_SetPersonnelID");
                cmd.Parameters.AddWithValue("@UserID", personnelID);
                DataManager.GetDM().ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
        }

        public static DataTable GetParty(Int32 partyId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                SqlParameter par = new SqlParameter("@partyId", partyId);
                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPartyHeader");
                cmd.Parameters.Add(par);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetYN()
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("YNValue", typeof(Int32));
                dt.Columns.Add("YNShow", typeof(string));

                DataRow no = dt.NewRow();
                no["YNValue"] = 1;
                no["YNShow"] = "No";

                dt.Rows.Add(no);

                DataRow yes = dt.NewRow();
                yes["YNValue"] = 2;
                yes["YNShow"] = "Yes";

                dt.Rows.Add(yes);

            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
            }
            return dt;
        }

        public static DataTable GetCountries()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"select CountryID, ShortName from Country;";
                cmd = DataManager.GetDM().GetCommandT(sql2);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static Int32 GetCountryFromState(Int32 stateId)
        {
            Int32 result = Int32.MinValue;
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"select CountryId from States where StatesID = @stateId;";
                SqlParameter parId = new SqlParameter("@stateId", stateId);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(parId);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);

                result = Convert.ToInt32(dt.Rows[0][0]);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return result;
        }

        public static DataTable GetStates()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"select StatesID, Name from States ORDER BY CountryId desc;";
                cmd = DataManager.GetDM().GetCommandT(sql2);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetPartyFunctions(Int32 partyId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"select PF.PartyId, PF.PartyFunctionTypeID, PT.PartyFunctionType as [Function]
                                from PartyFunction PF
                                join PartyFunctionType PT on PT.PartyFunctionTypeID = PF.PartyFunctionTypeID
                                where PartyId = @PartyId
                                ;";

                SqlParameter pparty = new SqlParameter("@PartyId", partyId);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(pparty);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static Int32 GetParentCompanyId(Int32 childId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"select ParentID 
                                from PartyRelation
                                where ChildID = @ChildID
                                ;";

                SqlParameter pparty = new SqlParameter("@ChildID", childId);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(pparty);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static DataTable GetPortfolio(Int32 portfolioId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetPortfolio");

                SqlParameter portfolio = new SqlParameter("@PortfolioID", portfolioId);
                cmd.Parameters.Add(portfolio);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);

                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DoubleDecker GetPortfolioListing()
        {
            DoubleDecker dd = new DoubleDecker();

            try
            {
                DataTable dt = new DataTable();

                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetPortfolioListing");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);

                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dd;
        }

        public static DoubleDecker GetCurveListing()
        {
            DoubleDecker dd = new DoubleDecker();

            try
            {
                DataTable dt = new DataTable();

                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetCurveListing");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);

                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dd;
        }

        public static DoubleDecker GetExchangeClearingRateListing()
        {
            DoubleDecker dd = new DoubleDecker();

            try
            {
                DataTable dt = new DataTable();

                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetExchangeClearingRateListing");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);

                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dd;
        }

        public static DoubleDecker GetPricingReferenceListing()
        {
            DoubleDecker dd = new DoubleDecker();

            try
            {
                DataTable dt = new DataTable();

                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPricingReferenceListing");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);

                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dd;
        }
        public static DoubleDecker GetOTCPricingReferenceListing()
        {
            DoubleDecker dd = new DoubleDecker();

            try
            {
                DataTable dt = new DataTable();

                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetOTCPricingReferenceListing");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);

                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dd;
        }

        public static DoubleDecker GetPowerLocations()
        {
            DoubleDecker dd = new DoubleDecker();

            try
            {
                DataTable dt = new DataTable();

                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPowerLocations");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);

                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dd;
        }

        public static DoubleDecker GetGasLocations()
        {
            DoubleDecker dd = new DoubleDecker();

            try
            {
                DataTable dt = new DataTable();

                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetGasLocations");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);

                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dd;
        }

        public static DoubleDecker GetSwapSpread()
        {
            DoubleDecker dd = new DoubleDecker();

            try
            {
                DataTable dt = new DataTable();

                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetSwapSpread");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);

                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dd;
        }

        public static DataTable GetPowerLocationType()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPowerLocationType");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DataTable GetPowerZone()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPowerZone");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DataTable GetTimeZone()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetTimeZone");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DataTable GetPowerOperationalRegion()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPowerOperationalRegion");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DataTable GetPersonnel()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetPersonnel");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DataTable GetPortfolioOwners()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetPortfolioOwners");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static Int32 GetNextExchangeProductID()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetNextExchangeProductSequence");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static Int32 GetNextCurveDefinitionID()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetNextCurveDefinitionSequence");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static Int32 GetNextClearingRateID()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetClearingRateSequence");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static Int32 GetNextPricingReferenceID()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetNextPricingReferenceSequence");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static Int32 GetNextExRecBNPOpenPosSequence()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_ExRec_ExRecBNPOpenPosSequence");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }


        public static Int32 GetNextExchangeContractID()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetNextExchangeContractSequence");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static Int32 GetNextPortfolioID()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetNextPortfolioSequence");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static DataTable GetPortfolioViewers(Int32 portfolioID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetPortfolioViewers");
                cmd.Parameters.AddWithValue("@PortfolioID", portfolioID);
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static Int32 SaveExchangeContract(ExCon ec)
        {
            Int32 result = GetNextExchangeContractID();

            if (result == Int32.MinValue)
            {
                return result;
            }

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_InsertExchangeContract");

                ec.ExchangeContractId = result;
                cmd.Parameters.AddWithValue("@ExchangeContractId", ec.ExchangeContractId);
                cmd.Parameters.AddWithValue("@ContractCode", ec.ContractCode);
                cmd.Parameters.AddWithValue("@ContractDate", ec.ContractDate);
                cmd.Parameters.AddWithValue("@ExchangeProductId", ec.ExchangeProductId);
                cmd.Parameters.AddWithValue("@FirstTradeDate", ec.FirstTradeDate);
                cmd.Parameters.AddWithValue("@LastTradeDate", ec.LastTradeDate);
                cmd.Parameters.AddWithValue("@SettlementDate", ec.SettlementDate);
                cmd.Parameters.AddWithValue("@FirstDeliveryDate", ec.FirstDeliveryDate);
                cmd.Parameters.AddWithValue("@LastDeliveryDate", ec.LastDeliveryDate);
                cmd.Parameters.AddWithValue("@ExpiryDate", ec.ExpiryDate);
                cmd.Parameters.AddWithValue("@UnitId", ec.UnitId);
                cmd.Parameters.Add(ETRMDataAccess.CreateDecimalParameter("@ContractSize", ec.ContractSize));
                cmd.Parameters.Add(ETRMDataAccess.CreateDecimalParameter("@OffpeakHours", ec.OffpeakHours));
                cmd.Parameters.Add(ETRMDataAccess.CreateDecimalParameter("@PeakHours", ec.PeakHours));

                cmd.Parameters.AddWithValue("@UpdateUser", ec.UpdateUser);

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 UpdateExchangeContract(ExCon ec)
        {
            Int32 result = ec.ExchangeContractId;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_UpdateExchangeContract");

                cmd.Parameters.AddWithValue("@ExchangeContractId", ec.ExchangeContractId);
                cmd.Parameters.AddWithValue("@ContractCode", ec.ContractCode);
                cmd.Parameters.AddWithValue("@ContractDate", ec.ContractDate);
                cmd.Parameters.AddWithValue("@ExchangeProductId", ec.ExchangeProductId);
                cmd.Parameters.AddWithValue("@FirstTradeDate", ec.FirstTradeDate);
                cmd.Parameters.AddWithValue("@LastTradeDate", ec.LastTradeDate);
                cmd.Parameters.AddWithValue("@SettlementDate", ec.SettlementDate);
                cmd.Parameters.AddWithValue("@FirstDeliveryDate", ec.FirstDeliveryDate);
                cmd.Parameters.AddWithValue("@LastDeliveryDate", ec.LastDeliveryDate);
                cmd.Parameters.AddWithValue("@ExpiryDate", ec.ExpiryDate);
                cmd.Parameters.AddWithValue("@UnitId", ec.UnitId);
                cmd.Parameters.Add(ETRMDataAccess.CreateDecimalParameter("@ContractSize", ec.ContractSize));
                cmd.Parameters.Add(ETRMDataAccess.CreateDecimalParameter("@OffpeakHours", ec.OffpeakHours));
                cmd.Parameters.Add(ETRMDataAccess.CreateDecimalParameter("@PeakHours", ec.PeakHours));
                cmd.Parameters.AddWithValue("@UpdateUser", ec.UpdateUser);

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 DeleteExchangeContract(Int32 userId, Int32 contractId)
        {
            Int32 result = contractId;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_DeleteExchangeContract");

                cmd.Parameters.AddWithValue("@ExchangeContractId", contractId);
                cmd.Parameters.AddWithValue("@UpdateUser", userId);

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 GetNextOTCPricingReference()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetNextOTCCommodityPricingRefSequence");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static Int32 GetNextPowerLocation()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetNextPowerLocationSequence");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static Int32 GetNextGasLocation()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetNextGasLocationSequence");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static Int32 GetNextSwapSpread()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetNextOTCSwapSpreadSequence");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }
        public static Int32 SaveSwapSpread(SSpread cd)
        {
            Int32 result = cd.OTCSwapSpreadID;
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_InsertSwapSpread");

                cmd.Parameters.Add(CreateInt32Parameter("@OTCSwapSpreadID", cd.OTCSwapSpreadID));
                cmd.Parameters.Add(CreateInt32Parameter("@OTCSwapSpreadCommodityID", cd.OTCSwapSpreadCommodityID));
                cmd.Parameters.Add(CreateStringParameter("@OTCSwapSpreadName", cd.OTCSwapSpreadName));
                cmd.Parameters.Add(CreateInt32Parameter("@PayIndexID", cd.PayIndexID));
                cmd.Parameters.Add(CreateInt32Parameter("@ReceiveIndexID", cd.ReceiveIndexID));
                cmd.Parameters.Add(CreateDoubleParameter("@DefaultIndexConversion", 1));
                cmd.Parameters.Add(CreateInt32Parameter("@eConfirmProductID", cd.eConfirmProductID));
                cmd.Parameters.Add(CreateBoolParameter("@IsIncDec", cd.IsIncDec));
                cmd.Parameters.Add(CreateBoolParameter("@Deleted", cd.Deleted));

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 UpdateSwapSpread(SSpread cd)
        {
            Int32 result = cd.OTCSwapSpreadID;
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_UpdateSwapSpread");

                cmd.Parameters.Add(CreateInt32Parameter("@OTCSwapSpreadID", cd.OTCSwapSpreadID));
                cmd.Parameters.Add(CreateInt32Parameter("@OTCSwapSpreadCommodityID", cd.OTCSwapSpreadCommodityID));
                cmd.Parameters.Add(CreateStringParameter("@OTCSwapSpreadName", cd.OTCSwapSpreadName));
                cmd.Parameters.Add(CreateInt32Parameter("@PayIndexID", cd.PayIndexID));
                cmd.Parameters.Add(CreateInt32Parameter("@ReceiveIndexID", cd.ReceiveIndexID));
                cmd.Parameters.Add(CreateDoubleParameter("@DefaultIndexConversion", 1));
                cmd.Parameters.Add(CreateInt32Parameter("@eConfirmProductID", cd.eConfirmProductID));
                cmd.Parameters.Add(CreateBoolParameter("@IsIncDec", cd.IsIncDec));
                cmd.Parameters.Add(CreateBoolParameter("@Deleted", cd.Deleted));

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 SaveGasLocation(Int32 locationID, string gasLocation, bool isDeleted)
        {
            Int32 result = locationID;
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_InsertGasLocation");

                cmd.Parameters.Add(CreateInt32Parameter("@GasLocationID", locationID));
                cmd.Parameters.Add(CreateStringParameter("@GasLocation", gasLocation));
                cmd.Parameters.Add(CreateBoolParameter("@Deleted", isDeleted));

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 UpdateGasLocation(Int32 locationID, string gasLocation, bool isDeleted)
        {
            Int32 result = locationID;
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_UpdateGasLocation");

                cmd.Parameters.Add(CreateInt32Parameter("@GasLocationID", locationID));
                cmd.Parameters.Add(CreateStringParameter("@GasLocation", gasLocation));
                cmd.Parameters.Add(CreateBoolParameter("@Deleted", isDeleted));

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 SavePowerLocation(PowLoc cd)
        {
            Int32 result = cd.PowerLocationID;
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_InsertPowerLocation");

                cmd.Parameters.Add(CreateInt32Parameter("@PowerLocationID", cd.PowerLocationID));
                cmd.Parameters.Add(CreateStringParameter("@PowerLocation", cd.PowerLocation));
                cmd.Parameters.Add(CreateStringParameter("@FullDescription", cd.FullDescription));
                cmd.Parameters.Add(CreateStringParameter("@ShortCode", cd.ShortCode));
                cmd.Parameters.Add(CreateStringParameter("@SchedulingLocationName", cd.SchedulingLocationName));
                cmd.Parameters.Add(CreateStringParameter("@InvoiceDescription", cd.InvoiceDescription));
                cmd.Parameters.Add(CreateStringParameter("@PNodeID", cd.PNodeID));
                cmd.Parameters.Add(CreateInt32Parameter("@PowerLocationTypeId", cd.PowerLocationTypeId));
                cmd.Parameters.Add(CreateInt32Parameter("@PowerZoneId", cd.PowerZoneId));
                cmd.Parameters.Add(CreateInt32Parameter("@TimeZoneId", cd.TimeZoneId));
                cmd.Parameters.Add(CreateInt32Parameter("@PowerOperationalRegionId", cd.PowerOperationalRegionId));
                cmd.Parameters.Add(CreateBoolParameter("@Deleted", cd.Deleted));

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 UpdatedPowerLocation(PowLoc cd)
        {
            Int32 result = cd.PowerLocationID;
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_UpdatePowerLocation");

                cmd.Parameters.Add(CreateInt32Parameter("@PowerLocationID", cd.PowerLocationID));
                cmd.Parameters.Add(CreateStringParameter("@PowerLocation", cd.PowerLocation));
                cmd.Parameters.Add(CreateStringParameter("@FullDescription", cd.FullDescription));
                cmd.Parameters.Add(CreateStringParameter("@ShortCode", cd.ShortCode));
                cmd.Parameters.Add(CreateStringParameter("@SchedulingLocationName", cd.SchedulingLocationName));
                cmd.Parameters.Add(CreateStringParameter("@InvoiceDescription", cd.InvoiceDescription));
                cmd.Parameters.Add(CreateStringParameter("@PNodeID", cd.PNodeID));
                cmd.Parameters.Add(CreateInt32Parameter("@PowerLocationTypeId", cd.PowerLocationTypeId));
                cmd.Parameters.Add(CreateInt32Parameter("@PowerZoneId", cd.PowerZoneId));
                cmd.Parameters.Add(CreateInt32Parameter("@TimeZoneId", cd.TimeZoneId));
                cmd.Parameters.Add(CreateInt32Parameter("@PowerOperationalRegionId", cd.PowerOperationalRegionId));
                cmd.Parameters.Add(CreateBoolParameter("@Deleted", cd.Deleted));

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 SaveOTCPricingReference(OTCPrRef cd)
        {
            Int32 result = cd.OTCCommodityPricingRefID;
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_InsertOTCCommodityPricingRef");

                cmd.Parameters.Add(CreateInt32Parameter("@OTCCommodityPricingRefID", cd.OTCCommodityPricingRefID));
                cmd.Parameters.Add(CreateStringParameter("@CommodityPricingRef", cd.CommodityPricingRef));
                cmd.Parameters.Add(CreateStringParameter("@Description", cd.Description));
                cmd.Parameters.Add(CreateStringParameter("@GUIName", cd.CommodityPricingRef));
                cmd.Parameters.Add(CreateStringParameter("@MultiLegs", cd.MultiLegs));
                cmd.Parameters.Add(CreateInt32Parameter("@SpecifiedPriceID", cd.SpecifiedPriceID));
                cmd.Parameters.Add(CreateInt32Parameter("@CurrencyID", cd.CurrencyID));
                cmd.Parameters.Add(CreateInt32Parameter("@UnitID", cd.UnitID));
                cmd.Parameters.Add(CreateInt32Parameter("@AveragingTypeID", cd.AveragingTypeID));
                cmd.Parameters.Add(CreateInt32Parameter("@CalculationPeriodID", cd.CalculationPeriodID));
                cmd.Parameters.Add(CreateInt32Parameter("@HolidayCalendarID", cd.HolidayCalendarID));
                cmd.Parameters.Add(CreateInt32Parameter("@DayDistributionID", cd.DayDistributionID));
                cmd.Parameters.Add(CreateInt32Parameter("@DayTypeID", cd.DayTypeID));
                cmd.Parameters.Add(CreateInt32Parameter("@PricingDeliveryDateID", cd.PricingDeliveryDateID));
                cmd.Parameters.Add(CreateInt32Parameter("@RollConventionID", cd.RollConventionID));
                cmd.Parameters.Add(CreateInt32Parameter("@DeliveryTypeID", cd.DeliveryTypeID));
                cmd.Parameters.Add(CreateInt32Parameter("@PowerLocationID", cd.PowerLocationID));
                cmd.Parameters.Add(CreateInt32Parameter("@GasLocationID", cd.GasLocationID));
                cmd.Parameters.Add(CreateInt32Parameter("@eConfirmProductID", cd.eConfirmProductID));
                cmd.Parameters.Add(CreateInt32Parameter("@PricePublicationId", cd.PricePublicationId));
                cmd.Parameters.Add(CreateStringParameter("@PricingDates", cd.PricingDates));
                cmd.Parameters.Add(CreateStringParameter("@InvoiceDescription", cd.InvoiceDescription));
                cmd.Parameters.Add(CreateBoolParameter("@Deleted", cd.Deleted));

                if (cd.Deleted.HasValue && cd.Deleted.Value == false)
                {
                    cmd.Parameters.Add(CreateStringParameter("@TradeableIndex", "TRUE"));
                }
                else
                {
                    cmd.Parameters.Add(CreateStringParameter("@TradeableIndex", "FALSE"));
                }

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 UpdateOTCPricingReference(OTCPrRef cd)
        {
            Int32 result = cd.OTCCommodityPricingRefID;
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_UpdateOTCCommodityPricingRef");

                cmd.Parameters.Add(CreateInt32Parameter("@OTCCommodityPricingRefID", cd.OTCCommodityPricingRefID));
                cmd.Parameters.Add(CreateStringParameter("@CommodityPricingRef", cd.CommodityPricingRef));
                cmd.Parameters.Add(CreateStringParameter("@Description", cd.Description));
                cmd.Parameters.Add(CreateStringParameter("@GUIName", cd.CommodityPricingRef));
                cmd.Parameters.Add(CreateStringParameter("@MultiLegs", cd.MultiLegs));
                cmd.Parameters.Add(CreateInt32Parameter("@SpecifiedPriceID", cd.SpecifiedPriceID));
                cmd.Parameters.Add(CreateInt32Parameter("@CurrencyID", cd.CurrencyID));
                cmd.Parameters.Add(CreateInt32Parameter("@UnitID", cd.UnitID));
                cmd.Parameters.Add(CreateInt32Parameter("@AveragingTypeID", cd.AveragingTypeID));
                cmd.Parameters.Add(CreateInt32Parameter("@CalculationPeriodID", cd.CalculationPeriodID));
                cmd.Parameters.Add(CreateInt32Parameter("@HolidayCalendarID", cd.HolidayCalendarID));
                cmd.Parameters.Add(CreateInt32Parameter("@DayDistributionID", cd.DayDistributionID));
                cmd.Parameters.Add(CreateInt32Parameter("@DayTypeID", cd.DayTypeID));
                cmd.Parameters.Add(CreateInt32Parameter("@PricingDeliveryDateID", cd.PricingDeliveryDateID));
                cmd.Parameters.Add(CreateInt32Parameter("@RollConventionID", cd.RollConventionID));
                cmd.Parameters.Add(CreateInt32Parameter("@DeliveryTypeID", cd.DeliveryTypeID));
                cmd.Parameters.Add(CreateInt32Parameter("@PowerLocationID", cd.PowerLocationID));
                cmd.Parameters.Add(CreateInt32Parameter("@GasLocationID", cd.GasLocationID));
                cmd.Parameters.Add(CreateInt32Parameter("@eConfirmProductID", cd.eConfirmProductID));
                cmd.Parameters.Add(CreateInt32Parameter("@PricePublicationId", cd.PricePublicationId));
                cmd.Parameters.Add(CreateStringParameter("@PricingDates", cd.PricingDates));
                cmd.Parameters.Add(CreateStringParameter("@InvoiceDescription", cd.InvoiceDescription));
                cmd.Parameters.Add(CreateBoolParameter("@Deleted", cd.Deleted));

                if (cd.Deleted.HasValue && cd.Deleted.Value == false)
                {
                    cmd.Parameters.Add(CreateStringParameter("@TradeableIndex", "TRUE"));
                }
                else
                {
                    cmd.Parameters.Add(CreateStringParameter("@TradeableIndex", "FALSE"));
                }

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 SavePricingReference(PrRefInfoEntity cd)
        {
            Int32 result = cd.PricingReferenceID;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_InsertPricingReference");

                cmd.Parameters.Add(CreateInt32Parameter("@PricingReferenceID", cd.PricingReferenceID));
                cmd.Parameters.Add(CreateStringParameter("@PricingReference", cd.PricingReference));
                cmd.Parameters.Add(CreateStringParameter("@Description", cd.Description));
                cmd.Parameters.Add(CreateInt32Parameter("@SpecifiedPriceID", cd.SpecifiedPriceID));
                cmd.Parameters.Add(CreateInt32Parameter("@PricePublicationID", cd.PricePublicationID));
                cmd.Parameters.Add(CreateInt32Parameter("@CCYID", cd.CCYID));
                cmd.Parameters.Add(CreateBoolParameter("@Deleted", cd.Deleted));

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 UpdatePricingReference(PrRefInfoEntity cd)
        {
            Int32 result = cd.PricingReferenceID;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_UpdatePricingReference");

                cmd.Parameters.Add(CreateInt32Parameter("@PricingReferenceID", cd.PricingReferenceID));
                cmd.Parameters.Add(CreateStringParameter("@PricingReference", cd.PricingReference));
                cmd.Parameters.Add(CreateStringParameter("@Description", cd.Description));
                cmd.Parameters.Add(CreateInt32Parameter("@SpecifiedPriceID", cd.SpecifiedPriceID));
                cmd.Parameters.Add(CreateInt32Parameter("@PricePublicationID", cd.PricePublicationID));
                cmd.Parameters.Add(CreateInt32Parameter("@CCYID", cd.CCYID));
                cmd.Parameters.Add(CreateBoolParameter("@Deleted", cd.Deleted));

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }
        public static Int32 SaveClearingRate(ClearingRate cd)
        {
            Int32 result = cd.ClearingRateID;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_InsertClearingRate");

                cmd.Parameters.Add(CreateInt32Parameter("@ClearingRateID", cd.ClearingRateID));
                cmd.Parameters.Add(CreateInt32Parameter("@ExchangeId", cd.ExchangeID));
                cmd.Parameters.Add(CreateInt32Parameter("@ExchangeProductId", cd.ProductID));
                cmd.Parameters.Add(CreateInt32Parameter("@CurrencyId", cd.CurrencyID));
                cmd.Parameters.Add(CreateInt32Parameter("@ExecutionTypeID", cd.ExecutionTypeID));
                cmd.Parameters.Add(CreateDoubleParameter("@MarketRegistrationRate", cd.MarketRegRate));
                cmd.Parameters.Add(CreateDoubleParameter("@ClearingHouseRate", cd.ClearingHouseRate));
                cmd.Parameters.Add(CreateDoubleParameter("@FeePerLot", cd.FeePerLot));
                cmd.Parameters.Add(CreateBoolParameter("@Deleted", cd.Deleted));
                cmd.Parameters.Add(CreateDateTimeParameter("@EffectiveFrom", cd.ValidFrom.Value.Date));
                cmd.Parameters.Add(CreateDateTimeParameter("@EffectiveTill", cd.ValidTo.Value.Date));

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 UpdateClearingRate(ClearingRate cd)
        {
            Int32 result = cd.ClearingRateID;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_UpdateClearingRate");

                cmd.Parameters.Add(CreateInt32Parameter("@ClearingRateID", cd.ClearingRateID));
                cmd.Parameters.Add(CreateInt32Parameter("@ExchangeId", cd.ExchangeID));
                cmd.Parameters.Add(CreateInt32Parameter("@ExchangeProductId", cd.ProductID));
                cmd.Parameters.Add(CreateInt32Parameter("@CurrencyId", cd.CurrencyID));
                cmd.Parameters.Add(CreateInt32Parameter("@ExecutionTypeID", cd.ExecutionTypeID));
                cmd.Parameters.Add(CreateDoubleParameter("@MarketRegistrationRate", cd.MarketRegRate));
                cmd.Parameters.Add(CreateDoubleParameter("@ClearingHouseRate", cd.ClearingHouseRate));
                cmd.Parameters.Add(CreateDoubleParameter("@FeePerLot", cd.FeePerLot));
                cmd.Parameters.Add(CreateBoolParameter("@Deleted", cd.Deleted));
                cmd.Parameters.Add(CreateDateTimeParameter("@EffectiveFrom", cd.ValidFrom.Value.Date));
                cmd.Parameters.Add(CreateDateTimeParameter("@EffectiveTill", cd.ValidTo.Value.Date));

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 SaveCurveDefinition(CurDef cd)
        {
            Int32 result = cd.CurveDefinitionID;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("[sp_StaticData_InsertCurveDefinition]");


                cmd.Parameters.Add(CreateInt32Parameter("@CurveDefinitionID", cd.CurveDefinitionID));
                cmd.Parameters.Add(CreateStringParameter("@CurveName", cd.CurveName));
                cmd.Parameters.Add(CreateStringParameter("@Description", cd.Description));
                cmd.Parameters.Add(CreateInt32Parameter("@CurveTypeId", cd.CurveTypeID));
                cmd.Parameters.Add(CreateInt32Parameter("@UnderlyingId", cd.UnderlyingID));
                cmd.Parameters.Add(CreateInt32Parameter("@UnitID", cd.UnitID));
                cmd.Parameters.Add(CreateInt32Parameter("@CurrencyID", cd.CurrencyID));
                cmd.Parameters.Add(CreateStringParameter("@LIMCode", cd.LIMCode));
                cmd.Parameters.Add(CreateInt32Parameter("@YieldBasisId", cd.YieldBasisID));
                cmd.Parameters.Add(CreateInt32Parameter("@InterpolationId", cd.InterpolationID));
                cmd.Parameters.Add(CreateBoolParameter("@Tradeable", cd.Tradeable));
                cmd.Parameters.Add(CreateBoolParameter("@Deleted", cd.Deleted));

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 UpdateCurveDefinition(CurDef cd)
        {
            Int32 result = cd.CurveDefinitionID;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_UpdateCurveDefinition");

                cmd.Parameters.Add(CreateInt32Parameter("@CurveDefinitionID", cd.CurveDefinitionID));
                cmd.Parameters.Add(CreateStringParameter("@CurveName", cd.CurveName));
                cmd.Parameters.Add(CreateStringParameter("@Description", cd.Description));
                cmd.Parameters.Add(CreateInt32Parameter("@CurveTypeId", cd.CurveTypeID));
                cmd.Parameters.Add(CreateInt32Parameter("@UnderlyingId", cd.UnderlyingID));
                cmd.Parameters.Add(CreateInt32Parameter("@UnitID", cd.UnitID));
                cmd.Parameters.Add(CreateInt32Parameter("@CurrencyID", cd.CurrencyID));
                cmd.Parameters.Add(CreateStringParameter("@LIMCode", cd.LIMCode));
                cmd.Parameters.Add(CreateInt32Parameter("@YieldBasisId", cd.YieldBasisID));
                cmd.Parameters.Add(CreateInt32Parameter("@InterpolationId", cd.InterpolationID));
                cmd.Parameters.Add(CreateBoolParameter("@Tradeable", cd.Tradeable));
                cmd.Parameters.Add(CreateBoolParameter("@Deleted", cd.Deleted));

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 SaveExchangeProduct(ExProd p)
        {
            Int32 result = p.ExchangeProductId;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("[sp_StaticData_InsertExchangeProduct]");

                cmd.Parameters.AddWithValue("@ExchangeProductId", p.ExchangeProductId);
                cmd.Parameters.AddWithValue("@ExchangeProductCode", p.ExchangeProductCode);
                cmd.Parameters.AddWithValue("@Name", p.Name);
                cmd.Parameters.AddWithValue("@GUIDescription", p.GUIDescription);
                cmd.Parameters.Add(CreateStringParameter("@BeaconValue", p.BeaconValue));
                cmd.Parameters.Add(CreateInt32Parameter("@UnderlyingId", p.UnderlyingId));
                cmd.Parameters.Add(CreateInt32Parameter("@PricingModelId", p.PricingModelId));
                cmd.Parameters.Add(CreateInt32Parameter("@ExchangeId", p.ExchangeId));
                cmd.Parameters.Add(CreateDoubleParameter("@LotSize", p.LotSize));
                cmd.Parameters.Add(CreateInt32Parameter("@LotSizeUnitId", p.LotSizeUnitId));
                cmd.Parameters.Add(CreateInt32Parameter("@CurrencyId", p.CurrencyId));
                cmd.Parameters.Add(CreateDoubleParameter("@TickSize", p.TickSize));
                cmd.Parameters.Add(CreateInt32Parameter("@LocationId", p.LocationId));
                cmd.Parameters.Add(CreateInt32Parameter("@PowerPricebandId", p.PowerPricebandId));
                cmd.Parameters.Add(CreateInt32Parameter("@TradingCalendarId", p.TradingCalendarId));
                cmd.Parameters.Add(CreateInt32Parameter("@SettlementTypeId", p.SettlementTypeId));
                cmd.Parameters.Add(CreateInt32Parameter("@OptionTypeId", p.OptionTypeId));
                cmd.Parameters.Add(CreateInt32Parameter("@FuturesCurveId", p.FuturesCurveId));
                cmd.Parameters.Add(CreateInt32Parameter("@ForwardCurveId", p.ForwardCurveId));
                cmd.Parameters.Add(CreateInt32Parameter("@PricingReferenceId", p.PricingReferenceId));
                cmd.Parameters.AddWithValue("@BasisContract", p.BasisContract);
                cmd.Parameters.AddWithValue("@IsMini", p.MiniContract);
                cmd.Parameters.Add(CreateInt32Parameter("@BasisReferenceId", p.BasisReferenceId));
                cmd.Parameters.Add(CreateInt32Parameter("@BasisForwardCurveId", p.BasisForwardCurveId));
                cmd.Parameters.Add(CreateInt32Parameter("@TradeTypeId", p.TradeTypeId));
                cmd.Parameters.AddWithValue("@BrokerProductName", p.BrokerProductName);
                cmd.Parameters.AddWithValue("@BrokerProductCode", p.BrokerProductCode);
                cmd.Parameters.AddWithValue("@ExchangeProductDescription", p.ExchangeProductDescription);
                cmd.Parameters.AddWithValue("@SpanCombinedCommodity", p.SpanCombinedCommodity);
                cmd.Parameters.AddWithValue("@UpdateUser", p.UpdateUser);

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 UpdatedExchangeProduct(ExProd p)
        {
            Int32 result = p.ExchangeProductId;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_UpdateExchangeProduct");

                cmd.Parameters.AddWithValue("@ExchangeProductId", p.ExchangeProductId);
                cmd.Parameters.AddWithValue("@ExchangeProductCode", p.ExchangeProductCode);
                cmd.Parameters.AddWithValue("@Name", p.Name);
                cmd.Parameters.AddWithValue("@GUIDescription", p.GUIDescription);
                cmd.Parameters.Add(CreateStringParameter("@BeaconValue", p.BeaconValue));
                cmd.Parameters.Add(CreateInt32Parameter("@UnderlyingId", p.UnderlyingId));
                cmd.Parameters.Add(CreateInt32Parameter("@PricingModelId", p.PricingModelId));
                cmd.Parameters.Add(CreateInt32Parameter("@ExchangeId", p.ExchangeId));
                cmd.Parameters.Add(CreateDoubleParameter("@LotSize", p.LotSize));
                cmd.Parameters.Add(CreateInt32Parameter("@LotSizeUnitId", p.LotSizeUnitId));
                cmd.Parameters.Add(CreateInt32Parameter("@CurrencyId", p.CurrencyId));
                cmd.Parameters.Add(CreateDoubleParameter("@TickSize", p.TickSize));
                cmd.Parameters.Add(CreateInt32Parameter("@LocationId", p.LocationId));
                cmd.Parameters.Add(CreateInt32Parameter("@PowerPricebandId", p.PowerPricebandId));
                cmd.Parameters.Add(CreateInt32Parameter("@TradingCalendarId", p.TradingCalendarId));
                cmd.Parameters.Add(CreateInt32Parameter("@SettlementTypeId", p.SettlementTypeId));
                cmd.Parameters.Add(CreateInt32Parameter("@OptionTypeId", p.OptionTypeId));
                cmd.Parameters.Add(CreateInt32Parameter("@FuturesCurveId", p.FuturesCurveId));
                cmd.Parameters.Add(CreateInt32Parameter("@ForwardCurveId", p.ForwardCurveId));
                cmd.Parameters.Add(CreateInt32Parameter("@PricingReferenceId", p.PricingReferenceId));
                cmd.Parameters.AddWithValue("@BasisContract", p.BasisContract);
                cmd.Parameters.AddWithValue("@IsMini", p.MiniContract);
                cmd.Parameters.Add(CreateInt32Parameter("@BasisReferenceId", p.BasisReferenceId));
                cmd.Parameters.Add(CreateInt32Parameter("@BasisForwardCurveId", p.BasisForwardCurveId));
                cmd.Parameters.Add(CreateInt32Parameter("@TradeTypeId", p.TradeTypeId));
                cmd.Parameters.AddWithValue("@BrokerProductName", p.BrokerProductName);
                cmd.Parameters.AddWithValue("@BrokerProductCode", p.BrokerProductCode);
                cmd.Parameters.AddWithValue("@ExchangeProductDescription", p.ExchangeProductDescription);
                cmd.Parameters.AddWithValue("@SpanCombinedCommodity", p.SpanCombinedCommodity);
                cmd.Parameters.AddWithValue("@UpdateUser", p.UpdateUser);

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 SavePorfolioMain(Domain.Entities.Portfolio p)
        {
            Int32 result = p.PortfolioID;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("[sp_FutureTrades_InsertPortfolio]");
                cmd.Parameters.AddWithValue("@PortfolioID", p.PortfolioID);
                cmd.Parameters.AddWithValue("@PortfolioName", p.PorfolioName);
                cmd.Parameters.AddWithValue("@PortfolioOwnerID", p.PortfolioOwner);
                cmd.Parameters.AddWithValue("@PortfolioTypeID", p.PortfolioType);
                cmd.Parameters.AddWithValue("@UpdateUser", p.UserID);
                cmd.Parameters.Add(CreateInt32Parameter("@AreaID", p.PortfolioArea));
                cmd.Parameters.Add(CreateInt32Parameter("@GroupID", p.PortfolioGroup));
                cmd.Parameters.Add(CreateInt32Parameter("@LineID", p.PortfolioLine));
                cmd.Parameters.Add(CreateInt32Parameter("@StrategyID", p.PortfolioStrategy));

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static void WriteLogEntry(string message)
        {
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_WriteLogEntry");
            cmd.CommandTimeout = 30;
            cmd.Parameters.AddWithValue("@LogEntry", message);
            DataManager.GetDM().ExecuteNonQuery(cmd);
        }

        public static void WriteBeaStraLogEntry(string message)
        {
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_Security_WriteBeastraLogEntry");
            cmd.CommandTimeout = 30;
            cmd.Parameters.AddWithValue("@LogEntry", message);
            DataManager.GetDM().ExecuteNonQuery(cmd);
        }

        public static Int32 UpdatedPorfolioMain(Domain.Entities.Portfolio p)
        {
            Int32 result = p.PortfolioID;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("[sp_FutureTrades_UpdatePortfolio]");
                cmd.Parameters.AddWithValue("@PortfolioID", p.PortfolioID);
                cmd.Parameters.AddWithValue("@PortfolioName", p.PorfolioName);
                cmd.Parameters.AddWithValue("@PortfolioOwnerID", p.PortfolioOwner);
                cmd.Parameters.AddWithValue("@PortfolioTypeID", p.PortfolioType);
                cmd.Parameters.AddWithValue("@UpdateUser", p.UserID);
                cmd.Parameters.Add(CreateInt32Parameter("@AreaID", p.PortfolioArea));
                cmd.Parameters.Add(CreateInt32Parameter("@GroupID", p.PortfolioGroup));
                cmd.Parameters.Add(CreateInt32Parameter("@LineID", p.PortfolioLine));
                cmd.Parameters.Add(CreateInt32Parameter("@StrategyID", p.PortfolioStrategy));

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 CleanupPersonnelPortfolio(Int32 portfolioID)
        {
            Int32 result = portfolioID;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_CleanPersonnelPortfolio");
                cmd.Parameters.AddWithValue("@PortfolioID", portfolioID);

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 CleanupStrategies(Int32 portfolioID)
        {
            Int32 result = portfolioID;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_CleanPortfolioSubStrategies");
                cmd.Parameters.AddWithValue("@PortfolioID", portfolioID);

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 SavePorfolioStrategy(Int32 portfolioID, Int32 strategyID, Int32 userID)
        {
            Int32 result = portfolioID;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_InsertPortfolioSubStrategies");
                cmd.Parameters.AddWithValue("@PortfolioID", portfolioID);
                cmd.Parameters.AddWithValue("@StrategyID", strategyID);
                cmd.Parameters.AddWithValue("@UpdateUser", userID);

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static Int32 SavePorfolioWriter(Int32 portfolioID, Int32 writerID, Int32 userID)
        {
            Int32 result = portfolioID;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_InsertPortfolioWriters");
                cmd.Parameters.AddWithValue("@PortfolioID", portfolioID);
                cmd.Parameters.AddWithValue("@WriterID", writerID);
                cmd.Parameters.AddWithValue("@UpdateUser", userID);

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return result;
        }

        public static DataTable GetPortfolioWriters(Int32 portfolioID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetPortfolioWriters");
                cmd.Parameters.AddWithValue("@PortfolioID", portfolioID);
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DataTable GetPortfolioSelectedSubStrategies(Int32 portfolioID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetPortfolioSubStrategies");
                cmd.Parameters.AddWithValue("@PortfolioID", portfolioID);
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DataTable GetPortfolioAvailableSubStrategies(Int32 portfolioID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetPortfolioAvailableSubStrategies");
                cmd.Parameters.AddWithValue("@PortfolioID", portfolioID);
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DataTable GetPortfolioType()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetPortfolioType");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DataTable GetPortfolioGroup()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetPortfolioGroup");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }
        public static DataTable GetPortfolioArea()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetPortfolioArea");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }
        public static DataTable GetPortfolioLine()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetPortfolioLine");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }
        public static DataTable GetPortfolioStrategy()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetPortfolioStrategy");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DataTable GetAssociatedPortfolios(Int32 partyId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"select PortfolioID
                                from PartyPortfolio
                                where PartyID = @PartyID
                                ;";

                SqlParameter pparty = new SqlParameter("@PartyID", partyId);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(pparty);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();

            }
            return dt;
        }

        public static void DeleteFunctions(Int32 partyId)
        {
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"DELETE
                                from PartyFunction
                                where PartyID = @PartyID
                                ;";

                SqlParameter pparty = new SqlParameter("@PartyID", partyId);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(pparty);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
        }

        public static void DeletePortfolios(Int32 partyId)
        {
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"DELETE
                                from PartyPortfolio
                                where PartyID = @PartyID
                                ;";

                SqlParameter pparty = new SqlParameter("@PartyID", partyId);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(pparty);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
        }

        public static Int32 SavePartyFunction(Int32 partyId, Int32 functionId)
        {

            SqlCommand cmd = new SqlCommand();
            Int32 result = partyId;

            try
            {
                Int32 maxRelationID = MaxRelationId() + 1;

                string sql3 = @"INSERT INTO PartyFunction(
                                PartyID,
                                PartyFunctionTypeID
                                ) VALUES  (
                                @PartyID,
                                @PartyFunctionTypeID
                                )";

                SqlParameter party = new SqlParameter("@PartyID", partyId);
                SqlParameter function = new SqlParameter("@PartyFunctionTypeID", functionId);

                cmd = DataManager.GetDM().GetCommandT(sql3);
                cmd.Parameters.Add(party);
                cmd.Parameters.Add(function);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return result;
        }

        public static Int32 SavePartyPortfolio(Int32 partyId, Int32 portfolioId)
        {

            SqlCommand cmd = new SqlCommand();
            Int32 result = partyId;

            try
            {
                Int32 maxRelationID = MaxRelationId() + 1;

                string sql3 = @"INSERT INTO PartyPortfolio(
                                PartyID,
                                PortfolioID
                                ) VALUES  (
                                @PartyID,
                                @PortfolioID
                                )";

                SqlParameter party = new SqlParameter("@PartyID", partyId);
                SqlParameter portfolio = new SqlParameter("@PortfolioID", portfolioId);

                cmd = DataManager.GetDM().GetCommandT(sql3);
                cmd.Parameters.Add(party);
                cmd.Parameters.Add(portfolio);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return result;
        }

        public static DataTable GetPortfoliosSubStrategies(Int32 portfolioId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                SELECT -1 as PortfolioSubStrategyID, convert(nvarchar(255), NULL) as PortfolioSubStrategy
                                UNION ALL
                                SELECT PortfolioSubStrategyID, PortfolioSubStrategy
                                from PortfolioSubStrategy
                                where PortfolioSubStrategyID in (SELECT PortfolioSubStrategyID
								                                 FROM PortfolioSubStrategyRelation
                                                                 where PortfolioID = @PortfolioID) and Deleted = 0
                                order by PortfolioSubStrategy";

                SqlParameter portfolio = new SqlParameter("@PortfolioID", portfolioId);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(portfolio);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetPortfoliosForPartyAndTrader(Int32 partyId, Int32 traderId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                select 
	                                p.Portfolio, 
	                                p.PortfolioID --,pl.username, pty.shortname, pap.partyid, pp.personnelid
                                from 
	                                Portfolio p 
	                            --    join personnelportfolio pp on p.portfolioid = pp.PortfolioID 
								--	  join Personnel per on per.PersonnelID = pp.PersonnelID 
	                            --    join partyportfolio pap on pap.portfolioid = p.portfolioid 
	                            --    join party pty on pty.PartyId = pap.PartyId
                                where p.deleted = 0
                                --and pp.deleted = 0
                                --and pap.deleted = 0
                                --and pty.deleted = 0
                                --and pty.PartyID = @PartyID
                                --and per.PersonnelID = @TraderID
                                order by portfolio  ";


                SqlParameter self = new SqlParameter("@PartyID", partyId);
                SqlParameter trader = new SqlParameter("@TraderID", traderId);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(self);
                cmd.Parameters.Add(trader);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        //////public static DataTable GetAllPortfolios(bool includeEmptyRow)
        //////{
        //////    DataTable dt = new DataTable();
        //////    SqlCommand cmd = new SqlCommand();
        //////    SqlConnection cn = new SqlConnection();
        //////    try
        //////    {
        //////        string connection = System.Configuration.ConfigurationManager.ConnectionStrings["."].ConnectionString;
        //////        cn = new SqlConnection(connection);
        //////        cn.Open();

        //////        string sql2 = string.Empty;

        //////        if (includeEmptyRow)
        //////        {
        //////            sql2 += "select -1 as PortfolioID, convert(nvarchar(255), NULL) as Portfolio union all ";
        //////        }

        //////        sql2 += @"SELECT PortfolioID, Portfolio FROM Portfolio;";

        //////        cmd = new SqlCommand(sql2, cn);
        //////        var dataReader = cmd.ExecuteReader();
        //////        dt.Load(dataReader);
        //////    }
        //////    catch (Exception ex)
        //////    {
        //////        ETRMDataAccess.WriteLogEntry(ex.Message);
        //////    }
        //////    finally
        //////    {
        //////        cmd.Dispose();
        //////        cn.Close();
        //////    }
        //////    return dt;
        //////}

        public static DataTable GetAllPortfolios()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPortfolios");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DataTable GetAllFunctions()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPartyFunctions");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DataTable GetPartyFunction()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"select PartyFunctionTypeID, PartyFunctionType from PartyFunctionType;";
                cmd = DataManager.GetDM().GetCommandT(sql2);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static bool IsPartyFunctionIn(Int32 partyId, Int32 functionId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"select * from PartyFunction where PartyId = @PartyId and PartyFunctionTypeID = @FunctionId;";

                SqlParameter party = new SqlParameter("@PartyId", partyId);
                SqlParameter function = new SqlParameter("@FunctionId", functionId);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(party);
                cmd.Parameters.Add(function);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            if (IsEmpty(dt))
            {
                return false;
            }
            return true;
        }

        public static DataTable GetBrokerRateTypes()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                //string emptyRow = "select -1 as PartyBrokerRateTypeID, convert(nvarchar(255), NULL) as PartyBrokerRateType union all ";
                string sql2 = @"SELECT PartyBrokerRateTypeID, PartyBrokerRateType FROM PartyBrokerRateType";

                cmd = DataManager.GetDM().GetCommandT(sql2);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetBrokerRateTypesNullable()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string emptyRow = "select -1 as PartyBrokerRateTypeID, convert(nvarchar(255), NULL) as PartyBrokerRateType union all ";
                string sql2 = emptyRow + @" SELECT PartyBrokerRateTypeID, PartyBrokerRateType FROM PartyBrokerRateType";

                cmd = DataManager.GetDM().GetCommandT(sql2);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetFeeType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"SELECT CashFlowTypeId, CashflowType FROM CashflowType";
                cmd = DataManager.GetDM().GetCommandT(sql2);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetFeeType(FeeType feeType)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = string.Empty;

                switch (feeType)
                {
                    case FeeType.Exchange:
                        sql2 = @"SELECT CashFlowTypeId, CashflowType 
                                 FROM CashflowType
                                 WHERE CashflowTypeId in (2, 3)
                                ";
                        break;

                    case FeeType.OTC:
                        sql2 = @"SELECT CashFlowTypeId, CashflowType 
                                 FROM CashflowType
                                 WHERE CashflowTypeId in (50)
                                ";
                        break;
                }

                cmd = DataManager.GetDM().GetCommandT(sql2);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable CalculationPeriod()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"SELECT CalculationPeriodId, CalculationPeriod FROM CalculationPeriod";
                cmd = DataManager.GetDM().GetCommandT(sql2);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetDeliveryTypes()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"SELECT DeliveryTypeId, DeliveryType FROM DeliveryType";
                cmd = DataManager.GetDM().GetCommandT(sql2);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetRestrictedDeliveryTypes()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetRestrictedDeliveryTypes");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetExecutionTypes()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"SELECT ExecutionTypeID, ExecutionType FROM ExecutionType";
                cmd = DataManager.GetDM().GetCommandT(sql2);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetCurrencies()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"SELECT CurrencyID, CurrencyISOCode AS Currency FROM Currency ORDER BY DiscountingIndexId asc";
                cmd = DataManager.GetDM().GetCommandT(sql2);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetCurrenciesSP(bool isNull)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetCurrencies");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetClearingAccounts(Int32 brokerId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetClearingAccounts");
                cmd.Parameters.AddWithValue("@ClearingPartyID", brokerId);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetClearingSubAccounts(Int32 clearingAccountId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetClearingSubAccounts");
                cmd.Parameters.AddWithValue("@ClearingAccountID ", clearingAccountId);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetExchangeNFARates(Int32 exchangeId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetExchangeNFARates");
                cmd.Parameters.AddWithValue("@ExchangeId", exchangeId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetExchangeClearingRates(Int32 exchangeId, Int32 productId, Int32 executionTypeId, DateTime tradeDate)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetExchangeClearingRates");
                cmd.Parameters.AddWithValue("@ExchangeId", exchangeId);
                cmd.Parameters.AddWithValue("@ExchangeProductId", productId);
                cmd.Parameters.AddWithValue("@ExecutionTypeID", executionTypeId);
                cmd.Parameters.AddWithValue("@TradeDate", tradeDate);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetTradeTypes()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"SELECT TradeTypeID, TradeType FROM TradeType";
                cmd = DataManager.GetDM().GetCommandT(sql2);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }
        //sp_StaticData_GetCurveDefinition
        public static DataTable GetExchangeContract(Int32 exchangeContractID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                SELECT *
                                FROM ExchangeContract
                                where  ExchangeContractId = @ExchangeContractId;
                               ";

                SqlParameter parId = new SqlParameter("@ExchangeContractId", exchangeContractID);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(parId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetSeasons(Int32 underlyingId, Int32 tradeTypeId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                SELECT *
                                FROM TradeSeasons
                                WHERE SeasonUnderlyingId = @SeasonUnderlyingId
                                AND SeasonTradeTypeId = @SeasonTradeTypeId
                               ";

                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.AddWithValue("@SeasonUnderlyingId", underlyingId);
                cmd.Parameters.AddWithValue("@SeasonTradeTypeId", tradeTypeId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetUIDefaults()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                SELECT *
                                FROM UI_Settings
                               ";

                cmd = DataManager.GetDM().GetCommandT(sql2);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static string GetStringUIDefault(Int32 defaultID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                SELECT SettingValueStr
                                FROM UI_Settings
                                WHERE SettingID = @DefaultID
                               ";

                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.AddWithValue("@DefaultID", defaultID);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            if (ETRMDataAccess.IsEmpty(dt))
            {
                return string.Empty;
            }

            return Convert.ToString(dt.Rows[0][0]);
        }

        public static Int32 GetNumericDefault(DataTable dt, Int32 key)
        {
            Int32 result = Int32.MinValue;
            try
            {
                DataRow[] rows = dt.Select(string.Format("SettingID = {0}", key.ToString()));

                if (rows != null && rows.Count() > 0)
                {
                    result = Convert.ToInt32(rows[0]["SettingValueNr"]);
                }
            }
            catch { }

            return result;
        }

        public static string GetStringDefault(DataTable dt, Int32 key)
        {
            string result = string.Empty;
            try
            {
                DataRow[] rows = dt.Select(string.Format("SettingID = {0}", key.ToString()));

                if (rows != null && rows.Count() > 0)
                {
                    result = Convert.ToString(rows[0]["SettingValueStr"]);
                }
            }
            catch { }

            return result;
        }

        public static string GetFormTypeForOptions(Int32 tradeID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_BeaStra_GetOptionForm");
                SqlParameter epID = new SqlParameter("@TradeID", tradeID);
                cmd.Parameters.Add(epID);
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            if (ETRMDataAccess.IsEmpty(dt))
            {
                return "NONE";
            }
            return Convert.ToString(dt.Rows[0][0]);
        }

        public static DataTable GetExchangeProduct(Int32 exchangeProductID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetExchangeProduct");
                SqlParameter epID = new SqlParameter("@ExchangeProductID", exchangeProductID);
                cmd.Parameters.Add(epID);
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DataTable GetPricerInformation(string granularity, Int32 contractCodeId, Int32 exchangeProductId, Int32 futuresCurveId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetPricerData");
                cmd.Parameters.AddWithValue("@Granularity", granularity);
                cmd.Parameters.AddWithValue("@ContractCodeID", contractCodeId);
                cmd.Parameters.AddWithValue("@ExchangeProductID", exchangeProductId);
                cmd.Parameters.AddWithValue("@FuturesCurveId", futuresCurveId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetCurveDefinitionRecord(Int32 curveDefinitionID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetCurveDefinition");
                SqlParameter parId = new SqlParameter("@CurveDefinitionID", curveDefinitionID);

                cmd.Parameters.Add(parId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }
        public static DataTable GetExchangeClearingRateRecord(Int32 clearingRateID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetExchangeClearingRate");
                SqlParameter parId = new SqlParameter("@ExchangeClearingRateID", clearingRateID);

                cmd.Parameters.Add(parId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetPricingReferenceRecord(Int32 pricingReferenceID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPricingReferenceDefinition");
                SqlParameter parId = new SqlParameter("@PricingReferenceID", pricingReferenceID);

                cmd.Parameters.Add(parId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetOTCPricingReferenceRecord(Int32 pricingReferenceID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetOTCPricingReferenceDefinition");
                SqlParameter parId = new SqlParameter("@PricingReferenceID", pricingReferenceID);

                cmd.Parameters.Add(parId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetSwapSpreadRecord(Int32 SwapSpreadID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetSwapSpreadRecord");
                SqlParameter parId = new SqlParameter("@SwapSpreadID", SwapSpreadID);

                cmd.Parameters.Add(parId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetPowerLocationRecord(Int32 locationID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPowerLocationRecord");
                SqlParameter parId = new SqlParameter("@LocationID", locationID);

                cmd.Parameters.Add(parId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetGasLocationRecord(Int32 locationID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetGasLocationRecord");
                SqlParameter parId = new SqlParameter("@LocationID", locationID);

                cmd.Parameters.Add(parId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetExchangeContractsSP(Int32 productId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetExchangeContractsSP");
                SqlParameter parId = new SqlParameter("@ExchangeProductId", productId);

                cmd.Parameters.Add(parId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable ExRecGetValuationControlRecords()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_ExRec_GetValuationControlRecords");

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable ExRecGetOpenPosMatches(Int32 reconciliationID, Int32 daily)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_ExRec_GetOpenPosMatches");

                SqlParameter recId = new SqlParameter("@ReconciliationID", reconciliationID);
                SqlParameter pdaily = new SqlParameter("@Daily", daily);

                cmd.Parameters.Add(recId);
                cmd.Parameters.Add(pdaily);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable ExRecGetOpenPosMissmatches(Int32 reconciliationID, Int32 daily)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_ExRec_GetOpenPosMissmatches");
                SqlParameter recId = new SqlParameter("@ReconciliationID", reconciliationID);
                SqlParameter pdaily = new SqlParameter("@Daily", daily);

                cmd.Parameters.Add(recId);
                cmd.Parameters.Add(pdaily);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable ExRecGetOpenPosDetailsMissmatches(Int32 reconciliationID, Int32 daily)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetOpenPosDetailsMissmatches");

                SqlParameter recId = new SqlParameter("@ReconciliationID", reconciliationID);
                SqlParameter pdaily = new SqlParameter("@Daily", daily);
                cmd.Parameters.Add(recId);
                cmd.Parameters.Add(pdaily);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable ExRecGetOpenPosDetailsMatches(Int32 reconciliationID, Int32 daily)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetOpenPosDetailsMatches");

                SqlParameter recId = new SqlParameter("@ReconciliationID", reconciliationID);
                SqlParameter pdaily = new SqlParameter("@Daily", daily);
                cmd.Parameters.Add(recId);
                cmd.Parameters.Add(pdaily);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable ExRecGetCFITradesMatches(Int32 reconciliationID, Int32 daily)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetCFITradesMatches");
                SqlParameter recId = new SqlParameter("@ReconciliationID", reconciliationID);
                SqlParameter pdaily = new SqlParameter("@Daily", daily);

                cmd.Parameters.Add(recId);
                cmd.Parameters.Add(pdaily);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable ExRecGetCFITradesMissmatches(Int32 reconciliationID, Int32 daily)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetCFITradesMissmatches");
                SqlParameter recId = new SqlParameter("@ReconciliationID", reconciliationID);
                SqlParameter pdaily = new SqlParameter("@Daily", daily);

                cmd.Parameters.Add(recId);
                cmd.Parameters.Add(pdaily);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable ExRecGetExpiringDealsMatches(Int32 reconciliationID, Int32 daily)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetExpiredTradesMatches");

                SqlParameter recId = new SqlParameter("@ReconciliationID", reconciliationID);
                SqlParameter pdaily = new SqlParameter("@Daily", daily);
                cmd.Parameters.Add(recId);
                cmd.Parameters.Add(pdaily);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable ExRecGetExpiringDealsMissmatches(Int32 reconciliationID, Int32 daily)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetExpiredTradesMissmatches");

                SqlParameter recId = new SqlParameter("@ReconciliationID", reconciliationID);
                SqlParameter pdaily = new SqlParameter("@Daily", daily);
                cmd.Parameters.Add(recId);
                cmd.Parameters.Add(pdaily);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable ExRecGetCancelledDealsMatches(Int32 reconciliationID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetCancelledTradesMatches");

                SqlParameter recId = new SqlParameter("@ReconciliationID", reconciliationID);
                cmd.Parameters.Add(recId);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable ExRecGetCancelledDealsMissmatches(Int32 reconciliationID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetCancelledTradesMissmatches");

                SqlParameter recId = new SqlParameter("@ReconciliationID", reconciliationID);
                cmd.Parameters.Add(recId);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }


        public static DataTable ExRecGetOpenPosBNP()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_ExRec_GetOpenPosBNP");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable ExRecGetOpenPosTemplate()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_ExRec_GetOpenPosTemplate");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            DataTable reportTemplate = new DataTable("d");

            List<string> numericCols = ExclusionList();

            if (dt != null)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    if (numericCols.Contains(dc.ColumnName.ToUpper().Trim()))
                    {
                        reportTemplate.Columns.Add(dc.ColumnName, typeof(double));
                    }
                    else
                    {
                        reportTemplate.Columns.Add(dc.ColumnName, typeof(string));
                    }
                }
            }

            return reportTemplate;
        }

        public static List<string> ExclusionList()
        {
            List<string> colNames = new List<string>();
            colNames.Add("VM");
            colNames.Add("OTE");
            return colNames;
        }

        public static DataTable ExRecGetOpenPosDetailsTemplate()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetOpenPosDetailsTemplate");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            DataTable reportTemplate = new DataTable("d");

            List<string> numericCols = ExclusionList();

            if (dt != null)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    if (numericCols.Contains(dc.ColumnName.ToUpper().Trim()))
                    {
                        reportTemplate.Columns.Add(dc.ColumnName, typeof(double));
                    }
                    else
                    {
                        reportTemplate.Columns.Add(dc.ColumnName, typeof(string));
                    }
                }
            }

            //////if (dt != null)
            //////{
            //////    foreach (DataColumn dc in dt.Columns)
            //////    {
            //////        reportTemplate.Columns.Add(dc.ColumnName, typeof(string));
            //////    }
            //////}

            return reportTemplate;
        }

        public static DataTable ExRecGetCFITradesTemplate()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetCFITradesTemplate");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            DataTable reportTemplate = new DataTable("d");

            //////if (dt != null)
            //////{
            //////    foreach (DataColumn dc in dt.Columns)
            //////    {
            //////        reportTemplate.Columns.Add(dc.ColumnName, typeof(string));
            //////    }
            //////}

            List<string> numericCols = ExclusionList();

            if (dt != null)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    if (numericCols.Contains(dc.ColumnName.ToUpper().Trim()))
                    {
                        reportTemplate.Columns.Add(dc.ColumnName, typeof(double));
                    }
                    else
                    {
                        reportTemplate.Columns.Add(dc.ColumnName, typeof(string));
                    }
                }
            }

            return reportTemplate;
        }

        public static DataTable ExRecGetExpiringTradesTemplate()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetExpiredTradesTemplate");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            DataTable reportTemplate = new DataTable("d");

            if (dt != null)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    reportTemplate.Columns.Add(dc.ColumnName, typeof(string));
                }
            }

            return reportTemplate;
        }

        public static DataTable ExRecGetCancelledTradesTemplate()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetCancelledTradesTemplate");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            DataTable reportTemplate = new DataTable("d");

            if (dt != null)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    reportTemplate.Columns.Add(dc.ColumnName, typeof(string));
                }
            }

            return reportTemplate;
        }


        public static DataTable ExRecGetExpiringTradesRecord(Int32 importID, DataRow header, string side)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                if (side == ExRecInfo.BSC)
                { cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetExpiredTradesBSCRow"); }
                else if (side == ExRecInfo.BNP)
                { cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetExpiredTradesBNPRow"); }
                else
                { return null; }
                cmd.Parameters.AddWithValue("@ImportID", importID);
                cmd.Parameters.AddWithValue("@Exchange", Convert.ToString(header["EXCHANGE"]));
                cmd.Parameters.AddWithValue("@Intermediary", Convert.ToString(header["INTERMEDIARY"]));
                cmd.Parameters.AddWithValue("@PhysicalSettlement", Convert.ToString(header["PHYSICAL_SETTLEMENT"]));
                cmd.Parameters.AddWithValue("@ExchangeCode", Convert.ToString(header["EXCHANGE_CODE"]));
                cmd.Parameters.AddWithValue("@Party", Convert.ToString(header["PARTY"]));
                cmd.Parameters.AddWithValue("@Account", Convert.ToString(header["ACCOUNT"]));
                cmd.Parameters.AddWithValue("@Product", Convert.ToString(header["PRODUCT"]));
                cmd.Parameters.AddWithValue("@ProductType", Convert.ToString(header["PRODUCT_TYPE"]));
                cmd.Parameters.AddWithValue("@BuySell", Convert.ToString(header["BUY_SELL"]));
                cmd.Parameters.AddWithValue("@MatMonth", Convert.ToString(header["MAT_MONTH"]));
                cmd.Parameters.AddWithValue("@MatYear", Convert.ToString(header["MAT_YEAR"]));
                //cmd.Parameters.AddWithValue("@MatDate", Convert.ToDateTime(header["MAT_DATE"]).Date);
                //cmd.Parameters.AddWithValue("@DeliveryDate", Convert.ToDateTime(header["DELIVERY_DATE"]).Date);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable ExRecGetExpiringTradesRecord(Int32 importID, DataRow header, ExRecReportManager.ReportType rtype, string side)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                if (side == ExRecInfo.BSC)
                { cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetExpiredTradesBSCRow"); }
                else if (side == ExRecInfo.BNP)
                { cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetExpiredTradesBNPRow"); }
                else
                { return null; }
                cmd.Parameters.AddWithValue("@ImportID", importID);
                cmd.Parameters.AddWithValue("@Exchange", Convert.ToString(header["EXCHANGE"]));
                cmd.Parameters.AddWithValue("@Intermediary", Convert.ToString(header["INTERMEDIARY"]));
                cmd.Parameters.AddWithValue("@PhysicalSettlement", Convert.ToString(header["PHYSICAL_SETTLEMENT"]));
                cmd.Parameters.AddWithValue("@ExchangeCode", Convert.ToString(header["EXCHANGE_CODE"]));
                cmd.Parameters.AddWithValue("@Party", Convert.ToString(header["PARTY"]));
                cmd.Parameters.AddWithValue("@Account", Convert.ToString(header["ACCOUNT"]));
                cmd.Parameters.AddWithValue("@Product", Convert.ToString(header["PRODUCT"]));
                cmd.Parameters.AddWithValue("@ProductType", Convert.ToString(header["PRODUCT_TYPE"]));
                cmd.Parameters.AddWithValue("@BuySell", Convert.ToString(header["BUY_SELL"]));
                cmd.Parameters.AddWithValue("@MatMonth", Convert.ToString(header["MAT_MONTH"]));
                cmd.Parameters.AddWithValue("@MatYear", Convert.ToString(header["MAT_YEAR"]));

                if (rtype == ExRecReportManager.ReportType.Daily)
                {
                    cmd.Parameters.AddWithValue("@MatDate", Convert.ToDateTime(header["MAT_DATE"].ToString()).Date);
                }

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable ExRecGetCFITradesRecord(Int32 importID, DataRow header, string side)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                if (side == ExRecInfo.BSC)
                { cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetBSCCFITradesRow"); }
                else if (side == ExRecInfo.BNP)
                { cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetBNPCFITradesRow"); }
                else
                { return null; }
                cmd.Parameters.AddWithValue("@ImportID", importID);
                cmd.Parameters.AddWithValue("@ExchangeID", Convert.ToString(header["EXCHANGE_ID"]));
                cmd.Parameters.AddWithValue("@Product", Convert.ToString(header["PRODUCT"]));
                cmd.Parameters.AddWithValue("@BuySell", Convert.ToString(header["BUY_SELL"]));
                cmd.Parameters.AddWithValue("@MatMonth", Convert.ToString(header["MAT_MONTH"]));
                cmd.Parameters.AddWithValue("@MatYear", Convert.ToString(header["MAT_YEAR"]));
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable ExRecGetCFITradesRecord(Int32 importID, DataRow header, ExRecReportManager.ReportType rtype, string side)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                if (side == ExRecInfo.BSC)
                { cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetBSCCFITradesRow"); }
                else if (side == ExRecInfo.BNP)
                { cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetBNPCFITradesRow"); }
                else
                { return null; }
                cmd.Parameters.AddWithValue("@ImportID", importID);
                cmd.Parameters.Add(CreateStringParameter("@ExchangeID", Convert.ToString(header["EXCHANGE_ID"])));
                cmd.Parameters.AddWithValue("@Product", Convert.ToString(header["PRODUCT"]));
                cmd.Parameters.AddWithValue("@BuySell", Convert.ToString(header["BUY_SELL"]));
                cmd.Parameters.AddWithValue("@MatMonth", Convert.ToString(header["MAT_MONTH"]));
                cmd.Parameters.AddWithValue("@MatYear", Convert.ToString(header["MAT_YEAR"]));

                if (rtype == ExRecReportManager.ReportType.Daily)
                {
                    cmd.Parameters.AddWithValue("@MatDate", Convert.ToDateTime(header["MAT_DATE"].ToString()).Date);
                }

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable ExRecGetOpenPosDetailsRecord(Int32 importID, DataRow header, string side)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                if (side == ExRecInfo.BSC)
                { cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetOpenPosBSCDetailsRow"); }
                else if (side == ExRecInfo.BNP)
                { cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetOpenPosBNPDetailsRow"); }
                else
                { return null; }
                cmd.Parameters.AddWithValue("@ImportID", importID);
                cmd.Parameters.AddWithValue("@ExchangeID", Convert.ToString(header["EXCHANGE_ID"]));
                cmd.Parameters.AddWithValue("@Product", Convert.ToString(header["PRODUCT"]));
                cmd.Parameters.AddWithValue("@BuySell", Convert.ToString(header["BUY_SELL"]));
                cmd.Parameters.AddWithValue("@MatMonth", Convert.ToString(header["MAT_MONTH"]));
                cmd.Parameters.AddWithValue("@MatYear", Convert.ToString(header["MAT_YEAR"]));

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable ExRecGetOpenPosDetailsRecord(Int32 importID, DataRow header, ExRecReportManager.ReportType rtype, string side)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                if (side == ExRecInfo.BSC)
                { cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetOpenPosBSCDetailsRow"); }
                else if (side == ExRecInfo.BNP)
                { cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetOpenPosBNPDetailsRow"); }
                else
                { return null; }
                cmd.Parameters.AddWithValue("@ImportID", importID);
                cmd.Parameters.AddWithValue("@ExchangeID", Convert.ToString(header["EXCHANGE_ID"]));
                cmd.Parameters.AddWithValue("@Product", Convert.ToString(header["PRODUCT"]));
                cmd.Parameters.AddWithValue("@BuySell", Convert.ToString(header["BUY_SELL"]));
                cmd.Parameters.AddWithValue("@MatMonth", Convert.ToString(header["MAT_MONTH"]));
                cmd.Parameters.AddWithValue("@MatYear", Convert.ToString(header["MAT_YEAR"]));

                if (rtype == ExRecReportManager.ReportType.Daily)
                {
                    cmd.Parameters.AddWithValue("@MatDate", Convert.ToDateTime(header["MAT_DATE"].ToString()).Date);
                }

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }


        public static DataTable ExRecGetOpenPosRecord(Int32 importID, DataRow header, ExRecReportManager.ReportType rtype, string side)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                if (side == ExRecInfo.BSC)
                { cmd = DataManager.GetDM().GetCommand("dbo.sp_ExRec_GetOpenPosBSCRow"); }
                else if (side == ExRecInfo.BNP)
                { cmd = DataManager.GetDM().GetCommand("dbo.sp_ExRec_GetOpenPosBNPRow"); }
                else
                { return null; }

                cmd.Parameters.AddWithValue("@ImportID", importID);
                cmd.Parameters.AddWithValue("@Exchange", Convert.ToString(header["EXCHANGE"]));
                cmd.Parameters.AddWithValue("@Account", Convert.ToString(header["ACCOUNT"]));
                cmd.Parameters.AddWithValue("@Clearer", Convert.ToString(header["CLEARER"]));
                cmd.Parameters.AddWithValue("@Intermediary", Convert.ToString(header["INTERMEDIARY"]));
                cmd.Parameters.AddWithValue("@Party", Convert.ToString(header["PARTY"]));
                cmd.Parameters.AddWithValue("@ProcuctType", Convert.ToString(header["PRODUCT_TYPE"]));
                cmd.Parameters.AddWithValue("@Product", Convert.ToString(header["PRODUCT"]));
                cmd.Parameters.AddWithValue("@ProdDesc", Convert.ToString(header["PRODUCT_DESCRIPTION"]));
                cmd.Parameters.AddWithValue("@ExCode", Convert.ToString(header["EXCHANGE_CODE"]));
                cmd.Parameters.AddWithValue("@BuySell", Convert.ToString(header["BUY_SELL"]));
                cmd.Parameters.AddWithValue("@MathMonth", Convert.ToString(header["MATH_MONTH"]));
                cmd.Parameters.AddWithValue("@MathYear", Convert.ToString(header["MATH_YEAR"]));

                if (rtype == ExRecReportManager.ReportType.Daily)
                {
                    cmd.Parameters.AddWithValue("@MatDate", Convert.ToDateTime(header["MAT_DATE"].ToString()).Date);
                }

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable ExRecGetOpenPosRecord(Int32 importID, DataRow header, string side)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                if (side == ExRecInfo.BSC)
                { cmd = DataManager.GetDM().GetCommand("dbo.sp_ExRec_GetOpenPosBSCRow"); }
                else if (side == ExRecInfo.BNP)
                { cmd = DataManager.GetDM().GetCommand("dbo.sp_ExRec_GetOpenPosBNPRow"); }
                else
                { return null; }

                cmd.Parameters.AddWithValue("@ImportID", importID);
                cmd.Parameters.AddWithValue("@Exchange", Convert.ToString(header["EXCHANGE"]));
                cmd.Parameters.AddWithValue("@Account", Convert.ToString(header["ACCOUNT"]));
                cmd.Parameters.AddWithValue("@Clearer", Convert.ToString(header["CLEARER"]));
                cmd.Parameters.AddWithValue("@Intermediary", Convert.ToString(header["INTERMEDIARY"]));
                cmd.Parameters.AddWithValue("@Party", Convert.ToString(header["PARTY"]));
                cmd.Parameters.AddWithValue("@ProcuctType", Convert.ToString(header["PRODUCT_TYPE"]));
                cmd.Parameters.AddWithValue("@Product", Convert.ToString(header["PRODUCT"]));
                cmd.Parameters.AddWithValue("@ProdDesc", Convert.ToString(header["PRODUCT_DESCRIPTION"]));
                cmd.Parameters.AddWithValue("@ExCode", Convert.ToString(header["EXCHANGE_CODE"]));
                cmd.Parameters.AddWithValue("@BuySell", Convert.ToString(header["BUY_SELL"]));
                cmd.Parameters.AddWithValue("@MathMonth", Convert.ToString(header["MATH_MONTH"]));
                cmd.Parameters.AddWithValue("@MathYear", Convert.ToString(header["MATH_YEAR"]));

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable ExRecGetCancelledTradesRecord(Int32 importID, DataRow header, string side)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                if (side == ExRecInfo.BSC)
                { cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetCancelledTradesBSCRow"); }
                else if (side == ExRecInfo.BNP)
                { cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetCancelledTradesBNPRow"); }
                else
                { return null; }
                cmd.Parameters.AddWithValue("@ImportID", importID);
                cmd.Parameters.AddWithValue("@ExchangeID", Convert.ToString(header["EXCHANGE_ID"]));
                cmd.Parameters.AddWithValue("@Product", Convert.ToString(header["PRODUCT"]));
                cmd.Parameters.AddWithValue("@BuySell", Convert.ToString(header["BUY_SELL"]));
                cmd.Parameters.AddWithValue("@MatMonth", Convert.ToString(header["MAT_MONTH"]));
                cmd.Parameters.AddWithValue("@MatYear", Convert.ToString(header["MAT_YEAR"]));
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetTradingProductCodesTotalSP(TradingProduct tradingProduct, Int32 exchangeId, Int32 tradeTypeId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetTradingProductCodesTotal");
                SqlParameter parId = new SqlParameter();

                switch (tradingProduct)
                {
                    case TradingProduct.Power:
                        parId = new SqlParameter("@UnderlyingId", 6);
                        break;
                    case TradingProduct.Gas:
                        parId = new SqlParameter("@UnderlyingId", 3);
                        break;
                    case TradingProduct.Cash:
                        parId = new SqlParameter("@UnderlyingId", 8);
                        break;
                    case TradingProduct.IR:
                        parId = new SqlParameter("@UnderlyingId", 23);
                        break;
                    case TradingProduct.Option:
                        parId = new SqlParameter("@UnderlyingId", 7);
                        break;
                    default:
                        parId = new SqlParameter("@UnderlyingId", -1);
                        break;
                }

                SqlParameter exc = new SqlParameter("@ExchangeId", exchangeId);
                SqlParameter ttId = new SqlParameter("@TradeTypeId", tradeTypeId);

                cmd.Parameters.Add(parId);
                cmd.Parameters.Add(exc);
                cmd.Parameters.Add(ttId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetExchangeProductsByExchange(Int32 exchangeId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetTradingProduct");
                SqlParameter exc = new SqlParameter("@ExchangeId", exchangeId);
                cmd.Parameters.Add(exc);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetExchangesSP(TradingProduct tradingProduct)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetExchanges");

                SqlParameter parId = new SqlParameter();

                switch (tradingProduct)
                {
                    case TradingProduct.Power:
                        parId = new SqlParameter("@UnderlyingId", 6);
                        break;
                    case TradingProduct.Gas:
                        parId = new SqlParameter("@UnderlyingId", 3);
                        break;
                    case TradingProduct.Cash:
                        parId = new SqlParameter("@UnderlyingId", 8);
                        break;
                    case TradingProduct.IR:
                        parId = new SqlParameter("@UnderlyingId", 23);
                        break;
                    case TradingProduct.Option:
                        parId = new SqlParameter("@UnderlyingId", 7);
                        break;
                    default:
                        parId = new SqlParameter("@UnderlyingId", -1);
                        break;
                }

                cmd.Parameters.Add(parId);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetCurveSnapshots(DateTime referenceDate)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetCurveSnapshots");
                cmd.Parameters.AddWithValue("@ReferenceDate", referenceDate);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetExchanges()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_FutureTrades_GetAllExchanges");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetDeliveryType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetDeliveryType");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetSettlementTypes()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetSettlementType");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetTradeType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetTradeType");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetOptionType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetOptionType");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetUnit()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetUnit");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetCurrency()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetCurrency");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetHolidayCalendar()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetHolidayCalendar");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DataTable GetPowerPriceBand()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPowerPriceBand");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetCurveDefinition()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetFuturesCurve");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetPricingModel()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPricingModel");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetPricingReference()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPricingReference");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetPricePublication()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPricePublication");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DataTable GetExchangeContractsForProduct(Int32 exchangeProductID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetExchangeContracts");
                cmd.Parameters.AddWithValue("@ProductID", exchangeProductID);
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static Int32 GetExchangeContractsByContractCode(string contractCode)
        {
            Int32 result = Int32.MinValue;
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetExchangeContract");
                cmd.Parameters.AddWithValue("@ContractCode", contractCode);
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            if (!IsEmpty(dt))
            {
                result = Convert.ToInt32(dt.Rows[0][0]);
            }
            return result;
        }

        public static DataTable GetExchangeContractsForProductExport(Int32 exchangeProductID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetExchangeContractsForExport");
                cmd.Parameters.AddWithValue("@ProductID", exchangeProductID);
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static Int32 GetSelfID(string username)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                SELECT PersonnelID
                                FROM Personnel P
                                WHERE Username = @Username
                            ";

                SqlParameter parId = new SqlParameter("@Username", username);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(parId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static DataTable GetPersonnelGroup(PersonnelFunction function, bool includeNull)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = string.Empty;

                if (includeNull)
                {
                    sql2 += @"SELECT -1 as PersonnelID, convert(nvarchar(255), NULL) as UserName
                              UNION ALL";
                }

                sql2 += @"
                                SELECT P.PersonnelID, P.FirstName + ' ' + P.LastName as UserName
                                FROM Personnel P
                                  join PersonnelFunctionRelation PFR on PFR.PersonnelID = P.PersonnelID
                                  join PersonnelFunction PF on PF.PersonnelFunctionID = PFR.PersonnelFunctionID
                                --WHERE   PFR.PersonnelFunctionID = @RoleId
                                and P.Deleted = 0
                                and PFR.Deleted = 0
                            ";

                Int32 functionId = Int32.MinValue;

                switch (function)
                {
                    case PersonnelFunction.Sales:
                        functionId = 1001;
                        break;
                    case PersonnelFunction.Trading:
                        functionId = 4;
                        break;
                }

                SqlParameter parId = new SqlParameter("@RoleId", functionId);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(parId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetBrokersSP(TradingProduct underlying, Int32 brokerFunction, bool addEmptyRow)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                Int32 commodity = Int32.MinValue;

                switch (underlying)
                {
                    case TradingProduct.Power:
                        commodity = 6;
                        break;
                    case TradingProduct.Gas:
                        commodity = 3;
                        break;
                    case TradingProduct.Option:
                        commodity = 7;
                        break;
                    case TradingProduct.IR:
                        commodity = 23;
                        break;
                    case TradingProduct.Cash:
                        commodity = 8;
                        break;
                }

                SqlParameter parId = new SqlParameter("@Underlying", commodity);
                SqlParameter function = new SqlParameter("@BrokerFunction", brokerFunction);
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetBrokers");

                cmd.Parameters.Add(parId);
                cmd.Parameters.Add(function);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetLE()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetLegalEntities");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetBU()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetBusinessUnits");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static string GetEnvironment()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            try
            {
                string connection = ConfigurationManager.ConnectionStrings["."].ConnectionString;
                cn = new SqlConnection(connection);
                cn.Open();


                cmd = new SqlCommand("dbo.sp_GetEnvironment", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
                cn.Close();
            }
            if (IsEmpty(dt))
            {
                return string.Empty;
            }
            else
            {
                return Convert.ToString(dt.Rows[0][0]);
            }
        }

        public static DataTable GetExecutionTypeSP()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetExecutionType");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetBrokers()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"select distinct p.ShortName, p.PartyID
                                from party p, partyfunction pf, partyfunctiontype pft
                                where p.partyid = pf.partyid
                                and pft.PartyFunctionTypeID = pf.PartyFunctionTypeID
                                and pf.PartyFunctionTypeID in (1, 3, 4)
                                order by p.ShortName";

                cmd = DataManager.GetDM().GetCommandT(sql2);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static Int32 GetPartyBrokerRateCCY(Int32 brokerID, Int32 brokerRateTypeID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                select RateCurrencyID
                                from PartyBrokerRate PBR
                                where BrokerID = @BrokerID and BrokerRateTypeID = @BrokerRateTypeID --- First Parameter
                                ";

                SqlParameter brId = new SqlParameter("@BrokerID", brokerID);
                SqlParameter brtId = new SqlParameter("@BrokerRateTypeID", brokerRateTypeID);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(brId);
                cmd.Parameters.Add(brtId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            if (IsEmpty(dt))
            { return -1; }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static DataTable GetPartyBrokerRateTypesSP(Int32 brokerID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                SqlParameter parId = new SqlParameter("@BrokerID", brokerID);
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetPartyBrokerRateTypes");

                cmd.Parameters.Add(parId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetDefaultBrokerRate(Int32 feeTypeID, Int32 exchangeId, Int32 underlyingId, Int32 settlementTypeId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetDefaultBrokerRate");

                SqlParameter pExchangeId = new SqlParameter("@ExchangeID", exchangeId);
                SqlParameter punderlyingId = new SqlParameter("@UnderlyingID", underlyingId);
                SqlParameter psettlementTypeId = new SqlParameter("@SettlementTypeId", settlementTypeId);
                SqlParameter pfeeTypeID = new SqlParameter("@FeeTypeId", feeTypeID);
                cmd.Parameters.Add(pExchangeId);
                cmd.Parameters.Add(punderlyingId);
                cmd.Parameters.Add(psettlementTypeId);
                cmd.Parameters.Add(pfeeTypeID);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetStripContracts(Int32 productId, DateTime firstDate, DateTime lastDate)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                SqlParameter pProductId = CreateInt32Parameter("@ProductID", productId);
                SqlParameter pStartDate = CreateDateTimeParameter("@FirstDate", firstDate);
                SqlParameter pEndDate = CreateDateTimeParameter("@LastDate", lastDate);
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetStripContracts");
                cmd.Parameters.Add(pProductId);
                cmd.Parameters.Add(pStartDate);
                cmd.Parameters.Add(pEndDate);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetEditableStripContracts(Int32 productId, DateTime firstDate, DateTime lastDate)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                SqlParameter pProductId = CreateInt32Parameter("@ProductID", productId);
                SqlParameter pStartDate = CreateDateTimeParameter("@FirstDate", firstDate);
                SqlParameter pEndDate = CreateDateTimeParameter("@LastDate", lastDate);
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetEditableStripContracts");
                cmd.Parameters.Add(pProductId);
                cmd.Parameters.Add(pStartDate);
                cmd.Parameters.Add(pEndDate);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetOpenPosReport(DateTime cobDate)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                SqlParameter pEndDate = CreateDateTimeParameter("@COBDate", cobDate);
                cmd = DataManager.GetDM().GetCommand("dbo.sp_GetPositionReport_BNP");
                cmd.Parameters.Add(pEndDate);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetPartyBrokerRate(Int32 brokerId, Int32 executionType, Int32 feeType, Int32 exchangeId, Int32 underlyingId, Int32 settlementTypeId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                SqlParameter pBrokerId = new SqlParameter("@BrokerID", brokerId);
                SqlParameter pExecutionType = new SqlParameter("@ExecutionTypeID", executionType);
                SqlParameter pFeeType = new SqlParameter("@FeeTypeID", feeType);
                SqlParameter pExchangeId = new SqlParameter("@ExchangeID", exchangeId);
                SqlParameter pUnderlyingId = new SqlParameter("@UnderlyingId", underlyingId);
                SqlParameter pSettlementTypeId = new SqlParameter("@SettlementTypeId", settlementTypeId);
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetBrokerRate");
                cmd.Parameters.Add(pBrokerId);
                cmd.Parameters.Add(pExecutionType);
                cmd.Parameters.Add(pFeeType);
                cmd.Parameters.Add(pExchangeId);
                cmd.Parameters.Add(pUnderlyingId);
                cmd.Parameters.Add(pSettlementTypeId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetPartyBrokerRate(Int32 partyBrokerRateID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"SELECT *
                                FROM PartyBrokerRate PBR
                                WHERE PartyBrokerRateID = @PartyBrokerRateID and PBR.Deleted = 0

                                ";

                SqlParameter parId = new SqlParameter("@PartyBrokerRateID", partyBrokerRateID);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(parId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetPartyBrokerRates(Int32 brokerId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetPartyBrokerRates");
                SqlParameter parId = new SqlParameter("@BrokerID", brokerId);
                cmd.Parameters.Add(parId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetSettlementInstructions(Int32 partyId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetPartyBrokerRates");
                SqlParameter parId = new SqlParameter("@PartyId", partyId);
                cmd.Parameters.Add(parId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetAddressType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"select PartyAddressTypeID, PartyAddressTypeName from PartyAddressType;";
                cmd = DataManager.GetDM().GetCommandT(sql2);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetContactType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"select PartyAddressTypeID, PartyAddressTypeName from PartyAddressType;";
                cmd = DataManager.GetDM().GetCommandT(sql2);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetTrueFalse()
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("TFValue", typeof(bool));
                dt.Columns.Add("TFShow", typeof(string));

                DataRow t = dt.NewRow();
                t["TFValue"] = true;
                t["TFShow"] = "True";

                dt.Rows.Add(t);

                DataRow f = dt.NewRow();
                f["TFValue"] = false;
                f["TFShow"] = "False";

                dt.Rows.Add(f);

            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
            }
            return dt;
        }

        public static DataTable GetContactTitle()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"select PartyTitleID, PartyTitle from PartyTitle;";
                cmd = DataManager.GetDM().GetCommandT(sql2);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetPartyClass()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @" select PartyClassID, Description from PartyClass;";
                cmd = DataManager.GetDM().GetCommandT(sql2);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static Int32 GetPartyAuthoriser(Int32 partyId)
        {
            Int32 authoriser = Int32.MinValue;
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @" select AuthoriserId from Party where PartyID=@PartyID;";
                SqlParameter parId = new SqlParameter("@PartyID", partyId);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(parId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            if (!ETRMDataAccess.IsEmpty(dt))
            {
                try
                {
                    authoriser = Convert.ToInt32(dt.Rows[0][PartyHI.AuthoriserId]);
                }
                catch { }
            }

            return authoriser;
        }

        public static Int32 GetPartyStatus(Int32 partyId)
        {
            Int32 status = Int32.MinValue;
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {
                string sql2 = @" select PartyStatusId from Party where PartyID=@PartyID;";
                SqlParameter parId = new SqlParameter("@PartyID", partyId);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(parId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            if (!ETRMDataAccess.IsEmpty(dt))
            {
                try
                {
                    status = Convert.ToInt32(dt.Rows[0][PartyHI.PartyStatusId]);
                }
                catch { }
            }

            return status;
        }

        public static DataTable GetPartyStatus()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPartyStatus");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DataTable GetPartyIntExt()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @" select InternalExternalID, Name from InternalExternal;";
                cmd = DataManager.GetDM().GetCommandT(sql2);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetAuthorisers()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"select FirstName + ' ' + LastName as UserName, PersonnelID from Personnel;";
                cmd = DataManager.GetDM().GetCommandT(sql2);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetBusinessUnits(Int32 parentId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetRelatedBusinessUnits");
                cmd.Parameters.AddWithValue("@ParentID", parentId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static ADODB.Recordset GetBusinessUnits()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetBusinessUnit");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return ConvertToRecordset(dt);
        }

        public static DataTable GetBusinessUnitsRelated(Int32 legalEntityId, Int32 internalExternal, Int32 partyClass)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                select PartyID, LongName 
                                from Party 
                                where Deleted = 0 and 
                                InternalExternalId = @IntExt and 
                                PartyClassId = 2 and PartyID in (select ChildID from PartyRelation where ParentID = @ParentID)
                               ";

                cmd = DataManager.GetDM().GetCommandT(sql2);

                SqlParameter parIntExt = new SqlParameter("@IntExt", internalExternal);
                cmd.Parameters.Add(parIntExt);

                SqlParameter parClass = new SqlParameter("@PartyClass", partyClass);
                cmd.Parameters.Add(parClass);

                SqlParameter parent = new SqlParameter("@ParentID", legalEntityId);
                cmd.Parameters.Add(parent);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            //catch (Exception ex)
            //{
            //    ETRMDataAccess.WriteLogEntry(ex.Message);
            //}
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetLegalEntities(Int32 internalExternal)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                Select PartyID, LongName from Party where Deleted = 0 and PartyClassId = 1 and InternalExternalId = @IntExt
                               ";

                SqlParameter parIntExt = new SqlParameter("@IntExt", internalExternal);
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.Add(parIntExt);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DoubleDecker GetLegalEntities()
        {
            DoubleDecker dd = new DoubleDecker();

            try
            {
                DataTable dt = new DataTable();

                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetLegalEntities");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);

                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dd;
        }

        public static DoubleDecker GetExchangeProducts()
        {
            DoubleDecker dd = new DoubleDecker();

            try
            {
                DataTable dt = new DataTable();

                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetExchangeProducts");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);

                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dd;
        }

        public static DoubleDecker GetExchangeContractsTemplate()
        {
            DoubleDecker dd = new DoubleDecker();

            try
            {
                DataTable dt = new DataTable();

                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetExchangeContractsTemplate");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);

                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dd;
        }

        public static DoubleDecker GetCashTrades()
        {
            DoubleDecker dd = new DoubleDecker();

            try
            {
                DataTable dt = new DataTable();

                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_CashTrades_GetList");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);

                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dd;
        }

        public static DoubleDecker GetFuturesListingDDSP(TradingProduct tradingProduct)
        {
            DoubleDecker dd = new DoubleDecker();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                SqlParameter underlying = new SqlParameter();

                switch (tradingProduct)
                {
                    case TradingProduct.Power:
                        underlying = new SqlParameter("@UnderlyingId", 6);
                        break;
                    case TradingProduct.Gas:
                        underlying = new SqlParameter("@UnderlyingId", 3);
                        break;
                    case TradingProduct.Cash:
                        underlying = new SqlParameter("@UnderlyingId", 8);
                        break;
                    case TradingProduct.IR:
                        underlying = new SqlParameter("@UnderlyingId", 23);
                        break;
                    case TradingProduct.Option:
                        underlying = new SqlParameter("@UnderlyingId", 7);
                        break;
                    default:
                        underlying = new SqlParameter("@UnderlyingId", -1);
                        break;
                }

                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetTradeListingEVP");

                cmd.Parameters.Add(underlying);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dd;
        }

        public static DataTable GetFuturesListing(TradingProduct tradingProduct)
        {
            List<Trade> tradeList = new List<Trade>();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            try
            {

                SqlParameter underlying = new SqlParameter();

                switch (tradingProduct)
                {
                    case TradingProduct.Power:
                        underlying = new SqlParameter("@UnderlyingId", 6);
                        break;
                    case TradingProduct.Gas:
                        underlying = new SqlParameter("@UnderlyingId", 3);
                        break;
                    case TradingProduct.Cash:
                        underlying = new SqlParameter("@UnderlyingId", 8);
                        break;
                    case TradingProduct.IR:
                        underlying = new SqlParameter("@UnderlyingId", 23);
                        break;
                    case TradingProduct.Option:
                        underlying = new SqlParameter("@UnderlyingId", 7);
                        break;
                    default:
                        underlying = new SqlParameter("@UnderlyingId", -1);
                        break;
                }

                cmd = DataManager.GetDM().GetCommand("dbo.sp_FutureTrades_GetTradeListingEVP");

                cmd.Parameters.Add(underlying);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        static public ADODB.Recordset ConvertToRecordset(DataTable inTable)
        {
            ADODB.Recordset result = new ADODB.Recordset();
            try
            {
                result.CursorLocation = ADODB.CursorLocationEnum.adUseClient;

                ADODB.Fields resultFields = result.Fields;
                System.Data.DataColumnCollection inColumns = inTable.Columns;

                foreach (DataColumn inColumn in inColumns)
                {
                    resultFields.Append(inColumn.ColumnName
                        , TranslateType(inColumn.DataType)
                        , inColumn.MaxLength
                        , inColumn.AllowDBNull ? ADODB.FieldAttributeEnum.adFldIsNullable :
                                                 ADODB.FieldAttributeEnum.adFldUnspecified
                        , null);
                }

                result.Open(System.Reflection.Missing.Value
                        , System.Reflection.Missing.Value
                        , ADODB.CursorTypeEnum.adOpenStatic
                        , ADODB.LockTypeEnum.adLockOptimistic, 0);

                foreach (DataRow dr in inTable.Rows)
                {
                    result.AddNew(System.Reflection.Missing.Value, System.Reflection.Missing.Value);

                    for (int columnIndex = 0; columnIndex < inColumns.Count; columnIndex++)
                    {
                        var type = dr[columnIndex].GetType();
                        resultFields[columnIndex].Value = dr[columnIndex];
                    }
                }
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }

            return result;
        }

        static ADODB.DataTypeEnum TranslateType(Type columnType)
        {
            switch (columnType.UnderlyingSystemType.ToString())
            {
                case "System.Boolean":
                    return ADODB.DataTypeEnum.adBoolean;

                case "System.Byte":
                    return ADODB.DataTypeEnum.adUnsignedTinyInt;

                case "System.Char":
                    return ADODB.DataTypeEnum.adChar;

                case "System.DateTime":
                    return ADODB.DataTypeEnum.adDate;

                case "System.Decimal":
                    return ADODB.DataTypeEnum.adCurrency;

                case "System.Double":
                    return ADODB.DataTypeEnum.adDouble;

                case "System.Int16":
                    return ADODB.DataTypeEnum.adSmallInt;

                case "System.Int32":
                    return ADODB.DataTypeEnum.adInteger;

                case "System.Int64":
                    return ADODB.DataTypeEnum.adBigInt;

                case "System.SByte":
                    return ADODB.DataTypeEnum.adTinyInt;

                case "System.Single":
                    return ADODB.DataTypeEnum.adSingle;

                case "System.UInt16":
                    return ADODB.DataTypeEnum.adUnsignedSmallInt;

                case "System.UInt32":
                    return ADODB.DataTypeEnum.adUnsignedInt;

                case "System.UInt64":
                    return ADODB.DataTypeEnum.adUnsignedBigInt;

                case "System.DateTimeOffset":
                    return ADODB.DataTypeEnum.adDBTimeStamp;

                case "System.String":
                default:
                    return ADODB.DataTypeEnum.adVarChar;
            }
        }

        public static Int32 GetNumberOfHolidays(DateTime dtStartingWith, DateTime dtEndingWith, Int32 calendarId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                select count(*)
                                from Holiday 
                                where CalendarDate >= @dtStartingWith 
                                and CalendarDate <= @dtEndingWith
                                and HolidayCalendarId = @calendarId
                                ";
                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.AddWithValue("@dtStartingWith", dtStartingWith);
                cmd.Parameters.AddWithValue("@dtEndingWith", dtEndingWith);
                cmd.Parameters.AddWithValue("@calendarId", calendarId);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            if (IsEmpty(dt))
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static Int32 GetHolidayCalendar(Int32 currencyId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                string sql2 = @"
                                select HolidayCalendarId
                                from Currency 
                                where CurrencyID = @CurrencyID
                                ";

                cmd = DataManager.GetDM().GetCommandT(sql2);
                cmd.Parameters.AddWithValue("@CurrencyID", currencyId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            if (IsEmpty(dt) || string.IsNullOrEmpty(Convert.ToString(dt.Rows[0][0])))
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static DataTable AddBitColumn(DataTable source, string nameOfBoolean)
        {
            source.Columns.Add(new DataColumn(nameOfBoolean, typeof(bool)));
            return source;
        }

        public static string GetStringDTRElement(DataTable source, string columnName)
        {
            if (IsEmpty(source))
            {
                return string.Empty;
            }
            else
            {
                return Convert.ToString(source.Rows[0][columnName]);
            }
        }

        public static DateTime? GetDateTimeDTRElement(DataTable source, string columnName)
        {
            DateTime? result = null;

            if (IsEmpty(source))
            {
                return null;
            }
            else
            {
                try
                {
                    result = Convert.ToDateTime(source.Rows[0][columnName]);
                }
                catch
                { return null; }
            }
            return result;
        }

        public static Int32? GetInt32DTRElement(DataTable source, string columnName)
        {
            if (IsEmpty(source))
            {
                return Int32.MinValue;
            }
            else
            {
                Int32 result = 0;

                try
                {
                    result = Convert.ToInt32(source.Rows[0][columnName]);
                }
                catch
                { return null; }
                return result;
            }
        }

        public static DateTime? GetDateTimeDTRElement(DataRow source, string columnName)
        {
            DateTime? result = null;

            try
            {
                result = Convert.ToDateTime(source[columnName]);
            }
            catch
            { return null; }
            return result;
        }

        public static string GetStringDTRElement(DataRow source, string columnName)
        {
            string result = string.Empty;

            try
            {
                return Convert.ToString(source[columnName]);
            }
            catch { }

            return result;
        }

        public static bool? GetBoolDTRElement(DataRow source, string columnName)
        {
            string result = string.Empty;

            try
            {
                return Convert.ToBoolean(source[columnName]);
            }
            catch { }

            return null;
        }

        public static bool? GetBoolDTRElement(DataTable source, string columnName)
        {
            try
            {
                return Convert.ToBoolean(source.Rows[0][columnName]);
            }
            catch { }

            return null;
        }

        public static Int32? GetInt32DTRElement(DataRow source, string columnName)
        {
            Int32 result = 0;

            try
            {
                result = Convert.ToInt32(source[columnName]);
            }
            catch
            { return null; }
            return result;
        }

        public static double? GetDoubleDTRElement(DataTable source, string columnName)
        {
            if (IsEmpty(source))
            {
                return 0;
            }
            else
            {
                double result = 0;

                try
                {
                    result = Convert.ToDouble(source.Rows[0][columnName]);
                }
                catch
                { return null; }
                return result;
            }
        }

        public static double? GetDoubleDTRElement(DataRow source, string columnName)
        {
            double result = 0;
            try
            {
                result = Convert.ToDouble(source[columnName]);
            }
            catch
            { return null; }
            return result;
        }

        public static decimal? GetDecimalDTRElement(DataTable source, string columnName)
        {
            if (IsEmpty(source))
            {
                return 0;
            }
            else
            {
                decimal result = 0;

                try
                {
                    result = Convert.ToDecimal(source.Rows[0][columnName]);
                }
                catch
                { return null; }
                return result;
            }
        }

        public static decimal? GetDecimalDTRElement(DataRow source, string columnName)
        {
            decimal result = 0;

            try
            {
                result = Convert.ToDecimal(source[columnName]);
            }
            catch
            { return null; }
            return result;
        }

        public static decimal RetrieveDecimalValue(string text)
        {
            decimal clearingBroerageAmount = 0;

            try
            {
                clearingBroerageAmount = decimal.Parse(text, CultureInfo.InvariantCulture);
            }
            catch { }

            return clearingBroerageAmount;
        }

        public static Microsoft.Office.Interop.Excel.Worksheet GetWorksheetByName(Microsoft.Office.Interop.Excel.Workbook workbook, string name)
        {
            return workbook.Worksheets.OfType<Microsoft.Office.Interop.Excel.Worksheet>().FirstOrDefault(ws => ws.Name == name);
        }

        public static void PasteRecordSetOnWorksheet(ADODB.Recordset pdRS, Microsoft.Office.Interop.Excel.Worksheet destination)
        {
            pdRS.MoveFirst();
            Int32 rows = pdRS.RecordCount;
            Int32 cols = pdRS.Fields.Count;
            destination = CreateHeader(pdRS, destination);
            Microsoft.Office.Interop.Excel.Range topLeft = GetTopLeft(destination, rows, cols);
            topLeft.CopyFromRecordset(pdRS);
            topLeft.EntireColumn.AutoFit();
        }
        public static void FormatReport(DataTable shadow, Microsoft.Office.Interop.Excel.Worksheet destination)
        {
            //////Microsoft.Office.Interop.Excel.Range topLeft = GetTopLeft(destination, rows, cols);
            //////topLeft.CopyFromRecordset(pdRS);
            //////topLeft.EntireColumn.AutoFit();
        }

        public static void FormatColumns(DataTable dt, Microsoft.Office.Interop.Excel.Worksheet destination)
        {
            if (!ETRMDataAccess.IsEmpty(dt))
            {
                for (Int32 i = 1; i < dt.Columns.Count + 1; i++)
                {
                    Microsoft.Office.Interop.Excel.Range topCell = destination.Cells[1, i];
                    topCell.EntireColumn.NumberFormat = AssignFormatType(dt.Columns[i - 1].DataType.Name);
                }
            }
        }


        private static string AssignFormatType(string columnTypeName)
        {
            string formatSpec = "General";
            //string formatSpec = "@";

            switch (columnTypeName)
            {
                case CommonFormats.DateTime:
                    formatSpec = "dd MMM yyyy";
                    break;
                case CommonFormats.Decimal:
                    formatSpec = "#,###.0000";
                    break;
                case CommonFormats.Int32:
                    //formatSpec = "#,###";
                    formatSpec = "#";
                    break;
                case CommonFormats.String:
                    break;
            }

            return formatSpec;
        }

        private static Microsoft.Office.Interop.Excel.Worksheet CreateHeader(ADODB.Recordset pdRS, Microsoft.Office.Interop.Excel.Worksheet destination)
        {
            Microsoft.Office.Interop.Excel.Worksheet sheet = destination;
            Microsoft.Office.Interop.Excel.Range tl = sheet.Cells[1, 1];

            for (Int32 i = 0; i < pdRS.Fields.Count; i++)
            {
                Microsoft.Office.Interop.Excel.Range header = sheet.Cells[1, i + 1];
                header.Value2 = pdRS.Fields[i].Name;
            }
            return sheet;
        }

        private static Microsoft.Office.Interop.Excel.Range GetTopLeft(Microsoft.Office.Interop.Excel.Worksheet sheet, Int32 rows, Int32 cols)
        {
            Microsoft.Office.Interop.Excel.Range tl = sheet.Cells[2, 1];
            Microsoft.Office.Interop.Excel.Range br = sheet.Cells[rows + 1, cols];
            Microsoft.Office.Interop.Excel.Range topLeftExtended = sheet.get_Range(tl, br);
            return topLeftExtended;
        }

        public static DataTable ExcelToTable(string filePath, string source)
        {
            DataTable dt = new DataTable();
            try
            {
                string rootConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0 XML;HDR=YES;';";
                string pathconn = string.Format(rootConn, filePath);
                OleDbConnection conn = new OleDbConnection(pathconn);
                OleDbDataAdapter MyDataAdapter = new OleDbDataAdapter(string.Format("Select * from [{0}$]", source), conn);
                MyDataAdapter.Fill(dt);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DataTable GetDataTableFromCsv(string path, bool isFirstRowHeader)
        {
            string header = isFirstRowHeader ? "Yes" : "No";

            string pathOnly = Path.GetDirectoryName(path);
            string fileName = Path.GetFileName(path);

            string sql = @"SELECT * FROM [" + fileName + "]";

            using (OleDbConnection connection = new OleDbConnection(
                      @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + pathOnly +
                      ";Extended Properties=\"Text;HDR=" + header + "\""))
            using (OleDbCommand command = new OleDbCommand(sql, connection))
            using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
            {
                DataTable dataTable = new DataTable();
                dataTable.Locale = CultureInfo.CurrentCulture;
                adapter.Fill(dataTable);
                return dataTable;
            }
        }

        public static DataTable PreprocessBNPOpenPos(DataTable original)
        {
            DataTable result = new DataTable();

            result.Columns.Add("COB", typeof(DateTime));
            result.Columns.Add("EXCHANGE", typeof(string));
            result.Columns.Add("PARTY", typeof(string));
            result.Columns.Add("ACCOUNT", typeof(string));
            result.Columns.Add("PRODUCT_TYPE", typeof(string));
            result.Columns.Add("PRODUCT", typeof(string));
            result.Columns.Add("EXCHANGE_CODE", typeof(string));
            result.Columns.Add("BUY_SELL", typeof(string));
            result.Columns.Add("CALL_PUT", typeof(string));
            result.Columns.Add("CALL_PUT", typeof(double));
            result.Columns.Add("MAT_MONTH", typeof(string));
            result.Columns.Add("MAT_YEAR", typeof(int));
            result.Columns.Add("MAT_DATE", typeof(DateTime));
            result.Columns.Add("QTY", typeof(double));
            result.Columns.Add("TRADE_PRICE", typeof(double));
            result.Columns.Add("SETT_PRICE", typeof(double));
            result.Columns.Add("OTE", typeof(double));
            result.Columns.Add("VM", typeof(double));
            result.Columns.Add("CONVERTED_PNL", typeof(double));
            result.Columns.Add("NOV", typeof(double));

            foreach (DataRow row in original.Rows)
            {
                DataRow rRow = result.NewRow();
            }

            return result;
        }

        public static DataTable CreateBNPExpiredTradesDataTable()
        {
            DataTable result = new DataTable();

            result.Columns.Add("ImportID", typeof(Int32));
            result.Columns.Add("COB", typeof(DateTime));
            result.Columns.Add("EXCHANGE", typeof(string));
            result.Columns.Add("INTERMEDIARY", typeof(string));
            result.Columns.Add("PARTY", typeof(string));
            result.Columns.Add("ACCOUNT", typeof(string));
            result.Columns.Add("PRODUCT_TYPE", typeof(string));
            result.Columns.Add("EXCHANGE_CODE", typeof(string));
            result.Columns.Add("PRODUCT", typeof(string));
            result.Columns.Add("PRODUCT_DESCRIPTION", typeof(string));
            result.Columns.Add("PHISYCAL_SETTLEMENT", typeof(string));
            result.Columns.Add("BUY_SELL", typeof(string));
            result.Columns.Add("CALL_PUT", typeof(string));
            result.Columns.Add("STRIKE", typeof(double));
            result.Columns.Add("MAT_MONTH", typeof(string));
            result.Columns.Add("MAT_YEAR", typeof(string));
            result.Columns.Add("MAT_DATE", typeof(DateTime));
            result.Columns.Add("FIRST_NOTICE_DATE", typeof(DateTime));
            result.Columns.Add("LAST_NOTICE_DATE", typeof(DateTime));
            result.Columns.Add("DELIVERY_DATE", typeof(DateTime));
            result.Columns.Add("QTY", typeof(Int32));

            return result;
        }

        public static DataTable CreateBNPDetailedCFITradesDataTable()
        {
            DataTable result = new DataTable();

            result.Columns.Add("ImportID", typeof(Int32));
            result.Columns.Add("COB", typeof(DateTime));
            result.Columns.Add("EXCHANGE", typeof(string));
            result.Columns.Add("ORIGIN", typeof(string));
            result.Columns.Add("OP_DATE", typeof(DateTime));
            result.Columns.Add("TRADE_TYPE", typeof(string));
            result.Columns.Add("OPERATION", typeof(string));
            result.Columns.Add("CLEARER", typeof(string));
            result.Columns.Add("INTERMEDIARY", typeof(string));
            result.Columns.Add("PARTY", typeof(string));
            result.Columns.Add("ACCOUNT", typeof(string));
            result.Columns.Add("BROKER", typeof(string));
            result.Columns.Add("EXECUTING_BROKER", typeof(string));
            result.Columns.Add("TRADER", typeof(string));
            result.Columns.Add("PRODUCT_TYPE", typeof(string));
            result.Columns.Add("PRODUCT", typeof(string));
            result.Columns.Add("EXCHANGE_CODE", typeof(string));
            result.Columns.Add("BUY_SELL", typeof(string));
            result.Columns.Add("CALL_PUT", typeof(string));
            result.Columns.Add("STRIKE", typeof(double));
            result.Columns.Add("MAT_MONTH", typeof(string));
            result.Columns.Add("MAT_YEAR", typeof(string));
            result.Columns.Add("MAT_DATE", typeof(DateTime));
            result.Columns.Add("INTERNAL_TRADE_ID", typeof(Int32));
            result.Columns.Add("INTERNAL_ALLOCATION_ID", typeof(Int32));
            result.Columns.Add("EXCHANGE_ID", typeof(Int32));
            result.Columns.Add("CLEARING_ID", typeof(Int32));
            result.Columns.Add("TRANSACTION_TYPE", typeof(string));
            result.Columns.Add("COMMISSION_TYPE", typeof(string));
            result.Columns.Add("VENUE_SESSION", typeof(string));
            result.Columns.Add("GLOBEX_NUMBER", typeof(string));
            result.Columns.Add("QTY", typeof(Int32));
            result.Columns.Add("TRADE_PRICE", typeof(double));
            result.Columns.Add("EXEC_FEES", typeof(double));
            result.Columns.Add("CLEARING_FEES", typeof(double));
            result.Columns.Add("NFA_FEES", typeof(double));
            result.Columns.Add("CLT_MKT_REGIS_FEES", typeof(double));
            result.Columns.Add("CLRG_HOUSE_FEES", typeof(double));
            result.Columns.Add("TOTAL_FEES", typeof(double));
            result.Columns.Add("CURRENCY", typeof(string));
            result.Columns.Add("TRADE_DESCRIPTION", typeof(string));
            result.Columns.Add("TRANSACTION_DESCRIPTION", typeof(string));
            result.Columns.Add("UOM", typeof(string));
            result.Columns.Add("CONTRACT_SIZE", typeof(double));
            result.Columns.Add("VOLUME", typeof(double));
            result.Columns.Add("NOTIONAL_VALUE", typeof(double));
            result.Columns.Add("VENUE_SESSION_DESCRIPTION", typeof(string));
            result.Columns.Add("EXCHANGE_PRODUCT_DESCRIPTION", typeof(string));
            result.Columns.Add("PREMIUM", typeof(double));

            return result;
        }

        public static DataTable CreateBNPCancelledTradesDataTable()
        {
            DataTable result = new DataTable();

            result.Columns.Add("ImportID", typeof(Int32));
            result.Columns.Add("COB", typeof(DateTime));
            result.Columns.Add("EXCHANGE", typeof(string));
            result.Columns.Add("ORIGIN", typeof(string));
            result.Columns.Add("OP_DATE", typeof(DateTime));
            result.Columns.Add("TRADE_TYPE", typeof(string));
            result.Columns.Add("OPERATION", typeof(string));
            result.Columns.Add("CLEARER", typeof(string));
            result.Columns.Add("INTERMEDIARY", typeof(string));
            result.Columns.Add("PARTY", typeof(string));
            result.Columns.Add("ACCOUNT", typeof(string));
            result.Columns.Add("BROKER", typeof(string));
            result.Columns.Add("EXECUTING_BROKER", typeof(string));
            result.Columns.Add("TRADER", typeof(string));
            result.Columns.Add("PRODUCT_TYPE", typeof(string));
            result.Columns.Add("PRODUCT", typeof(string));
            result.Columns.Add("EXCHANGE_CODE", typeof(string));
            result.Columns.Add("BUY_SELL", typeof(string));
            result.Columns.Add("CALL_PUT", typeof(string));
            result.Columns.Add("STRIKE", typeof(double));
            result.Columns.Add("MAT_MONTH", typeof(string));
            result.Columns.Add("MAT_YEAR", typeof(string));
            result.Columns.Add("MAT_DATE", typeof(DateTime));
            result.Columns.Add("INTERNAL_TRADE_ID", typeof(Int32));
            result.Columns.Add("INTERNAL_ALLOCATION_ID", typeof(Int32));
            result.Columns.Add("EXCHANGE_ID", typeof(Int32));
            result.Columns.Add("CLEARING_ID", typeof(Int32));
            result.Columns.Add("TRANSACTION_TYPE", typeof(string));
            result.Columns.Add("COMMISSION_TYPE", typeof(string));
            result.Columns.Add("GLOBEX_SESSION", typeof(string));
            result.Columns.Add("GLOBEX_NUMBER", typeof(string));
            result.Columns.Add("QTY", typeof(Int32));
            result.Columns.Add("TRADE_PRICE", typeof(double));
            result.Columns.Add("EXEC_FEES", typeof(double));
            result.Columns.Add("CLEARING_FEES", typeof(double));
            result.Columns.Add("NFA_FEES", typeof(double));
            result.Columns.Add("CLT_MKT_REGIS_FEES", typeof(double));
            result.Columns.Add("CLRG_HOUSE_FEES", typeof(double));
            result.Columns.Add("CURRENCY", typeof(string));
            result.Columns.Add("TRADE_DESCRIPTION", typeof(string));
            result.Columns.Add("TRANSACTION_DESCRIPTION", typeof(string));
            return result;
        }

        public static DataTable CreateBNPOpenPosDetailsDataTable()
        {
            DataTable result = new DataTable();

            result.Columns.Add("ImportID", typeof(Int32));
            result.Columns.Add("COB", typeof(DateTime));
            result.Columns.Add("EXCHANGE", typeof(string));
            result.Columns.Add("CLEARER", typeof(string));
            result.Columns.Add("INTERMEDIARY", typeof(string));
            result.Columns.Add("PARTY", typeof(string));
            result.Columns.Add("ACCOUNT", typeof(string));
            result.Columns.Add("PRODUCT_TYPE", typeof(string));
            result.Columns.Add("PRODUCT", typeof(string));
            result.Columns.Add("PRODUCT_DESCRIPTION", typeof(string));
            result.Columns.Add("EXCHANGE_CODE", typeof(string));
            result.Columns.Add("BUY_SELL", typeof(string));
            result.Columns.Add("CALL_PUT", typeof(string));
            result.Columns.Add("STRIKE", typeof(double));
            result.Columns.Add("MAT_MONTH", typeof(string));
            result.Columns.Add("MAT_YEAR", typeof(string));
            result.Columns.Add("MAT_DATE", typeof(DateTime));
            result.Columns.Add("QTY", typeof(Int32));
            result.Columns.Add("TRADE_PRICE", typeof(double));
            result.Columns.Add("SETT_PRICE", typeof(double));
            result.Columns.Add("OTE", typeof(double));
            result.Columns.Add("VM", typeof(double));
            result.Columns.Add("ORIGINAL_CURRENCY", typeof(string));
            result.Columns.Add("DISCOUNT_FACTOR", typeof(double));
            result.Columns.Add("FX_RATE", typeof(double));
            result.Columns.Add("CONVERTED_PNL", typeof(double));
            result.Columns.Add("CONVERSION_CURRENCY", typeof(string));
            result.Columns.Add("PREMIUM", typeof(double));
            result.Columns.Add("ORIGINAL_CURRENCY2", typeof(string));
            result.Columns.Add("FX_RATE2", typeof(double));
            result.Columns.Add("CONVERTED_PREMIUM", typeof(double));
            result.Columns.Add("CONVERSION_CURRENCY2", typeof(string));
            result.Columns.Add("NOV", typeof(double));
            result.Columns.Add("ORIGINAL_CURRENCY3", typeof(string));
            result.Columns.Add("FX_RATE3", typeof(double));
            result.Columns.Add("CONVERTED_NOV", typeof(double));
            result.Columns.Add("CONVERSION_CURRENCY3", typeof(string));
            result.Columns.Add("INTERNAL_TRADE_ID", typeof(Int32));
            result.Columns.Add("CLEARING_ID", typeof(Int32));
            result.Columns.Add("EXCHANGE_ID", typeof(Int32));
            result.Columns.Add("INTERNAL_ALLOCATION_ID", typeof(Int32));
            result.Columns.Add("UOM", typeof(string));
            result.Columns.Add("CONTRACT_SIZE", typeof(double));
            result.Columns.Add("VOLUME", typeof(double));
            result.Columns.Add("NOTIONAL_VALUE", typeof(double));
            result.Columns.Add("SPAN_COMBINED_COMMODITY", typeof(string));
            result.Columns.Add("EXCHANGE_PRODUCT_DESCRIPTION", typeof(string));
            result.Columns.Add("TRANSACTION_TYPE", typeof(string));
            result.Columns.Add("TRANSACTION_DESCRIPTION", typeof(string));
            result.Columns.Add("OPERATION_DATE", typeof(DateTime));
            result.Columns.Add("TRADER", typeof(string));

            return result;
        }

        public static DataTable CreateBNPOpenPosDataTable()
        {
            DataTable result = new DataTable();

            result.Columns.Add("ImportID", typeof(Int32));
            result.Columns.Add("COB", typeof(DateTime));
            result.Columns.Add("EXCHANGE", typeof(string));
            result.Columns.Add("CLEARER", typeof(string));
            result.Columns.Add("INTERMEDIARY", typeof(string));
            result.Columns.Add("PARTY", typeof(string));
            result.Columns.Add("ACCOUNT", typeof(string));
            result.Columns.Add("PRODUCT_TYPE", typeof(string));
            result.Columns.Add("PRODUCT", typeof(string));
            result.Columns.Add("PRODUCT_DESCRIPTION", typeof(string));
            result.Columns.Add("EXCHANGE_CODE", typeof(string));
            result.Columns.Add("BUY_SELL", typeof(string));
            result.Columns.Add("CALL_PUT", typeof(string));
            result.Columns.Add("STRIKE", typeof(double));
            result.Columns.Add("MAT_MONTH", typeof(string));
            result.Columns.Add("MAT_YEAR", typeof(string));
            result.Columns.Add("MAT_DATE", typeof(DateTime));
            result.Columns.Add("QTY", typeof(Int32));
            result.Columns.Add("TRADE_PRICE", typeof(double));
            result.Columns.Add("SETT_PRICE", typeof(double));
            result.Columns.Add("OTE", typeof(double));
            result.Columns.Add("VM", typeof(double));
            result.Columns.Add("ORIGINAL_CURRENCY", typeof(string));
            result.Columns.Add("DISCOUNT_FACTOR", typeof(double));
            result.Columns.Add("FX_RATE", typeof(double));
            result.Columns.Add("CONVERTED_PNL", typeof(double));
            result.Columns.Add("CONVERSION_CURRENCY", typeof(string));
            result.Columns.Add("PREMIUM", typeof(double));
            result.Columns.Add("ORIGINAL_CURRENCY2", typeof(string));
            result.Columns.Add("FX_RATE2", typeof(double));
            result.Columns.Add("CONVERTED_PREMIUM", typeof(double));
            result.Columns.Add("CONVERSION_CURRENCY2", typeof(string));
            result.Columns.Add("NOV", typeof(double));
            result.Columns.Add("ORIGINAL_CURRENCY3", typeof(string));
            result.Columns.Add("FX_RATE3", typeof(double));
            result.Columns.Add("CONVERTED_NOV", typeof(double));
            result.Columns.Add("CONVERSION_CURRENCY3", typeof(string));
            result.Columns.Add("SPAN_COMBINED_COMMODITY", typeof(string));
            result.Columns.Add("EXCHANGE_PRODUCT_DESCRIPTION", typeof(string));

            return result;
        }

        public static bool UploadDataTableToTable(DataTable source, string destination)
        {
            try
            {
                DataManager.GetDM().BulkInsertion(source, destination);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
                ETRMDataAccess.WriteLogEntry(ex.StackTrace);
                return false;
            }
            return true;
        }

        public static DataTable GetBSCOpenPosReport(DateTime cobDate)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GetPositionReport_BNP");
            cmd.Parameters.AddWithValue("@COBDate", cobDate);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }
        public static void GetBSCOpenPosReport(DateTime cobDate, Int32 importID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetOpenPosBSCReport");
            cmd.Parameters.AddWithValue("@COBDate", cobDate);
            cmd.Parameters.AddWithValue("@ImportID", importID);
            DataManager.GetDM().ExecuteNonQuery(cmd);
        }

        //[sp_ExRec_GetOpenPosDetailsBSCReport]

        public static void GetBSCOpenPosDetailsReport(DateTime cobDate, Int32 importID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetOpenPosDetailsBSCReport");
            cmd.Parameters.AddWithValue("@COBDate", cobDate);
            cmd.Parameters.AddWithValue("@ImportID", importID);
            DataManager.GetDM().ExecuteNonQuery(cmd);
        }

        public static DataTable GetBSCOpenPosDetailsReport(DateTime cobDate)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GetPositionReport_BNP_Detailed");
            cmd.Parameters.AddWithValue("@COBDate", cobDate);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static void GetBSCCFITradesReport(DateTime cobDate, Int32 importID)
        {
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetCFIReportDump");
            cmd.Parameters.AddWithValue("@ImportID", importID);
            cmd.Parameters.AddWithValue("@AsOf", cobDate.Date);
            DataManager.GetDM().ExecuteNonQuery(cmd);
        }

        public static void GetBSCExpiringTradesReport(DateTime cobDate, Int32 importID)
        {
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_ExRec_GetExpiryingDealsReportDump");
            cmd.Parameters.AddWithValue("@ImportID", importID);
            cmd.Parameters.AddWithValue("@AsOf", cobDate.Date);
            DataManager.GetDM().ExecuteNonQuery(cmd);
        }

        public static DataTable GetBSCCFITradesReport(DateTime cobDate)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GetTradesReport");
            cmd.Parameters.AddWithValue("@TradeDateFrom", cobDate);
            cmd.Parameters.AddWithValue("@TradeDateThrough", cobDate);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetBSCCancelledTradesReport(DateTime cobDate)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GetCancelledTradesReport");
            cmd.Parameters.AddWithValue("@CancellationDateFrom", cobDate);
            cmd.Parameters.AddWithValue("@CancellationDateThrough", cobDate);
            cmd.Parameters.AddWithValue("@RetrieveAll", false);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetBroxitReport()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_Broxit_GetBroxitReport");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static string GetBroxitXML(Int32 tradeID, string reportingParty)
        {
            SqlCommand cmd = DataManager.GetDM().GetCommand("ACP_EConfirmRequest_Smart");
            cmd.Parameters.AddWithValue("@UserEvent_ReferenceTradeID", tradeID);
            cmd.Parameters.AddWithValue("@ReportingParty", reportingParty);

            using (XmlReader reader = cmd.ExecuteXmlReader())
            {
                string s = string.Empty;
                while (reader.Read())
                {
                    s = reader.ReadOuterXml();
                }
                return s;
            }
        }

        public static string GetBroxitXML(Int32 tradeID, string reportingParty, Int32 userEventID)
        {
            SqlCommand cmd = DataManager.GetDM().GetCommand("ACP_EConfirmRequest_Smart");
            cmd.Parameters.AddWithValue("@UserEvent_ReferenceTradeID", tradeID);
            cmd.Parameters.AddWithValue("@ReportingParty", reportingParty);
            cmd.Parameters.AddWithValue("@UserEvent_ID", userEventID);

            using (XmlReader reader = cmd.ExecuteXmlReader())
            {
                string s = string.Empty;
                while (reader.Read())
                {
                    s = reader.ReadOuterXml();
                }
                return s;
            }
        }

        public static Int32 FlagConfirmTaskCompleted(Int32 userEventID)
        {
            Int32 result = userEventID;

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_CL_FlagConfirmTaskAsCompleted");
                cmd.Parameters.AddWithValue("@UserEvent_RecordID", userEventID);

                if (!DataManager.GetDM().ExecuteNonQuery(cmd))
                {
                    result = Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                result = Int32.MinValue;
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
            }
            return result;
        }

        public static DataTable GetBSCExpiredTradesReport(DateTime cobDate)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GetExpiryReport");
            cmd.Parameters.AddWithValue("@COBDate", cobDate);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DateTime AddBusinessDays(DateTime date, int days)
        {
            if (days < 0)
            {
                throw new ArgumentException("days cannot be negative", "days");
            }

            if (days == 0) return date;

            if (date.DayOfWeek == DayOfWeek.Saturday)
            {
                date = date.AddDays(2);
                days -= 1;
            }
            else if (date.DayOfWeek == DayOfWeek.Sunday)
            {
                date = date.AddDays(1);
                days -= 1;
            }

            date = date.AddDays(days / 5 * 7);
            int extraDays = days % 5;

            if ((int)date.DayOfWeek + extraDays > 5)
            {
                extraDays += 2;
            }

            return date.AddDays(extraDays);
        }

        #region PowerPHysical
        public static DataTable GetParties(Int32? leBu, Int32? intExt)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetParties");

            if (leBu.HasValue)
            {
                cmd.Parameters.AddWithValue("@lebu", leBu.Value);
            }

            if (intExt.HasValue)
            {
                cmd.Parameters.AddWithValue("@intExt", intExt.Value);
            }

            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetPartiesNullable(Int32? leBu, Int32? intExt)
        {
            DataTable dt = ETRMDataAccess.GetParties(leBu, intExt);
            DataRow row = dt.NewRow();
            row["PartyID"] = -1;
            row["ShortName"] = "";
            dt.Rows.Add(row);
            return dt;
        }

        public static DataTable GetPartiesWithID(Int32? leBu, Int32? intExt, Int32 partyID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPartiesWithID");

            if (leBu.HasValue)
            {
                cmd.Parameters.AddWithValue("@lebu", leBu.Value);
            }

            if (intExt.HasValue)
            {
                cmd.Parameters.AddWithValue("@intExt", intExt.Value);
            }
            cmd.Parameters.AddWithValue("@PartyID", partyID);

            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetTradeableParties(Int32? leBu, Int32? intExt)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetNonBrokers");

            if (leBu.HasValue)
            {
                cmd.Parameters.AddWithValue("@lebu", leBu.Value);
            }

            if (intExt.HasValue)
            {
                cmd.Parameters.AddWithValue("@intExt", intExt.Value);
            }

            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetTradeablePartiesNullable(Int32? leBu, Int32? intExt)
        {
            DataTable dt = ETRMDataAccess.GetTradeableParties(leBu, intExt);
            DataRow row = dt.NewRow();
            row["PartyID"] = -1;
            row["ShortName"] = "";
            row["LongName"] = "";
            row["PartyClassId"] = 0;
            row["InternalExternalId"] = 0;
            row["PartyStatusId"] = 0;
            dt.Rows.Add(row);
            return dt;
        }


        public static DataTable GetPhysicalPartyBrokerRateTypes(Int32? brokerId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPartyBrokerRateTypes");

            if (brokerId.HasValue)
            {
                cmd.Parameters.AddWithValue("@BrokerID", brokerId.Value);
            }

            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetAssociatedBU(Int32 le)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetAssociatedBusinessUnits");
            cmd.Parameters.AddWithValue("@le", le);

            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetAssociatedAgreements(Int32 internalLE, Int32 externalLE)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetAssociatedAgreements");
            cmd.Parameters.AddWithValue("@InternalLE", internalLE);
            cmd.Parameters.AddWithValue("@ExternalLE", externalLE);

            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }
        public static DataTable GetBuySell()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetBuySell");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetCurveType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetCurveType");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }
        public static DataTable GetSpecifiedPriceType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetSpecifiedPrice");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }
        public static DataTable GetPricePub()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPricePub");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }
        public static DataTable GetYieldBasis()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetYieldBasis");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }
        //
        public static DataTable GetInterpolationMethod()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetInterpolationMethod");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetAgreementStatus()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetAgreementStatus");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }
        public static DataTable GetAgreementCalculationYear()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetAgreementVersion");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }
        public static DataTable GetAgreementCalculationAgent()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetAgreementCalculationAgent");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetAgreementType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetAgreementType");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetUnits()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetUnits");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetCollateralizationType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetCollateralizationType");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetSwapPurpose()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetSwapPurpose");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetPowerPricingUnits()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPowerPricingUnits");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }


        public static DataTable GetPowerVolumeType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetVolumeType");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetGasVolumeType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetGasVolumeType");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetPricingType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPricingType");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetSwapType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetSwapTypes");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetPaymentConvention()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_PaymentConvention");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetPaymentCalendar()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPaymentCalendar");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetAllPaymentCalendars()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_BeaStra_GetBeaStraPricingCalendar");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetPowerPricingMarketType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_PowerPricingMarketType");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetPowerOTCCalendar()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetOTCDayType");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetOTCPaymentEvent()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetOTCPaymentEvent");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetServiceLevel()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetServiceLevel");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetSettlementFrequency()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetOTCSettlementFrequency");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetPowerRegions()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPowerRegion");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetPowerZones()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPowerZones");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetPowerPricingReference()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPowerPricingReference");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetSwapPricingReference(Int32 commodityId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPricingReference");
            cmd.Parameters.AddWithValue("@CommodityID", commodityId);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetAgreementDeliveryType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetAgreementDeliveryType");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetAgreementSettlementType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("[sp_StaticData_GetAgreementSettlementType]");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetAgreementCurrency()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetCurrency");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetDoddFrankType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPartyDoddFrankType");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetDoddFrankParty()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPartyDoddFrankReportingParty");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetTaxDomicile()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetTaxDomicile");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetTaxExemptStatus()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetTaxExemptStatus");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetCountry()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetCountry");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetConfirmingParty()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetConfirmingParty");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetConfirmingMethod()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetConfirmMethod");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static Int32 GetNextOTCPowerPhysicalSequence()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetNextOTCTradeHeaderSequence");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        public static Int32 GetNextOTCTradePricingInfoSequence()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetNextOTCTradePricingInfoSequence");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }


        public static DataTable GetPowerLocations(Int32 powerZoneId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPowerLocations");
            cmd.Parameters.AddWithValue("@PowerZoneId", powerZoneId);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetPowerPhysicalHeader(Int32 tradeId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPowerPhysHeader");
            cmd.Parameters.AddWithValue("@TradeId", tradeId);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetPowerPhysicalPricingInfo(Int32 tradeId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPowerPhysPricingInfo");
            cmd.Parameters.AddWithValue("@TradeId", tradeId);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetPowerPhysicalHours(DateTime firstDay, DateTime lastDay)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetHours");
            cmd.Parameters.AddWithValue("@FirstDay", firstDay.Date);
            cmd.Parameters.AddWithValue("@LastDay", lastDay.Date);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetPowerProductsAccordingToRegion(Int32 powerZoneId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPowerProductsAccordingToLocation");
            cmd.Parameters.AddWithValue("@PowerZoneId", powerZoneId);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetPowerProductsAccordingToTimeZone(Int32 timeZoneId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPowerProductsAccordingToTimeZone");
            cmd.Parameters.AddWithValue("@TimeZoneId", timeZoneId);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetTimeZoneAccordingToLocation(Int32 locationId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetTimeZoneAccordingToLocation");
            cmd.Parameters.AddWithValue("@LocationID", locationId);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetAgreementHeaderInfo(Int32 AgreementID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetAgreementHeaderInfo");
            cmd.Parameters.AddWithValue("@AgreementID", AgreementID);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static void DeleteAgreementAssociations(Int32 agreementID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_DeleteAgreementsAssociations");
            cmd.Parameters.AddWithValue("@AgreementID", agreementID);
            DataManager.GetDM().ExecuteNonQuery(cmd);
        }

        public static DataTable GetAssociatedCommodities(Int32 agreementID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetAssociatedCummodities");
            cmd.Parameters.AddWithValue("@AgreementID", agreementID);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetAssociatedCurrencies(Int32 agreementID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetAssociatedCurrencies");
            cmd.Parameters.AddWithValue("@AgreementID", agreementID);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetAssociatedSettlementTypes(Int32 agreementID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetAssociatedSettlementTypes");
            cmd.Parameters.AddWithValue("@AgreementID", agreementID);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static void SaveAgreementCurrency(Int32 agreementID, Int32 currencyID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_InsertAgreementCurrencies");
            cmd.Parameters.AddWithValue("@AgreementID", agreementID);
            cmd.Parameters.AddWithValue("@CurrencyID", currencyID);
            DataManager.GetDM().ExecuteNonQuery(cmd);
        }

        public static void SaveAgreementSettlementType(Int32 agreementID, Int32 settlementTypeID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_InsertAgreementSettlementType");
            cmd.Parameters.AddWithValue("@AgreementID", agreementID);
            cmd.Parameters.AddWithValue("@SettlementTypeID", settlementTypeID);
            DataManager.GetDM().ExecuteNonQuery(cmd);
        }

        public static void SaveAgreementCommodity(Int32 agreementID, Int32 underlyingID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_InsertPartyAgreementUnderlying");
            cmd.Parameters.AddWithValue("@AgreementID", agreementID);
            cmd.Parameters.AddWithValue("@UnderlyingID", underlyingID);
            DataManager.GetDM().ExecuteNonQuery(cmd);
        }

        public static Int32 SaveAgreementHeader(AgreementHeader deal)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_InsertPartyAgreement");
                cmd.Parameters.Add(CreateInt32Parameter("@PartyAgreementID", deal.AgreementID));
                cmd.Parameters.Add(CreateStringParameter("@PartyAgreementName", deal.AgreementName));
                cmd.Parameters.Add(CreateStringParameter("@CPTYAgreementID", deal.CptyAgreementID));
                cmd.Parameters.Add(CreateInt32Parameter("@PartyAgreementStatusID", deal.AgreementStatusID));
                cmd.Parameters.Add(CreateInt32Parameter("@PartyAgreementTypeID", deal.AgreementTypeID));
                cmd.Parameters.Add(CreateDateTimeParameter("@CoverageStartDate", deal.EffectiveFrom));
                cmd.Parameters.Add(CreateDateTimeParameter("@CoverageEndDate", deal.EffectiveTo));
                cmd.Parameters.Add(CreateDateTimeParameter("@ContractSignDate", deal.SignedOn));
                cmd.Parameters.Add(CreateInt32Parameter("@IntPartyID", deal.InternalLE));
                cmd.Parameters.Add(CreateInt32Parameter("@ExtPartyID", deal.ExternalLE));
                cmd.Parameters.Add(CreateInt32Parameter("@AgreementAgentID", deal.AgreementAgentID));
                cmd.Parameters.Add(CreateInt32Parameter("@AgreementVersionID", deal.AgreementVersionYearID));
                cmd.Parameters.Add(CreateInt32Parameter("@ConfirmRouteID", deal.ConfirmRouteID));
                DataManager.GetDM().ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }
            return deal.AgreementID;
        }

        public static Int32 UpdateAgreementHeader(AgreementHeader deal)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_UpdatePartyAgreement");
                cmd.Parameters.Add(CreateInt32Parameter("@PartyAgreementID", deal.AgreementID));
                cmd.Parameters.Add(CreateStringParameter("@PartyAgreementName", deal.AgreementName));
                cmd.Parameters.Add(CreateStringParameter("@CPTYAgreementID", deal.CptyAgreementID));
                cmd.Parameters.Add(CreateInt32Parameter("@PartyAgreementStatusID", deal.AgreementStatusID));
                cmd.Parameters.Add(CreateInt32Parameter("@PartyAgreementTypeID", deal.AgreementTypeID));
                cmd.Parameters.Add(CreateDateTimeParameter("@CoverageStartDate", deal.EffectiveFrom));
                cmd.Parameters.Add(CreateDateTimeParameter("@CoverageEndDate", deal.EffectiveTo));
                cmd.Parameters.Add(CreateDateTimeParameter("@ContractSignDate", deal.SignedOn));
                cmd.Parameters.Add(CreateInt32Parameter("@IntPartyID", deal.InternalLE));
                cmd.Parameters.Add(CreateInt32Parameter("@ExtPartyID", deal.ExternalLE));
                cmd.Parameters.Add(CreateInt32Parameter("@AgreementAgentID", deal.AgreementAgentID));
                cmd.Parameters.Add(CreateInt32Parameter("@AgreementVersionID", deal.AgreementVersionYearID));
                cmd.Parameters.Add(CreateInt32Parameter("@ConfirmRouteID", deal.ConfirmRouteID));
                DataManager.GetDM().ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }
            return deal.AgreementID;
        }

        public static bool IsHoliday(DateTime date, Int32 calId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_IsHoliday");
            cmd.Parameters.AddWithValue("@CalendarID", calId);
            cmd.Parameters.AddWithValue("@Date", date.Date);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return !IsEmpty(dt);
        }

        public static DataTable GetCashSettlementEvents(Int32 TradeId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetCashSettlementEvents");
            cmd.Parameters.AddWithValue("@TradeId", TradeId);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetPowerPhysicalSchedule(Int32 TradeId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetSchedule");
            cmd.Parameters.AddWithValue("@TradeId", TradeId);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetTradeDeliveries(Int32 TradeId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetDeliveries");
            cmd.Parameters.AddWithValue("@TradeId", TradeId);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetSpreadDeliveries(Int32 TradeId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetSpreadDeliveries");
            cmd.Parameters.AddWithValue("@TradeId", TradeId);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetFixedDeliveries(Int32 TradeId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetFixedDeliveries");
            cmd.Parameters.AddWithValue("@TradeId", TradeId);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static Int32 CleanupPhisycalSettlementEvent(Int32 tradeId)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_CleanupSettlementEvents");
                cmd.Parameters.Add(CreateInt32Parameter("@TradeId", tradeId));
                DataManager.GetDM().ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }
            return tradeId;
        }

        public static Int32 CleanupOTCTradePricingInfo(Int32 tradeId)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_CleanupOTCTradePricingInfo");
                cmd.Parameters.Add(CreateInt32Parameter("@TradeId", tradeId));
                //DataManager.GetDM().ExecuteNonQuery(cmd);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }
            finally
            {
                cmd.Dispose();
            }
            return tradeId;
        }

        public static Int32 UpdatePowerPhisycalSettlementEvent(CashEvt evt)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_UpdateTradeSettlementEvent");
                cmd.Parameters.Add(CreateInt32Parameter("@TradeId", evt.TradeID));
                cmd.Parameters.Add(CreateInt32Parameter("@TradeSettlementEventId", evt.SettlementID));
                cmd.Parameters.Add(CreateDateTimeParameter("@SettlementPeriodStart", evt.SettPeriodStart));
                cmd.Parameters.Add(CreateDateTimeParameter("@SettlementPeriodEnd", evt.SettPeriodEnd));
                cmd.Parameters.Add(CreateDateTimeParameter("@PaymentDate", evt.PaymentDate));
                cmd.Parameters.Add(CreateDoubleParameter("@EventQuantity", evt.Quantity));
                cmd.Parameters.Add(CreateDoubleParameter("@SettlementAmount", evt.Amount));
                cmd.Parameters.Add(CreateInt32Parameter("@CreateByUIID", evt.UserID));

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }
            return evt.SettlementID;
        }

        public static Int32 SavePowerPhisycalSettlementEvent(CashEvt evt)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_InsertTradeSettlementEvent");
                cmd.Parameters.Add(CreateInt32Parameter("@TradeId", evt.TradeID));
                cmd.Parameters.Add(CreateInt32Parameter("@TradeSettlementEventId", evt.TradeSettlementEventId));
                cmd.Parameters.Add(CreateInt32Parameter("@SettlementID", evt.SettlementID));
                cmd.Parameters.Add(CreateStringParameter("@ExternalTradeId", string.Empty));
                cmd.Parameters.Add(CreateStringParameter("@ExternalReference", string.Empty));
                cmd.Parameters.Add(CreateInt32Parameter("@EventStatusID", evt.EventStatusID));
                cmd.Parameters.Add(CreateInt32Parameter("@SettlementTypeID", evt.SettlementTypeID));
                cmd.Parameters.Add(CreateDateTimeParameter("@SettlementPeriodStart", evt.SettPeriodStart));
                cmd.Parameters.Add(CreateDateTimeParameter("@SettlementPeriodEnd", evt.SettPeriodEnd));
                cmd.Parameters.Add(CreateDateTimeParameter("@PaymentDate", evt.PaymentDate));
                cmd.Parameters.Add(CreateDoubleParameter("@EventQuantity", evt.Quantity));
                cmd.Parameters.Add(CreateInt32Parameter("@EventQuantityUnitID", evt.QuantityUnit));
                cmd.Parameters.Add(CreateDoubleParameter("@EventPrice", evt.Price));
                cmd.Parameters.Add(CreateDoubleParameter("@EventEffectivePrice", evt.EffectivePrice));
                cmd.Parameters.Add(CreateInt32Parameter("@PricingIndexID", evt.PricingIndexID));
                cmd.Parameters.Add(CreateInt32Parameter("@SwapID", evt.SwapID));
                cmd.Parameters.Add(CreateDoubleParameter("@EventSpread", evt.Spread));
                cmd.Parameters.Add(CreateInt32Parameter("@EventPriceCcyID", evt.CCY));
                cmd.Parameters.Add(CreateDoubleParameter("@SettlementAmount", evt.Amount));
                cmd.Parameters.Add(CreateInt32Parameter("@SettlementAmountCcyID", evt.AmtCCY));
                cmd.Parameters.Add(CreateInt32Parameter("@TradePricingInfoId", evt.TradePricingInfoId));
                cmd.Parameters.Add(CreateBoolParameter("@IsDelivery", evt.IsDelivery));
                cmd.Parameters.Add(CreateInt32Parameter("@PayerID", evt.PayerID));
                cmd.Parameters.Add(CreateInt32Parameter("@PayeeID", evt.PayeeID));
                cmd.Parameters.Add(CreateInt32Parameter("@CreateByUIID", evt.UserID));

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }
            return evt.SettlementID;
        }

        public static Int32 SavePowerPhysicalPricingInfo(TradePricingInfo pi)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_InsertPricingInfo");

                cmd.Parameters.Add(CreateInt32Parameter("@TradePricingInfoId", pi.TradePricingInfoId));
                cmd.Parameters.Add(CreateInt32Parameter("@TradeId", pi.TradeId));
                cmd.Parameters.Add(CreateInt32Parameter("@PricingTypeID", pi.PricingTypeID));
                cmd.Parameters.Add(CreateDoubleParameter("@FixedPxSpread", pi.FixedPxSpread));
                cmd.Parameters.Add(CreateInt32Parameter("@FixedPxSpreadCcyID", pi.FixedPxSpreadCcyID));
                cmd.Parameters.Add(CreateInt32Parameter("@FixedPxSpreadUnitID", pi.FixedPxSpreadUnitID));
                cmd.Parameters.Add(CreateInt32Parameter("@PriceIndexID", pi.PriceIndexID));
                cmd.Parameters.Add(CreateInt32Parameter("@PriceIndexCcyID", pi.PriceIndexCcyID));
                cmd.Parameters.Add(CreateDoubleParameter("@PriceIndexPercent", pi.PriceIndexPercent));
                cmd.Parameters.Add(CreateInt32Parameter("@SpecifiedPriceID", pi.SpecifiedPriceID));
                cmd.Parameters.Add(CreateInt32Parameter("@PricingCalendarID", pi.PricingCalendarID));
                cmd.Parameters.Add(CreateInt32Parameter("@CalculationPeriodID", pi.CalculationPeriodID));
                cmd.Parameters.Add(CreateInt32Parameter("@Rounding", pi.Rounding));
                cmd.Parameters.Add(CreateInt32Parameter("@DayDistributionID", pi.DayDistributionID));
                cmd.Parameters.Add(CreateInt32Parameter("@DayTypeID", pi.DayTypeID));
                cmd.Parameters.Add(CreateInt32Parameter("@DayCountID", pi.DayCountID));
                cmd.Parameters.Add(CreateInt32Parameter("@PricingDeliveryDateID", pi.PricingDeliveryDateID));
                cmd.Parameters.Add(CreateInt32Parameter("@RollConventionID", pi.RollConventionID));
                cmd.Parameters.Add(CreateInt32Parameter("@AveragingMethodID", pi.AveragingMethodID));
                cmd.Parameters.Add(CreateInt32Parameter("@FXReferenceID", pi.FXReferenceID));
                cmd.Parameters.Add(CreateInt32Parameter("@FXTypeID", pi.FXTypeID));
                cmd.Parameters.Add(CreateDoubleParameter("@FXRate", pi.FXRate));
                cmd.Parameters.Add(CreateInt32Parameter("@PayReceive", pi.PayReceive));
                cmd.Parameters.Add(CreateInt32Parameter("@eConfirmProductID", pi.eConfirmID));
                cmd.Parameters.Add(CreateInt32Parameter("@CreateByUIID", pi.CreateByUIID));
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }

            return pi.TradePricingInfoId;
        }

        public static Int32 UpdatePowerPhysicalPricingInfo(TradePricingInfo pi)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_UpdatePricingInfo");

                cmd.Parameters.Add(CreateInt32Parameter("@TradePricingInfoId", pi.TradePricingInfoId));
                cmd.Parameters.Add(CreateInt32Parameter("@TradeId", pi.TradeId));
                cmd.Parameters.Add(CreateInt32Parameter("@PricingTypeID", pi.PricingTypeID));
                cmd.Parameters.Add(CreateDoubleParameter("@FixedPxSpread", pi.FixedPxSpread));
                cmd.Parameters.Add(CreateInt32Parameter("@FixedPxSpreadCcyID", pi.FixedPxSpreadCcyID));
                cmd.Parameters.Add(CreateInt32Parameter("@FixedPxSpreadUnitID", pi.FixedPxSpreadUnitID));
                cmd.Parameters.Add(CreateInt32Parameter("@PriceIndexID", pi.PriceIndexID));
                cmd.Parameters.Add(CreateInt32Parameter("@PriceIndexCcyID", pi.PriceIndexCcyID));
                cmd.Parameters.Add(CreateDoubleParameter("@PriceIndexPercent", pi.PriceIndexPercent));
                cmd.Parameters.Add(CreateInt32Parameter("@SpecifiedPriceID", pi.SpecifiedPriceID));
                cmd.Parameters.Add(CreateInt32Parameter("@PricingCalendarID", pi.PricingCalendarID));
                cmd.Parameters.Add(CreateInt32Parameter("@CalculationPeriodID", pi.CalculationPeriodID));
                cmd.Parameters.Add(CreateInt32Parameter("@Rounding", pi.Rounding));
                cmd.Parameters.Add(CreateInt32Parameter("@DayDistributionID", pi.DayDistributionID));
                cmd.Parameters.Add(CreateInt32Parameter("@DayTypeID", pi.DayTypeID));
                cmd.Parameters.Add(CreateInt32Parameter("@DayCountID", pi.DayCountID));
                cmd.Parameters.Add(CreateInt32Parameter("@PricingDeliveryDateID", pi.PricingDeliveryDateID));
                cmd.Parameters.Add(CreateInt32Parameter("@RollConventionID", pi.RollConventionID));
                cmd.Parameters.Add(CreateInt32Parameter("@AveragingMethodID", pi.AveragingMethodID));
                cmd.Parameters.Add(CreateInt32Parameter("@FXReferenceID", pi.FXReferenceID));
                cmd.Parameters.Add(CreateInt32Parameter("@FXTypeID", pi.FXTypeID));
                cmd.Parameters.Add(CreateDoubleParameter("@FXRate", pi.FXRate));
                cmd.Parameters.Add(CreateInt32Parameter("@UpdateByUIID", pi.UpdateByUIID));
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }

            return pi.TradePricingInfoId;
        }

        public static Int32 DeletePowerPhysicalPricingInfo(TradePricingInfo pi)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_DeletePricingInfo");
                cmd.Parameters.Add(CreateInt32Parameter("@TradePricingInfoId", pi.TradePricingInfoId));
                cmd.Parameters.Add(CreateInt32Parameter("@TradeId", pi.TradeId));
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }

            return 1;
        }

        public static Int32 SavePowerPhisycalDeal(PhyPowFwd deal)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_InsertTrade");
                cmd.Parameters.Add(CreateInt32Parameter("@TradeId", deal.TradeId));
                cmd.Parameters.Add(CreateStringParameter("@ExternalTradeId", string.Empty));
                cmd.Parameters.Add(CreateInt32Parameter("@VersionNumber", 1));
                cmd.Parameters.Add(CreateInt32Parameter("@TradeStatusId", deal.TradeStatus));
                cmd.Parameters.Add(CreateDateTimeParameter("@TradeDate", deal.TradeDate));
                cmd.Parameters.AddWithValue("@TradeExecutionDateTime", DateTimeOffset.Now);
                cmd.Parameters.Add(CreateInt32Parameter("@ExecutionTypeId", deal.ExecutionTypeId));
                cmd.Parameters.Add(CreateInt32Parameter("@ExecutionVenueTypeId", deal.ExecutionVenueTypeId));
                cmd.Parameters.Add(CreateInt32Parameter("@BuySellId", deal.BuySellId));
                cmd.Parameters.Add(CreateInt32Parameter("@CommodityId", deal.CommodityId));
                cmd.Parameters.Add(CreateInt32Parameter("@SettlementTypeId", deal.SettlementTypeId));
                cmd.Parameters.Add(CreateInt32Parameter("@TradeTypeId", deal.TradeTypeId));
                cmd.Parameters.Add(CreateInt32Parameter("@TradeSubTypeId", deal.TradeSubTypeId));
                cmd.Parameters.Add(CreateInt32Parameter("@IntCounterpartyId", deal.IntCounterpartyId));
                cmd.Parameters.Add(CreateInt32Parameter("@ExtCounterpartyId", deal.ExtCounterpartyId));
                cmd.Parameters.Add(CreateInt32Parameter("@AgreementId", deal.AgreementId));
                cmd.Parameters.Add(CreateInt32Parameter("@TraderId", deal.TraderId));
                cmd.Parameters.Add(CreateInt32Parameter("@SalesPersonId", deal.SalesPersonId));
                cmd.Parameters.Add(CreateInt32Parameter("@PortfolioId", deal.PortfolioId));
                cmd.Parameters.Add(CreateInt32Parameter("@StrategyId", deal.StrategyId));
                cmd.Parameters.Add(CreateStringParameter("@Reference", deal.Reference));
                cmd.Parameters.Add(CreateInt32Parameter("@ExtTraderId", deal.ExTraderId));
                cmd.Parameters.Add(CreateInt32Parameter("@ExtSalesPersonId", deal.ExtSalesPersonId));
                cmd.Parameters.Add(CreateInt32Parameter("@ExtPortfolioId", deal.ExtPortfolioId));
                cmd.Parameters.Add(CreateInt32Parameter("@ExtStrategyId", deal.ExtStrategyId));
                cmd.Parameters.Add(CreateDateTimeParameter("@StartDate", deal.StartDate));
                cmd.Parameters.Add(CreateDateTimeParameter("@EndDate", deal.EndDate));
                cmd.Parameters.Add(CreateInt32Parameter("@PriceIndexId", deal.PriceIndexId));
                cmd.Parameters.Add(CreateInt32Parameter("@LocationId", deal.LocationId));
                cmd.Parameters.Add(CreateInt32Parameter("@ProductId", deal.ProductId));
                cmd.Parameters.Add(CreateInt32Parameter("@ServiceLevelId", deal.ServiceLevelId));
                cmd.Parameters.Add(CreateInt32Parameter("@TimezoneId", deal.TimeZoneId));
                cmd.Parameters.Add(CreateStringParameter("@UPIStub", deal.UPIStub));
                cmd.Parameters.Add(CreateStringParameter("@USI", deal.USI));
                cmd.Parameters.Add(CreateInt32Parameter("@eConfirmProductID", deal.eConfirmProductID));
                cmd.Parameters.Add(CreateDoubleParameter("@Quantity", deal.Quantity));
                cmd.Parameters.Add(CreateInt32Parameter("@QuantityUnitId", deal.QuantityUnitId));
                cmd.Parameters.Add(CreateInt32Parameter("@QuantityFrequencyId", deal.QuantityFreqId));
                cmd.Parameters.Add(CreateDoubleParameter("@QuantityUnitConversionFactor", deal.QuantityUnitConv));
                cmd.Parameters.Add(CreateDoubleParameter("@Price", deal.Price));
                cmd.Parameters.Add(CreateInt32Parameter("@PriceUnitId", deal.PriceUnitId));
                cmd.Parameters.Add(CreateInt32Parameter("@PriceCcyId", deal.PriceCCYId));
                cmd.Parameters.Add(CreateDoubleParameter("@PriceUnitConversionFactor", deal.PriceUnitConv));
                cmd.Parameters.Add(CreateDoubleParameter("@HeatRate", deal.HeatRate));
                cmd.Parameters.Add(CreateInt32Parameter("@Rounding", deal.Rounding));
                cmd.Parameters.Add(CreateDoubleParameter("@TotalNotionalQty", deal.TotalNotionalQTY));
                cmd.Parameters.Add(CreateInt32Parameter("@TotalNotionalQtyUnitId", deal.TotalNotionalQTYUnitId));
                cmd.Parameters.Add(CreateInt32Parameter("@SettlementCcyId", deal.SettlementCCYId));
                cmd.Parameters.Add(CreateInt32Parameter("@PaymentTermDays", deal.PaymentTermDays));
                cmd.Parameters.Add(CreateInt32Parameter("@PaymentTermDayTypeId", deal.PaymentTermDaysTypeID));
                cmd.Parameters.Add(CreateInt32Parameter("@PaymentTermAnchorEventId", deal.PaymentTermAnchorEventId));
                cmd.Parameters.Add(CreateInt32Parameter("@PaymentTermCalendarId", deal.PaymentTermCalendarId));
                cmd.Parameters.Add(CreateInt32Parameter("@PaymentTermPaymentConvId", deal.PaymentTermPaymentConvId));
                cmd.Parameters.Add(CreateInt32Parameter("@PaymentFrequencyId", deal.PaymentFrequencyId));
                cmd.Parameters.Add(CreateBoolParameter("@IncrementDecrement", deal.IncrementDecrement));
                cmd.Parameters.Add(CreateBoolParameter("@OffMarketPrice", deal.OffMarketPrice));
                cmd.Parameters.Add(CreateBoolParameter("@LargeSizeTrade", deal.LargeSizeTrade));
                cmd.Parameters.Add(CreateBoolParameter("@IntentToClear", deal.IntentToClear));
                cmd.Parameters.Add(CreateBoolParameter("@Cleared", deal.Cleared));
                cmd.Parameters.Add(CreateInt32Parameter("@AccountingObservabilityId", deal.AccountingObservabilityId));
                cmd.Parameters.Add(CreateBoolParameter("@CommonPricing", deal.CommonPricing));
                cmd.Parameters.Add(CreateDoubleParameter("@IndependentAmount", deal.IndependentAmount));
                cmd.Parameters.Add(CreateInt32Parameter("@IndependentAmountCCYId", deal.IndependentAmountCCYId));
                cmd.Parameters.Add(CreateDateTimeParameter("@IndependentAmountPaymentDate", deal.IndependentAmountPaymentDate));

                cmd.Parameters.Add(CreateInt32Parameter("@MarketDisruption", deal.MarketDisruption));

                cmd.Parameters.Add(CreateInt32Parameter("@BrokerID", deal.BrokerID));
                cmd.Parameters.Add(CreateInt32Parameter("@BrokerRateTypeID", deal.BrokerRateTypeID));
                cmd.Parameters.Add(CreateInt32Parameter("@BrokerAmountCCYID", deal.BrokerAmountCCYID));
                cmd.Parameters.Add(CreateDoubleParameter("@BrokerRate", deal.BrokerRate));
                cmd.Parameters.Add(CreateDoubleParameter("@BrokerAmount", deal.BrokerAmount));

                cmd.Parameters.Add(CreateInt32Parameter("@OffsetDealID", deal.OffsetDealID));
                cmd.Parameters.Add(CreateStringParameter("@StructureID", deal.StructureID));

                cmd.Parameters.Add(CreateInt32Parameter("@CollateralizationTypeID", deal.CollateralizationTypeId));
                cmd.Parameters.Add(CreateBoolParameter("@EFRP", deal.PartOfEFRP));
                cmd.Parameters.Add(CreateBoolParameter("@BuyerEndUserException", deal.BuyerEndUserException));
                cmd.Parameters.Add(CreateBoolParameter("@SellerEndUserException", deal.SellerEndUserException));

                cmd.Parameters.Add(CreateInt32Parameter("@SwapPurposeID", deal.SwapPurposeID));

                cmd.Parameters.Add(CreateInt32Parameter("@MarketTypeId", deal.MarketTypeId));
                cmd.Parameters.Add(CreateInt32Parameter("@CreateByUIID", deal.CreateByUIID));
                cmd.Parameters.Add(CreateInt32Parameter("@UpdateByUIID", deal.UpdateByUIID));

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }
            return deal.TradeId;
        }

        public static Int32 UpdatePowerPhisycalDeal(PhyPowFwd deal)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_UpdateTrade");
                cmd.Parameters.Add(CreateInt32Parameter("@TradeId", deal.TradeId));
                cmd.Parameters.Add(CreateStringParameter("@ExternalTradeId", string.Empty));
                cmd.Parameters.Add(CreateInt32Parameter("@VersionNumber", 1));
                cmd.Parameters.Add(CreateInt32Parameter("@TradeStatusId", deal.TradeStatus));
                cmd.Parameters.Add(CreateDateTimeParameter("@TradeDate", deal.TradeDate));
                cmd.Parameters.Add(CreateDateTimeParameter("@TradeExecutionDateTime", DateTime.Now));
                cmd.Parameters.Add(CreateInt32Parameter("@ExecutionTypeId", deal.ExecutionTypeId));
                cmd.Parameters.Add(CreateInt32Parameter("@ExecutionVenueTypeId", deal.ExecutionVenueTypeId));
                cmd.Parameters.Add(CreateInt32Parameter("@BuySellId", deal.BuySellId));
                cmd.Parameters.Add(CreateInt32Parameter("@CommodityId", deal.CommodityId));
                cmd.Parameters.Add(CreateInt32Parameter("@SettlementTypeId", deal.SettlementTypeId));
                cmd.Parameters.Add(CreateInt32Parameter("@TradeTypeId", deal.TradeTypeId));
                cmd.Parameters.Add(CreateInt32Parameter("@TradeSubTypeId", deal.TradeSubTypeId));
                cmd.Parameters.Add(CreateInt32Parameter("@IntCounterpartyId", deal.IntCounterpartyId));
                cmd.Parameters.Add(CreateInt32Parameter("@ExtCounterpartyId", deal.ExtCounterpartyId));
                cmd.Parameters.Add(CreateInt32Parameter("@AgreementId", deal.AgreementId));
                cmd.Parameters.Add(CreateInt32Parameter("@TraderId", deal.TraderId));
                cmd.Parameters.Add(CreateInt32Parameter("@SalesPersonId", deal.SalesPersonId));
                cmd.Parameters.Add(CreateInt32Parameter("@PortfolioId", deal.PortfolioId));
                cmd.Parameters.Add(CreateInt32Parameter("@StrategyId", deal.StrategyId));
                cmd.Parameters.Add(CreateStringParameter("@Reference", deal.Reference));
                cmd.Parameters.Add(CreateInt32Parameter("@ExtTraderId", deal.ExTraderId));
                cmd.Parameters.Add(CreateInt32Parameter("@ExtSalesPersonId", deal.ExtSalesPersonId));
                cmd.Parameters.Add(CreateInt32Parameter("@ExtPortfolioId", deal.ExtPortfolioId));
                cmd.Parameters.Add(CreateInt32Parameter("@ExtStrategyId", deal.ExtStrategyId));
                cmd.Parameters.Add(CreateDateTimeParameter("@StartDate", deal.StartDate));
                cmd.Parameters.Add(CreateDateTimeParameter("@EndDate", deal.EndDate));
                cmd.Parameters.Add(CreateInt32Parameter("@PriceIndexId", deal.PriceIndexId));
                cmd.Parameters.Add(CreateInt32Parameter("@LocationId", deal.LocationId));
                cmd.Parameters.Add(CreateInt32Parameter("@ProductId", deal.ProductId));
                cmd.Parameters.Add(CreateInt32Parameter("@ServiceLevelId", deal.ServiceLevelId));
                cmd.Parameters.Add(CreateInt32Parameter("@TimezoneId", deal.TimeZoneId));
                cmd.Parameters.Add(CreateStringParameter("@UPIStub", deal.UPIStub));
                cmd.Parameters.Add(CreateStringParameter("@USI", deal.USI));
                cmd.Parameters.Add(CreateInt32Parameter("@eConfirmProductID", deal.eConfirmProductID));
                cmd.Parameters.Add(CreateDoubleParameter("@Quantity", deal.Quantity));
                cmd.Parameters.Add(CreateInt32Parameter("@QuantityUnitId", deal.QuantityUnitId));
                cmd.Parameters.Add(CreateInt32Parameter("@QuantityFrequencyId", deal.QuantityFreqId));
                cmd.Parameters.Add(CreateDoubleParameter("@QuantityUnitConversionFactor", deal.QuantityUnitConv));
                cmd.Parameters.Add(CreateDoubleParameter("@Price", deal.Price));
                cmd.Parameters.Add(CreateInt32Parameter("@PriceUnitId", deal.PriceUnitId));
                cmd.Parameters.Add(CreateInt32Parameter("@PriceCcyId", deal.PriceCCYId));
                cmd.Parameters.Add(CreateDoubleParameter("@PriceUnitConversionFactor", deal.PriceUnitConv));
                cmd.Parameters.Add(CreateDoubleParameter("@HeatRate", deal.HeatRate));
                cmd.Parameters.Add(CreateInt32Parameter("@Rounding", deal.Rounding));
                cmd.Parameters.Add(CreateDoubleParameter("@TotalNotionalQty", deal.TotalNotionalQTY));
                cmd.Parameters.Add(CreateInt32Parameter("@TotalNotionalQtyUnitId", deal.TotalNotionalQTYUnitId));
                cmd.Parameters.Add(CreateInt32Parameter("@SettlementCcyId", deal.SettlementCCYId));
                cmd.Parameters.Add(CreateInt32Parameter("@PaymentTermDays", deal.PaymentTermDays));
                cmd.Parameters.Add(CreateInt32Parameter("@PaymentTermDayTypeId", deal.PaymentTermDaysTypeID));
                cmd.Parameters.Add(CreateInt32Parameter("@PaymentTermAnchorEventId", deal.PaymentTermAnchorEventId));
                cmd.Parameters.Add(CreateInt32Parameter("@PaymentTermCalendarId", deal.PaymentTermCalendarId));
                cmd.Parameters.Add(CreateInt32Parameter("@PaymentTermPaymentConvId", deal.PaymentTermPaymentConvId));
                cmd.Parameters.Add(CreateInt32Parameter("@PaymentFrequencyId", deal.PaymentFrequencyId));
                cmd.Parameters.Add(CreateBoolParameter("@IncrementDecrement", deal.IncrementDecrement));
                cmd.Parameters.Add(CreateBoolParameter("@OffMarketPrice", deal.OffMarketPrice));
                cmd.Parameters.Add(CreateBoolParameter("@LargeSizeTrade", deal.LargeSizeTrade));
                cmd.Parameters.Add(CreateBoolParameter("@IntentToClear", deal.IntentToClear));
                cmd.Parameters.Add(CreateBoolParameter("@Cleared", deal.Cleared));
                cmd.Parameters.Add(CreateInt32Parameter("@AccountingObservabilityId", deal.AccountingObservabilityId));
                cmd.Parameters.Add(CreateBoolParameter("@CommonPricing", deal.CommonPricing));
                cmd.Parameters.Add(CreateDoubleParameter("@IndependentAmount", deal.IndependentAmount));
                cmd.Parameters.Add(CreateInt32Parameter("@IndependentAmountCCYId", deal.IndependentAmountCCYId));
                cmd.Parameters.Add(CreateDateTimeParameter("@IndependentAmountPaymentDate", deal.IndependentAmountPaymentDate));

                cmd.Parameters.Add(CreateInt32Parameter("@MarketDisruption", deal.MarketDisruption));

                cmd.Parameters.Add(CreateInt32Parameter("@BrokerID", deal.BrokerID));
                cmd.Parameters.Add(CreateInt32Parameter("@BrokerRateTypeID", deal.BrokerRateTypeID));
                cmd.Parameters.Add(CreateInt32Parameter("@BrokerAmountCCYID", deal.BrokerAmountCCYID));
                cmd.Parameters.Add(CreateDoubleParameter("@BrokerRate", deal.BrokerRate));
                cmd.Parameters.Add(CreateDoubleParameter("@BrokerAmount", deal.BrokerAmount));

                cmd.Parameters.Add(CreateStringParameter("@StructureID", deal.StructureID));

                cmd.Parameters.Add(CreateInt32Parameter("@CollateralizationTypeID", deal.CollateralizationTypeId));
                cmd.Parameters.Add(CreateBoolParameter("@EFRP", deal.PartOfEFRP));
                cmd.Parameters.Add(CreateBoolParameter("@BuyerEndUserException", deal.BuyerEndUserException));
                cmd.Parameters.Add(CreateBoolParameter("@SellerEndUserException", deal.SellerEndUserException));

                cmd.Parameters.Add(CreateInt32Parameter("@SwapPurposeID", deal.SwapPurposeID));

                cmd.Parameters.Add(CreateInt32Parameter("@MarketTypeId", deal.MarketTypeId));
                cmd.Parameters.Add(CreateInt32Parameter("@CreateByUIID", deal.CreateByUIID));
                cmd.Parameters.Add(CreateInt32Parameter("@UpdateByUIID", deal.UpdateByUIID));

                //DataManager.GetDM().ExecuteNonQuery(cmd);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }
            return deal.TradeId;
        }

        public static Int32 VoidOTCDeal(Int32 tradeID, Int32 userID)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_VoidTrade");
                cmd.Parameters.Add(CreateInt32Parameter("@TradeId", tradeID));
                cmd.Parameters.Add(CreateInt32Parameter("@UpdateByUIID", userID));
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }
            return tradeID;
        }

        public static Int32 UpdateBackToBackReferenceTrade(Int32 tradeId, Int32 offsetTradeId, Int32 userID)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_UpdateBackToBackReferenceTrade");
                cmd.Parameters.Add(CreateInt32Parameter("@TradeId", tradeId));
                cmd.Parameters.Add(CreateInt32Parameter("@OffsetDealID", offsetTradeId));
                cmd.Parameters.Add(CreateInt32Parameter("@UpdateByUIID", userID));

                DataManager.GetDM().ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }
            return tradeId;
        }

        public static DataTable GetExecutionType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetExecutionType");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static string GetCalculationPeriod(Int32 idValue)
        {
            string literalValue = string.Empty;
            switch (idValue)
            {
                case 1:
                    literalValue = "D";
                    break;
                case 2:
                    literalValue = "W";
                    break;
                case 3:
                    literalValue = "M";
                    break;
                case 4:
                    literalValue = "Y";
                    break;
                case 5:
                    literalValue = "T";
                    break;
                case 2134216:
                    literalValue = "Q";
                    break;
                default:
                    literalValue = string.Empty;
                    break;

            }
            return literalValue;
        }

        public static DataTable GetPowerPhysicalCalculationPeriod()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetCalculationPeriod");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        //sp_PowerPhysical_GetPricingDayType
        public static DataTable GetPowerPricingDayType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPricingDayType");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }


        public static DataTable GetPowerPricingDeliveryDate()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPricingDeliveryDate");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetAveragingMethod()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetAveragingMethod");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetPowerRollConvention()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetRollConvention");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        //public static DataTable GetSwapType()
        //{
        //    DataTable dt = new DataTable();
        //    SqlCommand cmd = new SqlCommand();

        //    try
        //    {
        //        cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetSwapType");
        //        var dataReader = cmd.ExecuteReader();
        //        dt.Load(dataReader);
        //    }
        //    catch (Exception ex)
        //    {
        //        ETRMDataAccess.WriteLogEntry(ex.Message);
        //    }
        //    finally
        //    {
        //        cmd.Dispose();
        //    }
        //    return dt;
        //}

        public static DataTable GetPowerPricingDays()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPricingDays");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetSpecifiedPrice()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetSpecifiedPrice");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetPowerLocation()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetPowerLocation");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetGasLocation()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_StaticData_GetGasLocation");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }


        public static DataTable GetPowerPhysicalPricingCalendar()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPricingCalendar");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetPowerPhysicalCurveDefinition(Int32 commodityID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetCurveDefinition");
                cmd.Parameters.Add(CreateInt32Parameter("@CommodityID", commodityID));
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetSwapsCurveDefinition(Int32 commodityID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPricingReference");
                cmd.Parameters.Add(CreateInt32Parameter("@CommodityID", commodityID));
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetFloatsFloats(Int32 commodityID, bool? isIncDec)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_FloatsFloats");
                cmd.Parameters.Add(CreateInt32Parameter("@CommodityID", commodityID));
                cmd.Parameters.Add(CreateBoolParameter("@IsIncDec", isIncDec));
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetExecutionVenue()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetExecutionVenue");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetLoadIndicators(Int32 loadshapeId, DateTime start, DateTime end, double power, double price, Int32 calendarID, Int32 settlementFrequencyID, Int32 paymentConvention, Int32 paymentTermDays, Int32 isBusinessDays, Int32 volumeTypeID, Int32 eConfirmID, Int32 OTCPaymentEventID, DateTime tradeDate, string calcPeriod1, string calcPeriod2, string paymentTermFrom, string index1, string index2, string pricingDays1, string pricingDays2, Int32 dayCount1, Int32 dayCount2)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("EXP_GetProductIndicators_UI");
                cmd.Parameters.Add(CreateInt32Parameter("@LoadshapeId", loadshapeId));
                cmd.Parameters.Add(CreateDateTimeParameter("@StartDate", start.Date));
                cmd.Parameters.Add(CreateDateTimeParameter("@EndDate", end.Date));
                cmd.Parameters.Add(CreateDoubleParameter("@Power", power));
                cmd.Parameters.Add(CreateDoubleParameter("@Price", price));
                cmd.Parameters.Add(CreateInt32Parameter("@CalendarID", calendarID));
                cmd.Parameters.Add(CreateInt32Parameter("@SettlementFrequencyID", settlementFrequencyID));
                cmd.Parameters.Add(CreateInt32Parameter("@PaymentConventionID", paymentConvention));
                cmd.Parameters.Add(CreateInt32Parameter("@PaymentTermDays", paymentTermDays));
                cmd.Parameters.Add(CreateInt32Parameter("@UseBusinessDayInsteadOfCalendar", isBusinessDays));
                cmd.Parameters.Add(CreateInt32Parameter("@Granularity", volumeTypeID));
                cmd.Parameters.Add(CreateInt32Parameter("@eConfirmID", eConfirmID));
                cmd.Parameters.Add(CreateInt32Parameter("@OTCPaymentEventID", OTCPaymentEventID));
                cmd.Parameters.Add(CreateDateTimeParameter("@TradeDate", tradeDate.Date));
                cmd.Parameters.Add(CreateStringParameterCBText("@AveragingType1", calcPeriod1));
                cmd.Parameters.Add(CreateStringParameterCBText("@AveragingType2", calcPeriod2));
                cmd.Parameters.Add(CreateStringParameter("@PaymentTermFrom", paymentTermFrom));
                cmd.Parameters.Add(CreateStringParameter("@index1", index1));
                cmd.Parameters.Add(CreateStringParameter("@index2", index2));
                cmd.Parameters.Add(CreateStringParameter("@PricingDays1", pricingDays1));
                cmd.Parameters.Add(CreateStringParameter("@PricingDays2", pricingDays2));

                if (dayCount1 == Int32.MinValue)
                {
                    cmd.Parameters.Add(CreateInt32Parameter("@DayCount1", 0));
                }
                else
                {
                    cmd.Parameters.Add(CreateInt32Parameter("@DayCount1", dayCount1));
                }

                if (dayCount2 == Int32.MinValue)
                {
                    cmd.Parameters.Add(CreateInt32Parameter("@DayCount2", 0));
                }
                else
                {
                    cmd.Parameters.Add(CreateInt32Parameter("@DayCount2", dayCount2));
                }

                cmd.CommandTimeout = 300;

                /*
                @LoadshapeId numeric = 4, 
                @StartDate smalldatetime = '01 Jan 2017', 
                @EndDate smalldatetime = '01 Jan 2018', 
                @Power float = 0, 
                @Price float = 0, 
                @CalendarID numeric = 1000016, 
                @SettlementFrequencyID numeric = 2134215, 
                @PaymentConventionID numeric = 2134225, 
                @PaymentTermDays numeric = 20 
                 */

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static Int32 SaveSchedule(Int32 TradeID)
        {
            try
            {
                Int32 cleanupResult = ETRMDataAccess.CleanupSchedule(TradeID);

                if (cleanupResult > 0)
                {
                    SqlCommand cmd = DataManager.GetDM().GetCommand("EXP_CreateTradePositions");
                    cmd.Parameters.Add(CreateInt32Parameter("@TradeID", TradeID));
                    cmd.CommandTimeout = 300;
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    return Int32.MinValue;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = string.Format("Error executing {0}. The error is: {1}", "SaveSchedule", ex.Message);
                ETRMDataAccess.WriteLogEntry(errorMessage);
                return Int32.MinValue;
            }
            return 1;
        }

        public static Int32 CleanupSchedule(Int32 TradeID)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_DeleteSchedule");
                cmd.Parameters.Add(CreateInt32Parameter("@TradeID", TradeID));
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
                return Int32.MinValue;
            }
            return 1;
        }

        public static DoubleDecker GetPowerPhysicalListing()
        {
            DoubleDecker dd = new DoubleDecker();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPowerPhysicalListing");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dd;
        }

        public static DoubleDecker GetOptionsListing()
        {
            DoubleDecker dd = new DoubleDecker();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetOptionsListing");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dd;
        }

        public static DataTable GetConfirmationLightRecord(Int32 confirmationID)
        {
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_CL_GetConfirmationLightRecord");
            DataTable dt = new DataTable();

            cmd.CommandTimeout = 30;
            cmd.Parameters.AddWithValue("@ConfirmationID", confirmationID);
            var dataReader = cmd.ExecuteReader();
            dt.Load(dataReader);

            return dt;
        }

        //ACP_ACESRequest
        public static DataTable GetAcesXML()
        {
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("ACP_ACESRequest");
                cmd.CommandTimeout = 30;
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                WriteLogEntry(ex.Message);
            }
            return dt;
        }

        public static DoubleDecker GetConfLightConfirmableTrades()
        {
            ETRMDataAccess.PopulateConfirmationTable();
            DoubleDecker dd = new DoubleDecker();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_CL_GetConfirmableTrades");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dd;
        }

        public static DoubleDecker GetConfLightOverview()
        {
            ETRMDataAccess.PopulateConfirmationTable();
            DoubleDecker dd = new DoubleDecker();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_CL_GetConfirmationOverview");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dd;
        }

        public static DataTable GetAcesFtpSettings()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_Aces_GetFtpSettings");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static void CleanUpStagingTable()
        {
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("ACP_ACES_CleanUpStagingTable");
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
        }

        public static void UpdateScheduledPositions()
        {
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("ACP_ACES_UpdateScheduledPositions");
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
        }

        public static void CleanupSchedulingHistory()
        {
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("EXP_ACES_CleanUp_History");
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
        }

        public static bool IsFileAlreadyProcessed(string fileName)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("ACP_ACES_IsFileAlreadyProcessed");
            cmd.Parameters.AddWithValue("@FileName", fileName);
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return !IsEmpty(dt);
        }

        //ACP_Aces_InsertFileRecord
        public static void AcesInsertFileRecord(string fileName)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("ACP_Aces_InsertFileRecord");
            cmd.Parameters.AddWithValue("@FileProcessed", fileName);
            cmd.ExecuteNonQuery();
        }

        public static DataTable GetAcesDownloadSettings()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_Aces_GetAcesDownloadSettings");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DoubleDecker GetSchedulableTrades()
        {
            DoubleDecker dd = new DoubleDecker();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("ACP_GetSchedulableTrades");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dd;
        }

        public static void PopulateConfirmationTable()
        {
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_CL_PopulateConfirmationTable");
                Int32 rows = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
        }


        public static DoubleDecker GetSwapsListing(Int32 commodityID)
        {
            DoubleDecker dd = new DoubleDecker();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetSwapListing");
                cmd.Parameters.AddWithValue("@CommodityID", commodityID);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dd;
        }

        public static DataTable GetDefaultOTCBrokerRate(Int32 underlyingID, Int32 brokerID, Int32 tradeTypeID)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetDefaultOTCBrokerRate");
                cmd.Parameters.AddWithValue("@BrokerID", brokerID);
                cmd.Parameters.AddWithValue("@UnderlyingID", underlyingID);
                cmd.Parameters.AddWithValue("@TradeTypeID", tradeTypeID);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        #endregion

        #region Beastra

        public static Int32 GetNextBeastraOTCSettImportID()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_BeaStra_GetNextOTCSettlementsSequence");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }

            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return Convert.ToInt32(dt.Rows[0][0]);
            }
        }

        //////public static DataTable GetNonProcessedDeals()
        //////{
        //////    DataTable dt = new DataTable();
        //////    try
        //////    {
        //////        SqlCommand cmd = DataManager.GetDM().GetCommand("sp_BeaStra_GetNonProcessedMessages");
        //////        dt = DataManager.GetDM().ExecuteDataTable(cmd);
        //////    }
        //////    catch (Exception ex)
        //////    {
        //////        ETRMDataAccess.WriteLogEntry(ex.Message);
        //////    }
        //////    return dt;
        //////}

        public static DataTable GetBeaconMessage(Int32 messageID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_BeaStra_GetBeaconMessage");
                cmd.Parameters.AddWithValue("@MessageID", messageID);
                dt = DataManager.GetDM().ExecuteDataTable(cmd);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dt;
        }
        public static DataTable GetBeaStraMessageLog(Int32 messageId)
        {
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_BeaStra_GetBeaStraMappingLog");
            DataTable dt = new DataTable();

            cmd.CommandTimeout = 30;
            cmd.Parameters.AddWithValue("@MessageId", messageId);
            var dataReader = cmd.ExecuteReader();
            dt.Load(dataReader);

            return dt;
        }

        public static DataTable GetOptionHeaderInfo(Int32 tradeID)
        {
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_BeaStra_GetOptionHeaderInfo");
            DataTable dt = new DataTable();

            cmd.CommandTimeout = 30;
            cmd.Parameters.AddWithValue("@TradeID", tradeID);
            var dataReader = cmd.ExecuteReader();
            dt.Load(dataReader);

            return dt;
        }

        public static DataTable GetOptionPremiumInfo(Int32 tradeID)
        {
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_BeaStra_GetOptionPremium");
            DataTable dt = new DataTable();

            cmd.CommandTimeout = 30;
            cmd.Parameters.AddWithValue("@TradeID", tradeID);
            var dataReader = cmd.ExecuteReader();
            dt.Load(dataReader);

            return dt;
        }
        public static DataTable GetOptionScheduleInfo(Int32 tradeID)
        {
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_BeaStra_GetOptionSchedule");
            DataTable dt = new DataTable();

            cmd.CommandTimeout = 30;
            cmd.Parameters.AddWithValue("@TradeID", tradeID);
            var dataReader = cmd.ExecuteReader();
            dt.Load(dataReader);

            return dt;
        }

        public static DataTable GetOptionCategory()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_BeaStra_GetOptionCategory");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetOptionExpirationType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_BeaStra_GetOptionExpirationType");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetOptionStatus()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_BeaStra_GetOptionStatus");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetOptionStyle()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_BeaStra_GetOptionStyle");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetBeaStraOptionType()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_BeaStra_GetOptionType");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetTimezone()
        {
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_BeaStra_GetTimezone");
            DataTable dt = new DataTable();

            cmd.CommandTimeout = 300;
            var dataReader = cmd.ExecuteReader();
            dt.Load(dataReader);

            return dt;
        }

        public static DoubleDecker GetNonProcessedDeals()
        {
            DoubleDecker dd = new DoubleDecker();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("sp_BeaStra_GetNonProcessedMessages");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dd;
        }

        public static void UpdateOTCSettlementRecords(Int32 importID)
        {
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_BeaStra_UpdateOTCTradeSettlements");
            cmd.Parameters.AddWithValue("@ImportID", importID);
            DataManager.GetDM().ExecuteNonQuery(cmd);
        }

        #endregion

        #region Invoice
        public static DataTable GetAllFromInvoiceLineItems(DateTime startDate, DateTime endDate, string BuIds = null, int commodityId = 0, int currencyId = 8)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("[dbo].[INV_sp_InvoiceGetTradeLinesForInvoicingAllPartyV2]");
                cmd.Parameters.AddWithValue("startDate", startDate);
                cmd.Parameters.AddWithValue("endDate", endDate);
                cmd.Parameters.AddWithValue("BuIds", BuIds);
                cmd.Parameters.AddWithValue("IntCounterpartyId", null);
                cmd.Parameters.AddWithValue("SettlementAmountCcyID", currencyId);
                if (commodityId != 0) cmd.Parameters.AddWithValue("CommodityId", commodityId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetAllFromInvoiceLineItemsByTradeId(int tradeId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("[dbo].[INV_sp_InvoiceGetTradeLinesForTradeId]");
                if (tradeId != 0) cmd.Parameters.AddWithValue("TradeId", tradeId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetAllBrokerageInvoiceLineItems(DateTime startDate, DateTime endDate, string brokerBUIds)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("[dbo].[INV_sp_InvoiceGetTradeLinesForInvoicingBrokerage]");
                cmd.Parameters.AddWithValue("startDate", startDate);
                cmd.Parameters.AddWithValue("endDate", endDate);
                cmd.Parameters.AddWithValue("BrokerIds", brokerBUIds);
                //  cmd.Parameters.AddWithValue("BuIds", brokerBUIds); 
                cmd.Parameters.AddWithValue("BuIds", null);

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetInvoiceTradeLinesForCashTrades(DateTime startDate, DateTime endDate, string BUIds)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("[dbo].[INV_sp_InvoiceGetTradeLinesForTrade]");
                cmd.Parameters.AddWithValue("startDate", startDate);
                cmd.Parameters.AddWithValue("endDate", endDate);
                cmd.Parameters.AddWithValue("BuIds", BUIds);
                cmd.Parameters.AddWithValue("BrokerIds", null);
                cmd.Parameters.AddWithValue("EventSubTypeId", null);
                cmd.Parameters.AddWithValue("TradeType", "Cash");

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetInvoiceTradeLinesForExecutionBroker(DateTime startDate, DateTime endDate, string BUIds)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("[dbo].[INV_sp_InvoiceGetTradeLinesForTrade]");
                cmd.Parameters.AddWithValue("startDate", startDate);
                cmd.Parameters.AddWithValue("endDate", endDate);
                cmd.Parameters.AddWithValue("BuIds", BUIds);
                cmd.Parameters.AddWithValue("EventSubTypeId", 3);
                cmd.Parameters.AddWithValue("TradeType", "Future");

                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetAccountFromExternalInvoiceAccount(int partyId, int commodityId, int invoiceGroupId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("[dbo].[INV_sp_GetAccountFromExternalInvoiceAccount]");
                cmd.Parameters.AddWithValue("PartyId", partyId);
                cmd.Parameters.AddWithValue("Commodityid", commodityId);
                cmd.Parameters.AddWithValue("InvoiceGroupID", invoiceGroupId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }


        public static DataTable GetInvoiceGroups()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("[dbo].[INV_GetInvoiceGroups]");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetAllPowerLocations()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("[dbo].[INV_SelectAllFromPowerLocation]");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetAllExternalAccounts()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("[dbo].[INV_SelectAllFromExternalInvoiceAccount]");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static int GetLatestInvoiceNumber()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("[dbo].[INV_GetLatestInvoiceNumber]");
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            if (IsEmpty(dt))
            {
                return Int32.MinValue;
            }
            else
            {
                return DbConvert.GetInt(dt.Rows[0][0], 0);
            }
        }

        public static string GetInvoicePutCall(int tradeId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("dbo.INV_GetPutCall");
                cmd.Parameters.AddWithValue("TradeId", tradeId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            //return dt;
            if (ETRMDataAccess.IsEmpty(dt))
            {
                return string.Empty;
            }
            else
            {
                return Convert.ToString(dt.Rows[0][0]);
            }
        }

        public static string GetInvoiceTradeType(int tradeId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("INV_GetTradeType");
                cmd.Parameters.AddWithValue("@TradeID", tradeId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            if (ETRMDataAccess.IsEmpty(dt))
            {
                return string.Empty;
            }
            else
            {
                return Convert.ToString(dt.Rows[0][0]);
            }
        }

        public static DataTable GetInvoiceDescriptionForBrookerageFee(int tradeId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("[dbo].[INV_GetInvoiceDescriptionForBrookerageFee]");
                cmd.Parameters.AddWithValue("TradeId", tradeId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }

            return dt;
        }

        //public static int SaveInvoiceHistory(InvoiceHistory invoiceHistory)
        //{
        //    int identifier;
        //    try
        //    {
        //        SqlCommand cmd = DataManager.GetDM().GetCommand("[dbo].[INV_InsertInvoiceHistory]");
        //        cmd.Parameters.AddWithValue("InvoiceID", invoiceHistory.InvoiceID);
        //        cmd.Parameters.AddWithValue("Startdate", invoiceHistory.Startdate);
        //        cmd.Parameters.AddWithValue("Enddate", invoiceHistory.Enddate);
        //        cmd.Parameters.AddWithValue("CommodityId", invoiceHistory.CommodityId);
        //        cmd.Parameters.AddWithValue("ProductTypeId", invoiceHistory.InvoiceGroupId);
        //        cmd.Parameters.AddWithValue("CurrencyId", invoiceHistory.CurrencyId);
        //        cmd.Parameters.AddWithValue("CounterPartyBuIds", invoiceHistory.CounterPartyBuIds);
        //        cmd.Parameters.AddWithValue("InvoiceXmlResult", invoiceHistory.InvoiceXmlResult);
        //        cmd.Parameters.AddWithValue("UpdateTimestamp", DateTime.UtcNow);
        //        cmd.Parameters.AddWithValue("UpdateUser", invoiceHistory.UpdateUser);
        //        cmd.Parameters.AddWithValue("Deleted", invoiceHistory.Deleted);
        //        cmd.Parameters.Add(new SqlParameter("@RETURN_VALUE", SqlDbType.Int, 4)).Direction = ParameterDirection.ReturnValue;

        //        var retStat = DataManager.GetDM().ExecuteNonQuery(cmd);
        //        identifier = (int)cmd.Parameters["@RETURN_VALUE"].Value;
        //        invoiceHistory.InvoiceHistoryID = identifier;
        //    }
        //    catch (Exception ex)
        //    {
        //        WriteLogEntry(ex.Message);
        //        return int.MinValue;
        //    }
        //    return identifier;
        //}


        public static DataTable GetDeliveryTypeNavision()
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("[dbo].[INV_StaticData_GetDeliveryType]");
            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        public static DataTable GetAllFromExternalInvoiceAccountForParty(int partyId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("[dbo].[INV_SelectAllFromExternalInvoiceAccountForParty]");
                cmd.Parameters.AddWithValue("PartyId", partyId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DataTable GetInvoiceHistoryFromInvoiceId(int invoiceId)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd = DataManager.GetDM().GetCommand("[dbo].[INV_SelectInvoiceHistoryFromInvoiceId]");
                cmd.Parameters.AddWithValue("InvoiceId", invoiceId);
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            finally
            {
                cmd.Dispose();
            }
            return dt;
        }

        public static DoubleDecker GetInvoiceHeaders()
        {
            DoubleDecker dd = new DoubleDecker();

            try
            {
                DataTable dt = new DataTable();

                SqlCommand cmd = DataManager.GetDM().GetCommand("[dbo].[INV_SelectAllFromInvoiceHeader]");
                dt = DataManager.GetDM().ExecuteDataTable(cmd);

                ADODB.Recordset rs = ConvertToRecordset(dt);
                dd.Table = dt;
                dd.Records = rs;
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }
            return dd;
        }

        public static DataTable GetPartiesSorted(Int32? leBu, Int32? intExt)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_PowerPhysical_GetPartiesSorted");

            if (leBu.HasValue)
            {
                cmd.Parameters.AddWithValue("@lebu", leBu.Value);
            }

            if (intExt.HasValue)
            {
                cmd.Parameters.AddWithValue("@intExt", intExt.Value);
            }

            dt = DataManager.GetDM().ExecuteDataTable(cmd);
            return dt;
        }

        #endregion

        public static void Beastr_AddMessageTo_out(string payload)
        {
            SqlCommand cmd = DataManager.GetDM().GetCommand("Beastr_AddMessageTo_out");
            cmd.Parameters.AddWithValue("@Payload", payload);
            DataManager.GetDM().ExecuteNonQuery(cmd);
        }

        public static DataTable CreateAcesCSVDataTable()
        {
            DataTable accesTable = new DataTable();

            accesTable.Columns.Add("START_DT", typeof(string));
            accesTable.Columns.Add("BLOCK", typeof(string));
            accesTable.Columns.Add("COMPANY", typeof(string));
            accesTable.Columns.Add("COUNTERPARTY", typeof(string));
            accesTable.Columns.Add("CPTY_MKT_ID", typeof(string));
            accesTable.Columns.Add("END_DT", typeof(string));
            accesTable.Columns.Add("EXTERNAL_SYS_ID", typeof(string));
            accesTable.Columns.Add("MARKET", typeof(string));
            accesTable.Columns.Add("MKT_ID", typeof(string));
            accesTable.Columns.Add("POSITION_TYPE", typeof(string));
            accesTable.Columns.Add("QUANTITY", typeof(double));
            accesTable.Columns.Add("SETTLE_CODE", typeof(string));
            accesTable.Columns.Add("SINK", typeof(string));
            accesTable.Columns.Add("SOURCE", typeof(string));
            accesTable.Columns.Add("TIMEZONE", typeof(string));
            accesTable.Columns.Add("NODE", typeof(string));
            accesTable.Columns.Add("CREATED_DT", typeof(string));
            accesTable.Columns.Add("INACTIVE_DT_SYS", typeof(string));
            accesTable.Columns.Add("FilePath", typeof(string));
            accesTable.Columns.Add("FileHash", typeof(string));

            return accesTable;
        }

        public static DataTable GetBeaStraQueueMessageD(Int32 messageId)
        {
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_BeaStra_GetBeaStraQueueMessage");
            DataTable dt = new DataTable();

            cmd.CommandTimeout = 30;
            cmd.Parameters.AddWithValue("@MessageId", messageId);
            var dataReader = cmd.ExecuteReader();
            dt.Load(dataReader);

            return dt;
        }
        #region SuperGotham

        public static DataTable GetGothamRootSchemas(bool? isNew)
        {
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GothamGetRootSchemas");
            DataTable dt = new DataTable();

            try
            {
                if (isNew.HasValue)
                {
                    Int32 typeID = (isNew.Value) ? 1 : 2;
                    cmd.Parameters.AddWithValue("@SchemaTypeID", typeID);
                }

                cmd.CommandTimeout = 30;
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ETRMDataAccess.WriteLogEntry(ex.Message);
            }

            return dt;
        }

        public static DataTable GetGothamSchemaTableDefinition(Int32 schemaID)
        {
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GothamGetRootSchemaTableDefinition");
            DataTable dt = new DataTable();
            cmd.CommandTimeout = 30;
            cmd.Parameters.AddWithValue("@SchemaID", schemaID);
            var dataReader = cmd.ExecuteReader();
            dt.Load(dataReader);

            return dt;
        }

        public static DataTable GetGothamSchemaDefinition(Int32 schemaID)
        {
            SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GothamGetRootSchema");
            DataTable dt = new DataTable();
            cmd.CommandTimeout = 30;
            cmd.Parameters.AddWithValue("@SchemaID", schemaID);
            var dataReader = cmd.ExecuteReader();
            dt.Load(dataReader);

            return dt;
        }

        //sp_GothamGetNextSchemaID

        public static Int32 GetNextSchemaID()
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GothamGetNextSchemaID");
                DataTable dt = new DataTable();
                cmd.CommandTimeout = 30;
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);

                return Convert.ToInt32(dt.Rows[0][0]);

            }catch(Exception ex)
            {
                return -1;
            }
        }

        //

        public static DataTable GetFieldType()
        {
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GothamGetFieldType");
                cmd.CommandTimeout = 30;
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
                ;
            }
            return dt;
        }


        public static DataTable GetGTMTemplates()
        {
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GTMGetTemplates");
                cmd.CommandTimeout = 30;
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
            }
            return dt;
        }

        public static DataTable GetGTMTemplateNames()
        {
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GTMGetTemplateNames");
                cmd.CommandTimeout = 30;
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
            }
            return dt;
        }

        public static DataTable GetUserDefinedItems()
        {
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GTMGetUserItems");
                cmd.CommandTimeout = 30;
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
            }
            return dt;
        }

        //sp_GothamGetNextSchemaTagID

        public static Int32 GetNextSchemaTagID(Int32 schemaID)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GothamGetNextSchemaTagID");
                DataTable dt = new DataTable();
                cmd.Parameters.AddWithValue("@SchemaID", schemaID);
                cmd.CommandTimeout = 30;
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);

                return Convert.ToInt32(dt.Rows[0][0]);
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public static DataTable GetSchemaElement(Int32 schemaID, Int32 keyID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GothamGetSchemaElement");

                cmd.Parameters.AddWithValue("@SchemaID", schemaID);
                cmd.Parameters.AddWithValue("@KeyID", keyID);
                cmd.CommandTimeout = 30;
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
            }
            return dt;
        }

        public static string GetRootSchema(Int32 schemaID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GetRootSchema");

                cmd.Parameters.AddWithValue("@SchemaID", schemaID);
                cmd.CommandTimeout = 30;
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
                return Convert.ToString(dt.Rows[0][0]);
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
            
        }

        //sp_GTMGetTemplate
        public static DataTable GTMGetTemplate(Int32 templateID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GTMGetTemplate");

                cmd.Parameters.AddWithValue("@TemplateID", templateID);
                cmd.CommandTimeout = 30;
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
            }
            return dt;
        }

        public static byte[] GetByteArrayDTRElement(DataRow source, string columnName)
        {
            byte[] result = null;

            try
            {
                result = (byte[])source[columnName];
            }
            catch
            { return null; }
            return result;
        }

        public static bool ByteArrayToFile(string fileName, byte[] byteArray)
        {
            try
            {
                using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(byteArray, 0, byteArray.Length);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool DeleteSchemaElement(Int32 schemaID, Int32 keyID)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GothamDeleteSchemaElement");

                cmd.Parameters.AddWithValue("@SchemaID", schemaID);
                cmd.Parameters.AddWithValue("@KeyID", keyID);
                cmd.CommandTimeout = 30;
                var dataReader = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        public static bool IsSchemaElementIn(Int32 schemaID, string fieldName)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GothamIsSchemaElementIn");
                DataTable dt = new DataTable();
                cmd.Parameters.AddWithValue("@SchemaID", schemaID);
                cmd.Parameters.AddWithValue("@FieldName", fieldName);
                cmd.CommandTimeout = 30;
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);

                return (dt.Rows.Count > 0);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool InsertSchemaHeader(Int32 schemaID, Int32 rootSchemaID, string schemaName)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GothamInsertSchemaHeader");
                DataTable dt = new DataTable();
                cmd.Parameters.AddWithValue("@SchemaID", schemaID);
                cmd.Parameters.AddWithValue("@SchemaName", schemaName);
                cmd.Parameters.AddWithValue("@RootSchemaID", rootSchemaID);
                cmd.CommandTimeout = 30;
                Int32 result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public static bool InsertGothamMessage(Int32 schemaID, Int32 templateID, Int32 routeID, Int32 workflowID, Int32? startingStatusID, string root, string customFields, string CustomTables, string internalPartyID, string externalPartyID, string tradeID, string priceIndex)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GTMInsertMessage");
                DataTable dt = new DataTable();
                cmd.Parameters.AddWithValue("@SchemaID", schemaID);
                cmd.Parameters.AddWithValue("@TemplateID", templateID);
                cmd.Parameters.AddWithValue("@RouteID", routeID);
                cmd.Parameters.Add(ETRMDataAccess.CreateInt32Parameter("@WorkflowID", workflowID));
                cmd.Parameters.Add(ETRMDataAccess.CreateInt32Parameter("@StartingStatusID", startingStatusID));
                cmd.Parameters.Add(ETRMDataAccess.CreateStringParameter("@Root", root));
                cmd.Parameters.Add(ETRMDataAccess.CreateStringParameter("@CustomFields", customFields));
                cmd.Parameters.Add(ETRMDataAccess.CreateStringParameter("@CustomTables", CustomTables));
                cmd.Parameters.Add(ETRMDataAccess.CreateStringParameter("@ExternalPartyID", externalPartyID));
                cmd.Parameters.Add(ETRMDataAccess.CreateStringParameter("@InternalPartyID", internalPartyID));
                cmd.Parameters.Add(ETRMDataAccess.CreateStringParameter("@TradeID", tradeID));
                cmd.Parameters.Add(ETRMDataAccess.CreateStringParameter("@PriceIndex", priceIndex));

                cmd.CommandTimeout = 30;
                Int32 result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public static DataTable GetGTMessage(Int32 messageID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GTMGetMessage");

                cmd.Parameters.AddWithValue("@MessageID", messageID);
                cmd.CommandTimeout = 30;
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
            }
            return dt;
        }

        //sp_GTMGetPartyInfo
        public static DataTable GetGTExtPartyInfo(string cptyID)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GTMGetPartyInfo");

                cmd.Parameters.AddWithValue("@PartyID", cptyID);
                cmd.CommandTimeout = 30;
                var dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
            }
            catch (Exception ex)
            {
            }
            return dt;
        }


        public static bool InsertSchemaElement(Int32 schemaID, string schemaTag, Int32 fieldTypeID, Int32? columns)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GothamInsertSchemaElement");
                DataTable dt = new DataTable();
                cmd.Parameters.AddWithValue("@SchemaID", schemaID);
                cmd.Parameters.AddWithValue("@SchemaTag", schemaTag);
                cmd.Parameters.AddWithValue("@FieldTypeID", fieldTypeID);
                cmd.Parameters.Add(ETRMDataAccess.CreateInt32Parameter("@Columns", columns));
                cmd.CommandTimeout = 30;
                Int32 result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public static bool UpdateSchemaElement(Int32 schemaID, Int32 tagID, string schemaTag, Int32 fieldTypeID, Int32? columns)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GothamUpdateSchemaElement");
                DataTable dt = new DataTable();
                cmd.Parameters.AddWithValue("@SchemaID", schemaID);
                cmd.Parameters.AddWithValue("@TagID", tagID);
                cmd.Parameters.AddWithValue("@SchemaTag", schemaTag);
                cmd.Parameters.AddWithValue("@FieldTypeID", fieldTypeID);
                cmd.Parameters.Add(ETRMDataAccess.CreateInt32Parameter("@Columns", columns));
                cmd.CommandTimeout = 30;
                Int32 result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public static bool InsertTemplate(string templateName, byte[] template)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GTMInsertTemplate");
                DataTable dt = new DataTable();
                cmd.Parameters.AddWithValue("@TemplateName", templateName);
                cmd.Parameters.AddWithValue("@Template", SqlDbType.VarBinary).Value = template;
                cmd.CommandTimeout = 30;
                Int32 result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        //sp_GTMInsertDocument
        /*
            @MessageID bigint,
            @TradeID varchar(500),
            @GeneratedDocument varbinary(MAX),
            @DocumentName varchar(500)
        */

        public static bool InsertDocument(DocumentInfo documentInfo, byte[] generatedDocument, byte[] pdfFile)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GTMInsertDocument");
                DataTable dt = new DataTable();
                cmd.Parameters.AddWithValue("@MessageID", documentInfo.MessageID);
                cmd.Parameters.AddWithValue("@DocumentName", documentInfo.DocumentName);
                cmd.Parameters.AddWithValue("@PdfFileName", documentInfo.PdfFileName);
                cmd.Parameters.AddWithValue("@GeneratedDocument", SqlDbType.VarBinary).Value = generatedDocument;
                cmd.Parameters.AddWithValue("@PdfFile", SqlDbType.VarBinary).Value = pdfFile;
                cmd.CommandTimeout = 30;
                Int32 result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public static bool UpdateTemplate(Int32 templateID, string templateName, byte[] template)
        {
            try
            {
                SqlCommand cmd = DataManager.GetDM().GetCommand("sp_GTMUpdateTemplate");
                DataTable dt = new DataTable();
                cmd.Parameters.AddWithValue("@TemplateID", templateID);
                cmd.Parameters.AddWithValue("@TemplateName", templateName);
                cmd.Parameters.AddWithValue("@Template", SqlDbType.VarBinary).Value = template;
                cmd.CommandTimeout = 30;
                Int32 result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public static bool InsertSchema(DataTable schema)
        {
            try
            {
                return DataManager.GetDM().BulkInsertion(schema, "GothamSchemaDefinition");
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static IEnumerable<long> GetAllMessagesWithoutDocuments()
        {
            var messageIds = new List<long>();
            var cmd = DataManager.GetDM().cn.CreateCommand();
            cmd.CommandText = "SELECT m.MessageID FROM [dbo].[GTMessage] m left join dbo.GTDocument d on d.MessageID = m.MessageID  where d.MessageID is null";
            using (var reader = cmd.ExecuteReader())
            {
                while(reader.Read())
                {
                    messageIds.Add(reader.GetInt64(0));
                }
            }
            return messageIds;
        }

        #endregion
    }
}
