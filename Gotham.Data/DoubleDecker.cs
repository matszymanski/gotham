﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Data
{
    public class DoubleDecker
    {
        public DataTable Table { get; set; }
        public ADODB.Recordset Records { get; set; }
    }
}
