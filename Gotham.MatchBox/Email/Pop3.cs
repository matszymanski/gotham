﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailKit;
using MailKit.Net.Pop3;
using MimeKit;

namespace Gotham.MatchBox.Email
{
    internal class Pop3 : MailServer
    {
        public Pop3(Configuration config) : base(config)
        {
        }
        

        public override IEnumerable<MimeMessage> GetMessages()
        {
            using (var client = new Pop3Client())
            {
                client.ServerCertificateValidationCallback = ValidateCertificate;

                client.Connect(_config.Inbox.Host, _config.Inbox.Port, _config.Inbox.Ssl);

                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                client.AuthenticationMechanisms.Remove("XOAUTH2");

                client.Authenticate(_config.Inbox.GetCredentials());

                for (int i = 0; i < client.Count; i++)
                {
                    yield return client.GetMessage(i);
                }

                client.Disconnect(true);
            }
        }
    }
}
