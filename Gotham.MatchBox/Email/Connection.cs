﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.MatchBox.Email
{
    internal class Connection
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public bool Ssl { get; set; }

        public NetworkCredential Credentials { get; set; }

    }
}
