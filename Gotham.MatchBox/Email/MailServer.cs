﻿using MailKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using MimeKit;
using MailKit.Net.Smtp;
using Gotham.Domain.MatchBox;
using Gotham.Domain.Confirmation;

namespace Gotham.MatchBox.Email
{
    internal abstract class MailServer
    {
        protected Configuration _config;

        public MailServer(Configuration config)
        {
            _config = config;
        }

        public abstract IEnumerable<MimeMessage> GetMessages();

        public virtual void ForwardProcessedMessage(IEnumerable<MimeEntity> attachments, MimeMessage originalMessage, TradeUniqueKey tradeKey)
        {
            var template = _config.ProcessedTemplate;
            var outMessage = new MimeMessage();
            outMessage.From.Add(new MailboxAddress(template.FromName, template.FromEmail));
            outMessage.To.AddRange(_config.ProcessedEmails);
            outMessage.Subject = FormatTemplate(template.Subject, tradeKey);
            outMessage.Body = CreateBody(template, tradeKey, attachments, originalMessage);

            SendEmail(outMessage);
        }

        public virtual void ForwardFailedMessage(IEnumerable<MimeEntity> attachments, MimeMessage originalMessage)
        {
            var template = _config.FailedTemplate;
            var outMessage = new MimeMessage();
            outMessage.From.Add(new MailboxAddress(template.FromName, template.FromEmail));
            outMessage.To.AddRange(_config.FailedEmails);
            outMessage.Subject = template.Subject;
            outMessage.Body = CreateBody(template, attachments, originalMessage);

            SendEmail(outMessage);
        }

        private void SendEmail(MimeMessage message)
        {
            using (var client = new SmtpClient())
            {
                client.ServerCertificateValidationCallback = ValidateCertificate;

                client.Connect(_config.Outbox.Host, _config.Outbox.Port, _config.Outbox.Ssl);

                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                client.AuthenticationMechanisms.Remove("XOAUTH2");

                client.Authenticate(_config.Outbox.GetCredentials());

                client.Send(message);
                client.Disconnect(true);
            }
        }

        public bool ValidateCertificate(object sender,
                              X509Certificate certificate,
                              X509Chain chain,
                              SslPolicyErrors sslErrors)
        {
            // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS) //TODO
            return true;
        }

        public static string FormatTemplate(string template, TradeUniqueKey tradeKey)
        {
            string result = template;
            result = result.Replace("@TradeID", tradeKey.TradeID);
            result = result.Replace("@InternalPartyID", tradeKey.InternalPartyID);
            return result;
        }

        public MimeEntity CreateBody(Template template, TradeUniqueKey tradeKey, IEnumerable<MimeEntity> attachments, MimeMessage originalMessage)
        {
            var htmlTemplate = FormatTemplate(template.HtmlBody, tradeKey);
            var textTemplate = FormatTemplate(template.TextBody, tradeKey);
            var body = new BodyBuilder();
            
            body.TextBody = textTemplate;
            body.HtmlBody = htmlTemplate;

            attachments.ToList().ForEach(attachment => body.Attachments.Add(attachment));
            StripAttachments(originalMessage);
            body.Attachments.Add(new MessagePart { Message = originalMessage });

            return body.ToMessageBody();
        }

        public MimeEntity CreateBody(Template template, IEnumerable<MimeEntity> attachments, MimeMessage originalMessage)
        {
            var body = new BodyBuilder();

            body.TextBody = template.TextBody;
            body.HtmlBody = template.HtmlBody;

            attachments.ToList().ForEach(attachment => body.Attachments.Add(attachment));
            StripAttachments(originalMessage);
            body.Attachments.Add(new MessagePart { Message = originalMessage });

            return body.ToMessageBody();
        }

        private void StripAttachments(MimeMessage message)
        {
            var body = new BodyBuilder();

            body.TextBody = message.TextBody;
            body.HtmlBody = message.HtmlBody;

            message.Body = body.ToMessageBody();
        }
    }
}
