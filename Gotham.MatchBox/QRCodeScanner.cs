﻿using Gotham.Domain.Confirmation;
using Gotham.Domain.Documents;
using MimeKit;
using Syncfusion.Pdf.Parsing;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.MatchBox
{
    internal class QRCodeScanner
    {
        public static TradeUniqueKey TryRetrieveTradeKey(MimePart attachment)
        {
            var stream = new MemoryStream();
            attachment.ContentObject.DecodeTo(stream);

            var extension = Path.GetExtension(attachment.FileName) ?? "";
            
            IEnumerable<TradeUniqueKey> tradeKeys;
            switch (extension.ToLowerInvariant())
            {

                case ".pdf":
                    tradeKeys = ParsePdfPages(stream);
                    break;

                default:
                    tradeKeys = ParseImage(stream);
                    break;
            }


            var uniqueTradeKeys = tradeKeys.Where(key => key != null && key.IsValid()).Distinct();
            if (uniqueTradeKeys.Count() == 1)
            {
                return uniqueTradeKeys.First();
            }
            return null;
        }

        private static TradeUniqueKey[] ParseImage(Stream stream)
        {
            var tradeKeys = new List<TradeUniqueKey>();
            try
            {
                Bitmap image = new Bitmap(stream);
                var scanResults = Scan(image);
                tradeKeys.Add(QrCoder.Parse(scanResults));
            }
            catch (Exception e)
            {
                //log
            }
            return tradeKeys.ToArray();
        }


        /// <returns>One key per page, can be empty</returns>
        public static TradeUniqueKey[] ParsePdfPages(Stream pdfStream)
        {
            var tradeKeys = new List<TradeUniqueKey>();
            try
            {
                PdfLoadedDocument pdf = new PdfLoadedDocument(pdfStream);
                var pageCount = pdf.Pages.Count;
                
                for (int page = 0; page < pageCount; page++)
                {
                    var bitmap = pdf.ExportAsImage(page);
                    var scanResults = Scan(bitmap);
                    tradeKeys.Add(QrCoder.Parse(scanResults));
                }
            }
            catch (Exception e)
            {
                //log
            }

            return tradeKeys.ToArray();
        }

        public static string Scan(Bitmap image)
        {
            var barcodeReader = new ZXing.BarcodeReader();
            var result = barcodeReader.Decode(image);

            if (result != null)
            {
                return result.Text;
            }

            return string.Empty;
        }
    }
}
