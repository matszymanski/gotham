﻿using ArtisanCode.SimpleAesEncryption;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.MatchBox
{
    internal class HashKeyGenerator
    {

        public string HashKey(string password)
        {
            var cryptoContainer = new RijndaelManaged { KeySize = 256 };
            cryptoContainer.GenerateKey();
            var key = Convert.ToBase64String(cryptoContainer.Key);
            var encrypt = new RijndaelMessageEncryptor();
            return encrypt.Encrypt(password);
        }
    }
}
