﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using MimeKit;
using Gotham.Domain.MatchBox;
using Gotham.Core.MatchBox;

namespace Gotham.MatchBox
{
    internal class Configuration
    {
        public EmailServer Inbox { get; set; }
        public EmailServer Outbox { get; set; }
        public IEnumerable<MailboxAddress> ProcessedEmails { get; set; }
        public IEnumerable<MailboxAddress> FailedEmails { get; set; }

        public Template ProcessedTemplate { get; set; }
        public Template FailedTemplate { get; set; }

        private Configuration() { }

        public static Configuration Init(MatchBoxService service)
        {
            var config = new Configuration();
            var servers = service.GetEmailServers();
            
            config.Inbox = servers.Where(s => s.IsInbox).FirstOrDefault();
            config.Outbox = servers.Where(s => !s.IsInbox).FirstOrDefault();

            var emails = service.GetRecipients();
            config.ProcessedEmails = emails.Where(e => e.Type == RecipientTypes.Processed).Select(e => new MailboxAddress(e.Name, e.Email)).ToArray();
            config.FailedEmails = emails.Where(e => e.Type == RecipientTypes.Failed).Select(e => new MailboxAddress(e.Name, e.Email)).ToArray();

            var templates = service.GetTemplates();
            config.ProcessedTemplate = templates.Where(t => t.Type == RecipientTypes.Processed).FirstOrDefault();
            config.FailedTemplate = templates.Where(t => t.Type == RecipientTypes.Failed).FirstOrDefault();

            return config;
        }

        //private static IEnumerable<MailboxAddress> ParseAddresses(string emailCsv)
        //{
        //    var emails = emailCsv.Split(',');
        //    return emails.Select(e => new MailboxAddress(e)).ToArray();
        //}

        //private static Email.Connection ParseConnection(string value)
        //{
            
        //    var emailParams = value.Split(',').ToDictionary(kvp => kvp.Split('=')[0], kvp=>kvp.Split('=')[1]);

        //    var connection = new Email.Connection
        //    {
        //        Host = emailParams["Host"],
        //        Port = int.Parse(emailParams["Port"]),
        //        Ssl = bool.Parse(emailParams["Ssl"]),
        //        Credentials = new System.Net.NetworkCredential(emailParams["Login"], emailParams["Password"])

        //    };
        //    return connection;
        //}
    }
}
