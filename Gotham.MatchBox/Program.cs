﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gotham.MatchBox.Email;
using System.Security.Cryptography;
using ArtisanCode.SimpleAesEncryption;
using Gotham.Core.MatchBox;
using MimeKit;
using System.IO;
using Gotham.Domain.Confirmation;
using Gotham.Core.Web;
using Gotham.Domain.Documents;
using System.Drawing;

namespace Gotham.MatchBox
{
    class Program
    {
        static void Main(string[] args)
        {
            var mbService = new MatchBoxService();
            var configuration = Configuration.Init(mbService);
            if (configuration.Inbox.Protocol == Domain.MatchBox.Protocols.Pop3)
            {
                var email = new Pop3(configuration);
                var messages = email.GetMessages().ToList();
                ScanAndForwardMessages(email, messages);
                //remove messages
            }
            else
                throw new Exception($"Protocol {configuration.Inbox.Protocol} not implemented");

        }

        private static void ScanAndForwardMessages(MailServer email, List<MimeMessage> messages)
        {
            foreach(var message in messages)
            {
                var matched = new List<Tuple<TradeUniqueKey, MimePart>>();
                var unmatched = new List<MimeEntity>();
                foreach (MimePart attachment in message.Attachments)
                {
                    try
                    {
                        var matchedTradeKey = QRCodeScanner.TryRetrieveTradeKey(attachment);
                        if(matchedTradeKey != null)
                        {
                            matched.Add(Tuple.Create(matchedTradeKey, attachment));
                        }
                        else
                            unmatched.Add(attachment);
                    }
                    catch(Exception e)
                    {
                        throw;
                    }

                }
                if (matched.Any())
                {
                    matched.ForEach(m => StoreAttachment(m.Item1, m.Item2, message.From.Mailboxes.FirstOrDefault()));
                    foreach (var tradeAttachmentGroup in matched.GroupBy(m => m.Item1))
                    {
                        email.ForwardProcessedMessage(tradeAttachmentGroup.Select(mes => mes.Item2), message, tradeAttachmentGroup.Key);
                    }
                }
                if (unmatched.Any())
                {
                    email.ForwardFailedMessage(unmatched, message);
                }
            }
        }

        

        private static void StoreAttachment(TradeUniqueKey tradeKey, MimePart attachment, MailboxAddress receivedFromEmail)
        {
            try
            {

                var uploadedDocumentInfo = new UploadedDocumentInfo() {
                    AutoMatched = true,
                    DocumentName = attachment.FileName,
                    SourceEmail = receivedFromEmail != null ? receivedFromEmail.Address : ""
                };
                var stream = new MemoryStream();
                attachment.ContentObject.DecodeTo(stream);
                DocumentService.UploadDocument(tradeKey, uploadedDocumentInfo, stream.ToArray());
            }
            catch(Exception e)
            {
                throw;
            }
        }
    }
}
