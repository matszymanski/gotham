﻿using Gotham.ElementsUI.UIUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gotham.UI.TemplateBuilder
{
    public partial class TemplateFieldsSelector : Form
    {
        public List<string> SelectedField { get; set; }
        public string SelectedItem { get; set; }
        public TemplateFieldsSelector()
        {
            InitializeComponent();
            this.SelectedField = this.GetTestElements();
            this.PopulateElements();
        }

        public TemplateFieldsSelector(List<string> elements, string title)
        {
            InitializeComponent();
            this.Text = title;
            LookAndFeel.SetFormStatus(this);
            this.SelectedField = elements;
            this.PopulateElements();
            this.SelectedItem = string.Empty;
        }

        private void PopulateElements()
        {
            this.lbTemplateFields.DataSource = this.SelectedField;
            this.lbTemplateFields.ClearSelected();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private List<string> GetTestElements()
        {
            List<string> elements = new List<string>();

            elements.Add("COMPANY");
            elements.Add("INTERNAL COMPANY");
            elements.Add("NUMERO UNO");
            elements.Add("INDEX");
            elements.Add("SECOND INDEX");
            elements.Add("CHOICE");

            return elements;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.AddField();
        }

        private void AddField()
        {
            var selectedItems = this.lbTemplateFields.SelectedItems;

            if (selectedItems != null)
            {
                this.SelectedItem = string.Empty;
                foreach (var item in selectedItems)
                {
                    string sit = item as string;
                    this.SelectedItem = sit;
                }
            }

            this.Close();
        }

        private void lbTemplateFields_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lbTemplateFields_DoubleClick(object sender, EventArgs e)
        {
            this.AddField();
        }
    }
}
