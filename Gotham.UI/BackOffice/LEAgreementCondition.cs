﻿using Strazze.Data;
using Strazze.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Strazze.AddIn.Forms
{
    public partial class LEAgreementCondition : Form
    {
        private DataTable AllSettlements;
        private DataTable SelectedSettlements;
        private DataTable AllUnderlying;
        private DataTable SelectedUnderlying;
        private DataTable AllCurrencies;
        private DataTable SelectedCurrencies;

        Int32 ConditionID { get; set; }

        public LEAgreementCondition()
        {
            InitializeComponent();
            this.InitialiseView();
            //this.ConditionID = Get next sequence...
        }

        public LEAgreementCondition(Int32 conditionId)
        {
            InitializeComponent();
            this.InitialiseView();
            this.ConditionID = conditionId;
            this.FillConditionParameters();
        }

        private void FillConditionParameters()
        {

        }

        private void InitialiseView()
        {
            this.FillComboBoxes();
        }

        private void FillComboBoxes()
        {
            this.FillSettlements();
            this.FillUnderlying();
            this.FillCurrency();
        }

        private void FillSettlements()
        {
            this.AllSettlements = ETRMDataAccess.AddBitColumn(ETRMDataAccess.GetSettlementTypes(), Constants.Associated);
            this.SelectedSettlements = null; //ETRMDataAccess.GetAgreementSettlements(Int32 AgreementID);

            this.dgvSettlementType.DataSource = this.AllSettlements.DefaultView;
            this.dgvSettlementType.ReadOnly = false;
            this.dgvSettlementType.Columns[Constants.Associated].ReadOnly = false;
            this.dgvSettlementType.Columns[STypes.SettlementType].ReadOnly = true;
            this.dgvSettlementType.Columns[STypes.SettlementTypeID].Visible = false;
        }

        private void FillUnderlying()
        {
            this.AllUnderlying = ETRMDataAccess.AddBitColumn(ETRMDataAccess.GetDeliveryTypes(), Constants.Associated);
            this.SelectedUnderlying = null; //ETRMDataAccess.GetAgreementUnderlyings(Int32 AgreementID);

            this.dgvUnderlying.DataSource = this.AllUnderlying.DefaultView;
            this.dgvUnderlying.ReadOnly = false;
            this.dgvUnderlying.Columns[Constants.Associated].ReadOnly = false;
            this.dgvUnderlying.Columns[DTypes.DeliveryType].ReadOnly = true;
            this.dgvUnderlying.Columns[DTypes.DeliveryTypeId].Visible = false;

            //ETRMDataAccess.MarkSelected(this.AllUnderlying, SelectedUnderlying, DTypes.DeliveryTypeId, Constants.Associated)
        }

        private void FillCurrency()
        {
            this.AllCurrencies = ETRMDataAccess.AddBitColumn(ETRMDataAccess.GetCurrencies(), Constants.Associated);
            this.SelectedCurrencies = null; //ETRMDataAccess.GetAgreementCurrencies(Int32 AgreementID);

            this.dgvCurrency.DataSource = this.AllCurrencies.DefaultView;
            this.dgvCurrency.ReadOnly = false;
            this.dgvCurrency.Columns[Constants.Associated].ReadOnly = false;
            this.dgvCurrency.Columns[CCY.Currency].ReadOnly = true;
            this.dgvCurrency.Columns[CCY.CurrencyID].Visible = false;
        }

        //////private void FillPortfolios()
        //////{
        //////    this.allPortfolios = ETRMDataAccess.GetAllPortfolios();
        //////    DataTable associatedPortfolios = ETRMDataAccess.GetAssociatedPortfolios(this.GetPartyId());
        //////    this.allPortfolios.Columns.Add(new DataColumn(Portfolio.Associated, typeof(bool)));

        //////    if (!ETRMDataAccess.IsEmpty(associatedPortfolios))
        //////    {
        //////        this.AssociatePortfolios(this.allPortfolios, associatedPortfolios);
        //////    }

        //////    if (!ETRMDataAccess.IsEmpty(this.allPortfolios))
        //////    {
        //////        this.dgvBUPortfolio.DataSource = null;

        //////        DataView sortedView = this.allPortfolios.DefaultView;
        //////        sortedView.Sort = "ASSOCIATED DESC, Portfolio";
        //////        DataTable sorted = sortedView.ToTable();

        //////        this.dgvBUPortfolio.DataSource = sorted.DefaultView;
        //////        this.dgvBUPortfolio.ReadOnly = false;
        //////        this.dgvBUPortfolio.Columns[Portfolio.Associated].ReadOnly = false;
        //////        this.dgvBUPortfolio.Columns[Portfolio.PortfolioName].ReadOnly = true;
        //////    }

        //////    this.HidePortfoliosColumns();
        //////}
        //////
        //////private void AssociatePortfolios(DataTable allPortfolios, DataTable associatedPortfolios)
        //////{
        //////    foreach (DataRow row in this.allPortfolios.Rows)
        //////    {
        //////        var results = from myRow in associatedPortfolios.AsEnumerable()
        //////                      where myRow.Field<int>(Portfolio.PortfolioID) == Convert.ToInt32(row[Portfolio.PortfolioID])
        //////                      select myRow;

        //////        if (results.Count() == 0)
        //////        {
        //////            row[Portfolio.Associated] = false;
        //////        }
        //////        else
        //////        {
        //////            row[Portfolio.Associated] = true;
        //////        }
        //////    }
        //////}


    }
}
