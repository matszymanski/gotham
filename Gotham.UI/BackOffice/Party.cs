﻿using Gotham.Data;
using Gotham.Domain;
using Gotham.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gotham.AddIn.Party
{
    public partial class Party : Form
    {
        ErrorProvider epParty;
        public DataTable headerInfo { get; set; }
        public DataTable address { get; set; }
        public DataTable businessUnits { get; set; }
        public DataTable agreements { get; set; }
        public Int32 PartyID { get; set; }
        public Int32 UserID { get; set; }
        public bool IsNew { get; set; }

        public Party(bool canWrite)
        {
            InitializeComponent();
            this.InitialiseView(canWrite);
            this.IsNew = true;
            this.PartyID = Int32.MinValue;
            this.FillComboBoxes();
            this.ActivateNewEntrySettings();
        }

        public Party(Int32 partyId, DataTable header, bool canWrite)
        {
            InitializeComponent();
            this.FillComboBoxes();
            this.headerInfo = header;
            this.InitialiseView(canWrite);
        }

        private void InitialiseView(bool canWrite)
        {
            this.FillHeaderInfo();
            this.FillAddresses();
            this.FillAgreements();
            this.FillBusinessUnits();
            this.HideColumns();
            this.SetAuthoriser();
            this.SetUserid();
            this.epParty = new ErrorProvider();
        }

        private void SetWritingPermissions(bool canWrite)
        {
            this.btnPartySave.Enabled = canWrite;
            this.btnAddsAdd.Enabled = canWrite;
            this.btnAddEdit.Enabled = canWrite;
            this.btnAddressDelete.Enabled = canWrite;
            this.btnAgreementsDelete.Enabled = canWrite;
            this.btAgreementsAdd.Enabled = canWrite;
            this.btAgreementsModify.Enabled = canWrite;
        }
        private void ActivateNewEntrySettings()
        {
            this.cbPartyClass.SelectedValue = 1;
            this.txtPartyId.Enabled = false;
            this.cbPartyClass.Enabled = false;

            ////this.chkInvoice.Checked = true;
            ////this.chkOcon.Checked = true;
            ////this.chkIcon.Checked = true;
        }

        private void SetAuthoriser()
        {
            if (this.IsNew || ETRMDataAccess.GetPartyStatus(this.GetPartyId()) != PartyStatus.Authorised)
            {
                this.cbAuthoriser.Visible = false;
                this.lblAuth.Visible = false;
            }
            else
            {
                this.cbAuthoriser.Visible = true;
                this.lblAuth.Visible = true;
                this.cbAuthoriser.Enabled = true;

                Int32 authoriser = ETRMDataAccess.GetPartyAuthoriser(this.GetPartyId());

                this.cbAuthoriser.SelectedValue = authoriser;

                this.cbAuthoriser.Enabled = false;
                this.lblAuth.Enabled = false;
            }
        }

        private void SetUserid()
        {
            try
            {
                this.UserID = ETRMDataAccess.GetSelfID(Environment.UserName);
            }
            catch { }
        }

        private Int32 GetUserId()
        {
            return ETRMDataAccess.GetSelfID(Environment.UserName);
        }

        private void btnPartySave_Click(object sender, EventArgs e)
        {
            if (this.IsFormContentValid())
            {
                Gotham.Domain.Entities.BusinessUnit data = new Domain.Entities.BusinessUnit();

                data.Code = this.txtShortName.Text;
                data.ShortName = this.txtShortName.Text;
                data.LongName = this.txtLongName.Text;
                data.SpiderId = this.txtSpiderId.Text;
                data.PartyClass = Convert.ToInt32(this.cbPartyClass.SelectedValue);
                data.InternalExternal = Convert.ToInt32(this.cbIntExt.SelectedValue);
                data.PartyStatusId = Convert.ToInt32(this.cbStatus.SelectedValue);
                data.LEI = this.txtLEI.Text;
                data.UpdateUser = this.UserID;
                data.CommercialParticipant = this.chkCommercialParticipant.Checked;
                //data.BeaconID = string.Empty;

                if (data.PartyStatusId == PartyStatus.Authorised)
                {
                    data.AuthoriserId = this.GetUserId();
                }
                else
                {
                    data.AuthoriserId = null;
                }

                //data.MICCode = txtMICCode.Text;
                //data.TaxID = txtTaxID.Text;
                //data.ConfirmingPartyID = Convert.ToInt32(this.cbConfirmingParty.SelectedValue);
                //data.DoddFrankTypeID = Convert.ToInt32(this.cbDoddFrankType.SelectedValue);
                //data.DoddFrankReportingParty = Convert.ToInt32(this.cbDoddFrankReportingParty.SelectedValue);
                //data.PrincipalTradingCountry = Convert.ToInt32(this.cbTradingCountry.SelectedValue);
                //data.RegisteredCountry = Convert.ToInt32(this.cbRegisteredCountry.SelectedValue);
                //data.TaxDomicileID = Convert.ToInt32(this.cbTaxDomicile.SelectedValue);
                //data.TaxStatusID = Convert.ToInt32(this.cbTaxStatus.SelectedValue);
                //data.IConReq = this.chkIcon.Checked;
                //data.OConReq = this.chkOcon.Checked;
                //data.InvoiceReq = this.chkInvoice.Checked;

                Int32 result = Int32.MinValue;

                if (this.IsNew)
                {
                    data.PartyId = ETRMDataAccess.MaxPartyId() + 1;
                    result = ETRMDataAccess.SaveBusinessUnit(data);

                    if (result != Int32.MinValue)
                    {
                        this.txtPartyId.Enabled = true;
                        this.txtPartyId.Text = result.ToString();
                        this.txtPartyId.Enabled = false;
                        this.IsNew = false;
                        this.PartyID = result;
                    }
                }
                else
                {
                    data.PartyId = Convert.ToInt32(txtPartyId.Text);
                    result = ETRMDataAccess.UpdateBusinessUnit(data);
                }

                this.SetAuthoriser();

                if (result == Int32.MinValue)
                {
                    MessageBox.Show("Something went wrong with the saving process...");
                    return;
                }

                MessageBox.Show("The details have been saved");
            }
            else
            {
                MessageBox.Show("Please fill all the required fields...");
            }
        }

        private bool IsFormContentValid()
        {
            epParty.Clear();
            bool everythingOK = true;

            if (string.IsNullOrEmpty(txtShortName.Text))
            {
                epParty.SetError(txtShortName, "Short name cannot be empty!");
                everythingOK = false;
            }

            if (string.IsNullOrEmpty(txtLongName.Text))
            {
                epParty.SetError(txtLongName, "Long name cannot be empty!");
                everythingOK = false;
            }

            if (string.IsNullOrEmpty(txtLEI.Text))
            {
                epParty.SetError(txtLEI, "LEI cannot be empty!");
                everythingOK = false;
            }

            if (Convert.ToInt32(this.cbStatus.SelectedValue) == PartyStatus.Authorised)
            {
                if (string.IsNullOrEmpty(txtSpiderId.Text))
                {
                    epParty.SetError(txtSpiderId, "Spider ID cannot be empty!");
                    everythingOK = false;
                }
            }

            return everythingOK;
        }

        private void HideColumns()
        {
            if (!ETRMDataAccess.IsEmpty(this.address))
            {
                this.HideAddressColumns();
            }
            if (!ETRMDataAccess.IsEmpty(this.agreements))
            {
                this.HideAgreementColumns();
            }
            if (!ETRMDataAccess.IsEmpty(this.businessUnits))
            {
                this.HideBusinessUnitsColumns();
            }
        }

        private void HideAddressColumns()
        {
            this.dgvAddress.Columns[Domain.AddressNames.PartyID].Visible = false;
            this.dgvAddress.Columns[Domain.AddressNames.PartyAddressID].Visible = false;
            this.dgvAddress.Columns[Domain.AddressNames.DefaultID].Visible = false;
        }

        private void HideAgreementColumns()
        {
            //this.dgvAgreements.Columns[Agreements.PartyAgreementID].Visible = false;
        }

        private void HideBusinessUnitsColumns()
        {
            this.dgvBusinessUnits.Columns[PartyHI.Party_ID].Visible = false;
        }

        private void FillAddresses()
        {
            int partyId = this.GetPartyId();

            if (partyId != Int32.MinValue)
            {
                address = ETRMDataAccess.GetPartyAddresses(partyId);

                dgvAddress.DataSource = null;

                if (!ETRMDataAccess.IsEmpty(address))
                {
                    dgvAddress.DataSource = address.DefaultView;
                    this.HideAddressColumns();

                    if (address.Rows.Count > 0)
                    {
                        this.btnAddsAdd.Enabled = false;
                    }
                }
                else
                {
                    this.btnAddsAdd.Enabled = true;
                }
            }
        }

        private void FillAgreements()
        {
            int partyId = this.GetPartyId();

            if (partyId != Int32.MinValue)
            {
                agreements = ETRMDataAccess.GetAgreements(partyId);

                dgvAgreements.DataSource = null;

                if (!ETRMDataAccess.IsEmpty(agreements))
                {
                    dgvAgreements.DataSource = agreements.DefaultView;
                    this.HideAgreementColumns();

                    this.dgvAgreements.ClearSelection();

                    if (agreements.Rows.Count > 0)
                    {
                        //Enable Add Edit buttons
                    }
                }
                else
                {
                    //Enable Add button
                }

                if (dgvAgreements.Columns != null)
                {
                    foreach (DataGridViewColumn column in dgvAgreements.Columns)
                    {
                        column.SortMode = DataGridViewColumnSortMode.NotSortable;
                    }
                }
            }
        }

        private void FillBusinessUnits()
        {
            int partyId = this.GetPartyId();

            if (partyId != Int32.MinValue)
            {
                businessUnits = ETRMDataAccess.GetBusinessUnits(partyId);
                dgvBusinessUnits.DataSource = null;

                if (!ETRMDataAccess.IsEmpty(businessUnits))
                {
                    dgvBusinessUnits.DataSource = businessUnits.DefaultView;
                    //this.HideAgreementColumns();

                    if (businessUnits.Rows.Count > 0)
                    {
                        //this.btnAddsAdd.Enabled = false;
                    }
                }
                else
                {
                    //this.btnAddsAdd.Enabled = true;
                }
            }
        }


        private void FillHeaderInfo()
        {
            if (!ETRMDataAccess.IsEmpty(headerInfo))
            {
                this.txtShortName.Text = Convert.ToString(headerInfo.Rows[0][PartyHI.ShortName]);
                this.txtLongName.Text = Convert.ToString(headerInfo.Rows[0][PartyHI.LongName]);
                this.txtSpiderId.Text = Convert.ToString(headerInfo.Rows[0][PartyHI.SpiderId]);
                this.txtPartyId.Text = Convert.ToString(headerInfo.Rows[0][PartyHI.PartyId]);
                this.PartyID = Convert.ToInt32(headerInfo.Rows[0][PartyHI.PartyId]);
                this.txtPartyId.Enabled = false;
                this.txtLEI.Text = Convert.ToString(headerInfo.Rows[0][PartyHI.LEI]);

                this.cbPartyClass.SelectedValue = Convert.ToInt32(headerInfo.Rows[0][PartyHI.PartyClassId]);
                this.cbPartyClass.Enabled = false;

                this.cbStatus.SelectedValue = Convert.ToInt32(headerInfo.Rows[0][PartyHI.PartyStatusId]);
                this.cbIntExt.SelectedValue = Convert.ToInt32(headerInfo.Rows[0][PartyHI.InternalExternalId]);

                bool? commercialParticipant = null;

                if (!string.IsNullOrEmpty(headerInfo.Rows[0][PartyHI.CommercialParticipant].ToString()))
                {
                    commercialParticipant = Convert.ToBoolean(headerInfo.Rows[0][PartyHI.CommercialParticipant]);
                }
                if (commercialParticipant.HasValue) { this.chkCommercialParticipant.Checked = commercialParticipant.Value; }
            }
        }

        private void FillComboBoxes()
        {
            DataTable partyClass = ETRMDataAccess.GetPartyClass();
            this.cbPartyClass.DataSource = partyClass.DefaultView;
            this.cbPartyClass.DisplayMember = PartyClass.Description;
            this.cbPartyClass.ValueMember = PartyClass.PartyClassID;
            this.cbPartyClass.BindingContext = this.BindingContext;

            DataTable partyStatus = ETRMDataAccess.GetPartyStatus();
            this.cbStatus.DataSource = partyStatus.DefaultView;
            this.cbStatus.DisplayMember = PartyHI.PartyStatus;
            this.cbStatus.ValueMember = PartyHI.PartyStatusId;
            this.cbStatus.BindingContext = this.BindingContext;

            DataTable intExt = ETRMDataAccess.GetPartyIntExt();
            this.cbIntExt.DataSource = intExt.DefaultView;
            this.cbIntExt.DisplayMember = PartyHI.InternalExternal;
            this.cbIntExt.ValueMember = PartyHI.InternalExternalId;
            this.cbIntExt.BindingContext = this.BindingContext;

            DataTable authorisers = ETRMDataAccess.GetAuthorisers();
            this.cbAuthoriser.DataSource = authorisers.DefaultView;
            this.cbAuthoriser.DisplayMember = Personnel.UserName;
            this.cbAuthoriser.ValueMember = Personnel.PersonnelID;
            this.cbAuthoriser.BindingContext = this.BindingContext;
        }

        Int32 GetPartyId()
        {
            Int32 result = Int32.MinValue;

            try
            {
                result = Convert.ToInt32(this.txtPartyId.Text);
            }
            catch { }


            return result;
        }

        private void btnAddsAdd_Click(object sender, EventArgs e)
        {
            if (this.PartyID == Int32.MinValue)
            {
                MessageBox.Show(this.GetNonRegistrationWarning());
                return;
            }

            //There is only PartyID
            if (!string.IsNullOrEmpty(txtPartyId.Text))
            {
                PartyAddress padds = new PartyAddress(Convert.ToInt32(txtPartyId.Text), this.UserID);
                padds.SetLegalEntityDefaults();
                padds.ShowDialog();

                this.address = ETRMDataAccess.GetPartyAddresses(Convert.ToInt32(txtPartyId.Text));
                this.FillAddresses();
            }
        }

        private void btnAddEdit_Click(object sender, EventArgs e)
        {
            if (this.dgvAddress.Rows.Count <= 0)
            {
                MessageBox.Show("There are currently no records, please enter the first address...");
                return;
            }

            int currentRow = Int32.MinValue;

            try
            {
                currentRow = this.dgvAddress.CurrentCell.RowIndex;
            }
            catch { }

            if (currentRow >= 0)
            {
                if (!ETRMDataAccess.IsEmpty(this.address))
                {
                    DataRow selectedRow = this.address.Rows[currentRow];

                    if (selectedRow != null)
                    {
                        PartyAddress padds = new PartyAddress(selectedRow, this.UserID);
                        padds.SetLegalEntityDefaults();
                        padds.ShowDialog();

                        this.FillAddresses();
                    }
                }
            }
            else
            {
                MessageBox.Show("Please select a record first...");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tcParty_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btAgreementsModify_Click(object sender, EventArgs e)
        {
            if (this.dgvAgreements.Rows.Count <= 0)
            {
                return;
            }

            int currentRow = Int32.MinValue;

            try
            {
                currentRow = this.dgvAgreements.CurrentCell.RowIndex;
            }
            catch { }

            if (currentRow >= 0)
            {
                if (!ETRMDataAccess.IsEmpty(this.agreements))
                {
                    DataRow selectedRow = this.agreements.Rows[currentRow];

                    if (selectedRow != null)
                    {
                        Int32 selectedAgreementId = Convert.ToInt32(this.agreements.Rows[currentRow][Agreements.PartyAgreementID]);

                        //LEAgreements agreement = new LEAgreements(this.PartyID, selectedAgreementId);
                        AgreementsEditor agreement = new AgreementsEditor(this.PartyID, selectedAgreementId);
                        agreement.ShowDialog();

                        this.FillAgreements();
                    }
                }
            }
            else
            {
                MessageBox.Show("Please select a record first...");
            }
        }

        private void btAgreementsAdd_Click(object sender, EventArgs e)
        {
            if (this.PartyID == Int32.MinValue)
            {
                MessageBox.Show(this.GetNonRegistrationWarning());
                return;
            }

            //Agreements agreement = new Agreements(this.PartyID);
            AgreementsEditor agreement = new AgreementsEditor(this.PartyID);
            agreement.ShowDialog();

            this.FillAgreements();
        }

        private string GetNonRegistrationWarning()
        {
            string warningMessage = "You cannot save legal entity details before having registered the entity first";
            warningMessage += Environment.NewLine;
            warningMessage += "Please save first the main information and then proceed in filling the details...";
            return warningMessage;
        }

        private void btnAddressDelete_Click(object sender, EventArgs e)
        {
            if (this.dgvAddress.Rows.Count <= 0)
            {
                return;
            }

            int currentRow = Int32.MinValue;

            try
            {
                currentRow = this.dgvAddress.CurrentCell.RowIndex;
            }
            catch { }

            if (currentRow >= 0)
            {
                if (!ETRMDataAccess.IsEmpty(this.address))
                {
                    DataRow selectedRow = this.address.Rows[currentRow];

                    if (selectedRow != null)
                    {
                        DialogResult confirmation = MessageBox.Show(MessageWarning.AreYouSureDeletion, "Address Deletion", MessageBoxButtons.YesNo);

                        if (confirmation == DialogResult.Yes)
                        {
                            Int32 addressID = Convert.ToInt32(selectedRow[Domain.AddressNames.PartyAddressID]);
                            Int32 deletionResult = ETRMDataAccess.DeleteAddress(addressID);
                        }
                    }
                }
                this.FillAddresses();
            }
            else
            {
                MessageBox.Show("Please select a record first...");
            }
        }

        private void btnAgreementsDelete_Click(object sender, EventArgs e)
        {
            if (this.dgvAgreements.Rows.Count <= 0)
            {
                return;
            }

            int currentRow = Int32.MinValue;

            try
            {
                currentRow = this.dgvAgreements.CurrentCell.RowIndex;
            }
            catch { }

            if (currentRow >= 0)
            {
                if (!ETRMDataAccess.IsEmpty(this.agreements))
                {
                    DataRow selectedRow = this.agreements.Rows[currentRow];

                    if (selectedRow != null)
                    {
                        DialogResult confirmation = MessageBox.Show(MessageWarning.AreYouSureDeletion, "Agreement Deletion", MessageBoxButtons.YesNo);

                        if (confirmation == DialogResult.Yes)
                        {
                            Int32 agreementID = Convert.ToInt32(selectedRow[Domain.Agreements.PartyAgreementID]);
                            Int32 deletionResult = ETRMDataAccess.DeleteAgreement(agreementID);
                        }
                    }
                }
                this.FillAgreements();
            }
            else
            {
                MessageBox.Show("Please select a record first...");
            }
        }
    }
}
