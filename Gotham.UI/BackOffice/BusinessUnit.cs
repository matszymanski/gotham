﻿using Strazze.Data;
using Strazze.Domain;
using Strazze.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Strazze.AddIn.Forms
{
    public partial class BusinessUnit : Form
    {
        ErrorProvider epBusinessUnit;
        public Int32 PartyId { get; set; }
        public Int32 ParentId { get; set; }
        public DataTable headerInfo { get; set; }
        public DataTable address { get; set; }
        public DataTable businessUnits { get; set; }
        public DataTable agreements { get; set; }
        public DataTable contacts { get; set; }
        public DataTable linkedPortfolios { get; set; }
        public DataTable functions { get; set; }
        public DataTable linkedfunctions { get; set; }

        public DataTable allfunctions;
        public Int32 UserID { get; set; }

        private DataTable allPortfolios;
        private DataTable PartyBronkerRates { get; set; }
        BusinessUnitComponents buComp { get; set; }
        public bool IsNew { get; set; }
        public BusinessUnit(bool canWrite)
        {
            InitializeComponent();
            this.InitialiseView();
            this.IsNew = true;
            this.FillComboBoxes();
            this.ActivateNewEntrySettings();
            this.SetSecurity(canWrite);
        }

        public BusinessUnit(Int32 partyId, bool canWrite)
        {
            InitializeComponent();

            this.PartyId = partyId;

            this.FillComboBoxes();

            this.headerInfo = ETRMDataAccess.GetParty(partyId);
            this.address = ETRMDataAccess.GetPartyAddresses(partyId);
            this.contacts = ETRMDataAccess.GetPartyContacts(partyId);
            this.functions = ETRMDataAccess.GetPartyFunction();
            this.PartyBronkerRates = ETRMDataAccess.GetPartyBrokerRates(partyId);
            this.ParentId = ETRMDataAccess.GetParentCompany(partyId);

            this.InitialiseView();
            this.SetSecurity(canWrite);
        }

        //////public BusinessUnit(Int32 partyId, DataTable header, DataTable addressesData, DataTable contactsData, DataTable f, DataTable pbr, Int32 parentId, bool canWrite)
        //////{
        //////    InitializeComponent();

        //////    this.PartyId = partyId;

        //////    this.FillComboBoxes();

        //////    this.headerInfo = header;
        //////    this.address = addressesData;
        //////    this.contacts = contactsData;
        //////    this.functions = f;
        //////    this.PartyBronkerRates = pbr;
        //////    this.ParentId = parentId;
        //////    this.InitialiseView();
        //////    this.SetSecurity(canWrite);
        //////}

        private void InitialiseView()
        {
            this.FillHeaderInfo();
            this.FillAddresses();
            this.FillContacts();
            this.FillFunctions();
            this.FillPortfolios();
            this.FillBrokerRates();
            this.FillInvoiceMapping();

            this.HideColumns();
            this.SetAuthoriser();
            this.SetTabVisibility();
            this.SetUserid();

            this.epBusinessUnit = new ErrorProvider();
        }

        private void SetSecurity(bool canWrite)
        {
            this.btnBUAddressAdd.Enabled = canWrite;
            this.btnBUAddressEdit.Enabled = canWrite;
            this.btnAddressDelete.Enabled = canWrite;
            this.btnBUContactsAdd.Enabled = canWrite;
            this.btnBUContactsEdit.Enabled = canWrite;
            this.btnDeleteContacts.Enabled = canWrite;
            this.btnBUFunctionsSave.Enabled = canWrite;
            this.btnBUPortfolioSave.Enabled = canWrite;
            this.btBUBrokRatesAdd.Enabled = canWrite;
            this.btnBUBrokerRatesEdit.Enabled = canWrite;
            this.btnBrokerRatesDelete.Enabled = canWrite;
            this.btnSave.Enabled = canWrite;
        }

        private void SetTabVisibility()
        {
            this.txtBrokerWarning.Visible = false;

            Int32 internalExternal = Convert.ToInt32(this.cbBUIntExt.SelectedValue);

            if (!this.IsNew && this.PartyId != -1)
            {
                bool isBroker = ETRMDataAccess.IsBroker(this.PartyId);

                if (!isBroker)
                {
                    foreach (TabPage tp in this.tcBusinessUnit.TabPages)
                    {
                        if (tp.Name == "tpBUBrokerRates")
                        {
                            ((Control)tp).Enabled = false;
                        }
                    }

                    if (this.dgvBUBrokerRates.Rows.Count > 0)
                    {
                        this.txtBrokerWarning.Visible = true;
                        this.txtBrokerWarning.BackColor = Color.Red;
                        this.txtBrokerWarning.ForeColor = Color.Black;
                        this.txtBrokerWarning.Enabled = true;
                    }
                }
                else
                {
                    foreach (TabPage tp in this.tcBusinessUnit.TabPages)
                    {
                        if (tp.Name == "tpBUBrokerRates")
                        {
                            ((Control)tp).Enabled = true;
                        }
                    }
                }

                if (internalExternal == PartyClass.External)
                {
                    foreach (TabPage tp in this.tcBusinessUnit.TabPages)
                    {
                        if (tp.Name == "tpBUPortfolios")
                        {
                            ((Control)tp).Enabled = false;
                        }
                    }
                }
            }
        }

        private void ActivateNewEntrySettings()
        {
            this.cbBUPartyClass.SelectedValue = 2;
            this.txtBUPartyId.Enabled = false;
            this.cbBUPartyClass.Enabled = false;

            this.chkInvoice.Checked = true;
            this.chkOcon.Checked = true;
            this.chkIcon.Checked = true;
            this.txtMICCode.Text = "AXPO";
        }

        private void SetAuthoriser()
        {
            if (this.IsNew || ETRMDataAccess.GetPartyStatus(this.GetPartyId()) != PartyStatus.Authorised)
            {
                this.cbAuthoriser.Visible = false;
                this.lblAuth.Visible = false;
            }
            else
            {
                this.cbAuthoriser.Visible = true;
                this.lblAuth.Visible = true;
                this.cbAuthoriser.Enabled = true;

                Int32 authoriser = ETRMDataAccess.GetPartyAuthoriser(this.GetPartyId());

                this.cbAuthoriser.SelectedValue = authoriser;

                this.cbAuthoriser.Enabled = false;
                this.lblAuth.Enabled = false;
            }
        }

        private void SetUserid()
        {
            try
            {
                this.UserID = ETRMDataAccess.GetSelfID(Environment.UserName);
            }
            catch { }
        }

        private Int32 GetUserId()
        {
            return ETRMDataAccess.GetSelfID(Environment.UserName);
        }

        private bool IsFormContentValid()
        {
            epBusinessUnit.Clear();
            bool everythingOK = true;

            if (string.IsNullOrEmpty(txtBUShortName.Text))
            {
                epBusinessUnit.SetError(txtBUShortName, "Short name cannot be empty!");
                everythingOK = false;
            }

            if (string.IsNullOrEmpty(txtBULegalName.Text))
            {
                epBusinessUnit.SetError(txtBULegalName, "Legal name cannot be empty!");
                everythingOK = false;
            }

            if (Convert.ToInt32(this.cbBUStatus.SelectedValue) == PartyStatus.Authorised)
            {
                if (string.IsNullOrEmpty(txtBUGlobalId.Text))
                {
                    epBusinessUnit.SetError(txtBUGlobalId, "Spider ID cannot be empty!");
                    everythingOK = false;
                }
            }

            if (this.cbBUParentGroup.SelectedValue == null)
            {
                epBusinessUnit.SetError(cbBUParentGroup, "Please associate a parent company!");
                everythingOK = false;
            }

            return everythingOK;
        }

        private void btnPartySave_Click(object sender, EventArgs e)
        {
            if (this.IsFormContentValid())
            {
                Strazze.Domain.Entities.BusinessUnit data = new Domain.Entities.BusinessUnit();
                data.Code = this.txtBUShortName.Text;
                data.LEI = string.Empty;
                data.ShortName = this.txtBUShortName.Text;
                data.LongName = this.txtBULegalName.Text;
                data.SpiderId = this.txtBUGlobalId.Text;
                data.BeaconID = this.txtBeaconID.Text;
                data.PartyClass = Convert.ToInt32(this.cbBUPartyClass.SelectedValue);
                data.InternalExternal = Convert.ToInt32(this.cbBUIntExt.SelectedValue);
                data.PartyStatusId = Convert.ToInt32(this.cbBUStatus.SelectedValue);
                data.UpdateUser = this.UserID;

                data.MICCode = txtMICCode.Text;
                data.TaxID = txtTaxID.Text;
                data.ConfirmingPartyID = Convert.ToInt32(this.cbConfirmingParty.SelectedValue);
                data.DoddFrankTypeID = Convert.ToInt32(this.cbDoddFrankType.SelectedValue);
                data.DoddFrankReportingParty = Convert.ToInt32(this.cbDoddFrankReportingParty.SelectedValue);
                data.PrincipalTradingCountry = Convert.ToInt32(this.cbTradingCountry.SelectedValue);
                data.RegisteredCountry = Convert.ToInt32(this.cbRegisteredCountry.SelectedValue);
                data.TaxDomicileID = Convert.ToInt32(this.cbTaxDomicile.SelectedValue);
                data.TaxStatusID = Convert.ToInt32(this.cbTaxStatus.SelectedValue);
                data.ConfirmMethodID = Convert.ToInt32(this.cbConfirmRoute.SelectedValue);
                data.IConReq = this.chkIcon.Checked;
                data.OConReq = this.chkOcon.Checked;
                data.InvoiceReq = this.chkInvoice.Checked;

                if(!string.IsNullOrEmpty(this.txtSchedulingName.Text))
                {
                    data.SchedulingPartyName = this.txtSchedulingName.Text;
                }
                else
                {
                    data.SchedulingPartyName = string.Empty;
                }

                if (data.PartyStatusId == PartyStatus.Authorised)
                {
                    data.AuthoriserId = this.GetUserId();
                }
                else
                {
                    data.AuthoriserId = null;
                }

                if (this.IsNew)
                {
                    data.PartyId = ETRMDataAccess.MaxPartyId() + 1;
                    Int32 result = ETRMDataAccess.SaveBusinessUnit(data);

                    if (result != Int32.MinValue)
                    {
                        this.txtBUPartyId.Enabled = true;
                        this.txtBUPartyId.Text = result.ToString();
                        this.txtBUPartyId.Enabled = false;
                        this.IsNew = false;
                    }
                    else
                    {
                        MessageBox.Show("Something went wrong with the saving process...");
                        return;
                    }

                    //Saving parent company ID
                    Int32 parentCompanyId = Convert.ToInt32(this.cbBUParentGroup.SelectedValue);
                    Int32 saved = ETRMDataAccess.SaveParentId(parentCompanyId, data.PartyId);
                }
                else
                {
                    data.PartyId = Convert.ToInt32(this.txtBUPartyId.Text);

                    Int32 result = ETRMDataAccess.UpdateBusinessUnit(data);
                    if (result == Int32.MinValue)
                    {
                        MessageBox.Show("Something went wrong with the saving process...");
                        return;
                    }

                    //Saving parent company ID
                    Int32 parentCompanyId = Convert.ToInt32(this.cbBUParentGroup.SelectedValue);
                    Int32 currentParent = ETRMDataAccess.GetParentCompany(data.PartyId);

                    if (currentParent == Int32.MinValue)
                    {
                        Int32 saved = ETRMDataAccess.SaveParentId(parentCompanyId, data.PartyId);
                    }
                    else
                    {
                        Int32 saved = ETRMDataAccess.UpdateParentId(parentCompanyId, data.PartyId);
                    }
                }
                this.FillFunctions();

                this.SetAuthoriser();

                MessageBox.Show("Update completed");
            }
            else
            {
                MessageBox.Show("Please fill all the required fields...");
            }
        }

        private void HideColumns()
        {
            if (!ETRMDataAccess.IsEmpty(this.address))
            {
                this.HideAddressColumns();
            }
        }

        private void FillParentCoComboBox()
        {
            Int32 intExt = Int32.MinValue;

            if (this.cbBUIntExt.SelectedValue != null)
            {
                intExt = Convert.ToInt32(this.cbBUIntExt.SelectedValue);
                DataTable parents = ETRMDataAccess.GetParentCompanies(intExt);

                this.cbBUParentGroup.DataSource = parents.DefaultView;
                this.cbBUParentGroup.DisplayMember = PartyHI.ShortName;
                this.cbBUParentGroup.ValueMember = PartyHI.PartyId;
                this.cbBUParentGroup.BindingContext = this.BindingContext;
            }

            if (this.ParentId != Int32.MinValue)
            {
                this.cbBUParentGroup.SelectedValue = this.ParentId;
            }
        }

        private void HideFunctionsColumns()
        {
            try
            {
                this.dgvBUFunctions.Columns[Function.PartyFunctionTypeID].Visible = false;
            }
            catch { }
        }

        private void HidePortfoliosColumns()
        {
            this.dgvBUPortfolio.Columns[Domain.Portfolio.PortfolioID].Visible = false;
        }

        private void HideAddressColumns()
        {
            this.dgvBUAddress.Columns[Domain.Address.PartyID].Visible = false;
            this.dgvBUAddress.Columns[Domain.Address.PartyAddressID].Visible = false;
            this.dgvBUAddress.Columns[Domain.Address.DefaultID].Visible = false;
        }

        private void HideContactsColumns()
        {
            this.dgvBUContacts.Columns[Domain.Contact.PartyID].Visible = false;
            this.dgvBUContacts.Columns[Domain.Contact.PartyContactID].Visible = false;
            this.dgvBUContacts.Columns[Domain.Contact.DefaultID].Visible = false;
            this.dgvBUContacts.Columns[Domain.Contact.PartyTitleID].Visible = false;
        }


        private void FillFunctionsR2()
        {
            if (!string.IsNullOrEmpty(txtBUPartyId.Text))
            {

                DataTable partyFunctions = ETRMDataAccess.GetPartyFunctions(Convert.ToInt32(txtBUPartyId.Text));
                this.functions = partyFunctions;

                if (!ETRMDataAccess.IsEmpty(functions))
                {
                    dgvBUFunctions.DataSource = null;
                    dgvBUFunctions.DataSource = functions.DefaultView;

                    this.HideFunctionsColumns();
                }
            }
        }

        private void FillFunctions()
        {
            if (!string.IsNullOrEmpty(txtBUPartyId.Text))
            {
                this.linkedfunctions = ETRMDataAccess.GetPartyFunctions(Convert.ToInt32(txtBUPartyId.Text));
                this.allfunctions = ETRMDataAccess.GetAllFunctions();
                this.allfunctions.Columns.Add(new DataColumn(Function.Associated, typeof(bool)));

                if (!ETRMDataAccess.IsEmpty(this.linkedfunctions))
                {
                    this.AssociateFunctions(this.allfunctions, this.linkedfunctions);
                }

                if (!ETRMDataAccess.IsEmpty(this.allfunctions))
                {
                    dgvBUFunctions.DataSource = null;

                    DataView sortedView = this.allfunctions.DefaultView;
                    sortedView.Sort = "ASSOCIATED DESC, PartyFunctionType";
                    DataTable sorted = sortedView.ToTable();

                    dgvBUFunctions.DataSource = sorted.DefaultView;
                    dgvBUFunctions.ReadOnly = false;
                    dgvBUFunctions.Columns[Function.Associated].ReadOnly = false;
                    dgvBUFunctions.Columns[Function.PartyFunctionType].ReadOnly = true;

                }
                this.HideFunctionsColumns();
            }
        }

        private void FillPortfolios()
        {
            this.allPortfolios = ETRMDataAccess.GetAllPortfolios();
            DataTable associatedPortfolios = ETRMDataAccess.GetAssociatedPortfolios(this.GetPartyId());
            this.allPortfolios.Columns.Add(new DataColumn(Domain.Portfolio.Associated, typeof(bool)));

            if (!ETRMDataAccess.IsEmpty(associatedPortfolios))
            {
                this.AssociatePortfolios(this.allPortfolios, associatedPortfolios);
            }

            if (!ETRMDataAccess.IsEmpty(this.allPortfolios))
            {
                this.dgvBUPortfolio.DataSource = null;

                DataView sortedView = this.allPortfolios.DefaultView;
                sortedView.Sort = "ASSOCIATED DESC, Portfolio";
                DataTable sorted = sortedView.ToTable();

                this.dgvBUPortfolio.DataSource = sorted.DefaultView;
                this.dgvBUPortfolio.ReadOnly = false;
                this.dgvBUPortfolio.Columns[Domain.Portfolio.Associated].ReadOnly = false;
                this.dgvBUPortfolio.Columns[Domain.Portfolio.PortfolioName].ReadOnly = true;
            }

            this.HidePortfoliosColumns();
        }

        private void FillPartyInfoComboBoxes()
        {
            DataTable doddFrankType = ETRMDataAccess.GetDoddFrankType();
            this.cbDoddFrankType.DataSource = doddFrankType.DefaultView;
            this.cbDoddFrankType.DisplayMember = PartyHI.DoddFrankType;
            this.cbDoddFrankType.ValueMember = PartyHI.PartyDoddFrankTypeID;
            this.cbDoddFrankType.BindingContext = this.BindingContext;
            this.cbDoddFrankType.SelectedValue = PartyHI.PartyDoddFrankTypeUnk;

            DataTable doddFrankParty = ETRMDataAccess.GetDoddFrankParty();
            this.cbDoddFrankReportingParty.DataSource = doddFrankParty.DefaultView;
            this.cbDoddFrankReportingParty.DisplayMember = PartyHI.PartyDoddFrankReportingParty;
            this.cbDoddFrankReportingParty.ValueMember = PartyHI.PartyDoddFrankReportingPartyId;
            this.cbDoddFrankReportingParty.BindingContext = this.BindingContext;
            this.cbDoddFrankReportingParty.SelectedValue = PartyHI.PartyDoddFrankReportingPartyUnk;

            DataTable taxExemptStatus = ETRMDataAccess.GetTaxExemptStatus();
            this.cbTaxStatus.DataSource = taxExemptStatus.DefaultView;
            this.cbTaxStatus.ValueMember = PartyHI.TaxExemptID;
            this.cbTaxStatus.DisplayMember = PartyHI.Name;
            this.cbTaxStatus.BindingContext = this.BindingContext;
            this.cbTaxStatus.SelectedValue = PartyHI.TaxExemptStatusUnk;

            DataTable taxDom = ETRMDataAccess.GetTaxDomicile();
            this.cbTaxDomicile.DataSource = taxDom.DefaultView;
            this.cbTaxDomicile.ValueMember = PartyHI.TaxDomicileID;
            this.cbTaxDomicile.DisplayMember = PartyHI.TaxDomicile;
            this.cbTaxDomicile.BindingContext = this.BindingContext;
            this.cbTaxDomicile.SelectedValue = PartyHI.TaxDomicileUnk;

            DataTable tradingCountry = ETRMDataAccess.GetCountry();
            this.cbTradingCountry.DataSource = tradingCountry.DefaultView;
            this.cbTradingCountry.ValueMember = PartyHI.CountryID;
            this.cbTradingCountry.DisplayMember = PartyHI.ShortName;
            this.cbTradingCountry.BindingContext = this.BindingContext;
            this.cbTradingCountry.SelectedValue = PartyHI.USA;

            DataTable registeredCountry = ETRMDataAccess.GetCountry();
            this.cbRegisteredCountry.DataSource = registeredCountry.DefaultView;
            this.cbRegisteredCountry.ValueMember = PartyHI.CountryID;
            this.cbRegisteredCountry.DisplayMember = PartyHI.ShortName;
            this.cbRegisteredCountry.BindingContext = this.BindingContext;
            this.cbRegisteredCountry.SelectedValue = PartyHI.USA;

            DataTable confirmingParty = ETRMDataAccess.GetConfirmingParty();
            this.cbConfirmingParty.DataSource = confirmingParty.DefaultView;
            this.cbConfirmingParty.ValueMember = PartyHI.ConfirmingPartyID;
            this.cbConfirmingParty.DisplayMember = PartyHI.ConfirmingParty;
            this.cbConfirmingParty.BindingContext = this.BindingContext;

            DataTable confirmRoute = ETRMDataAccess.GetConfirmingMethod();
            this.cbConfirmRoute.DataSource = confirmRoute.DefaultView;
            this.cbConfirmRoute.ValueMember = PartyHI.ConfirmMethodID;
            this.cbConfirmRoute.DisplayMember = PartyHI.ConfirmMethod;
            this.cbConfirmRoute.BindingContext = this.BindingContext;
        }

        private void HideBrokerRateColumns()
        {
            this.dgvBUBrokerRates.Columns[Domain.BrokerRates.BrokerID].Visible = false;
            this.dgvBUBrokerRates.Columns[Domain.BrokerRates.PartyID].Visible = false;
            this.dgvBUBrokerRates.Columns[Domain.BrokerRates.PartyBrokerRateID].Visible = false;
            this.dgvBUBrokerRates.Columns[Domain.BrokerRates.FeeTypeID].Visible = false;
            this.dgvBUBrokerRates.Columns[Domain.BrokerRates.ExchangeID].Visible = false;
            this.dgvBUBrokerRates.Columns[Domain.BrokerRates.ExecutionTypeID].Visible = false;
            this.dgvBUBrokerRates.Columns[Domain.BrokerRates.UnderlyingID].Visible = false;
            this.dgvBUBrokerRates.Columns[Domain.BrokerRates.SettlementTypeID].Visible = false;
            this.dgvBUBrokerRates.Columns[Domain.BrokerRates.BrokerRateTypeID].Visible = false;
            this.dgvBUBrokerRates.Columns[Domain.BrokerRates.RateCurrencyID].Visible = false;
            this.dgvBUBrokerRates.Columns[Domain.BrokerRates.PaymentFrequencyID].Visible = false;
            this.dgvBUBrokerRates.Columns[Domain.BrokerRates.BrokerRateTypeID].Visible = false;
            this.DisableSorting();
        }

        private void DisableSorting()
        {
            foreach (DataGridViewColumn column in dgvBUBrokerRates.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }

        private void AssociatePortfolios(DataTable allPortfolios, DataTable associatedPortfolios)
        {
            foreach (DataRow row in this.allPortfolios.Rows)
            {
                var results = from myRow in associatedPortfolios.AsEnumerable()
                              where myRow.Field<int>(Domain.Portfolio.PortfolioID) == Convert.ToInt32(row[Domain.Portfolio.PortfolioID])
                              select myRow;

                if (results.Count() == 0)
                {
                    row[Domain.Portfolio.Associated] = false;
                }
                else
                {
                    row[Domain.Portfolio.Associated] = true;
                }
            }
        }

        private void AssociateFunctions(DataTable allFunctions, DataTable associatedFunctions)
        {
            foreach (DataRow row in allFunctions.Rows)
            {
                var results = from myRow in associatedFunctions.AsEnumerable()
                              where myRow.Field<int>(Function.PartyFunctionTypeID) == Convert.ToInt32(row[Function.PartyFunctionTypeID])
                              select myRow;

                if (results.Count() == 0)
                {
                    row[Function.Associated] = false;
                }
                else
                {
                    row[Function.Associated] = true;
                }
            }
        }

        private void FillAddresses()
        {
            int partyId = this.GetPartyId();

            if (partyId != Int32.MinValue)
            {
                this.address = ETRMDataAccess.GetPartyAddresses(partyId);
                dgvBUAddress.DataSource = null;

                if (!ETRMDataAccess.IsEmpty(address))
                {
                    dgvBUAddress.DataSource = address.DefaultView;
                    this.HideAddressColumns();
                }
            }
        }

        private void FillContacts()
        {
            int partyId = this.GetPartyId();

            if (partyId != Int32.MinValue)
            {
                this.contacts = ETRMDataAccess.GetPartyContacts(partyId);
                dgvBUContacts.DataSource = null;

                if (!ETRMDataAccess.IsEmpty(contacts))
                {
                    dgvBUContacts.DataSource = contacts.DefaultView;
                    this.HideContactsColumns();
                }
            }
        }

        private void FillBrokerRates()
        {
            int partyId = this.GetPartyId();

            if (partyId != Int32.MinValue)
            {
                this.PartyBronkerRates = ETRMDataAccess.GetPartyBrokerRates(partyId);
                this.dgvBUBrokerRates.DataSource = null;

                if (!ETRMDataAccess.IsEmpty(this.PartyBronkerRates))
                {
                    this.dgvBUBrokerRates.DataSource = this.PartyBronkerRates.DefaultView;
                    this.dgvBUBrokerRates.ReadOnly = true;
                    this.HideBrokerRateColumns();
                }
            }
        }

        private void FillHeaderInfo()
        {
            if (!ETRMDataAccess.IsEmpty(headerInfo))
            {
                this.txtBUShortName.Text = Convert.ToString(headerInfo.Rows[0][PartyHI.ShortName]);
                this.txtBULegalName.Text = Convert.ToString(headerInfo.Rows[0][PartyHI.LongName]);
                this.txtBUGlobalId.Text = Convert.ToString(headerInfo.Rows[0][PartyHI.SpiderId]);
                this.txtBUPartyId.Text = Convert.ToString(headerInfo.Rows[0][PartyHI.PartyId]);
                this.txtBeaconID.Text = Convert.ToString(headerInfo.Rows[0][PartyHI.BeaconID]);
                this.txtSchedulingName.Text = Convert.ToString(headerInfo.Rows[0][PartyHI.SchedulingPartyName]);
                this.txtBUPartyId.Enabled = false;

                this.cbBUPartyClass.SelectedValue = Convert.ToInt32(headerInfo.Rows[0][PartyHI.PartyClassId]);
                this.cbBUPartyClass.Enabled = false;

                this.cbBUStatus.SelectedValue = Convert.ToInt32(headerInfo.Rows[0][PartyHI.PartyStatusId]);
                this.cbBUIntExt.SelectedValue = Convert.ToInt32(headerInfo.Rows[0][PartyHI.InternalExternalId]);

                if (this.ParentId != Int32.MinValue)
                {
                    this.cbBUParentGroup.SelectedValue = this.ParentId;
                }

                this.txtMICCode.Text = ETRMDataAccess.GetStringDTRElement(headerInfo, PartyHI.MICCode);
                this.txtTaxID.Text = ETRMDataAccess.GetStringDTRElement(headerInfo, PartyHI.TaxId);

                if (ETRMDataAccess.GetBoolDTRElement(headerInfo, PartyHI.IConRequiredId).HasValue)
                {
                    this.chkIcon.Checked = ETRMDataAccess.GetBoolDTRElement(headerInfo, PartyHI.IConRequiredId).Value;
                }
                if (ETRMDataAccess.GetBoolDTRElement(headerInfo, PartyHI.OConRequiredId).HasValue)
                {
                    this.chkOcon.Checked = ETRMDataAccess.GetBoolDTRElement(headerInfo, PartyHI.OConRequiredId).Value;
                }
                if (ETRMDataAccess.GetBoolDTRElement(headerInfo, PartyHI.InvoiceRequiredId).HasValue)
                {
                    this.chkInvoice.Checked = ETRMDataAccess.GetBoolDTRElement(headerInfo, PartyHI.InvoiceRequiredId).Value;
                }

                if (ETRMDataAccess.GetInt32DTRElement(headerInfo, PartyHI.DoddFrankTypeId).HasValue)
                {
                    this.cbDoddFrankType.SelectedValue = ETRMDataAccess.GetInt32DTRElement(headerInfo, PartyHI.DoddFrankTypeId).Value;
                }

                if (ETRMDataAccess.GetInt32DTRElement(headerInfo, PartyHI.DoddFrankReportingPartyId).HasValue)
                {
                    this.cbDoddFrankReportingParty.SelectedValue = ETRMDataAccess.GetInt32DTRElement(headerInfo, PartyHI.DoddFrankReportingPartyId).Value;
                }

                if (ETRMDataAccess.GetInt32DTRElement(headerInfo, PartyHI.TaxExemptStatusId).HasValue)
                {
                    this.cbTaxStatus.SelectedValue = ETRMDataAccess.GetInt32DTRElement(headerInfo, PartyHI.TaxExemptStatusId).Value;
                }

                if (ETRMDataAccess.GetInt32DTRElement(headerInfo, PartyHI.TaxDomicileID).HasValue)
                {
                    this.cbTaxDomicile.SelectedValue = ETRMDataAccess.GetInt32DTRElement(headerInfo, PartyHI.TaxDomicileID).Value;
                }

                if (ETRMDataAccess.GetInt32DTRElement(headerInfo, PartyHI.PrincipalTradingCountry).HasValue)
                {
                    this.cbTradingCountry.SelectedValue = ETRMDataAccess.GetInt32DTRElement(headerInfo, PartyHI.PrincipalTradingCountry).Value;
                }

                if (ETRMDataAccess.GetInt32DTRElement(headerInfo, PartyHI.RegisteredCountry).HasValue)
                {
                    this.cbRegisteredCountry.SelectedValue = ETRMDataAccess.GetInt32DTRElement(headerInfo, PartyHI.RegisteredCountry).Value;
                }

                if (ETRMDataAccess.GetInt32DTRElement(headerInfo, PartyHI.ConfirmingPartyID).HasValue)
                {
                    this.cbConfirmingParty.SelectedValue = ETRMDataAccess.GetInt32DTRElement(headerInfo, PartyHI.ConfirmingPartyID).Value;
                }

                if(ETRMDataAccess.GetInt32DTRElement(headerInfo, PartyHI.ConfirmMethodID).HasValue)
                {
                    this.cbConfirmRoute.SelectedValue = ETRMDataAccess.GetInt32DTRElement(headerInfo, PartyHI.ConfirmMethodID).Value;
                }
            }
        }

        private void FillComboBoxes()
        {
            DataTable partyClass = ETRMDataAccess.GetPartyClass();
            this.cbBUPartyClass.DataSource = partyClass.DefaultView;
            this.cbBUPartyClass.DisplayMember = PartyClass.Description;
            this.cbBUPartyClass.ValueMember = PartyClass.PartyClassID;
            this.cbBUPartyClass.BindingContext = this.BindingContext;

            DataTable partyStatus = ETRMDataAccess.GetPartyStatus();
            this.cbBUStatus.DataSource = partyStatus.DefaultView;
            this.cbBUStatus.DisplayMember = PartyHI.PartyStatus;
            this.cbBUStatus.ValueMember = PartyHI.PartyStatusId;
            this.cbBUStatus.BindingContext = this.BindingContext;

            DataTable intExt = ETRMDataAccess.GetPartyIntExt();
            this.cbBUIntExt.DataSource = intExt.DefaultView;
            this.cbBUIntExt.DisplayMember = PartyHI.InternalExternal;
            this.cbBUIntExt.ValueMember = PartyHI.InternalExternalId;
            this.cbBUIntExt.BindingContext = this.BindingContext;

            DataTable authorisers = ETRMDataAccess.GetAuthorisers();
            this.cbAuthoriser.DataSource = authorisers.DefaultView;
            this.cbAuthoriser.DisplayMember = Personnel.UserName;
            this.cbAuthoriser.ValueMember = Personnel.PersonnelID;
            this.cbAuthoriser.BindingContext = this.BindingContext;

            this.FillParentCoComboBox();

            this.FillPartyInfoComboBoxes();
        }

        private void btnAddsAdd_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtBUPartyId.Text))
            {
                PartyAddress padds = new PartyAddress(Convert.ToInt32(txtBUPartyId.Text), this.UserID);
                padds.ShowDialog();

                this.FillAddresses();
            }
        }

        private void btnBUAddressAdd_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtBUPartyId.Text))
            {
                PartyAddress padds = new PartyAddress(Convert.ToInt32(txtBUPartyId.Text), this.UserID);
                padds.ShowDialog();

                this.FillAddresses();
            }
            else
            {
                MessageBox.Show("Please register the business unit first but filling the header information and save them to the database.");
            }
        }

        private void btnBUAddressEdit_Click(object sender, EventArgs e)
        {
            if (this.dgvBUAddress.Rows.Count <= 0)
            {
                MessageBox.Show("There are currently no records to edit...");
                return;
            }

            int currentRow = Int32.MinValue;

            try
            {
                currentRow = this.dgvBUAddress.CurrentCell.RowIndex;
            }
            catch { }

            if (currentRow >= 0)
            {
                if (!ETRMDataAccess.IsEmpty(this.address))
                {
                    DataRow selectedRow = this.address.Rows[currentRow];

                    if (selectedRow != null)
                    {
                        PartyAddress padds = new PartyAddress(selectedRow, this.UserID);
                        padds.ShowDialog();

                        this.FillAddresses();
                    }
                }
            }
            else
            {
                MessageBox.Show("Please select a record first...");
            }
        }

        private void btnBUContactsAdd_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtBUPartyId.Text))
            {
                BUContact buContact = new BUContact(Convert.ToInt32(txtBUPartyId.Text), this.UserID);
                buContact.ShowDialog();
                this.FillContacts();
            }
            else
            {
                MessageBox.Show("Please register the business unit first but filling the header information and save them to the database.");
            }
        }

        private void btnBUContactsEdit_Click(object sender, EventArgs e)
        {
            if (this.dgvBUContacts.Rows.Count <= 0)
            {
                MessageBox.Show("There are currently no records to edit...");
                return;
            }

            int currentRow = Int32.MinValue;

            try
            {
                currentRow = this.dgvBUContacts.CurrentCell.RowIndex;
            }
            catch { }

            if (currentRow >= 0)
            {
                if (!ETRMDataAccess.IsEmpty(this.contacts))
                {
                    DataRow selectedRow = this.contacts.Rows[currentRow];

                    if (selectedRow != null)
                    {
                        BUContact buc = new BUContact(selectedRow, this.UserID);
                        buc.ShowDialog();
                        this.FillContacts();
                    }
                }
            }
        }

        private void btnBUFunctionsAdd_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtBUPartyId.Text))
            {
                Int32 partyId = GetPartyId();
                if (partyId != Int32.MinValue)
                {
                    BUFunctions function = new BUFunctions(partyId);
                    function.ShowDialog();
                    this.FillFunctions();
                }
            }
        }

        private void btnFncRemove_Click(object sender, EventArgs e)
        {
        }

        Int32 GetPartyId()
        {
            Int32 result = Int32.MinValue;

            try
            {
                //result = Convert.ToInt32(headerInfo.Rows[0][PartyHI.PartyId]);
                result = Convert.ToInt32(this.txtBUPartyId.Text);
            }
            catch { }

            return result;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbBUIntExt_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.FillParentCoComboBox();
            }
            catch { }
        }

        private void btnBUFunctionsSave_Click(object sender, EventArgs e)
        {
            if (this.dgvBUFunctions.Rows != null && this.dgvBUFunctions.Rows.Count > 0)
            {
                ETRMDataAccess.DeleteFunctions(this.GetPartyId());
                {
                    foreach (DataGridViewRow row in this.dgvBUFunctions.Rows)
                    {
                        if (row.Cells[Function.Associated].Value != DBNull.Value && Convert.ToBoolean(row.Cells[Function.Associated].Value))
                        {
                            Int32 pId = Convert.ToInt32(row.Cells[Function.PartyFunctionTypeID].Value);
                            Int32 result = ETRMDataAccess.SavePartyFunction(this.GetPartyId(), pId);
                        }
                    }
                }
                this.FillFunctions();
                this.SetTabVisibility();
            }
        }

        private void btnBUPortfolioSave_Click(object sender, EventArgs e)
        {
            Int32 internalExternal = Convert.ToInt32(this.cbBUIntExt.SelectedValue);

            if (internalExternal == PartyClass.External)
            {
                MessageBox.Show("The external parties cannot be associated with any portfolio...");
                return;
            }

            if (this.dgvBUPortfolio.Rows != null && this.dgvBUPortfolio.Rows.Count > 0)
            {
                ETRMDataAccess.DeletePortfolios(this.GetPartyId());

                foreach (DataGridViewRow row in this.dgvBUPortfolio.Rows)
                {
                    if (row.Cells[Domain.Portfolio.Associated].Value != DBNull.Value && Convert.ToBoolean(row.Cells[Domain.Portfolio.Associated].Value))
                    {
                        Int32 pId = Convert.ToInt32(row.Cells[Domain.Portfolio.PortfolioID].Value);
                        Int32 result = ETRMDataAccess.SavePartyPortfolio(this.GetPartyId(), pId);
                    }
                }
            }
            this.FillPortfolios();
        }

        private void dgvBUPortfolio_AllowUserToResizeColumnsChanged(object sender, EventArgs e)
        {

        }

        private void dgvBUBrokerRates_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btBUBrokRatesAdd_Click(object sender, EventArgs e)
        {
            BUBrokerRates br = new BUBrokerRates(this.GetPartyId());
            br.ShowDialog();
            this.FillBrokerRates();
        }

        private void btnBUBrokerRatesEdit_Click(object sender, EventArgs e)
        {
            if (this.dgvBUBrokerRates.Rows.Count <= 0)
            {
                MessageBox.Show("There are currently no records, please enter the first address...");
                return;
            }
            int currentRow = this.dgvBUBrokerRates.CurrentCell.RowIndex;

            if (currentRow >= 0)
            {
                if (!ETRMDataAccess.IsEmpty(this.PartyBronkerRates))
                {
                    DataRow selectedRow = this.PartyBronkerRates.Rows[currentRow];

                    if (selectedRow != null)
                    {
                        BUBrokerRates pbr = new BUBrokerRates(selectedRow);
                        pbr.ShowDialog();
                        this.FillBrokerRates();
                    }
                }
            }
            else
            {
                MessageBox.Show("Please select a record first...");
            }
        }

        private void btnBrokerRatesDelete_Click(object sender, EventArgs e)
        {
            if (this.dgvBUBrokerRates.Rows.Count <= 0)
            {
                return;
            }

            int currentRow = Int32.MinValue;

            try
            {
                currentRow = this.dgvBUBrokerRates.CurrentCell.RowIndex;
            }
            catch { }

            if (currentRow >= 0)
            {
                if (!ETRMDataAccess.IsEmpty(this.PartyBronkerRates))
                {
                    DataRow selectedRow = this.PartyBronkerRates.Rows[currentRow];

                    if (selectedRow != null)
                    {
                        Int32 brokerRateID = Convert.ToInt32(selectedRow[Domain.BrokerRates.PartyBrokerRateID]);
                        Int32 deletionResult = ETRMDataAccess.DeletePartyBrokerRate(brokerRateID);
                    }
                }
                this.FillBrokerRates();
            }
            else
            {
                MessageBox.Show("Please select a record first...");
            }
        }

        private void btnAddressDelete_Click(object sender, EventArgs e)
        {
            if (this.dgvBUAddress.Rows.Count <= 0)
            {
                return;
            }

            int currentRow = Int32.MinValue;

            try
            {
                currentRow = this.dgvBUAddress.CurrentCell.RowIndex;
            }
            catch { }

            if (currentRow >= 0)
            {
                if (!ETRMDataAccess.IsEmpty(this.address))
                {
                    DataRow selectedRow = this.address.Rows[currentRow];

                    if (selectedRow != null)
                    {

                        DialogResult confirmation = MessageBox.Show(MessageWarning.AreYouSureDeletion, "Address Deletion", MessageBoxButtons.YesNo);

                        if (confirmation == DialogResult.Yes)
                        {
                            Int32 addressID = Convert.ToInt32(selectedRow[Domain.Address.PartyAddressID]); //PartyAddressID
                            Int32 deletionResult = ETRMDataAccess.DeleteAddress(addressID);
                        }
                    }
                }
                this.FillAddresses();
            }
            else
            {
                MessageBox.Show("Please select a record first...");
            }
        }

        private void btnDeleteContacts_Click(object sender, EventArgs e)
        {
            if (this.dgvBUContacts.Rows.Count <= 0)
            {
                return;
            }

            int currentRow = Int32.MinValue;

            try
            {
                currentRow = this.dgvBUContacts.CurrentCell.RowIndex;
            }
            catch { }

            if (currentRow >= 0)
            {
                if (!ETRMDataAccess.IsEmpty(this.contacts))
                {
                    DataRow selectedRow = this.contacts.Rows[currentRow];

                    if (selectedRow != null)
                    {
                        Int32 contactID = Convert.ToInt32(selectedRow[Domain.Contact.PartyContactID]);
                        Int32 deletionResult = ETRMDataAccess.DeleteContact(contactID);
                    }
                }
                this.FillContacts();
            }
            else
            {
                MessageBox.Show("Please select a record first...");
            }
        }

        private void btnInvoiceMappingAdd_Click(object sender, EventArgs e)
        {
            var mappingForm = new InvoiceAccountMapping(_invoiceMapping);
            mappingForm.ShowDialog();
            FillInvoiceMapping();
        }

        private void FillInvoiceMapping()
        {
            int partyId = this.GetPartyId();
            if (partyId != Int32.MinValue)
            {
                _invoiceMapping = new InvoiceMapping();
                _invoiceMapping.PartyID = partyId;
                dataGridViewInvoiceMapping.Rows.Clear();
               var dd = ETRMDataAccess.GetAllFromExternalInvoiceAccountForParty(partyId);
                if (dd != null && dd.Rows != null && dd.Rows.Count > 0)
                {
                    var allItems = new List<InvoiceDbMapping>(dd.Rows.Count);
                    foreach (DataRow dr in dd.Rows)
                    {
                        var rd = new InvoiceDbMapping
                        {
                            PartyID = DbConvert.GetInt(dr["PartyID"], 0),
                            InvoiceGroupID = DbConvert.GetInt(dr["InvoiceGroupID"], 0),
                            DeliveryTypeId = DbConvert.GetInt(dr["DeliveryTypeId"], 0),
                            NavisionAccountNo = DbConvert.GetString(dr["NavisionAccountNo"]),
                            NavisionCustomerNo = DbConvert.GetString(dr["NavisionCustomerNo"]),
                            NavisionVendorNo = DbConvert.GetString(dr["NavisionVendorNo"]),
                            NavisionCommodityName = DbConvert.GetString(dr["NavisionCommodityName"]),
                            InvoiceGroupName = DbConvert.GetString(dr["InvoiceGroup"])
                        };
                        allItems.Add(rd);

                        if (rd.DeliveryTypeId == 3) // Natgas
                        {
                            switch (rd.InvoiceGroupID)
                            {
                                case 1:
                                    _invoiceMapping.NatGasFinancialAccNo = rd.NavisionAccountNo;   // Financial
                                    break;
                                case 2:
                                    _invoiceMapping.NatGasPhysicalAccNo = rd.NavisionAccountNo;        // Physical
                                    break;
                                case 4:
                                    _invoiceMapping.BrookerageAccNo = rd.NavisionAccountNo;        
                                    break;
                                case 5:
                                    _invoiceMapping.NatGasOptPremAccNo = rd.NavisionAccountNo;        
                                    break;
                            }
                        }

                        if (rd.DeliveryTypeId == 6) // Power
                        {
                            switch (rd.InvoiceGroupID)
                            {
                                case 1:
                                    _invoiceMapping.PowerFinancialAccNo = rd.NavisionAccountNo;   // Financial
                                    break;
                                case 2:
                                    _invoiceMapping.PowerPhysicalAccNo = rd.NavisionAccountNo;        // Physical
                                    break;
                                case 4:
                                    _invoiceMapping.BrookerageAccNo = rd.NavisionAccountNo;        
                                    break;
                                case 5:
                                    _invoiceMapping.PowerOptPremAccNo = rd.NavisionAccountNo;        
                                    break;
                            }
                        }

                        if (rd.DeliveryTypeId == 7) // Option
                        {

                        }

                        if (rd.DeliveryTypeId == 8) // Cash
                        {
                            switch (rd.InvoiceGroupID)
                            {
                                case 3:
                                    _invoiceMapping.BrookerageAccNo = rd.NavisionAccountNo;   // Financial
                                    break;
                            }
                        }
                        _invoiceMapping.NavisionCustomerNo = rd.NavisionCustomerNo;
                        _invoiceMapping.NavisionVendorNo = rd.NavisionVendorNo;
                     //   _invoiceMapping.PartyID = rd.PartyID;
                    }
                     dataGridViewInvoiceMapping.Rows.Add(_invoiceMapping.NavisionCustomerNo, _invoiceMapping.NavisionVendorNo, _invoiceMapping.PowerPhysicalAccNo, _invoiceMapping.PowerFinancialAccNo, _invoiceMapping.NatGasPhysicalAccNo, _invoiceMapping.NatGasFinancialAccNo, _invoiceMapping.PowerOptPremAccNo, _invoiceMapping.NatGasOptPremAccNo, _invoiceMapping.BrookerageAccNo);
                    btnInvoiceMappingAdd.Enabled = false;
                    btnInvoiceMappingEdit.Enabled = true;
                }
            }
        }

        private InvoiceMapping _invoiceMapping;

        private void btnInvoiceMappingEdit_Click(object sender, EventArgs e)
        {
            var mappingForm = new InvoiceAccountMapping(_invoiceMapping);
            mappingForm.ShowDialog();
            FillInvoiceMapping();
        }

        private void btnInvoiceMappingDelete_Click(object sender, EventArgs e)
        {

        }
    }
}
