﻿namespace Gotham.AddIn.Party
{
    partial class AgreementsEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.splitContainerAg = new System.Windows.Forms.SplitContainer();
            this.cbConfirmRoute = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbCalculationAgent = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbVersionYear = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.dtpEffectiveEnd = new System.Windows.Forms.DateTimePicker();
            this.dtpEffectiveStart = new System.Windows.Forms.DateTimePicker();
            this.dtpSigned = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cbAgreementStatus = new System.Windows.Forms.ComboBox();
            this.cbAgrementType = new System.Windows.Forms.ComboBox();
            this.cbExternalLE = new System.Windows.Forms.ComboBox();
            this.cbInternalLE = new System.Windows.Forms.ComboBox();
            this.txtAgreementID = new System.Windows.Forms.TextBox();
            this.txtAgreementName = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvCommodity = new System.Windows.Forms.DataGridView();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.dgvCurrency = new System.Windows.Forms.DataGridView();
            this.dgvSettlementType = new System.Windows.Forms.DataGridView();
            this.label13 = new System.Windows.Forms.Label();
            this.txtCptyAgreementID = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerAg)).BeginInit();
            this.splitContainerAg.Panel1.SuspendLayout();
            this.splitContainerAg.Panel2.SuspendLayout();
            this.splitContainerAg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCommodity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSettlementType)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Agreement Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Agreement ID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(489, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Agreement Type";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(489, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Status";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Internal Legal Entity";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(44, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "External Legal Entity";
            // 
            // splitContainerAg
            // 
            this.splitContainerAg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerAg.Location = new System.Drawing.Point(0, 0);
            this.splitContainerAg.Name = "splitContainerAg";
            this.splitContainerAg.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerAg.Panel1
            // 
            this.splitContainerAg.Panel1.Controls.Add(this.txtCptyAgreementID);
            this.splitContainerAg.Panel1.Controls.Add(this.label13);
            this.splitContainerAg.Panel1.Controls.Add(this.cbConfirmRoute);
            this.splitContainerAg.Panel1.Controls.Add(this.label12);
            this.splitContainerAg.Panel1.Controls.Add(this.cbCalculationAgent);
            this.splitContainerAg.Panel1.Controls.Add(this.label11);
            this.splitContainerAg.Panel1.Controls.Add(this.cbVersionYear);
            this.splitContainerAg.Panel1.Controls.Add(this.label10);
            this.splitContainerAg.Panel1.Controls.Add(this.btnClose);
            this.splitContainerAg.Panel1.Controls.Add(this.btnSave);
            this.splitContainerAg.Panel1.Controls.Add(this.dtpEffectiveEnd);
            this.splitContainerAg.Panel1.Controls.Add(this.dtpEffectiveStart);
            this.splitContainerAg.Panel1.Controls.Add(this.dtpSigned);
            this.splitContainerAg.Panel1.Controls.Add(this.label9);
            this.splitContainerAg.Panel1.Controls.Add(this.label8);
            this.splitContainerAg.Panel1.Controls.Add(this.label7);
            this.splitContainerAg.Panel1.Controls.Add(this.cbAgreementStatus);
            this.splitContainerAg.Panel1.Controls.Add(this.cbAgrementType);
            this.splitContainerAg.Panel1.Controls.Add(this.cbExternalLE);
            this.splitContainerAg.Panel1.Controls.Add(this.cbInternalLE);
            this.splitContainerAg.Panel1.Controls.Add(this.txtAgreementID);
            this.splitContainerAg.Panel1.Controls.Add(this.txtAgreementName);
            this.splitContainerAg.Panel1.Controls.Add(this.label3);
            this.splitContainerAg.Panel1.Controls.Add(this.label6);
            this.splitContainerAg.Panel1.Controls.Add(this.label1);
            this.splitContainerAg.Panel1.Controls.Add(this.label5);
            this.splitContainerAg.Panel1.Controls.Add(this.label2);
            this.splitContainerAg.Panel1.Controls.Add(this.label4);
            // 
            // splitContainerAg.Panel2
            // 
            this.splitContainerAg.Panel2.Controls.Add(this.splitContainer1);
            this.splitContainerAg.Size = new System.Drawing.Size(971, 582);
            this.splitContainerAg.SplitterDistance = 235;
            this.splitContainerAg.TabIndex = 6;
            // 
            // cbConfirmRoute
            // 
            this.cbConfirmRoute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbConfirmRoute.FormattingEnabled = true;
            this.cbConfirmRoute.Location = new System.Drawing.Point(591, 98);
            this.cbConfirmRoute.Name = "cbConfirmRoute";
            this.cbConfirmRoute.Size = new System.Drawing.Size(316, 21);
            this.cbConfirmRoute.TabIndex = 25;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(489, 105);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "Confirm Route";
            // 
            // cbCalculationAgent
            // 
            this.cbCalculationAgent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCalculationAgent.FormattingEnabled = true;
            this.cbCalculationAgent.Location = new System.Drawing.Point(158, 137);
            this.cbCalculationAgent.Name = "cbCalculationAgent";
            this.cbCalculationAgent.Size = new System.Drawing.Size(316, 21);
            this.cbCalculationAgent.TabIndex = 23;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(44, 141);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "Calculation Agent";
            // 
            // cbVersionYear
            // 
            this.cbVersionYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVersionYear.FormattingEnabled = true;
            this.cbVersionYear.Location = new System.Drawing.Point(591, 43);
            this.cbVersionYear.Name = "cbVersionYear";
            this.cbVersionYear.Size = new System.Drawing.Size(316, 21);
            this.cbVersionYear.TabIndex = 21;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(489, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "Version Year";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(832, 195);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 19;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(751, 195);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 18;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dtpEffectiveEnd
            // 
            this.dtpEffectiveEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEffectiveEnd.Location = new System.Drawing.Point(591, 200);
            this.dtpEffectiveEnd.Name = "dtpEffectiveEnd";
            this.dtpEffectiveEnd.Size = new System.Drawing.Size(92, 20);
            this.dtpEffectiveEnd.TabIndex = 17;
            // 
            // dtpEffectiveStart
            // 
            this.dtpEffectiveStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpEffectiveStart.Location = new System.Drawing.Point(591, 171);
            this.dtpEffectiveStart.Name = "dtpEffectiveStart";
            this.dtpEffectiveStart.Size = new System.Drawing.Size(92, 20);
            this.dtpEffectiveStart.TabIndex = 16;
            // 
            // dtpSigned
            // 
            this.dtpSigned.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpSigned.Location = new System.Drawing.Point(591, 142);
            this.dtpSigned.Name = "dtpSigned";
            this.dtpSigned.Size = new System.Drawing.Size(92, 20);
            this.dtpSigned.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(489, 205);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Effective End";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(489, 175);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Effective Start";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(489, 145);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Signed Date";
            // 
            // cbAgreementStatus
            // 
            this.cbAgreementStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAgreementStatus.FormattingEnabled = true;
            this.cbAgreementStatus.Location = new System.Drawing.Point(591, 70);
            this.cbAgreementStatus.Name = "cbAgreementStatus";
            this.cbAgreementStatus.Size = new System.Drawing.Size(316, 21);
            this.cbAgreementStatus.TabIndex = 11;
            // 
            // cbAgrementType
            // 
            this.cbAgrementType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAgrementType.FormattingEnabled = true;
            this.cbAgrementType.Location = new System.Drawing.Point(591, 16);
            this.cbAgrementType.Name = "cbAgrementType";
            this.cbAgrementType.Size = new System.Drawing.Size(316, 21);
            this.cbAgrementType.TabIndex = 10;
            // 
            // cbExternalLE
            // 
            this.cbExternalLE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbExternalLE.FormattingEnabled = true;
            this.cbExternalLE.Location = new System.Drawing.Point(158, 105);
            this.cbExternalLE.Name = "cbExternalLE";
            this.cbExternalLE.Size = new System.Drawing.Size(316, 21);
            this.cbExternalLE.TabIndex = 9;
            // 
            // cbInternalLE
            // 
            this.cbInternalLE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInternalLE.FormattingEnabled = true;
            this.cbInternalLE.Location = new System.Drawing.Point(158, 74);
            this.cbInternalLE.Name = "cbInternalLE";
            this.cbInternalLE.Size = new System.Drawing.Size(316, 21);
            this.cbInternalLE.TabIndex = 8;
            // 
            // txtAgreementID
            // 
            this.txtAgreementID.Location = new System.Drawing.Point(158, 45);
            this.txtAgreementID.Name = "txtAgreementID";
            this.txtAgreementID.Size = new System.Drawing.Size(316, 20);
            this.txtAgreementID.TabIndex = 7;
            // 
            // txtAgreementName
            // 
            this.txtAgreementName.Location = new System.Drawing.Point(158, 17);
            this.txtAgreementName.Name = "txtAgreementName";
            this.txtAgreementName.Size = new System.Drawing.Size(316, 20);
            this.txtAgreementName.TabIndex = 6;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgvCommodity);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(971, 343);
            this.splitContainer1.SplitterDistance = 305;
            this.splitContainer1.TabIndex = 0;
            // 
            // dgvCommodity
            // 
            this.dgvCommodity.AllowUserToAddRows = false;
            this.dgvCommodity.AllowUserToDeleteRows = false;
            this.dgvCommodity.AllowUserToResizeRows = false;
            this.dgvCommodity.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCommodity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCommodity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCommodity.Location = new System.Drawing.Point(0, 0);
            this.dgvCommodity.Name = "dgvCommodity";
            this.dgvCommodity.RowHeadersVisible = false;
            this.dgvCommodity.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCommodity.Size = new System.Drawing.Size(305, 343);
            this.dgvCommodity.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dgvCurrency);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dgvSettlementType);
            this.splitContainer2.Size = new System.Drawing.Size(662, 343);
            this.splitContainer2.SplitterDistance = 325;
            this.splitContainer2.TabIndex = 0;
            // 
            // dgvCurrency
            // 
            this.dgvCurrency.AllowUserToAddRows = false;
            this.dgvCurrency.AllowUserToDeleteRows = false;
            this.dgvCurrency.AllowUserToResizeRows = false;
            this.dgvCurrency.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCurrency.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCurrency.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCurrency.Location = new System.Drawing.Point(0, 0);
            this.dgvCurrency.Name = "dgvCurrency";
            this.dgvCurrency.RowHeadersVisible = false;
            this.dgvCurrency.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCurrency.Size = new System.Drawing.Size(325, 343);
            this.dgvCurrency.TabIndex = 1;
            // 
            // dgvSettlementType
            // 
            this.dgvSettlementType.AllowUserToAddRows = false;
            this.dgvSettlementType.AllowUserToDeleteRows = false;
            this.dgvSettlementType.AllowUserToResizeRows = false;
            this.dgvSettlementType.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSettlementType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSettlementType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSettlementType.Location = new System.Drawing.Point(0, 0);
            this.dgvSettlementType.Name = "dgvSettlementType";
            this.dgvSettlementType.RowHeadersVisible = false;
            this.dgvSettlementType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSettlementType.Size = new System.Drawing.Size(333, 343);
            this.dgvSettlementType.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(44, 173);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(96, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "Cpty Agreement ID";
            // 
            // txtCptyAgreementID
            // 
            this.txtCptyAgreementID.Location = new System.Drawing.Point(158, 168);
            this.txtCptyAgreementID.Name = "txtCptyAgreementID";
            this.txtCptyAgreementID.Size = new System.Drawing.Size(316, 20);
            this.txtCptyAgreementID.TabIndex = 27;
            // 
            // AgreementsEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(971, 582);
            this.Controls.Add(this.splitContainerAg);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AgreementsEditor";
            this.Text = "Agreement";
            this.splitContainerAg.Panel1.ResumeLayout(false);
            this.splitContainerAg.Panel1.PerformLayout();
            this.splitContainerAg.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerAg)).EndInit();
            this.splitContainerAg.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCommodity)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSettlementType)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.SplitContainer splitContainerAg;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TextBox txtAgreementName;
        private System.Windows.Forms.TextBox txtAgreementID;
        private System.Windows.Forms.ComboBox cbInternalLE;
        private System.Windows.Forms.ComboBox cbExternalLE;
        private System.Windows.Forms.ComboBox cbAgreementStatus;
        private System.Windows.Forms.ComboBox cbAgrementType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtpEffectiveEnd;
        private System.Windows.Forms.DateTimePicker dtpEffectiveStart;
        private System.Windows.Forms.DateTimePicker dtpSigned;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgvCommodity;
        private System.Windows.Forms.DataGridView dgvCurrency;
        private System.Windows.Forms.DataGridView dgvSettlementType;
        private System.Windows.Forms.ComboBox cbVersionYear;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbCalculationAgent;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbConfirmRoute;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtCptyAgreementID;
        private System.Windows.Forms.Label label13;
    }
}