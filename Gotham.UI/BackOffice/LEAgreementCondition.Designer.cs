﻿namespace Strazze.AddIn.Forms
{
    partial class LEAgreementCondition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvUnderlying = new System.Windows.Forms.DataGridView();
            this.dgvCurrency = new System.Windows.Forms.DataGridView();
            this.dgvSettlementType = new System.Windows.Forms.DataGridView();
            this.scConditions = new System.Windows.Forms.SplitContainer();
            this.gbAction = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUnderlying)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSettlementType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scConditions)).BeginInit();
            this.scConditions.Panel1.SuspendLayout();
            this.scConditions.Panel2.SuspendLayout();
            this.scConditions.SuspendLayout();
            this.gbAction.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "ID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Agreement Type";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Jurisdiction";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(147, 17);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(475, 20);
            this.textBox1.TabIndex = 4;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(147, 49);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(475, 20);
            this.textBox2.TabIndex = 5;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(147, 76);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(475, 21);
            this.comboBox1.TabIndex = 6;
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(147, 114);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(475, 21);
            this.comboBox2.TabIndex = 7;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgvUnderlying);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvCurrency);
            this.splitContainer1.Size = new System.Drawing.Size(437, 314);
            this.splitContainer1.SplitterDistance = 212;
            this.splitContainer1.TabIndex = 0;
            // 
            // dgvUnderlying
            // 
            this.dgvUnderlying.AllowUserToAddRows = false;
            this.dgvUnderlying.AllowUserToDeleteRows = false;
            this.dgvUnderlying.AllowUserToResizeRows = false;
            this.dgvUnderlying.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvUnderlying.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUnderlying.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvUnderlying.Location = new System.Drawing.Point(0, 0);
            this.dgvUnderlying.Name = "dgvUnderlying";
            this.dgvUnderlying.RowHeadersVisible = false;
            this.dgvUnderlying.Size = new System.Drawing.Size(212, 314);
            this.dgvUnderlying.TabIndex = 0;
            // 
            // dgvCurrency
            // 
            this.dgvCurrency.AllowUserToAddRows = false;
            this.dgvCurrency.AllowUserToDeleteRows = false;
            this.dgvCurrency.AllowUserToResizeRows = false;
            this.dgvCurrency.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCurrency.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCurrency.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCurrency.Location = new System.Drawing.Point(0, 0);
            this.dgvCurrency.Name = "dgvCurrency";
            this.dgvCurrency.RowHeadersVisible = false;
            this.dgvCurrency.Size = new System.Drawing.Size(221, 314);
            this.dgvCurrency.TabIndex = 0;
            // 
            // dgvSettlementType
            // 
            this.dgvSettlementType.AllowUserToAddRows = false;
            this.dgvSettlementType.AllowUserToDeleteRows = false;
            this.dgvSettlementType.AllowUserToResizeRows = false;
            this.dgvSettlementType.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSettlementType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSettlementType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSettlementType.Location = new System.Drawing.Point(0, 0);
            this.dgvSettlementType.Name = "dgvSettlementType";
            this.dgvSettlementType.RowHeadersVisible = false;
            this.dgvSettlementType.Size = new System.Drawing.Size(211, 314);
            this.dgvSettlementType.TabIndex = 0;
            // 
            // scConditions
            // 
            this.scConditions.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.scConditions.Location = new System.Drawing.Point(0, 178);
            this.scConditions.Name = "scConditions";
            // 
            // scConditions.Panel1
            // 
            this.scConditions.Panel1.Controls.Add(this.dgvSettlementType);
            // 
            // scConditions.Panel2
            // 
            this.scConditions.Panel2.Controls.Add(this.splitContainer1);
            this.scConditions.Size = new System.Drawing.Size(652, 314);
            this.scConditions.SplitterDistance = 211;
            this.scConditions.TabIndex = 8;
            // 
            // gbAction
            // 
            this.gbAction.BackColor = System.Drawing.SystemColors.Control;
            this.gbAction.Controls.Add(this.btnClose);
            this.gbAction.Controls.Add(this.btnSave);
            this.gbAction.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gbAction.Location = new System.Drawing.Point(0, 492);
            this.gbAction.Name = "gbAction";
            this.gbAction.Size = new System.Drawing.Size(652, 67);
            this.gbAction.TabIndex = 9;
            this.gbAction.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(547, 20);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(25, 20);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // LEAgreementCondition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 559);
            this.Controls.Add(this.scConditions);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gbAction);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LEAgreementCondition";
            this.Text = "Agreement Condition";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUnderlying)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSettlementType)).EndInit();
            this.scConditions.Panel1.ResumeLayout(false);
            this.scConditions.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scConditions)).EndInit();
            this.scConditions.ResumeLayout(false);
            this.gbAction.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgvUnderlying;
        private System.Windows.Forms.DataGridView dgvCurrency;
        private System.Windows.Forms.DataGridView dgvSettlementType;
        private System.Windows.Forms.SplitContainer scConditions;
        private System.Windows.Forms.GroupBox gbAction;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
    }
}