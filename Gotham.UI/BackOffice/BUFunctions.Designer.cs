﻿namespace Strazze.AddIn.Forms
{
    partial class BUFunctions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbPartyFunctions = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddBUFunction = new System.Windows.Forms.Button();
            this.btnCancelBUFunction = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbPartyFunctions
            // 
            this.cbPartyFunctions.FormattingEnabled = true;
            this.cbPartyFunctions.Location = new System.Drawing.Point(96, 41);
            this.cbPartyFunctions.Name = "cbPartyFunctions";
            this.cbPartyFunctions.Size = new System.Drawing.Size(176, 21);
            this.cbPartyFunctions.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Function:";
            // 
            // btnAddBUFunction
            // 
            this.btnAddBUFunction.Location = new System.Drawing.Point(15, 92);
            this.btnAddBUFunction.Name = "btnAddBUFunction";
            this.btnAddBUFunction.Size = new System.Drawing.Size(75, 23);
            this.btnAddBUFunction.TabIndex = 2;
            this.btnAddBUFunction.Text = "Add";
            this.btnAddBUFunction.UseVisualStyleBackColor = true;
            this.btnAddBUFunction.Click += new System.EventHandler(this.btnAddBUFunction_Click);
            // 
            // btnCancelBUFunction
            // 
            this.btnCancelBUFunction.Location = new System.Drawing.Point(197, 92);
            this.btnCancelBUFunction.Name = "btnCancelBUFunction";
            this.btnCancelBUFunction.Size = new System.Drawing.Size(75, 23);
            this.btnCancelBUFunction.TabIndex = 3;
            this.btnCancelBUFunction.Text = "Cancel";
            this.btnCancelBUFunction.UseVisualStyleBackColor = true;
            this.btnCancelBUFunction.Click += new System.EventHandler(this.btnCancelBUFunction_Click);
            // 
            // BUFunctions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 150);
            this.Controls.Add(this.btnCancelBUFunction);
            this.Controls.Add(this.btnAddBUFunction);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbPartyFunctions);
            this.Name = "BUFunctions";
            this.Text = "Business Unit Functions";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbPartyFunctions;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddBUFunction;
        private System.Windows.Forms.Button btnCancelBUFunction;
    }
}