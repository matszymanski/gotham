﻿using Strazze.Data;
using Strazze.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Strazze.AddIn.Forms
{
    public partial class BUContact : Form
    {
        DataRow EditableInfo { get; set; }
        Int32 PartyId { get; set; }
        bool IsNew { get; set; }
        Int32 UserID { get; set; }

        public BUContact(DataRow row, Int32 userId)
        {
            InitializeComponent();
            this.EditableInfo = row;
            this.InitialiseView();
            this.FillEditableInformation();
            this.IsNew = false;
            this.UserID = userId;
        }

        public BUContact(Int32 partyId, Int32 userId)
        {
            InitializeComponent();
            this.PartyId = partyId;
            this.InitialiseView();
            this.IsNew = true;
            this.UserID = userId;
        }

        private void InitialiseView()
        {
            this.FillComboBoxes();
        }

        private void FillComboBoxes()
        {
            DataTable isDefault = ETRMDataAccess.GetYN();
            this.cbContactDefault.DataSource = isDefault.DefaultView;
            this.cbContactDefault.ValueMember = Address.IsDefaultValue;
            this.cbContactDefault.DisplayMember = Address.IsDefaultShow;
            this.cbContactDefault.BindingContext = this.BindingContext;

            DataTable contactType = ETRMDataAccess.GetContactType();
            this.cbContactType.DataSource = contactType.DefaultView;
            this.cbContactType.ValueMember = Address.PartyAddressTypeID; ;
            this.cbContactType.DisplayMember = Address.PartyAddressTypeName; ;
            this.cbContactType.BindingContext = this.BindingContext;

            DataTable contactTitle = ETRMDataAccess.GetContactTitle();
            this.cbTitle.DataSource = contactTitle.DefaultView;
            this.cbTitle.ValueMember = Contact.TitleID;
            this.cbTitle.DisplayMember = Contact.Title;
            this.cbTitle.BindingContext = this.BindingContext;

            DataTable isActive = ETRMDataAccess.GetTrueFalse();
            this.cbContactActive.DataSource = isActive.DefaultView;
            this.cbContactActive.ValueMember = Contact.TFValue;
            this.cbContactActive.DisplayMember = Contact.TFShow;
            this.cbContactActive.BindingContext = this.BindingContext;
        }

        private void FillEditableInformation()
        {
            if (this.EditableInfo != null)
            {
                Int32 partyId = Convert.ToInt32(this.EditableInfo[Address.PartyID]);
                Int32 partyAddId = Convert.ToInt32(this.EditableInfo[Contact.PartyContactID]);
                DataTable rawData = ETRMDataAccess.GetPartyContactRecord(partyId, partyAddId);

                if (rawData != null)
                {
                    this.cbContactType.SelectedValue = Convert.ToInt32(rawData.Rows[0][Contact.PartyAddressTypeID]);
                    this.cbTitle.SelectedValue = Convert.ToInt32(rawData.Rows[0][Contact.PartyTitleID]);
                    this.cbContactDefault.SelectedValue = Convert.ToInt32(rawData.Rows[0][Contact.DefaultId]);

                    if (!string.IsNullOrEmpty(Convert.ToString(rawData.Rows[0][Contact.IsActive])))
                    {
                        this.cbContactActive.SelectedValue = Convert.ToBoolean(rawData.Rows[0][Contact.IsActive]);
                    }

                    this.txtFirstName.Text = Convert.ToString(rawData.Rows[0][Contact.FirstName]);
                    this.txtLastName.Text = Convert.ToString(rawData.Rows[0][Contact.LastName]);
                    this.txtEmail.Text = Convert.ToString(rawData.Rows[0][Contact.Email]);
                    this.txtPhone.Text = Convert.ToString(rawData.Rows[0][Contact.PhoneNumber]);
                    this.txtFax.Text = Convert.ToString(rawData.Rows[0][Contact.FaxNumber]);
                }
            }
        }

        private bool IsContactValid()
        {
            return true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsContactValid())
            {
                Domain.Entities.Contact contact = new Domain.Entities.Contact();
                contact.PartyAddressTypeID = Convert.ToInt32(this.cbContactType.SelectedValue);
                contact.PartyTitleID = Convert.ToInt32(this.cbTitle.SelectedValue);
                contact.FirstName = txtFirstName.Text;
                contact.LastName = txtLastName.Text;
                contact.Email = txtEmail.Text;
                contact.PhoneNumber = txtPhone.Text;
                contact.FaxNumber = txtFax.Text;
                contact.DefaultId = Convert.ToInt32(this.cbContactDefault.SelectedValue);
                contact.Invalid = (Convert.ToBoolean(this.cbContactActive.SelectedValue) == false);
                contact.UpdateUser = this.UserID;
                contact.IsActive = Convert.ToBoolean(this.cbContactActive.SelectedValue);

                //Send it to the database...
                if (IsNew)
                {
                    contact.PartyID = this.PartyId;
                    contact.PartyContactID = ETRMDataAccess.MaxContactId() + 1;
                    if (ETRMDataAccess.SaveContact(contact) == Int32.MinValue)
                    {
                        MessageBox.Show("Something went wrong with the contact saving process. Please contact support");
                        return;
                    }
                }
                else
                {
                    contact.PartyID = Convert.ToInt32(this.EditableInfo[Contact.PartyID]);
                    contact.PartyContactID = Convert.ToInt32(this.EditableInfo[Contact.PartyContactID]);

                    if (ETRMDataAccess.UpdateContact(contact) == Int32.MinValue)
                    {
                        MessageBox.Show("Something went wrong with the contact updating process. Please contact support");
                        return;
                    }

                }

                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
