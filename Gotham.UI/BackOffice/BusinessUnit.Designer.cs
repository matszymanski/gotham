﻿namespace Strazze.AddIn.Forms
{
    partial class BusinessUnit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblAuth = new System.Windows.Forms.Label();
            this.txtBUShortName = new System.Windows.Forms.TextBox();
            this.txtBULegalName = new System.Windows.Forms.TextBox();
            this.txtBUGlobalId = new System.Windows.Forms.TextBox();
            this.txtBUPartyId = new System.Windows.Forms.TextBox();
            this.cbBUPartyClass = new System.Windows.Forms.ComboBox();
            this.cbBUIntExt = new System.Windows.Forms.ComboBox();
            this.cbBUStatus = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cbBUParentGroup = new System.Windows.Forms.ComboBox();
            this.cbAuthoriser = new System.Windows.Forms.ComboBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.tbBUInvoiceMapping = new System.Windows.Forms.TabPage();
            this.splitContainerInvoiceMapping = new System.Windows.Forms.SplitContainer();
            this.dataGridViewInvoiceMapping = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PowerPhysical = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PowerFinancial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NatGasPhysical = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NatGasFinancial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PowerOptPremium = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NatGasOptPremium = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Brokerage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnInvoiceMappingDelete = new System.Windows.Forms.Button();
            this.btnInvoiceMappingEdit = new System.Windows.Forms.Button();
            this.btnInvoiceMappingAdd = new System.Windows.Forms.Button();
            this.tbBUPartyInfo = new System.Windows.Forms.TabPage();
            this.txtBeaconID = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.cbConfirmRoute = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.cbTaxStatus = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cbTaxDomicile = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtTaxID = new System.Windows.Forms.TextBox();
            this.txtMICCode = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cbRegisteredCountry = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cbTradingCountry = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbDoddFrankReportingParty = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.chkOcon = new System.Windows.Forms.CheckBox();
            this.cbDoddFrankType = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.chkIcon = new System.Windows.Forms.CheckBox();
            this.cbConfirmingParty = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.chkInvoice = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tpBUSettlementInstructions = new System.Windows.Forms.TabPage();
            this.scSettlementInstructions = new System.Windows.Forms.SplitContainer();
            this.dgvSettlementInstructions = new System.Windows.Forms.DataGridView();
            this.btnBUSettInDelete = new System.Windows.Forms.Button();
            this.btnBUSettInEdit = new System.Windows.Forms.Button();
            this.btnBUSettInAdd = new System.Windows.Forms.Button();
            this.tpBUBrokerRates = new System.Windows.Forms.TabPage();
            this.splContBrokerRates = new System.Windows.Forms.SplitContainer();
            this.dgvBUBrokerRates = new System.Windows.Forms.DataGridView();
            this.txtBrokerWarning = new System.Windows.Forms.TextBox();
            this.btnBrokerRatesDelete = new System.Windows.Forms.Button();
            this.btnBUBrokerRatesEdit = new System.Windows.Forms.Button();
            this.btBUBrokRatesAdd = new System.Windows.Forms.Button();
            this.tpBUPortfolios = new System.Windows.Forms.TabPage();
            this.splitContBUPortfolio = new System.Windows.Forms.SplitContainer();
            this.dgvBUPortfolio = new System.Windows.Forms.DataGridView();
            this.btnBUPortfolioSave = new System.Windows.Forms.Button();
            this.tpBUFunctions = new System.Windows.Forms.TabPage();
            this.scBUFunctions = new System.Windows.Forms.SplitContainer();
            this.dgvBUFunctions = new System.Windows.Forms.DataGridView();
            this.btnBUFunctionsSave = new System.Windows.Forms.Button();
            this.tpBUContacts = new System.Windows.Forms.TabPage();
            this.scContacts = new System.Windows.Forms.SplitContainer();
            this.dgvBUContacts = new System.Windows.Forms.DataGridView();
            this.btnDeleteContacts = new System.Windows.Forms.Button();
            this.btnBUContactsEdit = new System.Windows.Forms.Button();
            this.btnBUContactsAdd = new System.Windows.Forms.Button();
            this.tpBUAddress = new System.Windows.Forms.TabPage();
            this.scAddress = new System.Windows.Forms.SplitContainer();
            this.dgvBUAddress = new System.Windows.Forms.DataGridView();
            this.btnAddressDelete = new System.Windows.Forms.Button();
            this.btnBUAddressEdit = new System.Windows.Forms.Button();
            this.btnBUAddressAdd = new System.Windows.Forms.Button();
            this.tcBusinessUnit = new System.Windows.Forms.TabControl();
            this.txtSchedulingName = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.tbBUInvoiceMapping.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerInvoiceMapping)).BeginInit();
            this.splitContainerInvoiceMapping.Panel1.SuspendLayout();
            this.splitContainerInvoiceMapping.Panel2.SuspendLayout();
            this.splitContainerInvoiceMapping.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewInvoiceMapping)).BeginInit();
            this.tbBUPartyInfo.SuspendLayout();
            this.tpBUSettlementInstructions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scSettlementInstructions)).BeginInit();
            this.scSettlementInstructions.Panel1.SuspendLayout();
            this.scSettlementInstructions.Panel2.SuspendLayout();
            this.scSettlementInstructions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSettlementInstructions)).BeginInit();
            this.tpBUBrokerRates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splContBrokerRates)).BeginInit();
            this.splContBrokerRates.Panel1.SuspendLayout();
            this.splContBrokerRates.Panel2.SuspendLayout();
            this.splContBrokerRates.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBUBrokerRates)).BeginInit();
            this.tpBUPortfolios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContBUPortfolio)).BeginInit();
            this.splitContBUPortfolio.Panel1.SuspendLayout();
            this.splitContBUPortfolio.Panel2.SuspendLayout();
            this.splitContBUPortfolio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBUPortfolio)).BeginInit();
            this.tpBUFunctions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scBUFunctions)).BeginInit();
            this.scBUFunctions.Panel1.SuspendLayout();
            this.scBUFunctions.Panel2.SuspendLayout();
            this.scBUFunctions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBUFunctions)).BeginInit();
            this.tpBUContacts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scContacts)).BeginInit();
            this.scContacts.Panel1.SuspendLayout();
            this.scContacts.Panel2.SuspendLayout();
            this.scContacts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBUContacts)).BeginInit();
            this.tpBUAddress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scAddress)).BeginInit();
            this.scAddress.Panel1.SuspendLayout();
            this.scAddress.Panel2.SuspendLayout();
            this.scAddress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBUAddress)).BeginInit();
            this.tcBusinessUnit.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Short Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Legal Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Spider ID";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Party ID";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(557, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "LE / BU";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(557, 52);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Int / Ext";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(557, 118);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Legal Entity";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(557, 84);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Status";
            // 
            // lblAuth
            // 
            this.lblAuth.AutoSize = true;
            this.lblAuth.Location = new System.Drawing.Point(557, 147);
            this.lblAuth.Name = "lblAuth";
            this.lblAuth.Size = new System.Drawing.Size(54, 13);
            this.lblAuth.TabIndex = 8;
            this.lblAuth.Text = "Authoriser";
            // 
            // txtBUShortName
            // 
            this.txtBUShortName.Location = new System.Drawing.Point(97, 20);
            this.txtBUShortName.Name = "txtBUShortName";
            this.txtBUShortName.Size = new System.Drawing.Size(407, 20);
            this.txtBUShortName.TabIndex = 9;
            // 
            // txtBULegalName
            // 
            this.txtBULegalName.Location = new System.Drawing.Point(97, 49);
            this.txtBULegalName.Name = "txtBULegalName";
            this.txtBULegalName.Size = new System.Drawing.Size(407, 20);
            this.txtBULegalName.TabIndex = 10;
            // 
            // txtBUGlobalId
            // 
            this.txtBUGlobalId.Location = new System.Drawing.Point(97, 82);
            this.txtBUGlobalId.Name = "txtBUGlobalId";
            this.txtBUGlobalId.Size = new System.Drawing.Size(407, 20);
            this.txtBUGlobalId.TabIndex = 11;
            // 
            // txtBUPartyId
            // 
            this.txtBUPartyId.Location = new System.Drawing.Point(97, 115);
            this.txtBUPartyId.Name = "txtBUPartyId";
            this.txtBUPartyId.Size = new System.Drawing.Size(407, 20);
            this.txtBUPartyId.TabIndex = 12;
            // 
            // cbBUPartyClass
            // 
            this.cbBUPartyClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBUPartyClass.FormattingEnabled = true;
            this.cbBUPartyClass.Location = new System.Drawing.Point(629, 20);
            this.cbBUPartyClass.Name = "cbBUPartyClass";
            this.cbBUPartyClass.Size = new System.Drawing.Size(372, 21);
            this.cbBUPartyClass.TabIndex = 15;
            // 
            // cbBUIntExt
            // 
            this.cbBUIntExt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBUIntExt.FormattingEnabled = true;
            this.cbBUIntExt.Location = new System.Drawing.Point(629, 49);
            this.cbBUIntExt.Name = "cbBUIntExt";
            this.cbBUIntExt.Size = new System.Drawing.Size(372, 21);
            this.cbBUIntExt.TabIndex = 16;
            this.cbBUIntExt.SelectedIndexChanged += new System.EventHandler(this.cbBUIntExt_SelectedIndexChanged);
            // 
            // cbBUStatus
            // 
            this.cbBUStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBUStatus.FormattingEnabled = true;
            this.cbBUStatus.Location = new System.Drawing.Point(629, 81);
            this.cbBUStatus.Name = "cbBUStatus";
            this.cbBUStatus.Size = new System.Drawing.Size(372, 21);
            this.cbBUStatus.TabIndex = 17;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(1040, 108);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 18;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnPartySave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(1040, 142);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 19;
            this.btnCancel.Text = "Close";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cbBUParentGroup
            // 
            this.cbBUParentGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBUParentGroup.FormattingEnabled = true;
            this.cbBUParentGroup.Location = new System.Drawing.Point(629, 112);
            this.cbBUParentGroup.Name = "cbBUParentGroup";
            this.cbBUParentGroup.Size = new System.Drawing.Size(372, 21);
            this.cbBUParentGroup.TabIndex = 21;
            // 
            // cbAuthoriser
            // 
            this.cbAuthoriser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAuthoriser.FormattingEnabled = true;
            this.cbAuthoriser.Location = new System.Drawing.Point(629, 144);
            this.cbAuthoriser.Name = "cbAuthoriser";
            this.cbAuthoriser.Size = new System.Drawing.Size(372, 21);
            this.cbAuthoriser.TabIndex = 22;
            // 
            // tbBUInvoiceMapping
            // 
            this.tbBUInvoiceMapping.Controls.Add(this.splitContainerInvoiceMapping);
            this.tbBUInvoiceMapping.Location = new System.Drawing.Point(4, 22);
            this.tbBUInvoiceMapping.Name = "tbBUInvoiceMapping";
            this.tbBUInvoiceMapping.Padding = new System.Windows.Forms.Padding(3);
            this.tbBUInvoiceMapping.Size = new System.Drawing.Size(1140, 395);
            this.tbBUInvoiceMapping.TabIndex = 10;
            this.tbBUInvoiceMapping.Text = "Invoice Mapping";
            this.tbBUInvoiceMapping.UseVisualStyleBackColor = true;
            // 
            // splitContainerInvoiceMapping
            // 
            this.splitContainerInvoiceMapping.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerInvoiceMapping.Location = new System.Drawing.Point(3, 3);
            this.splitContainerInvoiceMapping.Name = "splitContainerInvoiceMapping";
            this.splitContainerInvoiceMapping.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerInvoiceMapping.Panel1
            // 
            this.splitContainerInvoiceMapping.Panel1.Controls.Add(this.dataGridViewInvoiceMapping);
            // 
            // splitContainerInvoiceMapping.Panel2
            // 
            this.splitContainerInvoiceMapping.Panel2.Controls.Add(this.btnInvoiceMappingDelete);
            this.splitContainerInvoiceMapping.Panel2.Controls.Add(this.btnInvoiceMappingEdit);
            this.splitContainerInvoiceMapping.Panel2.Controls.Add(this.btnInvoiceMappingAdd);
            this.splitContainerInvoiceMapping.Size = new System.Drawing.Size(1134, 389);
            this.splitContainerInvoiceMapping.SplitterDistance = 329;
            this.splitContainerInvoiceMapping.TabIndex = 0;
            // 
            // dataGridViewInvoiceMapping
            // 
            this.dataGridViewInvoiceMapping.AllowUserToAddRows = false;
            this.dataGridViewInvoiceMapping.AllowUserToDeleteRows = false;
            this.dataGridViewInvoiceMapping.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedHeaders;
            this.dataGridViewInvoiceMapping.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewInvoiceMapping.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.PowerPhysical,
            this.PowerFinancial,
            this.NatGasPhysical,
            this.NatGasFinancial,
            this.PowerOptPremium,
            this.NatGasOptPremium,
            this.Brokerage});
            this.dataGridViewInvoiceMapping.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewInvoiceMapping.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewInvoiceMapping.MultiSelect = false;
            this.dataGridViewInvoiceMapping.Name = "dataGridViewInvoiceMapping";
            this.dataGridViewInvoiceMapping.ReadOnly = true;
            this.dataGridViewInvoiceMapping.Size = new System.Drawing.Size(1134, 329);
            this.dataGridViewInvoiceMapping.TabIndex = 1;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Counterparty code AR";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Counterparty code AP";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // PowerPhysical
            // 
            this.PowerPhysical.HeaderText = "Power Physical";
            this.PowerPhysical.Name = "PowerPhysical";
            this.PowerPhysical.ReadOnly = true;
            // 
            // PowerFinancial
            // 
            this.PowerFinancial.HeaderText = "Power Financial";
            this.PowerFinancial.Name = "PowerFinancial";
            this.PowerFinancial.ReadOnly = true;
            // 
            // NatGasPhysical
            // 
            this.NatGasPhysical.HeaderText = "NatGas Physical";
            this.NatGasPhysical.Name = "NatGasPhysical";
            this.NatGasPhysical.ReadOnly = true;
            // 
            // NatGasFinancial
            // 
            this.NatGasFinancial.HeaderText = "NatGas Financial";
            this.NatGasFinancial.Name = "NatGasFinancial";
            this.NatGasFinancial.ReadOnly = true;
            // 
            // PowerOptPremium
            // 
            this.PowerOptPremium.HeaderText = "PowerOpt Premium";
            this.PowerOptPremium.Name = "PowerOptPremium";
            this.PowerOptPremium.ReadOnly = true;
            // 
            // NatGasOptPremium
            // 
            this.NatGasOptPremium.HeaderText = "NatGas Opt Premium";
            this.NatGasOptPremium.Name = "NatGasOptPremium";
            this.NatGasOptPremium.ReadOnly = true;
            // 
            // Brokerage
            // 
            this.Brokerage.HeaderText = "Brokerage";
            this.Brokerage.Name = "Brokerage";
            this.Brokerage.ReadOnly = true;
            // 
            // btnInvoiceMappingDelete
            // 
            this.btnInvoiceMappingDelete.Location = new System.Drawing.Point(204, 18);
            this.btnInvoiceMappingDelete.Name = "btnInvoiceMappingDelete";
            this.btnInvoiceMappingDelete.Size = new System.Drawing.Size(75, 23);
            this.btnInvoiceMappingDelete.TabIndex = 5;
            this.btnInvoiceMappingDelete.Text = "Delete";
            this.btnInvoiceMappingDelete.UseVisualStyleBackColor = true;
            this.btnInvoiceMappingDelete.Visible = false;
            this.btnInvoiceMappingDelete.Click += new System.EventHandler(this.btnInvoiceMappingDelete_Click);
            // 
            // btnInvoiceMappingEdit
            // 
            this.btnInvoiceMappingEdit.Enabled = false;
            this.btnInvoiceMappingEdit.Location = new System.Drawing.Point(113, 18);
            this.btnInvoiceMappingEdit.Name = "btnInvoiceMappingEdit";
            this.btnInvoiceMappingEdit.Size = new System.Drawing.Size(75, 23);
            this.btnInvoiceMappingEdit.TabIndex = 4;
            this.btnInvoiceMappingEdit.Text = "Edit";
            this.btnInvoiceMappingEdit.UseVisualStyleBackColor = true;
            this.btnInvoiceMappingEdit.Click += new System.EventHandler(this.btnInvoiceMappingEdit_Click);
            // 
            // btnInvoiceMappingAdd
            // 
            this.btnInvoiceMappingAdd.Location = new System.Drawing.Point(22, 18);
            this.btnInvoiceMappingAdd.Name = "btnInvoiceMappingAdd";
            this.btnInvoiceMappingAdd.Size = new System.Drawing.Size(75, 23);
            this.btnInvoiceMappingAdd.TabIndex = 3;
            this.btnInvoiceMappingAdd.Text = "Add";
            this.btnInvoiceMappingAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnInvoiceMappingAdd.UseVisualStyleBackColor = true;
            this.btnInvoiceMappingAdd.Click += new System.EventHandler(this.btnInvoiceMappingAdd_Click);
            // 
            // tbBUPartyInfo
            // 
            this.tbBUPartyInfo.BackColor = System.Drawing.Color.Gainsboro;
            this.tbBUPartyInfo.Controls.Add(this.txtSchedulingName);
            this.tbBUPartyInfo.Controls.Add(this.label20);
            this.tbBUPartyInfo.Controls.Add(this.txtBeaconID);
            this.tbBUPartyInfo.Controls.Add(this.label19);
            this.tbBUPartyInfo.Controls.Add(this.cbConfirmRoute);
            this.tbBUPartyInfo.Controls.Add(this.label18);
            this.tbBUPartyInfo.Controls.Add(this.cbTaxStatus);
            this.tbBUPartyInfo.Controls.Add(this.label16);
            this.tbBUPartyInfo.Controls.Add(this.cbTaxDomicile);
            this.tbBUPartyInfo.Controls.Add(this.label15);
            this.tbBUPartyInfo.Controls.Add(this.txtTaxID);
            this.tbBUPartyInfo.Controls.Add(this.txtMICCode);
            this.tbBUPartyInfo.Controls.Add(this.label14);
            this.tbBUPartyInfo.Controls.Add(this.cbRegisteredCountry);
            this.tbBUPartyInfo.Controls.Add(this.label13);
            this.tbBUPartyInfo.Controls.Add(this.cbTradingCountry);
            this.tbBUPartyInfo.Controls.Add(this.label12);
            this.tbBUPartyInfo.Controls.Add(this.cbDoddFrankReportingParty);
            this.tbBUPartyInfo.Controls.Add(this.label11);
            this.tbBUPartyInfo.Controls.Add(this.chkOcon);
            this.tbBUPartyInfo.Controls.Add(this.cbDoddFrankType);
            this.tbBUPartyInfo.Controls.Add(this.label10);
            this.tbBUPartyInfo.Controls.Add(this.chkIcon);
            this.tbBUPartyInfo.Controls.Add(this.cbConfirmingParty);
            this.tbBUPartyInfo.Controls.Add(this.label9);
            this.tbBUPartyInfo.Controls.Add(this.chkInvoice);
            this.tbBUPartyInfo.Controls.Add(this.label17);
            this.tbBUPartyInfo.Location = new System.Drawing.Point(4, 22);
            this.tbBUPartyInfo.Name = "tbBUPartyInfo";
            this.tbBUPartyInfo.Size = new System.Drawing.Size(1140, 395);
            this.tbBUPartyInfo.TabIndex = 9;
            this.tbBUPartyInfo.Text = "Party Info";
            // 
            // txtBeaconID
            // 
            this.txtBeaconID.Location = new System.Drawing.Point(625, 87);
            this.txtBeaconID.Name = "txtBeaconID";
            this.txtBeaconID.Size = new System.Drawing.Size(372, 20);
            this.txtBeaconID.TabIndex = 45;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(524, 89);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(58, 13);
            this.label19.TabIndex = 44;
            this.label19.Text = "Beacon ID";
            // 
            // cbConfirmRoute
            // 
            this.cbConfirmRoute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbConfirmRoute.FormattingEnabled = true;
            this.cbConfirmRoute.Location = new System.Drawing.Point(625, 26);
            this.cbConfirmRoute.Name = "cbConfirmRoute";
            this.cbConfirmRoute.Size = new System.Drawing.Size(218, 21);
            this.cbConfirmRoute.TabIndex = 43;
            this.cbConfirmRoute.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(524, 30);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(74, 13);
            this.label18.TabIndex = 42;
            this.label18.Text = "Confirm Route";
            this.label18.Visible = false;
            // 
            // cbTaxStatus
            // 
            this.cbTaxStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTaxStatus.FormattingEnabled = true;
            this.cbTaxStatus.Location = new System.Drawing.Point(166, 342);
            this.cbTaxStatus.Name = "cbTaxStatus";
            this.cbTaxStatus.Size = new System.Drawing.Size(266, 21);
            this.cbTaxStatus.TabIndex = 41;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(29, 345);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(58, 13);
            this.label16.TabIndex = 40;
            this.label16.Text = "Tax Status";
            // 
            // cbTaxDomicile
            // 
            this.cbTaxDomicile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTaxDomicile.FormattingEnabled = true;
            this.cbTaxDomicile.Location = new System.Drawing.Point(166, 314);
            this.cbTaxDomicile.Name = "cbTaxDomicile";
            this.cbTaxDomicile.Size = new System.Drawing.Size(266, 21);
            this.cbTaxDomicile.TabIndex = 39;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(29, 317);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(68, 13);
            this.label15.TabIndex = 38;
            this.label15.Text = "Tax Domicile";
            // 
            // txtTaxID
            // 
            this.txtTaxID.Location = new System.Drawing.Point(166, 287);
            this.txtTaxID.Name = "txtTaxID";
            this.txtTaxID.Size = new System.Drawing.Size(266, 20);
            this.txtTaxID.TabIndex = 37;
            // 
            // txtMICCode
            // 
            this.txtMICCode.Location = new System.Drawing.Point(166, 27);
            this.txtMICCode.Name = "txtMICCode";
            this.txtMICCode.Size = new System.Drawing.Size(266, 20);
            this.txtMICCode.TabIndex = 22;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(29, 295);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 13);
            this.label14.TabIndex = 36;
            this.label14.Text = "Tax ID";
            // 
            // cbRegisteredCountry
            // 
            this.cbRegisteredCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRegisteredCountry.FormattingEnabled = true;
            this.cbRegisteredCountry.Location = new System.Drawing.Point(166, 260);
            this.cbRegisteredCountry.Name = "cbRegisteredCountry";
            this.cbRegisteredCountry.Size = new System.Drawing.Size(266, 21);
            this.cbRegisteredCountry.TabIndex = 35;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(29, 263);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(97, 13);
            this.label13.TabIndex = 34;
            this.label13.Text = "Registered Country";
            // 
            // cbTradingCountry
            // 
            this.cbTradingCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTradingCountry.FormattingEnabled = true;
            this.cbTradingCountry.Location = new System.Drawing.Point(166, 232);
            this.cbTradingCountry.Name = "cbTradingCountry";
            this.cbTradingCountry.Size = new System.Drawing.Size(266, 21);
            this.cbTradingCountry.TabIndex = 33;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(29, 235);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(125, 13);
            this.label12.TabIndex = 32;
            this.label12.Text = "Principal Trading Country";
            // 
            // cbDoddFrankReportingParty
            // 
            this.cbDoddFrankReportingParty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDoddFrankReportingParty.FormattingEnabled = true;
            this.cbDoddFrankReportingParty.Location = new System.Drawing.Point(166, 205);
            this.cbDoddFrankReportingParty.Name = "cbDoddFrankReportingParty";
            this.cbDoddFrankReportingParty.Size = new System.Drawing.Size(266, 21);
            this.cbDoddFrankReportingParty.TabIndex = 31;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(29, 208);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(113, 13);
            this.label11.TabIndex = 30;
            this.label11.Text = "Dodd Frank Rep Party";
            // 
            // chkOcon
            // 
            this.chkOcon.Location = new System.Drawing.Point(32, 140);
            this.chkOcon.Name = "chkOcon";
            this.chkOcon.Size = new System.Drawing.Size(129, 24);
            this.chkOcon.TabIndex = 29;
            this.chkOcon.Text = "OCon Required";
            this.chkOcon.UseVisualStyleBackColor = true;
            // 
            // cbDoddFrankType
            // 
            this.cbDoddFrankType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDoddFrankType.FormattingEnabled = true;
            this.cbDoddFrankType.Location = new System.Drawing.Point(166, 176);
            this.cbDoddFrankType.Name = "cbDoddFrankType";
            this.cbDoddFrankType.Size = new System.Drawing.Size(266, 21);
            this.cbDoddFrankType.TabIndex = 28;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(29, 179);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "Dodd Frank Type";
            // 
            // chkIcon
            // 
            this.chkIcon.Location = new System.Drawing.Point(32, 117);
            this.chkIcon.Name = "chkIcon";
            this.chkIcon.Size = new System.Drawing.Size(129, 24);
            this.chkIcon.TabIndex = 26;
            this.chkIcon.Text = "ICon Required";
            this.chkIcon.UseVisualStyleBackColor = true;
            // 
            // cbConfirmingParty
            // 
            this.cbConfirmingParty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbConfirmingParty.FormattingEnabled = true;
            this.cbConfirmingParty.Location = new System.Drawing.Point(166, 86);
            this.cbConfirmingParty.Name = "cbConfirmingParty";
            this.cbConfirmingParty.Size = new System.Drawing.Size(266, 21);
            this.cbConfirmingParty.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(29, 89);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Confirming Party";
            // 
            // chkInvoice
            // 
            this.chkInvoice.Location = new System.Drawing.Point(32, 56);
            this.chkInvoice.Name = "chkInvoice";
            this.chkInvoice.Size = new System.Drawing.Size(129, 24);
            this.chkInvoice.TabIndex = 23;
            this.chkInvoice.Text = "Invoice Required";
            this.chkInvoice.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(29, 35);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 13);
            this.label17.TabIndex = 21;
            this.label17.Text = "MIC Code";
            // 
            // tpBUSettlementInstructions
            // 
            this.tpBUSettlementInstructions.Controls.Add(this.scSettlementInstructions);
            this.tpBUSettlementInstructions.Location = new System.Drawing.Point(4, 22);
            this.tpBUSettlementInstructions.Name = "tpBUSettlementInstructions";
            this.tpBUSettlementInstructions.Size = new System.Drawing.Size(1140, 395);
            this.tpBUSettlementInstructions.TabIndex = 8;
            this.tpBUSettlementInstructions.Text = "Settlement Instructions";
            this.tpBUSettlementInstructions.UseVisualStyleBackColor = true;
            // 
            // scSettlementInstructions
            // 
            this.scSettlementInstructions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scSettlementInstructions.Location = new System.Drawing.Point(0, 0);
            this.scSettlementInstructions.Name = "scSettlementInstructions";
            this.scSettlementInstructions.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scSettlementInstructions.Panel1
            // 
            this.scSettlementInstructions.Panel1.Controls.Add(this.dgvSettlementInstructions);
            // 
            // scSettlementInstructions.Panel2
            // 
            this.scSettlementInstructions.Panel2.Controls.Add(this.btnBUSettInDelete);
            this.scSettlementInstructions.Panel2.Controls.Add(this.btnBUSettInEdit);
            this.scSettlementInstructions.Panel2.Controls.Add(this.btnBUSettInAdd);
            this.scSettlementInstructions.Size = new System.Drawing.Size(1140, 395);
            this.scSettlementInstructions.SplitterDistance = 327;
            this.scSettlementInstructions.TabIndex = 0;
            // 
            // dgvSettlementInstructions
            // 
            this.dgvSettlementInstructions.AllowUserToAddRows = false;
            this.dgvSettlementInstructions.AllowUserToDeleteRows = false;
            this.dgvSettlementInstructions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSettlementInstructions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSettlementInstructions.Location = new System.Drawing.Point(0, 0);
            this.dgvSettlementInstructions.Name = "dgvSettlementInstructions";
            this.dgvSettlementInstructions.RowHeadersVisible = false;
            this.dgvSettlementInstructions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSettlementInstructions.Size = new System.Drawing.Size(1140, 327);
            this.dgvSettlementInstructions.TabIndex = 0;
            // 
            // btnBUSettInDelete
            // 
            this.btnBUSettInDelete.Enabled = false;
            this.btnBUSettInDelete.Location = new System.Drawing.Point(192, 20);
            this.btnBUSettInDelete.Name = "btnBUSettInDelete";
            this.btnBUSettInDelete.Size = new System.Drawing.Size(75, 23);
            this.btnBUSettInDelete.TabIndex = 3;
            this.btnBUSettInDelete.Text = "Delete";
            this.btnBUSettInDelete.UseVisualStyleBackColor = true;
            // 
            // btnBUSettInEdit
            // 
            this.btnBUSettInEdit.Location = new System.Drawing.Point(101, 20);
            this.btnBUSettInEdit.Name = "btnBUSettInEdit";
            this.btnBUSettInEdit.Size = new System.Drawing.Size(75, 23);
            this.btnBUSettInEdit.TabIndex = 2;
            this.btnBUSettInEdit.Text = "Edit";
            this.btnBUSettInEdit.UseVisualStyleBackColor = true;
            // 
            // btnBUSettInAdd
            // 
            this.btnBUSettInAdd.Location = new System.Drawing.Point(10, 20);
            this.btnBUSettInAdd.Name = "btnBUSettInAdd";
            this.btnBUSettInAdd.Size = new System.Drawing.Size(75, 23);
            this.btnBUSettInAdd.TabIndex = 1;
            this.btnBUSettInAdd.Text = "Add";
            this.btnBUSettInAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnBUSettInAdd.UseVisualStyleBackColor = true;
            // 
            // tpBUBrokerRates
            // 
            this.tpBUBrokerRates.Controls.Add(this.splContBrokerRates);
            this.tpBUBrokerRates.Location = new System.Drawing.Point(4, 22);
            this.tpBUBrokerRates.Name = "tpBUBrokerRates";
            this.tpBUBrokerRates.Size = new System.Drawing.Size(1140, 395);
            this.tpBUBrokerRates.TabIndex = 4;
            this.tpBUBrokerRates.Text = "Broker Rates";
            this.tpBUBrokerRates.UseVisualStyleBackColor = true;
            // 
            // splContBrokerRates
            // 
            this.splContBrokerRates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splContBrokerRates.Location = new System.Drawing.Point(0, 0);
            this.splContBrokerRates.Name = "splContBrokerRates";
            this.splContBrokerRates.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splContBrokerRates.Panel1
            // 
            this.splContBrokerRates.Panel1.Controls.Add(this.dgvBUBrokerRates);
            // 
            // splContBrokerRates.Panel2
            // 
            this.splContBrokerRates.Panel2.Controls.Add(this.txtBrokerWarning);
            this.splContBrokerRates.Panel2.Controls.Add(this.btnBrokerRatesDelete);
            this.splContBrokerRates.Panel2.Controls.Add(this.btnBUBrokerRatesEdit);
            this.splContBrokerRates.Panel2.Controls.Add(this.btBUBrokRatesAdd);
            this.splContBrokerRates.Size = new System.Drawing.Size(1140, 395);
            this.splContBrokerRates.SplitterDistance = 327;
            this.splContBrokerRates.TabIndex = 0;
            // 
            // dgvBUBrokerRates
            // 
            this.dgvBUBrokerRates.AllowUserToAddRows = false;
            this.dgvBUBrokerRates.AllowUserToDeleteRows = false;
            this.dgvBUBrokerRates.AllowUserToResizeRows = false;
            this.dgvBUBrokerRates.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvBUBrokerRates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBUBrokerRates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBUBrokerRates.Location = new System.Drawing.Point(0, 0);
            this.dgvBUBrokerRates.Name = "dgvBUBrokerRates";
            this.dgvBUBrokerRates.ReadOnly = true;
            this.dgvBUBrokerRates.RowHeadersVisible = false;
            this.dgvBUBrokerRates.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBUBrokerRates.Size = new System.Drawing.Size(1140, 327);
            this.dgvBUBrokerRates.TabIndex = 0;
            this.dgvBUBrokerRates.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBUBrokerRates_CellContentClick);
            // 
            // txtBrokerWarning
            // 
            this.txtBrokerWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBrokerWarning.ForeColor = System.Drawing.Color.Black;
            this.txtBrokerWarning.Location = new System.Drawing.Point(291, 21);
            this.txtBrokerWarning.Name = "txtBrokerWarning";
            this.txtBrokerWarning.Size = new System.Drawing.Size(622, 22);
            this.txtBrokerWarning.TabIndex = 4;
            this.txtBrokerWarning.Text = "The broker rate tab is disabled because the business unit does not have broker fu" +
    "nction.";
            // 
            // btnBrokerRatesDelete
            // 
            this.btnBrokerRatesDelete.Enabled = false;
            this.btnBrokerRatesDelete.Location = new System.Drawing.Point(192, 20);
            this.btnBrokerRatesDelete.Name = "btnBrokerRatesDelete";
            this.btnBrokerRatesDelete.Size = new System.Drawing.Size(75, 23);
            this.btnBrokerRatesDelete.TabIndex = 2;
            this.btnBrokerRatesDelete.Text = "Delete";
            this.btnBrokerRatesDelete.UseVisualStyleBackColor = true;
            this.btnBrokerRatesDelete.Click += new System.EventHandler(this.btnBrokerRatesDelete_Click);
            // 
            // btnBUBrokerRatesEdit
            // 
            this.btnBUBrokerRatesEdit.Location = new System.Drawing.Point(101, 20);
            this.btnBUBrokerRatesEdit.Name = "btnBUBrokerRatesEdit";
            this.btnBUBrokerRatesEdit.Size = new System.Drawing.Size(75, 23);
            this.btnBUBrokerRatesEdit.TabIndex = 1;
            this.btnBUBrokerRatesEdit.Text = "Edit";
            this.btnBUBrokerRatesEdit.UseVisualStyleBackColor = true;
            this.btnBUBrokerRatesEdit.Click += new System.EventHandler(this.btnBUBrokerRatesEdit_Click);
            // 
            // btBUBrokRatesAdd
            // 
            this.btBUBrokRatesAdd.Enabled = false;
            this.btBUBrokRatesAdd.Location = new System.Drawing.Point(10, 20);
            this.btBUBrokRatesAdd.Name = "btBUBrokRatesAdd";
            this.btBUBrokRatesAdd.Size = new System.Drawing.Size(75, 23);
            this.btBUBrokRatesAdd.TabIndex = 0;
            this.btBUBrokRatesAdd.Text = "Add";
            this.btBUBrokRatesAdd.UseVisualStyleBackColor = true;
            this.btBUBrokRatesAdd.Click += new System.EventHandler(this.btBUBrokRatesAdd_Click);
            // 
            // tpBUPortfolios
            // 
            this.tpBUPortfolios.Controls.Add(this.splitContBUPortfolio);
            this.tpBUPortfolios.Location = new System.Drawing.Point(4, 22);
            this.tpBUPortfolios.Name = "tpBUPortfolios";
            this.tpBUPortfolios.Size = new System.Drawing.Size(1140, 395);
            this.tpBUPortfolios.TabIndex = 3;
            this.tpBUPortfolios.Text = "Portfolios";
            this.tpBUPortfolios.UseVisualStyleBackColor = true;
            // 
            // splitContBUPortfolio
            // 
            this.splitContBUPortfolio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContBUPortfolio.Location = new System.Drawing.Point(0, 0);
            this.splitContBUPortfolio.Name = "splitContBUPortfolio";
            this.splitContBUPortfolio.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContBUPortfolio.Panel1
            // 
            this.splitContBUPortfolio.Panel1.Controls.Add(this.dgvBUPortfolio);
            // 
            // splitContBUPortfolio.Panel2
            // 
            this.splitContBUPortfolio.Panel2.Controls.Add(this.btnBUPortfolioSave);
            this.splitContBUPortfolio.Size = new System.Drawing.Size(1140, 395);
            this.splitContBUPortfolio.SplitterDistance = 327;
            this.splitContBUPortfolio.TabIndex = 1;
            // 
            // dgvBUPortfolio
            // 
            this.dgvBUPortfolio.AllowUserToAddRows = false;
            this.dgvBUPortfolio.AllowUserToDeleteRows = false;
            this.dgvBUPortfolio.AllowUserToResizeColumns = false;
            this.dgvBUPortfolio.AllowUserToResizeRows = false;
            this.dgvBUPortfolio.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvBUPortfolio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBUPortfolio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBUPortfolio.Location = new System.Drawing.Point(0, 0);
            this.dgvBUPortfolio.Name = "dgvBUPortfolio";
            this.dgvBUPortfolio.ReadOnly = true;
            this.dgvBUPortfolio.RowHeadersVisible = false;
            this.dgvBUPortfolio.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBUPortfolio.Size = new System.Drawing.Size(1140, 327);
            this.dgvBUPortfolio.TabIndex = 0;
            this.dgvBUPortfolio.AllowUserToResizeColumnsChanged += new System.EventHandler(this.dgvBUPortfolio_AllowUserToResizeColumnsChanged);
            // 
            // btnBUPortfolioSave
            // 
            this.btnBUPortfolioSave.Enabled = false;
            this.btnBUPortfolioSave.Location = new System.Drawing.Point(25, 22);
            this.btnBUPortfolioSave.Name = "btnBUPortfolioSave";
            this.btnBUPortfolioSave.Size = new System.Drawing.Size(75, 23);
            this.btnBUPortfolioSave.TabIndex = 0;
            this.btnBUPortfolioSave.Text = "Save";
            this.btnBUPortfolioSave.UseVisualStyleBackColor = true;
            this.btnBUPortfolioSave.Click += new System.EventHandler(this.btnBUPortfolioSave_Click);
            // 
            // tpBUFunctions
            // 
            this.tpBUFunctions.Controls.Add(this.scBUFunctions);
            this.tpBUFunctions.Location = new System.Drawing.Point(4, 22);
            this.tpBUFunctions.Name = "tpBUFunctions";
            this.tpBUFunctions.Size = new System.Drawing.Size(1140, 395);
            this.tpBUFunctions.TabIndex = 2;
            this.tpBUFunctions.Text = "Functions";
            this.tpBUFunctions.UseVisualStyleBackColor = true;
            // 
            // scBUFunctions
            // 
            this.scBUFunctions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scBUFunctions.Location = new System.Drawing.Point(0, 0);
            this.scBUFunctions.Name = "scBUFunctions";
            this.scBUFunctions.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scBUFunctions.Panel1
            // 
            this.scBUFunctions.Panel1.Controls.Add(this.dgvBUFunctions);
            // 
            // scBUFunctions.Panel2
            // 
            this.scBUFunctions.Panel2.Controls.Add(this.btnBUFunctionsSave);
            this.scBUFunctions.Size = new System.Drawing.Size(1140, 395);
            this.scBUFunctions.SplitterDistance = 327;
            this.scBUFunctions.TabIndex = 0;
            // 
            // dgvBUFunctions
            // 
            this.dgvBUFunctions.AllowUserToAddRows = false;
            this.dgvBUFunctions.AllowUserToDeleteRows = false;
            this.dgvBUFunctions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBUFunctions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBUFunctions.Location = new System.Drawing.Point(0, 0);
            this.dgvBUFunctions.Name = "dgvBUFunctions";
            this.dgvBUFunctions.ReadOnly = true;
            this.dgvBUFunctions.RowHeadersVisible = false;
            this.dgvBUFunctions.Size = new System.Drawing.Size(1140, 327);
            this.dgvBUFunctions.TabIndex = 0;
            // 
            // btnBUFunctionsSave
            // 
            this.btnBUFunctionsSave.Enabled = false;
            this.btnBUFunctionsSave.Location = new System.Drawing.Point(25, 22);
            this.btnBUFunctionsSave.Name = "btnBUFunctionsSave";
            this.btnBUFunctionsSave.Size = new System.Drawing.Size(75, 23);
            this.btnBUFunctionsSave.TabIndex = 0;
            this.btnBUFunctionsSave.Text = "Save";
            this.btnBUFunctionsSave.UseVisualStyleBackColor = true;
            this.btnBUFunctionsSave.Click += new System.EventHandler(this.btnBUFunctionsSave_Click);
            // 
            // tpBUContacts
            // 
            this.tpBUContacts.Controls.Add(this.scContacts);
            this.tpBUContacts.Location = new System.Drawing.Point(4, 22);
            this.tpBUContacts.Name = "tpBUContacts";
            this.tpBUContacts.Padding = new System.Windows.Forms.Padding(3);
            this.tpBUContacts.Size = new System.Drawing.Size(1140, 395);
            this.tpBUContacts.TabIndex = 1;
            this.tpBUContacts.Text = "Contacts";
            this.tpBUContacts.UseVisualStyleBackColor = true;
            // 
            // scContacts
            // 
            this.scContacts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scContacts.Location = new System.Drawing.Point(3, 3);
            this.scContacts.Name = "scContacts";
            this.scContacts.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scContacts.Panel1
            // 
            this.scContacts.Panel1.Controls.Add(this.dgvBUContacts);
            // 
            // scContacts.Panel2
            // 
            this.scContacts.Panel2.Controls.Add(this.btnDeleteContacts);
            this.scContacts.Panel2.Controls.Add(this.btnBUContactsEdit);
            this.scContacts.Panel2.Controls.Add(this.btnBUContactsAdd);
            this.scContacts.Size = new System.Drawing.Size(1134, 389);
            this.scContacts.SplitterDistance = 327;
            this.scContacts.TabIndex = 0;
            // 
            // dgvBUContacts
            // 
            this.dgvBUContacts.AllowUserToAddRows = false;
            this.dgvBUContacts.AllowUserToDeleteRows = false;
            this.dgvBUContacts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvBUContacts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBUContacts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBUContacts.Location = new System.Drawing.Point(0, 0);
            this.dgvBUContacts.Name = "dgvBUContacts";
            this.dgvBUContacts.ReadOnly = true;
            this.dgvBUContacts.RowHeadersVisible = false;
            this.dgvBUContacts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBUContacts.Size = new System.Drawing.Size(1134, 327);
            this.dgvBUContacts.TabIndex = 0;
            // 
            // btnDeleteContacts
            // 
            this.btnDeleteContacts.Location = new System.Drawing.Point(192, 20);
            this.btnDeleteContacts.Name = "btnDeleteContacts";
            this.btnDeleteContacts.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteContacts.TabIndex = 2;
            this.btnDeleteContacts.Text = "Delete";
            this.btnDeleteContacts.UseVisualStyleBackColor = true;
            this.btnDeleteContacts.Click += new System.EventHandler(this.btnDeleteContacts_Click);
            // 
            // btnBUContactsEdit
            // 
            this.btnBUContactsEdit.Location = new System.Drawing.Point(101, 20);
            this.btnBUContactsEdit.Name = "btnBUContactsEdit";
            this.btnBUContactsEdit.Size = new System.Drawing.Size(75, 23);
            this.btnBUContactsEdit.TabIndex = 1;
            this.btnBUContactsEdit.Text = "Edit";
            this.btnBUContactsEdit.UseVisualStyleBackColor = true;
            this.btnBUContactsEdit.Click += new System.EventHandler(this.btnBUContactsEdit_Click);
            // 
            // btnBUContactsAdd
            // 
            this.btnBUContactsAdd.Location = new System.Drawing.Point(10, 20);
            this.btnBUContactsAdd.Name = "btnBUContactsAdd";
            this.btnBUContactsAdd.Size = new System.Drawing.Size(75, 23);
            this.btnBUContactsAdd.TabIndex = 0;
            this.btnBUContactsAdd.Text = "Add";
            this.btnBUContactsAdd.UseVisualStyleBackColor = true;
            this.btnBUContactsAdd.Click += new System.EventHandler(this.btnBUContactsAdd_Click);
            // 
            // tpBUAddress
            // 
            this.tpBUAddress.Controls.Add(this.scAddress);
            this.tpBUAddress.Location = new System.Drawing.Point(4, 22);
            this.tpBUAddress.Name = "tpBUAddress";
            this.tpBUAddress.Padding = new System.Windows.Forms.Padding(3);
            this.tpBUAddress.Size = new System.Drawing.Size(1140, 395);
            this.tpBUAddress.TabIndex = 0;
            this.tpBUAddress.Text = "Address";
            this.tpBUAddress.UseVisualStyleBackColor = true;
            // 
            // scAddress
            // 
            this.scAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scAddress.Location = new System.Drawing.Point(3, 3);
            this.scAddress.Name = "scAddress";
            this.scAddress.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scAddress.Panel1
            // 
            this.scAddress.Panel1.Controls.Add(this.dgvBUAddress);
            // 
            // scAddress.Panel2
            // 
            this.scAddress.Panel2.Controls.Add(this.btnAddressDelete);
            this.scAddress.Panel2.Controls.Add(this.btnBUAddressEdit);
            this.scAddress.Panel2.Controls.Add(this.btnBUAddressAdd);
            this.scAddress.Size = new System.Drawing.Size(1134, 389);
            this.scAddress.SplitterDistance = 327;
            this.scAddress.TabIndex = 0;
            // 
            // dgvBUAddress
            // 
            this.dgvBUAddress.AllowUserToAddRows = false;
            this.dgvBUAddress.AllowUserToDeleteRows = false;
            this.dgvBUAddress.AllowUserToResizeColumns = false;
            this.dgvBUAddress.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvBUAddress.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBUAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBUAddress.Location = new System.Drawing.Point(0, 0);
            this.dgvBUAddress.Name = "dgvBUAddress";
            this.dgvBUAddress.ReadOnly = true;
            this.dgvBUAddress.RowHeadersVisible = false;
            this.dgvBUAddress.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBUAddress.Size = new System.Drawing.Size(1134, 327);
            this.dgvBUAddress.TabIndex = 0;
            // 
            // btnAddressDelete
            // 
            this.btnAddressDelete.Location = new System.Drawing.Point(192, 20);
            this.btnAddressDelete.Name = "btnAddressDelete";
            this.btnAddressDelete.Size = new System.Drawing.Size(75, 23);
            this.btnAddressDelete.TabIndex = 2;
            this.btnAddressDelete.Text = "Delete";
            this.btnAddressDelete.UseVisualStyleBackColor = true;
            this.btnAddressDelete.Click += new System.EventHandler(this.btnAddressDelete_Click);
            // 
            // btnBUAddressEdit
            // 
            this.btnBUAddressEdit.Location = new System.Drawing.Point(101, 20);
            this.btnBUAddressEdit.Name = "btnBUAddressEdit";
            this.btnBUAddressEdit.Size = new System.Drawing.Size(75, 23);
            this.btnBUAddressEdit.TabIndex = 1;
            this.btnBUAddressEdit.Text = "Edit";
            this.btnBUAddressEdit.UseVisualStyleBackColor = true;
            this.btnBUAddressEdit.Click += new System.EventHandler(this.btnBUAddressEdit_Click);
            // 
            // btnBUAddressAdd
            // 
            this.btnBUAddressAdd.Location = new System.Drawing.Point(10, 20);
            this.btnBUAddressAdd.Name = "btnBUAddressAdd";
            this.btnBUAddressAdd.Size = new System.Drawing.Size(75, 23);
            this.btnBUAddressAdd.TabIndex = 0;
            this.btnBUAddressAdd.Text = "Add";
            this.btnBUAddressAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnBUAddressAdd.UseVisualStyleBackColor = true;
            this.btnBUAddressAdd.Click += new System.EventHandler(this.btnBUAddressAdd_Click);
            // 
            // tcBusinessUnit
            // 
            this.tcBusinessUnit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcBusinessUnit.Controls.Add(this.tpBUAddress);
            this.tcBusinessUnit.Controls.Add(this.tpBUContacts);
            this.tcBusinessUnit.Controls.Add(this.tpBUFunctions);
            this.tcBusinessUnit.Controls.Add(this.tpBUPortfolios);
            this.tcBusinessUnit.Controls.Add(this.tpBUBrokerRates);
            this.tcBusinessUnit.Controls.Add(this.tpBUSettlementInstructions);
            this.tcBusinessUnit.Controls.Add(this.tbBUPartyInfo);
            this.tcBusinessUnit.Controls.Add(this.tbBUInvoiceMapping);
            this.tcBusinessUnit.Location = new System.Drawing.Point(0, 184);
            this.tcBusinessUnit.Name = "tcBusinessUnit";
            this.tcBusinessUnit.SelectedIndex = 0;
            this.tcBusinessUnit.Size = new System.Drawing.Size(1148, 421);
            this.tcBusinessUnit.TabIndex = 20;
            // 
            // txtSchedulingName
            // 
            this.txtSchedulingName.Location = new System.Drawing.Point(625, 131);
            this.txtSchedulingName.Name = "txtSchedulingName";
            this.txtSchedulingName.Size = new System.Drawing.Size(372, 20);
            this.txtSchedulingName.TabIndex = 47;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(524, 133);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(69, 13);
            this.label20.TabIndex = 46;
            this.label20.Text = "Sched Name";
            // 
            // BusinessUnit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1148, 605);
            this.Controls.Add(this.cbAuthoriser);
            this.Controls.Add(this.cbBUParentGroup);
            this.Controls.Add(this.tcBusinessUnit);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.cbBUStatus);
            this.Controls.Add(this.cbBUIntExt);
            this.Controls.Add(this.cbBUPartyClass);
            this.Controls.Add(this.txtBUPartyId);
            this.Controls.Add(this.txtBUGlobalId);
            this.Controls.Add(this.txtBULegalName);
            this.Controls.Add(this.txtBUShortName);
            this.Controls.Add(this.lblAuth);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "BusinessUnit";
            this.Text = "Business Unit";
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.tbBUInvoiceMapping.ResumeLayout(false);
            this.splitContainerInvoiceMapping.Panel1.ResumeLayout(false);
            this.splitContainerInvoiceMapping.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerInvoiceMapping)).EndInit();
            this.splitContainerInvoiceMapping.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewInvoiceMapping)).EndInit();
            this.tbBUPartyInfo.ResumeLayout(false);
            this.tbBUPartyInfo.PerformLayout();
            this.tpBUSettlementInstructions.ResumeLayout(false);
            this.scSettlementInstructions.Panel1.ResumeLayout(false);
            this.scSettlementInstructions.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scSettlementInstructions)).EndInit();
            this.scSettlementInstructions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSettlementInstructions)).EndInit();
            this.tpBUBrokerRates.ResumeLayout(false);
            this.splContBrokerRates.Panel1.ResumeLayout(false);
            this.splContBrokerRates.Panel2.ResumeLayout(false);
            this.splContBrokerRates.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splContBrokerRates)).EndInit();
            this.splContBrokerRates.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBUBrokerRates)).EndInit();
            this.tpBUPortfolios.ResumeLayout(false);
            this.splitContBUPortfolio.Panel1.ResumeLayout(false);
            this.splitContBUPortfolio.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContBUPortfolio)).EndInit();
            this.splitContBUPortfolio.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBUPortfolio)).EndInit();
            this.tpBUFunctions.ResumeLayout(false);
            this.scBUFunctions.Panel1.ResumeLayout(false);
            this.scBUFunctions.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scBUFunctions)).EndInit();
            this.scBUFunctions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBUFunctions)).EndInit();
            this.tpBUContacts.ResumeLayout(false);
            this.scContacts.Panel1.ResumeLayout(false);
            this.scContacts.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scContacts)).EndInit();
            this.scContacts.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBUContacts)).EndInit();
            this.tpBUAddress.ResumeLayout(false);
            this.scAddress.Panel1.ResumeLayout(false);
            this.scAddress.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scAddress)).EndInit();
            this.scAddress.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBUAddress)).EndInit();
            this.tcBusinessUnit.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblAuth;
        private System.Windows.Forms.TextBox txtBUShortName;
        private System.Windows.Forms.TextBox txtBULegalName;
        private System.Windows.Forms.TextBox txtBUGlobalId;
        private System.Windows.Forms.TextBox txtBUPartyId;
        private System.Windows.Forms.ComboBox cbBUPartyClass;
        private System.Windows.Forms.ComboBox cbBUIntExt;
        private System.Windows.Forms.ComboBox cbBUStatus;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox cbBUParentGroup;
        private System.Windows.Forms.ComboBox cbAuthoriser;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.TabPage tbBUInvoiceMapping;
        private System.Windows.Forms.SplitContainer splitContainerInvoiceMapping;
        private System.Windows.Forms.DataGridView dataGridViewInvoiceMapping;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PowerPhysical;
        private System.Windows.Forms.DataGridViewTextBoxColumn PowerFinancial;
        private System.Windows.Forms.DataGridViewTextBoxColumn NatGasPhysical;
        private System.Windows.Forms.DataGridViewTextBoxColumn NatGasFinancial;
        private System.Windows.Forms.DataGridViewTextBoxColumn PowerOptPremium;
        private System.Windows.Forms.DataGridViewTextBoxColumn NatGasOptPremium;
        private System.Windows.Forms.DataGridViewTextBoxColumn Brokerage;
        private System.Windows.Forms.Button btnInvoiceMappingDelete;
        private System.Windows.Forms.Button btnInvoiceMappingEdit;
        private System.Windows.Forms.Button btnInvoiceMappingAdd;
        private System.Windows.Forms.TabPage tbBUPartyInfo;
        private System.Windows.Forms.ComboBox cbConfirmRoute;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cbTaxStatus;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cbTaxDomicile;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtTaxID;
        private System.Windows.Forms.TextBox txtMICCode;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cbRegisteredCountry;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbTradingCountry;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbDoddFrankReportingParty;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox chkOcon;
        private System.Windows.Forms.ComboBox cbDoddFrankType;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chkIcon;
        private System.Windows.Forms.ComboBox cbConfirmingParty;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox chkInvoice;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TabPage tpBUSettlementInstructions;
        private System.Windows.Forms.SplitContainer scSettlementInstructions;
        private System.Windows.Forms.DataGridView dgvSettlementInstructions;
        private System.Windows.Forms.Button btnBUSettInDelete;
        private System.Windows.Forms.Button btnBUSettInEdit;
        private System.Windows.Forms.Button btnBUSettInAdd;
        private System.Windows.Forms.TabPage tpBUBrokerRates;
        private System.Windows.Forms.SplitContainer splContBrokerRates;
        private System.Windows.Forms.DataGridView dgvBUBrokerRates;
        private System.Windows.Forms.TextBox txtBrokerWarning;
        private System.Windows.Forms.Button btnBrokerRatesDelete;
        private System.Windows.Forms.Button btnBUBrokerRatesEdit;
        private System.Windows.Forms.Button btBUBrokRatesAdd;
        private System.Windows.Forms.TabPage tpBUPortfolios;
        private System.Windows.Forms.SplitContainer splitContBUPortfolio;
        private System.Windows.Forms.DataGridView dgvBUPortfolio;
        private System.Windows.Forms.Button btnBUPortfolioSave;
        private System.Windows.Forms.TabPage tpBUFunctions;
        private System.Windows.Forms.SplitContainer scBUFunctions;
        private System.Windows.Forms.DataGridView dgvBUFunctions;
        private System.Windows.Forms.Button btnBUFunctionsSave;
        private System.Windows.Forms.TabPage tpBUContacts;
        private System.Windows.Forms.SplitContainer scContacts;
        private System.Windows.Forms.DataGridView dgvBUContacts;
        private System.Windows.Forms.Button btnDeleteContacts;
        private System.Windows.Forms.Button btnBUContactsEdit;
        private System.Windows.Forms.Button btnBUContactsAdd;
        private System.Windows.Forms.TabPage tpBUAddress;
        private System.Windows.Forms.SplitContainer scAddress;
        private System.Windows.Forms.DataGridView dgvBUAddress;
        private System.Windows.Forms.Button btnAddressDelete;
        private System.Windows.Forms.Button btnBUAddressEdit;
        private System.Windows.Forms.Button btnBUAddressAdd;
        private System.Windows.Forms.TabControl tcBusinessUnit;
        private System.Windows.Forms.TextBox txtBeaconID;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtSchedulingName;
        private System.Windows.Forms.Label label20;
    }
}