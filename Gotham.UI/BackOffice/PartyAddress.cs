﻿using Gotham.Data;
using Gotham.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gotham.AddIn.Party
{
    public partial class PartyAddress : Form
    {
        DataRow EditableInfo { get; set; }
        Int32 PartyId { get; set; }
        Int32 UserId { get; set; }
        bool IsNew { get; set; }

        public PartyAddress(DataRow row, Int32 userID)
        {
            InitializeComponent();
            this.EditableInfo = row;
            this.InitialiseView();
            this.FillEditableInformation();
            this.IsNew = false;
            this.SetCountry();
            this.UserId = userID;
        }

        public PartyAddress(Int32 partyId, Int32 userID)
        {
            InitializeComponent();
            this.PartyId = partyId;
            this.InitialiseView();
            this.IsNew = true;
            this.SetCountry();
            this.UserId = userID;
        }

        private void InitialiseView()
        {
            this.FillComboBoxes();
        }

        public void SetLegalEntityDefaults()
        {
            this.cbAddressType.SelectedValue = AddressTypes.Main;
            this.cbAddressType.Enabled = false; ;
            this.cbAddressIsDefault.SelectedValue = NoYes.Yes;
            this.cbAddressIsDefault.Enabled = false;
        }

        private void FillComboBoxes()
        {
            DataTable addressType = ETRMDataAccess.GetAddressType();
            this.cbAddressType.DataSource = addressType.DefaultView;
            this.cbAddressType.ValueMember = AddressNames.PartyAddressTypeID;
            this.cbAddressType.DisplayMember = AddressNames.PartyAddressTypeName;
            this.cbAddressType.BindingContext = this.BindingContext;

            DataTable isDefault = ETRMDataAccess.GetYN();
            this.cbAddressIsDefault.DataSource = isDefault.DefaultView;
            this.cbAddressIsDefault.ValueMember = AddressNames.IsDefaultValue;
            this.cbAddressIsDefault.DisplayMember = AddressNames.IsDefaultShow;
            this.cbAddressIsDefault.BindingContext = this.BindingContext;

            DataTable states = ETRMDataAccess.GetStates();
            this.cbAddState.DataSource = states.DefaultView;
            this.cbAddState.ValueMember = AddressNames.StatesID;
            this.cbAddState.DisplayMember = AddressNames.Name;
            this.cbAddState.BindingContext = this.BindingContext;

            DataTable countries = ETRMDataAccess.GetCountries();
            this.cbAddCountry.DataSource = countries.DefaultView;
            this.cbAddCountry.ValueMember = AddressNames.CountryID;
            this.cbAddCountry.DisplayMember = AddressNames.ShortName;
            this.cbAddCountry.BindingContext = this.BindingContext;
        }

        private void FillEditableInformation()
        {
            if (this.EditableInfo != null)
            {
                Int32 partyId = Convert.ToInt32(this.EditableInfo[AddressNames.PartyID]);
                Int32 partyAddId = Convert.ToInt32(this.EditableInfo[AddressNames.PartyAddressID]);
                DataTable rawData = ETRMDataAccess.GetPartyAddressRecord(partyId, partyAddId);

                if (rawData != null)
                {
                    this.cbAddressIsDefault.SelectedValue = Convert.ToInt32(rawData.Rows[0][AddressNames.DefaultID]);

                    this.cbAddressType.SelectedValue = Convert.ToInt32(rawData.Rows[0][AddressNames.PartyAddressTypeID]);
                    this.txtCity.Text = Convert.ToString(rawData.Rows[0][AddressNames.City]);
                    try
                    {
                        this.cbAddState.SelectedValue = Convert.ToInt32(rawData.Rows[0][AddressNames.StateID]);
                    }catch { }
                    this.cbAddCountry.SelectedValue = Convert.ToInt32(rawData.Rows[0][AddressNames.CountryID]);

                    this.txtAddress1.Text = Convert.ToString(rawData.Rows[0][AddressNames.AddressLine1]);
                    this.txtAddress2.Text = Convert.ToString(rawData.Rows[0][AddressNames.AddressLine2]);
                    this.txtAddress3.Text = Convert.ToString(rawData.Rows[0][AddressNames.AddressLine3]);
                    this.txtAddress4.Text = Convert.ToString(rawData.Rows[0][AddressNames.AddressLine4]);
                    this.txtAddress5.Text = Convert.ToString(rawData.Rows[0][AddressNames.AddressLine5]);

                    this.txtMailCode.Text = Convert.ToString(rawData.Rows[0][AddressNames.MailCode]);

                    this.txtGroupEmail.Text = Convert.ToString(rawData.Rows[0][AddressNames.GroupEmail]);
                    this.txtGroupPhone.Text = Convert.ToString(rawData.Rows[0][AddressNames.GroupPhone]);
                    this.txtGroupFax.Text = Convert.ToString(rawData.Rows[0][AddressNames.GroupFax]);
                    this.txtDesc.Text = Convert.ToString(rawData.Rows[0][AddressNames.Description]);
                }
            }
        }

        private void btnAddressDetailsCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddressDetailsSave_Click(object sender, EventArgs e)
        {
            if (this.IsAddressValid())
            {
                Domain.Entities.Address address = new Domain.Entities.Address();

                address.AddressType = Convert.ToInt32(this.cbAddressType.SelectedValue);

                address.AddressDefault = Convert.ToInt32(this.cbAddressIsDefault.SelectedValue);

                address.AddressLine1 = txtAddress1.Text;
                address.AddressLine2 = txtAddress2.Text;
                address.AddressLine3 = txtAddress3.Text;
                address.AddressLine4 = txtAddress4.Text;
                address.AddressLine5 = txtAddress5.Text;

                address.CityId = Convert.ToString(this.txtCity.Text);
                address.StateId = Convert.ToInt32(this.cbAddState.SelectedValue);
                address.CountryId = Convert.ToInt32(this.cbAddCountry.SelectedValue);

                //if (this.cbContact.SelectedValue != null)
                //{
                //    address.ContactId = Convert.ToInt32(this.cbContact.SelectedValue);
                //}

                address.GroupEmail = txtGroupEmail.Text;
                address.GroupPhone = txtGroupPhone.Text;
                address.MailCode = txtMailCode.Text;
                address.GroupFax = txtGroupFax.Text;
                address.Description = txtDesc.Text;
                address.UpdateUser = this.UserId;

                //Send it to the database...
                if (IsNew)
                {
                    address.PartyID = this.PartyId;
                    address.PartyAddressID = ETRMDataAccess.MaxAddressId() + 1;
                    ETRMDataAccess.SaveAddress(address);
                }
                else
                {
                    address.PartyAddressID = Convert.ToInt32(this.EditableInfo[AddressNames.PartyAddressID]);
                    address.PartyID = Convert.ToInt32(this.EditableInfo[AddressNames.PartyID]);
                    ETRMDataAccess.UpdateAddress(address);
                }

                this.Close();
            }
        }

        private bool IsAddressValid()
        {
            return true;
        }

        private void cbAddState_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SetCountry();
        }

        private void SetCountry()
        {
            try
            {
                Int32 state = Convert.ToInt32(cbAddState.SelectedValue);
                Int32 countryId = ETRMDataAccess.GetCountryFromState(state);

                this.cbAddCountry.SelectedValue = countryId;
            }
            catch { }
        }
    }
}

