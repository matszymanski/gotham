﻿namespace Gotham.AddIn.Party
{
    partial class Party
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tcParty = new System.Windows.Forms.TabControl();
            this.tabPageAddress = new System.Windows.Forms.TabPage();
            this.splitContAddresses = new System.Windows.Forms.SplitContainer();
            this.dgvAddress = new System.Windows.Forms.DataGridView();
            this.btnAddressDelete = new System.Windows.Forms.Button();
            this.btnAddEdit = new System.Windows.Forms.Button();
            this.btnAddsAdd = new System.Windows.Forms.Button();
            this.tabPageBusinessUnit = new System.Windows.Forms.TabPage();
            this.dgvBusinessUnits = new System.Windows.Forms.DataGridView();
            this.tabPageAgreements = new System.Windows.Forms.TabPage();
            this.scAgreements = new System.Windows.Forms.SplitContainer();
            this.dgvAgreements = new System.Windows.Forms.DataGridView();
            this.btnAgreementsDelete = new System.Windows.Forms.Button();
            this.btAgreementsAdd = new System.Windows.Forms.Button();
            this.btAgreementsModify = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblAuth = new System.Windows.Forms.Label();
            this.txtShortName = new System.Windows.Forms.TextBox();
            this.txtLongName = new System.Windows.Forms.TextBox();
            this.txtSpiderId = new System.Windows.Forms.TextBox();
            this.txtPartyId = new System.Windows.Forms.TextBox();
            this.cbPartyClass = new System.Windows.Forms.ComboBox();
            this.cbIntExt = new System.Windows.Forms.ComboBox();
            this.cbStatus = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.btnPartySave = new System.Windows.Forms.Button();
            this.txtLEI = new System.Windows.Forms.TextBox();
            this.LEI = new System.Windows.Forms.Label();
            this.cbAuthoriser = new System.Windows.Forms.ComboBox();
            this.chkCommercialParticipant = new System.Windows.Forms.CheckBox();
            this.tcParty.SuspendLayout();
            this.tabPageAddress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContAddresses)).BeginInit();
            this.splitContAddresses.Panel1.SuspendLayout();
            this.splitContAddresses.Panel2.SuspendLayout();
            this.splitContAddresses.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAddress)).BeginInit();
            this.tabPageBusinessUnit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBusinessUnits)).BeginInit();
            this.tabPageAgreements.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scAgreements)).BeginInit();
            this.scAgreements.Panel1.SuspendLayout();
            this.scAgreements.Panel2.SuspendLayout();
            this.scAgreements.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAgreements)).BeginInit();
            this.SuspendLayout();
            // 
            // tcParty
            // 
            this.tcParty.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcParty.Controls.Add(this.tabPageAddress);
            this.tcParty.Controls.Add(this.tabPageBusinessUnit);
            this.tcParty.Controls.Add(this.tabPageAgreements);
            this.tcParty.Location = new System.Drawing.Point(0, 184);
            this.tcParty.Name = "tcParty";
            this.tcParty.SelectedIndex = 0;
            this.tcParty.Size = new System.Drawing.Size(1163, 425);
            this.tcParty.TabIndex = 0;
            this.tcParty.SelectedIndexChanged += new System.EventHandler(this.tcParty_SelectedIndexChanged);
            // 
            // tabPageAddress
            // 
            this.tabPageAddress.Controls.Add(this.splitContAddresses);
            this.tabPageAddress.Location = new System.Drawing.Point(4, 22);
            this.tabPageAddress.Name = "tabPageAddress";
            this.tabPageAddress.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageAddress.Size = new System.Drawing.Size(1155, 399);
            this.tabPageAddress.TabIndex = 0;
            this.tabPageAddress.Text = "Address";
            this.tabPageAddress.UseVisualStyleBackColor = true;
            // 
            // splitContAddresses
            // 
            this.splitContAddresses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContAddresses.Location = new System.Drawing.Point(3, 3);
            this.splitContAddresses.Name = "splitContAddresses";
            this.splitContAddresses.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContAddresses.Panel1
            // 
            this.splitContAddresses.Panel1.Controls.Add(this.dgvAddress);
            // 
            // splitContAddresses.Panel2
            // 
            this.splitContAddresses.Panel2.Controls.Add(this.btnAddressDelete);
            this.splitContAddresses.Panel2.Controls.Add(this.btnAddEdit);
            this.splitContAddresses.Panel2.Controls.Add(this.btnAddsAdd);
            this.splitContAddresses.Size = new System.Drawing.Size(1149, 393);
            this.splitContAddresses.SplitterDistance = 327;
            this.splitContAddresses.TabIndex = 1;
            // 
            // dgvAddress
            // 
            this.dgvAddress.AllowUserToAddRows = false;
            this.dgvAddress.AllowUserToDeleteRows = false;
            this.dgvAddress.AllowUserToResizeRows = false;
            this.dgvAddress.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAddress.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAddress.Location = new System.Drawing.Point(0, 0);
            this.dgvAddress.Name = "dgvAddress";
            this.dgvAddress.ReadOnly = true;
            this.dgvAddress.RowHeadersVisible = false;
            this.dgvAddress.Size = new System.Drawing.Size(1149, 327);
            this.dgvAddress.TabIndex = 0;
            // 
            // btnAddressDelete
            // 
            this.btnAddressDelete.Location = new System.Drawing.Point(192, 20);
            this.btnAddressDelete.Name = "btnAddressDelete";
            this.btnAddressDelete.Size = new System.Drawing.Size(75, 23);
            this.btnAddressDelete.TabIndex = 27;
            this.btnAddressDelete.Text = "Delete";
            this.btnAddressDelete.UseVisualStyleBackColor = true;
            this.btnAddressDelete.Click += new System.EventHandler(this.btnAddressDelete_Click);
            // 
            // btnAddEdit
            // 
            this.btnAddEdit.Location = new System.Drawing.Point(101, 20);
            this.btnAddEdit.Name = "btnAddEdit";
            this.btnAddEdit.Size = new System.Drawing.Size(75, 23);
            this.btnAddEdit.TabIndex = 26;
            this.btnAddEdit.Text = "Edit";
            this.btnAddEdit.UseVisualStyleBackColor = true;
            this.btnAddEdit.Click += new System.EventHandler(this.btnAddEdit_Click);
            // 
            // btnAddsAdd
            // 
            this.btnAddsAdd.Location = new System.Drawing.Point(10, 20);
            this.btnAddsAdd.Name = "btnAddsAdd";
            this.btnAddsAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAddsAdd.TabIndex = 0;
            this.btnAddsAdd.Text = "Add";
            this.btnAddsAdd.UseVisualStyleBackColor = true;
            this.btnAddsAdd.Click += new System.EventHandler(this.btnAddsAdd_Click);
            // 
            // tabPageBusinessUnit
            // 
            this.tabPageBusinessUnit.Controls.Add(this.dgvBusinessUnits);
            this.tabPageBusinessUnit.Location = new System.Drawing.Point(4, 22);
            this.tabPageBusinessUnit.Name = "tabPageBusinessUnit";
            this.tabPageBusinessUnit.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageBusinessUnit.Size = new System.Drawing.Size(1155, 399);
            this.tabPageBusinessUnit.TabIndex = 1;
            this.tabPageBusinessUnit.Text = "Business Units";
            this.tabPageBusinessUnit.UseVisualStyleBackColor = true;
            // 
            // dgvBusinessUnits
            // 
            this.dgvBusinessUnits.AllowUserToAddRows = false;
            this.dgvBusinessUnits.AllowUserToDeleteRows = false;
            this.dgvBusinessUnits.AllowUserToResizeRows = false;
            this.dgvBusinessUnits.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvBusinessUnits.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBusinessUnits.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBusinessUnits.Location = new System.Drawing.Point(3, 3);
            this.dgvBusinessUnits.Name = "dgvBusinessUnits";
            this.dgvBusinessUnits.ReadOnly = true;
            this.dgvBusinessUnits.RowHeadersVisible = false;
            this.dgvBusinessUnits.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBusinessUnits.Size = new System.Drawing.Size(1149, 393);
            this.dgvBusinessUnits.TabIndex = 0;
            // 
            // tabPageAgreements
            // 
            this.tabPageAgreements.Controls.Add(this.scAgreements);
            this.tabPageAgreements.Location = new System.Drawing.Point(4, 22);
            this.tabPageAgreements.Name = "tabPageAgreements";
            this.tabPageAgreements.Size = new System.Drawing.Size(1155, 399);
            this.tabPageAgreements.TabIndex = 2;
            this.tabPageAgreements.Text = "Agreements";
            this.tabPageAgreements.UseVisualStyleBackColor = true;
            // 
            // scAgreements
            // 
            this.scAgreements.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scAgreements.Location = new System.Drawing.Point(0, 0);
            this.scAgreements.Name = "scAgreements";
            this.scAgreements.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scAgreements.Panel1
            // 
            this.scAgreements.Panel1.Controls.Add(this.dgvAgreements);
            // 
            // scAgreements.Panel2
            // 
            this.scAgreements.Panel2.Controls.Add(this.btnAgreementsDelete);
            this.scAgreements.Panel2.Controls.Add(this.btAgreementsAdd);
            this.scAgreements.Panel2.Controls.Add(this.btAgreementsModify);
            this.scAgreements.Size = new System.Drawing.Size(1155, 399);
            this.scAgreements.SplitterDistance = 332;
            this.scAgreements.TabIndex = 1;
            // 
            // dgvAgreements
            // 
            this.dgvAgreements.AllowUserToAddRows = false;
            this.dgvAgreements.AllowUserToDeleteRows = false;
            this.dgvAgreements.AllowUserToResizeRows = false;
            this.dgvAgreements.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAgreements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAgreements.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAgreements.Location = new System.Drawing.Point(0, 0);
            this.dgvAgreements.Name = "dgvAgreements";
            this.dgvAgreements.ReadOnly = true;
            this.dgvAgreements.RowHeadersVisible = false;
            this.dgvAgreements.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAgreements.Size = new System.Drawing.Size(1155, 332);
            this.dgvAgreements.TabIndex = 0;
            // 
            // btnAgreementsDelete
            // 
            this.btnAgreementsDelete.Location = new System.Drawing.Point(192, 20);
            this.btnAgreementsDelete.Name = "btnAgreementsDelete";
            this.btnAgreementsDelete.Size = new System.Drawing.Size(75, 23);
            this.btnAgreementsDelete.TabIndex = 2;
            this.btnAgreementsDelete.Text = "Delete";
            this.btnAgreementsDelete.UseVisualStyleBackColor = true;
            this.btnAgreementsDelete.Click += new System.EventHandler(this.btnAgreementsDelete_Click);
            // 
            // btAgreementsAdd
            // 
            this.btAgreementsAdd.Location = new System.Drawing.Point(10, 20);
            this.btAgreementsAdd.Name = "btAgreementsAdd";
            this.btAgreementsAdd.Size = new System.Drawing.Size(75, 23);
            this.btAgreementsAdd.TabIndex = 1;
            this.btAgreementsAdd.Text = "Add";
            this.btAgreementsAdd.UseVisualStyleBackColor = true;
            this.btAgreementsAdd.Click += new System.EventHandler(this.btAgreementsAdd_Click);
            // 
            // btAgreementsModify
            // 
            this.btAgreementsModify.Location = new System.Drawing.Point(101, 20);
            this.btAgreementsModify.Name = "btAgreementsModify";
            this.btAgreementsModify.Size = new System.Drawing.Size(75, 23);
            this.btAgreementsModify.TabIndex = 0;
            this.btAgreementsModify.Text = "Modify";
            this.btAgreementsModify.UseVisualStyleBackColor = true;
            this.btAgreementsModify.Click += new System.EventHandler(this.btAgreementsModify_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Short Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Legal Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Spider ID";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Party ID";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(590, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "LE / BU";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(590, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Int / Ext";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(590, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Status";
            // 
            // lblAuth
            // 
            this.lblAuth.AutoSize = true;
            this.lblAuth.Location = new System.Drawing.Point(590, 100);
            this.lblAuth.Name = "lblAuth";
            this.lblAuth.Size = new System.Drawing.Size(54, 13);
            this.lblAuth.TabIndex = 9;
            this.lblAuth.Text = "Authoriser";
            // 
            // txtShortName
            // 
            this.txtShortName.Location = new System.Drawing.Point(130, 10);
            this.txtShortName.Name = "txtShortName";
            this.txtShortName.Size = new System.Drawing.Size(406, 20);
            this.txtShortName.TabIndex = 10;
            // 
            // txtLongName
            // 
            this.txtLongName.Location = new System.Drawing.Point(130, 39);
            this.txtLongName.Name = "txtLongName";
            this.txtLongName.Size = new System.Drawing.Size(406, 20);
            this.txtLongName.TabIndex = 11;
            // 
            // txtSpiderId
            // 
            this.txtSpiderId.Location = new System.Drawing.Point(130, 67);
            this.txtSpiderId.Name = "txtSpiderId";
            this.txtSpiderId.Size = new System.Drawing.Size(406, 20);
            this.txtSpiderId.TabIndex = 12;
            // 
            // txtPartyId
            // 
            this.txtPartyId.Location = new System.Drawing.Point(130, 128);
            this.txtPartyId.Name = "txtPartyId";
            this.txtPartyId.Size = new System.Drawing.Size(406, 20);
            this.txtPartyId.TabIndex = 13;
            // 
            // cbPartyClass
            // 
            this.cbPartyClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPartyClass.FormattingEnabled = true;
            this.cbPartyClass.Location = new System.Drawing.Point(652, 9);
            this.cbPartyClass.Name = "cbPartyClass";
            this.cbPartyClass.Size = new System.Drawing.Size(373, 21);
            this.cbPartyClass.TabIndex = 19;
            // 
            // cbIntExt
            // 
            this.cbIntExt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbIntExt.FormattingEnabled = true;
            this.cbIntExt.Location = new System.Drawing.Point(652, 38);
            this.cbIntExt.Name = "cbIntExt";
            this.cbIntExt.Size = new System.Drawing.Size(373, 21);
            this.cbIntExt.TabIndex = 20;
            // 
            // cbStatus
            // 
            this.cbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStatus.FormattingEnabled = true;
            this.cbStatus.Location = new System.Drawing.Point(652, 67);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(373, 21);
            this.cbStatus.TabIndex = 21;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(1061, 121);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 24;
            this.button3.Text = "Close";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnPartySave
            // 
            this.btnPartySave.Location = new System.Drawing.Point(1061, 92);
            this.btnPartySave.Name = "btnPartySave";
            this.btnPartySave.Size = new System.Drawing.Size(75, 23);
            this.btnPartySave.TabIndex = 25;
            this.btnPartySave.Text = "Save";
            this.btnPartySave.UseVisualStyleBackColor = true;
            this.btnPartySave.Click += new System.EventHandler(this.btnPartySave_Click);
            // 
            // txtLEI
            // 
            this.txtLEI.Location = new System.Drawing.Point(130, 97);
            this.txtLEI.Name = "txtLEI";
            this.txtLEI.Size = new System.Drawing.Size(406, 20);
            this.txtLEI.TabIndex = 26;
            // 
            // LEI
            // 
            this.LEI.AutoSize = true;
            this.LEI.Location = new System.Drawing.Point(23, 100);
            this.LEI.Name = "LEI";
            this.LEI.Size = new System.Drawing.Size(23, 13);
            this.LEI.TabIndex = 27;
            this.LEI.Text = "LEI";
            // 
            // cbAuthoriser
            // 
            this.cbAuthoriser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAuthoriser.FormattingEnabled = true;
            this.cbAuthoriser.Location = new System.Drawing.Point(652, 97);
            this.cbAuthoriser.Name = "cbAuthoriser";
            this.cbAuthoriser.Size = new System.Drawing.Size(373, 21);
            this.cbAuthoriser.TabIndex = 28;
            // 
            // chkCommercialParticipant
            // 
            this.chkCommercialParticipant.AutoSize = true;
            this.chkCommercialParticipant.Location = new System.Drawing.Point(593, 130);
            this.chkCommercialParticipant.Name = "chkCommercialParticipant";
            this.chkCommercialParticipant.Size = new System.Drawing.Size(133, 17);
            this.chkCommercialParticipant.TabIndex = 29;
            this.chkCommercialParticipant.Text = "Commercial Participant";
            this.chkCommercialParticipant.UseVisualStyleBackColor = true;
            // 
            // Party
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1163, 609);
            this.Controls.Add(this.chkCommercialParticipant);
            this.Controls.Add(this.cbAuthoriser);
            this.Controls.Add(this.LEI);
            this.Controls.Add(this.txtLEI);
            this.Controls.Add(this.btnPartySave);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.cbStatus);
            this.Controls.Add(this.cbIntExt);
            this.Controls.Add(this.cbPartyClass);
            this.Controls.Add(this.txtPartyId);
            this.Controls.Add(this.txtSpiderId);
            this.Controls.Add(this.txtLongName);
            this.Controls.Add(this.txtShortName);
            this.Controls.Add(this.lblAuth);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tcParty);
            this.Name = "Party";
            this.Text = "Legal Entity";
            this.tcParty.ResumeLayout(false);
            this.tabPageAddress.ResumeLayout(false);
            this.splitContAddresses.Panel1.ResumeLayout(false);
            this.splitContAddresses.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContAddresses)).EndInit();
            this.splitContAddresses.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAddress)).EndInit();
            this.tabPageBusinessUnit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBusinessUnits)).EndInit();
            this.tabPageAgreements.ResumeLayout(false);
            this.scAgreements.Panel1.ResumeLayout(false);
            this.scAgreements.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scAgreements)).EndInit();
            this.scAgreements.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAgreements)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tcParty;
        private System.Windows.Forms.TabPage tabPageAddress;
        private System.Windows.Forms.TabPage tabPageBusinessUnit;
        private System.Windows.Forms.TabPage tabPageAgreements;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblAuth;
        private System.Windows.Forms.TextBox txtShortName;
        private System.Windows.Forms.TextBox txtLongName;
        private System.Windows.Forms.TextBox txtSpiderId;
        private System.Windows.Forms.TextBox txtPartyId;
        private System.Windows.Forms.ComboBox cbPartyClass;
        private System.Windows.Forms.ComboBox cbIntExt;
        private System.Windows.Forms.ComboBox cbStatus;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DataGridView dgvAddress;
        private System.Windows.Forms.DataGridView dgvBusinessUnits;
        private System.Windows.Forms.DataGridView dgvAgreements;
        private System.Windows.Forms.Button btnPartySave;
        private System.Windows.Forms.SplitContainer splitContAddresses;
        private System.Windows.Forms.Button btnAddEdit;
        private System.Windows.Forms.Button btnAddsAdd;
        private System.Windows.Forms.SplitContainer scAgreements;
        private System.Windows.Forms.Button btAgreementsModify;
        private System.Windows.Forms.Button btAgreementsAdd;
        private System.Windows.Forms.TextBox txtLEI;
        private System.Windows.Forms.Label LEI;
        private System.Windows.Forms.Button btnAddressDelete;
        private System.Windows.Forms.Button btnAgreementsDelete;
        private System.Windows.Forms.ComboBox cbAuthoriser;
        private System.Windows.Forms.CheckBox chkCommercialParticipant;
    }
}