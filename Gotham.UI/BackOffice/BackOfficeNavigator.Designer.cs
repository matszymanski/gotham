﻿namespace Strazze.AddIn.Forms
{
    partial class BackOfficeNavigator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Business Unit");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Legal Entity");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Back Office", new System.Windows.Forms.TreeNode[] {
            treeNode4,
            treeNode5});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BackOfficeNavigator));
            this.businessUnitsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cashTrades = new Strazze.AddIn.CashTrades();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.treeView = new System.Windows.Forms.TreeView();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.businessUnitsTableAdapter = new Strazze.AddIn.CashTradesTableAdapters.BusinessUnitsTableAdapter();
            this.tableAdapterManager = new Strazze.AddIn.CashTradesTableAdapters.TableAdapterManager();
            ((System.ComponentModel.ISupportInitialize)(this.businessUnitsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cashTrades)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // businessUnitsBindingSource
            // 
            this.businessUnitsBindingSource.DataMember = "BusinessUnits";
            this.businessUnitsBindingSource.DataSource = this.cashTrades;
            // 
            // cashTrades
            // 
            this.cashTrades.DataSetName = "CashTrades";
            this.cashTrades.EnforceConstraints = false;
            this.cashTrades.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.treeView);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.dataGridView);
            this.splitContainer.Panel2.Controls.Add(this.menuStrip);
            this.splitContainer.Size = new System.Drawing.Size(872, 621);
            this.splitContainer.SplitterDistance = 117;
            this.splitContainer.TabIndex = 0;
            // 
            // treeView
            // 
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.ImageIndex = 0;
            this.treeView.ImageList = this.imageList;
            this.treeView.Location = new System.Drawing.Point(0, 0);
            this.treeView.Name = "treeView";
            treeNode4.Name = "ngBU";
            treeNode4.Text = "Business Unit";
            treeNode5.Name = "ngLE";
            treeNode5.Text = "Legal Entity";
            treeNode6.Name = "ndBO";
            treeNode6.Text = "Back Office";
            this.treeView.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode6});
            this.treeView.SelectedImageIndex = 0;
            this.treeView.Size = new System.Drawing.Size(117, 621);
            this.treeView.TabIndex = 0;
            this.treeView.AfterCollapse += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterCollapse);
            this.treeView.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterExpand);
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
            this.treeView.Resize += new System.EventHandler(this.treeView_Resize);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "BU.ico");
            this.imageList.Images.SetKeyName(1, "LE.ico");
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AllowUserToResizeRows = false;
            this.dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(0, 24);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.Size = new System.Drawing.Size(751, 597);
            this.dataGridView.TabIndex = 0;
            this.dataGridView.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellContentDoubleClick);
            // 
            // menuStrip
            // 
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(751, 24);
            this.menuStrip.TabIndex = 1;
            this.menuStrip.Text = "menuStrip1";
            // 
            // businessUnitsTableAdapter
            // 
            this.businessUnitsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.CashflowTypeTableAdapter = null;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.CurrencyTableAdapter = null;
            this.tableAdapterManager.EventStatusTableAdapter = null;
            this.tableAdapterManager.PartyTableAdapter = null;
            this.tableAdapterManager.PortfolioSubStrategyTableAdapter = null;
            this.tableAdapterManager.PortfolioTableAdapter = null;
            this.tableAdapterManager.TradeEventTableAdapter = null;
            this.tableAdapterManager.TradeLegTableAdapter = null;
            this.tableAdapterManager.TradeTableAdapter = null;
            this.tableAdapterManager.TradeTypeTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Strazze.AddIn.CashTradesTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // BackOfficeNavigator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 621);
            this.Controls.Add(this.splitContainer);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "BackOfficeNavigator";
            this.Text = "Back Office Navigator";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.BackOfficeNavigator_Load);
            ((System.ComponentModel.ISupportInitialize)(this.businessUnitsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cashTrades)).EndInit();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.BindingSource businessUnitsBindingSource;
        private CashTrades cashTrades;
        private CashTradesTableAdapters.BusinessUnitsTableAdapter businessUnitsTableAdapter;
        private CashTradesTableAdapters.TableAdapterManager tableAdapterManager;
    }
}