﻿using Strazze.AddIn.Controls;
using Strazze.Data;
using Strazze.Domain;
using Strazze.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Strazze.AddIn.Forms
{
    public partial class LEAgreements : Form
    {
        public Int32 PartyID { get; set; }
        public Int32 PartyAgreementID { get; set; }

        public LEAgreements(Int32 partyId)
        {
            this.PartyID = partyId;
            this.PartyAgreementID = ETRMDataAccess.GetNextAgreementID();
            InitializeComponent();
            this.InitialiseView();
        }
        public LEAgreements(Int32 partyId, Int32 partyAgreementId)
        {
            this.PartyID = partyId;
            this.PartyAgreementID = partyAgreementId;
            InitializeComponent();
            this.InitialiseView();
        }

        public LEAgreements()
        {
            InitializeComponent();
            this.InitialiseView();
        }

        private void InitialiseView()
        {
            this.SetAgreementID();
            this.FillComboBoxes();
        }

        private void SetAgreementID()
        {
            this.txtAgreementId.Text = this.PartyAgreementID.ToString();
            this.txtAgreementId.Enabled = false;
            this.txtAgreementId.ReadOnly = true;
        }

        private void FillComboBoxes()
        {
            this.FillInternalLE();
            this.FillExternalLE();
            this.FillConditions();
        }

        private void FillInternalLE()
        {
            DataTable le = ETRMDataAccess.GetLegalEntities(PartyClass.Internal);

            if(!ETRMDataAccess.IsEmpty(le))
            {
                this.cbInternalLE.DataSource = le.DefaultView;
                this.cbInternalLE.ValueMember = PartyClass.PartyID;
                this.cbInternalLE.DisplayMember = PartyClass.LongName;
                this.cbInternalLE.BindingContext = this.BindingContext;
            }
        }

        private void FillExternalLE()
        {
            DataTable le = ETRMDataAccess.GetLegalEntities(PartyClass.External);

            if (!ETRMDataAccess.IsEmpty(le))
            {
                this.cbExternalLE.DataSource = le.DefaultView;
                this.cbExternalLE.ValueMember = PartyClass.PartyID;
                this.cbExternalLE.DisplayMember = PartyClass.LongName;
                this.cbExternalLE.BindingContext = this.BindingContext;
            }
        }

        private void FillConditions()
        {
            DataTable le = ETRMDataAccess.GetAgreementConditions();

            if (!ETRMDataAccess.IsEmpty(le))
            {
                this.cbAgreementCondition.DataSource = le.DefaultView;
                this.cbAgreementCondition.ValueMember = AgreementConditions.PartyAgreementConditionID;
                this.cbAgreementCondition.DisplayMember = AgreementConditions.PartyAgreementCondition;
                this.cbAgreementCondition.BindingContext = this.BindingContext;
            }
        }

        private void TestData()
        {
            this.lbInternalBU.Items.Add("IB1");
            this.lbInternalBU.Items.Add("IB2");
            this.lbInternalBU.Items.Add("IB3");
            this.lbInternalBU.Items.Add("IB4");
            this.lbInternalBU.Items.Add("IB5");
            this.lbInternalBU.Items.Add("IB6");
            this.lbInternalBU.Items.Add("IB7");
            this.lbInternalBU.Items.Add("IB11");
            this.lbInternalBU.Items.Add("IB21");
            this.lbInternalBU.Items.Add("IB31");
            this.lbInternalBU.Items.Add("IB41");
            this.lbInternalBU.Items.Add("IB51");
            this.lbInternalBU.Items.Add("IB61");
            this.lbInternalBU.Items.Add("IB71");


            this.lbExternalBU.Items.Add("IB1");
            this.lbExternalBU.Items.Add("IB2");
            this.lbExternalBU.Items.Add("IB3");
            this.lbExternalBU.Items.Add("IB4");
            this.lbExternalBU.Items.Add("IB5");
            this.lbExternalBU.Items.Add("IB6");
            this.lbExternalBU.Items.Add("IB7");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //Validate()
            //Save()
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btManageInternalBU_Click(object sender, EventArgs e)
        {
            DataTable internalBusinessUnits = ETRMDataAccess.GetBusinessUnitsRelated(Convert.ToInt32(this.cbInternalLE.SelectedValue), PartyClass.Internal, PartyClass.BU);
            IList<SelectionItems> availableParties = ETRMDataAccess.CreateSelectionItemsList(internalBusinessUnits, PartyClass.PartyID, PartyClass.LongName);
            IList<SelectionItems> selectedParties = new List<SelectionItems>();
            ConditionSelector cs = new ConditionSelector(availableParties, selectedParties);
            cs.Title = "Selection of associated Internal Business Units";
            cs.ShowDialog();
            IList<SelectionItems> selectedInternalBU = cs.SelectedItems;
            this.FillList(selectedInternalBU, lbInternalBU);
        }

        private void btManageExternalBU_Click(object sender, EventArgs e)
        {
            DataTable externalBusinessUnits = ETRMDataAccess.GetBusinessUnitsRelated(Convert.ToInt32(this.cbExternalLE.SelectedValue), PartyClass.External, PartyClass.BU);
            IList<SelectionItems> availableParties = ETRMDataAccess.CreateSelectionItemsList(externalBusinessUnits, PartyClass.PartyID, PartyClass.LongName);
            IList<SelectionItems> selectedParties = new List<SelectionItems>();
            ConditionSelector cs = new ConditionSelector(availableParties, selectedParties);
            cs.Title = "Selection of associated External Business Units";
            cs.ShowDialog();
            IList<SelectionItems> selectedExternalBU = cs.SelectedItems;
            this.FillList(selectedExternalBU, lbExternalBU);
        }

        private void btAgrNewCondition_Click(object sender, EventArgs e)
        {
            LEAgreementCondition leac = new LEAgreementCondition();
            leac.ShowDialog();
        }

        private void btAgrConditionDetails_Click(object sender, EventArgs e)
        {
            if(this.cbAgreementCondition.SelectedValue != null)
            {
                Int32 conditionId = Convert.ToInt32(this.cbAgreementCondition.SelectedValue);
                DataTable conditionDetails = ETRMDataAccess.GetAgreementConditions(conditionId);
            }
            else
            {
                MessageBox.Show("Please select a condition first...");
            }
        }

        private void FillList(IList<SelectionItems> items, ListBox component)
        {
            this.CleanUp(component);
            component.DisplayMember = "Name";
            component.DataSource = items;
            component.ClearSelected();
        }

        private void CleanUp(ListBox component)
        {
            //component.DataSource = null;
            //component.Items.Clear();
            component.Refresh();
        }

        private void FillListboxElements(ListBox destination, IList<SelectionItems> elements)
        {

        }

    }
}
