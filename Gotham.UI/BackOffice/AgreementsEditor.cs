﻿using Gotham.Data;
using Gotham.Domain;
using Gotham.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gotham.AddIn.Party
{
    public partial class AgreementsEditor : Form
    {
        ErrorProvider epAgreement;
        private bool IsNew { get; set; }
        private Int32 AgreementId { get; set; }
        private Int32 PartyId { get; set; }
        private DataTable HeaderInfo { get; set; }

        public AgreementsEditor()
        {
            InitializeComponent();
            this.InitialiseView();
            this.IsNew = true;
            this.epAgreement = new ErrorProvider();
        }

        public AgreementsEditor(Int32 partyId)
        {
            InitializeComponent();
            this.PartyId = partyId;
            this.AgreementId = ETRMDataAccess.GetNextAgreementID();
            this.IsNew = true;
            this.InitialiseView();
            this.epAgreement = new ErrorProvider();
        }

        public AgreementsEditor(Int32 partyId, Int32 agreementId)
        {
            InitializeComponent();
            this.PartyId = partyId;
            this.AgreementId = agreementId;
            this.IsNew = false;
            this.InitialiseView();
            this.FillHeaderInfo();
            this.epAgreement = new ErrorProvider();
        }

        private void InitialiseView()
        {
            this.FillInternalLE();
            this.FillExternalLE();
            this.FillAgreementStatus();
            this.FillCalculationAgent();
            this.FillVersionYear();
            this.FillAgreementType();
            this.FillCurrencies();
            this.FillCommodities();
            this.FillSettlements();
            this.FillConfirmRoute();
            this.HideColumns();
            this.SetAgreementID();
            this.SetDefaults();
        }

        private void SetDefaults()
        {
            this.dtpEffectiveEnd.Value = new DateTime(2099, 12, 31);
        }
        private void SetAgreementID()
        {
            this.txtAgreementID.ReadOnly = false;
            this.txtAgreementID.Text = this.AgreementId.ToString();
            this.txtAgreementID.ReadOnly = true;
        }

        private void FillHeaderInfo()
        {
            this.HeaderInfo = ETRMDataAccess.GetAgreementHeaderInfo(this.AgreementId);
            if (!ETRMDataAccess.IsEmpty(this.HeaderInfo))
            {
                this.txtAgreementName.Text = Convert.ToString(HeaderInfo.Rows[0][AgHelper.PartyAgreementName]);
                this.txtCptyAgreementID.Text = Convert.ToString(HeaderInfo.Rows[0][AgHelper.CPTYAgreementID]);
                this.cbAgreementStatus.SelectedValue = Convert.ToInt32(HeaderInfo.Rows[0][AgHelper.PartyAgreementStatusID]);
                this.cbAgrementType.SelectedValue = Convert.ToInt32(HeaderInfo.Rows[0][AgHelper.PartyAgreementTypeID]);
                this.cbExternalLE.SelectedValue = Convert.ToInt32(HeaderInfo.Rows[0][AgHelper.ExtPartyID]);
                this.cbInternalLE.SelectedValue = Convert.ToInt32(HeaderInfo.Rows[0][AgHelper.IntPartyID]);
                this.dtpSigned.Checked = true;
                this.dtpSigned.Value = Convert.ToDateTime(HeaderInfo.Rows[0][AgHelper.ContractSignDate]);
                this.dtpEffectiveStart.Checked = true;
                this.dtpEffectiveStart.Value = Convert.ToDateTime(HeaderInfo.Rows[0][AgHelper.CoverageStartDate]);
                this.dtpEffectiveEnd.Value = Convert.ToDateTime(HeaderInfo.Rows[0][AgHelper.CoverageEndDate]);
                this.cbCalculationAgent.SelectedValue = Convert.ToInt32(HeaderInfo.Rows[0][AgHelper.PartyAgreementCalculationAgentId]);
                this.cbVersionYear.SelectedValue = Convert.ToInt32(HeaderInfo.Rows[0][AgHelper.PartyAgreementVersionID]);

                Int32? confirmRouteID = ETRMDataAccess.GetInt32DTRElement(HeaderInfo.Rows[0], AgHelper.ConfirmRouteID);
                if(confirmRouteID.HasValue)
                {
                    this.cbConfirmRoute.SelectedValue = confirmRouteID.Value;
                }
            }
        }

        private void FillConfirmRoute()
        {
            DataTable confirmRoute = ETRMDataAccess.GetConfirmingMethod();
            this.cbConfirmRoute.DataSource = confirmRoute.DefaultView;
            this.cbConfirmRoute.ValueMember = PartyHI.ConfirmMethodID;
            this.cbConfirmRoute.DisplayMember = PartyHI.ConfirmMethod;
            this.cbConfirmRoute.BindingContext = this.BindingContext;
        }

        private void FillCalculationAgent()
        {
            DataTable agStatus = ETRMDataAccess.GetAgreementCalculationAgent();
            this.cbCalculationAgent.DataSource = agStatus.DefaultView;
            this.cbCalculationAgent.DisplayMember = AgreementAgent.PartyAgreementCalculationAgent;
            this.cbCalculationAgent.ValueMember = AgreementAgent.PartyAgreementCalculationAgentId;
            this.cbCalculationAgent.BindingContext = this.BindingContext;
            this.cbCalculationAgent.SelectedValue = AgreementAgent.AsPerAgreement;
        }

        private void FillVersionYear()
        {
            DataTable agStatus = ETRMDataAccess.GetAgreementCalculationYear();
            this.cbVersionYear.DataSource = agStatus.DefaultView;
            this.cbVersionYear.DisplayMember = AgreementVersion.PartyAgreementVersion;
            this.cbVersionYear.ValueMember = AgreementVersion.PartyAgreementVersionID;
            this.cbVersionYear.BindingContext = this.BindingContext;
        }

        private void FillAgreementStatus()
        {
            DataTable agStatus = ETRMDataAccess.GetAgreementStatus();
            this.cbAgreementStatus.DataSource = agStatus.DefaultView;
            this.cbAgreementStatus.DisplayMember = AgreementStatus.PartyAgreementStatus;
            this.cbAgreementStatus.ValueMember = AgreementStatus.PartyAgreementStatusID;
            this.cbAgreementStatus.BindingContext = this.BindingContext;
        }
        private void FillAgreementType()
        {
            DataTable agType = ETRMDataAccess.GetAgreementType();
            this.cbAgrementType.DataSource = agType.DefaultView;
            this.cbAgrementType.DisplayMember = AgreementType.PartyAgreementType;
            this.cbAgrementType.ValueMember = AgreementType.PartyAgreementTypeID;
            this.cbAgrementType.BindingContext = this.BindingContext;
        }
        private void FillInternalLE()
        {
            DataTable legalEntity = ETRMDataAccess.GetParties(1, 1);
            this.cbInternalLE.DataSource = legalEntity.DefaultView;
            this.cbInternalLE.DisplayMember = PartyHI.ShortName;
            this.cbInternalLE.ValueMember = PartyHI.PartyID;
            this.cbInternalLE.BindingContext = this.BindingContext;
        }
        private void FillExternalLE()
        {
            DataTable legalEntity = null;

            if (this.PartyId == PartyHI.BSCLE)
            {
                legalEntity = ETRMDataAccess.GetTradeableParties(1, 2);
            }
            else
            {
                legalEntity = ETRMDataAccess.GetPartiesWithID(1, 2, this.PartyId);
            }

            this.cbExternalLE.DataSource = legalEntity.DefaultView;
            this.cbExternalLE.DisplayMember = PartyHI.ShortName;
            this.cbExternalLE.ValueMember = PartyHI.PartyID;
            this.cbExternalLE.BindingContext = this.BindingContext;
        }
        private void FillCurrencies()
        {
            DataTable currencies = ETRMDataAccess.GetAgreementCurrency();
            currencies.Columns.Add(new DataColumn(AgHelper.Associated, typeof(bool)));

            DataTable associatedCurrencies = ETRMDataAccess.GetAssociatedCurrencies(this.AgreementId);
            if (!ETRMDataAccess.IsEmpty(associatedCurrencies))
            {
                this.AssociateCurrencies(currencies, associatedCurrencies);
            }

            if (!ETRMDataAccess.IsEmpty(currencies))
            {
                this.dgvCurrency.DataSource = null;

                DataView sortedView = currencies.DefaultView;
                sortedView.Sort = "ASSOCIATED DESC";
                DataTable sorted = sortedView.ToTable();

                this.dgvCurrency.DataSource = sorted.DefaultView;
                this.dgvCurrency.ReadOnly = false;
                this.dgvCurrency.Columns[AgHelper.Associated].ReadOnly = false;
                this.dgvCurrency.Columns[AgHelper.Currency].ReadOnly = true;
                this.dgvCurrency.Columns[AgHelper.CurrencyISOCode].Visible = false;
            }
        }

        private void FillCommodities()
        {
            DataTable commodities = ETRMDataAccess.GetAgreementDeliveryType();
            commodities.Columns.Add(new DataColumn(AgHelper.Associated, typeof(bool)));

            DataTable associatedCommodities = ETRMDataAccess.GetAssociatedCommodities(this.AgreementId);
            if (!ETRMDataAccess.IsEmpty(associatedCommodities))
            {
                this.AssociateCommodities(commodities, associatedCommodities);
            }

            if (!ETRMDataAccess.IsEmpty(commodities))
            {
                this.dgvCommodity.DataSource = null;

                DataView sortedView = commodities.DefaultView;
                sortedView.Sort = "ASSOCIATED DESC";
                DataTable sorted = sortedView.ToTable();

                this.dgvCommodity.DataSource = sorted.DefaultView;
                this.dgvCommodity.ReadOnly = false;
                this.dgvCommodity.Columns[AgHelper.Associated].ReadOnly = false;
                this.dgvCommodity.Columns[AgHelper.Commodity].ReadOnly = true;
            }
        }

        private void FillSettlements()
        {
            DataTable settlements = ETRMDataAccess.GetAgreementSettlementType();
            settlements.Columns.Add(new DataColumn(AgHelper.Associated, typeof(bool)));

            DataTable associatedSettlements = ETRMDataAccess.GetAssociatedSettlementTypes(this.AgreementId);
            if (!ETRMDataAccess.IsEmpty(associatedSettlements))
            {
                this.AssociateSettlements(settlements, associatedSettlements);
            }

            if (!ETRMDataAccess.IsEmpty(settlements))
            {
                this.dgvSettlementType.DataSource = null;

                DataView sortedView = settlements.DefaultView;
                sortedView.Sort = "ASSOCIATED DESC";
                DataTable sorted = sortedView.ToTable();

                this.dgvSettlementType.DataSource = sorted.DefaultView;
                this.dgvSettlementType.ReadOnly = false;
                this.dgvSettlementType.Columns[AgHelper.Associated].ReadOnly = false;
                this.dgvSettlementType.Columns[AgHelper.SettlementType].ReadOnly = true;
            }
        }

        private void HideColumns()
        {
            try
            {
                this.dgvCommodity.Columns[AgHelper.DeliveryTypeId].Visible = false;
                this.dgvCurrency.Columns[AgHelper.CurrencyID].Visible = false;
                this.dgvSettlementType.Columns[AgHelper.SettlementTypeId].Visible = false;
            }
            catch { }
        }

        private void AssociateSettlements(DataTable allSettlements, DataTable associatedSettlements)
        {
            foreach (DataRow row in allSettlements.Rows)
            {
                var results = from myRow in associatedSettlements.AsEnumerable()
                              where myRow.Field<int>(AgHelper.SettlementTypeId) == Convert.ToInt32(row[AgHelper.SettlementTypeId])
                              select myRow;

                if (results.Count() == 0)
                {
                    row[AgHelper.Associated] = false;
                }
                else
                {
                    row[AgHelper.Associated] = true;
                }
            }
        }
        private void AssociateCurrencies(DataTable allSettlements, DataTable associatedSettlements)
        {
            foreach (DataRow row in allSettlements.Rows)
            {
                var results = from myRow in associatedSettlements.AsEnumerable()
                              where myRow.Field<int>(AgHelper.CurrencyID) == Convert.ToInt32(row[AgHelper.CurrencyID])
                              select myRow;

                if (results.Count() == 0)
                {
                    row[AgHelper.Associated] = false;
                }
                else
                {
                    row[AgHelper.Associated] = true;
                }
            }
        }

        private void AssociateCommodities(DataTable allSettlements, DataTable associatedSettlements)
        {
            foreach (DataRow row in allSettlements.Rows)
            {
                var results = from myRow in associatedSettlements.AsEnumerable()
                              where myRow.Field<int>(AgHelper.DeliveryTypeId) == Convert.ToInt32(row[AgHelper.DeliveryTypeId])
                              select myRow;

                if (results.Count() == 0)
                {
                    row[AgHelper.Associated] = false;
                }
                else
                {
                    row[AgHelper.Associated] = true;
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            if (!this.IsFormContentValid(out message))
            {
                MessageBox.Show(message);
                return;
            }

            AgreementHeader ah = this.FillAgreementHeader();

            Int32 result = Int32.MinValue;

            if (this.IsNew)
            {
                result = ETRMDataAccess.SaveAgreementHeader(ah);
            }
            else
            {
                result = ETRMDataAccess.UpdateAgreementHeader(ah);
            }

            if (result != Int32.MinValue)
            {
                ETRMDataAccess.DeleteAgreementAssociations(this.AgreementId);
                this.SaveCurrencies();
                this.SaveCommodities();
                this.SaveSettlementTypes();

                //this.Close();
                MessageBox.Show("The agreement has been saved.");
            }
            else
            {
                MessageBox.Show("Something went wronte with the agreement saving process. Please contact support.");
            }
        }

        private AgreementHeader FillAgreementHeader()
        {
            AgreementHeader ah = new AgreementHeader();
            ah.AgreementID = this.AgreementId;
            ah.AgreementName = this.txtAgreementName.Text;
            ah.CptyAgreementID = this.txtCptyAgreementID.Text;
            ah.AgreementStatusID = Convert.ToInt32(this.cbAgreementStatus.SelectedValue);
            ah.AgreementTypeID = Convert.ToInt32(this.cbAgrementType.SelectedValue);
            ah.InternalLE = Convert.ToInt32(this.cbInternalLE.SelectedValue);
            ah.ExternalLE = Convert.ToInt32(this.cbExternalLE.SelectedValue);
            ah.AgreementAgentID = Convert.ToInt32(this.cbCalculationAgent.SelectedValue);
            ah.AgreementVersionYearID = Convert.ToInt32(this.cbVersionYear.SelectedValue);
            //ah.AgreementCalculationAgentID = Convert.ToInt32(this.cbCalculationAgent.SelectedValue);
            ah.EffectiveFrom = dtpEffectiveStart.Value.Date;
            ah.EffectiveTo = dtpEffectiveEnd.Value.Date;
            ah.SignedOn = dtpSigned.Value.Date;
            ah.ConfirmRouteID = Convert.ToInt32(this.cbConfirmRoute.SelectedValue);
            return ah;
        }

        private void SaveCommodities()
        {
            if (this.dgvCommodity.Rows != null && this.dgvCommodity.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in this.dgvCommodity.Rows)
                {
                    if (row.Cells[AgHelper.Associated].Value != DBNull.Value && Convert.ToBoolean(row.Cells[AgHelper.Associated].Value))
                    {
                        Int32 pId = Convert.ToInt32(row.Cells[AgHelper.DeliveryTypeId].Value);
                        ETRMDataAccess.SaveAgreementCommodity(this.AgreementId, pId);
                    }
                }
            }
        }

        private bool AreCommoditiesSelected()
        {
            if (this.dgvCommodity.Rows != null && this.dgvCommodity.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in this.dgvCommodity.Rows)
                {
                    if (row.Cells[AgHelper.Associated].Value != DBNull.Value && Convert.ToBoolean(row.Cells[AgHelper.Associated].Value))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void SaveCurrencies()
        {
            if (this.dgvCurrency.Rows != null && this.dgvCurrency.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in this.dgvCurrency.Rows)
                {
                    if (row.Cells[AgHelper.Associated].Value != DBNull.Value && Convert.ToBoolean(row.Cells[AgHelper.Associated].Value))
                    {
                        Int32 pId = Convert.ToInt32(row.Cells[AgHelper.CurrencyID].Value);
                        ETRMDataAccess.SaveAgreementCurrency(this.AgreementId, pId);
                    }
                }
            }
        }

        private bool AreSelectedCurrencies()
        {
            if (this.dgvCurrency.Rows != null && this.dgvCurrency.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in this.dgvCurrency.Rows)
                {
                    if (row.Cells[AgHelper.Associated].Value != DBNull.Value && Convert.ToBoolean(row.Cells[AgHelper.Associated].Value))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void SaveSettlementTypes()
        {
            if (this.dgvSettlementType.Rows != null && this.dgvSettlementType.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in this.dgvSettlementType.Rows)
                {
                    if (row.Cells[AgHelper.Associated].Value != DBNull.Value && Convert.ToBoolean(row.Cells[AgHelper.Associated].Value))
                    {
                        Int32 pId = Convert.ToInt32(row.Cells[AgHelper.SettlementTypeId].Value);
                        ETRMDataAccess.SaveAgreementSettlementType(this.AgreementId, pId);
                    }
                }
            }
        }

        private bool AreSettlementTypesSelected()
        {
            if (this.dgvSettlementType.Rows != null && this.dgvSettlementType.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in this.dgvSettlementType.Rows)
                {
                    if (row.Cells[AgHelper.Associated].Value != DBNull.Value && Convert.ToBoolean(row.Cells[AgHelper.Associated].Value))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private bool AreBothDatesSet()
        {
            if(this.dtpEffectiveStart.Checked && this.dtpEffectiveEnd.Checked)
            {
                return true;
            }
            return false;
        }

        private bool IsFormContentValid(out string message)
        {
            epAgreement.Clear();
            bool everythingOK = true;
            message = string.Empty;

            if (string.IsNullOrEmpty(txtAgreementName.Text))
            {
                epAgreement.SetError(txtAgreementName, "Agreement name cannot be empty!");
                message += Environment.NewLine + "Agreement name cannot be empty!";
                everythingOK = false;
            }

            Int32 versionYear = Int32.MinValue;

            try
            {
                versionYear = Convert.ToInt32(this.cbVersionYear.SelectedValue);
            }
            catch { }

            if (versionYear == Int32.MinValue || versionYear == 0)
            {
                epAgreement.SetError(cbVersionYear, "Please select a version!");
                message += Environment.NewLine + "Agreement version cannot be empty!";
                everythingOK = false;
            }


            Int32 calculationAgent = Int32.MinValue;

            try
            {
                calculationAgent = Convert.ToInt32(this.cbCalculationAgent.SelectedValue);
            }
            catch { }

            if (calculationAgent == Int32.MinValue || calculationAgent == 0)
            {
                epAgreement.SetError(cbCalculationAgent, "Please select a calculation agent!");
                message += Environment.NewLine + "Calculation agent cannot be empty!";
                everythingOK = false;
            }

            if (!AreCommoditiesSelected())
            {
                message += Environment.NewLine + "Please select at least one commodity!";
                everythingOK = false;
            }

            if (!AreSelectedCurrencies())
            {
                message += Environment.NewLine + "Please select at least one currency!";
                everythingOK = false;
            }

            if (!AreSettlementTypesSelected())
            {
                message += Environment.NewLine + "Please select at least one settlement type!";
                everythingOK = false;
            }

            if (!AreBothDatesSet())
            {
                message += Environment.NewLine + "Please both Signed Date and Effective Start need to be selected!";
                everythingOK = false;
            }

            return everythingOK;
        }
    }
}
