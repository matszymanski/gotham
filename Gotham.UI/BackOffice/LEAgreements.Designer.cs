﻿namespace Strazze.AddIn.Forms
{
    partial class LEAgreements
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAgreementName = new System.Windows.Forms.TextBox();
            this.txtAgreementId = new System.Windows.Forms.TextBox();
            this.cbAgreementCondition = new System.Windows.Forms.ComboBox();
            this.cbAgreementStatus = new System.Windows.Forms.ComboBox();
            this.btAgrConditionDetails = new System.Windows.Forms.Button();
            this.btAgrNewCondition = new System.Windows.Forms.Button();
            this.spAgreemBusinessUnits = new System.Windows.Forms.SplitContainer();
            this.scAggBusinessUnitsSelect = new System.Windows.Forms.SplitContainer();
            this.btManageInternalBU = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.lbInternalBU = new System.Windows.Forms.ListBox();
            this.cbInternalLE = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btManageExternalBU = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.lbExternalBU = new System.Windows.Forms.ListBox();
            this.cbExternalLE = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.dtpCoverageStart = new System.Windows.Forms.DateTimePicker();
            this.dtpCoverageEnds = new System.Windows.Forms.DateTimePicker();
            this.dtpContractSign = new System.Windows.Forms.DateTimePicker();
            this.txtPaymentDateOffset = new System.Windows.Forms.TextBox();
            this.txtRounding = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.spAgreemBusinessUnits)).BeginInit();
            this.spAgreemBusinessUnits.Panel1.SuspendLayout();
            this.spAgreemBusinessUnits.Panel2.SuspendLayout();
            this.spAgreemBusinessUnits.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scAggBusinessUnitsSelect)).BeginInit();
            this.scAggBusinessUnitsSelect.Panel1.SuspendLayout();
            this.scAggBusinessUnitsSelect.Panel2.SuspendLayout();
            this.scAggBusinessUnitsSelect.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "ID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Status";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 167);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Condition";
            // 
            // txtAgreementName
            // 
            this.txtAgreementName.Location = new System.Drawing.Point(86, 34);
            this.txtAgreementName.Name = "txtAgreementName";
            this.txtAgreementName.Size = new System.Drawing.Size(714, 20);
            this.txtAgreementName.TabIndex = 6;
            // 
            // txtAgreementId
            // 
            this.txtAgreementId.Location = new System.Drawing.Point(86, 75);
            this.txtAgreementId.Name = "txtAgreementId";
            this.txtAgreementId.ReadOnly = true;
            this.txtAgreementId.Size = new System.Drawing.Size(128, 20);
            this.txtAgreementId.TabIndex = 7;
            // 
            // cbAgreementCondition
            // 
            this.cbAgreementCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAgreementCondition.FormattingEnabled = true;
            this.cbAgreementCondition.Location = new System.Drawing.Point(86, 159);
            this.cbAgreementCondition.Name = "cbAgreementCondition";
            this.cbAgreementCondition.Size = new System.Drawing.Size(562, 21);
            this.cbAgreementCondition.TabIndex = 8;
            // 
            // cbAgreementStatus
            // 
            this.cbAgreementStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAgreementStatus.FormattingEnabled = true;
            this.cbAgreementStatus.Location = new System.Drawing.Point(86, 119);
            this.cbAgreementStatus.Name = "cbAgreementStatus";
            this.cbAgreementStatus.Size = new System.Drawing.Size(128, 21);
            this.cbAgreementStatus.TabIndex = 9;
            // 
            // btAgrConditionDetails
            // 
            this.btAgrConditionDetails.Location = new System.Drawing.Point(86, 195);
            this.btAgrConditionDetails.Name = "btAgrConditionDetails";
            this.btAgrConditionDetails.Size = new System.Drawing.Size(128, 23);
            this.btAgrConditionDetails.TabIndex = 10;
            this.btAgrConditionDetails.Text = "Condition Details";
            this.btAgrConditionDetails.UseVisualStyleBackColor = true;
            this.btAgrConditionDetails.Click += new System.EventHandler(this.btAgrConditionDetails_Click);
            // 
            // btAgrNewCondition
            // 
            this.btAgrNewCondition.Location = new System.Drawing.Point(231, 195);
            this.btAgrNewCondition.Name = "btAgrNewCondition";
            this.btAgrNewCondition.Size = new System.Drawing.Size(128, 23);
            this.btAgrNewCondition.TabIndex = 11;
            this.btAgrNewCondition.Text = "New Condition";
            this.btAgrNewCondition.UseVisualStyleBackColor = true;
            this.btAgrNewCondition.Click += new System.EventHandler(this.btAgrNewCondition_Click);
            // 
            // spAgreemBusinessUnits
            // 
            this.spAgreemBusinessUnits.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.spAgreemBusinessUnits.Location = new System.Drawing.Point(0, 354);
            this.spAgreemBusinessUnits.Name = "spAgreemBusinessUnits";
            this.spAgreemBusinessUnits.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spAgreemBusinessUnits.Panel1
            // 
            this.spAgreemBusinessUnits.Panel1.Controls.Add(this.scAggBusinessUnitsSelect);
            // 
            // spAgreemBusinessUnits.Panel2
            // 
            this.spAgreemBusinessUnits.Panel2.Controls.Add(this.btnClose);
            this.spAgreemBusinessUnits.Panel2.Controls.Add(this.btnSave);
            this.spAgreemBusinessUnits.Size = new System.Drawing.Size(844, 317);
            this.spAgreemBusinessUnits.SplitterDistance = 242;
            this.spAgreemBusinessUnits.TabIndex = 12;
            // 
            // scAggBusinessUnitsSelect
            // 
            this.scAggBusinessUnitsSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scAggBusinessUnitsSelect.IsSplitterFixed = true;
            this.scAggBusinessUnitsSelect.Location = new System.Drawing.Point(0, 0);
            this.scAggBusinessUnitsSelect.Name = "scAggBusinessUnitsSelect";
            // 
            // scAggBusinessUnitsSelect.Panel1
            // 
            this.scAggBusinessUnitsSelect.Panel1.Controls.Add(this.btManageInternalBU);
            this.scAggBusinessUnitsSelect.Panel1.Controls.Add(this.label7);
            this.scAggBusinessUnitsSelect.Panel1.Controls.Add(this.lbInternalBU);
            this.scAggBusinessUnitsSelect.Panel1.Controls.Add(this.cbInternalLE);
            this.scAggBusinessUnitsSelect.Panel1.Controls.Add(this.label5);
            // 
            // scAggBusinessUnitsSelect.Panel2
            // 
            this.scAggBusinessUnitsSelect.Panel2.Controls.Add(this.btManageExternalBU);
            this.scAggBusinessUnitsSelect.Panel2.Controls.Add(this.label8);
            this.scAggBusinessUnitsSelect.Panel2.Controls.Add(this.lbExternalBU);
            this.scAggBusinessUnitsSelect.Panel2.Controls.Add(this.cbExternalLE);
            this.scAggBusinessUnitsSelect.Panel2.Controls.Add(this.label6);
            this.scAggBusinessUnitsSelect.Size = new System.Drawing.Size(844, 242);
            this.scAggBusinessUnitsSelect.SplitterDistance = 409;
            this.scAggBusinessUnitsSelect.TabIndex = 0;
            // 
            // btManageInternalBU
            // 
            this.btManageInternalBU.Location = new System.Drawing.Point(155, 46);
            this.btManageInternalBU.Name = "btManageInternalBU";
            this.btManageInternalBU.Size = new System.Drawing.Size(75, 23);
            this.btManageInternalBU.TabIndex = 17;
            this.btManageInternalBU.Text = "Select";
            this.btManageInternalBU.UseVisualStyleBackColor = true;
            this.btManageInternalBU.Click += new System.EventHandler(this.btManageInternalBU_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Internal Business Units";
            // 
            // lbInternalBU
            // 
            this.lbInternalBU.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbInternalBU.FormattingEnabled = true;
            this.lbInternalBU.Location = new System.Drawing.Point(0, 121);
            this.lbInternalBU.Name = "lbInternalBU";
            this.lbInternalBU.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbInternalBU.Size = new System.Drawing.Size(409, 121);
            this.lbInternalBU.TabIndex = 15;
            // 
            // cbInternalLE
            // 
            this.cbInternalLE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInternalLE.FormattingEnabled = true;
            this.cbInternalLE.Location = new System.Drawing.Point(67, 14);
            this.cbInternalLE.Name = "cbInternalLE";
            this.cbInternalLE.Size = new System.Drawing.Size(323, 21);
            this.cbInternalLE.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Internal LE";
            // 
            // btManageExternalBU
            // 
            this.btManageExternalBU.Location = new System.Drawing.Point(163, 46);
            this.btManageExternalBU.Name = "btManageExternalBU";
            this.btManageExternalBU.Size = new System.Drawing.Size(75, 23);
            this.btManageExternalBU.TabIndex = 18;
            this.btManageExternalBU.Text = "Select";
            this.btManageExternalBU.UseVisualStyleBackColor = true;
            this.btManageExternalBU.Click += new System.EventHandler(this.btManageExternalBU_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 58);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(117, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "External Business Units";
            // 
            // lbExternalBU
            // 
            this.lbExternalBU.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbExternalBU.FormattingEnabled = true;
            this.lbExternalBU.Location = new System.Drawing.Point(0, 121);
            this.lbExternalBU.Name = "lbExternalBU";
            this.lbExternalBU.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbExternalBU.Size = new System.Drawing.Size(431, 121);
            this.lbExternalBU.TabIndex = 16;
            // 
            // cbExternalLE
            // 
            this.cbExternalLE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbExternalLE.FormattingEnabled = true;
            this.cbExternalLE.Location = new System.Drawing.Point(70, 14);
            this.cbExternalLE.Name = "cbExternalLE";
            this.cbExternalLE.Size = new System.Drawing.Size(323, 21);
            this.cbExternalLE.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "External LE";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(725, 19);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(17, 19);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dtpCoverageStart
            // 
            this.dtpCoverageStart.CustomFormat = "dd.MM.yyyy";
            this.dtpCoverageStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCoverageStart.Location = new System.Drawing.Point(104, 237);
            this.dtpCoverageStart.Name = "dtpCoverageStart";
            this.dtpCoverageStart.Size = new System.Drawing.Size(108, 20);
            this.dtpCoverageStart.TabIndex = 13;
            // 
            // dtpCoverageEnds
            // 
            this.dtpCoverageEnds.CustomFormat = "dd.MM.yyyy";
            this.dtpCoverageEnds.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCoverageEnds.Location = new System.Drawing.Point(314, 237);
            this.dtpCoverageEnds.Name = "dtpCoverageEnds";
            this.dtpCoverageEnds.Size = new System.Drawing.Size(98, 20);
            this.dtpCoverageEnds.TabIndex = 14;
            // 
            // dtpContractSign
            // 
            this.dtpContractSign.CustomFormat = "dd.MM.yyyy";
            this.dtpContractSign.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpContractSign.Location = new System.Drawing.Point(550, 237);
            this.dtpContractSign.Name = "dtpContractSign";
            this.dtpContractSign.Size = new System.Drawing.Size(98, 20);
            this.dtpContractSign.TabIndex = 15;
            // 
            // txtPaymentDateOffset
            // 
            this.txtPaymentDateOffset.Location = new System.Drawing.Point(128, 283);
            this.txtPaymentDateOffset.Name = "txtPaymentDateOffset";
            this.txtPaymentDateOffset.Size = new System.Drawing.Size(84, 20);
            this.txtPaymentDateOffset.TabIndex = 16;
            // 
            // txtRounding
            // 
            this.txtRounding.Location = new System.Drawing.Point(344, 283);
            this.txtRounding.Name = "txtRounding";
            this.txtRounding.Size = new System.Drawing.Size(68, 20);
            this.txtRounding.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 240);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Coverage Starts";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(228, 240);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Coverage Ends";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(438, 240);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Contract Signed Date";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 286);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(105, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "Payment Date Offset";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(228, 286);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(97, 13);
            this.label13.TabIndex = 22;
            this.label13.Text = "Payment Rounding";
            // 
            // LEAgreements
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 671);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtRounding);
            this.Controls.Add(this.txtPaymentDateOffset);
            this.Controls.Add(this.dtpContractSign);
            this.Controls.Add(this.dtpCoverageEnds);
            this.Controls.Add(this.dtpCoverageStart);
            this.Controls.Add(this.spAgreemBusinessUnits);
            this.Controls.Add(this.btAgrNewCondition);
            this.Controls.Add(this.btAgrConditionDetails);
            this.Controls.Add(this.cbAgreementStatus);
            this.Controls.Add(this.cbAgreementCondition);
            this.Controls.Add(this.txtAgreementId);
            this.Controls.Add(this.txtAgreementName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LEAgreements";
            this.Text = "Agreements";
            this.spAgreemBusinessUnits.Panel1.ResumeLayout(false);
            this.spAgreemBusinessUnits.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spAgreemBusinessUnits)).EndInit();
            this.spAgreemBusinessUnits.ResumeLayout(false);
            this.scAggBusinessUnitsSelect.Panel1.ResumeLayout(false);
            this.scAggBusinessUnitsSelect.Panel1.PerformLayout();
            this.scAggBusinessUnitsSelect.Panel2.ResumeLayout(false);
            this.scAggBusinessUnitsSelect.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scAggBusinessUnitsSelect)).EndInit();
            this.scAggBusinessUnitsSelect.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAgreementName;
        private System.Windows.Forms.TextBox txtAgreementId;
        private System.Windows.Forms.ComboBox cbAgreementCondition;
        private System.Windows.Forms.ComboBox cbAgreementStatus;
        private System.Windows.Forms.Button btAgrConditionDetails;
        private System.Windows.Forms.Button btAgrNewCondition;
        private System.Windows.Forms.SplitContainer spAgreemBusinessUnits;
        private System.Windows.Forms.SplitContainer scAggBusinessUnitsSelect;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox lbInternalBU;
        private System.Windows.Forms.ComboBox cbInternalLE;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListBox lbExternalBU;
        private System.Windows.Forms.ComboBox cbExternalLE;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btManageInternalBU;
        private System.Windows.Forms.Button btManageExternalBU;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DateTimePicker dtpCoverageStart;
        private System.Windows.Forms.DateTimePicker dtpCoverageEnds;
        private System.Windows.Forms.DateTimePicker dtpContractSign;
        private System.Windows.Forms.TextBox txtPaymentDateOffset;
        private System.Windows.Forms.TextBox txtRounding;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
    }
}