﻿using Microsoft.Reporting.WinForms;
using Strazze.AddIn.Management;
//using Strazze.AddIn.Reports;
using Strazze.Data;
using Strazze.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Strazze.AddIn.Forms
{
    public partial class BackOfficeNavigator : Form
    {
        private System.Data.SqlClient.SqlConnection _cn;

        private string AppMode { get; set; }
        private DataTable MainView { get; set; }
        public BackOfficeNavigator()
        {
            InitializeComponent();
            this.InitialiseView();
        }

        private void InitialiseView()
        {
            this.PopulateMenuStrip();
        }

        private void SetSplitContainerSize()
        {
            try
            {
                double treeViewWidth = Convert.ToDouble(this.treeView.Width);
                double desiredWidth = treeViewWidth * 1.1;
                this.splitContainer.Width = Convert.ToInt32(Math.Ceiling(desiredWidth));
            }
            catch (Exception ex)
            { }
        }

        private void PopulateMenuStrip()
        {
            //ToolStripButton
            ToolStripButton toExcelButton = new ToolStripButton();
            toExcelButton.Name = "ExportToExcel";
            toExcelButton.Text = "Excel Export";
            toExcelButton.Visible = true;
            toExcelButton.Enabled = true;
            toExcelButton.Click += ToExcelClicked;
            //toExcelButton.Image
            //toExcelButton.BackColor = Color.Orange;
            this.menuStrip.Items.Add(toExcelButton);

            this.menuStrip.Visible = true;
            this.menuStrip.Enabled = true;
        }

        private void ToExcelClicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.AppMode))
            {
                Microsoft.Office.Interop.Excel.Worksheet sheet = UIManager.ManageWorksheet(this.AppMode);
                UIManager.PasteDataIntoExcel(this.MainView, sheet);
            }
        }

        private void MenuClicked(object sender, EventArgs e)
        {
            //MessageBox.Show("Clicked on " + ((ToolStripButton)sender).Text);
            //if (!ETRMDataAccess.IsEmpty(this.MainView))
            //{
            //    this.MainView.TableName = "DataTable1";
            //    this.reportViewer.Visible = true;
            //    BusinessUnitsReport reportDs = new BusinessUnitsReport();
            //    reportDs.Tables.Add(this.MainView);
            //    ReportDataSource rptDs = new ReportDataSource(this.MainView.TableName, reportDs);
            //    this.reportViewer.LocalReport.DataSources.Clear();
            //    this.reportViewer.Reset();
            //    this.reportViewer.LocalReport.DataSources.Add(rptDs);
            //    this.reportViewer.RefreshReport();
            //}

            //////////this.reportViewer.Visible = true;

            //////////DataSet ds = new DataSet();

            //////////SqlDataAdapter da = new SqlDataAdapter(@"exec dbo.sp_FutureTrades_GetBusinessUnitsDS;", cn);

            //////////da.TableMappings.Add("t", "t1");

            //////////da.Fill(ds, "t1");

            //////////BindingSource bs = new BindingSource(ds, "t1");

            //////////reportViewer.LocalReport.ReportEmbeddedResource = "Strazze.AddIn.Forms.BusinessUnits.rdlc";
            ////////////reportViewer.LocalReport.ReportPath = "Strazze.AddIn.Forms.BusinessUnits.rdlc";
            //////////reportViewer.LocalReport.DataSources.Add(new ReportDataSource("BusinessUnits", bs));
            //////////reportViewer.RefreshReport();
        }

        //////public System.Data.SqlClient.SqlConnection cn
        //////{
        //////    get
        //////    {
        //////        if (_cn == null || _cn.State == ConnectionState.Closed)
        //////        {
        //////            _cn = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["."].ConnectionString);
        //////            _cn.Open();
        //////        }

        //////        return _cn;
        //////    }
        //////}

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            string selectedNode = e.Node.TreeView.SelectedNode.Name;

            switch (selectedNode)
            {
                case BackOfficeNavigatorItems.ngLE:
                    this.AppMode = Constants.LegalEntities;
                    this.RefreshMainView();
                    break;
                case BackOfficeNavigatorItems.ngBU:
                    this.AppMode = Constants.BusinessUnits;
                    this.RefreshMainView();
                    break;
            }
        }

        private void RefreshMainView()
        {
            switch (this.AppMode)
            {
                case Constants.LegalEntities:
                    this.MainView = ETRMDataAccess.GetLE();
                    break;
                case Constants.BusinessUnits:
                    this.MainView = ETRMDataAccess.GetBU();
                    break;
            }

            if (!ETRMDataAccess.IsEmpty(this.MainView))
            {
                this.dataGridView.DataSource = null;
                this.dataGridView.DataSource = this.MainView.DefaultView;
                this.MainView.TableName = this.AppMode;
            }
        }

        private Int32 GetPartyID()
        {
            return Convert.ToInt32(this.dataGridView.SelectedRows[0].Cells[PartyHI.PartyID].Value);
        }

        private void dataGridView_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Int32 selectedRow = e.RowIndex;

            if (selectedRow > -1)
            {
                switch (this.AppMode)
                {
                    case Constants.LegalEntities:
                        Int32 leId = this.GetPartyID();
                        DataTable partyDetails = ETRMDataAccess.GetParty(leId);
                        Party party = new Party(leId, partyDetails, true);
                        party.ShowDialog();
                        break;
                    case Constants.BusinessUnits:
                        Int32 buId = this.GetPartyID();
                        //////DataTable partyDetailsBu = ETRMDataAccess.GetParty(buId);
                        //////DataTable partyAddress = ETRMDataAccess.GetPartyAddresses(buId);
                        //////DataTable partyContracts = ETRMDataAccess.GetPartyContacts(buId);
                        //////DataTable partyFunctions = ETRMDataAccess.GetPartyFunction();
                        //////DataTable partyBrokerRates = ETRMDataAccess.GetPartyBrokerRates(buId);
                        //////Int32 parentId = ETRMDataAccess.GetParentCompany(buId);

                        //BusinessUnit bu = new BusinessUnit(buId, partyDetailsBu, partyAddress, partyContracts, partyFunctions, partyBrokerRates, parentId, true);
                        BusinessUnit bu = new BusinessUnit(buId, true);

                        bu.ShowDialog();
                        break;

                }
            }
        }

        private void BackOfficeNavigator_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'cashTrades.BusinessUnits' table. You can move, or remove it, as needed.
            //////this.businessUnitsTableAdapter.Fill(this.cashTrades.BusinessUnits);
            //////this.reportViewer.RefreshReport();
        }

        private void treeView_AfterCollapse(object sender, TreeViewEventArgs e)
        {
            this.SetSplitContainerSize();
        }

        private void treeView_AfterExpand(object sender, TreeViewEventArgs e)
        {
            this.SetSplitContainerSize();
        }

        private void treeView_Resize(object sender, EventArgs e)
        {
            this.SetSplitContainerSize();
        }
    }
}
