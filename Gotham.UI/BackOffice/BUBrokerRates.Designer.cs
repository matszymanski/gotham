﻿namespace Strazze.AddIn.Forms
{
    partial class BUBrokerRates
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.cbBroker = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbExchange = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbDeliveryType = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cbRateCalculation = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbFeeCCY = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbPaymentFrequency = new System.Windows.Forms.ComboBox();
            this.txtFeeRate = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btBUBrokSave = new System.Windows.Forms.Button();
            this.btBUBrokCancel = new System.Windows.Forms.Button();
            this.cbFeeType = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.cbSettlementType = new System.Windows.Forms.ComboBox();
            this.txtPaymentDateOffset = new System.Windows.Forms.TextBox();
            this.chkIsDefault = new System.Windows.Forms.CheckBox();
            this.cbTradeType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbExecutionType = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMiniFeeRate = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cbMiniRateCalculation = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Broker";
            // 
            // cbBroker
            // 
            this.cbBroker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBroker.FormattingEnabled = true;
            this.cbBroker.Location = new System.Drawing.Point(139, 33);
            this.cbBroker.Name = "cbBroker";
            this.cbBroker.Size = new System.Drawing.Size(199, 21);
            this.cbBroker.TabIndex = 3;
            this.cbBroker.SelectedIndexChanged += new System.EventHandler(this.cbBroker_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Exchange";
            // 
            // cbExchange
            // 
            this.cbExchange.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbExchange.FormattingEnabled = true;
            this.cbExchange.Location = new System.Drawing.Point(139, 63);
            this.cbExchange.Name = "cbExchange";
            this.cbExchange.Size = new System.Drawing.Size(199, 21);
            this.cbExchange.TabIndex = 5;
            this.cbExchange.SelectedIndexChanged += new System.EventHandler(this.cbExchange_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 164);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Delivery Type";
            // 
            // cbDeliveryType
            // 
            this.cbDeliveryType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDeliveryType.FormattingEnabled = true;
            this.cbDeliveryType.Location = new System.Drawing.Point(139, 159);
            this.cbDeliveryType.Name = "cbDeliveryType";
            this.cbDeliveryType.Size = new System.Drawing.Size(199, 21);
            this.cbDeliveryType.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(372, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Fee Rate";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(372, 67);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Rate Calc";
            // 
            // cbRateCalculation
            // 
            this.cbRateCalculation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRateCalculation.FormattingEnabled = true;
            this.cbRateCalculation.Location = new System.Drawing.Point(489, 59);
            this.cbRateCalculation.Name = "cbRateCalculation";
            this.cbRateCalculation.Size = new System.Drawing.Size(199, 21);
            this.cbRateCalculation.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(372, 160);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Fee Currency";
            // 
            // cbFeeCCY
            // 
            this.cbFeeCCY.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFeeCCY.FormattingEnabled = true;
            this.cbFeeCCY.Location = new System.Drawing.Point(489, 157);
            this.cbFeeCCY.Name = "cbFeeCCY";
            this.cbFeeCCY.Size = new System.Drawing.Size(199, 21);
            this.cbFeeCCY.TabIndex = 16;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(372, 192);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(101, 13);
            this.label11.TabIndex = 17;
            this.label11.Text = "Payment Frequency";
            // 
            // cbPaymentFrequency
            // 
            this.cbPaymentFrequency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPaymentFrequency.FormattingEnabled = true;
            this.cbPaymentFrequency.Location = new System.Drawing.Point(489, 189);
            this.cbPaymentFrequency.Name = "cbPaymentFrequency";
            this.cbPaymentFrequency.Size = new System.Drawing.Size(199, 21);
            this.cbPaymentFrequency.TabIndex = 18;
            // 
            // txtFeeRate
            // 
            this.txtFeeRate.Location = new System.Drawing.Point(489, 31);
            this.txtFeeRate.Name = "txtFeeRate";
            this.txtFeeRate.Size = new System.Drawing.Size(199, 20);
            this.txtFeeRate.TabIndex = 19;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(372, 226);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(105, 13);
            this.label12.TabIndex = 20;
            this.label12.Text = "Payment Date Offset";
            // 
            // btBUBrokSave
            // 
            this.btBUBrokSave.Location = new System.Drawing.Point(34, 310);
            this.btBUBrokSave.Name = "btBUBrokSave";
            this.btBUBrokSave.Size = new System.Drawing.Size(75, 23);
            this.btBUBrokSave.TabIndex = 24;
            this.btBUBrokSave.Text = "Save";
            this.btBUBrokSave.UseVisualStyleBackColor = true;
            this.btBUBrokSave.Click += new System.EventHandler(this.btBUBrokSave_Click);
            // 
            // btBUBrokCancel
            // 
            this.btBUBrokCancel.Location = new System.Drawing.Point(535, 310);
            this.btBUBrokCancel.Name = "btBUBrokCancel";
            this.btBUBrokCancel.Size = new System.Drawing.Size(75, 23);
            this.btBUBrokCancel.TabIndex = 25;
            this.btBUBrokCancel.Text = "Cancel";
            this.btBUBrokCancel.UseVisualStyleBackColor = true;
            this.btBUBrokCancel.Click += new System.EventHandler(this.btBUBrokCancel_Click);
            // 
            // cbFeeType
            // 
            this.cbFeeType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFeeType.FormattingEnabled = true;
            this.cbFeeType.Location = new System.Drawing.Point(139, 94);
            this.cbFeeType.Name = "cbFeeType";
            this.cbFeeType.Size = new System.Drawing.Size(199, 21);
            this.cbFeeType.TabIndex = 26;
            this.cbFeeType.SelectedIndexChanged += new System.EventHandler(this.cbFeeType_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 99);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "Fee Type";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(29, 195);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "Settlement Type";
            // 
            // cbSettlementType
            // 
            this.cbSettlementType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSettlementType.FormattingEnabled = true;
            this.cbSettlementType.Location = new System.Drawing.Point(139, 191);
            this.cbSettlementType.Name = "cbSettlementType";
            this.cbSettlementType.Size = new System.Drawing.Size(199, 21);
            this.cbSettlementType.TabIndex = 29;
            // 
            // txtPaymentDateOffset
            // 
            this.txtPaymentDateOffset.Location = new System.Drawing.Point(489, 223);
            this.txtPaymentDateOffset.Name = "txtPaymentDateOffset";
            this.txtPaymentDateOffset.Size = new System.Drawing.Size(199, 20);
            this.txtPaymentDateOffset.TabIndex = 32;
            // 
            // chkIsDefault
            // 
            this.chkIsDefault.AutoSize = true;
            this.chkIsDefault.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkIsDefault.Location = new System.Drawing.Point(31, 235);
            this.chkIsDefault.Name = "chkIsDefault";
            this.chkIsDefault.Size = new System.Drawing.Size(69, 17);
            this.chkIsDefault.TabIndex = 33;
            this.chkIsDefault.Text = "Is default";
            this.chkIsDefault.UseVisualStyleBackColor = true;
            // 
            // cbTradeType
            // 
            this.cbTradeType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTradeType.FormattingEnabled = true;
            this.cbTradeType.Location = new System.Drawing.Point(139, 127);
            this.cbTradeType.Name = "cbTradeType";
            this.cbTradeType.Size = new System.Drawing.Size(199, 21);
            this.cbTradeType.TabIndex = 34;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 132);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 35;
            this.label1.Text = "Trade Type";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(372, 261);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 36;
            this.label5.Text = "Execution Type";
            // 
            // cbExecutionType
            // 
            this.cbExecutionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbExecutionType.FormattingEnabled = true;
            this.cbExecutionType.Location = new System.Drawing.Point(489, 253);
            this.cbExecutionType.Name = "cbExecutionType";
            this.cbExecutionType.Size = new System.Drawing.Size(199, 21);
            this.cbExecutionType.TabIndex = 37;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(372, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 38;
            this.label6.Text = "Mini Rate";
            // 
            // txtMiniFeeRate
            // 
            this.txtMiniFeeRate.Location = new System.Drawing.Point(489, 94);
            this.txtMiniFeeRate.Name = "txtMiniFeeRate";
            this.txtMiniFeeRate.Size = new System.Drawing.Size(199, 20);
            this.txtMiniFeeRate.TabIndex = 39;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(372, 135);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 13);
            this.label14.TabIndex = 40;
            this.label14.Text = "Mini Rate Calc";
            // 
            // cbMiniRateCalculation
            // 
            this.cbMiniRateCalculation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMiniRateCalculation.FormattingEnabled = true;
            this.cbMiniRateCalculation.Location = new System.Drawing.Point(489, 124);
            this.cbMiniRateCalculation.Name = "cbMiniRateCalculation";
            this.cbMiniRateCalculation.Size = new System.Drawing.Size(199, 21);
            this.cbMiniRateCalculation.TabIndex = 41;
            // 
            // BUBrokerRates
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(731, 352);
            this.Controls.Add(this.cbMiniRateCalculation);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtMiniFeeRate);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbExecutionType);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbTradeType);
            this.Controls.Add(this.chkIsDefault);
            this.Controls.Add(this.txtPaymentDateOffset);
            this.Controls.Add(this.cbSettlementType);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cbFeeType);
            this.Controls.Add(this.btBUBrokCancel);
            this.Controls.Add(this.btBUBrokSave);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtFeeRate);
            this.Controls.Add(this.cbPaymentFrequency);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cbFeeCCY);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cbRateCalculation);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cbDeliveryType);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbExchange);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbBroker);
            this.Controls.Add(this.label2);
            this.Name = "BUBrokerRates";
            this.Text = "Broker Rates";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbBroker;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbExchange;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbDeliveryType;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbRateCalculation;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbFeeCCY;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbPaymentFrequency;
        private System.Windows.Forms.TextBox txtFeeRate;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btBUBrokSave;
        private System.Windows.Forms.Button btBUBrokCancel;
        private System.Windows.Forms.ComboBox cbFeeType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbSettlementType;
        private System.Windows.Forms.TextBox txtPaymentDateOffset;
        private System.Windows.Forms.CheckBox chkIsDefault;
        private System.Windows.Forms.ComboBox cbTradeType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbExecutionType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMiniFeeRate;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cbMiniRateCalculation;
    }
}