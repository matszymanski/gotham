﻿using Strazze.Data;
using Strazze.Domain;
using Strazze.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Strazze.AddIn.Forms
{
    public partial class BUBrokerRates : Form
    {
        DataTable Brokers { get; set; }
        DataTable FeeTypes { get; set; }
        DataTable Exchanges { get; set; }
        DataTable TradeTypes { get; set; }
        DataTable DeliveryTypes { get; set; }
        DataTable ExecutionTypes { get; set; }
        DataTable SettlementTypes { get; set; }
        DataTable RateTypes { get; set; }
        DataTable MiniRateTypes { get; set; }
        DataTable FeeCurrencies { get; set; }
        DataTable DeliveryType { get; set; }
        DataTable PaymentFrequency { get; set; }
        DataRow EditableInfo { get; set; }

        bool IsNew { get; set; }

        Int32 BrokerId { get; set; }

        public BUBrokerRates(DataRow row)
        {
            InitializeComponent();
            this.EditableInfo = row;
            this.InitialiseView();
            this.IsNew = false;
            //this.BrokerId = brokerId;
            this.SetInitialValues();
            this.FillEditableInfo();

        }

        public BUBrokerRates(Int32 brokerId)
        {
            InitializeComponent();
            this.InitialiseView();
            this.BrokerId = brokerId;
            this.IsNew = true;
            this.SetInitialValues();
        }

        private void InitialiseView()
        {
            this.FillComboBoxes();
        }
        private void SetInitialValues()
        {
            try
            {
                this.cbBroker.SelectedValue = this.BrokerId;
                this.cbBroker.Enabled = false;
                this.cbPaymentFrequency.SelectedValue = Constants.CalculationPeriodMonthly;
            }
            catch { }

            this.SetFeeType();
        }

        private void FillEditableInfo()
        {
            if (this.EditableInfo != null)
            {
                Int32 partyBrokerRateId = Convert.ToInt32(this.EditableInfo[BrokerRates.PartyBrokerRateID]);
                DataTable rawData = ETRMDataAccess.GetPartyBrokerRate(partyBrokerRateId);

                if (rawData != null)
                {
                    this.BrokerId = Convert.ToInt32(rawData.Rows[0][BrokerRates.BrokerID]);
                    this.cbBroker.SelectedValue = Convert.ToInt32(rawData.Rows[0][BrokerRates.BrokerID]);

                    if (string.IsNullOrEmpty(Convert.ToString(rawData.Rows[0][BrokerRates.ExchangeID])))
                    {
                        this.cbExchange.SelectedValue = Int32.MinValue;
                    }
                    else
                    {
                        this.cbExchange.SelectedValue = Convert.ToInt32(rawData.Rows[0][BrokerRates.ExchangeID]);
                    }

                    this.cbDeliveryType.SelectedValue = Convert.ToInt32(rawData.Rows[0][BrokerRates.UnderlyingID]);

                    if (string.IsNullOrEmpty(Convert.ToString(rawData.Rows[0][BrokerRates.ExecutionTypeID])))
                    {
                        this.cbExecutionType.SelectedValue = Int32.MinValue;
                    }
                    else
                    {
                        this.cbExecutionType.SelectedValue = Convert.ToInt32(rawData.Rows[0][BrokerRates.ExecutionTypeID]);
                    }

                    this.cbTradeType.SelectedValue = Convert.ToInt32(rawData.Rows[0][BrokerRates.TradeTypeId]);
                    this.cbSettlementType.SelectedValue = Convert.ToInt32(rawData.Rows[0][BrokerRates.SettlementTypeID]);
                    this.txtPaymentDateOffset.Text = Convert.ToString(rawData.Rows[0][BrokerRates.PaymentDateOffset]);

                    //Standard
                    decimal feeRate = Decimal.Parse(Convert.ToString(rawData.Rows[0][BrokerRates.FeeRate]), NumberStyles.Any, CultureInfo.InvariantCulture);
                    this.txtFeeRate.Text = Convert.ToString(feeRate);

                    this.cbRateCalculation.SelectedValue = Convert.ToString(rawData.Rows[0][BrokerRates.BrokerRateTypeID]);
                    //

                    //Mini
                    if (!string.IsNullOrEmpty(Convert.ToString(rawData.Rows[0][BrokerRates.MiniFeeRate])))
                    {
                        decimal miniFeeRate = Decimal.Parse(Convert.ToString(rawData.Rows[0][BrokerRates.MiniFeeRate]), NumberStyles.Any, CultureInfo.InvariantCulture);
                        this.txtMiniFeeRate.Text = Convert.ToString(miniFeeRate);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(rawData.Rows[0][BrokerRates.MiniBrokerRateTypeID])))
                    {
                        this.cbMiniRateCalculation.SelectedValue = Convert.ToString(rawData.Rows[0][BrokerRates.MiniBrokerRateTypeID]);
                    }
                    //

                    this.cbFeeCCY.SelectedValue = Convert.ToString(rawData.Rows[0][BrokerRates.RateCurrencyID]);
                    this.cbPaymentFrequency.SelectedValue = Convert.ToString(rawData.Rows[0][BrokerRates.PaymentFrequencyID]);
                    this.chkIsDefault.Checked = Convert.ToBoolean(rawData.Rows[0][BrokerRates.DefaultRate]);

                    this.cbFeeType.SelectedValue = Convert.ToInt32(rawData.Rows[0][BrokerRates.FeeTypeID]);
                }
            }
        }

        private void FillComboBoxes()
        {
            this.FillBrokers();
            this.FillFeeTypes();
            this.FillExchanges();
            this.FillDeliveryTypes();
            this.FillExecutionTypes();
            this.FillTradeTypes();
            this.FillSettlementTypes();
            this.FillBrokerRateTypes();
            this.FillPaymentFrequency();
            this.FillCurrencies();
        }

        private void FillBrokers()
        {
            this.Brokers = ETRMDataAccess.GetBrokers();
            this.cbBroker.DataSource = this.Brokers.DefaultView;
            this.cbBroker.DisplayMember = PartyClass.ShortName;
            this.cbBroker.ValueMember = PartyClass.PartyID;
            this.cbBroker.BindingContext = this.BindingContext;
        }
        private void FillFeeTypes()
        {
            this.FeeTypes = ETRMDataAccess.GetFeeType();
            this.SetFeeDataSource();
        }

        private void SetFeeDataSource()
        {
            this.cbFeeType.DataSource = this.FeeTypes.DefaultView;
            this.cbFeeType.DisplayMember = CashFlow.CashFlowType;
            this.cbFeeType.ValueMember = CashFlow.CashFlowTypeId;
            this.cbFeeType.BindingContext = this.BindingContext;
        }

        private void FillExchanges()
        {
            this.Exchanges = ETRMDataAccess.GetExchanges();
            var emptyRow = this.Exchanges.NewRow();
            emptyRow[PartyClass.PartyID] = Int32.MinValue;
            emptyRow[PartyClass.ShortName] = string.Empty;
            this.Exchanges.Rows.Add(emptyRow);

            DataView sortedView = this.Exchanges.DefaultView;
            sortedView.Sort = "PartyID ASC";
            DataTable sorted = sortedView.ToTable();

            this.cbExchange.DataSource = sorted.DefaultView;
            this.cbExchange.DisplayMember = PartyClass.ShortName;
            this.cbExchange.ValueMember = PartyClass.PartyID;
            this.cbExchange.BindingContext = this.BindingContext;
        }

        private void FillTradeTypes()
        {
            this.TradeTypes = ETRMDataAccess.GetTradeTypes();

            this.cbTradeType.DataSource = this.TradeTypes.DefaultView;
            this.cbTradeType.DisplayMember = TTypes.TradeType;
            this.cbTradeType.ValueMember = TTypes.TradeTypeID;
            this.cbTradeType.BindingContext = this.BindingContext;
        }
        private void FillExecutionTypes()
        {
            this.ExecutionTypes = ETRMDataAccess.GetExecutionTypes();

            this.cbExecutionType.DataSource = this.ExecutionTypes.DefaultView;
            this.cbExecutionType.DisplayMember = ExTypes.ExecutionType;
            this.cbExecutionType.ValueMember = ExTypes.ExecutionTypeID;
            this.cbExecutionType.BindingContext = this.BindingContext;
        }

        private void FillDeliveryTypes()
        {
            this.DeliveryTypes = ETRMDataAccess.GetDeliveryTypes();

            this.cbDeliveryType.DataSource = this.DeliveryTypes.DefaultView;
            this.cbDeliveryType.DisplayMember = DTypes.DeliveryType;
            this.cbDeliveryType.ValueMember = DTypes.DeliveryTypeId;
            this.cbDeliveryType.BindingContext = this.BindingContext;
        }
        private void FillSettlementTypes()
        {
            this.SettlementTypes = ETRMDataAccess.GetSettlementTypes();

            this.cbSettlementType.DataSource = this.SettlementTypes.DefaultView;
            this.cbSettlementType.DisplayMember = STypes.SettlementType;
            this.cbSettlementType.ValueMember = STypes.SettlementTypeID;
            this.cbSettlementType.BindingContext = this.BindingContext;
        }
        private void FillBrokerRateTypes()
        {
            this.RateTypes = ETRMDataAccess.GetBrokerRateTypes();

            this.cbRateCalculation.DataSource = this.RateTypes.DefaultView;
            this.cbRateCalculation.DisplayMember = PBRateTypes.PartyBrokerRateType;
            this.cbRateCalculation.ValueMember = PBRateTypes.PartyBrokerRateTypeID;
            this.cbRateCalculation.BindingContext = this.BindingContext;

            this.MiniRateTypes = ETRMDataAccess.GetBrokerRateTypesNullable();

            this.cbMiniRateCalculation.DataSource = this.MiniRateTypes.DefaultView;
            this.cbMiniRateCalculation.DisplayMember = PBRateTypes.PartyBrokerRateType;
            this.cbMiniRateCalculation.ValueMember = PBRateTypes.PartyBrokerRateTypeID;
            this.cbMiniRateCalculation.BindingContext = this.BindingContext;
        }
        private void FillCurrencies()
        {
            this.FeeCurrencies = ETRMDataAccess.GetCurrencies();

            this.cbFeeCCY.DataSource = this.FeeCurrencies.DefaultView;
            this.cbFeeCCY.DisplayMember = CCY.Currency;
            this.cbFeeCCY.ValueMember = CCY.CurrencyID;
            this.cbFeeCCY.BindingContext = this.BindingContext;
        }
        private void FillPaymentFrequency()
        {
            this.PaymentFrequency = ETRMDataAccess.CalculationPeriod();

            this.cbPaymentFrequency.DataSource = this.PaymentFrequency.DefaultView;
            this.cbPaymentFrequency.DisplayMember = PFrequency.CalculationPeriod;
            this.cbPaymentFrequency.ValueMember = PFrequency.CalculationPeriodId;
            this.cbPaymentFrequency.BindingContext = this.BindingContext;
        }

        private bool IsBrokerRateValid()
        {
            decimal brokerRate;
            if (!decimal.TryParse(txtFeeRate.Text, out brokerRate))
            {
                MessageBox.Show("Please enter a valid broker rate");
                return false;
            }
            return true;
        }

        private void btBUBrokSave_Click(object sender, EventArgs e)
        {
            if (this.IsBrokerRateValid())
            {
                BrokerRate rate = new BrokerRate();

                rate.ExchangeID = Convert.ToInt32(cbExchange.SelectedValue);
                rate.FeeRate = Convert.ToDecimal(txtFeeRate.Text);

                rate.MiniFeeRate = null;
                if (!string.IsNullOrEmpty(txtMiniFeeRate.Text))
                {
                    rate.MiniFeeRate = Convert.ToDecimal(txtMiniFeeRate.Text);
                }

                rate.FeeTypeID = Convert.ToInt32(cbFeeType.SelectedValue);
                rate.PaymentDateOffset = txtPaymentDateOffset.Text;
                rate.PaymentFrequencyID = Convert.ToInt32(cbPaymentFrequency.SelectedValue);
                rate.RateCurrencyID = Convert.ToInt32(cbFeeCCY.SelectedValue);
                rate.SettlementTypeID = Convert.ToInt32(cbSettlementType.SelectedValue);
                rate.UnderlyingID = Convert.ToInt32(cbDeliveryType.SelectedValue);
                rate.BrokerRateTypeID = Convert.ToInt32(cbRateCalculation.SelectedValue);

                rate.MiniBrokerRateTypeID = Convert.ToInt32(cbMiniRateCalculation.SelectedValue);

                if (Convert.ToInt32(cbMiniRateCalculation.SelectedValue) < 0)
                {
                    rate.MiniBrokerRateTypeID = null;
                }

                rate.TradeTypeID = Convert.ToInt32(cbTradeType.SelectedValue);
                rate.ExecutionTypeID = Convert.ToInt32(cbExecutionType.SelectedValue);
                rate.DefaultRate = chkIsDefault.Checked;

                if (IsNew)
                {
                    rate.BrokerID = this.BrokerId;
                    rate.PartyBrokerRateID = ETRMDataAccess.MaxPartyBrokerRateId() + 1;
                    ETRMDataAccess.SavePartyBrokerRate(rate);
                }
                else
                {
                    rate.BrokerID = Convert.ToInt32(this.EditableInfo[BrokerRates.BrokerID]);
                    rate.PartyBrokerRateID = Convert.ToInt32(this.EditableInfo[BrokerRates.PartyBrokerRateID]);
                    ETRMDataAccess.UpdatePartyBrokerRate(rate);
                }
                this.Close();
            }
        }

        private void btBUBrokCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbExchange_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SetFeeType();
        }

        private void SetFeeType()
        {
            try
            {
                Int32 selectedValue = Convert.ToInt32(this.cbExchange.SelectedValue);

                switch (selectedValue)
                {
                    case Int32.MinValue:
                        this.FeeTypes = ETRMDataAccess.GetFeeType(FeeType.OTC);
                        this.SetFeeDataSource();
                        break;
                    default:
                        this.FeeTypes = ETRMDataAccess.GetFeeType(FeeType.Exchange);
                        this.SetFeeDataSource();
                        break;
                }
            }
            catch { }
        }

        private void cbBroker_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cbFeeType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
