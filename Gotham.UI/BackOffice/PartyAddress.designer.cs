﻿namespace Gotham.AddIn.Party
{
    partial class PartyAddress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbAddressType = new System.Windows.Forms.ComboBox();
            this.cbAddressIsDefault = new System.Windows.Forms.ComboBox();
            this.txtAddress1 = new System.Windows.Forms.TextBox();
            this.txtAddress2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAddress3 = new System.Windows.Forms.TextBox();
            this.txtAddress4 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAddress5 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cbAddState = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbAddCountry = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtMailCode = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtGroupEmail = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtGroupPhone = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtGroupFax = new System.Windows.Forms.TextBox();
            this.btnAddressDetailsSave = new System.Windows.Forms.Button();
            this.btnAddressDetailsCancel = new System.Windows.Forms.Button();
            this.txtDesc = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Address Type:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Default:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Address Line 1:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Address Line 2:";
            // 
            // cbAddressType
            // 
            this.cbAddressType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAddressType.FormattingEnabled = true;
            this.cbAddressType.Location = new System.Drawing.Point(108, 14);
            this.cbAddressType.Name = "cbAddressType";
            this.cbAddressType.Size = new System.Drawing.Size(121, 21);
            this.cbAddressType.TabIndex = 5;
            // 
            // cbAddressIsDefault
            // 
            this.cbAddressIsDefault.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAddressIsDefault.FormattingEnabled = true;
            this.cbAddressIsDefault.Location = new System.Drawing.Point(108, 41);
            this.cbAddressIsDefault.Name = "cbAddressIsDefault";
            this.cbAddressIsDefault.Size = new System.Drawing.Size(121, 21);
            this.cbAddressIsDefault.TabIndex = 6;
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new System.Drawing.Point(108, 71);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(290, 20);
            this.txtAddress1.TabIndex = 7;
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(108, 104);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(290, 20);
            this.txtAddress2.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 144);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Address Line 3:";
            // 
            // txtAddress3
            // 
            this.txtAddress3.Location = new System.Drawing.Point(108, 137);
            this.txtAddress3.Name = "txtAddress3";
            this.txtAddress3.Size = new System.Drawing.Size(290, 20);
            this.txtAddress3.TabIndex = 10;
            // 
            // txtAddress4
            // 
            this.txtAddress4.Location = new System.Drawing.Point(108, 172);
            this.txtAddress4.Name = "txtAddress4";
            this.txtAddress4.Size = new System.Drawing.Size(290, 20);
            this.txtAddress4.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 175);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Address Line 4:";
            // 
            // txtAddress5
            // 
            this.txtAddress5.Location = new System.Drawing.Point(108, 207);
            this.txtAddress5.Name = "txtAddress5";
            this.txtAddress5.Size = new System.Drawing.Size(290, 20);
            this.txtAddress5.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 210);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Address Line 5:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 250);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "City:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 284);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "State:";
            // 
            // cbAddState
            // 
            this.cbAddState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAddState.FormattingEnabled = true;
            this.cbAddState.Location = new System.Drawing.Point(108, 276);
            this.cbAddState.Name = "cbAddState";
            this.cbAddState.Size = new System.Drawing.Size(121, 21);
            this.cbAddState.TabIndex = 18;
            this.cbAddState.SelectedIndexChanged += new System.EventHandler(this.cbAddState_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 319);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Country:";
            // 
            // cbAddCountry
            // 
            this.cbAddCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAddCountry.FormattingEnabled = true;
            this.cbAddCountry.Location = new System.Drawing.Point(108, 311);
            this.cbAddCountry.Name = "cbAddCountry";
            this.cbAddCountry.Size = new System.Drawing.Size(121, 21);
            this.cbAddCountry.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 363);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Mail Code:";
            // 
            // txtMailCode
            // 
            this.txtMailCode.Location = new System.Drawing.Point(108, 356);
            this.txtMailCode.Name = "txtMailCode";
            this.txtMailCode.Size = new System.Drawing.Size(121, 20);
            this.txtMailCode.TabIndex = 22;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(285, 250);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 25;
            this.label13.Text = "Group Email:";
            // 
            // txtGroupEmail
            // 
            this.txtGroupEmail.Location = new System.Drawing.Point(380, 243);
            this.txtGroupEmail.Name = "txtGroupEmail";
            this.txtGroupEmail.Size = new System.Drawing.Size(187, 20);
            this.txtGroupEmail.TabIndex = 26;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(285, 283);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "Group Phone:";
            // 
            // txtGroupPhone
            // 
            this.txtGroupPhone.Location = new System.Drawing.Point(380, 276);
            this.txtGroupPhone.Name = "txtGroupPhone";
            this.txtGroupPhone.Size = new System.Drawing.Size(187, 20);
            this.txtGroupPhone.TabIndex = 28;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(287, 319);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 13);
            this.label15.TabIndex = 29;
            this.label15.Text = "Group Fax:";
            // 
            // txtGroupFax
            // 
            this.txtGroupFax.Location = new System.Drawing.Point(380, 312);
            this.txtGroupFax.Name = "txtGroupFax";
            this.txtGroupFax.Size = new System.Drawing.Size(187, 20);
            this.txtGroupFax.TabIndex = 30;
            // 
            // btnAddressDetailsSave
            // 
            this.btnAddressDetailsSave.Location = new System.Drawing.Point(15, 424);
            this.btnAddressDetailsSave.Name = "btnAddressDetailsSave";
            this.btnAddressDetailsSave.Size = new System.Drawing.Size(75, 23);
            this.btnAddressDetailsSave.TabIndex = 31;
            this.btnAddressDetailsSave.Text = "Save";
            this.btnAddressDetailsSave.UseVisualStyleBackColor = true;
            this.btnAddressDetailsSave.Click += new System.EventHandler(this.btnAddressDetailsSave_Click);
            // 
            // btnAddressDetailsCancel
            // 
            this.btnAddressDetailsCancel.Location = new System.Drawing.Point(492, 424);
            this.btnAddressDetailsCancel.Name = "btnAddressDetailsCancel";
            this.btnAddressDetailsCancel.Size = new System.Drawing.Size(75, 23);
            this.btnAddressDetailsCancel.TabIndex = 32;
            this.btnAddressDetailsCancel.Text = "Cancel";
            this.btnAddressDetailsCancel.UseVisualStyleBackColor = true;
            this.btnAddressDetailsCancel.Click += new System.EventHandler(this.btnAddressDetailsCancel_Click);
            // 
            // txtDesc
            // 
            this.txtDesc.Location = new System.Drawing.Point(380, 356);
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.Size = new System.Drawing.Size(187, 20);
            this.txtDesc.TabIndex = 34;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(287, 363);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(63, 13);
            this.label16.TabIndex = 33;
            this.label16.Text = "Description:";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(108, 243);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(158, 20);
            this.txtCity.TabIndex = 35;
            // 
            // PartyAddress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(589, 479);
            this.Controls.Add(this.txtCity);
            this.Controls.Add(this.txtDesc);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.btnAddressDetailsCancel);
            this.Controls.Add(this.btnAddressDetailsSave);
            this.Controls.Add(this.txtGroupFax);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtGroupPhone);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtGroupEmail);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtMailCode);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cbAddCountry);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cbAddState);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtAddress5);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtAddress4);
            this.Controls.Add(this.txtAddress3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtAddress2);
            this.Controls.Add(this.txtAddress1);
            this.Controls.Add(this.cbAddressIsDefault);
            this.Controls.Add(this.cbAddressType);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PartyAddress";
            this.Text = "PartyAddress";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbAddressType;
        private System.Windows.Forms.ComboBox cbAddressIsDefault;
        private System.Windows.Forms.TextBox txtAddress1;
        private System.Windows.Forms.TextBox txtAddress2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAddress3;
        private System.Windows.Forms.TextBox txtAddress4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAddress5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbAddState;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbAddCountry;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtMailCode;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtGroupEmail;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtGroupPhone;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtGroupFax;
        private System.Windows.Forms.Button btnAddressDetailsSave;
        private System.Windows.Forms.Button btnAddressDetailsCancel;
        private System.Windows.Forms.TextBox txtDesc;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtCity;
    }
}