﻿using Strazze.Data;
using Strazze.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Strazze.AddIn.Forms
{
    public partial class BUFunctions : Form
    {
        Int32 PartyId { get; set; }
        public BUFunctions(Int32 partyId)
        {
            InitializeComponent();
            this.PartyId = partyId;
            this.InitialiseView();
        }

        private void InitialiseView()
        {
            this.FillComboBox();
        }

        private void FillComboBox()
        {
            DataTable functions = ETRMDataAccess.GetPartyFunction();
            this.cbPartyFunctions.DataSource = functions.DefaultView;
            this.cbPartyFunctions.ValueMember = Function.PartyFunctionTypeID;
            this.cbPartyFunctions.DisplayMember = Function.Name;
            this.cbPartyFunctions.BindingContext = this.BindingContext;
        }

        private void btnAddBUFunction_Click(object sender, EventArgs e)
        {
            Int32 selectedFunction = Convert.ToInt32(this.cbPartyFunctions.SelectedValue);
            if(this.IsIn(this.PartyId, selectedFunction))
            {
                MessageBox.Show("You are trying to add a function that is already listed for the selected party!");
            }
            else
            {
                Int32 result = ETRMDataAccess.SavePartyFunction(this.PartyId, selectedFunction);
                if (result != Int32.MinValue)
                {
                    MessageBox.Show("The function was successfully added for the selected party!");
                }
            }
        }

        private void btnCancelBUFunction_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private bool IsIn(Int32 partyId, Int32 functionId)
        {
            return ETRMDataAccess.IsPartyFunctionIn(partyId, functionId);
        }
    }
}
