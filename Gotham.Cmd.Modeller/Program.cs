﻿using Newtonsoft.Json;
using Gotham.Data;
using Gotham.Elements;
using Gotham.Elements.Instruments;
using Gotham.PaperConfirmationEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Gotham.Elements.Instruments.Brent;
using Gotham.Domain.Documents;
using Gotham.Elements.Schemas0510;
using System.Xml.Serialization;

namespace Gotham.Cmd.Modeller
{
    class Program
    {
        static void Main(string[] args)
        {
            //foreach(var messageID in GetAllMessagesWithoutDocuments())
            //{
            //    GenerateDocument((int)messageID);
            //}
            //MOAT();
            //GenerateDocument(12);
            //GenerateDocument(13);
            //GenerateDocument(14);


            //JsonifyWTI();
            //JsonifyBrent(); 

            //string wti = JsonifyWTI();
            //string gtma = JsonifyGTMA();
            //string a = JsonifyBrent();

            GenerateGTMA0510();
        }

        private static void GenerateGTMA0510()
        {

            GTMA0510 g = new GTMA0510("GBP", 23, "MWh", 200, "MWh");

            string json = g.AsFPMLJsonified();


            //MATEUSZ!!!! Uncomment the below lines and you can see what I meant about 
            //the XSD datasets not compliant with xml serialisation: I wanted to see if 
            //rendering the data in xml rather than Json made any difference.

            //var writer = new StringWriter();
            //var serializer = new XmlSerializer(typeof(GTMA0510));
            //serializer.Serialize(writer, g);
            //string xml = writer.ToString();


            //////string gtma = string.Empty;
            //////GTMAFixedPrice gtmaFixed = new GTMAFixedPrice("1", "9", new Date(2019, 6, 1), new DateTime(2019, 5, 30), "CET", "1", "9", new DateTime(2019, 6, 1), new DateTime(2019, 7, 31), 67, 1, "M", true, 23.45, "EUR", "MWh", true);
            //////gtma = gtmaFixed.AsFPMLJsonified();

            //var writer2 = new StringWriter();
            //var serializer2 = new XmlSerializer(typeof(GTMAFixedPrice));
            //serializer2.Serialize(writer2, gtmaFixed);
            //string xml2 = writer2.ToString();
        }

        private static void GenerateGTMAHomeMade()
        {
            //GTMA0510 gtma = new GTMA0510();

            GTMA0510 g = new GTMA0510("GBP", 23, "MWh", 200, "MWh");

            string json = g.AsFPMLJsonified();

            var writer = new StringWriter();
            var serializer = new XmlSerializer(typeof(GTMA0510));
            serializer.Serialize(writer, g);
            string xml = writer.ToString();
        }

        private static IEnumerable<long> GetAllMessagesWithoutDocuments()
        {
            return ETRMDataAccess.GetAllMessagesWithoutDocuments();
        }

        private void TestCpML()
        {
            RosettaStone.EfetBcnV4R2.BrokerConfirmation c = new RosettaStone.EfetBcnV4R2.BrokerConfirmation();
            c.Agents = new RosettaStone.EfetBcnV4R2.BrokerConfirmationAgent[10];
            c.CapacityUnit = RosettaStone.EfetBcnV4R2.UnitOfMeasureType.BBL;
            c.CapacityUnitSpecified = true;

            System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(typeof(RosettaStone.EfetBcnV4R2.BrokerConfirmation));
            System.IO.StreamWriter sr = new System.IO.StreamWriter(@"F:\Development\RosettaStone\poc\mother_of_all_xml.xml");
            s.Serialize(sr, c);

            string test = string.Empty;

        }

        static string JsonifyGTMA()
        {
            string gtma = string.Empty;
            GTMAFixedPrice gtmaFixed = new GTMAFixedPrice("1", "9", new Date(2019, 6, 1), new DateTime(2019, 5, 30), "CET", "1", "9", new DateTime(2019, 6, 1), new DateTime(2019, 7, 31), 67, 1, "M", true, 23.45, "EUR", "MWh", true);
            gtma = gtmaFixed.AsFPMLJsonified();
            return gtma;
        }

        static string JsonifyWTI()
        {
            string wtfti = string.Empty;
            WTIFloatingPrice wtf = new WTIFloatingPrice("1", "9", "1", "9", new Date(2017, 10, 30), new DateTime(2017, 10, 30), "CET", new Date(2017, 11, 01), new Date(2017, 11, 30), "USD", 1, "T", false, "Oil", "WTI", "TeppcoSeawayCrude", "Cushing", false, "FOB", "BBL", "PerCalculationPeriod", 10000, "OIL-WTI-NYMEX", "Settlement", "CommodityBusiness", "All");
            wtfti = wtf.AsFPMLJsonified();

            //ETRMDataAccess.InsertGothamMessage(2, 1, 1, 1, null, wtfti, string.Empty, string.Empty, "Gotham", "Gazprom", "123", "OIL_WTI_NYMEX");
            //ETRMDataAccess.InsertGothamMessage(2, 1, 1, 1, null, wtfti, string.Empty, string.Empty, "Gotham", "Gazprom", "124", "OIL_WTI_NYMEX_Cst");
            //ETRMDataAccess.InsertGothamMessage(2, 1, 1, 1, null, wtfti, string.Empty, string.Empty, "1001", "1001", "125", "OIL-WTI-NYMEX");

            return wtfti;
        }

        static string JsonifyBrent()
        {
            string brentify = string.Empty;
            Brent brent = new Brent(new DateTime(2017, 11, 1), new DateTime(2017, 11, 30), "USD", "GETTHAT", 1, "M", "Settlement", "FirstNearby", "BBL", "PerSettlementPeriod", 65000, 195000, "BATSHITCRAZY", 79.37, "BBL", "USD", "BBL", "PerSettlementPeriod", 65000, 195000);
            brentify = JsonConvert.SerializeObject(brent);
            return brentify;
        }

        static void MOAT()
        {
            string partyA = "Gazprom";
            string partyB = "Alpha Energy";
            Date tradeDate = new Date(2017, 9, 5);
            Date effectiveDate = new Date(2017, 10, 1);
            Date terminationDate = new Date(2017, 10, 31);
            string settlementCCY = "EUR";
            string gasType = "NaturalGas";
            string deliveryPt = "ZBT";
            string deliveryType = "Firm";
            string physQtyUnit = "GJ";
            string physQtyFreq = "PerCalendarDay";
            double physQty = 2400.0;
            string totalQtyUnit = "GJ";
            double totalQty = 74400.0;
            double fixedPrice = 5.05;
            string fixedPriceCCY = "EUR";
            string fixedPriceUnit = "GJ";
            string agreement = "ISDA";

            ZBTFixedPrice p = new ZBTFixedPrice(partyA, partyB, partyA, partyB, tradeDate, effectiveDate, terminationDate, settlementCCY, gasType, deliveryPt, deliveryType, physQtyUnit, physQtyFreq, physQty, totalQtyUnit, totalQty, fixedPrice, fixedPriceCCY, fixedPriceUnit, agreement);

            Int32 schemaID = 1;
            Int32 templateID = 1;
            Int32 routeID = 1;
            Int32 workflowID = 1;

            p.InternalPartyID = "1";
            p.ExternalPartyID = "1002";
            p.TradeID = "2010";

            string json = JsonConvert.SerializeObject(p);

            bool saved = ETRMDataAccess.InsertGothamMessage(schemaID, templateID, routeID, workflowID, null, json, string.Empty, string.Empty, p.InternalPartyID, p.ExternalPartyID, p.TradeID, string.Empty);

            tradeDate = new Date(2017, 9, 5);
            effectiveDate = new Date(2017, 11, 1);
            terminationDate = new Date(2017, 11, 30);
            physQty = 2400.0;
            totalQty = 72000;

            ZBTFixedPrice p2 = new ZBTFixedPrice(partyA, partyB, partyA, partyB, tradeDate, effectiveDate, terminationDate, settlementCCY, gasType, deliveryPt, deliveryType, physQtyUnit, physQtyFreq, physQty, totalQtyUnit, totalQty, fixedPrice, fixedPriceCCY, fixedPriceUnit, agreement);
            p2.InternalPartyID = "1";
            p2.ExternalPartyID = "1002";
            p2.TradeID = "2011";

            string json2 = JsonConvert.SerializeObject(p2);

            saved = ETRMDataAccess.InsertGothamMessage(schemaID, templateID, routeID, workflowID, null, json2, string.Empty, string.Empty, p2.InternalPartyID, p2.ExternalPartyID, p2.TradeID, string.Empty);

            tradeDate = new Date(2017, 10, 5);
            effectiveDate = new Date(2017, 12, 1);
            terminationDate = new Date(2017, 12, 31);
            physQty = 2400.0;
            totalQty = 74400;

            ZBTFixedPrice p3 = new ZBTFixedPrice(partyA, partyB, partyA, partyB, tradeDate, effectiveDate, terminationDate, settlementCCY, gasType, deliveryPt, deliveryType, physQtyUnit, physQtyFreq, physQty, totalQtyUnit, totalQty, fixedPrice, fixedPriceCCY, fixedPriceUnit, agreement);
            p3.InternalPartyID = "1";
            p3.ExternalPartyID = "1002";
            p3.TradeID = "2012";

            string json3 = JsonConvert.SerializeObject(p3);

            saved = ETRMDataAccess.InsertGothamMessage(schemaID, templateID, routeID, workflowID, null, json3, string.Empty, string.Empty, p3.InternalPartyID, p3.ExternalPartyID, p3.TradeID, string.Empty);

            tradeDate = new Date(2017, 10, 5);
            effectiveDate = new Date(2018, 1, 1);
            terminationDate = new Date(2018, 12, 31);
            physQty = 2400.0;
            totalQty = 876000;

            ZBTFixedPrice p4 = new ZBTFixedPrice(partyA, partyB, partyA, partyB, tradeDate, effectiveDate, terminationDate, settlementCCY, gasType, deliveryPt, deliveryType, physQtyUnit, physQtyFreq, physQty, totalQtyUnit, totalQty, fixedPrice, fixedPriceCCY, fixedPriceUnit, agreement);
            p4.InternalPartyID = "1";
            p4.ExternalPartyID = "1002";
            p4.TradeID = "2013";

            string json4 = JsonConvert.SerializeObject(p4);

            saved = ETRMDataAccess.InsertGothamMessage(schemaID, templateID, routeID, workflowID, null, json4, string.Empty, string.Empty, p4.InternalPartyID, p4.ExternalPartyID, p4.TradeID, string.Empty);

            tradeDate = new Date(2017, 10, 7);
            effectiveDate = new Date(2018, 3, 1);
            terminationDate = new Date(2018, 3, 31);
            physQty = 2400.0;
            totalQty = 74400;

            ZBTFixedPrice p5 = new ZBTFixedPrice(partyA, partyB, partyA, partyB, tradeDate, effectiveDate, terminationDate, settlementCCY, gasType, deliveryPt, deliveryType, physQtyUnit, physQtyFreq, physQty, totalQtyUnit, totalQty, fixedPrice, fixedPriceCCY, fixedPriceUnit, agreement);
            p5.InternalPartyID = "1";
            p5.ExternalPartyID = "1002";
            p5.TradeID = "2014";

            string json5 = JsonConvert.SerializeObject(p5);

            saved = ETRMDataAccess.InsertGothamMessage(schemaID, templateID, routeID, workflowID, null, json5, string.Empty, string.Empty, p5.InternalPartyID, p5.ExternalPartyID, p5.TradeID, string.Empty);
        }

        private static bool GenerateDocument(int messageID)
        {
            try
            {
                DataTable message = ETRMDataAccess.GetGTMessage(messageID);

                string jsonifiedView = ETRMDataAccess.GetStringDTRElement(message.Rows[0], "Root");

                ZBTFixedPrice zbtMessage = JsonConvert.DeserializeObject<ZBTFixedPrice>(jsonifiedView);

                Int32 schemaID = ETRMDataAccess.GetInt32DTRElement(message.Rows[0], "SchemaID").Value;

                DataTable schema = ETRMDataAccess.GetGothamSchemaTableDefinition(schemaID);

                Dictionary<string, string> confirmationSchemaElements = new Dictionary<string, string>();
                Dictionary<string, string> confirmationView = new Dictionary<string, string>();

                foreach (DataRow r in schema.Rows)
                {
                    if (Convert.ToInt32(r["IsFixed"]) == 1)
                    {
                        string key = Convert.ToString(r["Key"]);
                        string tag = Convert.ToString(r["Tag"]);
                        if (!confirmationSchemaElements.ContainsKey(key))
                        {
                            confirmationSchemaElements.Add(key, tag);
                        }
                    }
                }

                PrintPropertiesWithClassPath(zbtMessage, 0, string.Empty);

                //CreatePropertiesDitctionary(Dictionary<string, string> dic, object obj, int indent, string path, Dictionary<string, string> propertiesDictionary)

                CreatePropertiesDitctionary(confirmationSchemaElements, zbtMessage, 0, string.Empty, confirmationView);

                DataTable party = ETRMDataAccess.GetGTExtPartyInfo(zbtMessage.ExternalPartyID);

                if (!ETRMDataAccess.IsEmpty(party))
                {
                    try
                    {
                        confirmationView.Add("EXT_COUNTERPARTY_NAME", ETRMDataAccess.GetStringDTRElement(party, "EXT_COUNTERPARTY_NAME"));
                        confirmationView.Add("EXT_ADDRESS_LINE_1", ETRMDataAccess.GetStringDTRElement(party, "EXT_ADDRESS_LINE_1"));
                        confirmationView.Add("EXT_ADDRESS_LINE_2", ETRMDataAccess.GetStringDTRElement(party, "EXT_ADDRESS_LINE_2"));
                        confirmationView.Add("EXT_ADDRESS_LINE_3", ETRMDataAccess.GetStringDTRElement(party, "EXT_ADDRESS_LINE_3"));
                        confirmationView.Add("EXT_ADDRESS_CITY", ETRMDataAccess.GetStringDTRElement(party, "EXT_ADDRESS_CITY"));
                        confirmationView.Add("EXT_ADDRESS_COUNTRY", ETRMDataAccess.GetStringDTRElement(party, "EXT_ADDRESS_COUNTRY"));
                        confirmationView.Add("EXT_ADDRESS_MAIL_CODE", ETRMDataAccess.GetStringDTRElement(party, "EXT_ADDRESS_MAIL_CODE"));
                        confirmationView.Add("TRADE_ID", zbtMessage.TradeID);
                        confirmationView.Add("CONFIRM_VERSION", "1");
                    }
                    catch
                    {

                    }
                }


                DataSet ds = new DataSet();
                ds.Tables.Add(new DataTable("data"));

                byte[] template = ETRMDataAccess.GetByteArrayDTRElement(message.Rows[0], "Template");
                byte[] document = OpenXmlHandler.GetWordReport(template, ds, confirmationView);
                byte[] pdfFile = PdfConverter.AsPdf(document);

                //File.WriteAllBytes(@"E:\uat\GothamPaperConfirmation-01.docx", document);

                var docInfo = new DocumentInfo
                {
                    DocumentName = string.Format("GothamConfirmation-{0}-v01.docx", zbtMessage.TradeID),
                    MessageID = messageID,
                    PdfFileName = string.Format("GothamConfirmation-{0}-v01.pdf", zbtMessage.TradeID)
                };

                ETRMDataAccess.InsertDocument(docInfo, document, pdfFile);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private static string DigOutTheInfo(string name, ZBTFixedPrice message)
        {
            string info = string.Empty;

            string[] objects = name.Split(Convert.ToChar("."));

            foreach (string o in objects)
            {
            }

            return info;
        }

        public static void PrintProperties(object obj, int indent)
        {
            if (obj == null) return;
            string indentString = new string(' ', indent);
            Type objType = obj.GetType();
            PropertyInfo[] properties = objType.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                object propValue = property.GetValue(obj, null);
                var elems = propValue as IList;
                if (elems != null)
                {
                    foreach (var item in elems)
                    {
                        PrintProperties(item, indent + 3);
                    }
                }
                else
                {
                    // This will not cut-off System.Collections because of the first check
                    if (property.PropertyType.Assembly == objType.Assembly)
                    {
                        Console.WriteLine("{0}{1}:", indentString, property.Name);

                        PrintProperties(propValue, indent + 2);
                    }
                    else
                    {
                        Console.WriteLine("{0}{1}: {2}", indentString, property.Name, propValue);
                    }
                }
            }
        }

        public static void PrintPropertiesWithClassPath(object obj, int indent, string path)
        {
            if (obj == null) return;
            string indentString = new string(' ', indent);
            Type objType = obj.GetType();
            PropertyInfo[] properties = objType.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                object propValue = property.GetValue(obj, null);
                var elems = propValue as IList;
                if (elems != null)
                {
                    foreach (var item in elems)
                    {
                        //string newPath = path + "." + property.Name;
                        string newPath = (path == string.Empty) ? property.Name : path + "." + property.Name;
                        PrintPropertiesWithClassPath(item, indent + 3, newPath);
                    }
                }
                else
                {
                    if (property.PropertyType.Assembly == objType.Assembly)
                    {
                        //string newPath = path + "." + property.Name;
                        string newPath = (path == string.Empty) ? property.Name : path + "." + property.Name;
                        Console.WriteLine("{0}{1}:", indentString, newPath);
                        PrintPropertiesWithClassPath(propValue, indent + 2, newPath);
                    }
                    else
                    {
                        string newPath = path + "." + property.Name;
                        Console.WriteLine("{0}{1}: {2}", indentString, newPath, propValue);
                    }
                }
            }
        }

        public static void CreatePropertiesDitctionary(Dictionary<string, string> dic, object obj, int indent, string path, Dictionary<string, string> propertiesDictionary)
        {
            if (obj == null) return;
            string indentString = new string(' ', indent);
            Type objType = obj.GetType();
            PropertyInfo[] properties = objType.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                object propValue = property.GetValue(obj, null);
                var elems = propValue as IList;
                if (elems != null)
                {
                    foreach (var item in elems)
                    {
                        //string newPath = path + "." + property.Name;
                        string newPath = (path == string.Empty) ? property.Name : path + "." + property.Name;
                        CreatePropertiesDitctionary(dic, item, indent + 3, newPath, propertiesDictionary);
                    }
                }
                else
                {
                    if (property.PropertyType.Assembly == objType.Assembly)
                    {
                        //string newPath = path + "." + property.Name;
                        string newPath = (path == string.Empty) ? property.Name : path + "." + property.Name;
                        //Console.WriteLine("{0}{1}:", indentString, newPath);
                        CreatePropertiesDitctionary(dic, propValue, indent + 2, newPath, propertiesDictionary);
                    }
                    else
                    {
                        string newPath = path + "." + property.Name;
                        Console.WriteLine("{0}{1}: {2}", indentString, newPath, propValue);

                        if (propValue != null)
                        {
                            if (dic.ContainsKey(newPath))
                            {
                                string newKey = string.Empty;
                                if (dic.TryGetValue(newPath, out newKey))
                                {
                                    propertiesDictionary.Add(newKey, propValue.ToString());
                                }
                            }
                        }
                    }
                }
            }
        }


    }
}
