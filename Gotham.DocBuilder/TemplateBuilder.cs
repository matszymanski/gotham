﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using Gotham.UI.TemplateBuilder;
using Word = Microsoft.Office.Interop.Word;
using Gotham.ElementsUI.Templates;
using System.Data;
using Gotham.Data;
using System.Windows.Forms;
using System.IO;
using COM = System.Runtime.InteropServices.ComTypes;
using Gotham.ElementsUI.Admin;

namespace Gotham.DocBuilder
{
    public partial class TemplateBuilder
    {
        Word.Window currentWindow;

        public NativeWindow fieldsWindow;
        private DataTable SchemaElements { get; set; }
        private List<string> TextElements { get; set; }
        private List<string> TableNames { get; set; }
        private string CurrentSchema { get; set; }
        private void TemplateBuilder_Load(object sender, RibbonUIEventArgs e)
        {
            this.EnableTemplateGeneration(false);
        }

        private void btnCreateSchema_Click(object sender, RibbonControlEventArgs e)
        {
            SchemaPicker sp = new SchemaPicker(true);
            sp.ShowDialog();

            Int32 selectedSchema = sp.SelectedSchemaID;
            string schemaName = sp.SchemaName;

            if (selectedSchema > 0)
            {
                DataTable t = ETRMDataAccess.GetGothamSchemaTableDefinition(selectedSchema);
                if (!ETRMDataAccess.IsEmpty(t))
                {
                    //This is a new Schema. A new ID needs to be assigned to it.
                    Int32 newSchemaID = ETRMDataAccess.GetNextSchemaID();
                    foreach (DataRow r in t.Rows)
                    {
                        r[SchemaDefinition.SchemaID] = newSchemaID;
                        //r[SchemaDefinition.IsFixed] = 1;
                    }

                    //Save Schema header and schema content
                    if (ETRMDataAccess.InsertSchemaHeader(newSchemaID, selectedSchema, schemaName))
                    {
                        //t = this.PrepareSchema(t);
                        ETRMDataAccess.InsertSchema(t);
                    }

                    t = ETRMDataAccess.GetGothamSchemaDefinition(selectedSchema); //to add back the additional columns
                    SchemaBuilder sb = new SchemaBuilder(t, newSchemaID, schemaName);
                    sb.ShowDialog();
                }
            }
        }

        private DataTable PrepareSchema(DataTable t)
        {
            List<string> definition = new List<string>();
            definition.Add("SchemaID");
            definition.Add("KeyID");
            definition.Add("Key");
            definition.Add("Tag");
            definition.Add("FieldTypeID");
            definition.Add("IsFixed");

            for (int i = t.Columns.Count - 1; i >= 0; i--)
            {
                DataColumn dc = t.Columns[i];
                if (!definition.Contains(dc.ColumnName))
                {
                    t.Columns.Remove(dc);
                }
            }
            return t;
        }

        private void btnEditSchema_Click(object sender, RibbonControlEventArgs e)
        {
            ETRMDataAccess.WriteLogEntry("Entering Edit Schema");
            try
            {
                SchemaPicker sp = new SchemaPicker(false);
                sp.ShowDialog();
                Int32 selectedSchema = sp.SelectedSchemaID;
                string schemaName = sp.SchemaName;

                if (selectedSchema > 0)
                {
                    DataTable t = ETRMDataAccess.GetGothamSchemaDefinition(selectedSchema);

                    if (!ETRMDataAccess.IsEmpty(t))
                    {
                        SchemaBuilder sb = new SchemaBuilder(t, selectedSchema, schemaName);
                        sb.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"{ex.Message}"+Environment.NewLine + Environment.NewLine + $"{ex.InnerException}");
            }
        }

        private void btnLoadSchema_Click(object sender, RibbonControlEventArgs e)
        {
            SchemaPicker sp = new SchemaPicker();
            sp.ShowDialog();

            Int32 selectedSchema = sp.SelectedSchemaID;
            this.CurrentSchema = sp.SchemaName;

            if (selectedSchema > 0)
            {
                this.SchemaElements = ETRMDataAccess.GetGothamSchemaDefinition(selectedSchema);
                if (ETRMDataAccess.IsEmpty(this.SchemaElements))
                {
                    this.lblCurrentSchema.Label = string.Format("No schema currently loaded");
                    MessageBox.Show("The selected schema has no elements associated! You cannot proceed with template generation");
                    this.EnableTemplateGeneration(false);
                }
                else
                {
                    this.FillLists();
                    this.lblCurrentSchema.Label = string.Format("Schema Loaded: {0}", this.CurrentSchema);
                    this.EnableTemplateGeneration(true);
                }
            }
        }

        private void FillLists()
        {
            this.TextElements = new List<string>();
            this.TextElements = (from x in this.SchemaElements.AsEnumerable() where x.Field<string>("FieldType") == "Fixed" select x.Field<string>("Tag")).ToList();
            this.TableNames = new List<string>();
            this.TableNames = (from x in this.SchemaElements.AsEnumerable() where x.Field<string>("FieldType") != "Fixed" select x.Field<string>("Tag")).ToList();
        }

        private void btnOpenTemplate_Click(object sender, RibbonControlEventArgs e)
        {
            TemplatePicker tp = new TemplatePicker();
            tp.ShowDialog();

            if (tp.TemplateID <= 0) { return; }

            DataTable t = ETRMDataAccess.GTMGetTemplate(tp.TemplateID);
            byte[] template = ETRMDataAccess.GetByteArrayDTRElement(t.Rows[0], TemplateConstants.Template);

            SaveFileDialog d = new SaveFileDialog();
            d.FileName = tp.TemplateName;

            d.ShowDialog();

            string destination = d.FileName;

            if (ETRMDataAccess.ByteArrayToFile(destination, template))
            {
                Word.Document doc = Globals.ThisAddIn.Application.Documents.Open(destination);
                doc.Activate();
            }
            else
            {
                MessageBox.Show("Problems in saving the file to disk");
            }

        }

        private void btnUploadTemplate_Click(object sender, RibbonControlEventArgs e)
        {
            Word.Window window = (Word.Window)e.Control.Context;
            Word.Application app = new Word.Application();
            Word.Document d = window.Document;

            bool canUpload = false;

            if (d.Saved)
            {
                canUpload = true;
            }
            else
            {
                try
                {
                    d.Save();
                }
                catch
                {
                }

                if (!d.Saved)
                {
                    MessageBox.Show(string.Format("The template will not be uploaded."));
                    return;
                }
                else
                {
                    canUpload = true;
                }
            }
            if (canUpload && this.UploadDocument(d))
            {
                MessageBox.Show("The document has been saved");
                this.CleanUp();
            }
            else
            {
                MessageBox.Show("The document has NOT been saved");
            }
        }

        private void CleanUp()
        {
            this.lblCurrentSchema.Label = string.Format("No schema currently loaded");
            this.EnableTemplateGeneration(false);
        }
        private bool UploadDocument(Word.Document d)
        {
            try
            {
                string tempFileName = Path.GetTempFileName() + ".docx";
                COM.IPersistFile compoundDocument = d as COM.IPersistFile;
                compoundDocument.Save(tempFileName, false);

                byte[] currentDocument = File.ReadAllBytes(tempFileName);
                string fileName = Path.GetFileName(d.FullName);

                TemplateUpload tu = new TemplateUpload(fileName);
                tu.ShowDialog();

                if (tu.IsCancel) { return false; }

                Int32 templateID = tu.SelectedTemplateID;
                string templateName = tu.TemplateName;

                if (templateID < 0)
                {
                    ETRMDataAccess.InsertTemplate(templateName, currentDocument);
                }
                else
                {
                    ETRMDataAccess.UpdateTemplate(templateID, templateName, currentDocument);
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private void btnAddField_Click(object sender, RibbonControlEventArgs e)
        {
            this.currentWindow = (Word.Window)e.Control.Context;
            this.AddTextElement();
        }

        public void AddTextElement()
        {

            this.currentWindow = Globals.ThisAddIn.Application.ActiveWindow;

            TemplateFieldsSelector p = new TemplateFieldsSelector(this.TextElements, "Field Selector");

            p.ShowDialog();

            if (!string.IsNullOrEmpty(p.SelectedItem))
            {
                //Word.Window window = (Word.Window)e.Control.Context;
                //Word.

                //Word.Window window = Globals.ThisAddIn.Application.Windows[;

                //Word.Application app = new Word.Application();
                Word.Document d = this.currentWindow.Document;
                d.MailMerge.Fields.Add(this.currentWindow.Selection.Range, p.SelectedItem);
            }
        }



        //////////private void btnAddField_Click(object sender, RibbonControlEventArgs e)
        //////////{
        //////////    TemplateFieldsSelector p = new TemplateFieldsSelector(this.TextElements);
        //////////    p.ShowDialog();

        //////////    if (p.SelectedField.Count > 0)
        //////////    {
        //////////        Word.Window window = (Word.Window)e.Control.Context;
        //////////        //int left, top, width, height;
        //////////        //window.GetPoint(out left, out top, out width, out height, window.Selection);
        //////////        //window.Selection.Text = "Si vis pacem para bellum";
        //////////        Word.Application app = new Word.Application();

        //////////        Word.Document d = window.Document;

        //////////        //Word.MailMergeField f = new Word.MailMergeField();

        //////////        d.MailMerge.Fields.Add(window.Selection.Range, p.SelectedField[0]);

        //////////        //d.Fields.Add(window.Selection, )

        //////////        //string mergeField = string.Format(@"{ MERGEFIELD {0} \*MERGEFORMAT }", p.SelectedField[0]);

        //////////        //window.Selection.Text = mergeField;
        //////////    }
        //////////}

        private void btnAddTable_Click(object sender, RibbonControlEventArgs e)
        {
            TemplateFieldsSelector p = new TemplateFieldsSelector(this.TableNames, "Table Selector");
            p.ShowDialog();

            if (!string.IsNullOrEmpty(p.SelectedItem))
            {
                Int32 cols = this.GetNumberOfColumns(p.SelectedItem);
                if (cols > 0)
                {
                    object oMissing = System.Reflection.Missing.Value;

                    Word.Window window = (Word.Window)e.Control.Context;
                    Word.Application app = new Word.Application();
                    Word.Document d = window.Document;
                    Word.Table table;
                    object oEndOfDoc = "\\endofdoc";
                    //Word.Range wrdRng = d.Bookmarks.get_Item(ref oEndOfDoc).Range;
                    Word.Range wrdRng = window.Selection.Range;
                    table = d.Tables.Add(wrdRng, 1, cols, ref oMissing, ref oMissing);
                    table.Title = p.SelectedItem;
                    table.Borders.InsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;
                    table.Borders.OutsideLineStyle = Word.WdLineStyle.wdLineStyleSingle;
                }
            }
        }

        private Int32 GetNumberOfColumns(string fieldName)
        {
            Int32 columns = 0;

            try
            {
                columns = Convert.ToInt32((from x in this.SchemaElements.AsEnumerable() where x.Field<string>("Tag").Equals(fieldName) select x.Field<int>("Columns")).FirstOrDefault());
            }
            catch
            {
            }

            return columns;
        }

        private void EnableTemplateGeneration(bool enable)
        {
            this.btnAddField.Enabled = enable;
            this.btnAddTable.Enabled = enable;
            this.btnUploadTemplate.Enabled = enable;
            this.btnOpenTemplate.Enabled = enable;
        }

        private void btnConfiguration_Click(object sender, RibbonControlEventArgs e)
        {
            Administration admin = new Administration();
            admin.ShowDialog();
        }
    }
}
