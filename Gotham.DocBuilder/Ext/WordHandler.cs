﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Word = Microsoft.Office.Interop.Word;

namespace Gotham.DocBuilder.Ext
{
    public static class WordHandler
    {
        public static void SaveCopyAs(this Word.Document doc, string path)
        {
            var persistFile = (System.Runtime.InteropServices.ComTypes.IPersistFile)doc;
            persistFile.Save(path, false);
        }

        public static bool IsValidFilename(string testName)
        {
            Regex containsABadCharacter = new Regex("[" + Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars())) + "]");
            if (containsABadCharacter.IsMatch(testName)) { return false; };
            return true;
        }
    }
}
