﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Word;
using System.Windows.Forms;

namespace Gotham.DocBuilder
{
    public partial class ThisAddIn
    {
        //Word.Application application;
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            //application = this.Application;
            this.Application.WindowBeforeRightClick += new Word.ApplicationEvents4_WindowBeforeRightClickEventHandler(application_WindowBeforeRightClick);
            this.Application.WindowBeforeDoubleClick += new Word.ApplicationEvents4_WindowBeforeDoubleClickEventHandler(WindowBeforeDoubleClick);
            //application.WindowBeforeRightClick += new Word.ApplicationEvents(application_WindowBeforeRightClick);

            /*
            application.CustomizationContext = application.ActiveDocument;

            Office.CommandBar commandBar = application.CommandBars["Text"];
            Office.CommandBarButton btnAddText = (Office.CommandBarButton)commandBar.Controls.Add(Office.MsoControlType.msoControlButton);
            btnAddText.accName = Constants.Constants.AddField;
            btnAddText.Caption = Constants.Constants.AddField;
            //btnAddText.Click -= this.btnShowFields;
            btnAddText.Click += new Microsoft.Office.Core._CommandBarButtonEvents_ClickEventHandler(this.btnShowFields);

            Office.CommandBarButton btnAddTable = (Office.CommandBarButton)commandBar.Controls.Add(Office.MsoControlType.msoControlButton);
            btnAddTable.accName = Constants.Constants.AddTable;
            btnAddTable.Caption = Constants.Constants.AddTable;
            //btnAddTable.Click -= this.btnShowTables;
            btnAddTable.Click += new Microsoft.Office.Core._CommandBarButtonEvents_ClickEventHandler(this.btnShowTables);
            */
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        private Office.CommandBar GetTextContextMenu()
        {
            return this.Application.CommandBars["Text"];
        }
        private void ResetContextMenu()
        {
            GetTextContextMenu().Reset();
        }

        private void AddMenuItems()
        {
            this.Application.CustomizationContext = this.Application.ActiveDocument;

            Office.CommandBar commandBar = this.Application.CommandBars["Text"];

            Office.CommandBarButton btnAddText = (Office.CommandBarButton)commandBar.Controls.Add(Office.MsoControlType.msoControlButton);
            btnAddText.accName = Constants.Constants.AddField;
            btnAddText.Caption = Constants.Constants.AddField;
            btnAddText.Click += new Microsoft.Office.Core._CommandBarButtonEvents_ClickEventHandler(this.btnShowFields);

            Office.CommandBarButton btnAddTable = (Office.CommandBarButton)commandBar.Controls.Add(Office.MsoControlType.msoControlButton);
            btnAddTable.accName = Constants.Constants.AddTable;
            btnAddTable.Caption = Constants.Constants.AddTable;
            btnAddTable.Click += new Microsoft.Office.Core._CommandBarButtonEvents_ClickEventHandler(this.btnShowTables);
        }


        public void application_WindowBeforeRightClick(Word.Selection selection, ref bool Cancel)
        {
            this.ResetContextMenu();
            this.AddMenuItems();

            //////string status = Globals.Ribbons.TemplateBuilder.lblCurrentSchema.Label;

            //////if (string.IsNullOrEmpty(status) || status.Equals("No schema currently loaded"))
            //////{
            //////    SetCommandVisibility(Constants.Constants.AddField, false);
            //////    SetCommandVisibility(Constants.Constants.AddTable, false);
            //////}
            //////else
            //////{
            //////    SetCommandVisibility(Constants.Constants.AddField, true);
            //////    SetCommandVisibility(Constants.Constants.AddTable, true);
            //////}
        }

        public void WindowBeforeDoubleClick(Word.Selection selection, ref bool Cancel)
        {
            //this.ResetContextMenu();
            //this.AddMenuItems();

            //MessageBox.Show("!");

            string status = Globals.Ribbons.TemplateBuilder.lblCurrentSchema.Label;

            if (!(string.IsNullOrEmpty(status) || status.Equals("No schema currently loaded")))
            {
                Globals.Ribbons.TemplateBuilder.AddTextElement();
            }

            //////string status = Globals.Ribbons.TemplateBuilder.lblCurrentSchema.Label;

            //////if (string.IsNullOrEmpty(status) || status.Equals("No schema currently loaded"))
            //////{
            //////    SetCommandVisibility(Constants.Constants.AddField, false);
            //////    SetCommandVisibility(Constants.Constants.AddTable, false);
            //////}
            //////else
            //////{
            //////    SetCommandVisibility(Constants.Constants.AddField, true);
            //////    SetCommandVisibility(Constants.Constants.AddTable, true);
            //////}
        }

        public void btnShowFields(Office.CommandBarButton vsoButton, ref bool cancelDefault)
        {
            MessageBox.Show("Fields here!");
        }

        public void btnShowTables(Office.CommandBarButton vsoButton, ref bool cancelDefault)
        {
            MessageBox.Show("Tables here!");
        }

        private void SetCommandVisibility(string name, bool visible)
        {
            this.Application.CustomizationContext = this.Application.ActiveDocument;
            Office.CommandBar commandBar = this.Application.CommandBars["Text"];
            commandBar.Controls[name].Visible = visible;
        }


        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
