﻿namespace Gotham.DocBuilder
{
    partial class TemplateBuilder : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public TemplateBuilder()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.tbTempalteBuilder = this.Factory.CreateRibbonTab();
            this.grpSchema = this.Factory.CreateRibbonGroup();
            this.btnCreateSchema = this.Factory.CreateRibbonButton();
            this.btnEditSchema = this.Factory.CreateRibbonButton();
            this.grpElements = this.Factory.CreateRibbonGroup();
            this.btnLoadSchema = this.Factory.CreateRibbonButton();
            this.btnOpenTemplate = this.Factory.CreateRibbonButton();
            this.btnUploadTemplate = this.Factory.CreateRibbonButton();
            this.btnAddField = this.Factory.CreateRibbonButton();
            this.btnAddTable = this.Factory.CreateRibbonButton();
            this.btnConfiguration = this.Factory.CreateRibbonButton();
            this.grpCurrentSettings = this.Factory.CreateRibbonGroup();
            this.lblCurrentSchema = this.Factory.CreateRibbonLabel();
            this.grpVersion = this.Factory.CreateRibbonGroup();
            this.lblVersion = this.Factory.CreateRibbonLabel();
            this.tab1.SuspendLayout();
            this.tbTempalteBuilder.SuspendLayout();
            this.grpSchema.SuspendLayout();
            this.grpElements.SuspendLayout();
            this.grpCurrentSettings.SuspendLayout();
            this.grpVersion.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.Groups.Add(this.group1);
            this.tab1.Label = "TabAddIns";
            this.tab1.Name = "tab1";
            // 
            // group1
            // 
            this.group1.Label = "group1";
            this.group1.Name = "group1";
            // 
            // tbTempalteBuilder
            // 
            this.tbTempalteBuilder.Groups.Add(this.grpSchema);
            this.tbTempalteBuilder.Groups.Add(this.grpElements);
            this.tbTempalteBuilder.Groups.Add(this.grpCurrentSettings);
            this.tbTempalteBuilder.Groups.Add(this.grpVersion);
            this.tbTempalteBuilder.KeyTip = "G";
            this.tbTempalteBuilder.Label = "Gotham Confirmation";
            this.tbTempalteBuilder.Name = "tbTempalteBuilder";
            // 
            // grpSchema
            // 
            this.grpSchema.Items.Add(this.btnCreateSchema);
            this.grpSchema.Items.Add(this.btnEditSchema);
            this.grpSchema.Label = "Schema Builder";
            this.grpSchema.Name = "grpSchema";
            // 
            // btnCreateSchema
            // 
            this.btnCreateSchema.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnCreateSchema.Image = global::Gotham.DocBuilder.Properties.Resources.CreateSchemaPlus;
            this.btnCreateSchema.Label = "Create Schema Plus";
            this.btnCreateSchema.Name = "btnCreateSchema";
            this.btnCreateSchema.ShowImage = true;
            this.btnCreateSchema.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnCreateSchema_Click);
            // 
            // btnEditSchema
            // 
            this.btnEditSchema.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnEditSchema.Image = global::Gotham.DocBuilder.Properties.Resources.edit_property1600;
            this.btnEditSchema.Label = "Edit Schema Plus";
            this.btnEditSchema.Name = "btnEditSchema";
            this.btnEditSchema.ShowImage = true;
            this.btnEditSchema.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnEditSchema_Click);
            // 
            // grpElements
            // 
            this.grpElements.Items.Add(this.btnLoadSchema);
            this.grpElements.Items.Add(this.btnOpenTemplate);
            this.grpElements.Items.Add(this.btnUploadTemplate);
            this.grpElements.Items.Add(this.btnAddField);
            this.grpElements.Items.Add(this.btnAddTable);
            this.grpElements.Items.Add(this.btnConfiguration);
            this.grpElements.Label = "Template Editor";
            this.grpElements.Name = "grpElements";
            // 
            // btnLoadSchema
            // 
            this.btnLoadSchema.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnLoadSchema.Image = global::Gotham.DocBuilder.Properties.Resources.LoadSchema02;
            this.btnLoadSchema.Label = "Load Schema";
            this.btnLoadSchema.Name = "btnLoadSchema";
            this.btnLoadSchema.ShowImage = true;
            this.btnLoadSchema.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnLoadSchema_Click);
            // 
            // btnOpenTemplate
            // 
            this.btnOpenTemplate.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnOpenTemplate.Image = global::Gotham.DocBuilder.Properties.Resources.OpenTemplate;
            this.btnOpenTemplate.Label = "Open Template";
            this.btnOpenTemplate.Name = "btnOpenTemplate";
            this.btnOpenTemplate.ShowImage = true;
            this.btnOpenTemplate.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnOpenTemplate_Click);
            // 
            // btnUploadTemplate
            // 
            this.btnUploadTemplate.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnUploadTemplate.Image = global::Gotham.DocBuilder.Properties.Resources.UploadTemplate;
            this.btnUploadTemplate.Label = "Upload Template";
            this.btnUploadTemplate.Name = "btnUploadTemplate";
            this.btnUploadTemplate.ShowImage = true;
            this.btnUploadTemplate.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnUploadTemplate_Click);
            // 
            // btnAddField
            // 
            this.btnAddField.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnAddField.Image = global::Gotham.DocBuilder.Properties.Resources.AddField;
            this.btnAddField.KeyTip = "TT";
            this.btnAddField.Label = "Add Field";
            this.btnAddField.Name = "btnAddField";
            this.btnAddField.ShowImage = true;
            this.btnAddField.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnAddField_Click);
            // 
            // btnAddTable
            // 
            this.btnAddTable.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnAddTable.Image = global::Gotham.DocBuilder.Properties.Resources.AddTable;
            this.btnAddTable.KeyTip = "T+T";
            this.btnAddTable.Label = "Add Table";
            this.btnAddTable.Name = "btnAddTable";
            this.btnAddTable.ShowImage = true;
            this.btnAddTable.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnAddTable_Click);
            // 
            // btnConfiguration
            // 
            this.btnConfiguration.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnConfiguration.Image = global::Gotham.DocBuilder.Properties.Resources.SchemasAndTemplates;
            this.btnConfiguration.Label = "Gotham Summary";
            this.btnConfiguration.Name = "btnConfiguration";
            this.btnConfiguration.ShowImage = true;
            this.btnConfiguration.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnConfiguration_Click);
            // 
            // grpCurrentSettings
            // 
            this.grpCurrentSettings.Items.Add(this.lblCurrentSchema);
            this.grpCurrentSettings.Label = "Current Settings";
            this.grpCurrentSettings.Name = "grpCurrentSettings";
            // 
            // lblCurrentSchema
            // 
            this.lblCurrentSchema.Label = "No schema currently loaded";
            this.lblCurrentSchema.Name = "lblCurrentSchema";
            // 
            // grpVersion
            // 
            this.grpVersion.Items.Add(this.lblVersion);
            this.grpVersion.Label = "Version";
            this.grpVersion.Name = "grpVersion";
            // 
            // lblVersion
            // 
            this.lblVersion.Label = "Version: Demo-016";
            this.lblVersion.Name = "lblVersion";
            // 
            // TemplateBuilder
            // 
            this.Name = "TemplateBuilder";
            this.RibbonType = "Microsoft.Word.Document";
            this.Tabs.Add(this.tab1);
            this.Tabs.Add(this.tbTempalteBuilder);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.TemplateBuilder_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.tbTempalteBuilder.ResumeLayout(false);
            this.tbTempalteBuilder.PerformLayout();
            this.grpSchema.ResumeLayout(false);
            this.grpSchema.PerformLayout();
            this.grpElements.ResumeLayout(false);
            this.grpElements.PerformLayout();
            this.grpCurrentSettings.ResumeLayout(false);
            this.grpCurrentSettings.PerformLayout();
            this.grpVersion.ResumeLayout(false);
            this.grpVersion.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonTab tbTempalteBuilder;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpElements;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnCreateSchema;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnAddField;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpSchema;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnEditSchema;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnLoadSchema;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnOpenTemplate;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnUploadTemplate;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpCurrentSettings;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel lblCurrentSchema;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnAddTable;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnConfiguration;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpVersion;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel lblVersion;
    }

    partial class ThisRibbonCollection
    {
        internal TemplateBuilder TemplateBuilder
        {
            get { return this.GetRibbon<TemplateBuilder>(); }
        }
    }
}
