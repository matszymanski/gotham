set local
set XSD_PATH="C:\Program Files (x86)\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.6 Tools"

@echo off

REM md output

REM %XSD_PATH%\xsd.exe EFET-CoreCmpts-V4R2.xsd /c /order /o:output /n:RosettaStone.EfetCoreCmptsV4R2

%XSD_PATH%\xsd.exe EFET-BCN-V4R2.xsd EFET-CoreCmpts-V4R2.xsd /c /order /o:output /n:RosettaStone.EfetBcnV4R2
%XSD_PATH%\xsd.exe EFET-CAN-V4R2.xsd EFET-CoreCmpts-V4R2.xsd /c /order /o:output /n:RosettaStone.EfetCanV4R2
%XSD_PATH%\xsd.exe EFET-CNF-V4R2.xsd EFET-CoreCmpts-V4R2.xsd /c /order /o:output /n:RosettaStone.EfetCnfV4R2
%XSD_PATH%\xsd.exe EFET-ConfirmationDescription-V4R2.xsd EFET-CoreCmpts-V4R2.xsd  /c /order /o:output /n:RosettaStone.EFETConfirmationDescriptionV4R2
%XSD_PATH%\xsd.exe EFET-CoreElements-V4R2.xsd  EFET-CoreCmpts-V4R2.xsd  /c /order /o:output /n:RosettaStone.EfetCoreElementsV4R2
%XSD_PATH%\xsd.exe EFET-GNF-V4R2.xsd EFET-CoreCmpts-V4R2.xsd  /c /order /o:output /n:RosettaStone.EfetGnfV4R2
%XSD_PATH%\xsd.exe EFET-SCN-V4R2.xsd EFET-CoreCmpts-V4R2.xsd  /c /order /o:output /n:RosettaStone.EfetScnV4R2
%XSD_PATH%\xsd.exe ERR-BoxResult-V3R0.xsd EFET-CoreCmpts-V4R2.xsd ERR-CoreCmpts-V3R0.xsd /c /order /o:output /n:ErrBoxResultV3R0
%XSD_PATH%\xsd.exe ERR-Collateral-V3R0.xsd EFET-CoreCmpts-V4R2.xsd ERR-CoreCmpts-V3R0.xsd /c /order /o:output /n:ErrCollateralV3R0
%XSD_PATH%\xsd.exe ERR-Confirmation-V3R0.xsd EFET-CoreCmpts-V4R2.xsd  ERR-CoreCmpts-V3R0.xsd /c /order /o:output /n:ErrConfirmationV3R0
%XSD_PATH%\xsd.exe ERR-CoreCmpts-V3R0.xsd EFET-CoreCmpts-V4R2.xsd ERR-CoreCmpts-V3R0.xsd /c /order /o:output /n:ErrCoreCmptsV3R0
%XSD_PATH%\xsd.exe ERR-CoreElements-V3R0.xsd EFET-CoreCmpts-V4R2.xsd ERR-CoreCmpts-V3R0.xsd /c /order /o:output /n:ErrCoreElementsV3R0

REM %XSD_PATH%\xsd.exe ERR-CpmlDocument-V3R0.xsd EFET-CoreCmpts-V4R2.xsd ERR-CoreElements-V3R0.xsd EFET-CNF-V4R2.xsd EFET-BCN-V4R2.xsd EFET-GNF-V4R2.xsd ERR-IRT-V3R0.xsd ERR-ETD-V3R0.xsd ERR-FXT-V3R0.xsd /c /order /o:output /n:ErrCpmlDocument-V3R0

%XSD_PATH%\xsd.exe ERR-ETD-V3R0.xsd EFET-CoreCmpts-V4R2.xsd   ERR-CoreCmpts-V3R0.xsd  /c /order /o:output /n:ErrETDV3R0
%XSD_PATH%\xsd.exe ERR-FXT-V3R0.xsd EFET-CoreCmpts-V4R2.xsd   ERR-CoreCmpts-V3R0.xsd  /c /order /o:output /n:ErrFXTV3R0
%XSD_PATH%\xsd.exe ERR-IRT-V3R0.xsd EFET-CoreCmpts-V4R2.xsd   ERR-CoreCmpts-V3R0.xsd  /c /order /o:output /n:ErrIRTV3R0

REM %XSD_PATH%\xsd.exe ERR-Schemas-V3R0.xsd EFET-CoreCmpts-V4R2.xsd /c /order /o:output /n:ErrSchemasV3R0

%XSD_PATH%\xsd.exe ERR-Valuation-V3R0.xsd EFET-CoreCmpts-V4R2.xsd   ERR-CoreCmpts-V3R0.xsd  /c /order /o:output /n:ERRValuationV3R0

endlocal

