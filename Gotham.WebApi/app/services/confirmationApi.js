﻿(function () {
    'use strict';

    angular
        .module('confirmationsApp')
        .factory('confirmationApi', confirmationApi);

    confirmationApi.$inject = ['$resource'];

    function confirmationApi($resource) {
        return {
            confirmations: $resource("/api/v1/confirmationview", {}, {
                list: { method: "GET", isArray: true }
            }),
            confirmationsBySchema: $resource("/api/v1/confirmationview/schema/:SchemaID", {
                SchemaID : '@SchemaID'
            }, {
                list: { method: "GET", isArray: true }
            }),
            confirmationsNew: $resource("/api/v1/confirmationview/new/:count", {
                count: '@count'
            }, {
                'new': { method: "GET", isArray: true }
            }),
            confirmation: $resource("/api/v1/confirmationview/query", {
            }, {
                query: { method: "POST", isArray: true }
            }),
            hospitalCount: $resource("/api/v1/confirmationview/hospital/count", {
            }, {
                get: { method: "GET", isArray: false }
                }),
            hospital: $resource("/api/v1/confirmationview/hospital", {
            }, {
                    get: { method: "GET", isArray: true }
            }),
            confirmationStatus: $resource("/api/v1/confirmationview/status/:statusId", {
                tradeId: '@tradeId',
                internalPartyId: '@internalPartyId',
                statusId: '@statusId'
            }, {
                set: { method: "PUT", isArray: false }
            }),
            audit: $resource("/api/v1/confirmationview/audit", {
                tradeId: '@tradeId',
                internalPartyId: '@internalPartyId'
            }, {
                get: { method: "POST", isArray: true }
            })
        };
    }
})();