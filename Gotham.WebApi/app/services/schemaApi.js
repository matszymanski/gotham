﻿(function () {
    'use strict';

    angular
        .module('confirmationsApp')
        .factory('schemaApi', schemaApi);

    schemaApi.$inject = ['$resource'];

    function schemaApi($resource) {
        return {
            schemas: $resource("/api/v1/schema", {
            }, {
                list: { method: "GET", isArray: true}
            })
        };
    }
})();