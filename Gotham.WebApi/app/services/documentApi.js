﻿(function () {
    'use strict';

    angular
        .module('confirmationsApp')
        .factory('documentApi', documentApi);

    documentApi.$inject = ['$resource'];

    function documentApi($resource) {
        return {
            generatedDocumentInfos: $resource("/api/v1/document/generated/info", {
            }, {
                list: { method: "POST", isArray: true}
            }),
            uploadedDocumentInfos: $resource("/api/v1/document/uploaded/info", {
            }, {
                list: { method: "POST", isArray: true }
            }),
            generated: $resource("/api/v1/document/generated/:messageId", {
                messageId: "@messageId"
            }, {
                download: {
                    method: "GET", responseType: 'arraybuffer',
                    interceptor: {
                        response: function (response) {
                            response.resource.$httpHeaders = response.headers;
                            return response.resource;
                        }
                    }
                }
            }),
            uploaded: $resource("/api/v1/document/uploaded/:id", {
                id: "@id"
            }, {
                download: {
                    method: "GET", responseType: 'arraybuffer',
                    interceptor: {
                        response: function (response) {
                            response.resource.$httpHeaders = response.headers;
                            return response.resource;
                        }
                    }
                }
            })
        };
    }
})();