﻿(function () {
    'use strict';

    angular
        .module('confirmationsApp')
        .factory('configurationApi', configurationApi);

    configurationApi.$inject = ['$resource'];

    function configurationApi($resource) {
        return {
            priceIndexes: $resource("/api/v1/priceindexes", {}, {
                list: { method: "GET", isArray: true },
                save: { method: "PUT" }
            }),
            priceIndexesUnaligned: $resource("/api/v1/priceindexes/unaligned/:count", {
                count: "@count"
            }, {
                list: { method: "GET", isArray: true }
            }),
            //priceIndex: $resource("/api/v1/priceindexes/:id", {
            //    id: "@id"
            //}, {
            //    save: { method: "PUT" }
            //}),
            parties: $resource("/api/v1/parties", {}, {
                list: { method: "GET", isArray: true },
                upsert: { method: "POST" }
            }),
            party: $resource("/api/v1/parties/:partyId", {
                partyId: "@partyId"
            }, {
                get: { method: "GET", isArray: false }
            }),
            partiesUnaligned: $resource("/api/v1/parties/unaligned/:count", {
                count: "@count"
            }, {
                list: { method: "GET", isArray: true }
            }),
            translation: $resource("/api/v1/translation", {}, {
                list: { method: "GET", isArray: true },
                set: { method: "POST"}
            })
        };
    }
})();