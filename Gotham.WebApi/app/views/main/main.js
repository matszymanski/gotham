﻿(function () {
    'use strict';

    var app = angular.module('confirmationsApp');
    app.controller('mainViewCtrl', mainViewCtrl);

    mainViewCtrl.$inject = ['$location', 'configurationApi', 'confirmationApi'];

    function mainViewCtrl($location, configurationApi, confirmationApi) {
        var vm = this;
        vm.priceIndexesUnaligned = [];
        vm.newEntries = [];

        activate();

        function activate() {
            vm.loadingUnalignedPriceIndexes = true;
            vm.loadingNewEntries = true;
            configurationApi.priceIndexesUnaligned.list({ count: 5 }).$promise.then(function (result) {
                angular.copy(result, vm.priceIndexesUnaligned);
                vm.loadingUnalignedPriceIndexes = false;
            },
            function (error) {
                console.error(error);
            });

            confirmationApi.confirmationsNew.new({ count: 10 }).$promise.then(function (result) {
                angular.copy(result, vm.newEntries);
                vm.loadingNewEntries = false;
            },
            function (error) {
                console.error(error);
            })
        }
    }


    app.component('mainView', {
        templateUrl: 'app/views/main/main.html',
        controller: mainViewCtrl
    });
})();
