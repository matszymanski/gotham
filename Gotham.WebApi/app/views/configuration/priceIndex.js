﻿(function () {
    'use strict';

    var app = angular.module('confirmationsApp');
    app.controller('configurationPriceIndexViewCtrl', configurationPriceIndexViewCtrl);

    configurationPriceIndexViewCtrl.$inject = ['$location', '$stateParams', 'configurationApi', 'NgTableParams'];

    function configurationPriceIndexViewCtrl($location, $stateParams, configurationApi, NgTableParams) {
        /* jshint validthis:true */
        var vm = this;
        vm.applying = false;
        vm.priceIndexes = [];
        vm.selectedPriceIndex = {};
        vm.userPriceIndexInput = "";
        vm.showWarningUserPriceIndexInput = false;
        vm.showWarningSelectedPriceIndex = false;
        vm.showApplyError = false;
        vm.showApplySuccess = false;

        activate();

        function activate() {
            loadIndexes();

            if ($stateParams.userPriceIndexID !== undefined) {
                vm.userPriceIndexInput = $stateParams.userPriceIndexID
            }
        }

        function loadIndexes()
        {
            vm.loading = true;
            configurationApi.priceIndexes.list().$promise.then(
                function (response) {
                    vm.priceIndexes = response;
                    vm.loading = false;
                },
                function (error) {
                    console.log("configurationApi request failed: " + error);
                }
            );
        }

        vm.priceIndexRadioClick = function()
        {
            vm.showWarningSelectedPriceIndex = false;
        }

        vm.userPriceIndexInputKeyup = function()
        {
            if(vm.userPriceIndexInput.length < 1)
                vm.showWarningUserPriceIndexInput = true;
            else
                vm.showWarningUserPriceIndexInput = false;
        }

        vm.applyMappingClick = function()
        {
            vm.showApplyError = false;
            vm.showApplySuccess = false;
            var error = false;
            if (vm.userPriceIndexInput.length < 1)
            {
                vm.showWarningUserPriceIndexInput = true;
                error = true;
            }
            if (angular.equals(vm.selectedPriceIndex, {})) {
                vm.showWarningSelectedPriceIndex = true;
                error = true;
            }
            if (!error) {
                applyMapping();
            }
        }

        function applyMapping()
        {
            vm.applying = true;
            configurationApi.priceIndexes.save({}, { id: vm.selectedPriceIndex, userPriceIndexIDs: [vm.userPriceIndexInput] })
            .$promise.then(function (result) {
                vm.updatedIndexId = vm.selectedPriceIndex;
                vm.updatedIndexCode = document.getElementById("code" + vm.selectedPriceIndex).innerText;
                vm.updatedUserIndexId = vm.userPriceIndexInput;
                vm.selectedPriceIndex = {};
                vm.userPriceIndexInput = "";
                vm.showApplySuccess = true;
                vm.applying = false;
                vm.priceIndexes = [];
                loadIndexes();
            },
                function (error) {
                    vm.showApplyError = true;
                    console.error(error);
                    vm.applying = false;
                });
        }
    };

    app.component('configurationPriceIndexView', {
        templateUrl: 'app/views/configuration/priceIndex.html',
        controller: configurationPriceIndexViewCtrl
    });

    app.filter('findPIdxBy', function () {
        return function (data, userInput) {
            if (!userInput || userInput === 'undefined') {
                return data;
            }
            var results = [];
            var keywordArray = userInput.split(" ");
            angular.forEach(data, function (element) {
                var matchesAll = true;
                angular.forEach(keywordArray, function (phrase) {
                    if (element.Description.toLowerCase().indexOf(phrase.toLowerCase()) == -1
                        && element.Code.toLowerCase().indexOf(phrase.toLowerCase()) == -1) {
                        matchesAll = false;
                        return;
                    }
                });
                if (matchesAll)
                    results.push(element);
            });
            return results;
        };
    });
})();
