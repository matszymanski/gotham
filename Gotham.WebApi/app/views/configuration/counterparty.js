﻿(function () {
    'use strict';

    var app = angular.module('confirmationsApp');
    app.controller('configurationCounterpartyViewCtrl', configurationCounterpartyViewCtrl);

    configurationCounterpartyViewCtrl.$inject = ['$location', 'NgTableParams', 'configurationApi', '$state', '$stateParams', '$scope'];

    function configurationCounterpartyViewCtrl($location, NgTableParams, configurationApi, $state, $stateParams, $scope) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'configurationCounterpartyViewCtrl';
        vm.party = {};
        vm.contacts = [];
        vm.contact = {};
        vm.isUpdate = false;
        vm.partyID = '';
        vm.tableParams = new NgTableParams({
        }, {
            filterDelay: 0,
            dataset: vm.contacts
        });

        //$scope.$watch('streetTextarea', function (newValue, oldValue) {
        //    vm.party.ContactInfo.StreetAddressLines = newValue.split('\n');
        //});

        activate();

        function activate() {
            if ($stateParams.new === true) {
                if ($stateParams.partyID !== undefined) {
                    vm.isUpdate = true;
                    vm.partyID = $stateParams.partyID;
                    vm.party.PartyID = vm.partyID;
                }
            }
            else {
                //!-- party update
                if ($stateParams.partyID !== undefined) {
                    vm.isUpdate = true;
                    vm.partyID = $stateParams.partyID;
                    configurationApi.party.get({ partyId: vm.partyID })
                        .$promise.then(function (response) {
                            vm.party = response;
                            console.log(vm.party);
                            //vm.streetTextarea = vm.party.ContactInfo.Address.StreetAddressLines.join("\n");
                        },
                        function (error) {
                            console.log("Error retrieving party for id: " + vm.partyID);
                            console.error(error);
                        });
                }
            }
        }

        vm.apply = function () {
            console.log(vm.party);
            //if (vm.streetTextarea !== undefined) {
            //    _.extend(vm.party, { ContactInfo: { Address: { StreetAddressLines: vm.streetTextarea.split('\n') } } });
            //}

            
            configurationApi.parties.upsert({ }, vm.party).$promise.then(function (result) {
                $state.go('configurationCounterpartiesView');
            }, function (error) {
                console.log("Error updating party for id: " + vm.party.PartyID);
                console.error(error);
            });
            
        }


        // for contacts edit

        //vm.addContact = function () {
        //    var isFilled = false;
        //    _.each(_.values(vm.contact), function (element, index, list) {
        //        if (element.length > 0) {
        //            isFilled = true;
        //        }
        //    });
        //    if (_.isEmpty(vm.contact) || !isFilled) {
        //        $('#nameadd').focus();
        //    }
        //    else {
        //        var contactCopy = angular.copy(vm.contact);
        //        vm.party.Contacts.push(contactCopy);
        //        vm.contact = {};
        //    }
        //}


        ////contacts table 
        //vm.cancel = function (contact, contactForm) {
        //    vm.reset(contact, contactForm);
        //    var original = _.findWhere(vm.contacts, contact);
        //    angular.extend(contact, original);
        //}

        //vm.del = function (contact) {
        //    console.log(contact);
        //    vm.contacts = _.without(vm.contacts, contact);
        //    vm.tableParams.reload();
        //}

        //vm.reset = function (contact, contactForm) {
        //    contact.isEditing = false;
        //    contactForm.$setPristine();
        //}

        //vm.save = function (contact, contactForm) {
        //    vm.reset(contact, contactForm);
        //    var original = _.findWhere(vm.contacts, contact);
        //    angular.extend(original, contact);
        //}
    }

    app.component('configurationCounterpartyView', {
        templateUrl: 'app/views/configuration/counterparty.html',
        controller: configurationCounterpartyViewCtrl
    });
})();
