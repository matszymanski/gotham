﻿(function () {
    'use strict';

    var app = angular.module('confirmationsApp');
    app.controller('configurationTranslatorViewCtrl', configurationTranslatorViewCtrl);

    configurationTranslatorViewCtrl.$inject = ['$location', '$stateParams', 'configurationApi', 'NgTableParams'];

    function configurationTranslatorViewCtrl($location, $stateParams, configurationApi, NgTableParams) {
        /* jshint validthis:true */
        var vm = this;
        vm.applying = false;
        vm.translations = [];
        vm.phraseInput = "";
        vm.translationInput = "";
        vm.showWarningPhraseInput = false;
        vm.showWarningTranslationInput = false;
        vm.showApplyError = false;
        vm.showApplySuccess = false;

        activate();

        function activate() {
            loadTranslations();
        }

        function loadTranslations()
        {
            vm.loading = true;
            configurationApi.translation.list().$promise.then(
                function (response) {
                    vm.translations = response;
                    vm.loading = false;
                },
                function (error) {
                    console.log("configurationApi request failed");
                    console.error(error);
                }
            );
        }

        vm.priceIndexRadioClick = function()
        {
            vm.showWarningTranslationInput = false;
        }

        vm.phraseInputKeyup = function()
        {
            if(vm.phraseInput.length < 1)
                vm.showWarningPhraseInput = true;
            else
                vm.showWarningPhraseInput = false;
        }

        vm.translationInputKeyup = function () {
            if (vm.phraseInput.length < 1)
                vm.showWarningTranslationInput = true;
            else
                vm.showWarningTranslationInput = false;
        }

        vm.applyMappingClick = function()
        {
            vm.showApplyError = false;
            vm.showApplySuccess = false;
            var error = false;
            if (vm.phraseInput.length < 1)
            {
                vm.showWarningPhraseInput = true;
                error = true;
            }
            if (vm.translationInput.length < 1) {
                vm.showWarningTranslationInput = true;
                error = true;
            }
            if (!error) {
                applyMapping();
            }
        }

        function applyMapping()
        {
            vm.applying = true;
            configurationApi.translation.set({}, { phrase: vm.phraseInput, translation: vm.translationInput })
            .$promise.then(function (result) {
                vm.updatedPhrase = vm.phraseInput;
                vm.updatedTranslation = vm.translationInput;
                vm.phraseInput = "";
                vm.translationInput = "";
                vm.showApplySuccess = true;
                vm.applying = false;
                vm.translations = [];
                loadTranslations();
            },
                function (error) {
                    vm.showApplyError = true;
                    console.error(error);
                    vm.applying = false;
                });
        }
    };

    app.component('configurationTranslatorView', {
        templateUrl: 'app/views/configuration/translator.html',
        controller: configurationTranslatorViewCtrl
    });

    app.filter('findBy', function () {
        return function (data, userInput) {
            if (!userInput || userInput === 'undefined') {
                return data;
            }
            var results = [];
            var keywordArray = userInput.split(" ");
            angular.forEach(data, function (element) {
                var matchesAll = true;
                angular.forEach(keywordArray, function (phrase) {
                    if (element.Translation.toLowerCase().indexOf(phrase.toLowerCase()) == -1
                        && element.Phrase.toLowerCase().indexOf(phrase.toLowerCase()) == -1) {
                        matchesAll = false;
                        return;
                    }
                });
                if (matchesAll)
                    results.push(element);
            });
            return results;
        };
    });
})();
