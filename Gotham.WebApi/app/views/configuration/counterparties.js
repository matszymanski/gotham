﻿(function () {
    'use strict';

    var app = angular.module('confirmationsApp');
    app.controller('configurationCounterpartiesViewCtrl', configurationCounterpartiesViewCtrl);

    configurationCounterpartiesViewCtrl.$inject = ['$location', 'configurationApi', 'NgTableParams', '$scope'];

    function configurationCounterpartiesViewCtrl($location, configurationApi, NgTableParams, $scope) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'counterpartiesViewCtrl';
        vm.parties = [];
        vm.displayParties = [];
      
        vm.tableParams = new NgTableParams({
            count: 4,
            sorting: { PartyId: "asc" }
        }, {
            counts: [],
            dataset: vm.parties
        });
        $scope.predicates = ['PartyID'];

        activate();

        function activate() {
           
            vm.loading = true;
            configurationApi.parties.list().$promise.then(
                function (response) {
                    //vm.parties = response;
                    vm.loading = false;
                    Array.prototype.push.apply(vm.parties, response);
                    Array.prototype.push.apply(vm.displayParties, response);
                    //vm.tableParams.dataset = vm.parties;
                    vm.tableParams.reload();
                    _.each(document.getElementsByName("PartyID"), function (element) { element.placeholder = "Search" });
                },
                function (error) {
                    console.log("configurationApi request failed");
                    console.error(error);
                });
        }

    }

    app.component('configurationCounterpartiesView', {
        templateUrl: 'app/views/configuration/counterparties.html',
        controller: configurationCounterpartiesViewCtrl
    });

})();

