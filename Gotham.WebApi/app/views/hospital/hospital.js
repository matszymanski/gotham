﻿(function () {
    'use strict';

    var app = angular.module('confirmationsApp');
    app.controller('hospitalViewCtrl', hospitalViewCtrl);

    hospitalViewCtrl.$inject = ['$location', 'configurationApi', 'confirmationApi'];

    function hospitalViewCtrl($location, configurationApi, confirmationApi) {
        /* jshint validthis:true */
        var vm = this;
        vm.priceIndexesUnaligned = [];
        vm.partiesUnaligned = [];
        vm.notValid = [];
        activate();

        function activate() {
            vm.loadingNotValid = true;
            confirmationApi.hospital.get().$promise.then(function (result) {
                angular.copy(result, vm.notValid);
                vm.loadingNotValid = false;
            },
            function (error) {
                console.error(error);
            });

            vm.loadingUnalignedPriceIndexes = true;
            configurationApi.priceIndexesUnaligned.list({ count: 30 }).$promise.then(function (result) {
                angular.copy(result, vm.priceIndexesUnaligned);
                vm.loadingUnalignedPriceIndexes = false;
            },
            function (error) {
                console.error(error);
            });

            vm.loadingUnalignedParties = true;
            configurationApi.partiesUnaligned.list({ count: 30 }).$promise.then(function (result) {
                angular.copy(result, vm.partiesUnaligned);
                vm.loadingUnalignedParties = false;
            },
            function (error) {
                console.error(error);
            });
        }
    }

    app.component('hospitalView', {
        templateUrl: 'app/views/hospital/hospital.html',
        controller: hospitalViewCtrl
    });
})();
