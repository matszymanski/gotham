﻿(function () {
    'use strict';

    var app = angular.module('confirmationsApp');
    app.controller('confirmationDetailsViewCtrl', confirmationDetailsViewCtrl);
    confirmationDetailsViewCtrl.$inject = ['$scope', 'confirmationApi', 'documentApi', '$state', '$stateParams', 'Upload'];

    function confirmationDetailsViewCtrl($scope, confirmationApi, documentApi, $state, $stateParams, Upload) {
        var vm = this;
        vm.tradeId = "";
        vm.internalPartyId = "";
        vm.userId = -1;
        vm.confirmation = {};
        vm.fields = [];
        vm.audits = [];
        vm.generatedDocumentInfos = [];
        vm.uploadedDocumentInfos = [];
        

        activate();

        function activate() {
            if ($stateParams.tradeId !== undefined && $stateParams.internalPartyId !== undefined) {
                
                vm.tradeId = $stateParams.tradeId;
                vm.internalPartyId = $stateParams.internalPartyId;

                documentApi.generatedDocumentInfos.list({}, { TradeID: vm.tradeId, InternalPartyID: vm.internalPartyId })
                    .$promise.then(function (response) {
                        vm.generatedDocumentInfos = response;
                        console.log(vm.generatedDocumentInfos);
                    },
                    function (error) {
                        console.log("Error retrieving document infos for trade id: " + vm.tradeId + " and internal party id: " + vm.internalPartyId);
                        console.error(error);
                    });

                documentApi.uploadedDocumentInfos.list({}, { TradeID: vm.tradeId, InternalPartyID: vm.internalPartyId })
                    .$promise.then(function (response) {
                        vm.uploadedDocumentInfos = response;
                        console.log(vm.uploadedDocumentInfos);
                    },
                    function (error) {
                        console.log("Error retrieving uploaded document infos for trade id: " + vm.tradeId + " and internal party id: " + vm.internalPartyId);
                        console.error(error);
                    });

                confirmationApi.confirmation.query({}, { TradeID: vm.tradeId, InternalPartyID: vm.internalPartyId })
                    .$promise.then(function (response) {
                        vm.confirmation = response[0];
                        console.log(vm.confirmation);
                    },
                    function (error) {
                        console.log("Error retrieving trade for trade id: " + vm.tradeId + " and internal party id: " + vm.internalPartyId);
                        console.error(error);
                    });

                confirmationApi.audit.get({ tradeId: vm.tradeId, internalPartyId: vm.internalPartyId })
                    .$promise.then(function (response) {
                        vm.audits = response;
                        console.log(vm.audits);
                    },
                    function (error) {
                        console.log("Error retrieving audit for trade id: " + vm.tradeId + " and internal party id: " + vm.internalPartyId);
                        console.error(error);
                    });

                
            }
        };

        vm.downloadDocument = function (id) {
            
            
            window.open("/api/v1/confirmationview/documents/"+id, '_self', '');
            
            //documentApi.document.download({ "id": id }).$promise.then(function (data) {
            //    var headers = data.$httpHeaders();
            //    var filename = headers['filename'] || 'download.docx';

            //    //var url = getDownloadUrl(data, headers);
            //    var linkElement = document.createElement('a');
            //    linkElement.setAttribute('href', url);
            //    linkElement.setAttribute("download", filename);

            //    var clickEvent = new MouseEvent("click", {
            //        "view": window,
            //        "bubbles": true,
            //        "cancelable": false
            //    });

                
            //    //linkElement.dispatchEvent(clickEvent);
            //    //window.location = url;

            //}, function (error) {
            //    console.log("Error retrieving document for id: " + id);
            //    console.error(error);
            //});
        };

        //function getDownloadUrl(data) {
            
            
        //    var blob = new Blob([data], { type: 'application/octet-stream' });
        //    var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;
        //    saveAs(blob, 'test.xxx');
        //    return urlCreator.createObjectURL(blob);
            
        //};

        // upload on file select or drop
        vm.upload = function (file) {
            Upload.upload({
                url: '/api/v1/document/upload',
                data: {
                    file: file, info: Upload.json(
                        {
                            TradeID: vm.tradeId, InternalPartyID: vm.internalPartyId, UserID : vm.userId
                        })
                }
            }).then(function (resp) {
                console.log('Success ' + resp.config.data.file.name + ' uploaded. Response: ' + resp.data);
            }, function (resp) {
                console.log('Error status: ' + resp.status);
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
        };

        // for multiple files:
        vm.uploadFiles = function (files) {
          if (files && files.length) {
              for (var i = 0; i < files.length; i++) {
                  vm.upload(files[i]);
            }
          }
        }
    };

    app.component('confirmationDetailsView', {
        templateUrl: 'app/views/confirmations/details.html',
        controller: confirmationDetailsViewCtrl
    });
}());



 

 
//if (!success) {
//    // Fallback to window.open method
//    $log.info("No methods worked for saving the arraybuffer, using last resort window.open");
//    window.open(httpPath, '_blank', '');
//}
//return filename;
//};