﻿(function () {
    'use strict';

    var app = angular.module('confirmationsApp');
    app.controller('confirmationsViewCtrl', confirmationsViewCtrl);
    confirmationsViewCtrl.$inject = ['$scope', '$state', 'NgTableParams', 'confirmationApi', '$stateParams'];

    function confirmationsViewCtrl($scope, $state, NgTableParams, confirmationApi, $stateParams) {
        var vm = this;
        vm.loading = false;
        vm.confirmations = {};
        vm.displayConfirmations = {};
        vm.columns = [];
        vm.itemsByPage = 20;

        vm.menuOptions = function (confirmation) {
            if (getStatusContextOptions(confirmation).length > 0) {
                return [
                    ['Details', function ($itemScope, $event, modelValue, text, $li) {
                        goToDetails($itemScope.confirmation['Trade ID'], $itemScope.confirmation['Internal party ID']);
                    }
                    ],
                    null,
                      ['Status', null,

                        getStatusContextOptions(confirmation)
                      ]

                ];
            }
            else
            {
                return [
                    ['Details', function ($itemScope, $event, modelValue, text, $li) {
                        goToDetails($itemScope.confirmation['Trade ID'], $itemScope.confirmation['Internal party ID']);
                    }
                    ]
                ];
            }
        };

        activate();

        function reload() {
            var current = $state.current;
            var params = angular.copy($stateParams);
            return $state.transitionTo(current, params, { reload: true, inherit: true, notify: true });
        }

        function activate() {

            setItemsByPage();
            //vm.displayTrades = confirmationApi.confirmations.list();
            vm.loading = true;
            var confirmationsPromise;
            if ($stateParams.schemaId !== undefined) {
                vm.schemaId = $stateParams.schemaId;
                confirmationsPromise = confirmationApi.confirmationsBySchema.list({ SchemaID: vm.schemaId }).$promise;
            }
            else
            {
                confirmationsPromise = confirmationApi.confirmations.list().$promise;
            }
            
            confirmationsPromise.then(
                function (response) {
                    vm.loading = false;
                    vm.confirmations = angular.copy(response);
                    vm.displayConfirmations = angular.copy(response);
                    if (vm.confirmations.length > 0) {
                        vm.columns = _.keys(vm.confirmations[0]);
                    }
                    //Array.prototype.push.apply(vm.trades, response);
                    //Array.prototype.push.apply(vm.displayTrades, response);
                },
                function (error) {
                    console.log("configurationApi request failed");
                    console.error(error);
                });
        };

        function activateBySchemaId()
        {

        }

        function goToDetails(tradeId, internalPartyId) {
            $state.go('confirmationDetailsView', { "tradeId": tradeId, "internalPartyId": internalPartyId });
        }

        function getStatusContextOptions(confirmation) {
            var contextOptions = [];
            for (var i = 0; i < confirmation.NextStatuses.length; i++)
            {
                var option = {
                    text: confirmation.NextStatuses[i],
                    click: function ($itemScope, $event, modelValue, text, $li) {
                        $scope.statusSelect($itemScope, $li[0].innerText);
                    }
                };
                contextOptions.push(option);
            }
            return contextOptions;
        };

        function setItemsByPage() {
            var height = window.innerHeight
                || document.documentElement.clientHeight
                || document.body.clientHeight;

            vm.itemsByPage = Math.max(0, (height - 200) / 32);
        }

        //window.onresize = function (event) {
        //    setItemsByPage();
        //    $scope.$apply();
        //};

        $scope.statusSelect = function($itemScope, text) {
            var tradeId = $itemScope.confirmation['Trade ID'];
            var internalPartyId = $itemScope.confirmation['Internal party ID'];
            var row = $("#" + tradeId + "_" + internalPartyId);
            row.find('#updateSpinner').removeClass('invisible');
            
            console.log(text);
            confirmationApi.confirmationStatus.set({ tradeId: tradeId, internalPartyId: internalPartyId, statusId: text })
                .$promise.then(function (response) {
                    console.log(response);
                    $itemScope.confirmation.Status = response.Status;
                    $itemScope.confirmation.NextStatuses = response.NextStatuses;
                    row.find('#updateSpinner').addClass('invisible');
                    
                },
                function(error){
                    console.error(error);
                });
            
        }

    };

    app.component('confirmationsView', {
        templateUrl: 'app/views/confirmations/confirmations.html',
        controller: confirmationsViewCtrl
    });

}());

//function statusSelect ($itemScope, statusName) {
//    var row = document.getElementById('trade_row_' + $itemScope.trade.Id);
//    jQuery('#trade_status_spinner_' + +$itemScope.trade.Id).removeClass('invisible');
//    console.log($itemScope);
//    $itemScope.confirmationApi.tryChangeStatus()
//    //alert(statusName + ' ' + $itemScope.trade.portfolio);
//}