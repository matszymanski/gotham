﻿(function () {
    'use strict';

    var app = angular.module('confirmationsApp', [
        'ngResource',
        'ui.router',
        'ngTable',
        'ui.bootstrap.contextMenu',
        'ui.bootstrap',
        'smart-table',
        'ngFileUpload'
    ]);


    app.config(config);

    function config($stateProvider, $locationProvider) {
        $stateProvider.state({
            name: 'mainView',
            cache: false,
            url: '',
            template: '<main-view></main-view>'
        });
        $stateProvider.state({
            name: 'confirmationsView',
            cache: false,
            url: '/confirmations',
            template: '<confirmations-view></confirmations-view>'
        });
        $stateProvider.state({
            name: 'confirmationsView.schema',
            cache: false,
            url: '/schema/:schemaId',
            template: '<confirmations-view></confirmations-view>'
        });
        $stateProvider.state({
            name: 'confirmationDetailsView',
            cache: false,
            url: '/confirmation/:tradeId/:internalPartyId',
            template: '<confirmation-details-view></confirmation-details-view>'
        });

        $stateProvider.state({
            name: 'configurationPriceIndexView',
            cache: false,
            url: '/configuration/priceIndex',
            template: '<configuration-price-index-view></configuration-price-index-view>'
        });
        $stateProvider.state({
            name: 'configurationPriceIndexView.edit',
            cache: false,
            url: '/:userPriceIndexID',
            template: '<configuration-price-index-view></configuration-price-index-view>'
        });
        //$stateProvider.state({
        //    name: 'configurationPriceIndexView',
        //    url: '/configuration',
        //    template: '<configuration-price-index-view></configuration-price-index-view>'
        //});

        $stateProvider.state({
            name: 'configurationCounterpartyView',
            cache: false,
            url: '/configuration/counterparty',
            template: '<configuration-counterparty-view></configuration-counterparty-view>'
        });

        $stateProvider.state({
            name: 'configurationCounterpartiesView',
            cache: false,
            url: '/configuration/counterparties',
            template: '<configuration-counterparties-view></configuration-counterparties-view>'
        });

        $stateProvider.state({
            name: 'configurationCounterpartyView.edit',
            cache: false,
            url: '/:partyID',
            params: { "new": false },
            template: '<configuration-counterparties-view></configuration-counterparties-view>'
        });
        $stateProvider.state({
            name: 'configurationCounterpartyView.new',
            cache: false,
            url: '/new/:partyID',
            params: {"new":true},
            template: '<configuration-counterparties-view></configuration-counterparties-view>'
        });
        $stateProvider.state({
            name: 'configurationTranslatorView',
            cache: false,
            url: '/configuration/translator',
            template: '<configuration-translator-view></configuration-translator-view>'
        });

        $stateProvider.state({
            name: 'hospitalView',
            cache: false,
            url: '/hospital',
            template: '<hospital-view></hospital-view>'
        });
        //$locationProvider.html5Mode(true);
    };
})();