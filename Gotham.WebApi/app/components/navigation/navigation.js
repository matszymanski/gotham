﻿(function () {
    'use strict';
    
    
    var app = angular.module('confirmationsApp');
    app.controller('navigationCtrl', navigationCtrl);
    navigationCtrl.$inject = ['$scope', 'confirmationApi', 'schemaApi'];

    function navigationCtrl($scope, confirmationApi, schemaApi) {
        var vm = this;
        vm.hospitalCount = 0;
        vm.schemas = [];
        activate();

        function activate() {
            schemaApi.schemas.list().$promise.then(function (result) {
                angular.copy(result, vm.schemas);
            },
            function (error) {
                console.error(error);
            });

            confirmationApi.hospitalCount.get().$promise.then(function (result) {
                vm.hospitalCount = result.Count;
            },
            function (error) {
                console.error(error);
            });
        };
    }

    app.component('navigation', {
        templateUrl: 'app/components/navigation/navigation.html',
        controller: navigationCtrl
    });

}());