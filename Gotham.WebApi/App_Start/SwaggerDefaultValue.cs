﻿using Swashbuckle.Swagger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Description;

namespace Gotham.WebApi
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    class SwaggerDefaultValueAttribute : Attribute
    {
        public SwaggerDefaultValueAttribute(string parameterName, object value)
        {
            this.ParameterName = parameterName;
            this.Value = value;
        }

        public string ParameterName { get; private set; }

        public object Value { get; set; }
    }

    class AddDefaultValues : IOperationFilter
    {
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            if (operation.parameters == null)
                return;

            var actionParams = apiDescription.ActionDescriptor.GetParameters();
            var customAttributes = apiDescription.ActionDescriptor.GetCustomAttributes<SwaggerDefaultValueAttribute>();
            foreach (var param in operation.parameters)
            {
                var actionParam = actionParams.FirstOrDefault(p => p.ParameterName == param.name);
                if (actionParam != null)
                {
                    if (actionParam.DefaultValue != null)
                    {
                        param.@default = actionParam.DefaultValue;
                    }
                    else
                    {
                        var customAttribute = customAttributes.FirstOrDefault(p => p.ParameterName == param.name);
                        if (customAttribute != null)
                        {
                            param.@default = customAttribute.Value;
                        }
                    }
                }
            }
        }
    }
}