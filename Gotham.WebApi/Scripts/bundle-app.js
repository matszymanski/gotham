(function () {
    'use strict';

    var app = angular.module('confirmationsApp', [
        'ngResource',
        'ui.router',
        'ngTable',
        'ui.bootstrap.contextMenu',
        'ui.bootstrap',
        'smart-table',
        'ngFileUpload'
    ]);


    app.config(config);

    function config($stateProvider, $locationProvider) {
        $stateProvider.state({
            name: 'mainView',
            cache: false,
            url: '',
            template: '<main-view></main-view>'
        });
        $stateProvider.state({
            name: 'confirmationsView',
            cache: false,
            url: '/confirmations',
            template: '<confirmations-view></confirmations-view>'
        });
        $stateProvider.state({
            name: 'confirmationsView.schema',
            cache: false,
            url: '/schema/:schemaId',
            template: '<confirmations-view></confirmations-view>'
        });
        $stateProvider.state({
            name: 'confirmationDetailsView',
            cache: false,
            url: '/confirmation/:tradeId/:internalPartyId',
            template: '<confirmation-details-view></confirmation-details-view>'
        });

        $stateProvider.state({
            name: 'configurationPriceIndexView',
            cache: false,
            url: '/configuration/priceIndex',
            template: '<configuration-price-index-view></configuration-price-index-view>'
        });
        $stateProvider.state({
            name: 'configurationPriceIndexView.edit',
            cache: false,
            url: '/:userPriceIndexID',
            template: '<configuration-price-index-view></configuration-price-index-view>'
        });
        //$stateProvider.state({
        //    name: 'configurationPriceIndexView',
        //    url: '/configuration',
        //    template: '<configuration-price-index-view></configuration-price-index-view>'
        //});

        $stateProvider.state({
            name: 'configurationCounterpartyView',
            cache: false,
            url: '/configuration/counterparty',
            template: '<configuration-counterparty-view></configuration-counterparty-view>'
        });

        $stateProvider.state({
            name: 'configurationCounterpartiesView',
            cache: false,
            url: '/configuration/counterparties',
            template: '<configuration-counterparties-view></configuration-counterparties-view>'
        });

        $stateProvider.state({
            name: 'configurationCounterpartyView.edit',
            cache: false,
            url: '/:partyID',
            params: { "new": false },
            template: '<configuration-counterparties-view></configuration-counterparties-view>'
        });
        $stateProvider.state({
            name: 'configurationCounterpartyView.new',
            cache: false,
            url: '/new/:partyID',
            params: {"new":true},
            template: '<configuration-counterparties-view></configuration-counterparties-view>'
        });
        $stateProvider.state({
            name: 'configurationTranslatorView',
            cache: false,
            url: '/configuration/translator',
            template: '<configuration-translator-view></configuration-translator-view>'
        });

        $stateProvider.state({
            name: 'hospitalView',
            cache: false,
            url: '/hospital',
            template: '<hospital-view></hospital-view>'
        });
        //$locationProvider.html5Mode(true);
    };
})();
(function () {
    'use strict';
    
    
    var app = angular.module('confirmationsApp');
    app.controller('navigationCtrl', navigationCtrl);
    navigationCtrl.$inject = ['$scope', 'confirmationApi', 'schemaApi'];

    function navigationCtrl($scope, confirmationApi, schemaApi) {
        var vm = this;
        vm.hospitalCount = 0;
        vm.schemas = [];
        activate();

        function activate() {
            schemaApi.schemas.list().$promise.then(function (result) {
                angular.copy(result, vm.schemas);
            },
            function (error) {
                console.error(error);
            });

            confirmationApi.hospitalCount.get().$promise.then(function (result) {
                vm.hospitalCount = result.Count;
            },
            function (error) {
                console.error(error);
            });
        };
    }

    app.component('navigation', {
        templateUrl: 'app/components/navigation/navigation.html',
        controller: navigationCtrl
    });

}());
(function () {
    'use strict';

    var app = angular.module('confirmationsApp');
    app.controller('configurationCounterpartiesViewCtrl', configurationCounterpartiesViewCtrl);

    configurationCounterpartiesViewCtrl.$inject = ['$location', 'configurationApi', 'NgTableParams', '$scope'];

    function configurationCounterpartiesViewCtrl($location, configurationApi, NgTableParams, $scope) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'counterpartiesViewCtrl';
        vm.parties = [];
        vm.displayParties = [];
      
        vm.tableParams = new NgTableParams({
            count: 4,
            sorting: { PartyId: "asc" }
        }, {
            counts: [],
            dataset: vm.parties
        });
        $scope.predicates = ['PartyID'];

        activate();

        function activate() {
           
            vm.loading = true;
            configurationApi.parties.list().$promise.then(
                function (response) {
                    //vm.parties = response;
                    vm.loading = false;
                    Array.prototype.push.apply(vm.parties, response);
                    Array.prototype.push.apply(vm.displayParties, response);
                    //vm.tableParams.dataset = vm.parties;
                    vm.tableParams.reload();
                    _.each(document.getElementsByName("PartyID"), function (element) { element.placeholder = "Search" });
                },
                function (error) {
                    console.log("configurationApi request failed");
                    console.error(error);
                });
        }

    }

    app.component('configurationCounterpartiesView', {
        templateUrl: 'app/views/configuration/counterparties.html',
        controller: configurationCounterpartiesViewCtrl
    });

})();


(function () {
    'use strict';

    var app = angular.module('confirmationsApp');
    app.controller('configurationCounterpartyViewCtrl', configurationCounterpartyViewCtrl);

    configurationCounterpartyViewCtrl.$inject = ['$location', 'NgTableParams', 'configurationApi', '$state', '$stateParams', '$scope'];

    function configurationCounterpartyViewCtrl($location, NgTableParams, configurationApi, $state, $stateParams, $scope) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'configurationCounterpartyViewCtrl';
        vm.party = {};
        vm.contacts = [];
        vm.contact = {};
        vm.isUpdate = false;
        vm.partyID = '';
        vm.tableParams = new NgTableParams({
        }, {
            filterDelay: 0,
            dataset: vm.contacts
        });

        //$scope.$watch('streetTextarea', function (newValue, oldValue) {
        //    vm.party.ContactInfo.StreetAddressLines = newValue.split('\n');
        //});

        activate();

        function activate() {
            if ($stateParams.new === true) {
                if ($stateParams.partyID !== undefined) {
                    vm.isUpdate = true;
                    vm.partyID = $stateParams.partyID;
                    vm.party.PartyID = vm.partyID;
                }
            }
            else {
                //!-- party update
                if ($stateParams.partyID !== undefined) {
                    vm.isUpdate = true;
                    vm.partyID = $stateParams.partyID;
                    configurationApi.party.get({ partyId: vm.partyID })
                        .$promise.then(function (response) {
                            vm.party = response;
                            console.log(vm.party);
                            //vm.streetTextarea = vm.party.ContactInfo.Address.StreetAddressLines.join("\n");
                        },
                        function (error) {
                            console.log("Error retrieving party for id: " + vm.partyID);
                            console.error(error);
                        });
                }
            }
        }

        vm.apply = function () {
            console.log(vm.party);
            //if (vm.streetTextarea !== undefined) {
            //    _.extend(vm.party, { ContactInfo: { Address: { StreetAddressLines: vm.streetTextarea.split('\n') } } });
            //}

            
            configurationApi.parties.upsert({ }, vm.party).$promise.then(function (result) {
                $state.go('configurationCounterpartiesView');
            }, function (error) {
                console.log("Error updating party for id: " + vm.party.PartyID);
                console.error(error);
            });
            
        }


        // for contacts edit

        //vm.addContact = function () {
        //    var isFilled = false;
        //    _.each(_.values(vm.contact), function (element, index, list) {
        //        if (element.length > 0) {
        //            isFilled = true;
        //        }
        //    });
        //    if (_.isEmpty(vm.contact) || !isFilled) {
        //        $('#nameadd').focus();
        //    }
        //    else {
        //        var contactCopy = angular.copy(vm.contact);
        //        vm.party.Contacts.push(contactCopy);
        //        vm.contact = {};
        //    }
        //}


        ////contacts table 
        //vm.cancel = function (contact, contactForm) {
        //    vm.reset(contact, contactForm);
        //    var original = _.findWhere(vm.contacts, contact);
        //    angular.extend(contact, original);
        //}

        //vm.del = function (contact) {
        //    console.log(contact);
        //    vm.contacts = _.without(vm.contacts, contact);
        //    vm.tableParams.reload();
        //}

        //vm.reset = function (contact, contactForm) {
        //    contact.isEditing = false;
        //    contactForm.$setPristine();
        //}

        //vm.save = function (contact, contactForm) {
        //    vm.reset(contact, contactForm);
        //    var original = _.findWhere(vm.contacts, contact);
        //    angular.extend(original, contact);
        //}
    }

    app.component('configurationCounterpartyView', {
        templateUrl: 'app/views/configuration/counterparty.html',
        controller: configurationCounterpartyViewCtrl
    });
})();

(function () {
    'use strict';

    var app = angular.module('confirmationsApp');
    app.controller('configurationPriceIndexViewCtrl', configurationPriceIndexViewCtrl);

    configurationPriceIndexViewCtrl.$inject = ['$location', '$stateParams', 'configurationApi', 'NgTableParams'];

    function configurationPriceIndexViewCtrl($location, $stateParams, configurationApi, NgTableParams) {
        /* jshint validthis:true */
        var vm = this;
        vm.applying = false;
        vm.priceIndexes = [];
        vm.selectedPriceIndex = {};
        vm.userPriceIndexInput = "";
        vm.showWarningUserPriceIndexInput = false;
        vm.showWarningSelectedPriceIndex = false;
        vm.showApplyError = false;
        vm.showApplySuccess = false;

        activate();

        function activate() {
            loadIndexes();

            if ($stateParams.userPriceIndexID !== undefined) {
                vm.userPriceIndexInput = $stateParams.userPriceIndexID
            }
        }

        function loadIndexes()
        {
            vm.loading = true;
            configurationApi.priceIndexes.list().$promise.then(
                function (response) {
                    vm.priceIndexes = response;
                    vm.loading = false;
                },
                function (error) {
                    console.log("configurationApi request failed: " + error);
                }
            );
        }

        vm.priceIndexRadioClick = function()
        {
            vm.showWarningSelectedPriceIndex = false;
        }

        vm.userPriceIndexInputKeyup = function()
        {
            if(vm.userPriceIndexInput.length < 1)
                vm.showWarningUserPriceIndexInput = true;
            else
                vm.showWarningUserPriceIndexInput = false;
        }

        vm.applyMappingClick = function()
        {
            vm.showApplyError = false;
            vm.showApplySuccess = false;
            var error = false;
            if (vm.userPriceIndexInput.length < 1)
            {
                vm.showWarningUserPriceIndexInput = true;
                error = true;
            }
            if (angular.equals(vm.selectedPriceIndex, {})) {
                vm.showWarningSelectedPriceIndex = true;
                error = true;
            }
            if (!error) {
                applyMapping();
            }
        }

        function applyMapping()
        {
            vm.applying = true;
            configurationApi.priceIndexes.save({}, { id: vm.selectedPriceIndex, userPriceIndexIDs: [vm.userPriceIndexInput] })
            .$promise.then(function (result) {
                vm.updatedIndexId = vm.selectedPriceIndex;
                vm.updatedIndexCode = document.getElementById("code" + vm.selectedPriceIndex).innerText;
                vm.updatedUserIndexId = vm.userPriceIndexInput;
                vm.selectedPriceIndex = {};
                vm.userPriceIndexInput = "";
                vm.showApplySuccess = true;
                vm.applying = false;
                vm.priceIndexes = [];
                loadIndexes();
            },
                function (error) {
                    vm.showApplyError = true;
                    console.error(error);
                    vm.applying = false;
                });
        }
    };

    app.component('configurationPriceIndexView', {
        templateUrl: 'app/views/configuration/priceIndex.html',
        controller: configurationPriceIndexViewCtrl
    });

    app.filter('findPIdxBy', function () {
        return function (data, userInput) {
            if (!userInput || userInput === 'undefined') {
                return data;
            }
            var results = [];
            var keywordArray = userInput.split(" ");
            angular.forEach(data, function (element) {
                var matchesAll = true;
                angular.forEach(keywordArray, function (phrase) {
                    if (element.Description.toLowerCase().indexOf(phrase.toLowerCase()) == -1
                        && element.Code.toLowerCase().indexOf(phrase.toLowerCase()) == -1) {
                        matchesAll = false;
                        return;
                    }
                });
                if (matchesAll)
                    results.push(element);
            });
            return results;
        };
    });
})();

(function () {
    'use strict';

    var app = angular.module('confirmationsApp');
    app.controller('configurationTranslatorViewCtrl', configurationTranslatorViewCtrl);

    configurationTranslatorViewCtrl.$inject = ['$location', '$stateParams', 'configurationApi', 'NgTableParams'];

    function configurationTranslatorViewCtrl($location, $stateParams, configurationApi, NgTableParams) {
        /* jshint validthis:true */
        var vm = this;
        vm.applying = false;
        vm.translations = [];
        vm.phraseInput = "";
        vm.translationInput = "";
        vm.showWarningPhraseInput = false;
        vm.showWarningTranslationInput = false;
        vm.showApplyError = false;
        vm.showApplySuccess = false;

        activate();

        function activate() {
            loadTranslations();
        }

        function loadTranslations()
        {
            vm.loading = true;
            configurationApi.translation.list().$promise.then(
                function (response) {
                    vm.translations = response;
                    vm.loading = false;
                },
                function (error) {
                    console.log("configurationApi request failed");
                    console.error(error);
                }
            );
        }

        vm.priceIndexRadioClick = function()
        {
            vm.showWarningTranslationInput = false;
        }

        vm.phraseInputKeyup = function()
        {
            if(vm.phraseInput.length < 1)
                vm.showWarningPhraseInput = true;
            else
                vm.showWarningPhraseInput = false;
        }

        vm.translationInputKeyup = function () {
            if (vm.phraseInput.length < 1)
                vm.showWarningTranslationInput = true;
            else
                vm.showWarningTranslationInput = false;
        }

        vm.applyMappingClick = function()
        {
            vm.showApplyError = false;
            vm.showApplySuccess = false;
            var error = false;
            if (vm.phraseInput.length < 1)
            {
                vm.showWarningPhraseInput = true;
                error = true;
            }
            if (vm.translationInput.length < 1) {
                vm.showWarningTranslationInput = true;
                error = true;
            }
            if (!error) {
                applyMapping();
            }
        }

        function applyMapping()
        {
            vm.applying = true;
            configurationApi.translation.set({}, { phrase: vm.phraseInput, translation: vm.translationInput })
            .$promise.then(function (result) {
                vm.updatedPhrase = vm.phraseInput;
                vm.updatedTranslation = vm.translationInput;
                vm.phraseInput = "";
                vm.translationInput = "";
                vm.showApplySuccess = true;
                vm.applying = false;
                vm.translations = [];
                loadTranslations();
            },
                function (error) {
                    vm.showApplyError = true;
                    console.error(error);
                    vm.applying = false;
                });
        }
    };

    app.component('configurationTranslatorView', {
        templateUrl: 'app/views/configuration/translator.html',
        controller: configurationTranslatorViewCtrl
    });

    app.filter('findBy', function () {
        return function (data, userInput) {
            if (!userInput || userInput === 'undefined') {
                return data;
            }
            var results = [];
            var keywordArray = userInput.split(" ");
            angular.forEach(data, function (element) {
                var matchesAll = true;
                angular.forEach(keywordArray, function (phrase) {
                    if (element.Translation.toLowerCase().indexOf(phrase.toLowerCase()) == -1
                        && element.Phrase.toLowerCase().indexOf(phrase.toLowerCase()) == -1) {
                        matchesAll = false;
                        return;
                    }
                });
                if (matchesAll)
                    results.push(element);
            });
            return results;
        };
    });
})();

(function () {
    'use strict';

    var app = angular.module('confirmationsApp');
    app.controller('confirmationsViewCtrl', confirmationsViewCtrl);
    confirmationsViewCtrl.$inject = ['$scope', '$state', 'NgTableParams', 'confirmationApi', '$stateParams'];

    function confirmationsViewCtrl($scope, $state, NgTableParams, confirmationApi, $stateParams) {
        var vm = this;
        vm.loading = false;
        vm.confirmations = {};
        vm.displayConfirmations = {};
        vm.columns = [];
        vm.itemsByPage = 20;

        vm.menuOptions = function (confirmation) {
            if (getStatusContextOptions(confirmation).length > 0) {
                return [
                    ['Details', function ($itemScope, $event, modelValue, text, $li) {
                        goToDetails($itemScope.confirmation['Trade ID'], $itemScope.confirmation['Internal party ID']);
                    }
                    ],
                    null,
                      ['Status', null,

                        getStatusContextOptions(confirmation)
                      ]

                ];
            }
            else
            {
                return [
                    ['Details', function ($itemScope, $event, modelValue, text, $li) {
                        goToDetails($itemScope.confirmation['Trade ID'], $itemScope.confirmation['Internal party ID']);
                    }
                    ]
                ];
            }
        };

        activate();

        function reload() {
            var current = $state.current;
            var params = angular.copy($stateParams);
            return $state.transitionTo(current, params, { reload: true, inherit: true, notify: true });
        }

        function activate() {

            setItemsByPage();
            //vm.displayTrades = confirmationApi.confirmations.list();
            vm.loading = true;
            var confirmationsPromise;
            if ($stateParams.schemaId !== undefined) {
                vm.schemaId = $stateParams.schemaId;
                confirmationsPromise = confirmationApi.confirmationsBySchema.list({ SchemaID: vm.schemaId }).$promise;
            }
            else
            {
                confirmationsPromise = confirmationApi.confirmations.list().$promise;
            }
            
            confirmationsPromise.then(
                function (response) {
                    vm.loading = false;
                    vm.confirmations = angular.copy(response);
                    vm.displayConfirmations = angular.copy(response);
                    if (vm.confirmations.length > 0) {
                        vm.columns = _.keys(vm.confirmations[0]);
                    }
                    //Array.prototype.push.apply(vm.trades, response);
                    //Array.prototype.push.apply(vm.displayTrades, response);
                },
                function (error) {
                    console.log("configurationApi request failed");
                    console.error(error);
                });
        };

        function activateBySchemaId()
        {

        }

        function goToDetails(tradeId, internalPartyId) {
            $state.go('confirmationDetailsView', { "tradeId": tradeId, "internalPartyId": internalPartyId });
        }

        function getStatusContextOptions(confirmation) {
            var contextOptions = [];
            for (var i = 0; i < confirmation.NextStatuses.length; i++)
            {
                var option = {
                    text: confirmation.NextStatuses[i],
                    click: function ($itemScope, $event, modelValue, text, $li) {
                        $scope.statusSelect($itemScope, $li[0].innerText);
                    }
                };
                contextOptions.push(option);
            }
            return contextOptions;
        };

        function setItemsByPage() {
            var height = window.innerHeight
                || document.documentElement.clientHeight
                || document.body.clientHeight;

            vm.itemsByPage = Math.max(0, (height - 200) / 32);
        }

        //window.onresize = function (event) {
        //    setItemsByPage();
        //    $scope.$apply();
        //};

        $scope.statusSelect = function($itemScope, text) {
            var tradeId = $itemScope.confirmation['Trade ID'];
            var internalPartyId = $itemScope.confirmation['Internal party ID'];
            var row = $("#" + tradeId + "_" + internalPartyId);
            row.find('#updateSpinner').removeClass('invisible');
            
            console.log(text);
            confirmationApi.confirmationStatus.set({ tradeId: tradeId, internalPartyId: internalPartyId, statusId: text })
                .$promise.then(function (response) {
                    console.log(response);
                    $itemScope.confirmation.Status = response.Status;
                    $itemScope.confirmation.NextStatuses = response.NextStatuses;
                    row.find('#updateSpinner').addClass('invisible');
                    
                },
                function(error){
                    console.error(error);
                });
            
        }

    };

    app.component('confirmationsView', {
        templateUrl: 'app/views/confirmations/confirmations.html',
        controller: confirmationsViewCtrl
    });

}());

//function statusSelect ($itemScope, statusName) {
//    var row = document.getElementById('trade_row_' + $itemScope.trade.Id);
//    jQuery('#trade_status_spinner_' + +$itemScope.trade.Id).removeClass('invisible');
//    console.log($itemScope);
//    $itemScope.confirmationApi.tryChangeStatus()
//    //alert(statusName + ' ' + $itemScope.trade.portfolio);
//}
(function () {
    'use strict';

    var app = angular.module('confirmationsApp');
    app.controller('confirmationDetailsViewCtrl', confirmationDetailsViewCtrl);
    confirmationDetailsViewCtrl.$inject = ['$scope', 'confirmationApi', 'documentApi', '$state', '$stateParams', 'Upload'];

    function confirmationDetailsViewCtrl($scope, confirmationApi, documentApi, $state, $stateParams, Upload) {
        var vm = this;
        vm.tradeId = "";
        vm.internalPartyId = "";
        vm.userId = -1;
        vm.confirmation = {};
        vm.fields = [];
        vm.audits = [];
        vm.generatedDocumentInfos = [];
        vm.uploadedDocumentInfos = [];
        

        activate();

        function activate() {
            if ($stateParams.tradeId !== undefined && $stateParams.internalPartyId !== undefined) {
                
                vm.tradeId = $stateParams.tradeId;
                vm.internalPartyId = $stateParams.internalPartyId;

                documentApi.generatedDocumentInfos.list({}, { TradeID: vm.tradeId, InternalPartyID: vm.internalPartyId })
                    .$promise.then(function (response) {
                        vm.generatedDocumentInfos = response;
                        console.log(vm.generatedDocumentInfos);
                    },
                    function (error) {
                        console.log("Error retrieving document infos for trade id: " + vm.tradeId + " and internal party id: " + vm.internalPartyId);
                        console.error(error);
                    });

                documentApi.uploadedDocumentInfos.list({}, { TradeID: vm.tradeId, InternalPartyID: vm.internalPartyId })
                    .$promise.then(function (response) {
                        vm.uploadedDocumentInfos = response;
                        console.log(vm.uploadedDocumentInfos);
                    },
                    function (error) {
                        console.log("Error retrieving uploaded document infos for trade id: " + vm.tradeId + " and internal party id: " + vm.internalPartyId);
                        console.error(error);
                    });

                confirmationApi.confirmation.query({}, { TradeID: vm.tradeId, InternalPartyID: vm.internalPartyId })
                    .$promise.then(function (response) {
                        vm.confirmation = response[0];
                        console.log(vm.confirmation);
                    },
                    function (error) {
                        console.log("Error retrieving trade for trade id: " + vm.tradeId + " and internal party id: " + vm.internalPartyId);
                        console.error(error);
                    });

                confirmationApi.audit.get({ tradeId: vm.tradeId, internalPartyId: vm.internalPartyId })
                    .$promise.then(function (response) {
                        vm.audits = response;
                        console.log(vm.audits);
                    },
                    function (error) {
                        console.log("Error retrieving audit for trade id: " + vm.tradeId + " and internal party id: " + vm.internalPartyId);
                        console.error(error);
                    });

                
            }
        };

        vm.downloadDocument = function (id) {
            
            
            window.open("/api/v1/confirmationview/documents/"+id, '_self', '');
            
            //documentApi.document.download({ "id": id }).$promise.then(function (data) {
            //    var headers = data.$httpHeaders();
            //    var filename = headers['filename'] || 'download.docx';

            //    //var url = getDownloadUrl(data, headers);
            //    var linkElement = document.createElement('a');
            //    linkElement.setAttribute('href', url);
            //    linkElement.setAttribute("download", filename);

            //    var clickEvent = new MouseEvent("click", {
            //        "view": window,
            //        "bubbles": true,
            //        "cancelable": false
            //    });

                
            //    //linkElement.dispatchEvent(clickEvent);
            //    //window.location = url;

            //}, function (error) {
            //    console.log("Error retrieving document for id: " + id);
            //    console.error(error);
            //});
        };

        //function getDownloadUrl(data) {
            
            
        //    var blob = new Blob([data], { type: 'application/octet-stream' });
        //    var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;
        //    saveAs(blob, 'test.xxx');
        //    return urlCreator.createObjectURL(blob);
            
        //};

        // upload on file select or drop
        vm.upload = function (file) {
            Upload.upload({
                url: '/api/v1/document/upload',
                data: {
                    file: file, info: Upload.json(
                        {
                            TradeID: vm.tradeId, InternalPartyID: vm.internalPartyId, UserID : vm.userId
                        })
                }
            }).then(function (resp) {
                console.log('Success ' + resp.config.data.file.name + ' uploaded. Response: ' + resp.data);
            }, function (resp) {
                console.log('Error status: ' + resp.status);
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
        };

        // for multiple files:
        vm.uploadFiles = function (files) {
          if (files && files.length) {
              for (var i = 0; i < files.length; i++) {
                  vm.upload(files[i]);
            }
          }
        }
    };

    app.component('confirmationDetailsView', {
        templateUrl: 'app/views/confirmations/details.html',
        controller: confirmationDetailsViewCtrl
    });
}());



 

 
//if (!success) {
//    // Fallback to window.open method
//    $log.info("No methods worked for saving the arraybuffer, using last resort window.open");
//    window.open(httpPath, '_blank', '');
//}
//return filename;
//};
(function () {
    'use strict';

    var app = angular.module('confirmationsApp');
    app.controller('hospitalViewCtrl', hospitalViewCtrl);

    hospitalViewCtrl.$inject = ['$location', 'configurationApi', 'confirmationApi'];

    function hospitalViewCtrl($location, configurationApi, confirmationApi) {
        /* jshint validthis:true */
        var vm = this;
        vm.priceIndexesUnaligned = [];
        vm.partiesUnaligned = [];
        vm.notValid = [];
        activate();

        function activate() {
            vm.loadingNotValid = true;
            confirmationApi.hospital.get().$promise.then(function (result) {
                angular.copy(result, vm.notValid);
                vm.loadingNotValid = false;
            },
            function (error) {
                console.error(error);
            });

            vm.loadingUnalignedPriceIndexes = true;
            configurationApi.priceIndexesUnaligned.list({ count: 30 }).$promise.then(function (result) {
                angular.copy(result, vm.priceIndexesUnaligned);
                vm.loadingUnalignedPriceIndexes = false;
            },
            function (error) {
                console.error(error);
            });

            vm.loadingUnalignedParties = true;
            configurationApi.partiesUnaligned.list({ count: 30 }).$promise.then(function (result) {
                angular.copy(result, vm.partiesUnaligned);
                vm.loadingUnalignedParties = false;
            },
            function (error) {
                console.error(error);
            });
        }
    }

    app.component('hospitalView', {
        templateUrl: 'app/views/hospital/hospital.html',
        controller: hospitalViewCtrl
    });
})();

(function () {
    'use strict';

    var app = angular.module('confirmationsApp');
    app.controller('mainViewCtrl', mainViewCtrl);

    mainViewCtrl.$inject = ['$location', 'configurationApi', 'confirmationApi'];

    function mainViewCtrl($location, configurationApi, confirmationApi) {
        var vm = this;
        vm.priceIndexesUnaligned = [];
        vm.newEntries = [];

        activate();

        function activate() {
            vm.loadingUnalignedPriceIndexes = true;
            vm.loadingNewEntries = true;
            configurationApi.priceIndexesUnaligned.list({ count: 5 }).$promise.then(function (result) {
                angular.copy(result, vm.priceIndexesUnaligned);
                vm.loadingUnalignedPriceIndexes = false;
            },
            function (error) {
                console.error(error);
            });

            confirmationApi.confirmationsNew.new({ count: 10 }).$promise.then(function (result) {
                angular.copy(result, vm.newEntries);
                vm.loadingNewEntries = false;
            },
            function (error) {
                console.error(error);
            })
        }
    }


    app.component('mainView', {
        templateUrl: 'app/views/main/main.html',
        controller: mainViewCtrl
    });
})();

(function () {
    'use strict';

    angular
        .module('confirmationsApp')
        .factory('configurationApi', configurationApi);

    configurationApi.$inject = ['$resource'];

    function configurationApi($resource) {
        return {
            priceIndexes: $resource("/api/v1/priceindexes", {}, {
                list: { method: "GET", isArray: true },
                save: { method: "PUT" }
            }),
            priceIndexesUnaligned: $resource("/api/v1/priceindexes/unaligned/:count", {
                count: "@count"
            }, {
                list: { method: "GET", isArray: true }
            }),
            //priceIndex: $resource("/api/v1/priceindexes/:id", {
            //    id: "@id"
            //}, {
            //    save: { method: "PUT" }
            //}),
            parties: $resource("/api/v1/parties", {}, {
                list: { method: "GET", isArray: true },
                upsert: { method: "POST" }
            }),
            party: $resource("/api/v1/parties/:partyId", {
                partyId: "@partyId"
            }, {
                get: { method: "GET", isArray: false }
            }),
            partiesUnaligned: $resource("/api/v1/parties/unaligned/:count", {
                count: "@count"
            }, {
                list: { method: "GET", isArray: true }
            }),
            translation: $resource("/api/v1/translation", {}, {
                list: { method: "GET", isArray: true },
                set: { method: "POST"}
            })
        };
    }
})();
(function () {
    'use strict';

    angular
        .module('confirmationsApp')
        .factory('confirmationApi', confirmationApi);

    confirmationApi.$inject = ['$resource'];

    function confirmationApi($resource) {
        return {
            confirmations: $resource("/api/v1/confirmationview", {}, {
                list: { method: "GET", isArray: true }
            }),
            confirmationsBySchema: $resource("/api/v1/confirmationview/schema/:SchemaID", {
                SchemaID : '@SchemaID'
            }, {
                list: { method: "GET", isArray: true }
            }),
            confirmationsNew: $resource("/api/v1/confirmationview/new/:count", {
                count: '@count'
            }, {
                'new': { method: "GET", isArray: true }
            }),
            confirmation: $resource("/api/v1/confirmationview/query", {
            }, {
                query: { method: "POST", isArray: true }
            }),
            hospitalCount: $resource("/api/v1/confirmationview/hospital/count", {
            }, {
                get: { method: "GET", isArray: false }
                }),
            hospital: $resource("/api/v1/confirmationview/hospital", {
            }, {
                    get: { method: "GET", isArray: true }
            }),
            confirmationStatus: $resource("/api/v1/confirmationview/status/:statusId", {
                tradeId: '@tradeId',
                internalPartyId: '@internalPartyId',
                statusId: '@statusId'
            }, {
                set: { method: "PUT", isArray: false }
            }),
            audit: $resource("/api/v1/confirmationview/audit", {
                tradeId: '@tradeId',
                internalPartyId: '@internalPartyId'
            }, {
                get: { method: "POST", isArray: true }
            })
        };
    }
})();
(function () {
    'use strict';

    angular
        .module('confirmationsApp')
        .factory('documentApi', documentApi);

    documentApi.$inject = ['$resource'];

    function documentApi($resource) {
        return {
            generatedDocumentInfos: $resource("/api/v1/document/generated/info", {
            }, {
                list: { method: "POST", isArray: true}
            }),
            uploadedDocumentInfos: $resource("/api/v1/document/uploaded/info", {
            }, {
                list: { method: "POST", isArray: true }
            }),
            generated: $resource("/api/v1/document/generated/:messageId", {
                messageId: "@messageId"
            }, {
                download: {
                    method: "GET", responseType: 'arraybuffer',
                    interceptor: {
                        response: function (response) {
                            response.resource.$httpHeaders = response.headers;
                            return response.resource;
                        }
                    }
                }
            }),
            uploaded: $resource("/api/v1/document/uploaded/:id", {
                id: "@id"
            }, {
                download: {
                    method: "GET", responseType: 'arraybuffer',
                    interceptor: {
                        response: function (response) {
                            response.resource.$httpHeaders = response.headers;
                            return response.resource;
                        }
                    }
                }
            })
        };
    }
})();
(function () {
    'use strict';

    angular
        .module('confirmationsApp')
        .factory('schemaApi', schemaApi);

    schemaApi.$inject = ['$resource'];

    function schemaApi($resource) {
        return {
            schemas: $resource("/api/v1/schema", {
            }, {
                list: { method: "GET", isArray: true}
            })
        };
    }
})();