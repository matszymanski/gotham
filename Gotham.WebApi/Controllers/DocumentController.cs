﻿using Gotham.Core;
using Gotham.Core.Web;
using Gotham.Domain.Confirmation;
using Gotham.Domain.Documents;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace Gotham.WebApi.Controllers
{

    [RoutePrefix("api/v1/document")]
    public class DocumentController : ApiController
    {
        [HttpPost]
        [Route("generated/info")]
        public IHttpActionResult GetGeneratedDocumentInfos([FromBody] TradeUniqueKey tradeKey)
        {
            try
            {
                return Ok(DocumentService.GetGeneratedDocumentInfos(tradeKey));
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("uploaded/info")]
        public IHttpActionResult GetUploadedDocumentInfos([FromBody] TradeUniqueKey tradeKey)
        {
            try
            {
                return Ok(DocumentService.GetUploadedDocumentInfos(tradeKey));
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [HttpGet]
        [Route("generated/{messageId}")]
        public HttpResponseMessage GetGeneratedDocumentByMessageId(long messageId)
        {
            try
            {
                var file = DocumentService.GetGeneratedDocument(messageId);
                if (file == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "No document associated with messageId: " + messageId);
                var fileStream = new MemoryStream(file.File);

                return FormatFileResponse(fileStream, file.FileName);
            }
            catch(Exception e)
            {
                throw;
            }
        }

        [HttpGet]
        [Route("uploaded/{id}")]
        public HttpResponseMessage GetUploadedDocumentById(long id)
        {
            try
            {
                var file = DocumentService.GetUploadedDocument(id);
                if (file == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "No document associated with id: " + id);
                var fileStream = new MemoryStream(file.File);

                return FormatFileResponse(fileStream, file.FileName);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        private HttpResponseMessage FormatFileResponse(MemoryStream fileStream, string fileName)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(fileStream);
            response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = fileName;
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            response.Headers.Add("filename", fileName);
            return response;
        }

        [HttpPost]
        [Route("upload")]
        public IHttpActionResult UploadDocument()
        {
            try
            {
                var request = HttpContext.Current.Request;
                var info = JObject.Parse(request.Form["info"]);
                string tradeID = info["TradeID"].ToString();
                string internalPartyID = info["InternalPartyID"].ToString();
                int userId = -1;
                int.TryParse(info["UserID"].ToString(), out userId);

                if (string.IsNullOrEmpty(tradeID))
                    return BadRequest("Missing TradeID");
                if (string.IsNullOrEmpty(internalPartyID))
                    return BadRequest("Missing InternalPartyID");

                var tradeKey = new TradeUniqueKey { TradeID = tradeID, InternalPartyID = internalPartyID };

                foreach (string fileKey in request.Files)
                {
                    var file = request.Files[fileKey];

                    var uploadedDocumentInfo = new UploadedDocumentInfo { AutoMatched = false, UserID = userId < 1 ? (int?)null : userId, DocumentName = file.FileName };

                    var memoryStream = new MemoryStream();
                    file.InputStream.CopyTo(memoryStream);
                    DocumentService.UploadDocument(tradeKey, uploadedDocumentInfo, memoryStream.ToArray());
                }

                return Ok("");
            }
            catch(Exception e)
            {
                throw;
            }
        }
    }
}