﻿using Gotham.Core.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Gotham.WebApi.Controllers
{
    [RoutePrefix("api/v1/schema")]
    public class SchemaController : ApiController
    {

        [HttpGet]
        [Route("")]
        public IHttpActionResult GetSchemas()
        {
            try
            {
                return this.Ok(SchemaService.GetSchemas());
            }
            catch (Exception e)
            {
                return InternalServerError();
            }
        }
    }
}
