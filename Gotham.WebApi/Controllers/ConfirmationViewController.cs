﻿using Gotham.Core;
using Gotham.Core.Web;
using Gotham.Domain.Confirmation;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace Gotham.WebApi.Controllers
{
    [RoutePrefix("api/v1/confirmationview")]
    public class ConfirmationViewController : ApiController
    {

        [HttpGet]
        [Route()]
        public IHttpActionResult GetList()
        {
            try
            {
                var jsonResponse = JToken.FromObject(ConfirmationViewService.ConfirmationViews());
                return Ok(jsonResponse);
            }
            catch(Exception e)
            {
                throw;
            }
        }

        [HttpGet]
        [Route("schema/{SchemaID}")]
        public IHttpActionResult GetListBySchema(int schemaID)
        {
            try
            {
                var jsonResponse = JToken.FromObject(ConfirmationViewService.ConfirmationViewsBySchema(schemaID));
                return Ok(jsonResponse);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("query")]
        public IHttpActionResult GetById([FromBody] TradeUniqueKey tradeKey)
        {
            try
            {
                var jsonResponse = JToken.FromObject(ConfirmationViewService.ConfirmationViewById(tradeKey));
                return Ok(jsonResponse);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [HttpPut]
        [Route("status/{statusId}")]
        public IHttpActionResult SetStatus([FromBody] TradeUniqueKey tradeKey, string statusId)
        {
            try
            {
                //var jsonResponse = JToken.FromObject(ConfirmationViewService.ConfirmationViewById(tradeId, internalPartyId));
                var st = StatusesUtil.FromDisplayString(statusId);
                var newStatus = ConfirmationViewService.UpdateStatus(tradeKey, st.Value);

                string[] nextStatuses = new string[0];
                nextStatuses =  ConfirmationStatus.GetNext(st.Value).Select(status => status.ToDisplayString()).ToArray();
                return Ok(new { Status = newStatus.ToDisplayString(), NextStatuses = nextStatuses });
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("audit")]
        public IHttpActionResult GetAudit([FromBody] TradeUniqueKey tradeKey)
        {
            try
            {
                var audits = ConfirmationViewService.ConfirmationViewAuditById(tradeKey);
                return Ok(audits);
            }
            catch (Exception e)
            {
                throw;
            }
        }
        

        [HttpGet]
        [Route("new/{count}")]
        public IHttpActionResult GetNew(int count)
        {
            try
            {
                var jsonResponse = JToken.FromObject(ConfirmationViewService.ConfirmationViewsNew(count));
                return Ok(jsonResponse);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [HttpGet]
        [Route("hospital/count")]
        public IHttpActionResult GetHospitalCount()
        {
            try
            {
                return Ok(new { Count = ConfirmationViewService.GetHospitalCount() });
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [HttpGet]
        [Route("hospital")]
        public IHttpActionResult GetHospital()
        {
            try
            {
                var jsonResponse = JToken.FromObject(ConfirmationViewService.GetHospital());
                return Ok(jsonResponse);
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
