﻿using Gotham.Core;
using Gotham.Core.Web;
using Gotham.Domain;
using Gotham.Domain.Confirmation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Gotham.WebApi.Controllers
{
    [RoutePrefix("api/v1")]
    public class ConfigurationController : ApiController
    {

        [HttpGet]
        [Route("priceindexes")]
        public IHttpActionResult GetPriceIndexes()
        {
            return this.Ok(ConfigurationService.PriceIndexes());
        }
        
        [HttpPut]
        [Route("priceindexes")]
        public IHttpActionResult SetPriceIndexMap([FromBody] PriceIndex priceIndex)
        {
            try
            {
                var userId = priceIndex.UserPriceIndexIDs.First();
                ConfigurationService.PriceIndexUpdate(priceIndex.ID, userId);
                return this.Ok();
            }
            catch(Exception e)
            {
                return InternalServerError();
            }
        }

        [HttpGet]
        [Route("priceindexes/unaligned/{count}")]
        public IHttpActionResult GetPriceIndexesUnaligned(int count)
        {
            try
            {
                return this.Ok(ConfigurationService.PriceIndexesUnaligned(count));
            }
            catch (Exception e)
            {
                return InternalServerError();
            }
        }

        [HttpGet]
        [Route("parties")]
        public IHttpActionResult GetParties()
        {
            return this.Ok(ConfigurationService.Parties());
        }

        [HttpPost]
        [Route("parties")]
        public IHttpActionResult UpsertParty([FromBody] Party party)
        {
            try
            {
                ConfigurationService.PartyUpsert(party);
                return this.Ok("stored");
            }
            catch (Exception e)
            {
                return this.InternalServerError(e);
            }
        }
        

        [HttpGet]
        [Route("parties/{partyId}")]
        public IHttpActionResult GetParty(string partyId)
        {
            return this.Ok(ConfigurationService.PartyById(partyId));
        }

        [HttpGet]
        [Route("parties/unaligned/{count}")]
        public IHttpActionResult GetPartiesUnaligned(int count)
        {
            try
            {
                return this.Ok(ConfigurationService.PartyIDsUnaligned(count));
            }
            catch (Exception e)
            {
                return InternalServerError();
            }
        }

        [HttpGet]
        [Route("translation")]
        public IHttpActionResult GetTranslations()
        {
            try
            {
                return this.Ok(TranslatorService.GetAllTranslations());
            }
            catch (Exception e)
            {
                return InternalServerError();
            }
        }

        [HttpPost]
        [Route("translation")]
        public IHttpActionResult UpsertTranslation([FromBody] TranslationPhrase translation)
        {
            try
            {
                TranslatorService.Upsert(translation);
                return this.Ok("Updated");
            }
            catch (Exception e)
            {
                return InternalServerError();
            }
        }
    }
}
