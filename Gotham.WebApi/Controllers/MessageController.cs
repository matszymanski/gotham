﻿using Gotham.Data;
using Gotham.Elements;
using Gotham.Elements.Instruments;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Gotham.WebApi.Controllers
{
    [RoutePrefix("api/v1/message")]
    public class MessageController : ApiController
    {

        [Route("ZBTFixedPrice/{routeId}/{workflowId}/{templateId}")]
        public IHttpActionResult ZBTFixedPrice(
            int routeId,
            int workflowId,
            int templateId,
            string internalPartyID,
            string externalPartyID,
            string partyAID,
            string partyBID,
            string tradeID,
            DateTime tradeDate,
            DateTime effectiveDate,
            DateTime terminationDate,
            string settlementCCY = "EUR",
            string gasType = "NaturalGas",
            string deliveryPoint = "ZBT",
            string deliveryType = "Firm",
            string quantityUnit = "GJ",
            string quantityFrequency = "PerCalendarDay",
            double quantity = 2400.0,
            string totalQuantityUnit = "GJ",
            double totalQuantity = 74400.0,
            double price = 5.05,
            string priceCCY = "EUR",
            string priceUnit = "GJ",
            string agreement = "ISDA")
        {
            try
            {
                ZBTFixedPrice zbt = new ZBTFixedPrice(internalPartyID, externalPartyID, partyAID, partyBID, new Date(tradeDate), new Date(effectiveDate), new Date(terminationDate), settlementCCY, gasType, deliveryPoint, deliveryType, quantityUnit, quantityFrequency, quantity, totalQuantityUnit, totalQuantity, price, priceCCY, priceUnit, agreement);
                int schemaID = 1;

                zbt.InternalPartyID = internalPartyID;
                zbt.ExternalPartyID = externalPartyID;
                zbt.TradeID = tradeID;

                string json = JsonConvert.SerializeObject(zbt);

                bool saved = ETRMDataAccess.InsertGothamMessage(schemaID, templateId, routeId, workflowId, null, json, string.Empty, string.Empty, internalPartyID, externalPartyID, tradeID, string.Empty);

                if (saved)
                    return Ok("stored");
                else
                    return InternalServerError();
            }
            catch (Exception e)
            {
                //log
                return BadRequest();
            }
        }

        /// <summary>
        /// PartyA is the receiver of physical oil and payer of floating leg
        /// PartyB is the payer of physical oil and receiver of floating leg
        /// </summary>
        /// <param name="internalPartyID"></param>
        /// <param name="externalPartyID"></param>
        /// <param name="tradeID"></param>
        /// <param name="tradeDate"></param>
        /// <param name="effectiveDate"></param>
        /// <param name="terminationDate"></param>
        /// <param name="settlementCCY"></param>
        /// <param name="periodMultiplier"></param>
        /// <param name="period"></param>
        /// <param name="balanceOfFirstPeriod"></param>
        /// <param name="oilType"></param>
        /// <param name="oilGrade"></param>
        /// <param name="pipelineName"></param>
        /// <param name="deliveryType"></param>
        /// <param name="physQtyUnit"></param>
        /// <param name="physQtyFreq"></param>
        /// <param name="physQty"></param>
        /// <param name="totalQtyUnit"></param>
        /// <param name="totalQty"></param>
        /// <param name="fixedPrice"></param>
        /// <param name="fixedPriceCCY"></param>
        /// <param name="fixedPriceUnit"></param>
        /// <param name="agreement"></param>
        /// <returns></returns>
        [Route("WTIFloatingPrice/{routeId}/{workflowId}/{templateId}")]
        public IHttpActionResult WTIFloatingPrice(
            int routeId,
            int workflowId,
            int templateId,
            string internalPartyID,
            string externalPartyID,
            string partyAID,
            string partyBID,
            string tradeID,
            DateTime tradeDate,
            DateTime executionDateTime,
            string executionTimeZone,
            DateTime effectiveDate,
            DateTime terminationDate,
            string settlementCCY = "EUR",
            int periodMultiplier = 1,
            string period = "Term",
            bool balanceOfFirstPeriod = false,
            string oilType = "CrudeOil",
            string oilGrade = "WTI",
            string pipelineName = "TeppcoSeawayCrude",
            string withdrawalPoint = "Cushing",
            bool deliverableByBarge = false,
            string risk = "FOB",
            string quantityUnit = "BBL",
            string quantityFrequency = "PerCalculationPeriod",
            double quantity = 10000.0,
            string instrumentId = "OIL-WTI-NYMEX",
            string specifiedPrice = "Settlement",
            string dayType = "CommodityBusiness",
            string dayDistribution = "All")
        {
            try { 
                WTIFloatingPrice wti = new WTIFloatingPrice(internalPartyID, externalPartyID, partyAID, partyBID, new Date(tradeDate), executionDateTime, executionTimeZone, new Date(effectiveDate), new Date(terminationDate), settlementCCY, periodMultiplier,
                    period, balanceOfFirstPeriod, oilType, oilGrade, pipelineName, withdrawalPoint, deliverableByBarge, risk, quantityUnit, quantityFrequency, quantity, instrumentId, specifiedPrice, dayType, dayDistribution);

                wti.TradeID = tradeID;
                int schemaID = 16;

                string json = JsonConvert.SerializeObject(wti);

                bool saved = ETRMDataAccess.InsertGothamMessage(schemaID, templateId, routeId, workflowId, null, json, string.Empty, string.Empty, internalPartyID, externalPartyID, tradeID, instrumentId);

                if (saved)
                    return Ok("stored");
                else
                    return InternalServerError();
            }
            catch (Exception e)
            {
                //log
                return BadRequest();
            }
        }

    //    /// <summary>
    //    /// PartyA is the receiver of physical oil and payer of floating leg
    //    /// PartyB is the payer of physical oil and receiver of floating leg
    //    /// </summary>
    //    /// <param name="internalPartyID"></param>
    //    /// <param name="externalPartyID"></param>
    //    /// <param name="tradeID"></param>
    //    /// <param name="tradeDate"></param>
    //    /// <param name="effectiveDate"></param>
    //    /// <param name="terminationDate"></param>
    //    /// <param name="settlementCCY"></param>
    //    /// <param name="periodMultiplier"></param>
    //    /// <param name="period"></param>
    //    /// <param name="balanceOfFirstPeriod"></param>
    //    /// <param name="oilType"></param>
    //    /// <param name="oilGrade"></param>
    //    /// <param name="pipelineName"></param>
    //    /// <param name="deliveryType"></param>
    //    /// <param name="physQtyUnit"></param>
    //    /// <param name="physQtyFreq"></param>
    //    /// <param name="physQty"></param>
    //    /// <param name="totalQtyUnit"></param>
    //    /// <param name="totalQty"></param>
    //    /// <param name="fixedPrice"></param>
    //    /// <param name="fixedPriceCCY"></param>
    //    /// <param name="fixedPriceUnit"></param>
    //    /// <param name="agreement"></param>
    //    /// <returns></returns>
    //    [Route("GTMAFixedPrice/{routeId}/{workflowId}/{templateId}")]
    //    public IHttpActionResult GTMAFixedPrice(
    //        int routeId,
    //        int workflowId,
    //        int templateId,
    //        string internalPartyID,
    //        string externalPartyID,
    //        string partyAID,
    //        string partyBID,
    //        string tradeID,
    //        DateTime tradeDate,
    //        DateTime executionDateTime,
    //        string executionTimeZone,
    //        DateTime effectiveDate,
    //        DateTime terminationDate,
    //        string settlementCCY = "EUR",
    //        int periodMultiplier = 1,
    //        string period = "Term",
    //        bool balanceOfFirstPeriod = false,
    //        string oilType = "CrudeOil",
    //        string oilGrade = "WTI",
    //        string pipelineName = "TeppcoSeawayCrude",
    //        string withdrawalPoint = "Cushing",
    //        bool deliverableByBarge = false,
    //        string risk = "FOB",
    //        string quantityUnit = "BBL",
    //        string quantityFrequency = "PerCalculationPeriod",
    //        double quantity = 10000.0,
    //        string instrumentId = "OIL-WTI-NYMEX",
    //        string specifiedPrice = "Settlement",
    //        string dayType = "CommodityBusiness",
    //        string dayDistribution = "All")
    //    {
    //        try
    //        {
    //            var gtma = new GTMAFixedPrice(internalPartyID, externalPartyID, partyAID, partyBID, new Date(tradeDate), executionDateTime, executionTimeZone, new Date(effectiveDate), new Date(terminationDate)
    //                , 67, 1, "M", true, 23.45, "EUR", "MWh", true);
    //            WTIFloatingPrice wti = new WTIFloatingPrice(internalPartyID, externalPartyID, partyAID, partyBID, new Date(tradeDate), executionDateTime, executionTimeZone, new Date(effectiveDate), new Date(terminationDate), settlementCCY, periodMultiplier,
    //                period, balanceOfFirstPeriod, oilType, oilGrade, pipelineName, withdrawalPoint, deliverableByBarge, risk, quantityUnit, quantityFrequency, quantity, instrumentId, specifiedPrice, dayType, dayDistribution);

    //            wti.TradeID = tradeID;
    //            int schemaID = 16;

    //            string json = JsonConvert.SerializeObject(wti);

    //            bool saved = ETRMDataAccess.InsertGothamMessage(schemaID, templateId, routeId, workflowId, null, json, string.Empty, string.Empty, internalPartyID, externalPartyID, tradeID, instrumentId);

    //            if (saved)
    //                return Ok("stored");
    //            else
    //                return InternalServerError();
    //        }
    //        catch (Exception e)
    //        {
    //            //log
    //            return BadRequest();
    //        }
    //    }
    }
}
