﻿CREATE PROCEDURE [test].[sp_UpdatePriceIndexMap]
AS
BEGIN
	SET NOCOUNT ON;

    exec api.sp_GTUpdatePriceIndexMap 'dummyIndexFromETRM_1', 6

	SELECT * FROM dbo.GothamPriceIndex_Map 

	delete from dbo.GothamPriceIndex_Map 
	where UserPriceIndexID = 'dummyIndexFromETRM_1'

	delete from dbo.GothamPriceIndex_MapHistory
	where UserPriceIndexID = 'dummyIndexFromETRM_1'
END