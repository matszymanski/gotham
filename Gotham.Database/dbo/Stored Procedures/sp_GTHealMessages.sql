﻿
create procedure dbo.sp_GTHealMessages
as
begin

declare @healed as table(MessageID bigint primary key, PriceIndexID int)

insert into @healed
select MessageID, NULL from dbo.GTMessageHospital mes
inner join dbo.GTParty intP on mes.InternalPartyID = intP.PartyID
inner join dbo.GTParty extP on mes.InternalPartyID = extP.PartyID
where PriceIndex is null

insert into @healed
select MessageID, isnull(map.PriceIndexID, std.ID) from dbo.GTMessageHospital mes
inner join dbo.GTParty intP on mes.InternalPartyID = intP.PartyID
inner join dbo.GTParty extP on mes.InternalPartyID = extP.PartyID
LEFT join [dbo].[GothamPriceIndex_Standard] std on std.Code = mes.PriceIndex
LEFT join [dbo].GothamPriceIndex_Map map on map.UserPriceIndexID = mes.PriceIndex
where mes.PriceIndex is not null
and isnull(map.PriceIndexID, std.ID) is not null


insert into dbo.GTMessage
([SchemaID]
      ,[TemplateID]
      ,[RouteID]
      ,[WorkflowID]
      ,[StartingStatusID]
      ,[Root]
      ,[CustomFields]
      ,[CustomTables]
      ,[InternalPartyID]
      ,[ExternalPartyID]
      ,[TradeID]
      ,PriceIndexID)
select [SchemaID]
      ,[TemplateID]
      ,[RouteID]
      ,[WorkflowID]
      ,[StartingStatusID]
      ,[Root]
      ,[CustomFields]
      ,[CustomTables]
      ,[InternalPartyID]
      ,[ExternalPartyID]
      ,[TradeID]
      ,heal.PriceIndexID
	  from dbo.GTMessageHospital mes
	  inner join @healed heal on mes.MessageID = heal.MessageID

end