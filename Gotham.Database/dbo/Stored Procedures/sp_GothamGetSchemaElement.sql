﻿CREATE PROCEDURE [dbo].[sp_GothamGetSchemaElement]
(
	@SchemaID int,
	@KeyID int
)
AS
BEGIN
	SET NOCOUNT ON;
	select *
	from GothamSchemaDefinition G
	where 
		G.SchemaID = @SchemaID
		and G.KeyID = @KeyID
END
	------GRANT EXECUTE ON [sp_GothamGetSchemaElement] to PUBLIC