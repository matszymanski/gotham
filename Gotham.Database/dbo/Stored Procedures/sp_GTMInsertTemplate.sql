﻿CREATE PROCEDURE [dbo].[sp_GTMInsertTemplate]
(
	@TemplateName varchar(500),
	@Template varbinary(MAX)
)
AS
BEGIN

	insert into GothamTemplate (TemplateName, Template) values (@TemplateName, @Template);

	----GRANT EXECUTE ON [sp_GTMInsertTemplate] to PUBLIC
END