﻿CREATE PROCEDURE [dbo].[sp_GothamGetRootSchemas]
(
	@SchemaTypeID int = null
)

AS
BEGIN
	SET NOCOUNT ON;
	if @SchemaTypeID is null 
	BEGIN
		select G.SchemaID, G.SchemaName
		from GothamSchemas G
	END

	if @SchemaTypeID is not null 
	BEGIN
		select G.SchemaID, G.SchemaName
		from GothamSchemas G
		where G.SchemaTypeID = @SchemaTypeID
	END
	----------GRANT EXECUTE ON sp_GothamGetRootSchemas to PUBLIC
END