﻿CREATE PROCEDURE [dbo].[sp_GTMInsertMessage]
(
	@SchemaID bigint,
	@TemplateID bigint = NULL,
	@RouteID bigint = NULL,
	@WorkflowID bigint = NULL,
	@StartingStatusID bigint = NULL,
	@Root nvarchar(max),
	@CustomFields nvarchar(max) = NULL,
	@CustomTables nvarchar(max) = NULL,
	@InternalPartyID varchar(36),
	@ExternalPartyID varchar(36),
	@TradeID bigint,
	@PriceIndex nvarchar(250) = NULL
)

AS
BEGIN

    declare @partyMapped int
    declare @priceIndexMapped int

    SELECT @partyMapped = count(1) 
    FROM (
	   SELECT PartyID FROM dbo.GTParty 
		  where PartyID = @InternalPartyID
	   union all
	   SELECT PartyID FROM dbo.GTParty 
		  where PartyID = @ExternalPartyID
    ) partyMap


    SELECT @priceIndexMapped = ID
    FROM (
	   SELECT ID 
	   FROM [dbo].[GothamPriceIndex_Standard]
	   WHERE Code = @PriceIndex
	   union
	   SELECT UserPriceIndexID
	   FROM dbo.GothamPriceIndex_Map 
	   WHERE UserPriceIndexID = @PriceIndex
    ) idx

    IF @PriceIndex is null
	   set @priceIndexMapped = -1
    

    IF @partyMapped < 2 OR @priceIndexMapped = 0 --hospitalize
    BEGIN
	   insert into GTMessageHospital
		(
			SchemaID, 
			TemplateID, 
			RouteID, 
			WorkflowID, 
			StartingStatusID, 
			[Root], 
			CustomFields, 
			CustomTables,
			InternalPartyID,
			ExternalPartyID,
			TradeID,
			PriceIndex
			) 
	values 
		(
			@SchemaID, 
			@TemplateID, 
			@RouteID, 
			@WorkflowID, 
			@StartingStatusID, 
			@Root, 
			@CustomFields, 
			@CustomTables,
			@InternalPartyID,
			@ExternalPartyID,
			@TradeID,
			@PriceIndex
		)
    END
    ELSE
    BEGIN

     If @PriceIndex is null
	   set @priceIndexMapped = null

	insert into GTMessage 
		(
			SchemaID, 
			TemplateID, 
			RouteID, 
			WorkflowID, 
			StartingStatusID, 
			[Root], 
			CustomFields, 
			CustomTables,
			InternalPartyID,
			ExternalPartyID,
			TradeID,
			PriceIndexID
			) 
	values 
		(
			@SchemaID, 
			@TemplateID, 
			@RouteID, 
			@WorkflowID, 
			@StartingStatusID, 
			@Root, 
			@CustomFields, 
			@CustomTables,
			@InternalPartyID,
			@ExternalPartyID,
			@TradeID,
			@priceIndexMapped
		)
    END
	----GRANT EXECUTE ON [sp_GTMInsertMessage] to PUBLIC
END
GO
