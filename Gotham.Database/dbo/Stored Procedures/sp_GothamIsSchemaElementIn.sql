﻿CREATE PROCEDURE [dbo].[sp_GothamIsSchemaElementIn]
(
	@SchemaID int,
	@FieldName varchar(400)
)
AS
BEGIN
	SET NOCOUNT ON;
	select *
	from GothamSchemaDefinition G
	where G.SchemaID = @SchemaID and G.Tag = @FieldName

	-----GRANT EXECUTE ON [sp_GothamIsSchemaElementIn] to PUBLIC
END