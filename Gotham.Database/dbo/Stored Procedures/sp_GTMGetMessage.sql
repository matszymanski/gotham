﻿CREATE PROCEDURE [dbo].[sp_GTMGetMessage]
(
	@MessageID as bigint
)

AS
BEGIN

	select 
		M.*,
		T.Template
	from GTMessage M
	left join GothamTemplate T on T.TemplateID = M.TemplateID
	where M.MessageID = @MessageID

	----------GRANT EXECUTE ON sp_GTMGetMessage to PUBLIC
END