﻿CREATE PROCEDURE [dbo].[sp_GTMUpdateTemplate]
(
	@TemplateID bigint,
	@TemplateName varchar(500),
	@Template varbinary(MAX)
)
AS
BEGIN

	insert into GothamTemplate_History
	select *,
	SYSDATETIMEOFFSET()
	from GothamTemplate T
	where T.TemplateID = @TemplateID

	UPDATE GothamTemplate
		set 
		TemplateName = @TemplateName,
		Template = @Template
	WHERE TemplateID = @TemplateID

	----GRANT EXECUTE ON [sp_GTMUpdateTemplate] to PUBLIC
END