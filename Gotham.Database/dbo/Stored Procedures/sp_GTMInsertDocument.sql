﻿CREATE PROCEDURE [dbo].[sp_GTMInsertDocument]
(
	@MessageID bigint,
	@GeneratedDocument varbinary(MAX),
	@DocumentName nvarchar(200),
	@PdfFile varbinary(max),
	@PdfFileName nvarchar(200)
)
AS
BEGIN

	insert into GTDocument (MessageID, GeneratedDocument, DocumentName, PdfFile, PdfFileName) 
	values (@MessageID, @GeneratedDocument, @DocumentName, @PdfFile, @PdfFileName);

	----GRANT EXECUTE ON [sp_GTMInsertDocument] to PUBLIC
END