﻿CREATE PROCEDURE [dbo].[sp_GothamDeleteSchemaElement]
(
	@SchemaID int,
	@KeyID int
)
AS
BEGIN

	DELETE GothamSchemaDefinition 
	WHERE KeyID = @KeyID and SchemaID = @SchemaID

	-----GRANT EXECUTE ON [sp_GothamDeleteSchemaElement] to PUBLIC
	--select * from GothamSchemaDefinition
END