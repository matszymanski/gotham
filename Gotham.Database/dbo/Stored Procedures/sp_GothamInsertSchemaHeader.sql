﻿CREATE PROCEDURE [dbo].[sp_GothamInsertSchemaHeader]
(
	@SchemaID int,
	@RootSchemaID int,
	@SchemaName varchar(400)
)
AS
BEGIN

	declare @rootClassName varchar(20)
	select @rootClassName = ClassName FROM GothamSchemas Where SchemaID = @RootSchemaID

	insert into GothamSchemas (SchemaID, SchemaName, SchemaTypeID, RootSchemaID, ClassName) values (@SchemaID, @SchemaName, 2, @RootSchemaID, @rootClassName);

	----GRANT EXECUTE ON [sp_GothamInsertSchemaHeader] to PUBLIC
END