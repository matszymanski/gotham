﻿CREATE PROCEDURE [dbo].[sp_GothamGetNextSchemaTagID]
(
	@SchemaID int
)

AS
BEGIN
	SET NOCOUNT ON;

	select (MAX(G.KeyID) + 1)
	from GothamSchemaDefinition G
	where G.SchemaID = @SchemaID

	--GRANT EXECUTE ON sp_GothamGetNextSchemaTagID to PUBLIC
END