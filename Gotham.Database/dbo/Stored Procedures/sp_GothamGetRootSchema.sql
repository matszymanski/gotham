﻿CREATE PROCEDURE [dbo].[sp_GothamGetRootSchema]
(
	@SchemaID int
)
AS
BEGIN
	--declare @SchemaID as int
	--set @SchemaID = 2
    --;	

	--With Roots as(select * from GothamSchemas Q where Q.SchemaID = @SchemaID)
	With Roots as(select * from GothamSchemas Q where Q.RootSchemaID is null)

	--select * from Roots
		
	select 
	G.SchemaID
	,G.KeyID
	,[Element ID] = G.KeyID
	,G.Tag
	,[Is Root Element] = G.IsFixed
	,G.IsFixed
	,GFT.FieldType
	,GFT.FieldTypeID
	,G.Columns
	--,R.SchemaID
	,R.SchemaName as RootSchemaName
	from GothamSchemaDefinition G
	left join GothamFieldType GFT on GFT.FieldTypeID = G.FieldTypeID
	left join GothamSchemas GS on GS.SchemaID = G.SchemaID
	left join Roots R on R.SchemaID = GS.RootSchemaID
	where G.SchemaID = @SchemaID
	order by G.KeyID asc

	----GRANT EXECUTE ON sp_GothamGetRootSchema to PUBLIC
END