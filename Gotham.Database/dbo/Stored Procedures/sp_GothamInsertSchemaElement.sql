﻿CREATE PROCEDURE [dbo].[sp_GothamInsertSchemaElement]
(
	@SchemaID int,
	@SchemaTag varchar(400),
	@FieldTypeID int,
	@Columns int = null
)
AS
BEGIN
	declare @TagID as int
	set @TagID = (select max(KeyID) from GothamSchemaDefinition where SchemaID = @SchemaID) + 1

	insert into GothamSchemaDefinition 
	(SchemaID,   KeyID,    [Key],        Tag,  FieldTypeID, IsFixed, Columns) values 
	(@SchemaID, @TagID, 'CUSTOM', @SchemaTag, @FieldTypeID, 0, @Columns);

	-----GRANT EXECUTE ON [sp_GothamInsertSchemaElement] to PUBLIC
END