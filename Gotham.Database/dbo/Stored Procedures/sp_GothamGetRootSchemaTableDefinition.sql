﻿CREATE PROCEDURE [dbo].[sp_GothamGetRootSchemaTableDefinition]
(
	@SchemaID int
)
AS
BEGIN
	select * from GothamSchemaDefinition D
	WHERE D.SchemaID = @SchemaID
	----GRANT EXECUTE ON [sp_GothamGetRootSchemaTableDefinition] to PUBLIC
END