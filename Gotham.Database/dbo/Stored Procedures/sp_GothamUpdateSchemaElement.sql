﻿CREATE PROCEDURE [dbo].[sp_GothamUpdateSchemaElement]
(
	@SchemaID int,
	@SchemaTag varchar(400),
	@FieldTypeID int,
	@TagID int,
	@Columns int = null
)
AS
BEGIN

	UPDATE GothamSchemaDefinition 
	SET 
		Tag = @SchemaTag,
		FieldTypeID = @FieldTypeID,
		Columns = @Columns
	WHERE GothamSchemaDefinition.SchemaID = @SchemaID and GothamSchemaDefinition.KeyID = @TagID

	-----GRANT EXECUTE ON [sp_GothamInsertSchemaElement] to PUBLIC
END