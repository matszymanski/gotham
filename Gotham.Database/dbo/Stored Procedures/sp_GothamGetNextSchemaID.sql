﻿CREATE PROCEDURE [dbo].[sp_GothamGetNextSchemaID]
AS
BEGIN
	SET NOCOUNT ON;
	select (MAX(G.SchemaID) + 1)
	from GothamSchemaDefinition G

	---GRANT EXECUTE ON sp_GothamGetNextSchemaID to PUBLIC
END