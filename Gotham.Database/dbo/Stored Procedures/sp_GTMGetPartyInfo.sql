﻿CREATE PROCEDURE [dbo].[sp_GTMGetPartyInfo]
(
	@PartyID as varchar(36)
)

AS
BEGIN

	SELECT
    p.PartyName as	EXT_COUNTERPARTY_NAME,
	p.StreetAddressLine1 as EXT_ADDRESS_LINE_1,
	p.StreetAddressLine2 as EXT_ADDRESS_LINE_2, 
	p.StreetAddressLine3 as EXT_ADDRESS_LINE_3,
	p.City as EXT_ADDRESS_CITY,
	p.Country as EXT_ADDRESS_COUNTRY,
	p.PostalCode as EXT_ADDRESS_MAIL_CODE

	from GTParty p
	where p.PartyID = @PartyID


	----------GRANT EXECUTE ON sp_GTMGetPartyInfo to PUBLIC

END