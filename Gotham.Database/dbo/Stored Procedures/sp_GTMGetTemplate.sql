﻿CREATE PROCEDURE [dbo].[sp_GTMGetTemplate]
(
	@TemplateID as bigint
)

AS
BEGIN

	select * from GothamTemplate T
	where T.TemplateID = @TemplateID

	----------GRANT EXECUTE ON sp_GTMGetTemplate to PUBLIC
END