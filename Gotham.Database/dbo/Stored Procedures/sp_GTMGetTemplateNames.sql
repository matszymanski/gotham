﻿CREATE PROCEDURE [dbo].[sp_GTMGetTemplateNames]

AS
BEGIN

	select 
		T.TemplateID,
		T.TemplateName
	from GothamTemplate T

---------GRANT EXECUTE ON sp_GTMGetTemplateNames to PUBLIC
END