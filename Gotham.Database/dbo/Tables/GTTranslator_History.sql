﻿CREATE TABLE dbo.GTTranslator_History
(
ID int identity(1,1) primary key,
Phrase nvarchar(200),
Translation nvarchar(200),
TransactionID bigint,
Timestamp datetime default(getutcdate())
)