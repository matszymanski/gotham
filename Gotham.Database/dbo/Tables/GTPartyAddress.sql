﻿CREATE TABLE [dbo].[GTPartyAddress](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PartyID] [varchar](36) NOT NULL,
	[StreetAddressLine1] [nvarchar](100) NULL,
	[StreetAddressLine2] [nvarchar](100) NULL,
	[StreetAddressLine3] [nvarchar](100) NULL,
	[StreetAddressLine4] [nvarchar](100) NULL,
	[StreetAddressLine5] [nvarchar](100) NULL,
	[City] [nvarchar](100) NULL,
	[State] [nvarchar](100) NULL,
	[Country] [nvarchar](100) NULL,
	[PostalCode] [varchar](10) NULL
) ON [PRIMARY]