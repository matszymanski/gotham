﻿
CREATE TABLE [dbo].[GTMessage](
	[MessageID] [bigint] IDENTITY(1,1) NOT NULL,
	[SchemaID] [bigint] NOT NULL,
	[TemplateID] [bigint] NULL,
	[RouteID] [bigint] NULL,
	[WorkflowID] [bigint] NULL,
	[StartingStatusID] [bigint] NULL,
	[Root] [nvarchar](max) NOT NULL,
	[CustomFields] [nvarchar](max) NULL,
	[CustomTables] [nvarchar](max) NULL,
	[InternalPartyID] [varchar](36) NULL,
	[ExternalPartyID] [varchar](36) NULL,
	[TradeID] [varchar](36) NULL,
	[InsertionTimeStamp] [datetimeoffset](7) NOT NULL,
	[PriceIndexID] int NULL,
 CONSTRAINT [PK_GTMessage] PRIMARY KEY CLUSTERED 
(
	[MessageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[GTMessage] ADD  CONSTRAINT [DF_GTMessage_InsertionTimeStamp]  DEFAULT (getutcdate()) FOR [InsertionTimeStamp]
GO

