﻿CREATE TABLE [dbo].[MatchBoxTemplate](
	[Type] [varchar](10) NOT NULL,
	[FromEmail] [varchar](100) NOT NULL,
	[FromName] [nvarchar](100) NOT NULL,
	[Subject] [nvarchar](100) NOT NULL,
	[HtmlBody] [nvarchar](max) NOT NULL,
	[TextBody] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]