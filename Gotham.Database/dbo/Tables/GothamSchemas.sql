﻿CREATE TABLE [dbo].[GothamSchemas](
	[SchemaID] [bigint] NOT NULL,
	[SchemaName] [varchar](250) NOT NULL,
	[SchemaTypeID] [int] NOT NULL,
	[RootSchemaID] [int] NULL, 
    [ClassName] VARCHAR(20) NOT NULL
) ON [PRIMARY]