﻿CREATE TABLE [dbo].[GTPartyAddress_History](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PartyID] [varchar](36) NULL,
	[StreetAddressLine1] [nvarchar](100) NULL,
	[StreetAddressLine2] [nvarchar](100) NULL,
	[StreetAddressLine3] [nvarchar](100) NULL,
	[StreetAddressLine4] [nvarchar](100) NULL,
	[StreetAddressLine5] [nvarchar](100) NULL,
	[City] [nvarchar](100) NULL,
	[State] [nvarchar](100) NULL,
	[Country] [nvarchar](100) NULL,
	[PostalCode] [varchar](10) NULL,
	[TransactionID] [bigint] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GTPartyAddress_History] ADD  DEFAULT (getutcdate()) FOR [Timestamp]