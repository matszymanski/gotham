﻿CREATE TABLE [dbo].[GothamPriceIndex_MapHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserPriceIndexID] [nvarchar](250) NULL,
	[PriceIndexID] [int] NULL,
	[TransactionID] [bigint] NULL,
	[Timestamp] [datetime] NOT NULL,
 CONSTRAINT [PK_GothamPriceIndex_MapHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GothamPriceIndex_MapHistory] ADD  CONSTRAINT [DF_GothamPriceIndex_MapHistory_Timestamp]  DEFAULT (getutcdate()) FOR [Timestamp]