﻿CREATE TABLE [dbo].[GothamSchemaDefinition](
	[SchemaID] [bigint] NOT NULL,
	[KeyID] [int] NOT NULL,
	[Key] [varchar](400) NOT NULL,
	[Tag] [varchar](400) NOT NULL,
	[FieldTypeID] [int] NOT NULL,
	[IsFixed] [bit] NOT NULL,
	[Columns] [int] NULL,
	[DisplayName] [nvarchar](400) NULL
) ON [PRIMARY]