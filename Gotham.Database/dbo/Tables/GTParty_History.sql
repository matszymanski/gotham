﻿CREATE TABLE [dbo].[GTParty_History](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PartyID] [varchar](36) NOT NULL,
	[PartyName] [nvarchar](250) NOT NULL,
	[Phone] [varchar](20) NULL,
	[Email] [varchar](100) NULL,
	[StreetAddressLine1] [nvarchar](100) NOT NULL,
	[StreetAddressLine2] [nvarchar](100) NULL,
	[StreetAddressLine3] [nvarchar](100) NULL,
	[StreetAddressLine4] [nvarchar](100) NULL,
	[StreetAddressLine5] [nvarchar](100) NULL,
	[City] [nvarchar](100) NOT NULL,
	[State] [nvarchar](100) NULL,
	[Country] [nvarchar](100) NOT NULL,
	[PostalCode] [varchar](10) NOT NULL,
	[TransactionID] [bigint] NOT NULL,
    [LEI] CHAR(20) NULL, 
    [EAN] varchar(18) NULL,
	[EIC] char(16) NULL,
	[Agreement] NVARCHAR(MAX) NULL, 
	[Timestamp] [datetime] NOT NULL,
    PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GTParty_History] ADD  DEFAULT (getutcdate()) FOR [Timestamp]