﻿CREATE TABLE [dbo].[GTConfirmationStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InternalPartyID] [varchar](36) NOT NULL,
	[TradeID] [varchar](36) NOT NULL,
	[Status] [varchar](100) NOT NULL,
	[Timestamp] [datetimeoffset](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]