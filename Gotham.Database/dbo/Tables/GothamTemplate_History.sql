﻿CREATE TABLE [dbo].[GothamTemplate_History](
	[TemplateID] [int] NOT NULL,
	[TemplateName] [varchar](400) NOT NULL,
	[Template] [varbinary](max) NOT NULL,
	[BackupTimeStamp] [datetimeoffset](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]