﻿CREATE TABLE [dbo].[GTDocumentUploaded](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TradeID] [bigint] NOT NULL,
	[InternalPartyID] [varchar](36) NOT NULL,
	[Document] [varbinary](max) NULL,
	[DocumentName] [nvarchar](200) NULL,
	[UserID] [int] NULL,
	[SourceEmail] [varchar](100) NULL,
	[AutoMatched] [bit] NOT NULL,
	[TimeStamp] [datetimeoffset](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[GTDocumentUploaded] ADD  DEFAULT ((0)) FOR [AutoMatched]
GO
ALTER TABLE [dbo].[GTDocumentUploaded] ADD  DEFAULT (getutcdate()) FOR [TimeStamp]