﻿CREATE TABLE [dbo].[MatchBoxEmailServer](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IsInbox] [bit] NOT NULL,
	[Host] [varchar](100) NOT NULL,
	[Port] [smallint] NOT NULL,
	[Ssl] [bit] NOT NULL,
	[Protocol] [varchar](4) NOT NULL,
	[Login] [varchar](100) NOT NULL,
	[Password] [varchar](256) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]