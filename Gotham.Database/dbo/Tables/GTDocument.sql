﻿
CREATE TABLE [dbo].[GTDocument](
	[MessageID] [bigint] NOT NULL,
	[GeneratedDocument] [varbinary](max) NOT NULL,
	[DocumentName] [nvarchar](200) NOT NULL,
	[PdfFile] [varbinary](max) NULL,
	[PdfFileName] [nvarchar](200) NULL,
	[CreationTimeStamp] [datetimeoffset](7) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[GTDocument] ADD  CONSTRAINT [DF_GTDocument_CreationTimeStamp]  DEFAULT (getutcdate()) FOR [CreationTimeStamp]
GO