﻿CREATE TABLE [log].[Addin](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TimeStamp] [datetime] NULL,
	[UserID] [int] NULL,
	[Context] [varchar](120) NOT NULL,
	[Level] [varchar](30) NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[TransactionID] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [log].[Addin] ADD  DEFAULT (getutcdate()) FOR [TimeStamp]