﻿
create view [api].[Party_Unaligned]
as
(
    SELECT InternalPartyID as PartyID
	   FROM [dbo].[GTMessageHospital] mes 
	   left join dbo.GTParty intPart on mes.InternalPartyID = intPart.PartyID
	   where intPart.PartyID is null
    
    union 
    
    SELECT ExternalPartyID as PartyID
	   FROM [dbo].[GTMessageHospital] mes 
	   left join dbo.GTParty extPart on mes.ExternalPartyID = extPart.PartyID
	   where extPart.PartyID is null
  )
GO
