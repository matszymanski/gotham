﻿CREATE view [api].[PriceIndex_Unaligned] as
(
    SELECT mes.PriceIndex
    FROM dbo.GTMessageHospital mes
    LEFT JOIN
    dbo.GothamPriceIndex_Standard std
    ON std.Code=mes.PriceIndex
	   LEFT JOIN
	   dbo.GothamPriceIndex_Map map
	   ON map.UserPriceIndexID=mes.PriceIndex
    WHERE isnull(std.ID, map.PriceIndexID) IS NULL
		AND mes.PriceIndex IS NOT NULL
    GROUP BY mes.PriceIndex
)