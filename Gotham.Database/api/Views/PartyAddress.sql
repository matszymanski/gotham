﻿CREATE VIEW [api].[PartyAddress] as
(
SELECT [ID]
      ,[PartyID]
      ,[StreetAddressLine1]
      ,[StreetAddressLine2]
      ,[StreetAddressLine3]
      ,[StreetAddressLine4]
      ,[StreetAddressLine5]
      ,[City]
      ,[State]
      ,[Country]
      ,[PostalCode]
  FROM [dbo].[GTPartyAddress]
)