﻿CREATE VIEW [api].[DocumentUploaded]
AS
(
  SELECT ID,
         TradeID,
         InternalPartyID,
         Document,
         DocumentName,
	    DATALENGTH(Document) as DocumentSizeBytes,
         UserID,
         SourceEmail,
         AutoMatched,
         TimeStamp
  FROM dbo.GTDocumentUploaded
);