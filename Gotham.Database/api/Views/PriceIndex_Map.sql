﻿CREATE view [api].[PriceIndex_Map] as
(
	SELECT 
		map.UserPriceIndexID, map.PriceIndexID
	from 
		dbo.GothamPriceIndex_Map map

)