﻿CREATE VIEW [api].[ConfirmationView]
AS
(
     SELECT InternalPartyID,
		   intParty.PartyName as InternalPartyName,	
		   ExternalPartyID,
		   extParty.PartyName as ExternalPartyName,
           TradeID,
           mesRanked.[SchemaID],
           [TemplateID],
           [RouteID],
           [WorkflowID],
           [StartingStatusID],
           [Root],
           [CustomFields],
           [CustomTables],
           InsertionTimeStamp,
		   ConfirmationStatus,
		   ClassName
    FROM
    (
        SELECT mes.InternalPartyID,
               mes.TradeID,
			   mes.ExternalPartyID,
               [SchemaID],
               [TemplateID],
               [RouteID],
               [WorkflowID],
               [StartingStatusID],
               [Root],
               [CustomFields],
               [CustomTables],
               InsertionTimeStamp,
			stat.Status as ConfirmationStatus,
               ROW_NUMBER() OVER(PARTITION BY mes.InternalPartyID,
                                              mes.TradeID ORDER BY MessageID DESC, stat.Timestamp DESC) rowRank
        FROM dbo.GTMessage mes
	   LEFT JOIN dbo.GTConfirmationStatus stat on stat.InternalPartyID = mes.InternalPartyID and stat.TradeID = mes.TradeID
    ) mesRanked
    INNER JOIN dbo.GothamSchemas schem on schem.SchemaID = mesRanked.SchemaID
	INNER JOIN dbo.GTParty intParty on intParty.PartyID = InternalPartyID 
	INNER JOIN dbo.GTParty extParty on extParty.PartyID = ExternalPartyID
    WHERE mesRanked.rowRank = 1
);