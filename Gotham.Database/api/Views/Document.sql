﻿CREATE VIEW [api].[Document]
AS
(
  SELECT doc.MessageID,
         mes.TradeID,
         mes.InternalPartyID,
	     templ.TemplateName,
         GeneratedDocument,
         DocumentName,
         PdfFile,
         PdfFileName,
         DATALENGTH(PdfFile) AS PdfFileSizeBytes,
         CreationTimeStamp AS TimeStamp
  FROM dbo.GTDocument doc
  INNER JOIN dbo.GTMessage mes ON mes.MessageID=doc.MessageID
  INNER JOIN dbo.[GothamTemplate] templ on templ.TemplateID = mes.TemplateID
  
);