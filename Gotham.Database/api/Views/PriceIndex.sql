﻿CREATE view [api].[PriceIndex] as
(
	SELECT 
		std.ID, std.Code, std.Description
	from 
		dbo.GothamPriceIndex_Standard std

)