﻿/****** Object:  UserDefinedTableType [api].[GTPartyAddressList]    Script Date: 01.11.2017 23:25:32 ******/
CREATE TYPE [api].[GTPartyAddressList] AS TABLE(
	[PartyID] [varchar](36) NOT NULL,
	[StreetAddressLine1] [nvarchar](100) NULL,
	[StreetAddressLine2] [nvarchar](100) NULL,
	[StreetAddressLine3] [nvarchar](100) NULL,
	[StreetAddressLine4] [nvarchar](100) NULL,
	[StreetAddressLine5] [nvarchar](100) NULL,
	[City] [nvarchar](100) NULL,
	[State] [nvarchar](100) NULL,
	[Country] [nvarchar](100) NULL,
	[PostalCode] [varchar](10) NULL
)