﻿-- =============================================
-- Author:		Mateusz
-- Create date: 15.10.2017
-- =============================================

CREATE PROCEDURE [api].[sp_GTUpdateConfirmationStatus]
	@TradeID bigint,
	@InternalPartyID varchar(36),
	@StatusID varchar(100),
	@Timestamp datetimeoffset,
	@NewStatusID varchar(100) out
AS
BEGIN
	SET NOCOUNT ON;
     

	DECLARE @TransactionID bigint;

	SELECT @TransactionID = transaction_id
	FROM sys.dm_tran_current_transaction;

	BEGIN TRY
		BEGIN TRANSACTION;
		
		INSERT INTO dbo.GTConfirmationStatus (TradeID, InternalPartyID, Status, Timestamp)
		VALUES (@TradeID, @InternalPartyID, @StatusID, @Timestamp)

		SELECT @NewStatusID = @StatusID
		Commit transaction;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
		Select TOP 1 @NewStatusID = Status FROM dbo.GTConfirmationStatus where TradeID = @TradeID and InternalPartyID = @InternalPartyID order by Timestamp desc
	END CATCH;
END