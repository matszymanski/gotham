﻿-- =============================================
-- Author:		Mateusz
-- Create date: 15.10.2017
-- =============================================

CREATE PROCEDURE [api].[sp_GTUpsertParty]
	@PartyID [varchar](36),
	@PartyName [nvarchar](250),
	@Phone [varchar](20) = null,
	@Email [varchar](100) = null,
	@StreetAddressLine1 [nvarchar](100),
	@StreetAddressLine2 [nvarchar](100) = NULL,
	@StreetAddressLine3 [nvarchar](100) = NULL,
	@StreetAddressLine4 [nvarchar](100) = NULL,
	@StreetAddressLine5 [nvarchar](100) = NULL,
	@City [nvarchar](100),
	@State [nvarchar](100) = NULL,
	@Country [nvarchar](100),
	@PostalCode [varchar](10),
	@EAN varchar(18) = NULL,
	@EIC char(16) = NULL,
	@LEI char(20) = NULL,
	@Agreement nvarchar(max) = null
AS
BEGIN
	SET NOCOUNT ON;
     SET XACT_ABORT ON;

	DECLARE @TransactionID bigint;

	SELECT @TransactionID = transaction_id
	FROM sys.dm_tran_current_transaction;

	BEGIN TRY
		BEGIN TRANSACTION;
		MERGE [dbo].[GTParty] AS target
		USING
		(
			SELECT 
			  @PartyID, 
			  @PartyName, 
			  @Phone, 
			  @Email,
			  @StreetAddressLine1,
			  @StreetAddressLine2,
			  @StreetAddressLine3,
			  @StreetAddressLine4,
			  @StreetAddressLine5,
			  @City,
			  @State,
			  @Country,
			  @PostalCode,
			  @LEI,
			  @EIC,
			  @EAN,
			  @Agreement
		) AS source(
			  PartyID, 
			  PartyName, 
			  Phone, 
			  Email,
			  StreetAddressLine1,
			  StreetAddressLine2,
			  StreetAddressLine3,
			  StreetAddressLine4,
			  StreetAddressLine5,
			  City,
			  State,
			  Country,
			  PostalCode,
			  LEI,
			  EIC,
			  EAN,
			  Agreement)
		ON(target.PartyID = source.PartyID)
		WHEN MATCHED
			  THEN UPDATE 
			  SET  
			  PartyName 		 = source.PartyName, 
			  Phone 			 = source.Phone, 
			  Email			 = source.Email,
			  StreetAddressLine1 = source.StreetAddressLine1,
			  StreetAddressLine2 = source.StreetAddressLine2,
			  StreetAddressLine3 = source.StreetAddressLine3,
			  StreetAddressLine4 = source.StreetAddressLine4,
			  StreetAddressLine5 = source.StreetAddressLine5,
			  City			 = source.City,
			  State			 = source.State,
			  Country 		 = source.Country,
			  PostalCode		 = source.PostalCode,
			  LEI = source.LEI,
			  EIC = source.EIC,
			  EAN = source.EAN,
		      Agreement = source.Agreement
		WHEN NOT MATCHED
			  THEN
			  INSERT(PartyID, 
			  PartyName, 
			  Phone, 
			  Email,
			  StreetAddressLine1,
			  StreetAddressLine2,
			  StreetAddressLine3,
			  StreetAddressLine4,
			  StreetAddressLine5,
			  City,
			  State,
			  Country,
			  PostalCode,
			  LEI,
			  EIC,
			  EAN,
			  Agreement)
			  VALUES(
			  source.PartyID, 
			  source.PartyName, 
			  source.Phone, 
			  source.Email,
			  source.StreetAddressLine1,
			  source.StreetAddressLine2,
			  source.StreetAddressLine3,
			  source.StreetAddressLine4,
			  source.StreetAddressLine5,
			  source.City,
			  source.State,
			  source.Country,
			  source.PostalCode,
			  source.LEI,
			  source.EIC,
			  source.EAN,
			  source.Agreement
			  );

		INSERT INTO dbo.GTParty_History(PartyID, 
			  PartyName, 
			  Phone, 
			  Email,
			  StreetAddressLine1,
			  StreetAddressLine2,
			  StreetAddressLine3,
			  StreetAddressLine4,
			  StreetAddressLine5,
			  City,
			  State,
			  Country,
			  PostalCode,
			  LEI,
			  EIC,
			  EAN,
			  Agreement,
			  TransactionID)
		VALUES( @PartyID, 
			  @PartyName, 
			  @Phone, 
			  @Email,
			  @StreetAddressLine1,
			  @StreetAddressLine2,
			  @StreetAddressLine3,
			  @StreetAddressLine4,
			  @StreetAddressLine5,
			  @City,
			  @State,
			  @Country,
			  @PostalCode, 
			  @LEI,
			  @EIC,
			  @EAN,
			  @Agreement,
			  @TransactionID );
			  
		COMMIT TRANSACTION;
		exec dbo.sp_GTHealMessages
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
	END CATCH;
END