﻿-- =============================================
-- Author:		Mateusz
-- Create date: 15.10.2017
-- =============================================

CREATE PROCEDURE [api].[sp_GTUpsertPartyAddress]
	@AddressList api.GTPartyAddressList readonly
AS
BEGIN
	SET NOCOUNT ON;
     SET XACT_ABORT ON;

	DECLARE @TransactionID bigint;

	SELECT @TransactionID = transaction_id
	FROM sys.dm_tran_current_transaction;

	BEGIN TRY
		BEGIN TRANSACTION;
		MERGE [dbo].[GTPartyAddress] AS target
		USING
		(
			SELECT [PartyID]
				  ,[StreetAddressLine1]
				  ,[StreetAddressLine2]
				  ,[StreetAddressLine3]
				  ,[StreetAddressLine4]
				  ,[StreetAddressLine5]
				  ,[City]
				  ,[State]
				  ,[Country]
				  ,[PostalCode] From @AddressList
		) AS source( [PartyID]
				  ,[StreetAddressLine1]
				  ,[StreetAddressLine2]
				  ,[StreetAddressLine3]
				  ,[StreetAddressLine4]
				  ,[StreetAddressLine5]
				  ,[City]
				  ,[State]
				  ,[Country]
				  ,[PostalCode])
		ON(target.PartyID = source.PartyID)
		WHEN MATCHED
			  THEN UPDATE 
			  SET   [StreetAddressLine1] = source.[StreetAddressLine1]
				  ,[StreetAddressLine2] = source.[StreetAddressLine2]
				  ,[StreetAddressLine3] = source.[StreetAddressLine3]
				  ,[StreetAddressLine4] = source.[StreetAddressLine4]
				  ,[StreetAddressLine5] = source.[StreetAddressLine5]
				  ,[City]			    = source.[City]
				  ,[State]		    = source.[State]
				  ,[Country]		    = source.[Country]
				  ,[PostalCode]	    = source.[PostalCode]
		WHEN NOT MATCHED
			  THEN
			  INSERT([PartyID]
				  ,[StreetAddressLine1]
				  ,[StreetAddressLine2]
				  ,[StreetAddressLine3]
				  ,[StreetAddressLine4]
				  ,[StreetAddressLine5]
				  ,[City]
				  ,[State]
				  ,[Country]
				  ,[PostalCode])
			  VALUES(source.[PartyID]
				  ,source.[StreetAddressLine1]
				  ,source.[StreetAddressLine2]
				  ,source.[StreetAddressLine3]
				  ,source.[StreetAddressLine4]
				  ,source.[StreetAddressLine5]
				  ,source.[City]
				  ,source.[State]
				  ,source.[Country]
				  ,source.[PostalCode]);

		INSERT INTO dbo.GTPartyAddress_History(
				   [PartyID]
				  ,[StreetAddressLine1]
				  ,[StreetAddressLine2]
				  ,[StreetAddressLine3]
				  ,[StreetAddressLine4]
				  ,[StreetAddressLine5]
				  ,[City]
				  ,[State]
				  ,[Country]
				  ,[PostalCode],
				   TransactionID)
		    SELECT [PartyID]
					 ,[StreetAddressLine1]
					 ,[StreetAddressLine2]
					 ,[StreetAddressLine3]
					 ,[StreetAddressLine4]
					 ,[StreetAddressLine5]
					 ,[City]
					 ,[State]
					 ,[Country]
					 ,[PostalCode]
					 ,@TransactionID 
		    From @AddressList
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
	END CATCH;
END