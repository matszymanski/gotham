﻿create procedure [api].[sp_GTUploadDocument]
    @TradeID varchar(36),
    @InternalPartyID varchar(36),
    @Document varbinary(max),
    @DocumentName nvarchar(200),
    @UserID int = null,
    @SourceEmail varchar(100) = null,
    @AutoMatched bit = 0
AS
BEGIN

    SET NOCOUNT ON;
     	
    INSERT INTO dbo.GTDocumentUploaded (TradeID, InternalPartyID, Document, DocumentName, UserID, SourceEmail, AutoMatched)
    VALUES (@TradeID, @InternalPartyID, @Document, @DocumentName, @UserID, @SourceEmail, @AutoMatched)

		
END