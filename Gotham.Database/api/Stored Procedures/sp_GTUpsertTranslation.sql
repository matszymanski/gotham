﻿
CREATE PROCEDURE api.sp_GTUpsertTranslation
(
    @Phrase nvarchar(200),
    @Translation nvarchar(200)
)
AS 
BEGIN
     SET NOCOUNT ON;
     SET XACT_ABORT ON;

	DECLARE @TransactionID bigint;

	SELECT @TransactionID = transaction_id
	FROM sys.dm_tran_current_transaction;

	BEGIN TRY
		BEGIN TRANSACTION;
		MERGE [dbo].GTTranslator AS target
		USING
		(
			SELECT 
			  @Phrase,
			  @Translation
		) AS source(
			  Phrase, 
			  Translation)
		ON(target.Phrase = source.Phrase)
		WHEN MATCHED
			  THEN UPDATE 
			  SET  
			  Translation 		 = source.Translation
		WHEN NOT MATCHED
			  THEN
			  INSERT(Phrase, 
			  Translation)
			  VALUES(
			  source.Phrase, 
			  source.Translation
			  );
	   INSERT INTO dbo.GTTranslator_History(Phrase, 
			  Translation,
			  TransactionID)
		VALUES( @Phrase, 
			  @Translation,
			  @TransactionID );
			  
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
	END CATCH;
END
GO

