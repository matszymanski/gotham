﻿CREATE PROCEDURE [api].[sp_GTUpdatePriceIndexMap] 
				@UserPriceIndexID nvarchar(250),
				@PriceIndexID int
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @TransactionID bigint;

	SELECT @TransactionID = transaction_id
	FROM sys.dm_tran_current_transaction;
	BEGIN TRY
		BEGIN TRANSACTION;
		MERGE [dbo].[GothamPriceIndex_Map] AS target
		USING
		(
			SELECT @UserPriceIndexID, @PriceIndexID
		) AS source(UserPriceIndexID, PriceIndexID)
		ON(target.UserPriceIndexID = source.UserPriceIndexID)
		WHEN MATCHED
			  THEN UPDATE SET PriceIndexID = source.PriceIndexID
		WHEN NOT MATCHED
			  THEN
			  INSERT(UserPriceIndexID, PriceIndexID)
			  VALUES( source.UserPriceIndexID, source.PriceIndexID );
		INSERT INTO dbo.GothamPriceIndex_MapHistory( UserPriceIndexID, PriceIndexID, TransactionID )
		VALUES( @UserPriceIndexID, @PriceIndexID, @TransactionID );

		COMMIT TRANSACTION;
		exec dbo.sp_GTHealMessages
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION;
	END CATCH;
END;