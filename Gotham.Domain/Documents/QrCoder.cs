﻿using Gotham.Domain.Confirmation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Gotham.Domain.Documents
{
    public class QrCoder
    {

        private const string _qrFormat = "GTMatchBox Trade:{0} Party:{1}";
        private static Regex _qrRegex = new Regex("^GTMatchBox Trade:(.+) Party:(.+)$");

        private TradeUniqueKey _tradeKey;

        public QrCoder(TradeUniqueKey tradeKey)
        {
            _tradeKey = tradeKey;
        }
        

        public string AsString()
        {
            return string.Format(_qrFormat, _tradeKey.TradeID, _tradeKey.InternalPartyID);
        }

        public static TradeUniqueKey Parse(string text)
        {
            var match = _qrRegex.Match(text);
            if (match.Success)
                return TradeUniqueKey.Create(match.Groups[1].Value, match.Groups[2].Value);

            return null;
        }
    }
}
