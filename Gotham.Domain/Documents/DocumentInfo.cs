﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Documents
{
    public class DocumentInfo
    {
        public long MessageID { get; set; }

        public string DocumentName { get; set; }

        public string PdfFileName { get; set; }

        public long PdfFileSizeBytes { get; set; }

        public string TemplateName { get; set; }

        public DateTimeOffset TimeStamp { get; set; }
    }
}
