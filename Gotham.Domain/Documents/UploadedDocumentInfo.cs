﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Documents
{
    public class UploadedDocumentInfo
    {
        public long ID { get; set; }

        public string DocumentName { get; set; }

        public long DocumentSizeBytes { get; set; }

        public int? UserID { get; set; }

        public string SourceEmail { get; set; }

        public bool AutoMatched { get; set; }

        public DateTimeOffset TimeStamp { get; set; }
    }
}
