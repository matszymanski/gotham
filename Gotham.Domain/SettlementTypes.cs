﻿namespace Gotham.Domain.Commodity
{
    public enum SettlementTypes
    {
        Financial,
        Physical
    }
}