﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain
{
    static public class Constants
    {
        public const string LegalEntitiesUAT = "Legal Entities UAT";
        public const string LegalEntities = "Legal Entities";
        public const string BusinessUnits = "Business Units";
        public const string PowerFutures = "Power Futures";
        public const string GasFutures = "Gas Futures";
        public const string CashTrades = "Cash Trades";
        public const string EODSnapshot = "EOD Snapshot";
        public const string EODMtM = "EOD MtM";
        public const string MtM = "MtM";
        public const string EODPnL = "EOD PnL";
        public const string PnLExplained = "PnL-Explained";
        public const string BNPOpenPos = "BNP-OpenPos";
        public const Int32 LegalEntityID = 1;
        public const Int32 BusinessUnitID = 2;
        public const Int32 CashTradesID = 3;
        public const Int32 CalculationPeriodMonthly = 3;
        public const string Associated = "Associated";
        public const string NOERROR = "No Error";
    }

    static public class AcesMessages
    {
        public const string Trades = "Trades";
        public const string Schedules = "Schedules";
        public const string DownloadFolder = "DownloadFolder";
        public const string ArchiveFolder = "ArchiveFolder";
    }

    static public class eCon
    {
        public const Int32 PowerIndexDefault = 600;
        public const Int32 InsideFerc = 309;
        public const Int32 GasDaily = 303;
    }
    static public class OTCSetType
    {
        public const string OTCSettlementTypeId = "OTCSettlementTypeId";
        public const string SettlementType = "SettlementType";
        public const Int32 Cash = 2134213;
        public const Int32 Fee = 2134214;
        public const Int32 Brokerage = 2134218;
        public const Int32 OptionPremium = 2134219;
    }

    static public class InvoiceType
    {
        public const string OptionPremium = "Option Premium";
    }

    static public class EvtStatus
    {
        public const Int32 Known = 1;
        public const Int32 Unknown = 2;
    }


    static public class PrPub
    {
        public const Int32 GasDaily = 17;
        public const Int32 InsideFerc = 18;
        public const Int32 NYMEX = 15;
        public const Int32 ICE = 8;

        public const Int32 PJM = 16;
        public const Int32 MISO = 11;
    }

    static public class UI
    {
        public const string Name = "Name";
    }

    static public class ExRecInfo
    {
        public const string BSC = "BSC";
        public const string BSCONLY = "BSC ONLY";
        public const string CLEARER = "CLEARER";


        public const string BNP = "BNP";
        public const string BNPONLY = "BNP ONLY";
        public const string MATCHSTATUS = "MATCH STATUS";
        public const string SOURCE = "SOURCE";

        public const string MATCHED = "MATCHED";
        public const string BREAK = "BREAK";
        public const string RowStatus = "RowStatus";
    }

    static public class OpenPosRep
    {
        public const string ImportID = "ImportID";
        public const string COB = "COB";
        public const string EXCHANGE = "EXCHANGE";
        public const string PARTY = "PARTY";
        public const string ACCOUNT = "ACCOUNT";
        public const string PRODUCT_TYPE = "PRODUCT_TYPE";
        public const string PRODUCT = "PRODUCT";
        public const string EXCHANGE_CODE = "EXCHANGE_CODE";
        public const string BUY_SELL = "BUY_SELL";
        public const string BROKER = "BROKER";
        public const string CALL_PUT = "CALL_PUT";
        public const string MAT_MONTH = "MAT_MONTH";
        public const string MAT_YEAR = "MAT_YEAR";
        public const string MAT_DATE = "MAT_DATE";
        public const string QTY = "QTY";
        public const string TRADE_PRICE = "TRADE_PRICE";
        public const string SETT_PRICE = "SETT_PRICE";
        public const string OTE = "OTE";
        public const string VM = "VM";
        public const string CONVERTED_PNL = "CONVERTED_PNL";
        public const string NOV = "NOV";
        public const string CLEARER = "CLEARER";
        public const string INTERMEDIARY = "INTERMEDIARY";
        public const string PRODUCT_DESCRIPTION = "PRODUCT_DESCRIPTION";
        public const string PHISYCAL_SETTLEMENT = "PHISYCAL_SETTLEMENT";
        public const string STRIKE = "STRIKE";
        public const string ORIGINAL_CURRENCY = "ORIGINAL_CURRENCY";
        public const string DISCOUNT_FACTOR = "DISCOUNT_FACTOR";
        public const string FX_RATE = "FX_RATE";
        public const string CONVERSION_CURRENCY = "CONVERSION_CURRENCY";
        public const string PREMIUM = "PREMIUM";
        public const string ORIGINAL_CURRENCY2 = "ORIGINAL_CURRENCY2";
        public const string FX_RATE2 = "FX_RATE2";
        public const string CONVERTED_PREMIUM = "CONVERTED_PREMIUM";
        public const string CONVERSION_CURRENCY2 = "CONVERSION_CURRENCY2";
        public const string ORIGINAL_CURRENCY3 = "ORIGINAL_CURRENCY3";
        public const string FX_RATE3 = "FX_RATE3";
        public const string CONVERTED_NOV = "CONVERTED_NOV";
        public const string CONVERSION_CURRENCY3 = "CONVERSION_CURRENCY3";
        public const string SPAN_COMBINED_COMMODITY = "SPAN_COMBINED_COMMODITY";
        public const string EXCHANGE_PRODUCT_DESCRIPTION = "EXCHANGE_PRODUCT_DESCRIPTION";
        public const string ORIGIN = "ORIGIN";
        public const string OP_DATE = "OP_DATE";
        public const string TRADE_TYPE = "TRADE_TYPE";
        public const string COMMISSION_TYPE = "COMMISSION_TYPE";

        public const string FIRST_NOTICE_DATE = "FIRST_NOTICE_DATE";
        public const string LAST_NOTICE_DATE = "LAST_NOTICE_DATE";
        public const string DELIVERY_DATE = "DELIVERY_DATE";

        public const string VENUE_SESSION = "VENUE_SESSION";
        public const string VENUE_SESSION_DESCRIPTION = "VENUE_SESSION_DESCRIPTION";
        public const string GLOBEX_NUMBER = "GLOBEX_NUMBER";
        public const string GLOBEX_SESSION = "GLOBEX_SESSION";

        public const string INTERNAL_TRADE_ID = "INTERNAL_TRADE_ID";
        public const string CLEARING_ID = "CLEARING_ID";
        public const string EXCHANGE_ID = "EXCHANGE_ID";
        public const string EXECUTING_BROKER = "EXECUTING_BROKER";


        public const string EXEC_FEES = "EXEC_FEES";
        public const string CLEARING_FEES = "CLEARING_FEES";
        public const string NFA_FEES = "NFA_FEES";
        public const string CLT_MKT_REGIS_FEES = "CLT_MKT_REGIS_FEES";
        public const string CLRG_HOUSE_FEES = "CLRG_HOUSE_FEES";
        public const string TOTAL_FEES = "TOTAL_FEES";
        public const string CURRENCY = "CURRENCY";
        public const string TRADE_DESCRIPTION = "TRADE_DESCRIPTION";
        public const string TRANSACTION_DESCRIPTION = "TRANSACTION_DESCRIPTION";


        public const string INTERNAL_ALLOCATION_ID = "INTERNAL_ALLOCATION_ID";
        public const string UOM = "UOM";
        public const string CONTRACT_SIZE = "CONTRACT_SIZE";
        public const string VOLUME = "VOLUME";
        public const string NOTIONAL_VALUE = "NOTIONAL_VALUE";
        public const string TRANSACTION_TYPE = "TRANSACTION_TYPE";
        public const string OPERATION_DATE = "OPERATION_DATE";
        public const string TRADER = "TRADER";
        public const string OPERATION = "OPERATION";
    }

    static public class AutoRec
    {
        public const string OpenPos = "Open_Positions_BSC_";
        public const string OpenPosDetailed = "Detailed_Open_Pos_BSC_";
        public const string DetailedCFI = "Detailed_CFI_Trades_BSC_";
        public const string ExpiredTrades = "Expiry_Report__BSC_";
        public const string CancelledTrades = "Cancelled_Trades_BSC_";
    }

    static public class PriceGranularity
    {
        public const string Monthly = "Monthly";
        public const string Daily = "Daily";
    }

    static public class ValuationControl
    {
        public const string ValuationControlID = "ValuationControlID";
        public const string ValuationTypeID = "ValuationTypeID";
        public const string UpdateTimestamp = "UpdateTimestamp";
        public const string UpdateUser = "UpdateUser";
        public const string CreatedBy = "CreatedBy";
        public const string Deleted = "Deleted";
        public const string CurrentValuationDate = "CurrentValuationDate";
        public const string PreviousValuationDate = "PreviousValuationDate";
    }

    static public class ExReco
    {
        public const string ExRecControlID = "ExRecControlID";
        public const string ValuationTypeID = "ValuationTypeID";
        public const string UpdateTimestamp = "UpdateTimestamp";
        public const string UpdateUser = "UpdateUser";
        public const string CreatedBy = "CreatedBy";
        public const string Deleted = "Deleted";
        public const string CurrentValuationDate = "CurrentValuationDate";
        public const string PreviousValuationDate = "PreviousValuationDate";
    }

    static public class ValuationType
    {
        public const Int32 EOD = 2134213;
        public const Int32 IntraDay = 2134214;
    }

    static public class FeesEvent
    {
        public const string TradeEventID = "TradeEventID";
        public const string EventSubTypeId = "EventSubTypeId";
        public const string FeeType = "Fee Type";
        public const string Amount = "Amount";
        public const string NewAmount = "New Amount";
    }

    static public class Underlying
    {
        public const Int32 Gas = 3;
        public const Int32 Power = 6;
        public const Int32 Cash = 8;
    }

    static public class PersonnelFunction
    {
        public const Int32 SecurityOfficer = 1;
        public const Int32 RiskManager = 2;
        public const Int32 Trader = 4;
        public const Int32 BackOffice = 5;
        public const Int32 Management = 6;
        public const Int32 AccountingUser = 7;
        public const Int32 Quant = 8;
        public const Int32 TechSupport = 9;
        public const Int32 Sales = 1001;
        public const Int32 LegalAndCompliance = 1002;
    }

    static public class CalculationPeriod
    {
        public const Int32 Daily = 1;
        public const Int32 Weekly = 2;
        public const Int32 Monthly = 3;
        public const Int32 Yearly = 4;
        public const Int32 Term = 5;
    }

    static public class Reports
    {
        public const string ReportID = "ReportID";
        public const string ReportName = "ReportName";
        public const Int32 SnapShot = 1;
        public const Int32 MtM = 2;
    }

    static public class ReportType
    {
        public const Int32 EOD = 1;
    }

    static public class BrokerRatesTypes
    {
        public const Int32 LumpSum = 1;
        public const Int32 PerTradeNotional = 2;
        public const Int32 PerLot = 3;
        public const Int32 Override = 2134214;
    }

    static public class BackOfficeNavigatorItems
    {
        public const string LegalEntities = "Legal Entities";
        public const string BusinessUnits = "Business Units";

        public const string ngBU = "ngBU";
        public const string ngLE = "ngLE";
    }

    static public class Seasons
    {
        public const string SeasonName = "SeasonName";
        public const string SeasonID = "SeasonID";
        public const string StartMonth = "StartMonth";
        public const string MonthsSpan = "MonthsSpan";

        public const string StartDate = "StartDate";
        public const string EndDate = "EndDate";

        public const Int32 CustomPower = 11;
        public const Int32 CustomGas = 10;
    }

    static public class OpForm
    {
        public const string PowerPhysical = "PPHYS";
        public const string PowerSwap = "PSWAP";
        public const string GasSwap = "GSWAP";
    }
    static public class ApplicationMode
    {
        public const string LegalEntitiesUAT = "Legal Entities UAT";
        public const string LegalEntities = "Legal Entities";
        public const string BusinessUnits = "Business Units";
        public const string BeaStraHospital = "BeaStra Hospital";
        public const string PowerFutures = "Power Futures";
        public const string PowerPhysical = "Power Physical";
        public const string PowerSwaps = "Power Swaps";
        public const string GasSwaps = "Gas Swaps";
        public const string GasFutures = "Gas Futures";
        public const string Portfolios = "Portfolios";
        public const string ExchangeProd = "Exchange Products";
        public const string CurveDef = "Curve Definition";
        public const string PricingRef = "Pricing Reference";
        public const string ExchangeFees = "Exchange Fees";
        public const string OTCPriceReference = "OTC Price Reference";
        public const string Options = "Options";
        public const string ShellTrades = "Shell Trades";
        public const string SwapSpread = "Swap Spread";
        public const string PowerLocations = "Power Locations";
        public const string GasLocations = "Gas Locations";
        public const string ConfirmationLight = "Confirmations Light";
        public const string ConfirmationLightOverview = "Confirmations Overview";
        public const string SchedulingLight = "Scheduling Light";
        public const string SchedulingPortfolio = "Scheduling Portfolio";
        public const string ExchangeContract = "Contracts Template";
        public const string ExRecReports = "Exchange Reconciliation Reports";

        public const string OpenPos = "OpenPos";
        public const string OpenPosDetails = "OpenPosDetails";
        public const string CFITrades = "CFITrades";
        public const string ExpiringDeals = "ExpiringDeals";
        public const string CancelledDeals = "CancelledDeals";
        public const string CashTrades = "Cash Trades";
        public const string Invoice = "Invoices";


        public const string Confirmations = "Confirmations";
        public const string Scheduling = "Scheduling";
        public const string ConfirmationsOverview = "Confirmations Overview";

    }

    static public class ConfLightInfo
    {
        public const string Route = "Route";
        public const string TradeID = "TradeID";
        public const string TradeType = "TradeType";
        public const string Commodity = "Commodity";
        public const string StructureID = "StructureID";
        public const string Email = "Email";
        public const string IsOutOfScope = "IsOutOfScope";
    }

    static public class AcesFtpSettings
    {
        public const string ID = "ID";
        public const string Account = "Account";
        public const string Username = "Username";
        public const string Password = "Password";
        public const string TradePath = "TradePath";
        public const string SchedulePath = "SchedulePath";
    }

    static public class DefaultSettings
    {
        public const Int32 DefaultExecutionBrokerPowerFutures = 1;
        public const Int32 DefaultClearingBrokerPowerFutures = 2;
        public const Int32 DefaultInternalBU = 3;
        public const Int32 DefaultHolidayCalendar = 4;
        public const Int32 DefaultFuturePricingModel = 5;
        public const Int32 DefaultExRecPath = 6;
        public const Int32 DefaultInvoicePath = 7;
        public const Int32 RootSettlementFolder = 8;
        public const Int32 ArchiveSettlementFolder = 9;
        public const Int32 HospitalSettlementFolder = 10;
        public const Int32 DefaultNavisionPath = 11;

        public const Int32 ConfirmationRootFolder = 15;
        public const Int32 IsSharePointInUse = 16;
        public const Int32 ConfirmationFolder = 17;
        public const Int32 ConfirmationEmailText = 18;
        public const Int32 ConfirmationSenderEmail = 19;
        public const Int32 SchedulingOutbound = 20;
        public const Int32 SchedulingInbound = 21;
        public const Int32 SchedulingUploadBatSC = 22;
        public const Int32 ConfirmationQRVerticalOffset = 23;
        public const Int32 ConfirmationQRVerticalOffsetFirstPage = 24;
    }

    static public class CommonFormats
    {
        public const string DateTime = "DateTime";
        public const string Int32 = "Int32";
        public const string String = "String";
        public const string Decimal = "Decimal";
        public const string Currency = "Currency";
    }

    static public class TradeTypeIDS
    {
        public const Int32 Futures = 1;
        public const Int32 PowerPhys = 4;
        public const Int32 Swap = 3;
    }

    static public class AddressTypes
    {
        public const Int32 Confirmation = 1;
        public const Int32 Invoicing = 2;
        public const Int32 Main = 3;
    }
    static public class NoYes
    {
        public const Int32 No = 1;
        public const Int32 Yes = 2;
    }
    static public class PartyStatus
    {
        public const Int32 AuthPending = 1;
        public const Int32 Authorised = 2;
        public const Int32 Suspended = 3;
        public const Int32 AmendmentPending = 4;
        public const Int32 CancelAmendment = 5;
    }

    static public class Evaluation
    {
        public const string CurveSnapshotID = "Curve Snapshot ID";
        public const string CurveTimeStamp = "Curve Time Stamp";
        public const string Notional = "Notional";
        public const string CurvePrice = "Curve Price";
        public const string OverridePrice = "Override Price";
        public const string TradePrice = "Trade Price";
        public const string CurveEvaluation = "Curve Evaluation";
        public const string OverrideEvaluation = "Override Evaluation";

        public const string CurveSnapshotIDID = "CurveSnapshotID";
        public const string Granularity = "Granularity";
        public const string Commodity = "Commodity";
        public const string Electricity = "Electricity";
        public const string NaturalGas = "Natural Gas";
    }

    static public class ClearingAccount
    {
        public const string ClearingPartyID = "ClearingPartyID";
        public const string ClearingAccountID = "ClearingAccountID";
        public const string ClearingAccountNumber = "ClearingAccountNumber";
    }

    static public class ClearingSubAccount
    {
        public const string ClearingSubAccountID = "ClearingSubAccountID";
        public const string ClearingAccountID = "ClearingAccountID";
        public const string ClearingSubAccountNumber = "ClearingSubAccountNumber";
    }

    static public class StructureDetails
    {
        public const string SeasonId = "SeasonId";
        public const string StartDate = "StartDate";
        public const string StopDate = "StopDate";
        public const string LotSize = "Lot Size";
        public const string NewVolume = "New Volume";
        public const string ExchangeContractID = "ExchangeContractID";
        public const string FirstDeliveryDate = "FirstDeliveryDate";
        public const string LastDeliveryDate = "LastDeliveryDate";
        public const string ContractSize = "ContractSize";
    }

    static public class EPInfo
    {
        public const Int32 ExchangeTradedOption = 2;
        public const Int32 Power = 6;

        public const string ExchangeProductId = "ExchangeProductId";
        public const string ExchangeProductCode = "ExchangeProductCode";
        public const string BeaconValue = "BeaconValue";
        public const string Name = "Name";
        public const string Mini = "Mini";
        public const string GUIDescription = "GUIDescription";
        public const string UnderlyingId = "UnderlyingId";
        public const string PricingModelId = "PricingModelId";
        public const string ExchangeId = "ExchangeId";
        public const string LotSize = "LotSize";
        public const string LotSizeUnitId = "LotSizeUnitId";
        public const string CurrencyId = "CurrencyId";
        public const string TickSize = "TickSize";
        public const string LocationId = "LocationId";
        public const string PowerPricebandId = "PowerPricebandId";
        public const string TradingCalendarId = "TradingCalendarId";
        public const string SettlementTypeId = "SettlementTypeId";
        public const string OptionTypeId = "OptionTypeId";
        public const string FuturesCurveId = "FuturesCurveId";
        public const string ForwardCurveId = "ForwardCurveId";
        public const string PricingReferenceId = "PricingReferenceId";
        public const string BasisContract = "BasisContract";
        public const string BasisReferenceId = "BasisReferenceId";
        public const string BasisForwardCurveId = "BasisForwardCurveId";
        public const string TradeTypeId = "TradeTypeId";
        public const string BrokerProductName = "BrokerProductName";
        public const string BrokerProductCode = "BrokerProductCode";
        public const string SpanCombinedCommodity = "SpanCombinedCommodity";
        public const string ExchangeProductDescription = "ExchangeProductDescription";
        public const string VersionNumber = "VersionNumber";
        public const string CreationTimestamp = "CreationTimestamp";
        public const string UpdateTimestamp = "UpdateTimestamp";
        public const string UpdateUser = "UpdateUser";
        public const string Deleted = "Deleted";

        public const string ExchangeContractId = "ExchangeContractId";
        public const string ContractCode = "ContractCode";
        public const string IsDuplicate = "IsDuplicate";
        public const string OldID = "OldID";
    }

    static public class EPImport
    {
        public const string ExchangeProduct = "ExchangeProduct";
        public const string ContractCode = "ContractCode";
        public const string ContractDate = "ContractDate";
        public const string ContractPeriod = "ContractPeriod";
        public const string FirstTradeDate = "FirstTradeDate";
        public const string LastTradeDate = "LastTradeDate";
        public const string SettlementDate = "SettlementDate";
        public const string ExpiryDate = "ExpiryDate";
        public const string FirstDeliveryDate = "FirstDeliveryDate";
        public const string LastDeliveryDate = "LastDeliveryDate";
        public const string ContractSize = "ContractSize";
        public const string PeakHours = "PeakHours";
        public const string OffpeakHours = "OffpeakHours";
    }

    static public class TradeInfo
    {
        public const Int32 BNPID = 6;
        public const string DealId = "Deal Id";
        public const string TradeId = "TradeId";
        public const string ExchangeId = "ExchangeId";
        public const string ExchangeProductId = "ExchangeProductId";
        public const string ExchangeContractId = "ExchangeContractId";
        public const string ExecutionTypeId = "ExecutionTypeId";
        public const string ExecutionTypeID = "ExecutionTypeID";
        public const string ContractCodeId = "ContractCodeId";
        public const string Position = "Position";
        public const string Price = "Price";
        public const string BrokerID = "BrokerID";
        public const string RateTypeID = "RateTypeID";
        public const string MiniBrokerRateTypeID = "MiniBrokerRateTypeID";
        public const string RateCurrencyID = "RateCurrencyID";
        public const string FeeRate = "FeeRate";
        public const string MiniFeeRate = "MiniFeeRate";
        public const string Trader = "Trader";
        public const string Product = "Product";

        public const string ClearingBrokerId = "ClearingBrokerId";
        public const string ClearingBrokerRateTypeId = "ClearingBrokerRateTypeId";
        public const string ClearingBrokerCCYId = "ClearingBrokerCCYId";
        public const string ClearingBrokerAmount = "ClearingBrokerAmount";
        public const string ContractSize = "ContractSize";
        public const string CurrencyID = "CurrencyID";
        public const string CurveSnapshotID = "CurveSnapshotID";
        public const string CurvePrice = "Curve Price";
        public const string CurveTimeStamp = "CurveTimeStamp";
        public const string CreateTimestamp = "CreateTimestamp";
        public const string FeePerLot = "FeePerLot";
        public const string ClearingHouseRate = "ClearingHouseRate";
        public const string MarketRegistrationRate = "MarketRegistrationRate";
        public const string Mini = "Mini";
        public const string CurrencyId = "CurrencyId";
        public const string ExecutingBrokerId = "ExecutingBrokerId";
        public const string ExecutionBrokerRateTypeId = "ExecutionBrokerRateTypeId";
        public const string ExecutionBrokerCCYId = "ExecutionBrokerCCYId";
        public const string ExecutionBrokerAmount = "ExecutionBrokerAmount";
        public const string TradeDate = "TradeDate";
        public const string InternalBusinessUnitId = "InternalBusinessUnitId";
        public const string InternalPortfolioId = "InternalPortfolioId";
        public const string InternalSubportfolioId = "InternalSubportfolioId";
        public const string ExternalBusinessUnidId = "ExternalBusinessUnidId";

        public const string PeakHours = "PeakHours";
        public const string OffpeakHours = "OffpeakHours";

        public const string Reference = "Reference";
        public const string TraderComments = "TraderComments";
        public const string TradeStatusId = "TradeStatusId";
        public const string TradeStatus = "TradeStatus";
        public const string TradeTypeId = "TradeTypeId";
        public const string TradeType = "TradeType";
        public const string TradeTime = "TradeTime";
        public const string TraderId = "TraderId";
        public const string SalesPersonId = "SalesPersonId";
        public const string SettlementTypeId = "SettlementTypeId";
        public const string StructureID = "StructureID";
        public const string LotSizeUnitId = "LotSizeUnitId";
        public const string UnderlyingId = "UnderlyingId";
        public const string UpdateUser = "UpdateUser";
        public const string Username = "Username";
        public const string VersionNumber = "VersionNumber";

        public const string ClearingAccount = "ClearingAccount";
        public const string ClearingSubAccount = "ClearingSubAccount";
    }

    static public class TradeStatus
    {
        public const Int32 New = 1;
        public const Int32 Validated = 2;
        public const Int32 Cancelled = 3;
        public const Int32 Deleted = 4;
        public const Int32 Matured = 5;
        public const Int32 Template = 6;
        public const Int32 AmendmendPending = 7;
        public const Int32 CancelPending = 8;
        public const Int32 Proposed = 9;
    }
    static public class Pers
    {
        public const string Personnel = "Personnel";
        public const string PersonnelID = "PersonnelID";
    }

    static public class PortType
    {
        public const string PortfolioType = "PortfolioType";
        public const string PortfolioTypeID = "PortfolioTypeID";
    }

    static public class PortBusinessGroup
    {
        public const string PortfolioGroup = "PortfolioGroup";
        public const string PortfolioGroupID = "PortfolioGroupID";
    }

    static public class PortArea
    {
        public const string BusinessArea = "BusinessArea";
        public const string BusinessAreaID = "BusinessAreaID";
    }

    static public class PortLine
    {
        public const string PortfolioBusinessLine = "PortfolioBusinessLine";
        public const string PortfolioBusinessLineID = "PortfolioBusinessLineID";
    }

    static public class PortStrat
    {
        public const string PortfolioStrategy = "PortfolioStrategy";
        public const string PortfolioStrategyID = "PortfolioStrategyID";
    }

    static public class PortInfo
    {
        public const string Portfolio = "Portfolio";
        public const string PortfolioOwnerID = "PortfolioOwnerID";
        public const string PortfolioGroupID = "PortfolioGroupID";
        public const string PortfolioTypeID = "PortfolioTypeID";
        public const string BusinessAreaID = "BusinessAreaID";
        public const string PortfolioBusinessLineID = "PortfolioBusinessLineID";
        public const string PortfolioStrategyID = "PortfolioStrategyID";
    }

    static public class TradeEventStatus
    {
        public const Int32 Known = 1;
        public const Int32 Unknown = 2;
    }

    static public class PartyHI
    {
        public const string PartyId = "PartyId";
        public const string PartyID = "PartyID";
        public const string Party_ID = "Party ID";
        public const string ShortName = "ShortName";
        public const string LongName = "LongName";
        public const string SpiderId = "SpiderId";
        public const string PartyClassDesc = "PartyClassDesc";
        public const string PartyClassId = "PartyClassId";
        public const string InternalExternal = "Name";
        public const string InternalExternalId = "InternalExternalID";
        public const string PartyStatus = "PartyStatus";
        public const string PartyStatusId = "PartyStatusId";
        public const string LEI = "LEI";
        public const string AuthoriserId = "AuthoriserId";
        public const string CommercialParticipant = "CommercialParticipant";
        public const Int32 BSCLE = 10;
        public const Int32 SuspenseLE = 2134291;
        public const Int32 USA = 309; //Country ID

        public const Int32 PartyDoddFrankReportingPartyUnk = 4;
        public const Int32 PartyDoddFrankTypeUnk = 4;
        public const Int32 TaxExemptStatusUnk = 3;
        public const Int32 TaxDomicileUnk = 3;

        public const string PartyDoddFrankTypeID = "PartyDoddFrankTypeID";
        public const string DoddFrankType = "DoddFrankType";
        public const string DoddFrankTypeId = "DoddFrankTypeId";
        public const string DoddFrankReportingPartyId = "DoddFrankReportingPartyId";
        public const string PrincipalTradingCountry = "PrincipalTradingCountry";
        public const string RegisteredCountry = "RegisteredCountry";
        public const string TaxExemptStatusId = "TaxExemptStatusId";

        public const string PartyDoddFrankReportingParty = "PartyDoddFrankReportingParty";
        public const string PartyDoddFrankReportingPartyId = "PartyDoddFrankReportingPartyId";

        public const string TaxDomicileID = "TaxDomicileID";
        public const string TaxDomicile = "TaxDomicile";

        public const string TaxExemptID = "TaxExemptID";
        public const string Name = "Name";

        public const string IConRequiredId = "IConRequiredId";
        public const string OConRequiredId = "OConRequiredId";
        public const string InvoiceRequiredId = "InvoiceRequiredId";
        public const string MICCode = "MICCode";
        public const string TaxId = "TaxId";
        public const string CountryID = "CountryID";

        public const string ConfirmingPartyID = "ConfirmingPartyID";
        public const string ConfirmingParty = "ConfirmingParty";
        public const string ConfirmingPartyId = "ConfirmingPartyId";

        public const string ConfirmMethodID = "ConfirmMethodID";
        public const string ConfirmMethod = "ConfirmMethod";
        public const string BeaconID = "BeaconID";
        public const string SchedulingPartyName = "SchedulingPartyName";
    }

    static public class BrokerRates
    {
        public const string PartyID = "PartyID";
        public const string BrokerID = "BrokerID";
        public const string PartyBrokerRateID = "PartyBrokerRateID";
        public const string PartyBrokerRateType = "PartyBrokerRateType";
        public const string FeeTypeID = "FeeTypeID";
        public const string ExchangeID = "ExchangeID";
        public const string ExecutionTypeID = "ExecutionTypeID";
        public const string UnderlyingID = "UnderlyingID";
        public const string SettlementTypeID = "SettlementTypeID";
        public const string BrokerRateTypeID = "BrokerRateTypeID";
        public const string MiniBrokerRateTypeID = "MiniBrokerRateTypeID";
        public const string ExchangeProductCodeID = "ExchangeProductCodeID";
        public const string RateCurrencyID = "RateCurrencyID";
        public const string PaymentFrequencyID = "PaymentFrequencyID";
        public const string PaymentDateOffset = "PaymentDateOffset";
        public const string TradeTypeId = "TradeTypeId";
        public const string FeeRate = "FeeRate";
        public const string MiniFeeRate = "MiniFeeRate";
        public const string DefaultRate = "DefaultRate";
    }

    static public class Contact
    {
        public const string TitleID = "PartyTitleID";
        public const string Title = "PartyTitle";
        public const string ID = "ID";
        public const string PartyID = "PartyID";
        public const string PartyContactID = "PartyContactID";
        public const string PartyAddressID = "PartyAddressID";
        public const string IsActive = "IsActive";

        public const string TFValue = "TFValue";
        public const string TFShow = "TFShow";

        public const string PartyAddressTypeID = "PartyAddressTypeID";
        public const string PartyTitleID = "PartyTitleID";
        public const string FirstName = "FirstName";
        public const string LastName = "LastName";
        public const string DefaultID = "DefaultID";
        public const string DefaultId = "DefaultId";
        public const string PhoneNumber = "PhoneNumber";
        public const string Email = "Email";
        public const string Invalid = "Invalid";
        public const string FaxNumber = "FaxNumber";

    }
    static public class BrokerFunction
    {
        public const Int32 OTCBroker = 1;
        public const Int32 Clearing = 4;
        public const Int32 Execution = 3;
        public const string ExecutionType = "ExecutionType";
        public const string ExecutionTypeID = "ExecutionTypeID";
        public const Int32 ExecutionTypeVoice = 1;
    }

    static public class CashFlowType
    {
        public const Int32 ClearingFee = 2;
        public const Int32 ExecutionFee = 3;
    }

    static public class Function
    {
        public const string PartyFunctionID = "PartyFunctionID";
        public const string Name = "Name";
        public const string PartyId = "PartyId";
        public const string PartyFunctionTypeID = "PartyFunctionTypeID";
        public const string Associated = "Associated";
        public const string PartyFunctionType = "PartyFunctionType";
    }
    static public class Portfolio
    {
        public const string PortfolioName = "Portfolio";
        public const string PortfolioID = "PortfolioID";
        public const string Associated = "Associated";
    }

    static public class AgHelper
    {
        public const string Associated = "Associated";
        public const string CurrencyISOCode = "CurrencyISOCode";
        public const string Currency = "Currency";
        public const string DeliveryType = "DeliveryType";
        public const string SettlementType = "SettlementType";
        public const string DeliveryTypeId = "DeliveryTypeId";
        public const string UnderlyingID = "UnderlyingID";
        public const string SettlementTypeId = "SettlementTypeId";
        public const string CurrencyID = "CurrencyID";
        public const string Commodity = "Commodity";
        public const string ConfirmRouteID = "ConfirmRouteID";
        public const string PartyAgreementName = "PartyAgreementName";
        public const string CPTYAgreementID = "CPTYAgreementID";
        public const string IntPartyID = "IntPartyID";
        public const string ExtPartyID = "ExtPartyID";
        public const string PartyAgreementTypeID = "PartyAgreementTypeID";
        public const string PartyAgreementStatusID = "PartyAgreementStatusID";
        public const string CoverageStartDate = "CoverageStartDate";
        public const string CoverageEndDate = "CoverageEndDate";
        public const string ContractSignDate = "ContractSignDate";
        public const string PartyAgreementCalculationAgentId = "PartyAgreementCalculationAgentId";
        public const string PartyAgreementVersionID = "PartyAgreementVersionID";
    }
    static public class PortfolioSubSt
    {
        public const string PortfolioSubStrategyID = "PortfolioSubStrategyID";
        public const string PortfolioSubStrategy = "PortfolioSubStrategy";
    }

    static public class AgreementConditions
    {
        public const string PartyAgreementConditionID = "PartyAgreementConditionID";
        public const string PartyAgreementCondition = "PartyAgreementCondition";
    }

    static public class Agreements
    {
        public const string PartyAgreementID = "PartyAgreementID";
        public const string PartyAgreementName = "PartyAgreementName";
        public const string PartyAgreementType = "PartyAgreementType";
        public const string PartyAgreementConditionID = "PartyAgreementConditionID";
    }
    static public class AddressNames
    {
        public const string DefaultID = "DefaultID";
        public const string ID = "ID";
        public const string Name = "Name";
        public const string ShortName = "ShortName";

        public const string IsDefaultValue = "YNValue";
        public const string IsDefaultShow = "YNShow";

        public const string PartyAddressTypeID = "PartyAddressTypeID";
        public const string PartyAddressTypeName = "PartyAddressTypeName";
        public const string City = "City";
        public const string State = "State";
        public const string Country = "Country";
        public const string AddressLine1 = "AddressLine1";
        public const string AddressLine2 = "AddressLine2";
        public const string AddressLine3 = "AddressLine3";
        public const string AddressLine4 = "AddressLine4";
        public const string AddressLine5 = "AddressLine5";

        public const string MailCode = "MailCode";
        public const string GroupEmail = "GroupEmail";
        public const string GroupPhone = "GroupPhone";
        public const string GroupFax = "GroupFax";
        public const string Description = "Description";

        public const string PartyID = "PartyID";
        public const string PartyAddressID = "PartyAddressID";
        public const string CityID = "CityID";
        public const string StateID = "StateID";
        public const string StatesID = "StatesID";
        public const string CountryID = "CountryID";
    }

    static public class CashFlow
    {
        public const string CashFlowType = "CashFlowType";
        public const string CashFlowTypeId = "CashFlowTypeId";
    }

    static public class TTypes
    {
        public const string TradeType = "TradeType";
        public const string TradeTypeID = "TradeTypeID";
        public const Int32 Future = 1;
        public const Int32 Forward = 4;
        public const Int32 Swap = 3;
    }

    static public class OTypes
    {
        public const string OptionType = "OptionType";
        public const string OptionTypeID = "OptionTypeID";
    }

    static public class UType
    {
        public const string Unit = "Unit";
        public const string UnitID = "UnitID";
    }

    static public class CType
    {
        public const string CurrencyID = "CurrencyID";
        public const string CurrencyISOCode = "CurrencyISOCode";
    }

    static public class TradeCal
    {
        public const string HolidayCalendarID = "HolidayCalendarID";
        public const string HolidayCalendar = "HolidayCalendar";
    }

    static public class PowerPrice
    {
        public const string PowerPriceBandID = "PowerPriceBandID";
        public const string PowerPriceBand = "PowerPriceBand";
    }

    static public class DTypes
    {
        public const string DeliveryType = "DeliveryType";
        public const Int32 Electricity = 6;
        public const Int32 Gas = 3;
        public const string DeliveryTypeId = "DeliveryTypeId";
    }

    static public class UTypes
    {
        public const Int32 MWh = 7;
        public const Int32 MW = 8;
    }

    static public class ExTypes
    {
        public const string ExecutionType = "ExecutionType";
        public const string ExecutionTypeID = "ExecutionTypeID";
    }

    static public class STypes
    {
        public const string SettlementType = "SettlementType";
        public const string SettlementTypeID = "SettlementTypeID";
        public const Int32 Physical = 2;
        public const Int32 Financial = 1;
    }

    static public class CalcPeriod
    {
        public const string CalculationPeriodId = "CalculationPeriodId";
        public const string CalculationPeriod = "CalculationPeriod";
        public const Int32 D = 1;
        public const Int32 M = 3;
    }

    static public class SpecPrice
    {
        public const string SpecifiedPriceId = "SpecifiedPriceId";
        public const string SpecifiedPrice = "SpecifiedPrice";
        public const Int32 LocationalMarginal = 8;
        public const Int32 Settlement = 17;
    }

    static public class PricingDay
    {
        public const string OTCDayDistributionID = "OTCDayDistributionID";
        public const string OTCDayDistribution = "OTCDayDistribution";

        public const Int32 First = 2134213;
        public const Int32 Last = 2134214;
        public const Int32 Penultimate = 2134215;
        public const Int32 Each = 2134216;
        public const Int32 All = 2134217;
        /*
        2134213	First
        2134214	Last
        2134215	Penultimate
        2134216	Each
        2134217	All   
        */
    }

    static public class PricingDayType
    {
        public const string OTCDayDayTypeID = "OTCDayTypeID";
        public const string OTCDayDayType = "DayType";
        public const Int32 Calendar = 2134214;
        public const Int32 CommodityBusiness = 2134215;
    }

    static public class PricingDeliveryDate
    {
        public const string OTCPricingDeliveryDateID = "OTCPricingDeliveryDateID";
        public const string ShortDescription = "ShortDescription";
        public const Int32 CalculationPeriod = 2134213;
    }

    static public class RollConv
    {
        public const string OTCRollConventionDescID = "OTCRollConventionDescID";
        public const string RollConventionDesc = "RollConventionDesc";
        public const Int32 NONE = 2134213;
        public const Int32 EOM = 2134221;
    }

    static public class SwapTp
    {
        public const string OTCSwapTypeID = "OTCSwapTypeID";
        public const string OTCSwapType = "OTCSwapType";
        public const Int32 FixedFloat = 1;
        public const Int32 FloatFloat = 2;

        public const Int32 FreeFormGas = 2134264;
        public const Int32 FreeFormPower = 2134263;

        public const Int32 DefaultGas = 3116220;  //NATURAL GAS-NYMEX
        public const Int32 DefaultPower = 3116223;//ELECTRICITY PJM-WESTERN HUB-DAY AHEAD

        public const string OTCSwapSpreadID = "OTCSwapSpreadID";
        public const string PayIndexID = "PayIndexID";
        public const string IsIncDec = "IsIncDec";
        public const string ReceiveIndexID = "ReceiveIndexID";

        public const Int32 PJMLE = 2134270;
    }

    static public class AvgMethod
    {
        public const string AveragingTypeId = "AveragingTypeId";
        public const string AveragingType = "AveragingType";
        public const Int32 Unweighted = 1;
        public const Int32 Weighted = 2;
    }

    static public class PBRateTypes
    {
        public const string PartyBrokerRateType = "PartyBrokerRateType";
        public const string PartyBrokerRateTypeID = "PartyBrokerRateTypeID";
    }

    static public class CCY
    {
        public const string Currency = "Currency";
        public const string CurrencyID = "CurrencyID";
        public const string CurrencyISOCode = "CurrencyISOCode";
        public const Int32 USD = 8;
    }

    static public class HolidayCalendar
    {
        public const Int32 NYC = 1000021;
    }

    static public class BuySell
    {
        public const string BS = "BuySell";
        public const string BuySellID = "BuySellID";
        public const Int32 Buy = 1;
        public const Int32 Sell = 2;
        public const Int32 USD = 8;
    }

    static public class SwapInfo
    {
        public const string GasLocationID = "GasLocationID";
        public const string PowerLocationID = "PowerLocationID";
        public const Int32 ATC = 5;
        public const Int32 InsideFerc = 309;
        public const Int32 GasDaily = 303;
        public const Int32 NATGasNIMEX = 3116220;
        //3116220 -- NATURAL GAS-NYMEX
        public const string eConfirmProductID = "eConfirmProductID";//eConfirmProductID
        public const string PricePublicationId = "PricePublicationId";

        public const Int32 GasDailyPP = 17;
        public const Int32 InsideFercPP = 18;
    }

    static public class VolType
    {
        public const string VolumeType = "VolumeType";
        public const string VolumeTypeID = "VolumeTypeID";
        public const Int32 Contract = 1;
        public const Int32 Daily = 2;
        public const Int32 Hourly = 3;
        public const Int32 Period = 4;
        public const Int32 Monthly = 5;
    }
    static public class CurveDef
    {
        public const string CurveDefinitionID = "CurveDefinitionID";
        public const string CurveName = "CurveName";
    }

    static public class PPhysPrRef
    {
        public const string OTCCommodityPricingRefID = "OTCCommodityPricingRefID";
        public const string GUIName = "GUIName";

        public const string OTCSwapSpreadID = "OTCSwapSpreadID";
        public const string OTCSwapSpreadName = "OTCSwapSpreadName";
        public const string PayIndexID = "PayIndexID";
        public const string ReceiveIndexID = "ReceiveIndexID";
        public const string DefaultIndexConversion = "DefaultIndexConversion";

        public const string CommodityPricingRef = "CommodityPricingRef";
        public const string Description = "Description";
        public const string TradeableIndex = "TradeableIndex";
        public const string CurveDefinitionID = "CurveDefinitionID";
        public const string MultiLegs = "MultiLegs";
        public const string SpecifiedPriceID = "SpecifiedPriceID";
        public const string PricePublicationId = "PricePublicationId";
        public const string CurrencyID = "CurrencyID";
        public const string UnitID = "UnitID";
        public const string AveragingTypeID = "AveragingTypeID";
        public const string CalculationPeriodID = "CalculationPeriodID";
        public const string HolidayCalendarID = "HolidayCalendarID";
        public const string DayDistributionID = "DayDistributionID";
        public const string DayTypeID = "DayTypeID";
        public const string PricingDeliveryDateID = "PricingDeliveryDateID";
        public const string RollConventionID = "RollConventionID";
        public const string DeliveryTypeID = "DeliveryTypeID";

    }

    static public class TPricingInfo
    {
        public const string TradePricingInfoId = "TradePricingInfoId";
        public const string TradeId = "TradeId";
        public const string PricingTypeID = "PricingTypeID";
        public const string FixedPxSpread = "FixedPxSpread";
        public const string FixedPxSpreadCcyID = "FixedPxSpreadCcyID";
        public const string FixedPxSpreadUnitID = "FixedPxSpreadUnitID";
        public const string PriceIndexID = "PriceIndexID";
        public const string PriceIndexCcyID = "PriceIndexCcyID";
        public const string PriceIndexPercent = "PriceIndexPercent";
        public const string SpecifiedPriceID = "SpecifiedPriceID";
        public const string PricingCalendarID = "PricingCalendarID";
        public const string CalculationPeriodID = "CalculationPeriodID";
        public const string Rounding = "Rounding";
        public const string DayDistributionID = "DayDistributionID";
        public const string DayTypeID = "DayTypeID";
        public const string PricingDeliveryDateID = "PricingDeliveryDateID";
        public const string DayCountID = "DayCountID";
        public const string DeliveryDate = "DeliveryDate";
        public const string RollConventionID = "RollConventionID";
        public const string AveragingMethodID = "AveragingMethodID";
        public const string FXReferenceID = "FXReferenceID";
        public const string FXTypeID = "FXTypeID";
        public const string FXRate = "FXRate";
        public const string CreationTimestamp = "CreationTimestamp";
        public const string CreateBy = "CreateBy";
        public const string UpdateTimestamp = "UpdateTimestamp";
        public const string UpdateBy = "UpdateBy";
        public const string CreateByUIID = "CreateByUIID";
        public const string UpdateByUIID = "UpdateByUIID";
        public const string PayReceive = "PayReceive";
    }
    static public class Spread
    {
        public const string OTCSwapSpreadID = "OTCSwapSpreadID";
        public const string OTCSwapSpreadCommodityID = "OTCSwapSpreadCommodityID";
        public const string OTCSwapSpreadName = "OTCSwapSpreadName";
        public const string PayIndexID = "PayIndexID";
        public const string ReceiveIndexID = "ReceiveIndexID";
        public const string DefaultIndexConversion = "DefaultIndexConversion";
        public const string eConfirmProductID = "eConfirmProductID";
        public const string IsIncDec = "IsIncDec";
        public const string CreationTimestamp = "CreationTimestamp";
        public const string CreateBy = "CreateBy";
        public const string UpdateTimestamp = "UpdateTimestamp";
        public const string UpdateBy = "UpdateBy";
        public const string CreateByUIID = "CreateByUIID";
        public const string UpdateByUIID = "UpdateByUIID";
        public const string Deleted = "Deleted";
    }
    static public class GasLoc
    {
        public const string GasLocationID = "GasLocationID";
        public const string GasLocation = "GasLocation";
        public const string Deleted = "Deleted";
    }
    static public class PowerLoc
    {
        public const string TimeZoneId = "TimeZoneId";
        public const string Name = "Name";

        public const string PowerOperationalRegion = "PowerOperationalRegion";
        public const string PowerOperationalRegionID = "PowerOperationalRegionID";

        public const string PowerZone = "PowerZone";
        public const string PowerZoneID = "PowerZoneID";

        public const string PowerLocationType = "PowerLocationType";
        public const string PowerLocationTypeID = "PowerLocationTypeID";

        public const string PowerLocationID = "PowerLocationID";
        public const string PowerLocation = "PowerLocation";
        public const string Description = "Description";
        public const string FullDescription = "FullDescription";
        public const string ShortCode = "ShortCode";
        public const string SchedulingLocationName = "SchedulingLocationName";
        public const string PowerLocationTypeId = "PowerLocationTypeId";
        public const string PowerZoneId = "PowerZoneId";
        public const string CurveDefinitionId = "CurveDefinitionId";
        public const string PNodeID = "PNodeID";
        public const string PowerOperationalRegionId = "PowerOperationalRegionId";
        public const string InvoiceDescription = "InvoiceDescription";
        public const string Active = "Active";
        public const string UpdateTimestamp = "UpdateTimestamp";
        public const string UpdateUser = "UpdateUser";
        public const string Deleted = "Deleted";
    }

    static public class Unt
    {
        public const string Unit = "Unit";
        public const string UnitId = "UnitId";
        public const Int32 MW = 8;
        public const Int32 MWh = 7;
        public const Int32 MMBTU = 5;
    }

    static public class PrType
    {
        public const string PricingTypeID = "PricingTypeID";
        public const string PricingType = "PricingType";
        public const Int32 Fixed = 1;
        public const Int32 Index = 2;
    }

    static public class PayCon
    {
        public const string PaymentConventionID = "PaymentConventionID";
        public const string PaymentConvention = "PaymentConvention";
        public const Int32 ModFollow = 3;
        public const Int32 FOLLOWING = 2134225;
    }

    static public class CurveInfo
    {
        public const string YieldBasis = "YieldBasis";
        public const string YieldBasisID = "YieldBasisID";
        public const string CurveTypeID = "CurveTypeID";
        public const string Name = "Name";

        public const string InterpolationMethod = "InterpolationMethod";
        public const string InterpolationMethodID = "InterpolationMethodID";

        public const string CurveName = "CurveName";
        public const string Description = "Description";
        public const string CurveTypeId = "CurveTypeId";
        public const string UnderlyingId = "UnderlyingId";
        public const string UnitID = "UnitID";
        public const string CurrencyID = "CurrencyID";
        public const string LIMCode = "LIMCode";
        public const string YieldBasisId = "YieldBasisId";
        public const string InterpolationMethodId = "InterpolationMethodId";
        public const string Tradeable = "Tradeable";
        public const string Deleted = "Deleted";
    }

    static public class ClearingRateInfo
    {
        public const string ExchangeClearingRateID = "ExchangeClearingRateID";
        public const string ExchangeProductId = "ExchangeProductId";
        public const string ExchangeId = "ExchangeId";
        public const string MarketRegistrationRate = "MarketRegistrationRate";
        public const string ClearingHouseRate = "ClearingHouseRate";
        public const string FeePerLot = "FeePerLot";
        public const string CurrencyId = "CurrencyId";
        public const string ExecutionTypeID = "ExecutionTypeID";
        public const string EffectiveFrom = "EffectiveFrom";
        public const string EffectiveTill = "EffectiveTill";
        public const string Deleted = "Deleted";
    }

    static public class OTCPrRefInfo
    {
        public const string OTCCommodityPricingRefID = "OTCCommodityPricingRefID";
        public const string CommodityPricingRef = "CommodityPricingRef";
        public const string Description = "Description";
        public const string GUIName = "GUIName";
        public const string TradeableIndex = "TradeableIndex";
        public const string CurveDefinitionID = "CurveDefinitionID";
        public const string MultiLegs = "MultiLegs";
        public const string SpecifiedPriceID = "SpecifiedPriceID";
        public const string PricePublicationId = "PricePublicationId";
        public const string CurrencyID = "CurrencyID";
        public const string UnitID = "UnitID";
        public const string AveragingTypeID = "AveragingTypeID";
        public const string CalculationPeriodID = "CalculationPeriodID";
        public const string HolidayCalendarID = "HolidayCalendarID";
        public const string DayDistributionID = "DayDistributionID";
        public const string DayTypeID = "DayTypeID";
        public const string PricingDeliveryDateID = "PricingDeliveryDateID";
        public const string RollConventionID = "RollConventionID";
        public const string DeliveryTypeID = "DeliveryTypeID";
        public const string PowerLocationID = "PowerLocationID";
        public const string GasLocationID = "GasLocationID";
        public const string eConfirmProductID = "eConfirmProductID";
        public const string PricingDates = "PricingDates";
        public const string InvoiceDescription = "InvoiceDescription";
        public const string VersionNumber = "VersionNumber";
        public const string CreationTimestamp = "CreationTimestamp";
        public const string UpdateTimestamp = "UpdateTimestamp";
        public const string UpdateUser = "UpdateUser";
        public const string Deleted = "Deleted";

        public const string GasDaily = "GAS DAILY";
        public const string InsideFerc = "INSIDE FERC";
        public const string DayAhead = "DAY AHEAD";
        public const string RealTime = "REAL TIME";
        public const string Nymex = "NYMEX";
        public const string PJM = "PJM";
        public const string MISO = "MISO";
    }

    static public class PrRefInfo
    {
        public const string SpecifiedPrice = "SpecifiedPrice";
        public const string SpecifiedPriceID = "SpecifiedPriceID";

        public const string Publication = "Publication";
        public const string PricePublicationId = "PricePublicationId";

        public const string PricingReferenceID = "PricingReferenceID";
        public const string PricingReference = "PricingReference";
        public const string Description = "Description";
        public const string PricingReferenceCCYID = "PricingReferenceCCYID";

        public const string Deleted = "Deleted";
    }

    static public class AgreementStatus
    {
        public const string PartyAgreementStatusID = "PartyAgreementStatusID";
        public const string PartyAgreementStatus = "PartyAgreementStatus";
    }

    static public class AgreementAgent
    {
        public const string PartyAgreementCalculationAgentId = "PartyAgreementCalculationAgentId";
        public const string PartyAgreementCalculationAgent = "PartyAgrementCalculationAgent";
        public const Int32 AsPerAgreement = 4;
    }

    static public class AgreementVersion
    {
        public const string PartyAgreementVersionID = "PartyAgreementVersionID";
        public const string PartyAgreementVersion = "PartyAgreementVersion";
    }

    static public class ExVenueType
    {
        public const string ExecutionVenueTypeID = "ExecutionVenueTypeID";
        public const string ExecutionVenueType = "ExecutionVenueType";
        public const Int32 OffFacility = 4;
    }

    static public class OTCCollateralizationType
    {
        public const string CollateralizationTypeID = "CollateralizationTypeID";
        public const string CollateralizationType = "CollateralizationType";
        public const Int32 PartiallyCollateralized = 5;
    }

    static public class OTCSwapPurpose
    {
        public const string SwapPurposeID = "SwapPurposeID";
        public const string SwapPurpose = "SwapPurpose";
        public const Int32 Undisclosed = 3;
    }

    static public class AgreementType
    {
        public const string PartyAgreementTypeID = "PartyAgreementTypeID";
        public const string PartyAgreementType = "PartyAgreementType";
    }

    static public class HolCal
    {
        public const string HolidayCalendarID = "HolidayCalendarID";
        public const string HolidayCalendar = "HolidayCalendar";
        public const Int32 NYC = 1000021;
        public const Int32 NymexNatgas = 1000018;
        public const Int32 InsideFerc = 1000009;
        public const Int32 GD = 1000005;
        public const Int32 PJM = 1000019;
        public const Int32 MISO = 1000012;
        public const Int32 NODEFAULT = -1;
    }
    static public class Loc
    {
        public const string GasLocationID = "GasLocationID";
        public const string GasLocation = "GasLocation";
        public const string PowerLocationID = "PowerLocationID";
        public const string PowerLocation = "PowerLocation";
    }

    static public class PowMarketType
    {
        public const string PowerPricingMarketTypeID = "PowerPricingMarketTypeID";
        public const string PowerPricingMarketType = "PowerPricingMarketType";
        public const string MarketType = "MarketType";
        public const string MarketTypeId = "MarketTypeId";
    }

    static public class PartyBrokerRateType
    {
        public const Int32 LumpSum = 1;
        public const Int32 TotalNotional = 2;
        public const Int32 Override = 2134214;
    }

    static public class PayReceive
    {
        public const Int32 Pay = 1;
        public const Int32 Receive = 2;
    }
    static public class DirectionLabels
    {
        public const string Pay = "Pay";
        public const string Receive = "Receive";
    }

    static public class OTCPaymentEvt
    {
        public const string OTCPaymentEventId = "OTCPaymentEventId";
        public const string OTCPaymentEvent = "PaymentEvent";
        public const Int32 EndofDeliveryMonth = 2134218;
        public const Int32 EndofCalendarMonth = 2134217;
        public const Int32 CalculationPeriodEndDate = 2134214;
        public const Int32 LastPricingDate = 2134215;
        /*
        2134213	CalculationDate
        2134214	CalculationPeriodEndDate
        2134215	LastPricingDate
        2134216	EndofMonth
        2134219	LastDeliveryDate
        2134220	AfterTradeDate
        2134221	AfterExercise
        2134222	EndofQuarter
        2134223	EndofYear
        2134224	Settlement
        */
    }

    static public class PowerOTCCalendar
    {
        public const string DayTypeId = "DayTypeId";
        public const string DayType = "DayType";
        public const string OTCDayTypeId = "OTCDayTypeId";
        public const Int32 Calendar = 2134214;
        public const Int32 Business = 2134213;
    }


    static public class Slevel
    {
        public const string ServiceLevelId = "ServiceLevelId";
        public const string ServiceLevel = "ServiceLevel";
        public const Int32 FirmLD = 2134213;
    }

    static public class SettFreq
    {
        public const string OTCSettlementPeriodId = "OTCSettlementPeriodId";
        public const string SettlementPeriod = "SettlementPeriod";

        public const Int32 Daily = 2134213;
        public const Int32 Weekly = 2134214;
        public const Int32 Monthly = 2134215;
        public const Int32 Quarterly = 2134216;
        public const Int32 Yearly = 2134217;
        public const Int32 Term = 2134218;
        public const Int32 BiWeekly = 2134219;
    }

    static public class CurDev
    {
        public const string CurveName = "CurveName";
        public const string CurveDefinitionID = "CurveDefinitionID";
    }

    static public class PModel
    {
        public const string PricingModel = "PricingModel";
        public const string PricingModelID = "PricingModelID";
    }

    static public class PPub
    {
        public const string PricePublicationId = "PricePublicationId";
        public const string Publication = "Publication";
    }

    static public class PRef
    {
        public const string PricingReferenceID = "PricingReferenceID";
        public const string PricingReference = "PricingReference";
    }

    static public class CSInfo
    {
        public const string TradeId = "TradeId";
        public const string TradeSettlementEventId = "TradeSettlementEventId";
        public const string ExternalTradeId = "ExternalTradeId";
        public const string ExternalReference = "ExternalReference";
        public const string EventStatusID = "EventStatusID";
        public const string SettlementTypeID = "SettlementTypeID";
        public const string SettlementPeriodStart = "SettlementPeriodStart";
        public const string SettlementPeriodEnd = "SettlementPeriodEnd";
        public const string PaymentDate = "PaymentDate";
        public const string EventQuantity = "EventQuantity";
        public const string EventQuantityUnitID = "EventQuantityUnitID";
        public const string EventPrice = "EventPrice";
        public const string PricingIndexID = "PricingIndexID";
        public const string EventSpread = "EventSpread";
        public const string EventPriceCcyID = "EventPriceCcyID";
        public const string SettlementAmount = "SettlementAmount";
        public const string SettlementAmountCcyID = "SettlementAmountCcyID";
        public const string PayerID = "PayerID";
        public const string PayeeID = "PayeeID";
        public const string CreationTimestamp = "CreationTimestamp";
        public const string CreateBy = "CreateBy";
        public const string UpdateTimestamp = "UpdateTimestamp";
        public const string UpdateBy = "UpdateBy";
        public const string CreateByUIID = "CreateByUIID";
        public const string UpdateByUIID = "UpdateByUIID";
    }

    static public class CashSettEvt
    {
        public const string FirstDay = "FirstDay";
        public const string LastDay = "LastDay";
        public const string BusinessDays = "BusinessDays";
        public const string PeakHours = "PeakHours";
        public const string OffpeakHours = "OffpeakHours";
        public const Int32 Peak = 2;
        public const Int32 OffPeak = 3;
        public const Int32 ATC = 5;
        public const Int32 Baseload = 1000008;

        public const Int32 ModFollow = 3;
        public const Int32 ModPreceding = 4;
        public const Int32 ModWeekend = 5;

        public const Int32 MWh = 7;
        public const Int32 MW = 8;
        public const Int32 MMBTU = 5;

        public const Int32 CashEvent = 2134213;

        public const Int32 Buy = 1;
        public const Int32 Sell = 2;
    }

    static public class PFrequency
    {
        public const string CalculationPeriodId = "CalculationPeriodId";
        public const string CalculationPeriod = "CalculationPeriod";
    }

    static public class PartyClass
    {
        public const string PartyClassID = "PartyClassID";
        public const string ShortName = "ShortName";
        public const string LongName = "LongName";
        public const string PartyID = "PartyID";
        public const string Description = "Description";
        public const Int32 LE = 1;
        public const Int32 BU = 2;
        public const Int32 Internal = 1;
        public const Int32 External = 2;
    }

    static public class PowPhysInfo
    {
        public const Int32 IndexConst = 500;
        public const Int32 FixedConst = 501;
        public const Int32 PJM = 4;
    }

    static public class PZone
    {
        public const string PowerZoneID = "PowerZoneID";
        public const string PowerZone = "PowerZone";
        public const string PowerRegionId = "PowerRegionId";
    }

    static public class PRegion
    {
        public const string PowerRegionID = "PowerRegionID";
        public const string PowerRegion = "PowerRegion";
    }

    static public class PLoc
    {
        public const string PowerLocationID = "PowerLocationID";
        public const string PowerLocation = "PowerLocation";
    }

    static public class PProd
    {
        public const string PowerProductID = "PowerProductID";
        public const string PowerProduct = "PowerProduct";
    }

    static public class TZones
    {
        public const string TimeZoneId = "TimeZoneId";
        public const string Code = "Code";
    }
    static public class OptInfo
    {
        public const string OTCTradeOptionHeaderID = "OTCTradeOptionHeaderID";
        public const string TradeID = "TradeID";
        public const string OptionTypeID = "OptionTypeID";
        public const string OptionStyleID = "OptionStyleID";
        public const string OptionPricingDatesID = "OptionPricingDatesID";
        public const string OptionExpirationFrequencyID = "OptionExpirationFrequencyID";
        public const string OptionExpirationTypeID = "OptionExpirationTypeID";
        public const string OptionLeadDays = "OptionLeadDays";
        public const string OptionLeadDaysTypeID = "OptionLeadDaysTypeID";
        public const string OptionLeadDaysCalendarID = "OptionLeadDaysCalendarID";
        public const string OptionExerciseTime = "OptionExerciseTime";
        public const string OptionExerciseTimeZoneID = "OptionExerciseTimeZoneID";
        public const string OptionExerciseWrittenConfirmationRequired = "OptionExerciseWrittenConfirmationRequired";
        public const string OptionStatusID = "OptionStatusID";
        public const string OptionDescriptionID = "OptionDescriptionID";

        public const string OptionExerciseDayConventionID = "OptionExerciseDayConventionID";
        public const string OptionExerciseAnchorDate = "OptionExerciseAnchorDate";

    }
    static public class PPInfo
    {
        public const string TradeId = "TradeId";
        public const string OffsetDealID = "OffsetDealID";
        public const string StructureID = "StructureID";
        public const string ExternalTradeId = "ExternalTradeId";
        public const string VersionNumber = "VersionNumber";
        public const string TradeStatusId = "TradeStatusId";
        public const string TradeDate = "TradeDate";
        public const string TradeExecutionDateTime = "TradeExecutionDateTime";
        public const string ExecutionTypeId = "ExecutionTypeId";
        public const string ExecutionVenueTypeId = "ExecutionVenueTypeId";
        public const string BuySellId = "BuySellId";
        public const string CommodityId = "CommodityId";
        public const string SettlementTypeId = "SettlementTypeId";
        public const string TradeTypeId = "TradeTypeId";
        public const string TradeSubTypeId = "TradeSubTypeId";
        public const string IntCounterpartyId = "IntCounterpartyId";
        public const string ExtCounterpartyId = "ExtCounterpartyId";
        public const string AgreementId = "AgreementId";
        public const string TraderId = "TraderId";
        public const string SalesPersonId = "SalesPersonId";
        public const string PortfolioId = "PortfolioId";
        public const string StrategyId = "StrategyId";
        public const string Reference = "Reference";
        public const string ExtTraderId = "ExtTraderId";
        public const string ExtSalesPersonId = "ExtSalesPersonId";
        public const string ExtPortfolioId = "ExtPortfolioId";
        public const string ExtStrategyId = "ExtStrategyId";
        public const string StartDate = "StartDate";
        public const string EndDate = "EndDate";
        public const string PriceIndexId = "PriceIndexId";
        public const string LocationId = "LocationId";
        public const string ProductId = "ProductId";
        public const string ServiceLevelId = "ServiceLevelId";
        public const string TimezoneId = "TimezoneId";
        public const string UPIStub = "UPIStub";
        public const string USI = "USI";
        public const string eConfirmProductID = "eConfirmProductID";
        public const string Quantity = "Quantity";
        public const string QuantityUnitId = "QuantityUnitId";
        public const string QuantityFrequencyId = "QuantityFrequencyId";
        public const string QuantityUnitConversionFactor = "QuantityUnitConversionFactor";
        public const string Price = "Price";
        public const string PriceUnitId = "PriceUnitId";
        public const string PriceCcyId = "PriceCcyId";
        public const string PriceUnitConversionFactor = "PriceUnitConversionFactor";
        public const string HeatRate = "HeatRate";
        public const string Rounding = "Rounding";
        public const string TotalNotionalQty = "TotalNotionalQty";
        public const string TotalNotionalQtyUnitId = "TotalNotionalQtyUnitId";
        public const string SettlementCcyId = "SettlementCcyId";
        public const string PaymentTermDays = "PaymentTermDays";
        public const string PaymentTermDayTypeId = "PaymentTermDayTypeId";
        public const string PaymentTermAnchorEventId = "PaymentTermAnchorEventId";
        public const string PaymentTermCalendarId = "PaymentTermCalendarId";
        public const string PaymentTermPaymentConvId = "PaymentTermPaymentConvId";
        public const string PaymentFrequencyId = "PaymentFrequencyId";
        public const string OffMarketPrice = "OffMarketPrice";
        public const string LargeSizeTrade = "LargeSizeTrade";
        public const string IntentToClear = "IntentToClear";
        public const string Cleared = "Cleared";
        public const string IncrementDecrement = "IncrementDecrement";
        public const string AccountingObservabilityId = "AccountingObservabilityId";
        public const string CommonPricing = "CommonPricing";
        public const string MarketDisruption = "MarketDisruption";
        public const string MarketTypeId = "MarketTypeId";
        public const string IndependentAmount = "IndependentAmount";
        public const string IndependentAmountCCYID = "IndependentAmountCCYID";
        public const string IndependentAmountPaymentDate = "IndependentAmountPaymentDate";

        public const string BrokerID = "BrokerID";
        public const string BrokerRateTypeID = "BrokerRateTypeID";
        public const string BrokerAmountCCYID = "BrokerAmountCCYID";
        public const string BrokerRate = "BrokerRate";
        public const string BrokerAmount = "BrokerAmount";

        public const string CreateBy = "CreateBy";
        public const string CreateByUIID = "CreateByUIID";
        public const string CreationTimestamp = "CreationTimestamp";
        public const string UpdateBy = "UpdateBy";
        public const string UpdateByUIID = "UpdateByUIID";
        public const string UpdateTimestamp = "UpdateTimestamp";

        public const string LEInt = "LEInt";
        public const string LEExt = "LEExt";

        public const string CollateralizationTypeID = "CollateralizationTypeID";
        public const string EFRP = "EFRP";
        public const string BuyerEndUserException = "BuyerEndUserException";
        public const string SellerEndUserException = "SellerEndUserException";

        public const string RegBuyerCooperativeClearingExemption = "RegBuyerCooperativeClearingExemption";
        public const string RegSellerCooperativeClearingExemption = "RegSellerCooperativeClearingExemption";
        public const string RegBuyerTreasuryAffiliateClearingRelief = "RegBuyerTreasuryAffiliateClearingRelief";
        public const string RegSellerTreasuryAffiliateClearingRelief = "RegSellerTreasuryAffiliateClearingRelief";

        public const string SwapPurposeID = "SwapPurposeID";
    }

    static public class PowerFutures
    {
        public const string ExchangeProductId = "ExchangeProductId";
        public const string Name = "Name";
        public const string ExchangeProductCode = "ExchangeProductCode";
        public const string GUIDescription = "GUIDescription";

        public const string FirstTradeDate = "FirstTradeDate";
        public const string LastTradeDate = "LastTradeDate";

        public const string FirstDeliveryDate = "FirstDeliveryDate";
        public const string LastDeliveryDate = "LastDeliveryDate";

        public const string ExchangeContractId = "ExchangeContractId";
        public const string ContractCode = "ContractCode";
        public const string ContractDate = "ContractDate";

        public const string SettlementTypeId = "SettlementTypeId";
        public const string OptionTypeId = "SettlementTypeId";
        public const string ForwardCurveId = "ForwardCurveId";
        public const string FuturesCurveId = "FuturesCurveId";
        public const string PricingReferenceId = "PricingReferenceId";
        public const string LotSize = "LotSize";
        public const string LotSizeUnitId = "LotSizeUnitId";
        public const string PowerPricebandId = "PowerPricebandId";
    }

    public static class MessageWarning
    {
        public const string AreYouSureDeletion = "Are you sure you want to cancel the selected item?";
    }

    static public class Personnel
    {
        public const string UserName = "UserName";
        public const string PersonnelID = "PersonnelID";
    }

    static public class CurveDefinition
    {
        public const string CurveDefinitionID = "CurveDefinitionID";
        public const string CurveName = "CurveName";
        public const string UnderlyingId = "UnderlyingId";
    }

    static public class AssetHierarchy
    {
        public const string UnderlyingId = "UnderlyingId";
        public const string AssetClass = "AssetClass";
        public const string AssetGroup = "AssetGroup";
        public const string DeliveryTypeId = "DeliveryTypeId";
        public const string UPICommodity = "UPICommodity";
    }

}

