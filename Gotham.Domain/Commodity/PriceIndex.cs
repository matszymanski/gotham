﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain
{
    public class PriceIndex : IEqualityComparer<PriceIndex>
    {
        public int ID { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public List<string> UserPriceIndexIDs { get; set; }

        public PriceIndex()
        {
            UserPriceIndexIDs = new List<string>();
        }

        public bool Equals(PriceIndex x, PriceIndex y)
        {
            return x == y;
        }

        public int GetHashCode(PriceIndex obj)
        {
            return obj.ID.GetHashCode();
        }
    }
}
