﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Gotham.Domain.Confirmation
{
    public enum Statuses
    {
        New,
        Generated,
        Sent
        , PendingApproval
        , ECMPending
        , ECMMatched
        , ECMFailure
        , ECMNotMatched
        , FailedDelivery
        , CounterpartyConfirmed
        , Amended
        , Cancelled
        , NoConfirmation
        , Disputed
        , Confirmed
        , PDFConfirmationToBeCreated
        , ReadyToBeSent
        , WaitingForCounterpartyConfirm

    }

    public static class StatusesUtil
    {
        public static string ToDisplayString(this Statuses status)
        {
            var split = Regex.Split(status.ToString(), @"(?<!^)(?=[A-Z])");

            return string.Join(" ", split.Take(1).Union(split.Skip(1).Select(s => s.ToLowerInvariant())));
        }

        public static string ToDisplayString(string status)
        {
            if (string.IsNullOrEmpty(status))
                return status;

            var split = Regex.Split(status, @"(?<!^)(?=[A-Z])");

            return string.Join(" ", split.Take(1).Union(split.Skip(1).Select(s => s.ToLowerInvariant())));
        }

        public static Statuses? FromDisplayString(string status)
        {
            if (Enum.TryParse<Statuses>(status.Replace(" ", ""), true, out Statuses parsed))
                return parsed;
            else
                return null;
        }
    }
}
