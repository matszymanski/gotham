﻿using Gotham.Domain.Confirmation;
using System;

namespace Gotham.Domain
{
    public class ConfirmationViewContainer
    {
        public int SchemaID { get; set; }
        public string Root { get; set; }

        public DateTimeOffset InsertionTimeStamp { get; set; }

        public Statuses ConfirmationStatus { get; set; }

        public string ClassName { get; set; }

        public string InternalPartyName { get; set; }
        public string ExternalPartyName { get; set; }
    }
}
