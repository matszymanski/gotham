﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Confirmation
{
    public class ConfirmationStatus
    {
        
        public static IEnumerable<Statuses> GetNext(Statuses status)
        {
            switch (status)
            {
                case Statuses.New:
                    return new List<Statuses> { Statuses.Cancelled, Statuses.Generated, Statuses.WaitingForCounterpartyConfirm, Statuses.NoConfirmation };
                case Statuses.Cancelled:
                    return new List<Statuses>();
                case Statuses.Generated:
                    return new List<Statuses> { Statuses.Cancelled, Statuses.NoConfirmation, Statuses.Sent };
                case Statuses.WaitingForCounterpartyConfirm:
                    return new List<Statuses> { Statuses.Cancelled, Statuses.NoConfirmation, Statuses.CounterpartyConfirmed, Statuses.New };
                case Statuses.NoConfirmation:
                    return new List<Statuses>();
                case Statuses.FailedDelivery:
                    return new List<Statuses> { Statuses.Sent };
                case Statuses.CounterpartyConfirmed:
                    return new List<Statuses>();
                case Statuses.Sent:
                    return new List<Statuses> { Statuses.FailedDelivery, Statuses.CounterpartyConfirmed, Statuses.Disputed };
                case Statuses.Disputed:
                    return new List<Statuses> { Statuses.Generated };
                case Statuses.PendingApproval:
                    return new List<Statuses> { Statuses.Disputed, Statuses.Confirmed };
                case Statuses.Confirmed:
                    return new List<Statuses> { Statuses.Amended };
                case Statuses.Amended:
                    return new List<Statuses> { Statuses.Disputed, Statuses.CounterpartyConfirmed, Statuses.WaitingForCounterpartyConfirm, Statuses.Generated, Statuses.NoConfirmation };
                default:
                    return new List<Statuses>();
            }
        }

    }
}
