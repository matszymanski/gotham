﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Confirmation
{
    public class TranslationPhrase
    {
        public string Phrase { get; set; }
        public string Translation { get; set; }
    }
}
