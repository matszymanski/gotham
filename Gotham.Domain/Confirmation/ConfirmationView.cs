﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Gotham.Elements.Instruments;

namespace Gotham.Domain.Confirmation
{
    public class ConfirmationView
    {
        public TradeRepresentation Trade { get; set; }
        public string InternalPartyName { get; set; }
        public string ExternalPartyName { get; set; }
        public DateTimeOffset InsertionTimeStamp { get; set; }

        public int SchemaID { get; set; }
        public IEnumerable<SchemaField> SchemaFields { get; set; }

        private Statuses _status = Statuses.New;
        public Statuses Status { get { return _status; } set { _status = value; } }

        //public DateTimeOffset CreationTime { get; set; }

        public string[] NextStatuses => ConfirmationStatus.GetNext(_status).Select(status => status.ToString()).ToArray();

        public DataTable BaseTable()
        {
            var table = new System.Data.DataTable();
            table.Columns.Add("Trade ID");
            table.Columns.Add("Internal party ID");
            table.Columns.Add("Internal party name");
            table.Columns.Add("External party ID");
            table.Columns.Add("External party name");
            table.Columns.Add("Status");
            table.Columns.Add("NextStatuses", typeof(string[]));
            table.Columns.Add("Insertion time");
            return table;
        }

        public object[] BaseTableValues()
        {
            return new object[] {
                Trade?.TradeID,
                Trade?.InternalPartyID,
                InternalPartyName,
                Trade?.ExternalPartyID,
                ExternalPartyName,
                StatusesUtil.ToDisplayString(Status),
                NextStatuses.Select(status => StatusesUtil.ToDisplayString(status)).ToArray(),
                InsertionTimeStamp
            };
        }

    }
}
