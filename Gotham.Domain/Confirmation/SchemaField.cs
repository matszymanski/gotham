﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Confirmation
{
    public class SchemaField
    {
        public int SchemaID { get; set; }

        public int KeyID { get; set; }
        public string Key { get; set; }

        public string Tag { get; set; }

        public bool IsFixed { get; set; }

        public int Columns { get; set; }
        public string DisplayName { get; set; }

        public override int GetHashCode()
        {
            return SchemaID.GetHashCode()*32 + Key.GetHashCode();
        }

        public string DisplayableName() => string.IsNullOrEmpty(DisplayName) ? Tag : DisplayName;
    }
}
