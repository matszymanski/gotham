﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Confirmation
{
    public class Schema
    {
        public int SchemaID;
        public string SchemaName;
        public int SchemaTypeID;
        public int RootSchemaID;
        public string ClassName;
    }
}
