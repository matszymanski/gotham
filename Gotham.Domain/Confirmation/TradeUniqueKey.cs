﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Confirmation
{
    public class TradeUniqueKey : IEqualityComparer<TradeUniqueKey>
    {
        public string TradeID { get; set; }
        public string InternalPartyID { get; set; }

        public static TradeUniqueKey Create(string tradeID, string internalPartyID)
        {
            return new TradeUniqueKey { TradeID = tradeID, InternalPartyID = internalPartyID };
        }

        public bool Equals(TradeUniqueKey x, TradeUniqueKey y)
        {
            return x.TradeID.Equals(y.TradeID, StringComparison.InvariantCultureIgnoreCase)
                && x.InternalPartyID.Equals(y.InternalPartyID, StringComparison.InvariantCultureIgnoreCase);
        }

        public int GetHashCode(TradeUniqueKey obj)
        {
            return (obj.TradeID + obj.InternalPartyID).GetHashCode();
        }

        public bool IsValid()
        {
            return !string.IsNullOrEmpty(TradeID.Trim()) && !string.IsNullOrEmpty(InternalPartyID.Trim());
        }
    }
}
