﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain
{
    public class HospitalContainer
    {
        public string TradeID { get; set; }
        public string InternalPartyID { get; set; }
        public string ExternalPartyID { get; set; }
        public DateTimeOffset InsertionTimeStamp { get; set; }
        public string ValidationError { get; set; }
    }
}
