﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Confirmation
{
    public class Audit
    {
        public Statuses Status { get; set; }

        public string StatusName => Status.ToDisplayString();

        public DateTimeOffset Timestamp { get; set; }
    }
}
