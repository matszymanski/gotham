﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using ArtisanCode.SimpleAesEncryption;
using System.Net;

namespace Gotham.Domain.MatchBox
{
    public class EmailServer
    {
        public int ID { get; set; }
        public bool IsInbox { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }

        public bool Ssl { get; set; }
        public Protocols Protocol { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public NetworkCredential GetCredentials()
        {
            var decryptor = new RijndaelMessageDecryptor();
            return new NetworkCredential(Login, decryptor.Decrypt(Password));
        }
    }
}