﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.MatchBox
{
    public class Recipient
    {
        public int ID { get; set; }
        public RecipientTypes Type { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }
    }
}