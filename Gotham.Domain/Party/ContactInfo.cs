﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain
{
    public class ContactInfo
    {
       
        public string Phone { get; set; }
        public string Email { get; set; }
        public Address Address { get; set; }

        public ContactInfo()
        {
            Address = new Address();
        }
        internal void UpdateFrom(ContactInfo contactInfo)
        {
            Phone = contactInfo.Phone;
            Email = contactInfo.Email;
            Address.UpdateFrom(contactInfo.Address);
        }
    }
}
