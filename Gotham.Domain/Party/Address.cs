﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain
{
    //http://www.fpml.org/spec/fpml-5-5-4-tr-1/html/reporting/schemaDocumentation/schemas/fpml-shared-5-5_xsd/complexTypes/Address.html
    public class Address
    {
        public int ID { get; set; }
        public string PartyID { get; set; }
        public string StreetAddressLine1 { get; set; }
        public string StreetAddressLine2 { get; set; }
        public string StreetAddressLine3 { get; set; }
        public string StreetAddressLine4 { get; set; }
        public string StreetAddressLine5 { get; set; }

        public string City { get; set; }
        public string State { get; set; }
        private ISOCountry ISOCountry => ISOCountry.FromName(Country);
        public string Country { get; set; }
        public string PostalCode { get; set; }

        internal void UpdateFrom(Address address)
        {
            StreetAddressLine1 = address.StreetAddressLine1;
            StreetAddressLine2 = address.StreetAddressLine2;
            StreetAddressLine3 = address.StreetAddressLine3;
            StreetAddressLine4 = address.StreetAddressLine4;
            StreetAddressLine5 = address.StreetAddressLine5;
            City = address.City;
            State = address.State;
            Country = address.Country;
            PostalCode = address.PostalCode;
        }

        public DataTable AsTable()
        {
            var table = new DataTable();
            table.Columns.Add("PartyID", typeof(string));
            table.Columns.Add("StreetAddressLine1", typeof(string));
            table.Columns.Add("StreetAddressLine2", typeof(string));
            table.Columns.Add("StreetAddressLine3", typeof(string));
            table.Columns.Add("StreetAddressLine4", typeof(string));
            table.Columns.Add("StreetAddressLine5", typeof(string));
            table.Columns.Add("City", typeof(string));
            table.Columns.Add("State", typeof(string));
            table.Columns.Add("Country", typeof(string));
            table.Columns.Add("PostalCode", typeof(string));

            table.Rows.Add(
                PartyID,
                StreetAddressLine1,
                StreetAddressLine2,
                StreetAddressLine3,
                StreetAddressLine4,
                StreetAddressLine5,
                City,
                State,
                Country,
                PostalCode
                );
            return table;
        }
    }
}
