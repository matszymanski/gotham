﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain
{
    public class ISOCountry
    {
        private ISOCountry() { }

        public static ISOCountry FromName(string name)
        {
            if (CodesKeyedOnName.ContainsKey(name.ToUpperInvariant()))
            {
                var match = CodesKeyedOnName[name.ToUpperInvariant()];
                return new ISOCountry { Code = match, Name = NamesKeyedOnCode[match] };
            }
            else
            {
                throw new ArgumentException($"Given name='{name}' does not conform to ISO3166");
            }
        }

        public static ISOCountry FromCode(string threeDigitCode)
        {
            if (NamesKeyedOnCode.ContainsKey(threeDigitCode.ToUpperInvariant()))
            {
                var match = NamesKeyedOnCode[threeDigitCode.ToUpperInvariant()];
                return new ISOCountry { Name = match, Code = CodesKeyedOnName[match] };
            }
            else
            {
                throw new ArgumentException($"Given code='{threeDigitCode}' does not conform to ISO3166 Alpha3");
            }
        }

        public string Name { get; set; }
        public string Code { get; set; }

        public static Dictionary<string, string> NamesKeyedOnCode 
            => ISO3166.Country.List.ToDictionary(ctry => ctry.ThreeLetterCode, ctry => ctry.Name);

        public static Dictionary<string, string> CodesKeyedOnName =>
            ISO3166.Country.List.ToDictionary(ctry => ctry.Name.ToUpperInvariant(), ctry => ctry.ThreeLetterCode);
    }
}
