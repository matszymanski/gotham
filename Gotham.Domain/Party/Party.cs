﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain
{

    //http://www.fpml.org/spec/fpml-5-5-4-tr-1/html/reporting/schemaDocumentation/schemas/fpml-shared-5-5_xsd/groups/PartiesAndAccounts.model/party.html
    public class Party
    {
        public string PartyID { get; set; }
        public string PartyName { get; set; }
        public ContactInfo ContactInfo { get; set; }

        public string Agreement { get; set; }

        public string LEI { get; set; }
        public string EAN { get; set; }
        public string EIC { get; set; }

        public Party()
        {
            ContactInfo = new ContactInfo();
        }


        public void UpdateFrom(Party other)
        {
            PartyName = other.PartyName;
            ContactInfo.UpdateFrom(other.ContactInfo);
        }
    }
}
