﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class PrRefInfoEntity
    {
        public Int32 PricingReferenceID { get; set; }
        public string PricingReference { get; set; }
        public string Description { get; set; }
        public Int32 SpecifiedPriceID { get; set; }
        public Int32 PricePublicationID { get; set; }
        public Int32 CCYID { get; set; }
        public bool?  Deleted { get; set; }
    }
}
