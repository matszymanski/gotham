﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class PowLoc
    {
        public Int32 PowerLocationID { get; set; }
        public string PowerLocation { get; set; }
        public string Description { get; set; }
        public string FullDescription { get; set; }
        public string InvoiceDescription { get; set; }
        public string ShortCode { get; set; }
        public string SchedulingLocationName { get; set; }
        public Int32 PowerLocationTypeId { get; set; }
        public Int32 PowerZoneId { get; set; }
        public Int32 TimeZoneId { get; set; }
        public Int32? PowerOperationalRegionId { get; set; }
        public string PNodeID { get; set; }
        public bool Deleted { get; set; }
    }
}
