﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class ExCon
    {
        public Int32 ExchangeContractId { get; set; }
        public Int32 ExchangeProductId { get; set; }
        public string ContractCode { get; set; }
        public string ContractDate { get; set; }
        public DateTime FirstTradeDate { get; set; }
        public DateTime LastTradeDate { get; set; }
        public DateTime SettlementDate { get; set; }
        public DateTime FirstDeliveryDate { get; set; }
        public DateTime LastDeliveryDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public decimal? ContractSize { get; set; }
        public Int32 UnitId { get; set; }
        public decimal? PeakHours { get; set; }
        public decimal? OffpeakHours { get; set; }
        public Int32 UpdateUser { get; set; }
    }
}
