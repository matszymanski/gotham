﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class BusinessUnitComponents
    {
        public DataTable Address { get; set; }
        public DataTable Contacts { get; set; }
        public DataTable Functions { get; set; }
        public DataTable Portfolios { get; set; }
        public DataTable BrokerRates { get; set; }
        public DataTable Confirmations { get; set; }
        public DataTable TaxForms { get; set; }
        public DataTable Details { get; set; }
    }
}
