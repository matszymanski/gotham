﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class Trade
    {
        public Int32 TradeID { get; set; }
        public Int32 TradeStatusID { get; set; }
        public string TemplateName { get; set; }
        public Int32 TemplateID { get; set; }
        public Int32 BtBTradeID { get; set; }
        public Int32 BuySellID { get; set; }
        public Int32 PriceModellingID { get; set; }
        public Int32 TradeTypeID { get; set; }
        public Int32 TradeSubTypeID { get; set; }
        public Int32 PriceTypeID { get; set; }
        public Int32 ContractCodeID { get; set; }
        public Int32 TraderID { get; set; }
        public string Trader { get; set; }
        public Int32 UpdateUser { get; set; }
        public Int32 SalesPersonID { get; set; }
        public string Reference { get; set; }
        public string TraderComment { get; set; }
        public Int32 InternalBusinessUnitID { get; set; }
        public Int32 InternalPortfolioID { get; set; }
        public Int32 InternalSubPortfolioID { get; set; }
        public Int32 ExternalBusinessUnitID { get; set; }
        public Int32 ExternalPortfolioID { get; set; }
        public Int32 ExternalSubPortfolioID { get; set; }
        public Int32 AgreementID { get; set; }
        public DateTime? TradeTime { get; set; }
        public DateTime TradeDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public Int32 ClearingBrokerID { get; set; }
        public Int32 ClearingBrokerRateTypeID { get; set; }
        public decimal? ClearingBrokerAmount { get; set; }
        public decimal Price { get; set; }
        public Int32 ClearingBrokerCCYID { get; set; }
        public Int32 ExecutionBrokerID { get; set; }
        public Int32 ExecutionBrokerRateTypeID { get; set; }
        public decimal? ExecutionBrokerAmount { get; set; }
        public Int32 ExecutionBrokerCCYID { get; set; }
        public Int32 OTCBrokerID { get; set; }
        public Int32 OTCBrokerRateTypeID { get; set; }
        public decimal? OTCBrokerAmount { get; set; }
        public Int32 OTCBrokerCCYID { get; set; }
        public bool IntentToClear { get; set; }
        public bool NonStandardTerms { get; set; }
        public bool OffMarketPrice { get; set; }
        public bool LargeSizeTrade { get; set; }
        public bool Cleared { get; set; }
        public bool CommonPricing { get; set; }
        public Int32 ExecutionTypeID { get; set; }
        public Int32? ExecutionVenueID { get; set; }
        public Int32? ExecutionVenueTypeID { get; set; }
        public Int32? StructureID { get; set; }
        public string UPI { get; set; }
        public string USI { get; set; }
        public string BSCUSI { get; set; }
        public Int32? ClearingAccount { get; set; }
        public Int32? ClearingSubAccount { get; set; }
        public Int32? AccountingObservability { get; set; }
    }
}
