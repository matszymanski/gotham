﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class FuturesStructuredDeal
    {
        public Int32 TradeStructureDefinitionID { get; set; }
        public Int32 SeasonID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime StopDate { get; set; }
    }
}
