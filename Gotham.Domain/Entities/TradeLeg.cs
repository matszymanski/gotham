﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class TradeLeg
    {
        public Int32 TradeLegId { get; set; }
        public Int32 TradeId { get; set; }
        public Int32? SettlementTypeId { get; set; }
        public Int32? OptionTypeId { get; set; }
        public Int32? PricingIndexId { get; set; }
        public Int32? PricingCurrencyId { get; set; }
        public Int32? PricingSourceId { get; set; }
        public decimal? PricePercentage { get; set; }
        public decimal? Price { get; set; }
        public Int32? Position { get; set; }
        public decimal? Notional { get; set; }
        public decimal? PriceSpread { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public Int32? DeliveryTypeId { get; set; }
        public Int32? UnitId { get; set; }
        public Int32 UpdateUser { get; set; }
    }
}
