﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class PhyPowFwd
    {
        public Int32 TradeId { get; set; }
        public Int32? OffsetDealID { get; set; }
        public string StructureID { get; set; }
        public Int32 TradeStatus { get; set; }
        public DateTime TradeDate { get; set; }
        public Int32 ExecutionTypeId { get; set; }
        public Int32? ExecutionVenueTypeId { get; set; }
        public Int32? CollateralizationTypeId { get; set; }
        public Int32? SwapPurposeID { get; set; }
        public Int32 BuySellId { get; set; }
        public Int32 CommodityId { get; set; }
        public Int32 SettlementTypeId { get; set; }
        public Int32 TradeTypeId { get; set; }
        public Int32 TradeSubTypeId { get; set; }
        public Int32 IntCounterpartyId { get; set; }
        public Int32 ExtCounterpartyId { get; set; }
        public Int32 AgreementId { get; set; }
        public Int32 TraderId { get; set; }
        public Int32 SalesPersonId { get; set; }
        public Int32 PortfolioId { get; set; }
        public Int32 StrategyId { get; set; }
        public Int32? ExTraderId { get; set; }
        public Int32? ExtSalesPersonId { get; set; }
        public Int32? ExtPortfolioId { get; set; }
        public Int32? ExtStrategyId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Int32 PriceIndexId { get; set; }
        public Int32? LocationId { get; set; }
        public Int32 ProductId { get; set; }
        public Int32? ServiceLevelId { get; set; }
        public Int32 TimeZoneId { get; set; }
        public Int32? MarketTypeId { get; set; }
        public string UPIStub { get; set; }
        public string USI { get; set; }
        public string Reference { get; set; }
        public Int32 eConfirmProductID { get; set; }
        public double? Quantity { get; set; }
        public Int32? QuantityUnitId { get; set; }
        public Int32? QuantityFreqId { get; set; }
        public double? QuantityUnitConv { get; set; }
        public double? Price { get; set; }
        public Int32? PriceUnitId { get; set; }
        public Int32? PriceCCYId { get; set; }
        public double? PriceUnitConv { get; set; }
        public double? HeatRate { get; set; }
        public Int32? Rounding { get; set; }
        public double? TotalNotionalQTY { get; set; }
        public Int32? TotalNotionalQTYUnitId { get; set; }
        public Int32? SettlementCCYId { get; set; }
        public Int32? PaymentTermDays { get; set; }
        public Int32? PaymentTermDaysTypeID { get; set; }
        public Int32? PaymentTermAnchorEventId { get; set; }
        public Int32? PaymentTermCalendarId { get; set; }
        public Int32? PaymentTermPaymentConvId { get; set; }
        public Int32? PaymentFrequencyId { get; set; }
        public double? IndependentAmount { get; set; }
        public Int32? IndependentAmountCCYId { get; set; }
        public DateTime? IndependentAmountPaymentDate { get; set; }
        public bool OffMarketPrice { get; set; }
        public bool IncrementDecrement { get; set; }
        public bool LargeSizeTrade { get; set; }
        public bool IntentToClear { get; set; }
        public bool Cleared { get; set; }
        public bool CommonPricing { get; set; }
        public bool PartOfEFRP { get; set; }
        public bool BuyerEndUserException { get; set; }
        public bool SellerEndUserException { get; set; }
        public Int32? AccountingObservabilityId { get; set; }
        public Int32? MarketDisruption { get; set; }
        public Int32? BrokerID { get; set; }
        public Int32? BrokerRateTypeID { get; set; }
        public double? BrokerRate { get; set; }
        public double? BrokerAmount { get; set; }
        public Int32? BrokerAmountCCYID { get; set; }
        public Int32? CreateByUIID { get; set; }
        public Int32? UpdateByUIID { get; set; }
    }
}
