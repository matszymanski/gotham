﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class ExProd
    {
        public Int32 ExchangeProductId { get; set; }
        public string ExchangeProductCode { get; set; }
        public string Name { get; set; }
        public string GUIDescription { get; set; }
        public string BeaconValue { get; set; }
        public Int32 UnderlyingId { get; set; }
        public Int32? PricingModelId { get; set; }
        public Int32? ExchangeId { get; set; }
        public double? LotSize { get; set; }
        public Int32? LotSizeUnitId { get; set; }
        public Int32? CurrencyId { get; set; }
        public double? TickSize { get; set; }
        public Int32? LocationId { get; set; }
        public Int32? PowerPricebandId { get; set; }
        public Int32? TradingCalendarId { get; set; }
        public Int32? SettlementTypeId { get; set; }
        public Int32? OptionTypeId { get; set; }
        public Int32? FuturesCurveId { get; set; }
        public Int32? ForwardCurveId { get; set; }
        public Int32? PricingReferenceId { get; set; }
        public bool BasisContract { get; set; }
        public bool MiniContract { get; set; }
        public Int32? BasisReferenceId { get; set; }
        public Int32? BasisForwardCurveId { get; set; }
        public Int32 TradeTypeId { get; set; }
        public string BrokerProductName { get; set; }
        public string BrokerProductCode { get; set; }
        public string ExchangeProductDescription { get; set; }
        public string SpanCombinedCommodity { get; set; }
        public Int32 VersionNumber { get; set; }
        public Int32 UpdateUser { get; set; }
        public DateTime CreationTimestamp { get; set; }
        public DateTime UpdateTimestamp { get; set; }
    }
}
