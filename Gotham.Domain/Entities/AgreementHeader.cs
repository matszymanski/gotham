﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class AgreementHeader
    {
        public string AgreementName { get; set; }
        public string CptyAgreementID { get; set; }
        public string Description { get; set; }
        public Int32 AgreementID { get; set; }
        public Int32 InternalLE { get; set; }
        public Int32 ExternalLE { get; set; }
        public Int32 AgreementStatusID { get; set; }
        public Int32 AgreementTypeID { get; set; }
        public Int32 AgreementAgentID { get; set; }
        public Int32 AgreementVersionYearID { get; set; }
        public Int32 AgreementCalculationAgentID { get; set; }
        public Int32? ConfirmRouteID { get; set; }
        public DateTime SignedOn { get; set; }
        public DateTime EffectiveFrom { get; set; }
        public DateTime EffectiveTo { get; set; }
    }
}
