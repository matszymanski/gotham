﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class BusinessUnit
    {
        public string ShortName { get; set; }
        public string Code { get; set; }
        public string LongName { get; set; }
        public string SchedulingPartyName { get; set; }
        public string SpiderId { get; set; }
        public string BeaconID { get; set; }
        public Int32 PartyId { get; set; }
        public Int32 PartyClass { get; set; }
        public Int32 InternalExternal { get; set; }
        public Int32 ParentLegalEntity { get; set; }
        public Int32 PartyStatusId { get; set; }
        public Int32? AuthoriserId { get; set; }
        public string LEI { get; set; }
        public Int32 UpdateUser { get; set; }

        public string MICCode { get; set; }
        public string TaxID { get; set; }
        public Int32? ConfirmingPartyID { get; set; }
        public Int32? DoddFrankTypeID { get; set; }
        public Int32? DoddFrankReportingParty { get; set; }
        public Int32? PrincipalTradingCountry { get; set; }
        public Int32? RegisteredCountry { get; set; }
        public Int32? TaxDomicileID { get; set; }
        public Int32? TaxStatusID { get; set; }
        public bool? IConReq { get; set; }
        public bool? OConReq { get; set; }
        public bool? InvoiceReq { get; set; }
        public bool? CommercialParticipant { get; set; }
        public Int32? ConfirmMethodID { get; set; }
    }

}
