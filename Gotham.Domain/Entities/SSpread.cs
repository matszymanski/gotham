﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class SSpread
    {
        public Int32 OTCSwapSpreadID { get; set; }
        public Int32 OTCSwapSpreadCommodityID { get; set; }
        public string OTCSwapSpreadName { get; set; }
        public Int32 PayIndexID { get; set; }
        public Int32 ReceiveIndexID { get; set; }
        public Int32? eConfirmProductID { get; set; }
        public bool IsIncDec { get; set; }
        public bool Deleted { get; set; }
    }
}
