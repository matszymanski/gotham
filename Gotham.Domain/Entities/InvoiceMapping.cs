﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class InvoiceMapping
    {
        public int PartyID { get; set; }
        public string NavisionCustomerNo { get; set; }
        public string NavisionVendorNo { get; set; }
        public string PowerPhysicalAccNo { get; set; }
        public string PowerFinancialAccNo { get; set; }
        public string NatGasPhysicalAccNo { get; set; }
        public string NatGasFinancialAccNo { get; set; }
        public string PowerOptPremAccNo { get; set; }
        public string NatGasOptPremAccNo { get; set; }
        public string BrookerageAccNo { get; set; }
    }

    public class InvoiceDbMapping
    {
        public int PartyID { get; set; }
        public int InvoiceGroupID { get; set; }
        public int DeliveryTypeId { get; set; }
        public string NavisionCustomerNo { get; set; }
        public string NavisionVendorNo { get; set; }
        public string NavisionAccountNo { get; set; }
        public string NavisionCommodityName { get; set; }
        public string InvoiceGroupName { get; set; }
    }

}
