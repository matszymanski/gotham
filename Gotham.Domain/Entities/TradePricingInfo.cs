﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class TradePricingInfo
    {
        public Int32 TradePricingInfoId { get; set; }
        public Int32 TradeId { get; set; }
        public Int32 PricingTypeID { get; set; }
        public double? FixedPxSpread { get; set; }
        public Int32? FixedPxSpreadCcyID { get; set; }
        public Int32? FixedPxSpreadUnitID { get; set; }
        public Int32? PriceIndexID { get; set; }
        public Int32? PriceIndexCcyID { get; set; }
        public double? PriceIndexPercent { get; set; }
        public Int32? SpecifiedPriceID { get; set; }
        public Int32? PricingCalendarID { get; set; }
        public Int32? CalculationPeriodID { get; set; }
        public Int32 Rounding { get; set; }
        public Int32? DayDistributionID { get; set; }
        public Int32? DayTypeID { get; set; }
        public Int32? DayCountID { get; set; }
        public Int32? PricingDeliveryDateID { get; set; }
        public Int32? RollConventionID { get; set; }
        public Int32? AveragingMethodID { get; set; }
        public Int32? FXReferenceID { get; set; }
        public Int32? FXTypeID { get; set; }
        public Int32? PayReceive { get; set; }
        public Int32? eConfirmID { get; set; }
        public double? FXRate { get; set; }
        public Int32? CreateByUIID { get; set; }
        public Int32? UpdateByUIID { get; set; }
    }
}
