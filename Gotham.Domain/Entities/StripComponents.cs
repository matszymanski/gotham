﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class StripComponents
    {
        public Int32 ContractId;
        public decimal Notional;
        public decimal ClearingBrokerAmount;
        public decimal ExecutingBrokerAmount;
    }
}
