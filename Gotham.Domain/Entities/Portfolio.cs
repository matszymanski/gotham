﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class Portfolio
    {
        public string PorfolioName { get; set; }
        public Int32 PortfolioID { get; set; }
        public Int32 UserID { get; set; }
        public Int32 PortfolioOwner { get; set; }
        public Int32 PortfolioType { get; set; }
        public Int32? PortfolioGroup { get; set; }
        public Int32? PortfolioArea { get; set; }
        public Int32? PortfolioLine { get; set; }
        public Int32? PortfolioStrategy { get; set; }
    }
}
