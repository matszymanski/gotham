﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class SelectionItems : ICloneable
    {
        public string Name { get; set; }

        public Int32 Identifier { get; set; }

        public SelectionItems()
        {
        }

        public SelectionItems(string name, Int32 id)
        {
            this.Name = name;
            this.Identifier = id;
        }

        public object Clone()
        {
            SelectionItems clone = new SelectionItems();
            clone.Identifier = this.Identifier;
            clone.Name = this.Name;
            return clone;
        }
    }
}
