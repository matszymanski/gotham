﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class CurDef
    {
        public Int32 CurveDefinitionID { get; set; }
        public string CurveName { get; set; }
        public string Description { get; set; }
        public Int32 CurveTypeID { get; set; }
        public Int32 UnderlyingID { get; set; }
        public Int32 UnitID { get; set; }
        public Int32 CurrencyID { get; set; }
        public string LIMCode { get; set; }
        public Int32 YieldBasisID { get; set; }
        public Int32 InterpolationID { get; set; }
        public bool Tradeable { get; set; }
        public bool Deleted { get; set; }
    }
}
