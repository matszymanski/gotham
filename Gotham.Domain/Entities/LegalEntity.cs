﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class LegalEntity
    {
        public string ShortName { get; set; }
        public string Code { get; set; }

        public string LongName { get; set; }

        public string SpiderId { get; set; }

        public Int32 PartyId { get; set; }

        public Int32 PartyClass { get; set; }

        public Int32 InternalExternal { get; set; }

        public string Group { get; set; }

        public Int32 PartyStatusId { get; set; }
    }
}
