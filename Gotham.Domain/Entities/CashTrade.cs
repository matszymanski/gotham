﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class CashTrade
    {
        public Int32 TradeID { get; set; }

        public Int32 VersionNumber { get; set; }

        public Int32 TradeStatusID { get; set; }

        public Int32 PricingModelID { get; set; }

        public Int32 TradeTypeID { get; set; }

        public Int32 TradeSubTypeID { get; set; }

        public Int32 BuySellID { get; set; }

        public String Reference { get; set; }
        
        public Int32 InternalBUnitID { get; set; }

        public Int32 ExternalBUnitID { get; set; }

        public Int32 InternalPortfolioID { get; set; }

        public Int32 InternalSubPortfolioID { get; set; }

        public DateTime TradeDate { get; set; }

        public DateTime InputDate { get; }

        public String LastUpdateBy { get; }

        public DateTime LastUpdate { get; }

        public DateTime StartDate { get; set; }

        public DateTime MaturityDate { get; set; }

        public String TemplateName { get; set; }

        public Int32 PricingTypeID { get; set; }

    }
}
