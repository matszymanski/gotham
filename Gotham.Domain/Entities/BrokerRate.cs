﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class BrokerRate
    {
        public Int32 BrokerID { get; set; }
        public Int32 PartyBrokerRateID { get; set; }
        public Int32 ExchangeID { get; set; }
        public Int32 ExecutionTypeID { get; set; }
        public Int32 SettlementTypeID { get; set; }
        public Int32 UnderlyingID { get; set; }
        public Int32 TradeTypeID { get; set; }
        public decimal FeeRate { get; set; }
        public Int32 BrokerRateTypeID { get; set; }
        public decimal? MiniFeeRate { get; set; }
        public Int32? MiniBrokerRateTypeID { get; set; }
        public Int32 ExchangeProductCodeID { get; set; }
        public Int32 RateCurrencyID { get; set; }
        public Int32 PaymentFrequencyID { get; set; }
        public string PaymentDateOffset { get; set; }
        public Int32 FeeTypeID { get; set; }
        public bool DefaultRate { get; set; }
    }
}
