﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class Address
    {
        public Int32 PartyAddressID { get; set; }
        public Int32 PartyID { get; set; }
        public Int32 AddressType { get; set; }

        public Int32 AddressDefault { get; set; }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string AddressLine5 { get; set; }

        public string CityId { get; set; }
        public Int32 StateId { get; set; }
        public Int32 CountryId { get; set; }
        public Int32 ContactId { get; set; }
        public Int32 UpdateUser { get; set; }
        public string MailCode { get; set; }
        public string GroupEmail { get; set; }
        public string GroupPhone { get; set; }
        public string GroupFax { get; set; }

        public string Description { get; set; }
    }
}
