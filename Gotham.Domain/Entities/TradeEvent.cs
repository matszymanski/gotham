﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public static class TradeEventType
    {
        public static Int32 CashSettlment = 1;
    }

    public static class TradeEventSubType
    {
        public static Int32 ClearingFee = 2;
        public static Int32 ExecutionFee = 3;
        public static Int32 ClearingHouseFee = 4;
        public static Int32 MarketRegistrationRate = 16;
        public static Int32 NFA = 15;
    }

    public class TradeEvent
    {
        public Int32 TradeEventID { get; set; }
        public Int32 TradeId { get; set; }
        public Int32 LegId { get; set; }
        public Int32 CalculationPeriodId { get; set; }
        public Int32 EventTypeId { get; set; }
        public Int32 EventSubTypeId { get; set; }
        public Int32? ExternalBusinessUnitId { get; set; }
        public decimal? Price { get; set; }
        public decimal? Notional { get; set; }
        public Int32? NotionalUnitId { get; set; }
        public decimal? Amount { get; set; }
        public Int32? CurrencyId { get; set; }
        public DateTime EventDate { get; set; }
        public Int32 EventStatusId { get; set; }
        public Int32 UpdateUser { get; set; }
    }

}
