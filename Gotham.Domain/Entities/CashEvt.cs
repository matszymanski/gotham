﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class CashEvt
    {
        public Int32 TradeID { get; set; }
        public Int32 SettlementID { get; set; }
        public Int32 TradeSettlementEventId { get; set; }        
        public Int32 EventStatusID { get; set; }
        public Int32 SettlementTypeID { get; set; }
        public DateTime SettPeriodStart { get; set; }
        public DateTime SettPeriodEnd { get; set; }
        public DateTime PaymentDate { get; set; }
        public double Quantity { get; set; }
        public Int32 QuantityUnit { get; set; }
        public double? Price { get; set; }
        public double? EffectivePrice { get; set; }
        public double? Spread { get; set; }
        public Int32? CCY { get; set; }
        public double? Amount { get; set; }
        public Int32? AmtCCY { get; set; }
        public Int32? PricingIndexID { get; set; }
        public Int32? SwapID { get; set; }
        public Int32? TradePricingInfoId { get; set; }
        public bool? IsDelivery { get; set; }
        public Int32 PayerID { get; set; }
        public Int32 PayeeID { get; set; }
        public Int32 UserID { get; set; }
        public string TempStatus { get; set; }
        public string TempSpread { get; set; }
        public string TempPrice { get; set; }
        public string TempPriceIndex { get; set; }
        public string TempPayer { get; set; }
        public string TempPayee { get; set; }

        public CashEvt() { }

        public CashEvt (CashEvt copyFrom)
        {
            this.Amount = copyFrom.Amount;
            this.AmtCCY = copyFrom.AmtCCY;
            this.CCY = copyFrom.CCY;
            this.EventStatusID = copyFrom.EventStatusID;
            this.IsDelivery = copyFrom.IsDelivery;
            this.PayeeID = copyFrom.PayeeID;
            this.PayerID = copyFrom.PayerID;
            this.PaymentDate = copyFrom.PaymentDate;
            this.Price = copyFrom.Price;
            this.PricingIndexID = copyFrom.PricingIndexID;
            this.Quantity = copyFrom.Quantity;
            this.QuantityUnit = copyFrom.QuantityUnit;
            this.SettlementID = copyFrom.SettlementID;
            this.SettlementTypeID = copyFrom.SettlementTypeID;
            this.SettPeriodEnd = copyFrom.SettPeriodEnd;
            this.SettPeriodStart = copyFrom.SettPeriodStart;
            this.SwapID = copyFrom.SwapID;
            this.TempPayee = copyFrom.TempPayee;
            this.TempPayer = copyFrom.TempPayer;
            this.TempPrice = copyFrom.TempPrice;
            this.TempPriceIndex = copyFrom.TempPriceIndex;
            this.TempSpread = copyFrom.TempSpread;
            this.TempStatus = copyFrom.TempStatus;
            this.TradeID = copyFrom.TradeID;
            this.TradePricingInfoId = copyFrom.TradePricingInfoId;
            this.TradeSettlementEventId = copyFrom.TradeSettlementEventId;
            this.UserID = copyFrom.UserID;
        }
    }

}
