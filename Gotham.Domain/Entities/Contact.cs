﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class Contact
    {
        public Int32 ID { get; set; }
        public Int32 PartyID { get; set; }
        public Int32 PartyContactID { get; set; }
        public Int32 PartyAddressTypeID { get; set; }
        public Int32 PartyTitleID { get; set; }
        public Int32 DefaultId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string Email { get; set; }
        public Int32 UpdateUser { get; set; }
        public bool? IsActive { get; set; }
        public bool Invalid { get; set; }
    }
}
