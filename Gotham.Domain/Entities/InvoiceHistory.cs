﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class InvoiceHistory
    {
        public int InvoiceHistoryID { get; set; }
        public int InvoiceID { get; set; }
        public DateTime Startdate { get; set; }
        public DateTime Enddate { get; set; }
        public int CommodityId { get; set; }
        public int InvoiceGroupId { get; set; }
        public int CurrencyId { get; set; }
        public string CounterPartyBuIds { get; set; }
        public string InvoiceXmlResult { get; set; }
        public string UpdateTimestamp { get; set; }
        public string UpdateUser { get; set; }
        public bool Deleted { get; set; }
    }
}
