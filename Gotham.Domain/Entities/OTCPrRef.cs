﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class OTCPrRef
    {
        public Int32 OTCCommodityPricingRefID { get; set; }
        public string CommodityPricingRef { get; set; }
        public string Description { get; set; }
        public string MultiLegs { get; set; }
        public Int32 SpecifiedPriceID { get; set; }
        public Int32 PricePublicationId { get; set; }
        public Int32 CurrencyID { get; set; }
        public Int32 UnitID { get; set; }
        public Int32 AveragingTypeID { get; set; }
        public Int32 CalculationPeriodID { get; set; }
        public Int32 HolidayCalendarID { get; set; }
        public Int32 DayDistributionID { get; set; }
        public Int32 DayTypeID { get; set; }
        public Int32 PricingDeliveryDateID { get; set; }
        public Int32 RollConventionID { get; set; }
        public Int32 DeliveryTypeID { get; set; }
        public Int32? PowerLocationID { get; set; }
        public Int32? GasLocationID { get; set; }
        public Int32 eConfirmProductID { get; set; }
        public string PricingDates { get; set; }
        public string InvoiceDescription { get; set; }
        public bool? Deleted { get; set; }
        /*
        */
    }
}
