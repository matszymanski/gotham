﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class CBItem
    {
        public string Name { get; set; }
        public Int32 ExchangeProductId { get; set; }

        public CBItem(string name, Int32 prod)
        {
            this.Name = name;
            this.ExchangeProductId = prod;
        }
    }
}
