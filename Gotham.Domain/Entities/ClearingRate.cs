﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Entities
{
    public class ClearingRate
    {
        public Int32 ClearingRateID { get; set; }
        public Int32? ProductID { get; set; }
        public Int32? CurrencyID { get; set; }
        public Int32? ExchangeID { get; set; }
        public Int32? ExecutionTypeID { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public bool? Deleted { get; set; }
        public double? ClearingHouseRate { get; set; }
        public double? MarketRegRate { get; set; }
        public double? FeePerLot { get; set; }

    }
}
