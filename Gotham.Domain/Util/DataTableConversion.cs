﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain.Util
{
    public class DataTableConversion
    {
        public static DataTable FromObject(object o)
        {
            var table = new DataTable();
            var properties = o.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var prop in properties)
            {
                var columnType = prop.PropertyType;
                if (columnType == typeof(DateTimeOffset?))
                    columnType = typeof(DateTimeOffset);
                if (columnType == typeof(int?))
                    columnType = typeof(int);
                if (columnType == typeof(long?))
                    columnType = typeof(long);

                table.Columns.Add(prop.Name, columnType);
            }
            var dataRow = table.NewRow();
            foreach (var prop in properties)
            {
                var value = prop.GetValue(o);
                dataRow[prop.Name] = value != null ? value : DBNull.Value;
            }
            table.Rows.Add(dataRow);

            return table;
        }
    }
}
