﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain
{
    public class StaticData
    {
        public DataTable GetMaintenanceScreens()
        {
            DataTable dt = this.SimpleList();

            dt.Rows.Add("");
            dt.Rows.Add(Constants.LegalEntities);
            dt.Rows.Add(Constants.BusinessUnits);
            //dt.Rows.Add("Brokers");
            //dt.Rows.Add("Exchanges");
            //dt.Rows.Add(Constants.CashTrades);

            return dt;
        }

        public DataTable GetTradersScreens()
        {

            return new DataTable("");
        }

        private DataTable SimpleList()
        {
            DataTable dt = new DataTable("data");
            dt.Columns.Add("Name");
            return dt;
        }
    }
}
