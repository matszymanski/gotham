﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Confirmations.Core
{

    public abstract class BaseTrade
    {
        public List<Party> Parties { get; set; }
        public TradeHeader tradeHeader { get; set; }
    }

    public class StandardNordicTrade 
    {
       
        public List<Party> Parties { get; set; }
        public TradeHeader tradeHeader { get; set; }
        public CommoditySwap commoditySwap { get; set; }
        public AxpoSystemInformation axpoSystemInformation { get; set; }
    }

    public class StandardNordicElcert
    {
        public List<Party> party { get; set; }
        public TradeHeader tradeHeader { get; set; }
        public envCommoditySwap commoditySwap { get; set; }
        public AxpoSystemInformation axpoSystemInformation { get; set; }
    }

    public class AxpoSystemInformation
    {
        public ETRM etrm { get; set; }
        public PricingModelInfo pricingModelInfo { get; set; }
        public UserParameters userParameters { get; set; }
    }

    public class ETRM
    {
        public string systemName { get; set; }
        public string systemLocale { get; set; }
        public string internalCounterparty { get; set; }
    }

    public class PricingModelInfo
    {
        public string useModel { get; set; }
    }

    public class UserParameters
    {
        public up source { get; set; }
    }

    public class up
    {
        public string system { get; set; }
        public string sourceId { get; set; }
    }

    public class TradeHeader
    {
        public List<partyTradeIdentifier> partyTradeIdentifier { get; set; }
        public string tradeDate { get; set; }
        public TradeExecutionDateTime tradeExecutionDateTime { get; set; }
    }

    public class TradeExecutionDateTime
    {
        public string executionDateTime { get; set; }
        public string executionTimeZone { get; set; }
    }
    public class partyTradeIdentifier
    {
        public string tradeId { get; set; }
        public string tradeIdContext { get; set; }
        public string partyReference { get; set; }
    }

    public class TradeID
    {
        public string id { get; set; }
        public string idType { get; set; }
    }

    public class Party
    {
        //public string id { get; set; }
        public string partyName { get; set; }
        public PartyID partyId { get; set; }
        public string partyRole { get; set; }

        //public string PartyIDType { get; set; }
        //public string IntExt { get; set; }
        public partyAccount partyAccount { get; set; }
    }

    public class PartyID
    {
        public string id { get; set; }
        public string idSource { get; set; }

    }

    public class partyAccount
    {
        public string partyAccountId { get; set; }
        public string partyAccountType { get; set; }
    }

    public class fixedLeg
    {
        public string payerPartyReference { get; set; }
        public string receiverPartyReference { get; set; }
        public string calculationPeriodsScheduleReference { get; set; }

        [JsonProperty(Required = Required.Always)]
        public Price fixedPrice { get; set; }
        public Quantity notionalQuantity { get; set; }
        public double totalNotionalQuantity { get; set; }
        public PaymentDates paymentDates { get; set; }
    }

    public class fixedLegElCert
    {
        public string payerPartyReference { get; set; }
        public string receiverPartyReference { get; set; }
        public string calculationPeriodsScheduleReference { get; set; }
        public Price fixedPrice { get; set; }
        public Quantity notionalQuantity { get; set; }
        public double totalNotionalQuantity { get; set; }
        public PaymentDates paymentDates { get; set; }
        public TotalPrice totalPrice { get; set; }
    }


    public class TotalPrice
    {
        public string currency { get; set; }
        public double amount { get; set; }  //in the case of elcerts, nr of allowances * fixedPrice
    }
    public class PaymentDatesPTD
    {
        public RelativePaymentDatesPTR relativePaymentDates { get; set; }
    }
    public class PaymentDates
    {
        public RelativePaymentDates relativePaymentDates { get; set; }
    }

    public class floatingLeg
    {
        public string payerPartyReference { get; set; }
        public string receiverPartyReference { get; set; }
        public CalculationPeriodsSchedule calculationPeriodSchedule { get; set; }
        public Commodity commodity { get; set; }
        public Quantity notionalQuantity { get; set; }
        public double totalNotionalQuantity { get; set; }
        public Calculation calculation { get; set; }
        public PaymentDates paymentDates { get; set; }

    }

    public class environmentalPhysicalLeg
    {
        public string payerPartyReference { get; set; }
        public string receiverPartyReference { get; set; }
        public Allowances numberOfAllowances { get; set; }
        public Environmental environmental { get; set; }
        public DeliveryDates deliveryDates { get; set; }
        public BusinessCenters businessCenter { get; set; }
    }

    public class DeliveryDates
    {
        public DateTime deliveryDate { get; set; }
    }
    public class Environmental
    {
        public string productType { get; set; }
        public CompliancePeriod compliancePeriod { get; set; }
    }

    public class CompliancePeriod
    {
        public string startYear { get; set; }
        public string endYear { get; set; }
    }
    public class Allowances
    {
        public string name { get; set; }
        public string quantityUnit { get; set; }
        public double quantity { get; set; }
    }

    public class Calculation
    {
        public PricingDates pricingDates { get; set; }
    }

    public class PricingDates
    {
        public string calculationPeriodsScheduleReference { get; set; }
        public string dayType { get; set; }
        public string dayDistribution { get; set; }
        public string businessCalendar { get; set; }
    }

    public class CommodityRef
    {
        public string Instrument { get; set; }
        public string SpecifiedPrice { get; set; }
    }

    public class Commodity
    {
        public string instrumentId { get; set; }
        public string specifiedPrice { get; set; }
        public double indexPercentage { get; set; }
        public string deliveryDates { get; set; }
    }

    public class CalculationPeriodsSchedule
    {
        public string id { get; set; }
        public Int32 periodMultiplier { get; set; }
        public string period { get; set; }
        public bool balanceOfFirstPeriod { get; set; }
    }

    //////public class PricingDates
    //////{
    //////    public string CalculationPeriodsScheduleReference { get; set; }
    //////}

    //////public class PaymentDates
    //////{
    //////    public string payRelativeTo { get; set; }
    //////    public PaymentDaysOffset paymentDaysOffset { get; set; }
    //////    public string businessCenters { get; set; }
    //////    public bool masterAgreementPaymentDates { get; set; }
    //////}

    public class RelativePaymentDates
    {
        //public string id { get; set; }
        public string payRelativeTo { get; set; }
        public string calculationPeriodsScheduleReference { get; set; }
        public PaymentDaysOffset paymentDaysOffset { get; set; }
        public BusinessCenters businessCenter { get; set; }
        public bool? masterAgreementPaymentDates { get; set; }
    }

    public class RelativePaymentDatesPTR
    {
        public string payRelativeTo { get; set; }
        public string calculationPeriodsScheduleReference { get; set; }
    }

    public class BusinessCenters
    {
        public string businessCenter { get; set; }
    }

    public enum BusinessDayConvention
    {
        MODFOLLOW

    }

    public class PaymentDaysOffset
    {
        public Int32 periodMultipier { get; set; }
        public string period { get; set; }
        public string dayType { get; set; }
        
        public BusinessDayConvention businessDayConvention { get; set; }
    }

    public class CommoditySwap
    {
        public Product product { get; set; }
        public CommoditySwapDetails commoditySwapDetails { get; set; }
    }

    public class envCommoditySwap
    {
        public Product product { get; set; }
        public envCommoditySwapDetails envCommoditySwapDetails { get; set; }
    }

    public class envCommoditySwapDetails
    {
        public string effectiveDate { get; set; }
        public string terminationDate { get; set; }
        public string settlementCurrency { get; set; }
        public string powerProduct { get; set; }
        public fixedLeg fixedLeg { get; set; }
        public environmentalPhysicalLeg environmentalPhysicalLeg { get; set; }
        //public MarketDisruption marketDisruption { get; set; }
    }

    public interface ILeg
    {
        int ordinal;
        string name;
        IBehaviour ; //is it fixed or float
    }

    public class CommoditySwapDetails
    {
        public DateTime effectiveDate { get; set; }
        public DateTime terminationDate { get; set; }
        public Currency settlementCurrency { get; set; }
        public string powerProduct { get; set; }
        public ILeg fixedLeg { get; set; }
        public ILeg floatingLeg { get; set; }
        public MarketDisruption marketDisruption { get; set; }
    }

    public class MarketDisruption
    {
        public string marketDisruptionEvents { get; set; }
        public string disruptionFallbacks { get; set; }
    }

    public class Product
    {
        public string primaryAssetClass { get; set; }
        public string secondaryAssetClass { get; set; }
        public string productType { get; set; }
        public string productID { get; set; }
    }
    public class Price
    {
        public double? price { get; set; }
        public string priceCurrency { get; set; }
        public string priceUnit { get; set; }
    }

    public class Quantity
    {
        public double? quantity { get; set; }
        public string quantityFrequency { get; set; }
        public string quantityUnit { get; set; }
    }

    public static class BradyMapping
    {
        public const string TransactionID = "TransactionID";
        public const string InternalCounterpartyID = "InternalCounterpartyID";
        public const string InternalCounterpartyName = "InternalCounterpartyName";
        public const string ExternalCounterpartyID = "ExternalCounterpartyID";
        public const string ExternalCounterpartyName = "ExternalCounterpartyName";
        public const string TradeTime = "TradeTime";
        public const string TimezoneName = "TimezoneName";
        public const string TimezoneOffset = "TimezoneOffset";
        public const string Commodity = "Commodity";
        public const string ProductType = "ProductType";
        public const string PowerProduct = "PowerProduct";
        public const string FromDate = "FromDate";
        public const string ToDate = "ToDate";
        public const string SettlementCurrency = "SettlementCurrency";
        public const string Price = "Price";
        public const string PriceCurrency = "PriceCurrency";
        public const string PriceUnit = "PriceUnit";
        public const string Quantity = "Quantity";
        public const string QuantityUnit = "QuantityUnit";
        public const string QuantityFrequency = "QuantityFrequency";
        public const string TotalNotionalQuantity = "TotalNotionalQuantity";
        public const string CalculationPeriodsScheduleReference = "CalculationPeriodsScheduleReference";
        public const string BusinessCenter = "BusinessCenter";
        public const string PeriodMultiplier = "PeriodMultiplier";
        public const string Period = "Period";
        public const string BalanceOfFirstPeriod = "BalanceOfFirstPeriod";
        public const string InstrumentID = "InstrumentID";
        public const string SpecifiedPrice = "SpecifiedPrice";
        public const string FloatingLegQuantity = "FloatingLegQuantity";
        public const string FloatingLegQuantityUnit = "FloatingLegQuantityUnit";
        public const string FloatingLegQuantityFrequency = "FloatingLegQuantityFrequency";
        public const string FloatingLegTotalNotionalQuantity = "FloatingLegTotalNotionalQuantity";
        public const string FloatingLegCalculationPeriodsScheduleReference = "FloatingLegCalculationPeriodsScheduleReference";
        public const string FloatingLegAveragingMethod = "FloatingLegAveragingMethod";
        public const string MasterAgreementPaymentDates = "MasterAgreementPaymentDates";
        public const string MarketDisruptionEvents = "MarketDisruptionEvents";
        public const string DisruptionFallbacks = "DisruptionFallbacks";
        public const string UTI = "UTI";
        public const string BuySell = "BuySell";
        public const string PutCall = "PutCall";
        public const string Strike = "Strike";
        public const string InstrumentType = "InstrumentType";
        public const string DeliveryType = "DeliveryType";
        public const string Deliveries = "Deliveries";
        public const string IndexFormulaName = "IndexFormulaName";
        public const string SettlementRule = "SettlementRule";
        public const string ConfirmationType = "ConfirmationType";
    }



}
