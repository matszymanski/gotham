﻿using Gotham.Domain;
using Gotham.Domain.Confirmation;
using Gotham.Elements;
using Gotham.Elements.Instruments;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Domain
{
    public class TradeSerializer
    {
        private static Dictionary<string, Type> _assignableTypes;
        
        public static IEnumerable<ConfirmationView> Deserialize(IEnumerable<ConfirmationViewContainer> confirmations, IEnumerable<Party> parties)
        {
            var partiesDict = parties.ToDictionary(x => x.PartyID, y => ToFpmlParty(y));
            return confirmations.Select(confirm => Deserialize(confirm, partiesDict));
        }


        public static ConfirmationView Deserialize(ConfirmationViewContainer confirmation, Dictionary<string, party> partiesDict)
        {
            var type = GetAssignableType(confirmation.ClassName);
            var confirmationView = new ConfirmationView();
            var trade = (TradeRepresentation) JsonConvert.DeserializeObject(confirmation.Root, type);
            AssignParties(trade, partiesDict);
            confirmationView.ExternalPartyName = confirmation.ExternalPartyName;
            confirmationView.InternalPartyName = confirmation.InternalPartyName;
            confirmationView.SchemaID = confirmation.SchemaID;
            confirmationView.Trade = trade;
            confirmationView.InsertionTimeStamp = confirmation.InsertionTimeStamp;
            confirmationView.Status = confirmation.ConfirmationStatus;
            

            return confirmationView;
        }

        private static void AssignParties(TradeRepresentation trade, Dictionary<string, party> partiesDict)
        {
            if (trade.partyA != null && !string.IsNullOrEmpty(trade.partyA.partyId.id))
            {
                var key = trade.partyA.partyId.id;
                if (partiesDict.ContainsKey(key))
                    trade.partyA = partiesDict[key];
            }
            if (trade.partyB != null && !string.IsNullOrEmpty(trade.partyB.partyId.id))
            {
                var key = trade.partyB.partyId.id;
                if (partiesDict.ContainsKey(key))
                    trade.partyB = partiesDict[key];
            }
        }

        private static Type GetAssignableType(string className)
        {
            if(_assignableTypes == null)
            {
                _assignableTypes = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => typeof(TradeRepresentation).IsAssignableFrom(p))
                .ToDictionary(type => type.Name.ToUpperInvariant(), type=>type);
            }

            if (_assignableTypes.ContainsKey(className.ToUpperInvariant()))
                return _assignableTypes[className.ToUpperInvariant()];

            return typeof(object);
        }

        public static Dictionary<string, string> GetTradeTags(ConfirmationView confirmation)
        {
            return GetTradeTags(confirmation.Trade, confirmation.SchemaFields);
        }

        public static Dictionary<string, string> GetTradeTags(TradeRepresentation trade, IEnumerable<SchemaField> schemaFields)
        {
            var result = new Dictionary<string, string>();
            if (!schemaFields.Any() || trade == null) return result;
            
            foreach(var schemaField in schemaFields)
            {
                var value = GetPropValue(trade, schemaField.Key);
                result.Add(schemaField.Tag, value.ToString());
            }
            return result;
        }

        public static DataTable GetTradeDisplayTable(ConfirmationView confirmation)
        {
            var trade = confirmation.Trade;
            var schemaFields = confirmation.SchemaFields;
            var result = confirmation.BaseTable();
            if (!schemaFields.Any() || trade == null) return result;

            var row = new List<object>();
            foreach (var schemaField in schemaFields)
            {

                var display = schemaField.DisplayableName();
                
                var value = GetPropValue(trade, schemaField.Key);

                if(!result.Columns.Contains(display))
                    result.Columns.Add(display, typeof(string));

                row.Add(value == null ? string.Empty : value.ToString());
            }
            result.Rows.Add(confirmation.BaseTableValues().Concat(row.ToArray()).ToArray());
            return result;
        }

        public static object GetPropValue(object obj, string name)
        {
            foreach (string part in name.Split('.'))
            {
                if (obj == null) { return null; }

                Type type = obj.GetType();
                PropertyInfo info;
                var arrayNumStart = part.IndexOf('[');
                if(arrayNumStart >= 0)
                {
                    arrayNumStart++;
                    var arrayNumEnd = part.IndexOf(']');
                    if (arrayNumEnd < arrayNumStart)
                    {
                        obj = null;
                        break;
                    }
                    int arrayNum = int.Parse(part.Substring(arrayNumStart, arrayNumEnd - arrayNumStart));
                    info = type.GetProperty(part.Substring(0, arrayNumStart-1));
                    if (info == null) { return null; }

                    obj = ((IEnumerable<object>)info.GetValue(obj)).ElementAtOrDefault(arrayNum);                    
                }
                else
                {
                    info = type.GetProperty(part);
                    if (info == null) { return null; }

                    obj = info.GetValue(obj, null);
                }

            }
            return obj;
        }

        public static party ToFpmlParty(Party party)
        {
            return new party { partyId = new PartyID { id = party.PartyID }, partyName = party.PartyName };
        }
    }
}
