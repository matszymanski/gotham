﻿using Gotham.Domain.MatchBox;
using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using System.Data.SqlClient;

namespace Gotham.Core.MatchBox
{
    public class MatchBoxService
    {

        public IList<EmailServer> GetEmailServers()
        {
            using (var sqlConn = DB.Factory.Connection())
            {
                sqlConn.Open();
                return sqlConn.Query<EmailServer>($"Select * from {DB.Factory.TabMatchBoxEmailServer}").ToList();
            }
        }

        public IList<Recipient> GetRecipients()
        {
            using (var sqlConn = DB.Factory.Connection())
            {
                sqlConn.Open();
                return sqlConn.Query<Recipient>($"Select * from {DB.Factory.TabMatchBoxRecipient}").ToList();
            }
        }

        public IList<Template> GetTemplates()
        {
            using (var sqlConn = DB.Factory.Connection())
            {
                sqlConn.Open();
                return sqlConn.Query<Template>($"Select * from {DB.Factory.TabMatchBoxTemplate}").ToList();
            }
        }
    }
}
