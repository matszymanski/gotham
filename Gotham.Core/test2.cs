﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Confirmations.Core
{
    public enum BusinessDayConvention
    {
        MODFOLLOW,
        ...

    }

    public StandardNordicTrade CreateSwapRepresentation()
    {
        if (ETRMDataAccess.IsEmpty(this.TransactionRecord)) return null;

        string buySell = ETRMDataAccess.GetStringDTRElement(this.TransactionRecord, BradyMapping.BuySell);

        StandardNordicTrade t = new StandardNordicTrade();

        t.party = new List<party>();

        party intParty = new party();
        intParty.partyName = ETRMDataAccess.GetStringDTRElement(this.TransactionRecord, BradyMapping.InternalCounterpartyName);
        intParty.partyId = new PartyID();
        intParty.partyId.id = ETRMDataAccess.GetStringDTRElement(this.TransactionRecord, BradyMapping.InternalCounterpartyID);
        intParty.partyId.idSource = "ETRM";
        intParty.partyRole = "Counterparty";
        intParty.partyAccount = new partyAccount();
        intParty.partyAccount.partyAccountId = "1123-5544"; //will get it from NordicStaticData in a join when the data are ready
        intParty.partyAccount.partyAccountType = "Bank"; //will get it from NordicStaticData in a join when the data are ready

        party extParty = new party();
        extParty.partyName = ETRMDataAccess.GetStringDTRElement(this.TransactionRecord, BradyMapping.ExternalCounterpartyName);
        extParty.partyId = new PartyID();
        extParty.partyId.id = ETRMDataAccess.GetStringDTRElement(this.TransactionRecord, BradyMapping.ExternalCounterpartyID);
        extParty.partyId.idSource = "ETRM";
        extParty.partyRole = "Counterparty";
        extParty.partyAccount = new partyAccount();
        extParty.partyAccount.partyAccountId = "6654-11224"; //will get it from NordicStaticData in a join when the data are ready
        extParty.partyAccount.partyAccountType = "Bank"; //will get it from NordicStaticData in a join when the data are ready

        t.party.Add(intParty);
        t.party.Add(extParty);

        t.tradeHeader = new TradeHeader();
        t.tradeHeader.partyTradeIdentifier = new List<partyTradeIdentifier>();
        partyTradeIdentifier ti = new partyTradeIdentifier();
        ti.tradeId = ETRMDataAccess.GetStringDTRElement(this.TransactionRecord, BradyMapping.TransactionID);
        ti.partyReference = intParty.partyName;
        ti.tradeIdContext = "ETRM";
        t.tradeHeader.partyTradeIdentifier.Add(ti);

        string uti = ETRMDataAccess.GetStringDTRElement(this.TransactionRecord, BradyMapping.UTI);
        if (!string.IsNullOrEmpty(uti))
        {
            partyTradeIdentifier utiId = new partyTradeIdentifier();
            utiId.tradeId = uti;
            utiId.partyReference = intParty.partyName;
            utiId.tradeIdContext = "UTI";
            t.tradeHeader.partyTradeIdentifier.Add(utiId);
        }

        t.tradeHeader.tradeDate = ETRMDataAccess.GetDateTimeDTRElement(this.TransactionRecord, BradyMapping.TradeTime).Value.ToString(dateFormat);
        t.tradeHeader.tradeExecutionDateTime = new TradeExecutionDateTime();
        t.tradeHeader.tradeExecutionDateTime.executionDateTime = ETRMDataAccess.GetDateTimeDTRElement(this.TransactionRecord, BradyMapping.TradeTime).Value.ToString(dateTimeFormat);
        t.tradeHeader.tradeExecutionDateTime.executionTimeZone = ETRMDataAccess.GetStringDTRElement(this.TransactionRecord, BradyMapping.TimezoneName).Replace("Central Europe Standard Time", "CET");

        CommoditySwap cs = new CommoditySwap();
        cs.product = new Product();
        cs.product.primaryAssetClass = "Commodity";
        cs.product.secondaryAssetClass = "Energy";
        cs.product.productType = ETRMDataAccess.GetStringDTRElement(this.TransactionRecord, BradyMapping.ProductType);

        cs.commoditySwapDetails = new CommoditySwapDetails();
        cs.commoditySwapDetails.effectiveDate = ETRMDataAccess.GetDateTimeDTRElement(this.TransactionRecord, BradyMapping.FromDate).Value.ToString(dateFormat);
        cs.commoditySwapDetails.terminationDate = ETRMDataAccess.GetDateTimeDTRElement(this.TransactionRecord, BradyMapping.ToDate).Value.ToString(dateFormat);
        cs.commoditySwapDetails.settlementCurrency = ETRMDataAccess.GetStringDTRElement(this.TransactionRecord, BradyMapping.SettlementCurrency);
        cs.commoditySwapDetails.powerProduct = ETRMDataAccess.GetStringDTRElement(this.TransactionRecord, BradyMapping.PowerProduct); //

        fixedLeg fl = new fixedLeg()
        {
            
        }
        fl.fixedPrice = new Price();
        fl.fixedPrice.priceCurrency = ETRMDataAccess.GetStringDTRElement(this.TransactionRecord, BradyMapping.PriceCurrency);
        fl.fixedPrice.priceUnit = ETRMDataAccess.GetStringDTRElement(this.TransactionRecord, BradyMapping.PriceUnit).Replace("PerMWh", "MWh");
        fl.fixedPrice.price = ETRMDataAccess.GetDoubleDTRElement(this.TransactionRecord, BradyMapping.Price);

        fl.notionalQuantity = new Quantity();
        fl.notionalQuantity.quantity = ETRMDataAccess.GetDoubleDTRElement(this.TransactionRecord, BradyMapping.Quantity);
        fl.notionalQuantity.quantityUnit = ETRMDataAccess.GetStringDTRElement(this.TransactionRecord, BradyMapping.QuantityUnit);
        fl.notionalQuantity.quantityFrequency = ETRMDataAccess.GetStringDTRElement(this.TransactionRecord, BradyMapping.QuantityFrequency);

        if (buySell == "Buy")
        {
            fl.payerPartyReference = intParty.partyName;
            fl.receiverPartyReference = extParty.partyName;
        }
        else
        {
            fl.payerPartyReference = extParty.partyName;
            fl.receiverPartyReference = intParty.partyName;
        }

        fl.calculationPeriodsScheduleReference = "floatingLegCalculationPeriods";
        //fl.paymentDates = new PaymentDates();
        //fl.paymentDates.relativePaymentDates = new RelativePaymentDates();
        //fl.paymentDates.relativePaymentDates.payRelativeTo = "CalculationPeriodEndDate";
        //fl.paymentDates.relativePaymentDates.calculationPeriodsScheduleReference = "floatingLegCalculationPeriods";

        fl.paymentDates = new PaymentDates();
        fl.paymentDates.relativePaymentDates = new RelativePaymentDates();
        fl.paymentDates.relativePaymentDates.calculationPeriodsScheduleReference = "floatingLegCalculationPeriods";
        fl.paymentDates.relativePaymentDates.payRelativeTo = "CalculationPeriodEndDate";

        fl.paymentDates.relativePaymentDates.paymentDaysOffset = new PaymentDaysOffset();
        fl.paymentDates.relativePaymentDates.paymentDaysOffset.periodMultipier = 5;
        fl.paymentDates.relativePaymentDates.paymentDaysOffset.period = "D";
        fl.paymentDates.relativePaymentDates.paymentDaysOffset.dayType = "Calendar";
        fl.paymentDates.relativePaymentDates.paymentDaysOffset.businessDayConvention = "MODFOLLOW";
        fl.paymentDates.relativePaymentDates.masterAgreementPaymentDates = true;

        fl.totalNotionalQuantity = ETRMDataAccess.GetDoubleDTRElement(this.TransactionRecord, BradyMapping.TotalNotionalQuantity).Value;


        floatingLeg floating = new floatingLeg();

        if (buySell == "Buy")
        {
            floating.payerPartyReference = extParty.partyName;
            floating.receiverPartyReference = intParty.partyName;
        }
        else
        {
            floating.payerPartyReference = intParty.partyName;
            floating.receiverPartyReference = extParty.partyName;
        }

        floating.calculationPeriodSchedule = new CalculationPeriodsSchedule();
        floating.calculationPeriodSchedule.id = "floatingLegCalculationPeriods";
        floating.calculationPeriodSchedule.periodMultiplier = 1;
        floating.calculationPeriodSchedule.period = "M";
        floating.calculationPeriodSchedule.balanceOfFirstPeriod = true;

        floating.commodity = new Commodity();
        floating.commodity.instrumentId = ETRMDataAccess.GetStringDTRElement(this.TransactionRecord, BradyMapping.InstrumentID);
        floating.commodity.specifiedPrice = "Settlement";
        floating.commodity.indexPercentage = 100;
        floating.commodity.deliveryDates = "Calendar";

        floating.notionalQuantity = new Quantity();
        floating.notionalQuantity.quantityUnit = ETRMDataAccess.GetStringDTRElement(this.TransactionRecord, BradyMapping.FloatingLegQuantityUnit);
        floating.notionalQuantity.quantityFrequency = ETRMDataAccess.GetStringDTRElement(this.TransactionRecord, BradyMapping.FloatingLegQuantityFrequency);
        floating.notionalQuantity.quantity = ETRMDataAccess.GetDoubleDTRElement(this.TransactionRecord, BradyMapping.FloatingLegQuantity);

        floating.totalNotionalQuantity = ETRMDataAccess.GetDoubleDTRElement(this.TransactionRecord, BradyMapping.FloatingLegTotalNotionalQuantity).Value;

        floating.calculation = new Calculation();
        floating.calculation.pricingDates = new PricingDates();
        floating.calculation.pricingDates.calculationPeriodsScheduleReference = "floatingLegCalculationPeriods";
        floating.calculation.pricingDates.dayType = "Business";
        floating.calculation.pricingDates.dayDistribution = "All";
        floating.calculation.pricingDates.businessCalendar = "Norway";

        floating.paymentDates = new PaymentDates();
        floating.paymentDates.relativePaymentDates = new RelativePaymentDates();
        floating.paymentDates.relativePaymentDates.calculationPeriodsScheduleReference = "floatingLegCalculationPeriods";
        floating.paymentDates.relativePaymentDates.payRelativeTo = "CalculationPeriodEndDate";

        floating.paymentDates.relativePaymentDates.paymentDaysOffset = new PaymentDaysOffset();
        floating.paymentDates.relativePaymentDates.paymentDaysOffset.periodMultipier = 5;
        floating.paymentDates.relativePaymentDates.paymentDaysOffset.period = "D";
        floating.paymentDates.relativePaymentDates.paymentDaysOffset.dayType = "Calendar";
        floating.paymentDates.relativePaymentDates.paymentDaysOffset.businessDayConvention = "MODFOLLOW";
        floating.paymentDates.relativePaymentDates.masterAgreementPaymentDates = true;

        MarketDisruption md = new MarketDisruption();
        md.disruptionFallbacks = "AsSpecifiedInMasterAgreement";
        md.marketDisruptionEvents = "AsSpecifiedInMasterAgreement";

        cs.commoditySwapDetails.fixedLeg = fl;
        cs.commoditySwapDetails.floatingLeg = floating;
        cs.commoditySwapDetails.marketDisruption = md;

        t.commoditySwap = cs;

        t.axpoSystemInformation = new AxpoSystemInformation();
        t.axpoSystemInformation.etrm = new ETRM();
        t.axpoSystemInformation.etrm.systemName = "Brady ETRM";
        t.axpoSystemInformation.etrm.systemLocale = "Axpo Nordic";
        t.axpoSystemInformation.etrm.internalCounterparty = "Axpo Nordic";

        t.axpoSystemInformation.pricingModelInfo = new PricingModelInfo();
        t.axpoSystemInformation.pricingModelInfo.useModel = "ETRM";

        t.axpoSystemInformation.userParameters = new UserParameters();
        t.axpoSystemInformation.userParameters.source = new up();
        t.axpoSystemInformation.userParameters.source.system = "ETRM";
        t.axpoSystemInformation.userParameters.source.sourceId = ti.tradeId;

        return t;
    }

}
