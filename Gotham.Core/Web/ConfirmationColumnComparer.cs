﻿using System;
using System.Collections.Generic;

namespace Gotham.Core.Web
{
    internal class ConfirmationColumnComparer : IComparer<string>
    {
        public Dictionary<string, int> ColumnOrder { get; private set; }
        public ConfirmationColumnComparer(Dictionary<string, int> columnOrder)
        {
            ColumnOrder = columnOrder;
        }

        public int Compare(string x, string y)
        {
            if (ColumnOrder.ContainsKey(x) && ColumnOrder.ContainsKey(y))
            {
                if (ColumnOrder[x].CompareTo(ColumnOrder[y]) == 0)
                    return x.CompareTo(y);
                else
                    return ColumnOrder[x].CompareTo(ColumnOrder[y]);
            }
            else if (!ColumnOrder.ContainsKey(x) && ColumnOrder.ContainsKey(y))
                return 1;
            else if (ColumnOrder.ContainsKey(x) && !ColumnOrder.ContainsKey(y))
                return -1;
            else
                return x.CompareTo(y);
        }
    }
}