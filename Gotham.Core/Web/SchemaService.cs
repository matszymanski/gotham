﻿using Gotham.Domain.Confirmation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Core.Web
{
    public class SchemaService
    {

        public static IList<Schema> GetSchemas()
        {
            return Data.DB.Schema.GetSchemas().OrderBy(s=>s.SchemaID).ToList();
        }
    }
}
