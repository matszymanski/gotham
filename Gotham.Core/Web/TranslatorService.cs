﻿using Gotham.Domain.Confirmation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Core.Web
{
    public class TranslatorService
    {

        public static IList<TranslationPhrase> GetAllTranslations()
        {
            return Data.DB.Translator.GetAllTranslations();
        }

        public static void Upsert(TranslationPhrase translation)
        {
            Data.DB.Translator.Upsert(translation);
        }
    }
}
