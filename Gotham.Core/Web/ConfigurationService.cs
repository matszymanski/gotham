﻿using Gotham.Domain;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace Gotham.Core.Web
{
    public class ConfigurationService
    {
        //http://staticdata.efet.org/view.aspx?d=CommodityReference
        public static IList<PriceIndex> PriceIndexes()
        {
            
            using (var sqlConn = DB.Factory.Connection())
            {
                sqlConn.Open();
                var priceIndexes = sqlConn.Query<PriceIndex>($"select * from {DB.Factory.ViewTabPriceIndex}");
                var userMap = sqlConn.Query($"select UserPriceIndexID, PriceIndexID from {DB.Factory.ViewPriceIndexMap}");
                var priceIndexesById = priceIndexes.ToDictionary(p => p.ID, p => p);
                foreach(var map in userMap)
                {
                    priceIndexesById[map.PriceIndexID].UserPriceIndexIDs.Add(map.UserPriceIndexID);
                }

                return priceIndexes.ToList();
            }
        }
        
        public static int PriceIndexUpdate(int id, string userPriceIndexId)
        {

            using (var sqlConn = DB.Factory.Connection())
            {
                sqlConn.Open();
                using (var command = new SqlCommand(DB.Factory.SpUpdatePriceIndexMap, sqlConn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PriceIndexID", id);
                    command.Parameters.AddWithValue("@UserPriceIndexID", userPriceIndexId);

                    return command.ExecuteNonQuery();
                }
            }
        }

        public static PriceIndex PriceIndexById(string id)
        {
            using (var sqlConn = DB.Factory.Connection())
            {
                sqlConn.Open();
                var priceIndex = sqlConn.QueryFirstOrDefault<PriceIndex>($"select * from {DB.Factory.ViewTabPriceIndex} where ID = @ID", new { ID = id });
                return priceIndex;
            }
        }

        public static IList<string> PriceIndexesUnaligned(int count)
        {
            using (var sqlConn = DB.Factory.Connection())
            {
                sqlConn.Open();
                var priceIndex = sqlConn.Query<string>($"select TOP {count} * from {DB.Factory.ViewPriceIndexUnaligned}");
                return priceIndex.ToList();
            }
        }

        public static IList<string> PartyIDsUnaligned(int count)
        {
            return Data.DB.Party.PartyIDsUnaligned(count);
        }


        public static IEnumerable<Party> Parties()
        {
            return Data.DB.Party.Parties();
        }

        public static Party PartyById(string partyId)
        {
            return Data.DB.Party.PartyById(partyId);
        }

        public static void PartyUpsert(Party party)
        {
            Data.DB.Party.PartyUpsert(party);
        }
    }
}

