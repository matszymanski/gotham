﻿using Gotham.Domain;
using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using System.Threading.Tasks;
using Gotham.Domain.Confirmation;
using System.Data;

namespace Gotham.Core.Web
{
    public class ConfirmationViewService
    {

        //The data is extracted from confirmation messages. There is the Root field that contains the json of the message. It is then joined on the schema definition and returned as table


        public static DataTable ConfirmationViews()
        {
            var confirmations = Data.DB.Message.ConfirmationViews();
            var schemas = Data.DB.Schema.GetSchemaFields();
            var parties = Data.DB.Party.Parties();
            return AsTable(confirmations, schemas, parties);
        }

        public static DataTable ConfirmationViewsBySchema(int schemaId)
        {
            var confirmations = Data.DB.Message.ConfirmationViewsBySchema(schemaId);
            var schemas = Data.DB.Schema.GetSchemaFields(schemaId);
            var parties = Data.DB.Party.Parties();
            return AsTable(confirmations, schemas, parties);
        }

        public static DataTable ConfirmationViewsNew(int limit)
        {
            var confirmations = Data.DB.Message.ConfirmationViewsNew(limit);
            var schemas = Data.DB.Schema.GetSchemaFields();
            var parties = Data.DB.Party.Parties();
            return AsTable(confirmations, schemas, parties);
        }


        public static DataTable ConfirmationViewById(TradeUniqueKey tradeKey)
        {
            var confirmations = Data.DB.Message.ConfirmationViewById(tradeKey);
            var schemas = Data.DB.Schema.GetSchemaFields();
            var parties = Data.DB.Party.Parties();
            return AsTable(confirmations, schemas, parties);
        }

        public static IList<HospitalContainer> GetHospital() //ToDo Move to hospital service
        {
            return Data.DB.Message.Hospital().OrderByDescending(h => h.InsertionTimeStamp).ToList();
        }


        private static DataTable AsTable(IList<ConfirmationViewContainer> confirmations, IList<SchemaField> schema, IEnumerable<Party> parties)
        {
            var records = TradeSerializer.Deserialize(confirmations, parties);

            var joined = records.GroupJoin(schema, outer => outer.SchemaID, inner => inner.SchemaID, (record, schem) => Tuple.Create(record, schem));
            records = joined.Select(r => { r.Item1.SchemaFields = new List<SchemaField>(r.Item2); return r.Item1; });
            var confTable = MergeIntoTable(records);
            ReorderColumnsInPlace(confTable, schema);
            return confTable;
        }

        public static int GetHospitalCount()
        {
            return Data.DB.Message.GetHospitalCount();
        }

        public static IList<Audit> ConfirmationViewAuditById(TradeUniqueKey tradeKey)
        {
            using (var sqlConn = DB.Factory.Connection())
            {
                sqlConn.Open();
                var audits = sqlConn.Query<Audit>(@"SELECT Status, Timestamp
                                                             FROM [dbo].[GTConfirmationStatus]
                                                             where TradeID = @TradeID and InternalPartyID = @InternalPartyID",
                                                             new { TradeID = tradeKey.TradeID, InternalPartyID = tradeKey.InternalPartyID });

                return audits.OrderByDescending(a=>a.Timestamp).ToList();
            }
        }

        public static Statuses UpdateStatus(TradeUniqueKey tradeKey, Statuses status)
        {
            using (var sqlConn = DB.Factory.Connection())
            {
                sqlConn.Open();
                using (var command = new System.Data.SqlClient.SqlCommand(DB.Factory.SpUpdateConfirmationStatus, sqlConn))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@TradeID", tradeKey.TradeID);
                    command.Parameters.AddWithValue("@InternalPartyID", tradeKey.InternalPartyID);
                    command.Parameters.AddWithValue("@StatusId", status.ToString());
                    command.Parameters.AddWithValue("@Timestamp", DateTimeOffset.Now.ToString("u", System.Globalization.CultureInfo.InvariantCulture));
                    var output = new System.Data.SqlClient.SqlParameter("@NewStatusId", SqlDbType.VarChar, 100);
                    output.Direction = ParameterDirection.Output;
                    command.Parameters.Add(output);

                    command.ExecuteNonQuery();
                    return (Statuses) Enum.Parse(typeof(Statuses), output.Value.ToString(), true);
                }
            }
        }


        private static DataTable MergeIntoTable(IEnumerable<ConfirmationView> records)
        {
            var outputTable = new DataTable();
            foreach (var record in records)
            {
                var recordDT = TradeSerializer.GetTradeDisplayTable(record);
                outputTable.Merge(recordDT);
            }

            return outputTable;
        }

        private static void ReorderColumnsInPlace(DataTable table, IList<SchemaField> schema)
        {
            var columnNames = table.Columns.Cast<DataColumn>().Select(col => col.ColumnName);
            var columnComparer = new ConfirmationColumnComparer(GetColumnOrder(schema));
            var orderedColumns = columnNames.OrderBy(column => column, columnComparer);
            var columnIndex = 0;
            foreach(var columnName in orderedColumns)
            {
                table.Columns[columnName].SetOrdinal(columnIndex);
                columnIndex++;
            }
        }

        private static Dictionary<string, int> GetColumnOrder(IList<SchemaField> schema)
        {
            int i = 1;
            var order = new Dictionary<string, int>();
            order.Add("Trade ID", i++);
            order.Add("Internal party ID", i++);
            order.Add("Internal party name", i++);
            order.Add("External party ID", i++);
            order.Add("External party name", i++);
            order.Add("Status", i++);
            foreach(var schemaField in schema.OrderBy(s=>s.KeyID))
            {
                if(!order.ContainsKey(schemaField.DisplayableName()))
                    order.Add(schemaField.DisplayableName(), i++);
            }

            order.Add("NextStatuses", 50000);
            return order;
        }
    }
}
