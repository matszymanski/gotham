﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Confirmations.Core
{
    public class DisplayTrades
    {
        public List<string> Columns { get; private set; }


        public List<ValueRow> Rows { get; private set; }

        public DisplayTrades(DataTable table)
        {
            Columns = table.Columns.Cast<DataColumn>().Select(column => column.ColumnName).ToList();
            Rows = table.Rows.Cast<DataRow>().Select(row => new ValueRow(row)).ToList();
        }
    }

    public class ValueRow
    {
        public List<string> Values;

        public ValueRow(DataRow values)
        {
            Values = values.ItemArray.Select(v => v.ToString()).ToList();
        }
    }
}
