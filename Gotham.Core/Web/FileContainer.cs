﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gotham.Core.Web
{
    public class FileContainer
    {
        public byte[] File { get; set; }
        public string FileName { get; set; }
    }
}
