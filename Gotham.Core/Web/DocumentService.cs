﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Dapper;
using System.Text;
using System.Threading.Tasks;
using Gotham.Domain.Documents;
using Gotham.Domain.Confirmation;
using System.Data.SqlClient;
using System.Data;

namespace Gotham.Core.Web
{
    public class DocumentService
    {
        public static FileContainer GetGeneratedDocument(long messageId)
        {
            using (var sqlConn = DB.Factory.Connection())
            {
                sqlConn.Open();
                var document = sqlConn.Query<FileContainer>(@"SELECT PdfFile as [File], PdfFileName as [FileName]
                                                             FROM " + DB.Factory.ViewDocument + 
                                                             " where MessageID = @MessageID",
                                                             new { MessageID = messageId });
                return document.FirstOrDefault();
            }
        }

        public static FileContainer GetUploadedDocument(long id)
        {
            using (var sqlConn = DB.Factory.Connection())
            {
                sqlConn.Open();
                var document = sqlConn.Query<FileContainer>(@"SELECT Document as [File], DocumentName as [FileName]
                                                             FROM " + DB.Factory.ViewDocumentUploaded 
                                                             + " where ID = @ID",
                                                             new { ID = id });
                return document.FirstOrDefault();
            }
        }

        public static DocumentInfo[] GetGeneratedDocumentInfos(TradeUniqueKey tradeKey)
        {
            using (var sqlConn = DB.Factory.Connection())
            {
                sqlConn.Open();
                var documents = sqlConn.Query<DocumentInfo>(@"SELECT MessageID, PdfFileName, TimeStamp, PdfFileSizeBytes, TemplateName
                                                             FROM " + DB.Factory.ViewDocument
                                                             + " where TradeID = @TradeID and InternalPartyID = @InternalPartyID",
                                                             new { TradeID = tradeKey.TradeID, InternalPartyID = tradeKey.InternalPartyID });

                return documents.ToArray();
            }
        }

        public static UploadedDocumentInfo[] GetUploadedDocumentInfos(TradeUniqueKey tradeKey)
        {
            using (var sqlConn = DB.Factory.Connection())
            {
                sqlConn.Open();
                var documents = sqlConn.Query<UploadedDocumentInfo>(@"SELECT ID, DocumentName, DocumentSizeBytes, UserID, SourceEmail, AutoMatched, TimeStamp
                                                             FROM " + DB.Factory.ViewDocumentUploaded
                                                             + " where TradeID = @TradeID and InternalPartyID = @InternalPartyID",
                                                             new { TradeID = tradeKey.TradeID, InternalPartyID = tradeKey.InternalPartyID });

                return documents.ToArray();
            }
        }

        public static void UploadDocument(TradeUniqueKey tradeKey, UploadedDocumentInfo info, byte[] documentBytes)
        {
            using (var sqlConn = DB.Factory.Connection())
            {
                using (var sqlComm = new SqlCommand("api.sp_GTUploadDocument", sqlConn))
                {
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@TradeID", tradeKey.TradeID);
                    sqlComm.Parameters.AddWithValue("@InternalPartyID", tradeKey.InternalPartyID);
                    sqlComm.Parameters.AddWithValue("@Document", documentBytes);
                    sqlComm.Parameters.AddWithValue("@DocumentName", info.DocumentName);
                    sqlComm.Parameters.AddWithValue("@UserID", info.UserID.HasValue ? info.UserID.Value : (object)DBNull.Value);
                    sqlComm.Parameters.AddWithValue("@SourceEmail", info.SourceEmail);
                    sqlComm.Parameters.AddWithValue("@AutoMatched", info.AutoMatched);
                    
                    sqlConn.Open();
                    sqlComm.ExecuteNonQuery();
                }
            }
        }
    }
}
