(function () {
    'use strict';

    var app = angular.module('app', []);

})();
(function () {
    'use strict';

    var app = angular.module('app');
    app.controller('mainCtrl', mainCtrl);

    mainCtrl.$inject = ['$location'];

    function mainCtrl($location) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'main';
        vm.navigate = function (element) {
            $location.hash(element);
        };

        activate();

        function activate() { }
    }

    app.component('main', {
        templateUrl: 'app/components/main.html',
        controller: mainCtrl
    });
})();